project('gasstation',
          version: '0.1.0',
    meson_version: '>= 0.59.0',
  default_options: [ 'warning_level=2',
                   ],
)
git = find_program('git', required : false)
revision = ''
if git.found()
revision = run_command('git', 'rev-parse', '--short', 'HEAD').stdout().strip()
endif
if revision == ''
  revision=meson.project_version()
endif

# Importing modules
i18n  = import('i18n')
gnome = import('gnome')

message('Looking for dependencies')
pymod = import('python')
python = pymod.find_installation('python3')
if not python.found()
    error('No valid python3 binary found')
else
    message('Found python3 binary')
endif

dependency('gobject-introspection-1.0', version: '>= 1.35.0')
dependency('glib-2.0')


# Constants
PACKAGE_URL = 'https://wiki.gnome.org/Apps/GasStation'
APPLICATION_ID = 'org.jonah.Gasstation'
DATA_DIR = join_paths(get_option('prefix'), get_option('datadir'), meson.project_name())
MODULE_DIR = python.get_install_dir()
LOCALE_DIR = join_paths(get_option('prefix'), get_option('localedir'))
BIN_DIR = join_paths(get_option('prefix'), get_option('bindir'))

bin_config = configuration_data()
bin_config.set('rdnn_name', APPLICATION_ID)
bin_config.set('DATA_DIR', DATA_DIR)
bin_config.set('LOCALE_DIR', LOCALE_DIR)
bin_config.set('MODULE_DIR', MODULE_DIR)
bin_config.set('VERSION', meson.project_version())
bin_config.set('REVISION', revision)
bin_config.set('PACKAGE_URL', PACKAGE_URL)
bin_config.set('PYTHON_PATH', MODULE_DIR)
bin_config.set('bindir', BIN_DIR)

subdir('po')
subdir('data')

install_subdir(
    'gasstation',
    install_dir: MODULE_DIR
)
install_subdir(
    'libgasstation',
    install_dir: MODULE_DIR,
)

message('Preparing init file')

configure_file(
  input: 'gasstation.in',
  output: 'gasstation',
  configuration: bin_config,
  install_dir: BIN_DIR
)
message('Preparing Config file')
configure_file(
  input: 'config.in',
  output: 'config.py',
  configuration: bin_config,
  install_dir: join_paths(MODULE_DIR, 'gasstation')
)

gnome.post_install(
  glib_compile_schemas: true,
  gtk_update_icon_cache: true,
  update_desktop_database: true,
)
