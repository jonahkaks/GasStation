FROM alpine
FROM python:3.9-slim-buster
ENV TZ=Africa/Kampala
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN --mount=type=cache,target=/root/.cache apt autoremove && apt autoclean && apt-get update && apt-get install -y --no-install-recommends tzdata libgirepository1.0-dev   \
    gcc libcairo2-dev pkg-config git libgtk-3-dev xsltproc gettext desktop-file-utils ninja-build gir1.2-secret-1 gir1.2-webkit2-4.0 gir1.2-cheese-3.0
COPY . /app
WORKDIR /app
RUN rm -rf builddir
RUN pip3 install --upgrade pip
RUN --mount=type=cache,target=/root/.cache pip install -r requirements.txt
RUN ls -a
RUN meson setup builddir --prefix=/usr/local/
RUN ninja -C builddir install
ENTRYPOINT ["gasstation"]
