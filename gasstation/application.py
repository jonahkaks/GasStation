from signal import signal, SIGTERM, SIGINT
from threading import current_thread

from gasstation.url_handler import HtmlUrlHandler
from gasstation.views.dialogs.setup import *
from gasstation.views.dialogs.splash_screen import SplashScreen, SPLASH_PERCENTAGE_UNKNOWN
from gasstation.views.main_window import MainWindow
from libgasstation.core.engine import Engine
from libgasstation.core.price_quotes import *


class Application(Gtk.Application):

    def __init__(self, version, data_directory):
        super().__init__(application_id="org.jonah.GasStation", flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE)
        self.__version = version
        self.__data_dir = data_directory
        self.debug = False
        self.file_to_load = FileHistory.get_last()
        self.main_window = None
        self.is_initialised = False
        self.is_terminating = False
        self.is_running = False
        self.set_property("register-session", True)
        signal(SIGTERM, lambda a, b: self.shutdown(0))
        signal(SIGINT, lambda a, b: self.shutdown(0))
        current_thread().name = "MainThread"

        self.add_main_option("debug", b"d", GLib.OptionFlags.NONE, GLib.OptionArg.NONE, "Debug GasStation", None)
        self.add_main_option("version", b"v", GLib.OptionFlags.NONE, GLib.OptionArg.NONE, "GasStation version", None)
        self.connect("command-line", self.__command_line)
        self.connect("handle-local-options", self.__handle_local_options)
        Engine.init()

    @property
    def version(self):
        return self.__version

    @property
    def data_dir(self):
        """
            Get data dir
            @return str
        """
        return self.__data_dir

    def __handle_local_options(self, app, options):
        if options.contains("version"):
            print("GasStation %s" % self.__version)
            exit(0)
        return -1

    def __command_line(self, app, app_cmd_line):
        try:
            args = app_cmd_line.get_arguments()
            options = app_cmd_line.get_options_dict()
            if options.contains("debug"):
                self.debug = True
            if len(args) > 1:
                self.file_to_load = args[1]
            self.activate()
        except Exception as e:
            print("Application::__on_command_line(): %s", e)
        return 0

    def ui_init(self):
        from gasstation.views.dialogs.scheduled.editor import ScheduledTransactionEditorDialog
        HtmlUrlHandler.init()

        def after_setup_window(window):
            dialog = FileAccessDialog(window, FileAccessType.SAVE_AS)
            if dialog.run() == Gtk.ResponseType.OK:
                url = dialog.geturl()
                if url is not None:
                    File.do_save_as(window, url)
            dialog.destroy()
        Hook.add_dangler(HOOK_NEW_BOOK, lambda window: SetupWindow(window, True, lambda: after_setup_window(window)))
        Hook.run(HOOK_UI_STARTUP)
        Hook.add_dangler(HOOK_BOOK_OPENED, self.restore_all_state)
        Hook.add_dangler(HOOK_BOOK_CLOSED, self.save_all_state)
        ScheduledTransactionEditorDialog.initialise()

    def do_startup(self):
        Gtk.Application.do_startup(self)
        if self.is_initialised:
            return self.main_window
        self.is_initialised = True
        GLib.set_application_name("gasstation")
        GLib.set_prgname("GasStation")
        self.ui_init()
        Hook.add_dangler(HOOK_UI_SHUTDOWN, File.quit)
        File.set_shutdown_callback(self.shutdown)
        Hook.run(HOOK_RESTORE_OPTIONS, GLib.build_filenamev([GLib.get_user_data_dir(), "gasstation"]))

    @staticmethod
    def add_css_file():
        provider_fallback = Gtk.CssProvider()
        provider_app = Gtk.CssProvider()
        provider_user = Gtk.CssProvider()
        display = Gdk.Display.get_default()
        screen = display.get_default_screen()
        Gtk.StyleContext.add_provider_for_screen(screen, provider_fallback, Gtk.STYLE_PROVIDER_PRIORITY_FALLBACK)
        Gtk.StyleContext.add_provider_for_screen(screen, provider_app, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        Gtk.StyleContext.add_provider_for_screen(screen, provider_user, Gtk.STYLE_PROVIDER_PRIORITY_USER)
        provider_app.load_from_resource('org/jonah/Gasstation/css/application.css')
        provider_fallback.load_from_resource('org/jonah/Gasstation/css/application-fallback.css')
        config_path = GLib.build_filenamev([GLib.get_user_config_dir(), "gasstation/"])
        if not ensure_dir(config_path):
            return
        try:
            provider_user.load_from_path(GLib.build_filenamev([config_path, "gtk-3.0.css"]))
        except GLib.Error:
            return

    def do_activate(self):
        splash_screen = SplashScreen()
        splash_screen.update("Checking Finance::Quote...", SPLASH_PERCENTAGE_UNKNOWN)
        PriceQuotes.install_sources()
        Hook.run(HOOK_STARTUP)
        fn = self.file_to_load
        gs_ui_util_init()
        gs_pref_reset_group(PREFS_GROUP_WARNINGS)
        self.main_window = MainWindow()
        self.main_window.set_default_icon_name(name=ICON_APP)
        self.add_css_file()
        self.add_window(self.main_window)
        self.main_window.set_application(self)
        self.main_window.set_progressbar_window(self.main_window)

        if fn is not None and len(fn) > 0:
            splash_screen.update("Loading data...", SPLASH_PERCENTAGE_UNKNOWN)
            if not File.open_file(self.main_window, fn, False):
                splash_screen.destroy()
                Hook.run(HOOK_NEW_BOOK, self.main_window)
            else:
                splash_screen.destroy()
        elif gs_pref_get_bool(PREFS_GROUP_NEW_USER, PREF_FIRST_STARTUP):
            splash_screen.destroy()
            FirstTimeUserDialog(parent=self.main_window)
        else:
            splash_screen.destroy()

        self.main_window.show_all_windows()
        Hook.run(HOOK_UI_POST_STARTUP)
        self.start_event_loop()
        Hook.remove_dangler(HOOK_UI_SHUTDOWN, File.quit)
        self.shutdown(0)

    @staticmethod
    def restore_all_state(session):
        keyfile = State.load(session)
        PrintOperation.load(session)
        if not keyfile.has_group(STATE_FILE_TOP):
            MainWindow.restore_default_state(None)
        MainWindow.restore_all_windows(keyfile)

    @staticmethod
    def save_all_state(session):
        keyfile = State.get_current()
        if keyfile is not None:
            groups, num_groups = keyfile.get_groups()
            for g in groups:
                if g.startswith("Window ") or g.startswith("Page "):
                    keyfile.remove_group(g)
        book = session.get_book()
        guid_string = book.get_guid() if book is not None else None
        if not guid_string:
            return
        keyfile.set_string(STATE_FILE_TOP, STATE_FILE_BOOK_GUID, str(guid_string))
        MainWindow.save_all_windows(keyfile)

    def shutdown(self, exit_status):
        if self.is_running:
            if File.query_save(self.main_window, False):
                Hook.run(HOOK_UI_SHUTDOWN)
                if self.is_running and not self.is_terminating:
                    self.is_terminating = True
                    Hook.run(HOOK_SAVE_OPTIONS, GLib.build_filenamev([GLib.get_user_data_dir(), "gasstation"]))
                    ComponentManager.shutdown()
                    Gtk.main_quit()
        else:
            Hook.run(HOOK_SHUTDOWN)
            Engine.shutdown()
            exit(exit_status)

    @staticmethod
    def check_events():
        if Gtk.main_level() != 1:
            return True
        if not Session.current_session_exist():
            return True
        session = Session.get_current_session()
        if ComponentManager.refresh_suspended():
            return True
        if not session.events_pending():
            logging.info("No events to process")
            return True
        ComponentManager.suspend()
        logging.info("Processing events")
        force = session.process_events()
        ComponentManager.resume()
        if force:
            logging.info("Now refresh after processing events")
            ComponentManager.refresh_all()
        return True

    def start_event_loop(self):
        self.is_running = True
        gid = GLib.timeout_add(10000, Application.check_events, priority=GLib.PRIORITY_DEFAULT_IDLE)
        Gtk.main()
        GLib.source_remove(gid)
        self.is_running = False
        self.is_terminating = False
        return 0
