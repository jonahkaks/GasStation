from .account import AccountTreePluginPage
from .fuel import *
from .person import StakeHolderPluginPage
from .product import *
from .register import *
from .report import *
from .scheduled_list import *
