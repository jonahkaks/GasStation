import weakref
from abc import abstractmethod
from collections import defaultdict
from collections import namedtuple

from gi.repository import GObject, Gtk, Gio, GLib

from gasstation.utilities.actions import add_actions
from libgasstation.core.component import Tracking

PREF_SUMMARY_BAR_POSITION_TOP = "summarybar-position-top"
PREF_SUMMARY_BAR_POSITION_BOTTOM = "summarybar-position-bottom"
PLUGIN_PAGE_LABEL = "plugin-page"
PLUGIN_PAGE_IMMUTABLE = "page-immutable"
PLUGIN_PAGE_CLOSE_BUTTON = "close-button"
PLUGIN_PAGE_TAB_LABEL = "label"

action_toolbar_labels = namedtuple("action_toolbar_labels", "action_name, label")


class PluginPage(GObject.Object):

    __gtype_name__ = "PluginPage"

    __gproperties__ = {
        "name": (str,
                 "Page Name",
                 "The name of this page.  This value is "
                 "used to generate the notebook tab and "
                 "menu items, and also the window title "
                 "when this page is visible.",
                 None,
                 GObject.ParamFlags.READWRITE
                 ),
        "color": (str,
                  "Page Color",
                  "The color of this page.  This value is "
                  "used to generate the notebook tab color "
                  "when this page is visible.",  # blurb
                  None,
                  GObject.ParamFlags.READWRITE  # flags
                  ),
        "uri": (str,
                "Page URI",
                "The uri for this page.",
                None,
                GObject.ParamFlags.READWRITE  # flags
                ),

        "statusbar-text": (str,
                           "Statusbar Text",
                           "The text to be displayed in the statusbar "
                           "at the bottom of the window when this page "
                           "is visible.",
                           None,
                           GObject.ParamFlags.READWRITE
                           ),
        "use-new-window": (bool,
                           "Use New Window",
                           "When True a new top level window will be "
                           "created to hold this page.",
                           False,
                           GObject.ParamFlags.READWRITE  # flags
                           ),
        "ui-description": (str,
                           "UI Description File",
                           "The filename containing the XML data that "
                           "describes this pages menus and toolbars.",
                           None,
                           GObject.ParamFlags.READWRITE
                           ),

        "ui-merge": (Gtk.UIManager,
                     "UI Merge",
                     "A pointer to the GtkUIManager object that "
                     "represents this pages menu hierarchy.",
                     GObject.ParamFlags.READABLE
                     ),
        "action-group": (Gio.SimpleActionGroup,
                         "UI Merge",
                         "A pointer to the GtkUIManager object that "
                         "represents this pages menu hierarchy.",
                         GObject.ParamFlags.READABLE  # flags
                         )

    }

    __gsignals__ = {
        'inserted': (GObject.SignalFlags.RUN_FIRST, None, (int,)),
        'removed': (GObject.SignalFlags.RUN_FIRST, None, (int,)),
        'selected': (GObject.SignalFlags.RUN_FIRST, None, (int,)),
        'unselected': (GObject.SignalFlags.RUN_FIRST, None, (int,))
    }
    widget = None

    def __init__(self):
        super().__init__()
        self.tab_icon = None
        self.summary_bar = None
        self.plugin_name = None
        self.window = None
        self.notebook_page = 0
        self.data = defaultdict(dict)
        self.action_group = None
        self.ui_merge = None
        self.merge_id = 0
        self.ui_description = ""
        self.books = weakref.WeakSet()
        self.use_new_window = False
        self.name = None
        self.long_name = None
        self.color = None
        self.uri = None
        self.focus_source_id = 0
        self.changed_id = 0
        self.statusbar_text = None
        Tracking.remember(self)

    def get_user_data(self, obj, key):
        return self.data.get(obj, {}).get(key, None)

    def set_user_data(self, obj, key, userdata):
        self.data[obj][key] = userdata

    def get_widget(self):
        return self.widget

    @abstractmethod
    def focus_widget(self):
        raise NotImplementedError

    @abstractmethod
    def create_widget(self):
        raise NotImplementedError

    @abstractmethod
    def destroy_widget(self):
        raise NotImplementedError

    def show_summary_bar(self, visible):
        if not self.summary_bar:
            return
        if visible:
            self.summary_bar.show()
        else:
            self.summary_bar.hide()

    @abstractmethod
    def save_page(self, key_file, group_name):
        raise NotImplementedError

    def finish_pending(self):
        return True

    def update_edit_menu_actions(self, *args, **kwargs):
        pass

    def name_changed(self, name):
        pass

    def window_changed(self, window):
        pass

    @classmethod
    def recreate_page(cls, window, key_file, group_name):
        raise NotImplementedError

    @classmethod
    def get_class_from_name(cls, name):
        for sub in cls.__subclasses__():
            if sub.__name__ == name:
                return sub
            else:
                sub.get_class_from_name(name)

    @classmethod
    def do_recreate_page(cls, window, type, key_file, group_name):
        klass = cls.get_class_from_name(type)
        if klass is None:
            return None
        try:
            page = klass.recreate_page(window, key_file, group_name)
        except NotImplementedError:
            page = None
        return page

    def merge_actions(self, ui_merge):
        self.ui_merge = ui_merge
        if self.action_group is not None:
            self.action_group.set_sensitive(True)
            self.merge_id = add_actions(self.ui_merge, self.action_group, self.ui_description)

    def unmerge_actions(self, ui_merge):
        if self.merge_id == 0 or self.action_group is None:
            return
        ui_merge.remove_ui(self.merge_id)
        self.action_group.set_sensitive(False)
        ui_merge.remove_action_group(self.action_group)
        self.ui_merge = None
        self.merge_id = 0

    def get_action(self, name):
        if name is None or self.action_group is None:
            return None
        return self.action_group.get_action(name)

    def get_plugin_name(self):
        return self.plugin_name

    def inserted(self):
        self.emit("inserted", 0)

    def removed(self):
        self.emit("removed", 0)

    def selected(self):
        self.emit("selected", 0)

    def unselected(self):
        self.emit("unselected", 0)

    def do_get_property(self, prop):
        if prop.name == 'name':
            return self.name
        elif prop.name == 'color':
            return self.color
        elif prop.name == 'uri':
            return self.uri
        elif prop.name == 'statusbar-text':
            return self.statusbar_text
        elif prop.name == 'use-new-window':
            return self.use_new_window
        elif prop.name == 'ui-description':
            return self.ui_description
        elif prop.name == 'ui-merge':
            return self.ui_merge
        elif prop.name == 'action-group':
            return self.action_group
        else:
            raise AttributeError('unknown property %s' % prop.name)

    def do_set_property(self, prop, value):
        if prop.name == 'name':
            self.set_name(value)
        elif prop.name == 'color':
            self.set_color(value)
        elif prop.name == 'uri':
            self.set_uri(value)
        elif prop.name == 'statusbar-text':
            self.set_set_statusbar_text(value)
        elif prop.name == 'use-new-window':
            self.set_use_new_window(value)
        elif prop.name == 'ui-description':
            self.set_ui_description(value)
        else:
            raise AttributeError('unknown property %s' % prop.name)

    def add_book(self, book):
        if book is not None:
            self.books.add(book)

    def has_book(self, book):
        return book in self.books

    def has_books(self):
        return bool(len(self.books))

    def get_window(self):
        return self.window

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name
        self.name_changed(name)

    def get_long_name(self):
        return self.long_name

    def set_long_name(self, name):
        self.long_name = name

    def get_color(self):
        return self.color

    def set_color(self, color):
        if color is not None:
            self.color = color

    def get_uri(self):
        return self.uri

    def set_uri(self, name):
        self.uri = name

    def get_statusbar_text(self):
        return self.statusbar_text

    def set_statusbar_text(self, message):
        self.statusbar_text = message

    def get_use_new_window(self):
        return self.use_new_window

    def set_use_new_window(self, use_new):
        self.use_new_window = use_new

    def get_ui_description(self):
        return self.ui_description

    def set_ui_description(self, ui_filename):
        self.ui_description = ui_filename

    def get_ui_merge(self):
        return self.ui_merge

    def get_action_group(self):
        return self.action_group

    def create_action_group(self, group_name):
        group = Gtk.ActionGroup.new(group_name)
        self.action_group = group
        return group

    def focus_idle_destroy(self, done):
        self.focus_source_id = 0

    def default_focus(self, on_current_page):
        if not on_current_page:
            return
        try:
            if self.focus_source_id > 0:
                GLib.source_remove(self.focus_source_id)
            self.focus_source_id = GLib.idle_add(lambda: self.focus_idle_destroy(self.focus_widget()),
                                                 priority=GLib.PRIORITY_DEFAULT_IDLE)
        except NotImplementedError:
            pass

    def main_window_changed(self, window, plugin_page):
        on_current_page = False
        if plugin_page is None or not isinstance(plugin_page, PluginPage):
            return
        if self == plugin_page:
            on_current_page = True
        self.default_focus(on_current_page)

    def inserted_cb(self, *args):
        self.changed_id = self.window.connect("page_changed", self.main_window_changed)
        self.default_focus(True)

    def disconnect_page_changed(self):
        if self.changed_id > 0:
            self.window.disconnect(self.changed_id)
        self.changed_id = 0

    def __del__(self):
        self.data = None
        self.books = None
        self.window = None
        Tracking.forget(self)


GObject.type_register(PluginPage)
