from gasstation.utilities.actions import update_actions, init_short_names, set_important_actions
from gasstation.utilities.cursors import gs_set_busy_cursor, gs_unset_busy_cursor
from gasstation.utilities.icons import *
from gasstation.views.register.ledger import *
from gasstation.views.register.register import RegisterWindow, STATE_SECTION_REG_PREFIX, KEY_PAGE_FILTER
from gasstation.views.reports.standard import StandardReports
from gasstation.views.selections import DateSelection
from libgasstation.core.scrub import *
from .page import PREF_SUMMARY_BAR_POSITION_TOP, PREF_SUMMARY_BAR_POSITION_BOTTOM, \
    PluginPage, action_toolbar_labels

DEFAULT_LINES_AMOUNT = 50
DEFAULT_FILTER_NUM_DAYS_GL = "30"
CUT_TRANSACTION_LABEL = "Cu_t Transaction"
COPY_TRANSACTION_LABEL = "_Copy Transaction"
PASTE_TRANSACTION_LABEL = "_Paste Transaction"
DUPLICATE_TRANSACTION_LABEL = "Dup_licate Transaction"
DELETE_TRANSACTION_LABEL = "_Delete Transaction"
ASSOCIATE_TRANSACTION_FILE_LABEL = "_Associate File with Transaction"
ASSOCIATE_TRANSACTION_LOCATION_LABEL = "_Associate Location with Transaction"
EXECASSOCIATED_TRANSACTION_LABEL = "_Open Associated File/Location"
JUMP_ASSOCIATED_INVOICE_LABEL = "Open Associated Invoice"
CUT_SPLIT_LABEL = "Cu_t Split"
COPY_SPLIT_LABEL = "_Copy Split"
PASTE_SPLIT_LABEL = "_Paste Split"
DUPLICATE_SPLIT_LABEL = "Dup_licate Split"
DELETE_SPLIT_LABEL = "_Delete Split"
CUT_TRANSACTION_TIP = "Cut the selected transaction into clipboard"
COPY_TRANSACTION_TIP = "Copy the selected transaction into clipboard"
PASTE_TRANSACTION_TIP = "Paste the transaction from the clipboard"
DUPLICATE_TRANSACTION_TIP = "Make a copy of the current transaction"
DELETE_TRANSACTION_TIP = "Delete the current transaction"
CUT_SPLIT_TIP = "Cut the selected split into clipboard"
COPY_SPLIT_TIP = "Copy the selected split into clipboard"
PASTE_SPLIT_TIP = "Paste the split from the clipboard"
DUPLICATE_SPLIT_TIP = "Make a copy of the current split"
DELETE_SPLIT_TIP = "Delete the current split"

ASSOCIATE_TRANSACTION_FILE_TIP = "Associate a file with the current transaction"
ASSOCIATE_TRANSACTION_LOCATION_TIP = "Associate a location with the current transaction"
EXECASSOCIATED_TRANSACTION_TIP = "Open the associated file or location with the current transaction"
JUMP_ASSOCIATED_INVOICE_TIP = "Open the associated invoice"

TRANSACTION_UP_ACTION = "TransactionUpAction"
TRANSACTION_DOWN_ACTION = "TransactionDownAction"
KEY_REGISTER_TYPE = "RegisterType"
KEY_ACCOUNT_NAME = "AccountName"
KEY_REGISTER_STYLE = "RegisterStyle"
KEY_DOUBLE_LINE = "DoubleLineMode"
KEY_EXTRA_DATES = "ExtraDatesMode"

LABEL_ACCOUNT = "Account"
LABEL_SUBACCOUNT = "SubAccount"
LABEL_GL = "GL"
LABEL_SEARCH = "Search"
PLUGIN_PAGE_REGISTER_NAME = "RegisterPluginPage"
SPLIT_REGISTER_GUID = "SplitRegister GUID"

style_names = ["Ledger",
               "Auto Ledger",
               "Journal"]

important_actions = ["SplitTransactionAction"]
actions_requiring_account = ["EditEditAccountAction",
                             "ActionsReconcileAction",
                             "ActionsAutoClearAction",
                             "ActionsLotsAction"]
view_style_actions = ["ViewStyleBasicAction",
                      "ViewStyleAutoSplitAction",
                      "ViewStyleJournalAction"]

radio_entries = [("ViewStyleBasicAction", None, "_Basic Ledger",
                  None, "Show transactions on one or two lines",
                  RegisterModelStyle.LEDGER),
                 ("ViewStyleAutoSplitAction", None,
                  "_Auto-Split Ledger", None,
                  "Show transactions on one or two lines and"
                  " expand the current transaction",
                  RegisterModelStyle.AUTO_LEDGER),
                 ("ViewStyleJournalAction", None,
                  "Transaction _Journal", None,
                  "Show expanded transactions with all splits",
                  RegisterModelStyle.JOURNAL)]
radio_action_struct = []
for acc, label, name, stock, tip, value in radio_entries:
    g = Gtk.RadioActionEntry()
    g.accelerator = acc
    g.label = label
    g.name = name
    g.stock_id = stock
    g.tooltip = tip
    g.value = value
    radio_action_struct.append(g)

toolbar_labels = [action_toolbar_labels(x, y) for x, y in [("ActionsTransferAction", "Transfer"),
                                                           ("RecordTransactionAction", "Enter"),
                                                           ("CancelTransactionAction", "Cancel"),
                                                           ("DeleteTransactionAction", "Delete"),
                                                           ("DuplicateTransactionAction", "Duplicate"),
                                                           ("SplitTransactionAction", "Split"),
                                                           ("ScheduleTransactionAction", "Schedule"),
                                                           ("BlankTransactionAction", "Blank"),
                                                           ("ActionsReconcileAction", "Reconcile"),
                                                           ("ActionsAutoClearAction", "Auto-clear"),
                                                           (TRANSACTION_UP_ACTION, "Up"),
                                                           (TRANSACTION_DOWN_ACTION, "Down"),
                                                           ("AssociateTransactionFileAction", "File"),
                                                           ("AssociateTransactionLocationAction", "Location"),
                                                           ("ExecAssociatedTransactionAction", "Open")
                                                           ]]


class Cleared(GObject.GEnum):
    NONE = 0x0000
    NO = 0x0001
    CLEARED = 0x0002
    RECONCILED = 0x0004
    FROZEN = 0x0008
    VOIDED = 0x0010
    ALL = 0x001F


class StatusAction:
    action_name = None
    value = None
    widget = None

    def __init__(self, a, b, c):
        self.action_name = a
        self.value = b
        self.widget = c


status_actions = [StatusAction(x, y, z) for x, y, z in
                  [("filter_status_reconciled", Cleared.RECONCILED, None),
                   ("filter_status_cleared", Cleared.CLEARED, None),
                   ("filter_status_voided", Cleared.VOIDED, None),
                   ("filter_status_frozen", Cleared.FROZEN, None),
                   ("filter_status_unreconciled", Cleared.NO, None)]]

ClEARED_VALUE = "cleared_value"
DEFAULT_FILTER = "0x001f"

readonly_inactive_actions = ["EditCutAction",
                             "EditPasteAction",
                             "CutTransactionAction",
                             "PasteTransactionAction",
                             TRANSACTION_UP_ACTION,
                             TRANSACTION_DOWN_ACTION,
                             "DuplicateTransactionAction",
                             "DeleteTransactionAction",
                             "RemoveTransactionSplitsAction",
                             "RecordTransactionAction",
                             "CancelTransactionAction",
                             "UnvoidTransactionAction",
                             "VoidTransactionAction",
                             "ReverseTransactionAction",
                             "ActionsTransferAction",
                             "ActionsReconcileAction",
                             "ActionsStockSplitAction",
                             "ScheduleTransactionAction",
                             "ScrubAllAction",
                             "ScrubCurrentAction"
                             "AssociateTransactionFileAction",
                             "AssociateTransactionLocationAction"]

tran_vs_split_actions = ("CutTransactionAction",
                         "CopyTransactionAction",
                         "PasteTransactionAction",
                         "DuplicateTransactionAction",
                         "DeleteTransactionAction")

tran_action_labels = (CUT_TRANSACTION_LABEL,
                      COPY_TRANSACTION_LABEL,
                      PASTE_TRANSACTION_LABEL,
                      DUPLICATE_TRANSACTION_LABEL,
                      DELETE_TRANSACTION_LABEL,
                      ASSOCIATE_TRANSACTION_FILE_LABEL,
                      ASSOCIATE_TRANSACTION_LOCATION_LABEL,
                      EXECASSOCIATED_TRANSACTION_LABEL)

tran_action_tips = (CUT_TRANSACTION_TIP,
                    COPY_TRANSACTION_TIP,
                    PASTE_TRANSACTION_TIP,
                    DUPLICATE_TRANSACTION_TIP,
                    DELETE_TRANSACTION_TIP,
                    ASSOCIATE_TRANSACTION_FILE_TIP,
                    ASSOCIATE_TRANSACTION_LOCATION_TIP,
                    EXECASSOCIATED_TRANSACTION_TIP)

split_action_labels = (CUT_SPLIT_LABEL,
                       COPY_SPLIT_LABEL,
                       PASTE_SPLIT_LABEL,
                       DUPLICATE_SPLIT_LABEL,
                       DELETE_SPLIT_LABEL)

split_action_tips = (CUT_SPLIT_TIP,
                     COPY_SPLIT_TIP,
                     PASTE_SPLIT_TIP,
                     DUPLICATE_SPLIT_TIP,
                     DELETE_SPLIT_TIP)


class RegisterPluginPage(PluginPage):
    tab_icon = ICON_ACCOUNT
    plugin_name = "RegisterPluginPage"
    __gtype_name__ = "RegisterPluginPage"

    class Fd:
        def __init__(self):
            self.dialog = None
            self.table = None
            self.start_date_choose = None
            self.start_date_today = None
            self.start_date = None
            self.start_time = None
            self.end_date_choose = None
            self.end_date_today = None
            self.end_date = None
            self.original_cleared_match = ClearedMatch.NONE
            self.cleared_match = ClearedMatch.NONE
            self.original_start_time = None
            self.original_end_time = None
            self.end_time = None
            self.original_save_filter = False
            self.save_filter = False

    def __init__(self):
        super().__init__()
        use_new = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_USE_NEW)
        self.add_book(Session.get_current_book())
        self.reference_ids = {}
        self.set_name("General Journal2")
        self.set_uri("default:")
        self.set_ui_description("plugin_page/register2-ui.xml")
        self.set_use_new_window(use_new)
        action_group = self.create_action_group("PluginPageRegister2Actions")
        for name, stock_id, label, accelerator, tooltip, callback in [(
                "FilePrintAction", "document-print", "_Print Checks...", "<primary>p", None, self.cmd_print_check),
            (
                    "EditCutAction", "edit-cut", "Cu_t", "<primary>X",
                    "Cut the current selection and copy it to clipboard",
                    self.cmd_cut
            ),
            (
                    "EditCopyAction", "edit-copy", "_Copy", "<primary>C",
                    "Copy the current selection to clipboard",
                    self.cmd_copy
            ),
            (
                    "EditPasteAction", "edit-paste", "_Paste", "<primary>V",
                    "Paste the clipboard content at the cursor position",
                    self.cmd_paste
            ),
            (
                    "EditEditAccountAction", ICON_EDIT_ACCOUNT, "Edit _Account", "<primary>e",
                    "Edit the selected account",
                    self.cmd_edit_account
            ),
            (
                    "EditFindAccountAction", "edit-find", "F_ind Account", "<primary>i",
                    "Find an account",
                    self.cmd_find_account
            ),
            (
                    "EditFindTransactionsAction", "edit-find", "_Find...", "<primary>f",
                    "Find transactions with a search",
                    self.cmd_find_transactions
            ),

            (
                    "CutTransactionAction", "edit-cut", CUT_TRANSACTION_LABEL, "",
                    CUT_TRANSACTION_TIP,
                    self.cmd_cut_transaction
            ),
            (
                    "CopyTransactionAction", "edit-copy", COPY_TRANSACTION_LABEL, "",
                    COPY_TRANSACTION_TIP,
                    self.cmd_copy_transaction
            ),
            (
                    "PasteTransactionAction", "edit-paste", PASTE_TRANSACTION_LABEL, "",
                    PASTE_TRANSACTION_TIP,
                    self.cmd_paste_transaction
            ),
            (
                    "DuplicateTransactionAction", "edit-copy", DUPLICATE_TRANSACTION_LABEL, "<primary>d",
                    DUPLICATE_TRANSACTION_TIP,
                    self.cmd_duplicate_transaction
            ),
            (
                    "DeleteTransactionAction", "edit-delete", DELETE_TRANSACTION_LABEL, "<primary>y",
                    DELETE_TRANSACTION_TIP,
                    self.cmd_delete_transaction
            ),
            (
                    "RemoveTransactionSplitsAction", "edit-clear", "Remo_ve All Splits", None,
                    "Remove all splits in the current transaction",
                    self.cmd_reinitialize_transaction
            ),
            (
                    "RecordTransactionAction", "list-add", "_Enter Transaction", None,
                    "Record the current transaction",
                    self.cmd_enter_transaction
            ),
            (
                    "CancelTransactionAction", "process-stop", "Ca_ncel Transaction", "<primary>z",
                    "Cancel the current transaction",
                    self.cmd_cancel_transaction
            ),
            (
                    "VoidTransactionAction", None, "_Void Transaction", None, None,
                    self.cmd_void_transaction
            ),
            (
                    "UnvoidTransactionAction", None, "_Unvoid Transaction", None, None,
                    self.cmd_unvoid_transaction
            ),
            (
                    "ReverseTransactionAction", None, "Add _Reversing Transaction", "<primary>r", None,
                    self.cmd_reverse_transaction
            ),
            (
                    "JumpAssociatedInvoiceAction", None, JUMP_ASSOCIATED_INVOICE_LABEL, None,
                    JUMP_ASSOCIATED_INVOICE_TIP,
                    self.cmd_jump_associated_invoice
            ),
            (
                    TRANSACTION_UP_ACTION, "go-up", "Move Transaction _Up", None,
                    "Move the current transaction one row upwards. Only available if the date and number of both rows "
                    "are identical and the register window is sorted by date.",
                    self.cmd_entryUp
            ),
            (
                    TRANSACTION_DOWN_ACTION, "go-down", "Move Transaction Do_wn", None,
                    "Move the current transaction one row downwards. Only available if the date and number of both "
                    "rows are identical and the register window is sorted by date.",
                    self.cmd_entryDown
            ),
            (
                    "ViewFilterByAction", None, "_Filter By...", None, None,
                    self.cmd_view_filter_by
            ),
            (
                    "ViewRefreshAction", "view-refresh", "_Refresh", None,
                    "Refresh this window",
                    self.cmd_reload
            ),

            (
                    "ActionsTransferAction", ICON_TRANSFER, "_Transfer...", "<primary>t",
                    "Transfer funds from one account to another",
                    self.cmd_transfer
            ),
            (
                    "ActionsReconcileAction", "edit-select-all", "_Reconcile...", None,
                    "Reconcile the selected account",
                    self.cmd_reconcile
            ),
            (
                    "ActionsAutoClearAction", "edit-select-all", "_Auto-clear...", None,
                    "Automatically clear individual transactions, so as to reach a certain cleared amount",
                    self.cmd_autoclear
            ),
            (
                    "ActionsStockSplitAction", None, "Stoc_k Split...", None,
                    "Record a stock split or a stock merger",
                    self.cmd_stock_split
            ),
            (
                    "ActionsLotsAction", None, "View _Lots...", None,
                    "Bring up the lot viewer/editor window",
                    self.cmd_lots
            ),
            (
                    "BlankTransactionAction", "go-bottom", "_Blank Transaction", "<primary>Page_Down",
                    "Move to the blank transaction at the bottom of the register",
                    self.cmd_blank_transaction
            ),
            (
                    "EditExchangeRateAction", None, "Edit E_xchange Rate", None,
                    "Edit the exchange rate for the current transaction",
                    self.cmd_exchange_rate
            ),
            (
                    "JumpTransactionAction", ICON_JUMP_TO, "_Jump", "<primary>j",
                    "Jump to the corresponding transaction in the other account",
                    self.cmd_jump
            ),
            (
                    "ScheduleTransactionAction", "appointment-new", "Sche_dule...", None,
                    "Create a Scheduled Transaction with the current transaction as a template",
                    self.cmd_schedule
            ),
            (
                    "ScrubAllAction", None, "_All transactions", None, None,
                    self.cmd_scrub_all
            ),
            (
                    "ScrubCurrentAction", None, "_This transaction", None, None,
                    self.cmd_scrub_current
            ),

            (
                    "ReportsAccountReportAction", None, "Account Report", None,
                    "Open a register report for this Account",
                    self.cmd_account_report
            ),
            (
                    "ReportsAcctTransReportAction", None, "Account Report - Single Transaction", None,
                    "Open a register report for the selected Transaction",
                    self.cmd_transaction_report
            )]:
            action = Gtk.Action(name, label, tooltip, None)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                self.reference_ids[action] = action.connect('activate', callback)
            action_group.add_action_with_accel(action, accelerator)

        for name, stock_id, label, accelerator, tooltip, callback, is_active in [("ViewStyleDoubleLineAction", None,
                                                                                  "_Double Line", None,
                                                                                  "Show two lines of information for "
                                                                                  "each transaction",
                                                                                  self.cmd_style_double_line, False),
                                                                                 (
                                                                                         "ViewStyleExtraDatesAction",
                                                                                         None,
                                                                                         "Show _Extra Dates", None,
                                                                                         "Show entered and reconciled "
                                                                                         "dates",
                                                                                         self.cmd_style_extra_dates,
                                                                                         False),
                                                                                 (
                                                                                         "SplitTransactionAction",
                                                                                         ICON_SPLIT_TRANS,
                                                                                         "S_plit Transaction", None,
                                                                                         "Show all splits in the "
                                                                                         "current transaction",
                                                                                         self.cmd_expand_transaction,
                                                                                         False)]:

            action = Gtk.ToggleAction(name, label, tooltip, None)
            action.set_active(is_active)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                self.reference_ids[action] = action.connect('toggled', callback)
            action_group.add_action_with_accel(action, accelerator)
        init_short_names(action_group, toolbar_labels)
        action_group.add_radio_actions(entries=radio_entries, on_change=self.cmd_style_changed)
        set_important_actions(action_group, important_actions)
        self.lines_default = DEFAULT_LINES_AMOUNT
        self.read_only = False
        self.fd = self.Fd()
        self.fd.cleared_match = Cleared.ALL
        self.widget = None

    def get_current_transaction(self):
        return self.ledger.get_view().get_current_trans()

    def destroy_widget(self):
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_SUMMARY_BAR_POSITION_TOP, self.summary_bar_position_changed)
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_SUMMARY_BAR_POSITION_BOTTOM, self.summary_bar_position_changed)
        self.disconnect_page_changed()
        if self.focus_source_id > 0:
            GLib.source_remove(self.focus_source_id)
        self.gsr.disconnect(self.help_id)
        view = self.gsr.get_register()
        view.disconnect(self.button_id)
        if self.ledger is not None:
            self.ledger.close()
            self.ledger = None
        self.gsr.disconnect_all()
        if self.widget is not None:
            self.widget.destroy()
            self.widget = None
        self.gsr = None
        if self.component_manager_id > 0:
            ComponentManager.unregister(self.component_manager_id)
            self.component_manager_id = 0
        if self.event_handler_id:
            Event.unregister_handler(self.event_handler_id)
            self.event_handler_id = 0

        if self.fd.dialog is not None:
            self.fd.dialog.destroy()
        self.fd = None
        for action in self.action_group.list_actions():
            f = False
            name = action.get_name()
            for ra in radio_entries:
                if name == ra[0]:
                    f = True
                    try:
                        action.disconnect_by_func(self.cmd_style_changed)
                    except TypeError:
                        continue
            if not f:
                action.disconnect(self.reference_ids[action])
            self.action_group.remove_action(action)
        self.action_group = None
        self.reference_ids = None
        self.books.clear()
        self.data.clear()

    @classmethod
    def new_common(cls, ledger):
        gsr = ledger.get_user_data()
        if gsr is not None:
            items = Tracking.get_list(PLUGIN_PAGE_REGISTER_NAME)
            for register_page in items:
                if gsr == register_page.gsr:
                    return register_page
        assert ledger is not None
        register_page = cls()
        register_page.ledger = ledger
        register_page.key = ""
        lb = register_page.get_tab_name()
        register_page.set_name(lb)
        label_color = register_page.get_tab_color()
        register_page.set_color(label_color)
        return register_page

    @classmethod
    def new(cls, account, subaccounts):
        items = Tracking.get_list(PLUGIN_PAGE_REGISTER_NAME)
        for new_register_page in items:
            new_account = new_register_page.get_account()
            if new_account is not None and new_account.guid == account.guid:
                return new_register_page
        if subaccounts:
            ledger = RegisterLedger.subaccounts(account)
        else:
            ledger = RegisterLedger.simple(account)
        page = cls.new_common(ledger)
        page.key = account.guid
        return page

    @classmethod
    def new_gl(cls):
        ledger = RegisterLedger.gl()
        return cls.new_common(ledger)

    @classmethod
    def new_ledger(cls, ledger):
        return cls.new_common(ledger)

    def get_account(self):
        if self.ledger is None:
            return
        leader = self.ledger.get_leader()
        ledger_type = self.ledger.get_type()
        if ledger_type == RegisterLedgerType.SINGLE or ledger_type == RegisterLedgerType.SUB_ACCOUNT:
            return leader
        return None

    def ui_update(self):
        model = self.ledger.get_model()
        view = self.ledger.get_view()
        if model is None or view is None:
            return
        expanded = view.trans_expanded(None)
        action = self.get_action("SplitTransactionAction")
        action.set_sensitive(model.style == RegisterModelStyle.LEDGER)
        action.handler_block_by_func(self.cmd_expand_transaction)
        action.set_active(expanded)
        action.handler_unblock_by_func(self.cmd_expand_transaction)
        trans = view.get_current_trans()
        if trans is not None:
            voided = trans.has_split_in_state(SplitState.VOIDED)
            action = self.get_action("VoidTransactionAction")
            action.set_sensitive(not voided)
            action = self.get_action("UnvoidTransactionAction")
            action.set_sensitive(voided)
        action = self.get_action(TRANSACTION_UP_ACTION)
        action.set_sensitive(view.is_current_movable_updown(True))
        action = self.get_action(TRANSACTION_DOWN_ACTION)
        action.set_sensitive(view.is_current_movable_updown(False))

        if Session.get_current_book().is_readonly():
            for a in readonly_inactive_actions:
                action = self.get_action(a)
                action.set_sensitive(False)
        curr_label_trans = False
        depth = view.get_selected_row_depth()
        _iter = tran_vs_split_actions
        action = self.get_action(_iter[0])
        label_iter = tran_action_labels
        if action.get_label() == label_iter:
            curr_label_trans = True
        if depth == RowDepth.SPLIT3 and curr_label_trans:
            label_iter = split_action_labels
            tooltip_iter = split_action_tips
            for i, x in enumerate(tran_vs_split_actions):
                action = self.get_action(x)
                action.set_label(label_iter[i])
                action.set_tooltip(tooltip_iter[i])

        elif (depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2) and not curr_label_trans:
            label_iter = tran_action_labels
            tooltip_iter = tran_action_tips
            for i, x in enumerate(tran_vs_split_actions):
                action = self.get_action(x)
                action.set_label(label_iter[i])
                action.set_tooltip(tooltip_iter[i])

    def ui_initial_state(self):
        model = self.ledger.get_model()
        is_readwrite = not Session.get_current_book().is_readonly()
        account = self.get_account()
        action_group = self.get_action_group()
        update_actions(action_group, actions_requiring_account, "sensitive", is_readwrite and account is not None)
        ledger_type = self.ledger.get_type()
        update_actions(action_group, view_style_actions, "sensitive", ledger_type == RegisterLedgerType.SINGLE)
        i = 0
        for p in radio_action_struct:
            if p.value == model.style:
                break
            i += 1
        action = action_group.get_action(radio_action_struct[i].accelerator)
        try:
            action.handler_block_by_func(self.cmd_style_changed)
            action.set_active(True)
            action.handler_unblock_by_func(self.cmd_style_changed)
        except TypeError:
            action.set_active(True)
        view = self.gsr.get_register()

        action = action_group.get_action("ViewStyleDoubleLineAction")
        action.handler_block_by_func(self.cmd_style_double_line)
        action.set_active(model.use_double_line)
        action.handler_unblock_by_func(self.cmd_style_double_line)

        action = action_group.get_action("ViewStyleExtraDatesAction")
        action.handler_block_by_func(self.cmd_style_extra_dates)
        action.set_active(view.show_extra_dates)
        action.handler_unblock_by_func(self.cmd_style_extra_dates)

    def window_changed(self, window):
        self.gsr.window = window.get_gtk_window(window)

    def save_page(self, key_file, group_name):
        view = self.ledger.get_view()
        model = self.ledger.get_model()
        ledger_type = self.ledger.get_type()
        if ledger_type > RegisterLedgerType.GL:
            return
        if ledger_type == RegisterLedgerType.SINGLE or ledger_type == RegisterLedgerType.SUB_ACCOUNT:
            label = LABEL_ACCOUNT if ledger_type == RegisterLedgerType.SINGLE else LABEL_SUBACCOUNT
            leader = self.ledger.get_leader()
            key_file.set_string(group_name, KEY_REGISTER_TYPE, label)
            name = leader.get_full_name()
            key_file.set_string(group_name, KEY_ACCOUNT_NAME, name)

        elif model.type == RegisterModelType.GENERAL_JOURNAL:
            key_file.set_string(group_name, KEY_REGISTER_TYPE, LABEL_GL)
        elif model.type == RegisterModelType.SEARCH_LEDGER:
            key_file.set_string(group_name, KEY_REGISTER_TYPE, LABEL_SEARCH)
        else:
            return
        key_file.set_string(group_name, KEY_REGISTER_STYLE, style_names[model.style])
        key_file.set_boolean(group_name, KEY_DOUBLE_LINE, model.use_double_line)
        key_file.set_boolean(group_name, KEY_EXTRA_DATES, view.show_extra_dates)

    def restore_edit_menu(self, key_file, group_name):
        style_name = key_file.get_string(group_name, KEY_REGISTER_STYLE)
        i = 0
        for i, _name in enumerate(style_names):
            if style_name == _name:
                break
        if i <= RegisterModelStyle.JOURNAL:
            action = self.get_action(radio_action_struct[i].accelerator)
            action.set_active(True)
        use_double_line = key_file.get_boolean(group_name, KEY_DOUBLE_LINE)
        action = self.get_action("ViewStyleDoubleLineAction")
        action.set_active(use_double_line)
        show_extra_dates = key_file.get_boolean(group_name, KEY_EXTRA_DATES)
        action = self.get_action("ViewStyleExtraDatesAction")
        action.set_active(show_extra_dates)

    @classmethod
    def recreate_page(cls, window, key_file, group_name):
        reg_type = key_file.get_string(group_name, KEY_REGISTER_TYPE)
        if GLib.ascii_strcasecmp(reg_type, LABEL_ACCOUNT) == 0 or GLib.ascii_strcasecmp(reg_type,
                                                                                        LABEL_SUBACCOUNT) == 0:
            include_subs = (GLib.ascii_strcasecmp(reg_type, LABEL_SUBACCOUNT) == 0)
            acct_name = key_file.get_string(group_name, KEY_ACCOUNT_NAME)
            root = Session.get_current_root()
            account = root.lookup_by_full_name(acct_name)
            if account is None:
                return None
            page = cls.new(account, include_subs)
        elif GLib.ascii_strcasecmp(reg_type, LABEL_GL) == 0:
            page = cls.new_gl()
        else:
            return None
        page.set_use_new_window(False)
        window.open_page(page)
        page.restore_edit_menu(key_file, group_name)
        return page

    def update_edit_menu(self, hide):
        is_readwrite = not Session.get_current_root().is_root()
        view = self.ledger.get_view()
        if view.editing_now:
            has_selection = True
        else:
            has_selection = False
        can_copy = has_selection
        can_cut = is_readwrite and has_selection
        can_paste = is_readwrite

        action = self.get_action("EditCopyAction")
        action.set_sensitive(can_copy)
        action.set_visible(not hide or can_copy)
        action = self.get_action("EditCutAction")
        action.set_sensitive(can_cut)
        action.set_visible(not hide or can_cut)
        action = self.get_action("EditPasteAction")
        action.set_sensitive(can_paste)
        action.set_visible(not hide or can_paste)

    def finish_pending(self):
        view = self.ledger.get_view()
        view.finish_edit()
        if view is None or (view.get_dirty_trans() is None):
            return True
        tab_name = self.get_tab_name()
        window = self.get_window()
        dialog = Gtk.MessageDialog(transient_for=window, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.WARNING,
                                   buttons=Gtk.ButtonsType.NONE, text="Save changes to %s?" % tab_name)

        dialog.format_secondary_text("This register has pending changes to a transaction. "
                                     "Would you like to save the changes to this transaction, "
                                     "discard the transaction, or cancel the operation?")
        gs_dialog_add_button(dialog, "_Discard Transaction", "edit-delete", Gtk.ResponseType.REJECT)
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        gs_dialog_add_button(dialog, "_Save Transaction", "document-save", Gtk.ResponseType.ACCEPT)
        response = dialog.run()
        dialog.destroy()

        if response == Gtk.ResponseType.ACCEPT:
            t = view.save(True)
            return t

        elif response == Gtk.ResponseType.REJECT:
            view.cancel_edit(True)
            return True
        else:
            return False

    def get_tab_name(self):
        ledger = self.get_ledger()
        model = ledger.get_model()
        ledger_type = ledger.get_type()
        leader = ledger.get_leader()

        if ledger_type == RegisterLedgerType.SINGLE:
            return leader.get_name()

        elif ledger_type == RegisterLedgerType.SUB_ACCOUNT:
            return "%s+" % leader.get_name()

        elif ledger_type == RegisterLedgerType.GL:
            if model.type == RegisterModelType.GENERAL_JOURNAL or model.type == RegisterModelType.INCOME_LEDGER:
                return "General Journal"
            elif model.type == RegisterModelType.PORTFOLIO_LEDGER:
                return "Portfolio"
            elif model.type == RegisterModelType.SEARCH_LEDGER:
                return "Search Results"

        return "unknown"

    def get_tab_color(self):
        ledger_type = self.ledger.get_type()
        leader = self.ledger.get_leader()
        color = None
        if ledger_type == RegisterLedgerType.SINGLE or ledger_type == RegisterLedgerType.SUB_ACCOUNT:
            color = leader.get_color()
        return color

    @staticmethod
    def get_filter_default_num_of_days(ledger_type):
        return DEFAULT_FILTER_NUM_DAYS_GL if ledger_type == RegisterLedgerType.GL else "0"

    @staticmethod
    def get_filter_gcm(leader):
        if leader is None: return
        state_file = State.get_current()
        acct_guid = str(leader.get_guid())
        state_section = STATE_SECTION_REG_PREFIX + " " + acct_guid
        try:
            f = state_file.get_string(state_section, KEY_PAGE_FILTER)
        except GLib.GError:
            return
        return f.replace(";", ',')

    def get_filter(self):
        ledger_type = self.ledger.get_type()
        leader = self.ledger.get_leader()

        f = self.get_filter_gcm(leader)
        if f is not None:
            return f

        return "%s,%s,%s,%s" % (DEFAULT_FILTER,
                                "0", "0", self.get_filter_default_num_of_days(ledger_type))

    @staticmethod
    def set_filter_gcm(leader, f, default_filter):
        if leader is None: return
        state_file = State.get_current()
        acct_guid = str(leader.get_guid())
        state_section = STATE_SECTION_REG_PREFIX + " " + acct_guid
        if f is None or f == default_filter:
            if state_file.has_key(state_section, KEY_PAGE_FILTER):
                state_file.remove_key(state_section, KEY_PAGE_FILTER)
            state_file.check_for_empty_group(state_file, state_section)
        else:
            state_file.set_string(state_section, KEY_PAGE_FILTER, f.replace(",", ";"))

    def set_filter(self, f):
        ledger_type = self.ledger.get_type()
        leader = self.ledger.get_leader()

        default_filter = "%s,%s,%s,%s" % (DEFAULT_FILTER,
                                          "0", "0", self.get_filter_default_num_of_days(ledger_type))
        self.set_filter_gcm(leader, f, default_filter)

    def get_long_name(self):
        if self.ledger is None:
            return ""
        ledger_type = self.ledger.get_type()
        leader = self.ledger.get_leader()
        if ledger_type == RegisterLedgerType.SINGLE:
            return leader.get_full_name()
        elif ledger_type == RegisterLedgerType.SUB_ACCOUNT:
            account_full_name = leader.get_full_name()
            return_string = "%s+" % account_full_name
            return return_string
        else:
            return None

    def summary_bar_position_changed(self, *_):
        position = Gtk.PositionType.BOTTOM
        if gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_SUMMARY_BAR_POSITION_TOP):
            position = Gtk.PositionType.TOP
        if self.widget is not None:
            self.widget.reorder_child(self.summary_bar, (0 if position == Gtk.PositionType.TOP else -1))

    def get_query(self):
        return self.ledger.get_query()

    def update_status_query(self, refresh_page):
        query = self.ledger.get_query()
        if query is None:
            return
        param_list = [SPLIT_RECONCILE]
        query.purge_terms(param_list)
        if self.fd.cleared_match != Cleared.ALL:
            query.add_cleared_match(self.fd.cleared_match, QueryOp.AND)
        if refresh_page:
            GLib.idle_add(self.ledger.refresh)

    def update_date_query(self, refresh_page):
        if not self.ledger:
            return
        query = self.ledger.get_query()
        if query is None:
            return
        param_list = [SPLIT_TRANS, TRANS_DATE_POSTED]
        query.purge_terms(param_list)
        if self.fd.start_time or self.fd.end_time:
            query.add_date_match_tt(
                self.fd.start_time != 0, self.fd.start_time,
                self.fd.end_time != 0, self.fd.end_time,
                QueryOp.AND)
        if refresh_page:
            GLib.idle_add(self.ledger.refresh)

    @staticmethod
    def filter_time2dmy(raw_time):
        timeinfo = time.localtime(raw_time)
        date_string = time.strftime("%d-%m-%Y", timeinfo)
        return date_string

    @staticmethod
    def filter_dmy2time(date_string):
        return time.mktime(time.strptime(date_string, "%d-%m-%Y"))

    def filter_status_one_cb(self, button):
        n = Gtk.Buildable.get_name(button)
        v = Cleared.NONE
        for p in status_actions:
            if n == p.action_name:
                v = p.value
                break
        if button.get_active():
            self.fd.cleared_match |= v
        else:
            self.fd.cleared_match &= ~v
        self.update_status_query(True)

    def filter_status_all_cb(self, button):
        for p in status_actions:
            widget = p.widget
            widget.handler_block_by_func(self.filter_status_one_cb)
            widget.set_active(True)
            widget.handler_unblock_by_func(self.filter_status_one_cb)
        self.fd.cleared_match = Cleared.ALL
        self.update_status_query(True)

    def get_filter_times(self):
        if self.fd.start_date_choose.get_active():
            time_val = self.fd.start_date.get_date()
            self.fd.start_time = time_val
        else:
            if self.fd.start_date_today.get_active():
                self.fd.start_time = datetime.date.today()
            else:
                self.fd.start_time = 0

        if self.fd.end_date_choose.get_active():
            time_val = self.fd.end_date.get_date()
            self.fd.end_time = time_val
        else:
            if self.fd.start_date_today.get_active():
                self.fd.end_time = datetime.date.today()
            else:
                self.fd.end_time = 0

    def filter_select_range_cb(self, button):
        active = button.get_active()
        self.fd.table.set_sensitive(active)
        if active:
            self.get_filter_times()
        else:
            self.fd.start_time = 0
            self.fd.end_time = 0

        self.update_date_query(True)

    def filter_gde_changed_cb(self, *unused):
        self.get_filter_times()
        self.update_date_query(True)

    def filter_start_cb(self, radio):
        if not radio.get_active():
            return
        active = Gtk.Buildable.get_name(radio) == "start_date_choose"
        self.fd.start_date.set_sensitive(active)
        self.get_filter_times()
        self.update_date_query(True)

    def filter_end_cb(self, radio):
        if not radio.get_active():
            return
        active = Gtk.Buildable.get_name(radio) == "end_date_choose"
        self.fd.end_date.set_sensitive(active)
        self.get_filter_times()
        self.update_date_query(True)

    def filter_save_cb(self, button):
        if button.get_active():
            self.fd.save_filter = True
        else:
            self.fd.save_filter = False

    def filter_response_cb(self, dialog, response):
        if response != Gtk.ResponseType.OK:
            self.fd.cleared_match = self.fd.original_cleared_match
            self.update_status_query(False)
            self.fd.start_time = self.fd.original_start_time
            self.fd.end_time = self.fd.original_end_time
            self.fd.save_filter = self.fd.original_save_filter
            self.update_date_query(False)
            GLib.idle_add(self.ledger.refresh)
        else:
            self.fd.original_save_filter = self.fd.save_filter
            if self.fd.save_filter:
                fil = "0x%04x" % self.fd.cleared_match
                if self.fd.start_date_choose.get_active() and self.fd.start_time != 0:
                    timeval = self.filter_time2dmy(self.fd.start_time)
                    fil = ",".join((fil, str(timeval)))
                else:
                    fil = ",".join([fil, "0"])

                if self.fd.end_date_choose.get_active() and self.fd.end_time != 0:
                    timeval = self.filter_time2dmy(self.fd.end_time)
                    fil = ",".join([fil, str(timeval)])
                else:
                    fil = ",".join([fil, "0"])
                self.set_filter(fil)
        self.fd.dialog = None
        dialog.destroy()

    @staticmethod
    def get_name(ledger, for_window):
        if ledger is None:
            return None
        model = ledger.get_model()
        ledger_type = ledger.get_type()
        if model.type == RegisterModelType.GENERAL_JOURNAL or model.type == RegisterModelType.INCOME_LEDGER:
            if for_window:
                reg_name = "General Journal"
            else:
                reg_name = "General Journal Report"

        elif model.type == RegisterModelType.PORTFOLIO_LEDGER:
            if for_window:
                reg_name = "Portfolio"
            else:
                reg_name = "Portfolio Report"

        elif model.type == RegisterModelType.SEARCH_LEDGER:
            if for_window:
                reg_name = "Search Results"
            else:
                reg_name = "Search Results Report"

        else:
            if for_window:
                reg_name = "Register"
            else:
                reg_name = "Register Report"

        leader = ledger.get_leader()
        if leader is not None and ledger_type != RegisterLedgerType.GL:
            account_name = leader.get_full_name()

            if ledger_type == RegisterLedgerType.SINGLE:
                name = account_name + " - " + reg_name
            else:
                name = account_name + " and subaccounts - " + reg_name
        else:
            name = reg_name
        return name

    def cmd_print_check(self, action):
        view = self.ledger.get_view()
        model = self.ledger.get_model()
        ledger_type = self.ledger.get_type()
        window = self.get_window()
        if ledger_type == RegisterLedgerType.SINGLE or ledger_type == RegisterLedgerType.SUB_ACCOUNT:
            account = self.get_account()
        split = view.get_current_split()
        trans = split.get_transaction()

        if trans is None:
            return

        if trans == view.get_blank_trans():
            return

        if view.trans_test_for_edit(trans):
            return

        if view.trans_open_and_warn(trans):
            return

        if split and trans:
            if split.get_account() == account:
                splits = trans.get_splits()
                splits = splits.append(split)
            # gnc_ui_print_check_dialog_create( window , splits )

        else:
            split = trans.get_split_equal_to_ancestor(trans, account)
            if split:
                splits = trans.get_splits()
                splits = splits.append(split)
                # gnc_ui_print_check_dialog_create( window , splits )


            elif ledger_type == RegisterLedgerType.GL and model.type == RegisterModelType.SEARCH_LEDGER:
                splits = self.ledger.get_query().all()

    def cmd_cut(self, action):
        window = self.get_window()
        widget = window.get_focus()
        if isinstance(widget, Gtk.Entry):
            widget.emit("cut-clipboard")

    def cmd_copy(self, action):
        window = self.get_window()
        widget = window.get_focus()
        if isinstance(widget, Gtk.Entry):
            widget.emit("copy-clipboard")

    def cmd_paste(self, action):
        window = self.get_window()
        widget = window.get_focus()
        if isinstance(widget, Gtk.Entry):
            widget.emit("paste-clipboard")

    def cmd_edit_account(self, action):
        from gasstation.views.dialogs.account import AccountDialog
        account = self.get_account()
        window = self.get_window()
        if account is not None:
            AccountDialog.new_edit(window, account).run()

    def cmd_find_account(self, action):
        from gasstation.views.dialogs.account import FindAccountDialog
        window = self.get_window()
        find_window = FindAccountDialog(window, None)
        find_window.present()

    def cmd_find_transactions(self, action):
        # gnc_ui_find_transactions_dialog_create2()
        pass

    def cmd_cut_transaction(self, action):
        view = self.ledger.get_view()
        view.cut_trans()

    def cmd_copy_transaction(self, action):
        view = self.ledger.get_view()
        view.copy_trans()

    def cmd_paste_transaction(self, action):
        view = self.ledger.get_view()
        view.paste_trans()

    def cmd_void_transaction(self, action):
        view = self.ledger.get_view()
        trans = view.get_current_trans()
        if trans is None:
            return
        if trans.has_split_in_state(SplitState.VOIDED):
            return
        if trans.has_reconciled_splits() or trans.has_split_in_state(SplitState.CLEARED):
            show_error(None, "You cannot void a transaction with reconciled or cleared splits.")
            return
        if not self.finish_pending():
            return
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/plugin_page/register.ui",
                                          ["void_transaction_dialog"])
        dialog = builder.get_object("void_transaction_dialog")
        entry = builder.get_object("reason")

        result = dialog.run()
        reason = ""
        if result == Gtk.ResponseType.OK:
            reason = entry.get_text()
        view.void_current_trans(reason)

        dialog.destroy()

    def cmd_unvoid_transaction(self, action):
        view = self.ledger.get_view()
        trans = view.get_current_trans()
        if not trans.has_split_in_state(SplitState.VOIDED):
            return
        view.un_void_current_trans()

    def cmd_reverse_transaction(self, action):
        view = self.ledger.get_view()
        view.reverse_current()

    def cmd_entryUp(self, action):
        view = self.ledger.get_view()
        view.move_current_entry_updown(True)

    def cmd_entryDown(self, action):
        view = self.ledger.get_view()
        view.move_current_entry_updown(False)

    def cmd_view_filter_by(self, action):
        if self.fd.dialog:
            self.fd.dialog.present()
            return
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/plugin_page/register2.ui",
                                          ["filter_by_dialog"])
        dialog = builder.get_object("filter_by_dialog")
        self.fd.dialog = dialog
        dialog.set_transient_for(self.window)
        title = "Filter %s by..." % PluginPage.get_name(self)
        dialog.set_title(title)

        for p in status_actions:
            toggle = builder.get_object(p.action_name)
            v = self.fd.cleared_match & p.value
            p.widget = toggle
            toggle.set_active(v)

        self.fd.original_cleared_match = self.fd.cleared_match
        button = builder.get_object("filter_save")
        if self.fd.save_filter:
            button.set_active(True)

        ledger_type = self.ledger.get_type()
        if ledger_type == RegisterLedgerType.GL:
            button.set_sensitive(False)
        button = builder.get_object("filter_show_range")
        query = self.ledger.get_query()
        start_time, end_time = query.get_date_match_tt()
        self.fd.original_start_time = start_time
        self.fd.start_time = start_time
        self.fd.original_end_time = end_time
        self.fd.end_time = end_time
        button.set_active(start_time or end_time)
        table = builder.get_object("select_range_table")
        self.fd.table = table
        table.set_sensitive(start_time or end_time)
        self.fd.start_date_choose = builder.get_object("start_date_choose")
        self.fd.start_date_today = builder.get_object("start_date_today")
        self.fd.end_date_choose = builder.get_object("end_date_choose")
        self.fd.end_date_today = builder.get_object("end_date_today")

        if start_time == 0:
            button = builder.get_object("start_date_earliest")
            time_val = query.get_earliest_date_found()
            sensitive = False
        else:
            time_val = start_time
            if datetime.datetime.now().replace(hour=00, minute=00,
                                               second=00) <= start_time <= datetime.datetime.now().replace(hour=23,
                                                                                                           minute=59,
                                                                                                           second=59):
                button = self.fd.start_date_today
                sensitive = False
            else:
                button = self.fd.start_date_choose
                sensitive = True
        button.set_active(True)
        self.fd.start_date = DateSelection()
        hbox = builder.get_object("start_date_hbox")
        hbox.pack_start(self.fd.start_date, True, True, 0)
        self.fd.start_date.show()
        self.fd.start_date.set_sensitive(sensitive)
        self.fd.start_date.set_time(time_val)
        self.fd.start_date.connect("date-changed", self.filter_gde_changed_cb)
        if end_time == 0:
            button = builder.get_object("end_date_latest")
            time_val = query.get_latest_date_found()
            sensitive = False
        else:
            time_val = end_time
            if time64_get_today_start() <= end_time <= time64_get_today_end():
                button = self.fd.end_date_today
                sensitive = False
            else:
                button = self.fd.end_date_choose
                sensitive = True

        button.set_active(True)
        self.fd.end_date = DateSelection()
        hbox = builder.get_object("end_date_hbox")
        hbox.pack_start(self.fd.end_date, True, True, 0)
        self.fd.end_date.show()
        self.fd.end_date.set_sensitive(sensitive)
        self.fd.end_date.set_time(time_val)
        self.fd.end_date.connect("date-changed", self.filter_gde_changed_cb)
        builder.connect_signals(self)
        dialog.show()

    def cmd_reload(self, action):
        view = self.ledger.get_view()
        trans = view.get_current_trans()
        if view.trans_open_and_warn(trans):
            return
        while Gtk.events_pending():
            Gtk.main_iteration()
        GLib.idle_add(self.ledger.refresh)

    def cmd_style_changed(self, action, _):
        value = action.get_current_value()
        self.gsr.change_style(value)
        self.ui_update()

    def cmd_style_double_line(self, action):
        model = self.ledger.get_model()
        view = self.ledger.get_view()
        use_double_line = action.get_active()
        if use_double_line != model.use_double_line:
            model.config(model.type, model.style, use_double_line)
        view.set_format()

    def cmd_style_extra_dates(self, action):
        view = self.ledger.get_view()
        show_extra_dates = action.get_active()
        view.show_extra_dates = show_extra_dates

    def cmd_transfer(self, _):
        from gasstation.views.dialogs import TransferDialog
        account = self.get_account()
        window = self.window
        TransferDialog(window, account)

    def cmd_reconcile(self, _):
        from gasstation.views.dialogs.reconcile import RecnWindow
        view = self.ledger.get_view()
        account = self.get_account()
        trans = view.get_current_trans()
        if view.trans_open_and_warn(trans):
            return
        window = self.window
        rw = RecnWindow.new(window, account)
        rw._raise()

    def cmd_autoclear(self, _):
        account = self.get_account()
        window = self.window
        # autoClearData = autoClearWindow( GTK_WIDGET( window ) , account )
        # gnc_ui_autoclear_window_raise( autoClearData )

    def cmd_stock_split(self, action):
        from gasstation.views.dialogs.stock_split import StockSplitDialog
        account = self.get_account()
        StockSplitDialog.new(None, account)

    def cmd_lots(self, _):
        from gasstation.views.dialogs.lots import LotViewer
        account = self.get_account()
        window = self.window
        LotViewer(window, account)

    def cmd_enter_transaction(self, _):
        view = self.ledger.get_view()
        view.control_enter()

    def cmd_cancel_transaction(self, _):
        view = self.ledger.get_view()
        view.cancel_edit(False)

    def cmd_delete_transaction(self, _):
        view = self.ledger.get_view()
        view.delete()

    def cmd_blank_transaction(self, _):
        view = self.ledger.get_view()
        view.jump_to_blank()

    def cmd_duplicate_transaction(self, _):
        view = self.ledger.get_view()
        view.duplicate_current()

    def cmd_reinitialize_transaction(self, _):
        view = self.ledger.get_view()
        view.reinit(None)

    def cmd_expand_transaction(self, action):
        view = self.ledger.get_view()
        expand = action.get_active()
        if expand:
            view.expand_trans(None)
        else:
            view.collapse_trans(None)

    def cmd_exchange_rate(self, _):
        view = self.ledger.get_view()
        view.exchange_rate()

    def cmd_jump(self, _):
        window = self.window
        if window is None:
            return
        view = self.ledger.get_view()
        split = view.get_current_split()
        if split is None:
            split = view.get_current_trans_split()
            if split is None:
                return

        if not view.trans_expanded(None):
            trans = split.get_transaction()
            if trans is not None and len(trans) > 2:
                return
        depth = view.get_selected_row_depth()
        if view.trans_expanded(None) and depth != RowDepth.SPLIT3:
            return
        account = split.get_account()
        if account is None:
            return
        leader = self.ledger.get_leader()
        if account == leader:
            split = split.get_other_split()
            if split is None:
                return

            account = split.get_account()
            if account is None:
                return

            if account == leader:
                return
        new_page = RegisterPluginPage.new(account, False)
        if new_page is None:
            return
        window.open_page(new_page)
        ld = new_page.get_ledger()
        new_view = ld.get_view()
        new_model = ld.get_model()
        new_model.current_trans = split.get_transaction()
        if not new_model.trans_is_in_view(split.get_transaction()):
            new_model.emit("refresh_trans", 0)
        new_view.jump_to(None, split, False)

    def cmd_schedule(self, _):
        from gasstation.views.dialogs.scheduled import ScheduledFromTransactionDialog, ScheduledTransactionEditorDialog
        view = self.ledger.get_view()
        window = self.window
        trans = view.get_current_trans()
        if trans is None:
            return
        if trans == view.get_blank_trans:
            return

        if view.trans_test_for_edit(trans):
            return

        if view.trans_open_and_warn(trans):
            return
        if view.trans_readonly_and_warn(trans):
            return

        the_scheduled = trans.scheduled_transaction
        if the_scheduled is not None:
            ScheduledTransactionEditorDialog(window, the_scheduled, False)
            return
        dialog = ScheduledFromTransactionDialog(window, trans)
        dialog.run()

    def cmd_scrub_current(self, _):
        query = self.ledger.get_query()
        if query is None:
            return
        view = self.ledger.get_view()
        trans = view.get_current_trans()
        if trans is None:
            return
        gs_set_busy_cursor(self.window, True)
        ComponentManager.suspend()
        root = Session.get_current_root()
        Scrub.trans_orphans(trans)
        Scrub.trans_imbalance(trans, root, None)
        ComponentManager.resume()
        gs_unset_busy_cursor(self.window)

    def cmd_scrub_all(self, _):
        query = self.ledger.get_query()
        if not query:
            return
        gs_set_busy_cursor(self.window, True)
        ComponentManager.suspend()

        root = Session.get_current_root()

        for split in query.run():
            trans = split.get_transaction()
            Scrub.trans_orphans(trans)
            Scrub.trans_imbalance(trans, root, None)

        ComponentManager.resume()
        gs_unset_busy_cursor(self.window)

    def cmd_account_report(self, _):
        from gasstation.plugins.pages.report import ReportPluginPage
        _id = self.report_helper()
        if _id >= 0:
            ReportPluginPage.open_report(_id, self.window)

    def cmd_jump_associated_invoice(self, action):
        pass

    def cmd_transaction_report(self, _):
        view = self.ledger.get_view()
        split = view.get_current_split()
        if split is None:
            return
        query = Query.create_for(ID_SPLIT)
        query.set_book(Session.get_current_book())
        query.add_guid_match(split.get_guid(), ID_SPLIT, QueryOp.AND)
        window = self.window
        _id = self.report_helper(split, query)
        if _id >= 0:
            from gasstation.plugins.pages import ReportPluginPage
            ReportPluginPage.open_report(_id, window)

    def set_options(self, lines_default, read_only):
        self.lines_default = lines_default
        self.read_only = read_only

    def get_gsr(self):
        return self.gsr

    def get_ledger(self):
        return self.ledger

    def refresh_cb(self, changes):
        view = self.ledger.get_view()
        if changes is not None:
            ei = ComponentManager.get_entity_events(changes, self.key)
            if ei:
                if ei.event_mask & EVENT_DESTROY:
                    view.set_state_section(None)
                    self.window.close_page(self)
                    return

                elif ei.event_mask & EVENT_MODIFY:
                    pass
        else:
            view.refresh_from_prefs()
        self.ui_update()

    def close_cb(self):
        self.window.close_page(self)

    def button_press_cb(self, widget, event):
        self.window.button_press_cb(widget, event, self)
        return False

    def create_widget(self):
        if self.widget is not None:
            return self.widget
        filter_changed = 0
        self.widget = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.widget.set_homogeneous(False)
        self.widget.show()
        gs_widget_set_style_context(self.widget, "RegisterPage")
        numRows = self.lines_default
        numRows = min(numRows, DEFAULT_LINES_AMOUNT)
        gs_window = self.window
        self.gsr = RegisterWindow(self.ledger, self.window.get_gtk_window(gs_window), numRows, self.read_only)
        self.gsr.show()
        self.widget.pack_start(self.gsr, True, True, 0)
        self.help_id = self.gsr.connect("help-changed", self.help_changed_cb)
        view = self.gsr.get_register()
        self.button_id = view.connect("button-press-event", self.button_press_cb)

        model = self.ledger.get_model()
        model.config(model.type, model.style, model.use_double_line)
        self.ui_initial_state()
        self.ui_update()
        ledger_type = self.ledger.get_type()
        if ledger_type == RegisterLedgerType.SINGLE or ledger_type == RegisterLedgerType.SUB_ACCOUNT or ledger_type == RegisterLedgerType.GL:
            self.fd.save_filter = False
            fil = self.get_filter().split(",")
            self.fd.cleared_match = int(fil[0] or "0", 16)

            if fil[0] and fil[0] != DEFAULT_FILTER:
                filter_changed += 1

            if fil[1] and fil[1] != "0":
                self.fd.start_time = self.filter_dmy2time(fil[1])
                self.fd.start_time = time64_get_day_start(self.fd.start_time)
                filter_changed += 1

                if fil[2] and fil[2] != "0":
                    self.fd.end_time = self.filter_dmy2time(fil[2])
                    self.fd.end_time = time64_get_day_end(self.fd.end_time)
                    filter_changed += 1
            if filter_changed != 0:
                self.fd.save_filter = True
            self.fd.original_save_filter = self.fd.save_filter
            self.update_status_query(False)
            self.update_date_query(False)
        self.ledger.refresh()
        if not model.get_template():
            view.default_selection()
        self.summary_bar = self.gsr.create_summary_bar()
        if self.summary_bar is not None:
            self.summary_bar.show_all()
            self.widget.pack_start(self.summary_bar, False, False, 0)
            self.summary_bar_position_changed(None, None)
            gs_pref_register_cb(PREFS_GROUP_GENERAL,
                                PREF_SUMMARY_BAR_POSITION_TOP,
                                self.summary_bar_position_changed)
            gs_pref_register_cb(PREFS_GROUP_GENERAL,
                                PREF_SUMMARY_BAR_POSITION_BOTTOM,
                                self.summary_bar_position_changed)
        self.event_handler_id = Event.register_handler(self.event_handler)
        self.component_manager_id = ComponentManager.register(PLUGIN_PAGE_REGISTER_NAME, self.refresh_cb,
                                                              self.close_cb)
        ComponentManager.set_session(self.component_manager_id, Session.get_current_session())
        acct = self.get_account()
        if acct is not None:
            ComponentManager.watch_entity(self.component_manager_id, acct.guid, EVENT_DESTROY | EVENT_MODIFY)
        self.gsr.set_moved_cb(self.ui_update)
        return self.widget

    def update_edit_menu_actions(self, hide):
        is_readwrite = not Session.get_current_book().is_readonly()
        view = self.ledger.get_view()
        if view.editing_now:
            has_selection = True
        else:
            has_selection = False
        can_copy = has_selection
        can_cut = is_readwrite and has_selection
        can_paste = is_readwrite
        action = self.get_action("EditCopyAction")
        action.set_sensitive(can_copy)
        action.set_visible(not hide or can_copy)
        action = self.get_action("EditCutAction")
        action.set_sensitive(can_cut)
        action.set_visible(not hide or can_cut)
        action = self.get_action("EditPasteAction")
        action.set_sensitive(can_paste)
        action.set_visible(not hide or can_paste)

    def event_handler(self, entity, event_type, *args):
        if not isinstance(entity, (Account, Transaction)):
            return
        window = self.get_window()
        if isinstance(entity, Account):
            window.update_page_name(self, self.get_tab_name())
            window.update_page_color(self, self.get_tab_color())

        if not event_type & (EVENT_MODIFY | EVENT_DESTROY):
            return
        trans = entity
        book = trans.get_book()
        if not self.has_book(book):
            return
        visible_page = window.get_current_page()
        if visible_page != self:
            return
        self.ui_update()

    def help_changed_cb(self, gsr, p):
        if not self.window: return
        view = self.ledger.get_view()
        self.window.set_status(self, view.help_text)

    def report_helper(self, split=None, query=None):
        ledger = self.ledger
        view = ledger.get_view()
        model = self.ledger.get_model()
        tmp = view.get_credit_debit_string(True)
        credit = tmp if tmp is not None else "Credit"
        tmp = view.get_credit_debit_string(False)
        debit = tmp if tmp is not None else "Debit"
        name = self.get_name(ledger, False)
        use_double_line = model.use_double_line
        _type = model.type == RegisterModelType.GENERAL_JOURNAL or \
                model.type == RegisterModelType.INCOME_LEDGER or model.type == RegisterModelType.SEARCH_LEDGER
        journal = model.style == RegisterModelStyle.JOURNAL
        if query is None:
            query = ledger.get_query()
            if query is None:
                return -1
        account = ledger.get_leader()
        return StandardReports.register_report(account, split, query, journal, _type, use_double_line, name, debit,
                                               credit)

    def __repr__(self):
        return u"<Register Page(%s)>" % self.get_long_name()


GObject.type_register(RegisterPluginPage)
