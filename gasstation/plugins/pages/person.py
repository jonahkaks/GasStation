from gasstation.plugins.pages.report import ReportPluginPage
from gasstation.utilities.actions import update_actions, init_short_names
from gasstation.utilities.cursors import gs_unset_busy_cursor, gs_set_busy_cursor
from gasstation.utilities.icons import ICON_DELETE_ACCOUNT, ICON_INVOICE_NEW, ICON_INVOICE_PAY
from gasstation.views.components.avatar import Avatar
from gasstation.views.dialogs import CustomerDialog
from gasstation.views.dialogs.customer.invoice import *
from gasstation.views.dialogs.staff.staff import StaffDialog
from gasstation.views.dialogs.supplier import SupplierDialog
from gasstation.views.dialogs.supplier.bill import BillDialog
from gasstation.views.dialogs.supplier.expense import ExpenseDialog
from gasstation.views.person import *
from gasstation.views.reports import PersonReport
from libgasstation import Bill, Invoice
from .page import PluginPage, action_toolbar_labels

PLUGIN_PAGE_ACCT_TREE_CM_CLASS = "plugin-page-person-tree"
DELETE_DIALOG_FILTER = "filter"
DELETE_DIALOG_PERSON = "person"

PERSON_TYPE_LABEL = "PersonType"

actions_requiring_person_rw = ["OTEditSupplierAction",
                               "OTEditCustomerAction",
                               "OTEditStaffAction",
                               "OTDeleteSupplierAction",
                               "OTDeleteCustomerAction",
                               "OTDeleteStaffAction",
                               ]

actions_requiring_person_always = ["OTSupplierReportAction",
                                   "OTCustomerReportAction",
                                   "OTStaffReportAction",
                                   "OTNewPaymentAction"]
readonly_inactive_actions = [
    "OTNewSupplierAction",
    "OTNewCustomerAction",
    "OTNewStaffAction",
    "OTNewBillAction",
    "OTNewInvoiceAction",
    "OTNewReceiptAction",
    "OTNewExpenseAction",
    "OTNewVoucherAction"]

toolbar_labels = [
    action_toolbar_labels("OTEditSupplierAction", "Edit"),
    action_toolbar_labels("OTEditCustomerAction", "Edit"),
    action_toolbar_labels("OTEditStaffAction", "Edit"),
    action_toolbar_labels("OTNewSupplierAction", "New"),
    action_toolbar_labels("OTNewCustomerAction", "New"),
    action_toolbar_labels("OTNewStaffAction", "New"),
    action_toolbar_labels("OTNewBillAction", "New Bill"),
    action_toolbar_labels("OTNewExpenseAction", "New Expense"),
    action_toolbar_labels("OTNewInvoiceAction", "New Invoice"),
    action_toolbar_labels("OTNewReceiptAction", "New Receipt"),
    action_toolbar_labels("OTNewVoucherAction", "New Voucher"),
    action_toolbar_labels("OTNewPaymentAction", "Pay"),
    action_toolbar_labels("OTSupplierListingReportAction", "Supplier Listing"),
    action_toolbar_labels("OTCustomerListingReportAction", "Customer Listing"),
    action_toolbar_labels("OTDeleteSupplierAction", "Delete"),
    action_toolbar_labels("OTDeleteCustomerAction", "Delete"),
    action_toolbar_labels("OTDeleteStaffAction", "Delete")]

action_persons = [("OTEditSupplierAction", "Supplier"),
                  ("OTEditCustomerAction", "Customer"),
                  ("OTEditStaffAction", "Staff"),
                  ("OTDeleteSupplierAction", "Supplier"),
                  ("OTDeleteCustomerAction", "Customer"),
                  ("OTDeleteStaffAction", "Staff"),
                  ("OTNewSupplierAction", "Supplier"),
                  ("OTNewCustomerAction", "Customer"),
                  ("OTNewStaffAction", "Staff"),
                  ("OTNewBillAction", "Supplier"),
                  ("OTNewExpenseAction", "Supplier"),
                  ("OTNewInvoiceAction", "Customer"),
                  ("OTNewReceiptAction", "Customer"),
                  ("OTNewVoucherAction", "Staff"),
                  ("OTSupplierListingReportAction", "Supplier"),
                  ("OTCustomerListingReportAction", "Customer"),
                  ("OTSupplierReportAction", "Supplier"),
                  ("OTCustomerReportAction", "Customer"),
                  ("OTStaffReportAction", "Staff")]

PLUGIN_PAGE_PERSON_NAME = "StakeHolderPluginPage"


class StakeHolderPluginPage(PluginPage):
    tab_icon = "system-users-symbolic"
    plugin_name = PLUGIN_PAGE_PERSON_NAME
    __gtype_name__ = "StakeHolderPluginPage"

    __gsignals__ = {
        "person_selected": (GObject.SignalFlags.RUN_FIRST, None, (object,))
    }

    def __init__(self):
        super().__init__()
        self.widget = None
        self.tree_view = None
        self.component_id = 0
        self.person_type = "Customer"
        self.fd = PersonFilterDialog()
        self.set_property("name", "Persons")
        self.set_property("uri", "default:")
        self.set_property("ui-description", "plugin_page/person-tree-ui.xml")

        self.connect("selected", self.selected)
        self.add_book(Session.get_current_book())
        action_group = self.create_action_group("StakeHolderPluginPageActions")
        for name, stock_id, label, accelerator, tooltip, callback in [
            ("FakeToplevel", None, "", None, None, None),
            (
                    "OTEditSupplierAction", "user-info-symbolic", "E_dit Supplier", "<primary>e",
                    "Edit the selected vendor",
                    self.cmd_edit_person
            ),
            (
                    "OTEditCustomerAction", "user-info-symbolic", "E_dit Customer", "<primary>e",
                    "Edit the selected customer",
                    self.cmd_edit_person
            ),
            (
                    "OTEditStaffAction", "user-info-symbolic", "E_dit Staff", "<primary>e",
                    "Edit the selected employee",
                    self.cmd_edit_person
            ),
            (
                    "OTNewSupplierAction", "contact-new-symbolic", "_New Supplier...", None,
                    "Create a new vendor",
                    self.cmd_new_person
            ),
            (
                    "OTNewCustomerAction", "contact-new-symbolic", "_New Customer...", None,
                    "Create a new customer",
                    self.cmd_new_person
            ),
            (
                    "OTNewStaffAction", "contact-new-symbolic", "_New Staff...", None,
                    "Create a new employee",
                    self.cmd_new_person
            ),
            (
                    "OTDeleteSupplierAction", ICON_DELETE_ACCOUNT, "_Delete Supplier...", "Delete",
                    "Delete selected supplier",
                    self.cmd_delete_person
            ),
            (
                    "OTDeleteCustomerAction", ICON_DELETE_ACCOUNT, "_Delete Customer...", "Delete",
                    "Delete selected customer",
                    self.cmd_delete_person
            ),
            (
                    "OTDeleteStaffAction", ICON_DELETE_ACCOUNT, "_Delete Staff...", "Delete",
                    "Delete selected employee",
                    self.cmd_delete_person
            ),

            (
                    "ViewFilterByAction", None, "_Filter By...", None, None,
                    self.cmd_view_filter_by
            ),
            (
                    "ViewRefreshAction", "view-refresh", "_Refresh", "<primary>r",
                    "Refresh this window",
                    self.cmd_refresh
            ),
            (
                    "OTNewBillAction", ICON_INVOICE_NEW, "New _Bill...", None,
                    "Create a new bill",
                    self.cmd_new_bill_invoice
            ),
            (
                    "OTNewInvoiceAction", ICON_INVOICE_NEW, "New _Invoice...", None,
                    "Create a new invoice",
                    self.cmd_new_bill_invoice
            ),
            (
                    "OTNewExpenseAction", ICON_INVOICE_NEW, "New _Expense...", None,
                    "Create a new expense",
                    self.cmd_new_expense_receipt
            ),
            (
                    "OTNewReceiptAction", ICON_INVOICE_NEW, "New _Receipt...", None,
                    "Create a new receipt",
                    self.cmd_new_expense_receipt
            ),
            (
                    "OTNewVoucherAction", ICON_INVOICE_NEW, "New _Voucher...", None,
                    "Create a new voucher",
                    self.cmd_new_bill_invoice
            ),
            (
                    "OTNewPaymentAction", ICON_INVOICE_PAY, "Receive Payment...", None,
                    "Pay",
                    self.cmd_new_payment
            ),
            (
                    "OTSupplierListingReportAction", "document-print-preview", "Supplier Listing", None,
                    "Show vendor aging overview for all vendors",
                    self.cmd_persons_report
            ),
            (
                    "OTCustomerListingReportAction", "document-print-preview", "Customer Listing", None,
                    "Show customer aging overview for all customers",
                    self.cmd_persons_report
            ),
            (
                    "OTSupplierReportAction", None, "Supplier Statement", None,
                    "Show vendor report",
                    self.cmd_person_report
            ),
            (
                    "OTCustomerReportAction", None, "Customer Statement", None,
                    "Show customer report",
                    self.cmd_person_report
            ),
            (
                    "OTStaffReportAction", None, "Staff Statement", None,
                    "Show employee report",
                    self.cmd_person_report
            )]:
            action = Gtk.Action(name, label, tooltip, None)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                action.connect('activate', callback)
            action_group.add_action_with_accel(action, accelerator)
        init_short_names(action_group, toolbar_labels)

        self.search_text = ""
        self.search_bar = None
        self.fd.show_inactive = True
        self.more_info_box = None
        self.fd.show_zero_total = True

    @classmethod
    def new(cls, person_type):
        items = Tracking.get_list(PLUGIN_PAGE_PERSON_NAME)
        for item in items:
            if item.person_type == person_type:
                return item
        self = cls()
        self.person_type = person_type
        action_group = self.get_action_group()
        for i in action_persons:
            action = action_group.get_action(i[0])
            action.set_visible(self.person_type == i[1])
        return self

    def focus_widget(self):
        tree_view = self.tree_view
        if isinstance(tree_view, Gtk.TreeView):
            if not tree_view.is_focus():
                tree_view.grab_focus()
        return False

    def update_inactive_actions(self):
        is_sensitive = not Session.get_current_book().is_readonly()
        action_group = self.get_action_group()
        if not isinstance(action_group, Gtk.ActionGroup):
            return
        update_actions(action_group, readonly_inactive_actions, "sensitive", is_sensitive)

    def selected(self, *args):
        self.update_inactive_actions()

    def refresh_cb(self, changes):
        if changes:
            return
        self.widget.queue_draw()

    def close_cb(self, *args):
        self.window.close_page(self)

    def create_widget(self):
        if self.widget is not None:
            return self.widget
        self.widget = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        self.widget.set_homogeneous(False)
        self.widget.show()
        gs_widget_set_style_context(self.widget, "BusinessPage")
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scrolled_window.set_border_width(10)
        scrolled_window.show()
        search_entry = Gtk.SearchEntry()
        bar = Gtk.SearchBar.new()
        bar.connect_entry(search_entry)
        bar.add(search_entry)
        search_entry.show()
        self.search_bar = bar
        self.widget.pack_start(bar, False, True, 0)
        bar.show()
        search_entry.connect("search-changed", self.search_changed_cb)

        hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 8)
        self.more_info_box = Gtk.ListBox()
        self.more_info_box.set_selection_mode(Gtk.SelectionMode.NONE)
        gs_widget_set_style_context(self.more_info_box, "more_info")
        hbox.pack_start(scrolled_window, True, True, 6)
        hbox.pack_start(self.more_info_box, False, True, 0)
        self.widget.pack_start(hbox, True, True, 0)
        hbox.show()

        tree_view = PersonView(self.person_type)
        tree_view.configure_columns()
        if self.person_type == "Customer":
            label = "Customers"
            state_section = "Customers Overview"
            style_label = "Customers"

        elif self.person_type == "Supplier":
            label = "Suppliers"
            state_section = "Suppliers Overview"
            style_label = "Suppliers"

        elif self.person_type == "Staff":
            label = "Staff"
            state_section = "Staff Overview"
            style_label = "Staff"
        else:
            raise ValueError("Unknown person type")

        gs_widget_set_style_context(self.widget, style_label)
        tree_view.set_properties(state_section=state_section, show_column_menu=True)
        self.set_name(label)
        self.tree_view = tree_view
        selection = tree_view.get_selection()
        selection.connect("changed", self.selection_changed_cb)
        tree_view.connect("button-press-event", self.button_press_cb)
        tree_view.connect("row-activated", self.double_click_cb)

        tree_view.set_headers_visible(True)
        self.selection_changed_cb(None)
        tree_view.show()
        scrolled_window.add(tree_view)

        self.fd.view = self.tree_view
        self.tree_view.set_filter(self.filter_person, None, None)
        self.component_id = ComponentManager.register(PLUGIN_PAGE_ACCT_TREE_CM_CLASS, self.refresh_cb, self.close_cb)
        ComponentManager.set_session(self.component_id, Session.get_current_session())
        self.connect("inserted", self.inserted_cb)
        self.connect("inserted", lambda *args: self.window.connect("key-press-event", self.key_press_cb))
        return self.widget

    def key_press_cb(self, window, evt):
        if window.get_current_page() != self:
            return
        if evt.keyval in [Gdk.KEY_F, Gdk.KEY_f]:
            if evt.state & Gdk.ModifierType.CONTROL_MASK:
                if self.search_bar.get_search_mode():
                    self.search_bar.set_search_mode(False)
                else:
                    self.search_bar.set_search_mode(True)

    def filter_person(self, person, *_):
        if person is None or person.get_name() is None:
            return True
        return self.search_text in person.get_name().lower()

    def search_changed_cb(self, widget):
        self.search_text = widget.get_text().lower()
        self.tree_view.refilter()

    def destroy_widget(self):
        self.disconnect_page_changed()
        if self.focus_source_id > 0:
            GLib.source_remove(self.focus_source_id)
        if self.widget is not None:
            self.widget.destroy()
            self.widget = None
        if self.component_id > 0:
            ComponentManager.unregister(self.component_id)
            self.component_id = 0
        Tracking.forget(self)

    def save_page(self, key_file, group_name):
        if key_file is None or group_name is None:
            return
        key_file.set_string(group_name, PERSON_TYPE_LABEL, self.person_type)
        self.tree_view.save(self.fd, key_file, group_name)

    @classmethod
    def recreate_page(cls, window, key_file, group_name):
        if key_file is None or group_name is None:
            return
        person_type = key_file.get_string(group_name, PERSON_TYPE_LABEL)
        self = cls.new(person_type)
        window.open_page(self)
        self.tree_view.restore(self.fd, key_file, group_name, person_type)
        return self

    def ui_edit(self, parent, person):
        book = Session.get_current_book()
        if person is None:
            return
        dialog = None
        if self.person_type == "Customer":
            dialog = CustomerDialog(parent, book, customer=person)
        elif self.person_type == "Supplier":
            dialog = SupplierDialog(parent, book, supplier=person)
        elif self.person_type == "Staff":
            dialog = StaffDialog(parent, book, staff=person)
        if dialog is not None:
            dialog.run()

    def button_press_cb(self, widget, event):
        self.window.button_press_cb(widget, event, self)
        return False

    def double_click_cb(self, treeview, path, col):
        self.cmd_edit_person(None)

    def show_more_info(self, current_person: Person):
        if self.more_info_box is not None:
            for child in self.more_info_box.get_children():
                self.more_info_box.remove(child)
            row = Gtk.ListBoxRow()
            box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 20)
            avatar_button = Avatar()
            avatar_button.connect("image-selected", lambda w, url:current_person.get_contact().set_image_url(url))
            contact = current_person.get_contact()
            avatar_button.set_fullname(contact.get_display_name())
            avatar_button.set_url(contact.get_image_url())
            avatar_button.set_halign(Gtk.Align.CENTER)
            box.pack_start(avatar_button, False, False, 0)
            person_name = Gtk.Label(contact.get_display_name())
            person_name.set_halign(Gtk.Align.CENTER)
            box.pack_start(person_name, True, True, 0)
            product_dec = Gtk.Label(current_person.__class__.__name__)
            product_dec.set_line_wrap(True)
            product_dec.set_halign(Gtk.Align.CENTER)
            product_dec.set_size_request(80, 20)
            box.pack_start(product_dec, True, True, 0)
            box.set_vexpand(True)
            row.add(box)
            self.more_info_box.add(row)

            row = Gtk.ListBoxRow()
            hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
            box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
            person_label = current_person.__class__.__name__
            title = "{} (Unpaid Balance)".format("Invoices" if person_label == "Customer" else
                                                 "Bills" if person_label == "Supplier" else "PaySlips")
            t = Gtk.Label(title)
            gs_widget_set_style_context(t, "title")
            t.set_halign(Gtk.Align.START)
            box.pack_start(t, True, True, 8)
            currency = current_person.get_currency()
            balance = sprintamount(current_person.get_balance_in_currency(currency),
                                   PrintAmountInfo.commodity(currency, True))
            balance_label = Gtk.Label(balance)
            gs_widget_set_style_context(balance_label, "sub_title")
            balance_label.set_halign(Gtk.Align.START)
            box.pack_start(balance_label, True, True, 0)
            hbox.pack_start(box, False, False, 0)
            hbox.pack_end(Gtk.Image.new_from_icon_name("go-next", Gtk.IconSize.BUTTON), False, False, 0)
            row.add(hbox)

            self.more_info_box.add(row)

            self.more_info_box.show_all()

    def selection_changed_cb(self, selection):
        is_readwrite = not Session.get_current_book().is_readonly()
        person = None
        if selection is None:
            sensitive = False
        else:
            view = selection.get_tree_view()
            persons = view.get_selected_persons()
            sensitive = any(persons)
            if any(persons):
                GLib.idle_add(self.show_more_info, persons[-1])
        action_group = self.get_action_group()
        update_actions(action_group, actions_requiring_person_always, "sensitive", sensitive)
        update_actions(action_group, actions_requiring_person_rw, "sensitive", sensitive and is_readwrite)
        self.emit("person_selected", person)

    def cmd_new_person(self, _):
        dialog = None
        parent = self.get_window()
        book = Session.get_current_book()
        if self.person_type == "Customer":
            dialog = CustomerDialog(parent, book)
        elif self.person_type == "Supplier":
            dialog = SupplierDialog(parent, book)
        elif self.person_type == "Staff":
            StaffDialog(parent, book)
        if dialog is not None:
            dialog.run()

    def cmd_edit_person(self, action):
        persons = self.tree_view.get_selected_persons()
        if persons is None or not isinstance(persons, list):
            return
        for person in persons:
            parent = self.get_window()
            self.ui_edit(parent, person)

    def cmd_delete_person(self, action):
        from gasstation.views.dialogs.object_references import ObjectReferencesDialog
        persons = self.tree_view.get_selected_persons()
        if persons is None or not isinstance(persons, list):
            return
        person_name = "customer" if self.person_type == "Customer" else "supplier" if self.person_type == "Supplier" else "employee"
        dialog = Gtk.MessageDialog(transient_for=self.get_window(), modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE,
                                   message_format="The following %s will be deleted %s" % (person_name,
                                                                                           ",".join(
                                                                                               [
                                                                                                   person.get_contact().get_display_name()
                                                                                                   for person in
                                                                                                   persons])))
        dialog.add_buttons("Cancel", Gtk.ResponseType.CANCEL, "Delete", Gtk.ResponseType.ACCEPT)
        dialog.set_default_response(Gtk.ResponseType.CANCEL)
        response = dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            gs_set_busy_cursor(None, True)
            ComponentManager.suspend()
            book = Session.get_current_book()
            be = book.get_backend()
            if be is not None:
                with be.add_lock():
                    for person in persons:
                        lis = person.get_referrers()
                        if len(lis):
                            exp = "The list below shows objects which make use of the %s which you want to delete." \
                                  "\nBefore you can delete it, you must either delete those objects or else modify them so " \
                                  "they make use of another %s" % (person_name, person_name)
                            ObjectReferencesDialog(exp, lis)
                            continue
                        person.destroy()
            ComponentManager.resume()
            gs_unset_busy_cursor(None)

    def cmd_view_filter_by(self, _):
        self.tree_view.set_filter(self.fd.filter_persons, None, None)
        self.fd.create(self)
        self.fd.dialog.connect("response", lambda *_: self.tree_view.set_filter(self.filter_person, None, None))

    def cmd_refresh(self, _):
        self.widget.queue_draw()

    def cmd_new_bill_invoice(self, _):
        persons = self.tree_view.get_selected_persons()
        window = self.get_window()
        book = Session.get_current_book()
        for person in persons:
            if isinstance(person, Customer):
                invoice = Invoice(book)
                invoice.begin_edit()
                invoice.set_customer(person)
                dialog = InvoiceDialog(window, invoice)
            elif isinstance(person, Supplier):
                bill = Bill(book)
                bill.begin_edit()
                bill.set_supplier(person)
                dialog = BillDialog(window, bill)
            else:
                return
            dialog.run()

    def cmd_new_expense_receipt(self, _):
        persons = self.tree_view.get_selected_persons()
        window = self.get_window()
        book = Session.get_current_book()
        for person in persons:
            if isinstance(person, Customer):
                invoice = Invoice(book)
                invoice.begin_edit()
                invoice.set_customer(person)
                InvoiceDialog(window, invoice)
            elif isinstance(person, Supplier):
                bill = Bill(book)
                bill.begin_edit()
                bill.set_supplier(person)
                dialog = ExpenseDialog(window, bill)
                dialog.present()

    def cmd_new_payment(self, _):
        from gasstation.views.dialogs.payment.payment import PaymentDialog
        persons = self.tree_view.get_selected_persons()
        window = self.get_window()
        for person in persons:
            d = PaymentDialog(window, person.get_book(), person=person)
            d.run()

    def cmd_persons_report(self, _):
        _id = self.build_aging_report(self.person_type)
        if _id >= 0:
            ReportPluginPage.open_report(_id, self.window)

    def cmd_person_report(self, _):
        persons = self.tree_view.get_selected_persons()
        for person in persons:
            _id = PersonReport.new(person,
                                   person.get_receivable_account() if isinstance(person, Customer) else
                                   person.get_payable_account())
            if _id >= 0:
                ReportPluginPage.open_report(_id, self.window)


GObject.type_register(StakeHolderPluginPage)
