from gasstation.utilities.dialog import *
from gasstation.utilities.icons import ICON_SALES
from gasstation.views.selections import ReferenceSelection
from libgasstation.core.component import ComponentManager
from libgasstation.core.session import *
from .page import *
from ...views.dips import DipsView
from ...views.fuel import FuelView

PLUGIN_PAGE_FUEL_NAME = "FuelPluginPage"
HPOS = "HorizontalSeparator"
VPOS = "VerticalSeparator"


class FuelPluginPage(PluginPage):
    plugin_name = PLUGIN_PAGE_FUEL_NAME
    widget = None
    tab_icon = ICON_SALES
    __gtype_name__ = PLUGIN_PAGE_FUEL_NAME

    def __init__(self):
        super().__init__()
        self.set_property("name", "Fuel Sales Register")
        self.set_property("uri", "default:")
        self.set_property("ui-description", "plugin_page/fuel-ui.xml")
        action_group = self.create_action_group("FuelPluginPageActions")
        for name, stock_id, label, accelerator, tooltip, callback in [
            ("TankAction", "tank", "_Tank", None, "Create a new Tank", self.cmd_tank),
            ("PumpAction", "pump", "_Pump", None, "Create a new pump", self.cmd_pump),
            ("NozzleAction", "nozzle", "Nozzle", None, "Create a new Nozzle", self.cmd_nozzle),
            ("ProcessAction", "preferences-desktop", "Process", None, "Process fuel data", self.cmd_process_data)
        ]:
            action = Gtk.Action(name, label, tooltip, None)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                action.connect('activate', callback)
            action_group.add_action_with_accel(action, accelerator)
        self.add_book(Session.get_current_book())
        self.fuel = None
        self.dips = None
        self.calendar = Gtk.Calendar()
        self.location = ReferenceSelection(Location, None)
        self.component_id = 0
        self.hpaned = None
        self.vpaned = None

    @classmethod
    def new(cls):
        items = Tracking.get_list(PLUGIN_PAGE_FUEL_NAME)
        if any(items):
            return items[0]
        return GObject.new(cls)

    def create_widget(self):
        if self.widget is not None:
            return self.widget
        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 5)
        self.widget = vbox
        self.vpaned = Gtk.Paned.new(Gtk.Orientation.VERTICAL)
        vbox.pack_start(self.vpaned, True, True, 0)

        self.fuel = FuelView()
        self.calendar.connect("day-selected", lambda calendar: self.fuel.refresh(calendar, self.location.get_ref()))
        self.location.connect("selection_changed", lambda w, loc: self.fuel.refresh(self.calendar, loc))
        box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
        box.pack_start(self.fuel.container, True, True, 0)
        cbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 10)
        frame = Gtk.Frame.new(label="Location")
        frame.set_label_align(xalign=0, yalign=0)
        frame.set_shadow_type(Gtk.ShadowType.NONE)
        frame.add(self.location)
        frame.show()
        cbox.pack_start(frame, False, False, 0)
        cbox.pack_start(self.calendar, True, True, 0)
        box.pack_start(cbox, False, False, 0)
        self.vpaned.pack1(box, True, True)

        self.dips = DipsView()
        self.fuel.connect("fuel-changed", self.dips.fuel_changed_cb)
        self.fuel.connect("refreshed", self.dips.refresh)
        self.vpaned.pack2(self.dips.container, True, True)
        self.widget.show_all()
        self.calendar.select_day(time.localtime().tm_mday)
        gs_widget_set_style_context(self.widget, "FuelSalesPage")
        self.component_id = ComponentManager.register(PLUGIN_PAGE_FUEL_NAME, self.refresh_cb, self.close_cb)
        ComponentManager.set_session(self.component_id, Session.get_current_session())
        return self.widget

    def destroy_widget(self):
        if self.component_id:
            ComponentManager.unregister(self.component_id)
            self.component_id = 0
        self.widget.hide()
        if self.fuel:
            self.fuel.destroy()
            self.fuel = None
        if self.dips:
            self.dips.destroy()
            self.dips = None
        if self.focus_source_id > 0:
            GLib.source_remove(self.focus_source_id)
        if self.widget is not None:
            self.widget.destroy()
            self.widget = None
        if self.component_id > 0:
            ComponentManager.unregister(self.component_id)
            self.component_id = 0
        Tracking.forget(self)

    def save_page(self, key_file, group_name):
        vpos = self.vpaned.get_position()
        key_file.set_integer(group_name, VPOS, vpos)

    @classmethod
    def recreate_page(cls, window, key_file, group_name):
        page = cls()
        window.open_page(page)
        page.restore_page(key_file, group_name)
        return page

    def restore_page(self, key_file, group_name):
        try:
            vpos = key_file.get_integer(group_name, VPOS)
            self.vpaned.set_position(vpos)
        except GLib.Error:
            pass

    def button_press_cb(self, widget, event):
        self.window.button_press_cb(widget, event, self)
        return False

    def focus_widget(self):
        view = self.fuel
        if view is not None and not view.is_focus():
            view.grab_focus()
        return False

    def refresh_cb(self, changes):
        self.widget.queue_draw()

    def close_cb(self):
        self.window.close_page(self)

    def cmd_pump(self, action):
        from gasstation.views.dialogs.pump import PumpsDialog
        window = self.get_window()
        PumpsDialog.new(window)

    def cmd_tank(self, action):
        from gasstation.views.dialogs.tank import TanksDialog
        window = self.get_window()
        TanksDialog.new(window)

    def cmd_nozzle(self, action):
        from gasstation.views.dialogs.nozzle import NozzlesDialog
        window = self.get_window()
        NozzlesDialog.new(window)

    def cmd_process_data(self, action):
        window = self.get_window()


GObject.type_register(FuelPluginPage)
