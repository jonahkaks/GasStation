from gasstation.utilities.icons import ICON_PRODUCT, ICON_EDIT_ACCOUNT, ICON_NEW_ACCOUNT, ICON_DELETE_ACCOUNT
from gasstation.views.components.avatar import Avatar
from gasstation.views.product import *
from libgasstation import Location, ProductType
from .page import PluginPage
from ...utilities.actions import update_actions
from ...utilities.custom_dialogs import ask_yes_no, show_error
from ...views.dialogs import ProductDialog

PLUGIN_PAGE_PRODUCT_CM_CLASS = "plugin-page-product"
STATE_SECTION = "Product"
PLUGIN_PAGE_PRODUCT_NAME = "ProductPluginPage"


class ProductPluginPage(PluginPage):
    __gtype_name__ = "ProductPluginPage"
    tab_icon = ICON_PRODUCT
    plugin_name = PLUGIN_PAGE_PRODUCT_NAME
    __gsignals__ = {
        "product_selected": (GObject.SignalFlags.RUN_FIRST, None, (object,))
    }

    def __init__(self):
        super().__init__()
        self.product_image = None
        self.disposed = None
        self.widget = None
        self.component_id = 0
        self.tree_view = None
        self.searchbar = None
        self.more_info_box = None

        self.search_text = ""
        self.set_property("name", "Products & Services")
        self.set_property("uri", "default:")
        self.set_property("ui-description", "plugin_page/product-ui.xml")
        self.add_book(Session.get_current_book())
        action_group = self.create_action_group("ProductPluginPageActions")

        for name, stock_id, label, accelerator, tooltip, callback in [
            (
                    "OTNewProductAction", ICON_NEW_ACCOUNT, "_New", None,
                    "Create a new Product", self.cmd_new
            ),
            (
                    "OTEditProductAction", ICON_EDIT_ACCOUNT, "_Edit", None,
                    "Edit the selected product", self.cmd_edit
            ),
            (
                    "OTDeleteProductAction", ICON_DELETE_ACCOUNT, "_Delete", None,
                    "Delete the selected product", self.cmd_delete
            ),

            (
                    "ViewRefreshAction", "view-refresh", "_Refresh", "<primary>r",
                    "Refresh this window", self.cmd_refresh
            ),
            ("OTProductListingReportAction", "document-print-preview", "Product Listing", None,
             "Show a list of all products",
             self.cmd_product_report
             )]:
            action = Gtk.Action(name, label, tooltip, None)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                action.connect('activate', callback)
            action_group.add_action_with_accel(action, accelerator)

    @classmethod
    def new(cls):
        obj = Tracking.get_list(PLUGIN_PAGE_PRODUCT_NAME)
        if obj is not None and len(obj):
            plugin_page = obj[0]
        else:
            plugin_page = cls()
        return plugin_page

    def focus_widget(self):
        tree_view = self.tree_view
        if isinstance(tree_view, Gtk.TreeView):
            if not tree_view.is_focus():
                tree_view.grab_focus()
        return False

    def refresh_cb(self, changes):
        if changes is not None:
            return
        self.widget.queue_draw()

    def close_cb(self):
        self.window.close_page(self)

    def show_more_info(self, current_product: Product):
        if self.more_info_box is not None:
            for child in self.more_info_box.get_children():
                self.more_info_box.remove(child)
            row = Gtk.ListBoxRow()
            box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 20)
            avatar_button = Avatar()
            avatar_button.set_fullname(current_product.get_name())
            avatar_button.set_url(current_product.get_image_url())
            avatar_button.connect("image-selected", lambda w, url:current_product.set_image_url(url))
            avatar_button.set_halign(Gtk.Align.CENTER)
            box.pack_start(avatar_button, False, False, 0)
            product_name = Gtk.Label(current_product.get_name())
            product_name.set_halign(Gtk.Align.CENTER)
            box.pack_start(product_name, True, True, 0)
            category = current_product.get_category()
            product_dec = Gtk.Label(category.get_name())
            product_dec.set_line_wrap(True)
            product_dec.set_halign(Gtk.Align.CENTER)
            product_dec.set_size_request(80, 20)
            box.pack_start(product_dec, True, True, 0)

            box.set_vexpand(True)
            row.add(box)
            self.more_info_box.add(row)

            if current_product.get_type() == ProductType.INVENTORY:
                quantity_location_heading = Gtk.Label()
                quantity_location_heading.set_use_markup(True)
                quantity_location_heading.set_markup("<b>Quantity at Hand</b>")
                row = Gtk.ListBoxRow()
                row.add(quantity_location_heading)
                self.more_info_box.add(row)
                for location in Location.get_list(current_product.book):
                    row = Gtk.ListBoxRow()
                    box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 0)
                    box.pack_start(Gtk.Label(location.get_name()), True, True, 0)
                    uom = current_product.get_unit_of_measure()
                    box.pack_start(Gtk.Label("{:,.2f} {}".format(current_product.get_quantity_at_hand_on_date(location),
                                             uom.get_default_unit().get_name() if uom is not None else "PC")), True, True, 0)
                    row.add(box)
                    self.more_info_box.add(row)
            self.more_info_box.show_all()

    def selection_changed_cb(self, selection):
        is_readwrite = not Session.get_current_book().is_readonly()
        prod = None
        if selection is None:
            sensitive = False
        else:
            view = selection.get_tree_view()
            products = view.get_selected_products()
            try:
                GLib.idle_add(self.show_more_info, products[-1])
            except IndexError:
                pass
            sensitive = any(products)

        action_group = self.get_action_group()
        update_actions(action_group, ["ReportProductListingReport"], "sensitive", sensitive)
        update_actions(action_group, ["OTEditProductAction", "OTDeleteProductAction"], "sensitive",
                       sensitive and is_readwrite)

        self.emit("product_selected", prod)

    def create_widget(self):
        if self.widget is not None:
            return self.widget
        self.widget = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.widget.set_homogeneous(False)
        self.widget.show()
        gs_widget_set_style_context(self.widget, "ProductPage")
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scrolled_window.set_border_width(10)
        scrolled_window.show()

        search_entry = Gtk.SearchEntry()
        bar = Gtk.SearchBar.new()
        bar.connect_entry(search_entry)
        bar.add(search_entry)
        search_entry.show()
        self.searchbar = bar
        self.widget.pack_start(bar, False, True, 0)
        bar.show()
        search_entry.connect("search-changed", self.search_changed_cb)

        hbox = Gtk.Box(Gtk.Orientation.HORIZONTAL, 3)
        self.more_info_box = Gtk.ListBox()
        self.more_info_box.set_selection_mode(Gtk.SelectionMode.NONE)
        gs_widget_set_style_context(self.more_info_box, "more_info")
        hbox.pack_start(scrolled_window, True, True, 0)
        hbox.pack_start(self.more_info_box, False, True, 0)
        self.widget.pack_start(hbox, True, True, 0)
        hbox.show()
        tree_view = ProductView()
        tree_view.configure_columns()
        tree_view.set_properties(state_section=STATE_SECTION, show_column_menu=True)
        self.tree_view = tree_view
        self.tree_view.set_filter(self.filter_products)
        selection = tree_view.get_selection()
        selection.connect("changed", self.selection_changed_cb)
        tree_view.connect("button-press-event", self.button_press_cb)
        tree_view.connect("row-activated", self.double_click_cb)

        tree_view.set_headers_visible(True)
        self.selection_changed_cb(None)
        tree_view.show()
        scrolled_window.add(tree_view)
        self.component_id = ComponentManager.register("plugin-page-product", self.refresh_cb, self.close_cb)
        ComponentManager.set_session(self.component_id, Session.get_current_session())
        self.connect("inserted", self.inserted_cb)
        self.connect("inserted", lambda *args: self.window.connect("key-press-event", self.key_press_cb))
        return self.widget

    def key_press_cb(self, window, evt):
        if window.get_current_page() != self:
            return
        if evt.keyval in [Gdk.KEY_F, Gdk.KEY_f]:
            if evt.state & Gdk.ModifierType.CONTROL_MASK:
                if self.searchbar.get_search_mode():
                    self.searchbar.set_search_mode(False)
                else:
                    self.searchbar.set_search_mode(True)

    def filter_products(self, product, *_):
        if product is None:
            return True
        return self.search_text.lower() in "".join(
            [product.get_name() or "", product.get_sku() or "", product.get_description() or ""]).lower()

    def search_changed_cb(self, widget):
        self.search_text = widget.get_text()
        self.tree_view.refilter()

    def destroy_widget(self):
        self.disconnect_page_changed()
        if self.focus_source_id > 0:
            GLib.source_remove(self.focus_source_id)
        if self.widget is not None:
            self.widget.destroy()
            self.widget = None
        if self.component_id > 0:
            ComponentManager.unregister(self.component_id)
            self.component_id = 0

        Tracking.forget(self)

    def save_page(self, key_file, group_name):
        if key_file is None:
            return
        if group_name is None:
            return
        self.tree_view.save(key_file, group_name)

    @classmethod
    def recreate_page(cls, window, key_file, group_name):
        if key_file is None or group_name is None:
            return
        page = cls.new()
        window.open_page(page)
        page.tree_view.restore(key_file, group_name)
        return page

    def button_press_cb(self, widget, event):
        self.window.button_press_cb(widget, event, self)
        return False

    def double_click_cb(self, treeview, path, col):
        self.cmd_product_report(None)

    def cmd_new(self, _):
        if not any(Location.get_list(Session.get_current_book())):
            show_error(self.window, "Please add a location Business ")
            return
        d = ProductDialog(parent=self.window, book=Session.get_current_book())
        d.run()

    def cmd_edit(self, _):
        selected_products = self.tree_view.get_selected_products()
        book = Session.get_current_book()
        for product in selected_products:
            d = ProductDialog(parent=self.window, book=book, product=product)
            d.run()

    def cmd_product_report(self, action):
        pass

    def cmd_delete(self, _):
        selection = self.tree_view.get_selection()
        model, selected_paths = selection.get_selected_rows()
        if len(selected_paths) == 0:
            return
        from gasstation.views.dialogs.object_references import ObjectReferencesDialog
        to_delete = list(map(self.tree_view.get_product_from_path, selected_paths))

        if ask_yes_no(self.window, False, "Do you really want to delete this product/service?"):
            for inv in to_delete:
                lis = inv.get_referrers()
                if len(lis):
                    exp = "The list below shows objects which make use of the product which you want to delete.\n" \
                          "Before you can delete it, you must either delete those objects or else modify them so they make " \
                          "use\nof another product"
                    ObjectReferencesDialog(exp, lis)
                    continue
                inv.destroy()

    def cmd_refresh(self, action):
        self.widget.queue_draw()


GObject.type_register(ProductPluginPage)
