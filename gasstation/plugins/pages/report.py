import stat

from gasstation.utilities.custom_dialogs import choose_radio_option_dialog, ask_yes_no
from gasstation.utilities.dialog import *
from gasstation.utilities.icons import *
from gasstation.views.dialogs import File, FileDialogType
from gasstation.views.reports import *
from libgasstation.core.component import *
from libgasstation.core.options import *
from .page import *
from ...utilities.actions import update_actions, init_short_names

OPTIONS = "Options"
OPTIONS_N = "Options%d"

PLUGIN_PAGE_REPORT_NAME = "PluginPageReport"
PREFS_GROUP_REPORT_PDFEXPORT = PREFS_GROUP_GENERAL_REPORT + ".pdf-export"
PREF_FILENAME_DATE_FMT = "filename-date-format"
PREF_FILENAME_FMT = "filename-format"
toolbar_labels = [action_toolbar_labels("FilePrintAction", "Print"),
                  action_toolbar_labels("ReportExportAction", "Export"),
                  action_toolbar_labels("ReportOptionsAction", "Options"),
                  action_toolbar_labels("ReportSaveAction", "Save Config"),
                  action_toolbar_labels("ReportSaveAsAction", "Save Config As..."),
                  action_toolbar_labels("FilePrintPDFAction", "Make Pdf")]
initially_insensitive_actions = []

WINDOW_REPORT_CM_CLASS = "window-report"
static_report_printnames = {}


class ReportPluginPage(PluginPage):
    plugin_name = PLUGIN_PAGE_REPORT_NAME
    tab_icon = ICON_ACCOUNT_REPORT

    __gtype_name__ = PLUGIN_PAGE_REPORT_NAME

    def __init__(self, report_id):
        super().__init__()
        self.report_id = report_id
        self.cur_report = None
        self.edited_reports = []
        self.name_changed_id = None
        self.option_change_cb_id = 0
        self.initial_odb = None
        self.cur_odb = None
        self.widget = None
        self.html = None
        self.component_manager_id = 0
        self.reloading = False
        self.name_change_cb_id = 0
        self.initial_report = Report.find(self.report_id)
        if self.initial_report is not None:
            self.initial_report.set_needs_save(True)
        use_new = gs_pref_get_bool(PREFS_GROUP_GENERAL_REPORT, PREF_USE_NEW)
        self.set_name(self.initial_report.get_name())
        self.set_uri("default:")
        self.set_ui_description("plugin_page/report-ui.xml")
        self.set_use_new_window(use_new)
        self.add_book(Session.get_current_book())
        saved_reports_path = ""
        report_save_str = "Update the current report's saved configuration. " \
                          "The report will be saved in the file %s. " % saved_reports_path
        report_saveas_str = "Add the current report's configuration to the `Saved Report Configurations' menu. " \
                            "The report will be saved in the file %s. " % saved_reports_path
        action_group = self.create_action_group("PluginPageReportActions")

        for name, stock_id, label, accelerator, tooltip, callback in [
            ("FilePrintAction", "document-print", "_Print Report...", "<primary>p", "Print the current report",
             self.print_cb),
            ("FilePrintPDFAction", ICON_PDF_EXPORT, "Export as P_DF...", None,
             "Export the current report as a PDF document", self.export_pdf_cb),
            ("EditCutAction", "edit-cut", "Cu_t", "<primary>X", "Cut the current selection and copy it to clipboard",
             None),
            ("EditCopyAction", "edit-copy", "_Copy", "<primary>C", "Copy the current selection to clipboard",
             self.copy_cb),
            ("EditPasteAction", "edit-paste", "_Paste", "<primary>V",
             "Paste the clipboard content at the cursor position", None),
            ("ViewRefreshAction", "view-refresh", "_Refresh", "<primary>r", "Refresh this window", self.reload_cb),
            ("ReportSaveAction", "document-save", "Save _Report Configuration", "<primary><alt>s", report_save_str,
             self.save_cb
             ),
            ("ReportSaveAsAction", "document-save-as", "Save Report Configuration As...", "<primary><alt><shift>s",
             report_saveas_str, self.save_as_cb
             ),
            ("ReportExportAction", "go-next", "Export _Report", None, "Export HTML-formatted report to file",
             self.export_cb),
            ("ReportOptionsAction", "document-properties", "_Report Options", None, "Edit report options",
             self.options_cb),
            ("ReportBackAction", "go-previous", "Back", None, "Move back one step in the history", self.back_cb),
            ("ReportForwAction", "go-next", "Forward", None, "Move forward one step in the history", self.forw_cb),
            ("ReportReloadAction", "view-refresh", "Reload", None, "Reload the current page", self.reload_cb),
            ("ReportStopAction", "process-stop", "Stop", None, "Cancel outstanding HTML requests", self.stop_cb)]:
            action = Gtk.Action(name, label, tooltip, None)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                action.connect('activate', callback)
            action_group.add_action_with_accel(action, accelerator)
        update_actions(action_group, initially_insensitive_actions, "sensitive", False)
        init_short_names(action_group, toolbar_labels)

    def finish_pending(self):
        return not self.reloading

    def update_edit_menu_actions(self, hide):
        action = self.get_action("EditCopyAction")
        if action is not None:
            action.set_sensitive(True)
            action.set_visible(True)
        action = self.get_action("EditCutAction")
        if action is not None:
            action.set_sensitive(False)
            action.set_visible(not hide)
        action = self.get_action("EditPasteAction")
        if action is not None:
            action.set_sensitive(False)
            action.set_visible(not hide)

    def refresh(self):
        pass

    def create_widget(self):
        self.html = WebKitWebView()
        self.html.set_parent(self.window)
        self.widget = Gtk.Frame()
        self.widget.set_shadow_type(Gtk.ShadowType.NONE)
        gs_widget_set_style_context(self.widget, "ReportPage")
        self.widget.add(self.html.get_widget())
        self.component_manager_id = ComponentManager.register(WINDOW_REPORT_CM_CLASS, None, self.close_cb)
        ComponentManager.set_session(self.component_manager_id, Session.get_current_session())
        self.html.set_urltype_cb(lambda t: t == URL_TYPE_REPORT)
        self.html.set_load_cb(self.load_cb)
        id_name = "id=%d" % self.report_id
        child_name = self.html.build_url(URL_TYPE_REPORT, id_name, None)
        _type, url_location, url_label = self.html.parse_url(self.html, child_name)
        self.load_cb(_type, id_name, url_label)
        self.widget.connect("realize", self.realize_cb)
        # self.window.connect("page_changed", self.main_window_page_changed)
        self.widget.show_all()
        return self.widget

    def set_progressbar(self, s):
        progressbar = self.window.get_progressbar()
        allocation = progressbar.get_allocation()
        if s:
            progressbar.set_size_request(-1, allocation.height)
        else:
            progressbar.set_size_request(-1, -1)

    def load_uri(self):

        id_name = "id=%d" % self.report_id
        child_name = self.html.build_url(URL_TYPE_REPORT, id_name, None)
        _type, url_location, url_label = self.html.parse_url(self.html, child_name)
        self.widget.show_all()
        Window.set_progressbar_window(self.window)
        self.set_progressbar(True)
        print("LOADING URL ", _type, url_location, url_label, 0)
        self.html.show_url(_type, url_location, url_label, 0)
        self.set_progressbar(False)
        Window.set_progressbar_window(None)
        return False

    def load_cb(self, _type, location, *args):
        if _type == URL_TYPE_REPORT and location is not None and len(location) > 3 and "id=" == location[:3]:
            report_id = int(location[3:])
            inst_report = Report.find(report_id)
        elif _type == URL_TYPE_OPTIONS and location is not None and len(location) > 10 and "report-id=" == location[
                                                                                                           :10]:
            report_id = int(location[10:])
            inst_report = Report.find(report_id)
            if inst_report is not None:
                self.add_edited_report(inst_report)
            return
        else:
            print(" unknown URL type [%s] location [%s]" % (_type, location))
            return

        if inst_report is None:
            return
        if self.initial_report is None:
            self.initial_report = inst_report
            inst_report.set_needs_save(True)
            self.initial_odb = OptionDB(inst_report.get_options())
            self.name_change_cb_id = self.initial_odb.register_change_callback(self.refresh, "General", "Report name")

        if self.cur_report is not None and self.cur_odb is not None:
            self.cur_odb.unregister_change_callback_id(self.option_change_cb_id)
            self.cur_odb.destroy()
            self.cur_odb = None

        if self.cur_report is None:
            self.cur_report = inst_report
        self.cur_odb = OptionDB(inst_report.get_options())
        self.option_change_cb_id = self.cur_odb.register_change_callback(self.option_change_cb, None, None)

        if self.html.get_history().forward_p():
            self.set_fwd_button(True)
        else:
            self.set_fwd_button(False)

        if self.html.get_history().back_p():
            self.set_back_button(True)
        else:
            self.set_back_button(False)

    def add_edited_report(self, report):
        self.edited_reports.append(report)

    def option_change_cb(self):
        old_name = self.get_name()
        new_name = self.cur_odb.lookup_string_option("General", "Report name", None)
        if old_name != new_name:
            self.window.update_page_name(self, new_name)
        self.cur_report.set_dirty(True)
        self.reloading = True
        Window.set_progressbar_window(self.window)
        self.set_progressbar(True)
        self.html.reload(True)
        self.set_progressbar(False)
        Window.set_progressbar_window(None)
        self.reloading = False

    def name_changed(self, name):
        if name is None or self.cur_odb is None:
            return
        old_name = self.cur_odb.lookup_string_option("General", "Report name", None)
        if old_name and old_name == name:
            return
        self.cur_odb.set_option("General", "Report name", name)
        self.option_change_cb()

    def set_fwd_button(self, enabled):
        act = self.get_action("ReportForwAction")
        act.set_sensitive(enabled)

    def set_back_button(self, enabled):
        act = self.get_action("ReportBackAction")
        act.set_sensitive(enabled)

    def destroy_widget(self):
        widget = self.html.get_widget()
        self.disconnect_page_changed()
        if self.focus_source_id > 0:
            GLib.source_remove(self.focus_source_id)
        if widget is not None:
            widget.destroy()
        if self.component_manager_id > 0:
            ComponentManager.unregister(self.component_manager_id)
            self.component_manager_id = 0
        Report.remove_by_id(self.report_id)

    @classmethod
    def recreate_page(cls, window, key_file, group_name):
        text = key_file.get_value(group_name, OPTIONS)
        report = Report.deserialize(text)
        if report is None:
            return
        report_page = cls(report.get_id())
        window.open_page(report_page)
        return report_page

    def save_page(self, key_file, group_name):
        if key_file is None or group_name is None:
            return
        if self.cur_report is None:
            return
        for i, embedded in enumerate(self.cur_report.get_embedded_list()):
            tmp_report = Report.find(i)
            text = tmp_report.serialize()
            key_file.set_value(group_name, OPTIONS_N % i, text.strip())
        text = self.cur_report.serialize()
        key_file.set_value(group_name, OPTIONS, text.strip())

    def button_press_cb(self, widget, event):
        self.window.button_press_cb(widget, event, self)
        return False

    def focus_widget(self):
        widget = self.html.get_widget()
        if isinstance(widget, Gtk.Widget):
            if not widget.is_focus():
                widget.grab_focus()
        return False

    def refresh_cb(self, changes):
        self.widget.queue_draw()

    def close_cb(self):
        self.window.close_page(self)

    def realize_cb(self, *args):
        self.load_uri()

    def create_jobname(self):
        report_number = ""
        job_name = ""
        default_jobname = "GasStation-Report"
        date_format_old = date_format_get()
        format_code = gs_pref_get_string(PREFS_GROUP_REPORT_PDFEXPORT, PREF_FILENAME_DATE_FMT)
        if format_code is None or len(format_code) == 0:
            format_code = "locale"
        date_format_here = date_string_to_dateformat(format_code)
        date_format_set(date_format_here)
        job_date = print_datetime(datetime.datetime.now())
        date_format_set(date_format_old)
        if self.cur_report is None:
            report_name = default_jobname
        else:
            report_name = self.cur_odb.lookup_string_option("General", "Report name", None)
            if report_name is None:
                report_name = default_jobname
        if report_name and job_date:
            f = gs_pref_get_string(PREFS_GROUP_REPORT_PDFEXPORT, PREF_FILENAME_FMT)
            job_name = f.format(report_name, report_number, job_date)
        job_name = job_name.replace("/", "_")
        global static_report_printnames
        value = static_report_printnames.get(job_name)
        found = value is not None
        if value is None:
            value = 0
        value += 1
        static_report_printnames[job_name] = value
        if found:
            job_name += "_" + str(value)
        return job_name

    def print_cb(self, action):
        jobname = self.create_jobname()
        self.html.print(jobname)

    def export_pdf_cb(self, action):
        job_name = self.create_jobname()
        self.html.print(job_name)

    def reload_cb(self, *args):
        if self.cur_report is None:
            return
        self.cur_report.set_dirty(True)
        self.reloading = True
        Window.set_progressbar_window(self.window)
        self.set_progressbar(True)
        self.html.reload(True)
        self.set_progressbar(False)
        Window.set_progressbar_window(None)
        self.reloading = False

    def save_cb(self):
        pass

    def save_as_cb(self):
        if self.cur_report is None:
            return
        # rpt_id = self.cur_report.to_template(False)
        # if (!scm_is_null (rpt_id))
        # {
        #     GncPluginPage *reportPage = GNC_PLUGIN_PAGE (report);
        #     GtkWidget *window = reportPage->window;
        #
        #     if (window)
        #     g_return_if_fail(GNC_IS_MAIN_WINDOW(window));
        #
        #     gnc_ui_custom_report_edit_name (GNC_MAIN_WINDOW (window), rpt_id);
        # }

    def export_cb(self, action):
        export_types = self.cur_report.get_export_types()
        export_cb = self.cur_report.get_export_cb()
        if isinstance(export_types, list) and any(export_types) and callable(export_cb):
            choice = self.get_export_type_choice(export_types)
        else:
            choice = 1
        if not choice:
            return
        filepath = self.get_export_filename(choice)
        if not filepath:
            return
        try:
            if isinstance(choice, (list, tuple)):
                export_cb(self.cur_report, choice, filepath)
            else:
                self.html.export_to_file(filepath)
        except OSError as e:
            show_error(self.window, "Could not open %s.The error is: %s" % (filepath, e.strerror))

    def options_cb(self, _):
        parent = self.get_window()
        if self.cur_report is None:
            return
        if self.cur_report.edit_options(parent):
            self.add_edited_report(self.cur_report)

    @classmethod
    def open_report(cls, report_id, window):
        if window is None:
            return
        report_page = cls(report_id)
        window.open_page(report_page)

    def forw_cb(self, action):
        his = self.html.get_history()
        his.forward()
        node = his.get_current()
        if node is not None:
            self.html.show_url(node.type, node.location, node.label, 0)

    def back_cb(self, action):
        his = self.html.get_history()
        his.back()
        node = his.get_current()
        if node is not None:
            self.html.show_url(node.type, node.location, node.label, 0)

    def stop_cb(self, action):
        self.html.cancel()

    def copy_cb(self, action):
        pass

    def destroy(self):
        for edited in self.edited_reports:
            editor = edited.get_editor_widget()
            edited.set_editor_widget(None)
            if editor is not None:
                editor.destroy()
        if self.initial_odb is not None:
            self.initial_odb.unregister_change_callback_id(self.name_change_cb_id)
            self.initial_odb.destroy()
            self.initial_odb = None
        self.html.destroy()
        self.widget = None
        self.html = None
        self.cur_report = None
        self.edited_reports = None

    def get_export_type_choice(self, export_types):
        bad = False
        choices = []
        if not isinstance(export_types, list):
            return False

        for p in export_types:
            if not isinstance(p, (list, tuple)):
                bad = True
                break
            if not isinstance(p[0], str):
                bad = True
                break
            choices.insert(0, p[0])

        if not bad:
            choices.reverse()
            choices.insert(0, "HTML")
            choice = choose_radio_option_dialog(self.window, "Choose export format",
                                                "Choose the export format for this report:", None, 0, choices)
        else:
            choice = -1
        if choice < 0:
            return False

        if choice == 0:
            return True
        choice -= 1
        if choice >= len(export_types):
            return False
        return export_types[choice]

    def get_export_filename(self, choice):
        _type = "HTML"
        if isinstance(choice, list):
            _type = choice[0]
        title = "Save %s To File" % _type
        default_dir = gs_get_default_directory(PREFS_GROUP_REPORT)
        filepath = File.dialog(self.window, title, None, default_dir, FileDialogType.EXPORT)

        if filepath is not None:
            if filepath.find(".") == -1:
                filepath = filepath + "." + _type.lower()
        if filepath is None or len(filepath) == 0:
            return None
        default_dir = GLib.path_get_dirname(filepath)
        gs_set_default_directory(PREFS_GROUP_REPORT, default_dir)
        try:
            statbuf = os.stat(filepath)
            if not stat.S_ISREG(statbuf.st_mode):
                show_error(self.window, "You Cannot save to that file")
                return
            if not ask_yes_no(self.window, False, "The file %s already exists. "
                                                  "Are you sure you want to overwrite it?" % filepath):
                return
        except FileNotFoundError:
            pass

        except Exception as e:
            show_error(self.window, e.args[0])
            return None

        return filepath


GObject.type_register(ReportPluginPage)
