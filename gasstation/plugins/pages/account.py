from gasstation.utilities.actions import update_actions, init_short_names
from gasstation.utilities.cursors import gs_unset_busy_cursor, gs_set_busy_cursor
from gasstation.utilities.icons import ICON_ACCOUNT, ICON_OPEN_ACCOUNT, ICON_NEW_ACCOUNT, ICON_EDIT_ACCOUNT, \
    ICON_DELETE_ACCOUNT
from gasstation.utilities.preferences import *
from gasstation.utilities.state import *
from gasstation.views.account import AccountView
from gasstation.views.dialogs.account import AccountDialog, CascadeAccountPropertiesDialog
from gasstation.views.dialogs.account import FilterAccountDialog
from gasstation.views.main_window_summary import MainSummary
from gasstation.views.selections.account import AccountSelection
from gasstation.views.tree import DEFAULT_VISIBLE
from libgasstation import Session, Account
from libgasstation.core.component import ComponentManager
from libgasstation.core.hook import Hook, HOOK_NEW_BOOK
from libgasstation.core.scrub import Scrub
from .page import *
from .register import RegisterPluginPage

PLUGIN_PAGE_ACCOUNT_TREE_NAME = "AccountTreePluginPage"
PLUGIN_PAGE_ACCT_TREE_CM_CLASS = "plugin-page-acct-tree"
STATE_SECTION = "Account Hierarchy"

DELETE_DIALOG_FILTER = "filter"
DELETE_DIALOG_ACCOUNT = "account"
DELETE_DIALOG_TRANS_MAS = "trans_mas"
DELETE_DIALOG_SA_MAS = "sa_mas"
DELETE_DIALOG_SA_TRANS_MAS = "sa_trans_mas"
DELETE_DIALOG_SA_TRANS = "sa_trans"
DELETE_DIALOG_SA_SPLITS = "sa_has_split"
DELETE_DIALOG_OK_BUTTON = "deletebutton"

actions_requiring_account_rw = [
    "EditEditAccountAction",
    "EditDeleteAccountAction",
    "ActionsReconcileAction",
    "ActionsAutoClearAction"]
actions_requiring_account_always = [
    "FileOpenAccountAction",
    "FileOpenSubaccountsAction",
    "ActionsLotsAction"]
readonly_inactive_actions = ["FileNewAccountAction",
                             "FileAddAccountHierarchyAssistantAction",
                             "EditEditAccountAction",
                             "EditDeleteAccountAction",
                             "EditRenumberSubaccountsAction",
                             "ActionsTransferAction",
                             "ActionsReconcileAction",
                             "ActionsAutoClearAction",
                             "ActionsStockSplitAction",
                             "ScrubAction",
                             "ScrubSubAction",
                             "ScrubAllAction"]
toolbar_labels = [action_toolbar_labels("FileOpenAccountAction", "Open"),
                  action_toolbar_labels("EditEditAccountAction", "Edit"),
                  action_toolbar_labels("FileNewAccountAction", "New"),
                  action_toolbar_labels("EditDeleteAccountAction", "Delete")]

radio_entries = []


class AccountTreePluginPage(PluginPage):
    tab_icon = ICON_ACCOUNT
    plugin_name = PLUGIN_PAGE_ACCOUNT_TREE_NAME
    __gtype_name__ = "AccountTreePluginPage"
    widget = None

    def __init__(self):
        super().__init__()
        self.set_property("name", "Accounts")
        self.set_property("uri", "default:")
        self.set_property("ui-description", "plugin_page/account-tree-ui.xml")
        self.reference_ids=defaultdict(list)
        self.reference_ids[self].append(self.connect("selected", self.selected_cb))
        self.add_book(Session.get_current_book())
        action_group = self.create_action_group("AccountTreePluginPageActions")
        for name, stock_id, label, accelerator, tooltip, callback in [
            ("FakeToplevel", None, "", None, None, None),
            ("FileNewAccountAction", ICON_NEW_ACCOUNT, "New _Account...", None, "Create a new Account",
             self.cmd_new_account),
            ("FileAddAccountHierarchyAssistantAction", ICON_NEW_ACCOUNT, "New Account _Hierarchy...", None,
             "Extend the current book by merging with new account type categories", self.cmd_file_new_hierarchy),
            ("FileOpenAccountAction", ICON_OPEN_ACCOUNT, "Open Account", None, "Open the selected account",
             self.cmd_open_account),
            ("FileOpenSubaccountsAction",
             ICON_OPEN_ACCOUNT,
             "Open _SubAccounts", None,
             "Open the selected account and all its subaccounts",
             self.cmd_open_sub_accounts),
            ("EditEditAccountAction", ICON_EDIT_ACCOUNT, "Edit _Account", "<primary>e", "Edit the selected account",
             self.cmd_edit_account),
            ("EditDeleteAccountAction", ICON_DELETE_ACCOUNT, "_Delete Account...", "Delete",
             "Delete selected account",
             self.cmd_delete_account),
            ("EditColorCascadeAccountAction", None, "_Cascade Account Color...", None, "Cascade selected account color",
             self.cmd_cascade_color_account),
            ("EditFindAccountAction", "edit-find", "F_ind Account", "<primary>i", "Find an account",
             self.cmd_find_account),
            ("EditFindAccountPopupAction", "edit-find", "F_ind Account", "<primary>i", "Find an account",
             self.cmd_find_account_popup),
            ("EditRenumberSubaccountsAction", None, "_Renumber Subaccounts...", None,
             "Renumber the children of the selected account", self.cmd_renumber_accounts),
            ("ViewFilterByAction", None, "_Filter By...", None, None, self.cmd_view_filter_by),
            ("ActionsReconcileAction", None, "_Reconcile...", None, "Reconcile the selected account",
             self.cmd_reconcile),
            ("ActionsAutoClearAction", None, "_Auto-clear...", None, "Automatically clear individual transactions, "
                                                                     "given a cleared amount", self.cmd_autoclear),
            ("ActionsTransferAction", None, "_Transfer...", "<primary>t", "Transfer funds from one account to another",
             self.cmd_transfer),
            ("ActionsStockSplitAction", None, "Stoc_k Split...", None, "Record a stock split or a stock merger",
             self.cmd_stock_split),
            ("ActionsLotsAction", None, "View _Lots...", None, "Bring up the lot viewer/editor window", self.cmd_lots),
            ("ScrubAction", None, "Check & Repair A_ccount", None, "Check for and repair unbalanced transactions "
                                                                   "and orphan splits " "in this account",
             self.cmd_scrub),
            ("ScrubSubAction", None, "Check & Repair Su_baccounts", None,
             "Check for and repair unbalanced transactions and orphan splits in this account and its subaccounts",
             self.cmd_scrub_sub),
            ("ScrubAllAction", None, "Check & Repair A_ll", None,
             "Check for and repair unbalanced transactions and orphan splits in all accounts", self.cmd_scrub_all)]:
            action = Gtk.Action(name, label, tooltip, None)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                self.reference_ids[action].append(action.connect('activate', callback))
            action_group.add_action_with_accel(action, accelerator)
        init_short_names(action_group, toolbar_labels)
        page_list = Tracking.get_list(PLUGIN_PAGE_ACCOUNT_TREE_NAME)
        if len(page_list) == 1:
            self.set_user_data(self, PLUGIN_PAGE_IMMUTABLE, 1)
        self.component_id = 0
        self.tree_view = None
        self.fd = None

    @classmethod
    def new(cls):
        return GObject.new(cls)

    @staticmethod
    def open(account, win):
        plugin_page = None
        page_list = Tracking.get_list(PLUGIN_PAGE_ACCOUNT_TREE_NAME)
        if len(page_list) != 0:
            if win is not None:
                for plugin_page in page_list:
                    if plugin_page.window == win:
                        break
            else:
                plugin_page = page_list[0]
        if plugin_page is None:
            return
        window = plugin_page.window
        window.open_page(plugin_page)
        if account is not None:
            root_account = Session.get_current_root()
            parent_account = None
            temp_account = account
            plugin_page.fd.filter_override[account] = account

            while parent_account != root_account:
                parent_account = temp_account.get_parent()
                plugin_page.fd.filter_override[parent_account] = parent_account
                temp_account = parent_account
            plugin_page.tree_view.refilter()
            plugin_page.tree_view.set_selected_account(account)

    def get_current_account(self):
        account = self.tree_view.get_selected_account()
        return account

    def focus_widget(self):
        view = self.tree_view
        if not view.is_focus():
            view.grab_focus()
        return False

    def refresh_cb(self, changes):
        self.tree_view.clear_model_cache()
        self.widget.queue_draw()

    def close_cb(self):
        self.window.close_page(self)

    def editing_started_cd(self, various):
        action = self.window.find_action("EditDeleteAccountAction")
        if action is not None:
            action.set_sensitive(False)

    def editing_finished_cd(self, various):
        action = self.window.find_action("EditDeleteAccountAction")
        if action is not None:
            action.set_sensitive(True)

    def create_widget(self):
        if self.widget is not None: return self.widget
        self.widget = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.widget.set_homogeneous(False)
        self.widget.show()
        self.widget.set_name("gs-id-account-page")
        scrolled_window = Gtk.ScrolledWindow(None, None)
        scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scrolled_window.show()
        self.widget.pack_start(scrolled_window, True, True, 0)
        tree_view = AccountView(show_root=False)
        col = tree_view.find_column_by_name("description")
        tree_view.set_user_data(col, DEFAULT_VISIBLE, 1)
        col = tree_view.find_column_by_name("total")
        tree_view.set_user_data(col, DEFAULT_VISIBLE, 1)
        tree_view.configure_columns()
        tree_view.set_properties(state_section=STATE_SECTION, show_column_menu=True)
        tree_view.set_code_edited(tree_view.code_edited_cb)
        tree_view.set_desc_edited(tree_view.description_edited_cb)
        tree_view.set_notes_edited(tree_view.notes_edited_cb)
        tree_view.set_selection_mode(Gtk.SelectionMode.MULTIPLE, True)
        self.tree_view = tree_view
        selection = tree_view.get_selection()
        self.reference_ids[selection].append(selection.connect("changed", self.selection_changed_cb))
        self.reference_ids[tree_view].append(tree_view.connect("button-press-event", self.button_press_cb))
        self.reference_ids[tree_view].append(tree_view.connect("row-activated", self.double_click_cb))
        tree_view.set_headers_visible(True)
        self.selection_changed_cb(None)
        tree_view.show()
        scrolled_window.add(tree_view)
        self.fd = FilterAccountDialog(self)
        self.fd.tree_view = tree_view

        tree_view.set_filter(self.fd.filter_accounts)
        self.component_id = ComponentManager.register(PLUGIN_PAGE_ACCT_TREE_CM_CLASS, self.refresh_cb, self.close_cb)
        ComponentManager.set_session(self.component_id, Session.get_current_session())
        self.summary_bar = MainSummary.new()
        self.widget.pack_start(self.summary_bar, False, False, 0)
        self.summary_bar.show()
        self.summary_bar_position_changed(None, None)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_SUMMARY_BAR_POSITION_TOP, self.summary_bar_position_changed)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_SUMMARY_BAR_POSITION_BOTTOM,
                            self.summary_bar_position_changed)
        self.reference_ids[self].append(self.connect("inserted", self.inserted_cb))
        tree_view.restore_filter(self.fd, State.get_current(), tree_view.get_state_section())
        return self.widget

    def destroy_widget(self):
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_SUMMARY_BAR_POSITION_TOP,
                                  self.summary_bar_position_changed)
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_SUMMARY_BAR_POSITION_BOTTOM,
                                  self.summary_bar_position_changed)
        self.disconnect_page_changed()
        self.tree_view.save_filter(self.fd, State.get_current(), self.tree_view.get_state_section())

        self.fd.tree_view = None
        if self.focus_source_id > 0:
            GLib.source_remove(self.focus_source_id)
        if self.widget is not None:
            self.widget.destroy()
            self.widget = None
        if self.component_id > 0:
            ComponentManager.unregister(self.component_id)
            self.component_id = 0
        for action in self.action_group.list_actions():
            name = action.get_name()
            for ra in radio_entries:
                if name == ra[0]:
                    try:
                        action.disconnect_by_func(self.cmd_style_changed)
                    except TypeError:
                        continue
            self.action_group.remove_action(action)
        for obj, ids in self.reference_ids.items():
            for _id in ids:
                obj.disconnect(_id)
        self.reference_ids = None
        self.action_group = None
        self.data = None

    def update_inactive_actions(self):
        is_sensitive = not Session.get_current_book().is_readonly()
        action_group = self.get_action_group()
        update_actions(action_group, readonly_inactive_actions, "sensitive", is_sensitive)

    def selected_cb(self, *args):
        self.update_inactive_actions()

    def save_page(self, key_file, group_name):
        self.tree_view.save(self.fd, key_file, group_name)

    @classmethod
    def recreate_page(cls, window, key_file, group_name):
        page = cls()
        window.open_page(page)
        page.tree_view.restore(page.fd, key_file, group_name)
        return page

    def summary_bar_position_changed(self, prefs, pref):
        if self.summary_bar is None:
            return
        position = Gtk.PositionType.BOTTOM
        if gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_SUMMARY_BAR_POSITION_TOP):
            position = Gtk.PositionType.TOP
        self.widget.reorder_child(self.summary_bar, 0 if position == Gtk.PositionType.TOP else -1)

    def open_account_common(self, account, include_subs):
        if account is None:
            return
        window = self.window
        new_page = RegisterPluginPage.new(account, include_subs)
        window.open_page(new_page)

    def double_click_cb(self, treeview, path, col):
        model = treeview.get_model()
        _iter = model.get_iter(path)
        if _iter is not None:
            account = treeview.get_account_from_path(path)
            if account.get_placeholder():
                if model.iter_has_child(_iter):
                    if treeview.row_expanded(path):
                        treeview.collapse_row(path)
                    else:
                        treeview.expand_row(path, False)
            else:
                self.open_account_common(account, False)

    def selection_changed_cb(self, selection):
        is_readwrite = True
        subaccounts = False
        if selection is not None:
            view = selection.get_tree_view()
            accounts = view.get_selected_accounts()
            sensitive = accounts is not None and len(accounts)
            for account in accounts:
                if sensitive and account.get_n_children() != 0:
                    subaccounts = True
                    break
            action_group = self.get_action_group()
            update_actions(action_group, actions_requiring_account_rw, "sensitive", is_readwrite and sensitive)
            update_actions(action_group, actions_requiring_account_always, "sensitive", sensitive)
            action = action_group.get_action("EditRenumberSubaccountsAction")
            action.set_property("sensitive", is_readwrite and sensitive and subaccounts)
            action = action_group.get_action("EditColorCascadeAccountAction")
            action.set_property("sensitive", subaccounts)
            update_actions(action_group, actions_requiring_account_rw, "sensitive", is_readwrite and sensitive)
            update_actions(action_group, actions_requiring_account_always, "sensitive", sensitive)

    def cmd_new_account(self, _):
        account = self.tree_view.get_selected_account()
        parent = self.get_window()
        AccountDialog.new_(parent, Session.get_current_book(), account).run()

    def cmd_file_new_hierarchy(self, _):
        window = self.get_window()
        Hook.run(HOOK_NEW_BOOK, window)

    def cmd_open_account(self, _):
        account = self.tree_view.get_selected_account()
        path = self.tree_view.get_path_from_account(account)
        self.double_click_cb(self.tree_view, path, None)

    def cmd_open_sub_accounts(self, action):
        account = self.tree_view.get_selected_account()
        self.open_account_common(account, True)

    def cmd_edit_account(self, action):
        accounts = self.tree_view.get_selected_accounts()
        window = self.get_window()
        for account in accounts:
            if account is not None:
                AccountDialog.new_edit(window, account).run()

    def cmd_find_account(self, _):
        from gasstation.views.dialogs.account import FindAccountDialog
        window = self.get_window()
        find_window = FindAccountDialog(window, None)
        find_window.present()

    def cmd_find_account_popup(self, _):
        from gasstation.views.dialogs.account import FindAccountDialog
        account = self.tree_view.get_selected_account()
        find_window = FindAccountDialog(self.get_window(), account)
        find_window.present()

    def cmd_cascade_color_account(self, action):
        accounts = self.tree_view.get_selected_accounts()
        window = self.get_window()
        for account in accounts:
            if account is not None:
                CascadeAccountPropertiesDialog(window, account)

    @staticmethod
    def delete_account_helper(account, helper_res):
        splits = account.get_splits()
        if len(splits):
            helper_res.has_splits = True
            for s in splits:
                transaction = s.get_transaction()
                if transaction.is_readonly():
                    helper_res.has_ro_splits = True
                    break
        return helper_res.has_splits or helper_res.has_ro_splits

    def set_ok_sensitivity(self):
        sa_mas = self.get_user_data(self, DELETE_DIALOG_SA_MAS)
        trans_mas = self.get_user_data(self, DELETE_DIALOG_TRANS_MAS)
        if sa_mas:
            sa_mas_cnt = sa_mas.get_num_account()
        else:
            sa_mas_cnt = 0
        if trans_mas:
            trans_mas_cnt = trans_mas.get_num_account()
        else:
            trans_mas_cnt = 0

        sensitive = (((None is sa_mas) or
                      (not sa_mas.is_sensitive() or sa_mas_cnt)) and
                     ((None is trans_mas) or
                      (not trans_mas.is_sensitive() or trans_mas_cnt)))

        button = self.get_user_data(self, DELETE_DIALOG_OK_BUTTON)
        button.set_sensitive(sensitive)

    def populate_gas_list(self, gas, exclude_subaccounts):
        if gas is None:
            return
        account = self.get_user_data(self, DELETE_DIALOG_ACCOUNT)
        filters = self.get_user_data(self, DELETE_DIALOG_FILTER)
        gas.set_account_filters(filters, None)
        gas.purge_account(account, exclude_subaccounts)
        self.set_ok_sensitivity()

    def populate_trans_mas_list(self, sa_mrb):
        trans_mas = self.get_user_data(self, DELETE_DIALOG_TRANS_MAS)
        self.populate_gas_list(trans_mas, not sa_mrb.get_active())

    def set_insensitive_iff_rb_active(self, widget, b):
        subaccount_trans = self.get_user_data(self, DELETE_DIALOG_SA_TRANS)
        sa_mas = self.get_user_data(self, DELETE_DIALOG_SA_MAS)
        have_splits = self.get_user_data(self, DELETE_DIALOG_SA_SPLITS)
        b.set_sensitive(not widget.get_active())
        subaccount_trans.set_sensitive(have_splits and not sa_mas.is_sensitive())
        self.set_ok_sensitivity()

    def setup_account_selector(self, builder, dialog, hbox, sel_name):
        selector = AccountSelection()
        selector.show()
        box = builder.get_object(hbox)
        box.pack_start(selector, True, True, 0)
        self.set_user_data(self, sel_name, selector)
        self.populate_gas_list(selector, True)
        box.show_all()
        return selector

    def cmd_delete_account(self, action):
        class delete_helper:
            def __init__(self):
                self.has_splits = False
                self.has_ro_splits = False
                self.delete_helper_t = None

        accounts = self.tree_view.get_selected_accounts()
        for account in accounts:
            delete_res = delete_helper()
            ta = None
            saa = None
            sta = None
            lis = account.get_referrers()
            from gasstation.views.dialogs.object_references import ObjectReferencesDialog
            if len(lis):
                exp = "The list below shows objects which make use of the account which you want to delete.\n" \
                      "Before you can delete it, you must either delete those objects or else modify them so they make " \
                      "use\nof another account"
                ObjectReferencesDialog(exp, lis)
                continue
            window = self.get_window()
            acct_name = account.get_full_name()
            if not len(acct_name):
                acct_name = "(no name)"
            splits = account.get_splits()
            if any(splits) or account.get_n_children():
                builder = Gtk.Builder()
                builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/account/delete.ui",
                                                  ["account_delete_dialog"])
                dialog = builder.get_object("account_delete_dialog")
                dialog.set_transient_for(window)
                sub_account_trans = builder.get_object("subaccount_trans")
                sa_trans_ro = builder.get_object("sa_trans_ro")
                sa_mrb = builder.get_object("sa_mrb")
                self.reference_ids[sa_mrb].append(sa_mrb.connect("toggled", self.set_insensitive_iff_rb_active, sub_account_trans))
                smh = builder.get_object("sa_mas_hbox")
                sa_drb = builder.get_object("sa_drb")
                self.reference_ids[sa_drb].append(sa_drb.connect("toggled", self.set_insensitive_iff_rb_active, smh))
                tmh = builder.get_object("trans_mas_hbox")

                trans_drb = builder.get_object("trans_drb")
                self.reference_ids[trans_drb].append(trans_drb.connect("toggled", self.set_insensitive_iff_rb_active, tmh))
                stmh = builder.get_object("sa_trans_mas_hbox")

                sa_trans_drb = builder.get_object("sa_trans_drb")
                self.reference_ids[sa_trans_drb].append(sa_trans_drb.connect("toggled", self.set_insensitive_iff_rb_active, stmh))

                widget = builder.get_object("header")
                title = "Deleting account {}".format(acct_name)
                widget.set_text(title)
                widget = builder.get_object(DELETE_DIALOG_OK_BUTTON)
                self.set_user_data(self, DELETE_DIALOG_OK_BUTTON, widget)
                self.set_user_data(self, DELETE_DIALOG_FILTER, [account.get_type()])
                self.set_user_data(self, DELETE_DIALOG_ACCOUNT, account)
                trans_mas = self.setup_account_selector(builder, dialog, "trans_mas_hbox", DELETE_DIALOG_TRANS_MAS)
                if any(splits):
                    delete_res2 = delete_helper()
                    self.delete_account_helper(account, delete_res2)
                    if delete_res2.has_ro_splits:
                        builder.get_object("trans_rw").hide()
                        widget = builder.get_object("trans_drb")
                        widget.set_sensitive(False)
                    else:
                        builder.get_object("trans_ro").hide()
                else:
                    builder.get_object("transactions").set_sensitive(False)
                    builder.get_object("trans_ro").hide()
                sa_trans_mas = self.setup_account_selector(builder, dialog, "sa_trans_mas_hbox",
                                                           DELETE_DIALOG_SA_TRANS_MAS)
                sa_mas = self.setup_account_selector(builder, dialog, "sa_mas_hbox", DELETE_DIALOG_SA_MAS)
                self.set_user_data(self, DELETE_DIALOG_SA_TRANS, sub_account_trans)
                if account.get_n_children() > 0:
                    account.foreach_descendant_until(self.delete_account_helper, delete_res)
                    if delete_res.has_splits:
                        if delete_res.has_ro_splits:
                            builder.get_object("sa_trans_rw").hide()
                            widget = builder.get_object("sa_trans_drb")
                            widget.set_sensitive(False)
                        else:
                            sa_trans_ro.hide()
                        self.set_user_data(self, DELETE_DIALOG_SA_SPLITS, 1)
                        sub_account_trans.set_sensitive(True)
                    else:
                        self.set_user_data(self, DELETE_DIALOG_SA_SPLITS, 0)
                        sub_account_trans.set_sensitive(False)
                        sa_trans_ro.hide()
                else:
                    builder.get_object("subaccounts").set_sensitive(False)
                    sub_account_trans.set_sensitive(False)
                    sa_trans_ro.hide()
                dialog.set_default_response(Gtk.ResponseType.CANCEL)
                builder.connect_signals(self)
                response = dialog.run()
                if Gtk.ResponseType.ACCEPT != response:
                    dialog.destroy()
                    continue
                if trans_mas and trans_mas.is_sensitive():
                    ta = trans_mas.get_account()
                if sa_mas and sa_mas.is_sensitive():
                    saa = sa_mas.get_account()
                if sa_trans_mas and sa_trans_mas.is_sensitive():
                    sta = sa_trans_mas.get_account()
                dialog.destroy()

            lines = ["The account {} will be deleted.".format(acct_name)]
            if any(splits):
                if ta is not None:
                    name = ta.get_full_name()
                    lines.append("All transactions in this account will be moved to the account {}.".format(name))
                else:
                    lines.append("All transactions in this account will be deleted.")
            if account.get_n_children() > 0:
                if saa is not None:
                    name = saa.get_full_name()
                    lines.append("All of its sub-accounts will be moved to the account {}.".format(name))
                else:
                    lines.append("All of its subaccounts will be deleted.")
                    if sta is not None:
                        name = sta.get_full_name()
                        lines.append("All sub-account transactions will be moved to the account {}.".format(name))

                    elif delete_res.has_splits:
                        lines.append("All sub-account transactions will be deleted.")

            message = " ".join(lines)

            dialog = Gtk.MessageDialog(transient_for=window, modal=True, destroy_with_parent=True,
                                       message_type=Gtk.MessageType.QUESTION,
                                       buttons=Gtk.ButtonsType.NONE,
                                       text=message)
            dialog.add_buttons("_Cancel", Gtk.ResponseType.CANCEL, "_Delete", Gtk.ResponseType.ACCEPT)
            dialog.set_default_response(Gtk.ResponseType.CANCEL)
            response = dialog.run()
            dialog.destroy()
            if Gtk.ResponseType.ACCEPT == response:
                gs_set_busy_cursor(None, True)
                ComponentManager.suspend()
                account.begin_edit()
                if saa is not None:
                    be = saa.book.get_backend()
                    if be is not None:
                        with be.add_lock():
                            saa.begin_edit()
                            acct_list = account.get_children()
                            for acct in acct_list:
                                saa.append_child(acct)
                            saa.commit_edit()
                elif sta is not None:
                    account.foreach_descendant(Account.move_all_splits, sta)
                if ta is not None:
                    account.move_all_splits(ta)
                account.commit_edit()
                for acc in account.children:
                    guid = str(acc.guid)
                    State.drop_sections_for(guid)
                guid = str(account.guid)
                State.drop_sections_for(guid)
                account.begin_edit()
                account.destroy()
                ComponentManager.resume()
                gs_unset_busy_cursor(None)

    def cmd_renumber_accounts(self, _):
        from gasstation.views.dialogs.account import RenumberAccountDialog
        window = self.get_window()
        accounts = self.tree_view.get_selected_accounts()
        if not window or accounts is None:
            return
        for account in accounts:
            RenumberAccountDialog(window, account)

    def cmd_view_filter_by(self, _):
        self.fd.run()

    def cmd_reconcile(self, _):
        from gasstation.views.dialogs.reconcile import RecnWindow
        account = self.tree_view.get_selected_account()
        if account is None:
            return
        window = self.window
        rw = RecnWindow.new(window, account)
        rw._raise()

    def cmd_autoclear(self, action):
        account = self.tree_view.get_selected_account()
        if account is None:
            return
        window = self.window
        # autoClearData = autoClearWindow(window, account)
        # gnc_ui_autoclear_window_raise(autoClearData)

    def cmd_transfer(self, _):
        accounts = self.tree_view.get_selected_accounts()
        window = self.window
        from gasstation.views.dialogs import TransferDialog
        for account in accounts:
            TransferDialog(window, account)

    def cmd_stock_split(self, _):
        from gasstation.views.dialogs.stock_split import StockSplitAssistant
        accounts = self.tree_view.get_selected_accounts()
        window = self.window
        for account in accounts:
            ss = StockSplitAssistant(window, account)
            ss.present()

    def cmd_lots(self, _):
        from gasstation.views.dialogs.lots import LotViewer
        account = self.tree_view.get_selected_account()
        window = self.window
        LotViewer(window, account)

    def cmd_scrub(self, action):
        accounts = self.tree_view.get_selected_accounts()
        if accounts is None:
            return
        gs_set_busy_cursor(self.window, True)
        ComponentManager.suspend()
        window = self.window
        window.set_progressbar_window(window)
        for account in accounts:
            Scrub.account_orphans(account, window.show_progress)
            Scrub.account_imbalance(account, window.show_progress)
        ComponentManager.resume()
        gs_unset_busy_cursor(self.window)

    def cmd_scrub_sub(self, action):
        account = self.tree_view.get_selected_account()
        ComponentManager.suspend()
        window = self.window
        if account is None:
            return
        gs_set_busy_cursor(self.window, True)
        window.set_progressbar_window(window)
        Scrub.account_tree_orphans(account, window.show_progress)
        Scrub.account_tree_imbalance(account, window.show_progress)
        ComponentManager.resume()
        gs_unset_busy_cursor(self.window)

    def cmd_scrub_all(self, action):
        root = Session.get_current_root()
        window = self.window
        window.set_progressbar_window(window)
        gs_set_busy_cursor(self.window, True)
        ComponentManager.suspend()
        Scrub.account_tree_orphans(root, window.show_progress)
        Scrub.account_tree_imbalance(root, window.show_progress)
        ComponentManager.resume()
        gs_unset_busy_cursor(self.window)

    def button_press_cb(self, widget, event):
        self.window.button_press_cb(widget, event, self)
        return False


GObject.type_register(AccountTreePluginPage)
