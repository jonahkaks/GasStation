from gasstation.models.scheduled_dense_cal import ScheduledDenseCal
from gasstation.models.scheduled_instance_model import *
from gasstation.utilities.icons import *
from gasstation.views.dialogs.scheduled.editor import *
from gasstation.views.scheduled_list import ScheduledListView
from libgasstation.core.scheduled_transaction import *
from .page import *
PLUGIN_PAGE_SX_LIST_CM_CLASS = "plugin-page-scheduled-list"
STATE_SECTION = "SX Transaction List"
PLUGIN_PAGE_SX_LIST_NAME = "ScheduledTransactionListPluginPage"


class ScheduledTransactionListPluginPage(PluginPage):
    __gtype_name__ = "ScheduledTransactionListPluginPage"
    tab_icon = ICON_ACCOUNT
    plugin_name = PLUGIN_PAGE_SX_LIST_NAME

    def __init__(self):
        super().__init__()
        self.disposed = None
        self.widget = None
        self.component_id = 0
        self.dense_cal_model = None
        self.gdcal = None
        self.instances = None
        self.tree_view = None
        self.set_property("name", "Scheduled Transactions")
        self.set_property("uri", "default:")
        self.set_property("ui-description", "scheduled-list-page-ui.xml")
        self.add_book(Session.get_current_book())
        action_group = self.create_action_group("ScheduledTransactionListPluginPageActions")

        for name, stock_id, label, accelerator, tooltip, callback in [
            ("ScheduledListAction", None, "_Scheduled", None, None, None),
            (
                    "ScheduledListNewAction", ICON_NEW_ACCOUNT, "_New", None,
                    "Create a new scheduled transaction", self.cmd_new
            ),
            (
                    "ScheduledListEditAction", ICON_EDIT_ACCOUNT, "_Edit", None,
                    "Edit the selected scheduled transaction", self.cmd_edit
            ),
            (
                    "ScheduledListDeleteAction", ICON_DELETE_ACCOUNT, "_Delete", None,
                    "Delete the selected scheduled transaction", self.cmd_delete
            ),

            (
                    "ViewRefreshAction", "view-refresh", "_Refresh", "<primary>r",
                    "Refresh this window", self.cmd_refresh
            )]:
            action = Gtk.Action(name, label, tooltip, None)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                action.connect('activate', callback)
            action_group.add_action_with_accel(action, accelerator)

    @classmethod
    def new(cls):
        obj = Tracking.get_list(PLUGIN_PAGE_SX_LIST_NAME)
        if obj is not None and len(obj):
            plugin_page = obj[0]
        else:
            plugin_page = cls()
        return plugin_page

    def focus_widget(self):
        tree_view = self.tree_view
        if isinstance(tree_view, Gtk.TreeView):
            if not tree_view.is_focus():
                tree_view.grab_focus()
        return False

    def refresh_cb(self, changes):
        if changes is not None:
            return
        self.widget.queue_draw()

    def close_cb(self):
        self.window.close_page(self)

    def selection_changed_cb(self, selection):
        edit_action = self.get_action("ScheduledListEditAction")
        delete_action = self.get_action("ScheduledListDeleteAction")
        selection_state = False if selection.count_selected_rows() == 0 else True
        edit_action.set_sensitive(selection_state)
        delete_action.set_sensitive(selection_state)

    def create_widget(self):
        if self.widget is not None:
            return self.widget
        widget = Gtk.Paned.new(Gtk.Orientation.VERTICAL)
        self.widget = widget
        self.widget.show()
        gs_widget_set_style_context(self.widget, "ScheduledPage")
        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        vbox.set_homogeneous(False)
        widget.pack1(vbox, True, False)
        label = Gtk.Label()
        label.set_markup("<b>Transactions</b>")
        gs_label_set_alignment(label, 0.0, 0)
        label.show()
        vbox.pack_start(label, False, False, 0)
        vbox.show()
        swin = Gtk.ScrolledWindow()
        swin.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        vbox.pack_start(swin, True, True, 5)
        swin.show()
        self.widget.set_position(160)
        end = datetime.date.today() + relativedelta(years=1)
        self.instances = ScheduledInstanceModel.get_instances(end, True)
        edit_action = self.get_action("ScheduledListEditAction")
        delete_action = self.get_action("ScheduledListDeleteAction")
        edit_action.set_sensitive(False)
        delete_action.set_sensitive(False)
        self.tree_view = ScheduledListView(self.instances)
        self.tree_view.set_properties(state_section=STATE_SECTION, show_column_menu=True)
        swin.add(self.tree_view)

        selection = self.tree_view.get_selection()
        selection.set_mode(Gtk.SelectionMode.MULTIPLE)
        selection.connect("changed", self.selection_changed_cb)
        self.tree_view.connect("row-activated", self.row_activated_cb)

        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        vbox.set_homogeneous(False)
        widget.pack2(vbox, True, False)

        label = Gtk.Label()
        label.set_markup("<b>Upcoming Transactions</b>")
        gs_label_set_alignment(label, 0.0, 0)
        label.show()
        vbox.pack_start(label, False, False, 0)
        vbox.show()

        swin = Gtk.ScrolledWindow()
        swin.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        vbox.pack_start(swin, True, True, 5)
        swin.show()
        self.dense_cal_model = ScheduledDenseCal.new(self.instances)
        self.gdcal = DenseCalendar.new_with_model(self.dense_cal_model)
        self.gdcal.set_months_per_col(4)
        self.gdcal.set_num_months(12)
        self.gdcal.show()
        swin.add(self.gdcal)
        self.component_id = ComponentManager.register("plugin-page-scheduled-list", self.refresh_cb, self.close_cb)
        ComponentManager.set_session(self.component_id, Session.get_current_session())
        self.connect("inserted", self.inserted_cb)
        return self.widget

    def destroy_widget(self):
        self.disconnect_page_changed()
        if self.focus_source_id > 0:
            GLib.source_remove(self.focus_source_id)
        if self.widget is not None:
            self.widget.destroy()
            self.widget = None
        if self.component_id > 0:
            ComponentManager.unregister(self.component_id)
            self.component_id = 0

    def save_page(self, key_file, group_name):
        if key_file is None:
            return
        if group_name is None:
            return
        key_file.set_integer(group_name, "dense_cal_num_months", self.gdcal.get_num_months())
        key_file.set_integer(group_name, "paned_position", self.widget.get_position())

    @classmethod
    def recreate_page(cls, window, key_file, group_name):
        if key_file is None or group_name is None:
            return
        page = cls.new()
        window.open_page(page)
        try:
            i = key_file.get_integer(group_name, "dense_cal_num_months")
            page.gdcal.set_num_months(i)
        except GLib.Error:
            pass
        try:
            i = key_file.get_integer(group_name, "paned_position")
            page.widget.set_position(i)
        except GLib.Error:
            pass
        return page

    def cmd_new(self, action):
        window = self.get_window()
        ScheduledTransactionEditorDialog(window)

    def cmd_edit(self, action):
        window = self.get_window()
        selection = self.tree_view.get_selection()
        model, selected_paths = selection.get_selected_rows()
        if len(selected_paths) == 0:
            return
        to_edit = list(map(self.tree_view.get_scheduled_from_path, selected_paths))
        for scheduled in to_edit:
            ScheduledTransactionEditorDialog(window, scheduled)

    def row_activated_cb(self, tree_view, path, column):
        window = self.get_window()
        scheduled = self.tree_view.get_scheduled_from_path(path)
        ScheduledTransactionEditorDialog(window, scheduled)

    def _destroy_scheduled(self, scheduled):
        book = Session.get_current_book()
        schedules = ScheduledTransactionList(book)
        schedules.remove(scheduled)
        scheduled.begin_edit()
        scheduled.destroy()

    def cmd_delete(self, action):
        window = self.get_window()
        selection = self.tree_view.get_selection()
        model, selected_paths = selection.get_selected_rows()
        if len(selected_paths) == 0:
            return
        to_delete = list(map(self.tree_view.get_scheduled_from_path, selected_paths))

        if ask_ok_cancel(window, False, "Do you really want to delete this scheduled transaction?"):
            for scheduled in to_delete:
                self._destroy_scheduled(scheduled)

    def cmd_refresh(self, action):
        self.widget.queue_draw()


GObject.type_register(ScheduledTransactionListPluginPage)
