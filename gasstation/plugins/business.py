from gasstation.plugins.pages import RegisterPluginPage, PluginPage, StakeHolderPluginPage, FuelPluginPage, \
    ProductPluginPage
from gasstation.utilities.actions import update_actions
from gasstation.utilities.icons import ICON_INVOICE_NEW
from gasstation.utilities.preferences import *
from libgasstation.core.session import Session
from libgasstation.core.transaction import TransactionType
from .plugin import Plugin

PLUGIN_ACTIONS_NAME = "plugin/business-actions"
PLUGIN_UI_FILENAME = "plugin/business-ui.xml"

PREFS_GROUP_INVOICE = "dialogs.business.invoice"
PREFS_GROUP_BILL = "dialogs.business.bill"
PREFS_GROUP_VOUCHER = "dialogs.business.voucher"

PREF_AUTO_PAY = "auto-pay"

PREF_EXTRA_TOOLBUTTONS = "enable-toolbuttons"
PREF_INV_PRINT_RPT = "invoice-printreport"
PLUGIN_BUSINESS_NAME = "plugin/business"

register_transaction_actions = ["RegisterAssignPayment"]
register_bus_transaction_actions = ["RegisterEditPayment"]
readonly_inactive_actions = ["CustomerNewCustomerOpenAction", "CustomerNewInvoiceOpenAction",
                             "CustomerNewInvoiceOpenAction", "CustomerProcessPaymentAction",
                             "SupplierNewSupplierOpenAction", "SupplierNewBillOpenAction",
                             "SupplierProcessPaymentAction", "StaffNewStaffOpenAction",
                             "StaffNewExpenseVoucherOpenAction", "StaffProcessPaymentAction",
                             "ToolbarNewInvoiceAction", "RegisterAssignPayment", "RegisterEditPayment"]


class BusinessPlugin(Plugin):
    plugin_name = PLUGIN_BUSINESS_NAME
    actions_name = PLUGIN_ACTIONS_NAME
    ui_filename = PLUGIN_UI_FILENAME
    __gtype_name__ = "BusinessPlugin"

    def __init__(self):
        super().__init__()
        self.actions = [
            ("BusinessAction", None, "_Business", None, None, None),
            ("CustomerMenuAction", None, "_Customer", None, None, None),
            (
                "CustomerOverviewPageAction", None, "Customers Overview", None,
                "Open a Customer overview page",
                self.cmd_customer_page
            ),
            (
                "CustomerNewCustomerOpenAction", None, "_New Customer...", None,
                "Open the New Customer dialog",
                self.cmd_customer_new_customer
            ),
            (
                "CustomerNewInvoiceOpenAction", None, "New _Invoice...", None,
                "Open the New Invoice dialog",
                self.cmd_customer_new_invoice
            ),
            (
                "CustomerFindInvoiceOpenAction", None, "Find In_voice...", None,
                "Open the Find Invoice dialog",
                self.cmd_customer_find_invoice
            ),
            (
                "CustomerProcessPaymentAction", None, "_Process Payment...", None,
                "Open the Process Payment dialog",
                self.cmd_customer_process_payment
            ),
            (
                "SupplierOverviewPageAction", None, "Suppliers Overview", None,
                "Open a Supplier overview page",
                self.cmd_supplier_page
            ),
            ("SupplierMenuAction", None, "_Supplier", None, None, None),
            (
                "SupplierNewSupplierOpenAction", None, "_New Supplier...", None,
                "Open the New Supplier dialog",
                self.cmd_supplier_new_supplier
            ),
            (
                "SupplierNewBillOpenAction", None, "New _Bill...", None,
                "Open the New Bill dialog",
                self.cmd_supplier_new_bill
            ),
            (
                "SupplierFindBillOpenAction", None, "Find Bi_ll...", None,
                "Open the Find Bill dialog",
                self.cmd_supplier_find_bill
            ),
            (
                "SupplierProcessPaymentAction", None, "_Process Payment...", None,
                "Open the Process Payment dialog",
                self.cmd_supplier_process_payment
            ),

            (
                "StaffOverviewPageAction", None, "Staff Overview", None,
                "Open a Staff overview page",
                self.cmd_employee_page
            ),
            ("StaffMenuAction", None, "_Staff", None, None, None),
            (
                "StaffNewStaffOpenAction", None, "_New Staff...", None,
                "Open the New Staff dialog",
                self.cmd_employee_new_employee
            ),
            (
                "StaffNewExpenseVoucherOpenAction", None, "New _Expense Voucher...", None,
                "Open the New Expense Voucher dialog",
                self.cmd_employee_new_expense_voucher
            ),
            (
                "StaffFindExpenseVoucherOpenAction", None, "Find Expense _Voucher...", None,
                "Open the Find Expense Voucher dialog",
                self.cmd_employee_find_expense_voucher
            ),
            (
                "StaffProcessPaymentAction", None, "_Process Payment...", None,
                "Open the Process Payment dialog",
                self.cmd_employee_process_payment
            ),
            (
                "ProductOverviewPageAction", None, "Products Overview", None,
                "Open a Product overview page",
                self.cmd_product_page
            ),
            ("ProductMenuAction", None, "_Inventory", None, None, None),
            (
                "ProductNewProductOpenAction", None, "_New Product...", None,
                "Open the New Product dialog",
                self.cmd_product_new_product
            ),
            (
                "ProductNewProductCategoryOpenAction", None, "_Categories", None,
                "Open the New ProductCategory dialog",
                self.cmd_product_categories
            ),
            (
                "ProductNewUnitOfMeasureOpenAction", None, "_Units", None,
                "Open the New UnitOfMeasure dialog",
                self.cmd_unit_of_measures
            ),
            (
                "ProductNewUnitOfMeasureGroupOpenAction", None, "_Unit Groups", None,
                "Open the New UnitOfMeasureGroup dialog",
                self.cmd_unit_of_measure_groups
            ),
            (
                "TaxsOpenAction", None, "Sales _Tax", None,
                "View and edit the list of Sales Taxs (GST/VAT",
                self.cmd_taxes
            ),

            (
                "PaymentTermsOpenAction", None, "_Payment Terms", None,
                "View and edit the list of Terms",
                self.cmd_payment_terms
            ),
            ("PriceRuleOpenAction", None, "_Price Rules", None, "View and edit price rules", self.cmd_price_rules),
            (
                "FuelOpenAction", None, "_Fuel Sales Register", None,
                "View and edit the list of Fuel Invoices",
                self.cmd_fuel
            ),
            (
                "TanksOpenAction", None, "_Tanks Setup", None,
                "View and edit the list of Fuel Tanks",
                self.cmd_fuel_tank
            ),

            (
                "PumpsOpenAction", None, "_Pumps Setup", None,
                "View and edit the list of Fuel Pumps",
                self.cmd_fuel_pump
            ),
            (
                "NozzlesOpenAction", None, "_Nozzles Setup", None,
                "View and edit the list of Fuel Nozzles",
                self.cmd_fuel_nozzle
            ),

            (
                "BillsDueReminderOpenAction", None, "Bills _Due Reminder", None,
                "Open the Bills Due Reminder dialog",
                self.cmd_bills_due_reminder
            ),
            (
                "InvoicesDueReminderOpenAction", None, "Invoices _Due Reminder", None,
                "Open the Invoices Due Reminder dialog",
                self.cmd_invoices_due_reminder
            ),
            ("ExportMenuAction", None, "E_xport", None, None, None),

            ("FuelMenuAction", None, "_Fuel", None, None, None),
            ("LocationMenuAction", None, "_Location", None, None, None),

            (
                "ToolbarNewInvoiceAction", ICON_INVOICE_NEW, "New _Invoice...", None,
                "Open the New Invoice dialog",
                self.cmd_customer_new_invoice
            ),

            (
                "RegisterAssignPayment", None, "Assign as payment...", None,
                "Assign the selected transaction as payment",
                self.cmd_assign_payment
            ),
            (
                "RegisterEditPayment", None, "Edit payment...", None,
                "Edit the payment this transaction is a part of",
                self.cmd_assign_payment
            ),
            (
                "LocationOpenAction", None, "Locations Overview...", None,
                "List of locations",
                self.cmd_locations
            )
        ]

    def add_to_window(self, window, _type, **kwargs):
        self.bind_toolbuttons_visibility(window)
        window.connect("page_changed", self.main_window_page_changed)

    @staticmethod
    def page_changed(window, plugin_page):
        if plugin_page is None:
            return
        if window.get_current_page() == plugin_page:
            if not isinstance(plugin_page, (FuelPluginPage, ProductPluginPage)):
                return
            GLib.idle_add(plugin_page.focus)

    @staticmethod
    def cmd_product_page(action, data):
        page = ProductPluginPage.new()
        data.window.open_page(page)

    @staticmethod
    def cmd_product_new_product(action, data):
        from gasstation.views.dialogs.product import ProductDialog
        d = ProductDialog(parent=data.window, book=Session.get_current_book())
        d.run()

    def cmd_product_categories(self, action, data):
        from gasstation.views.dialogs.product.product_categories import ProductCategoriesDialog
        ProductCategoriesDialog.new(data.window)

    def cmd_unit_of_measures(self, action, data):
        from gasstation.views.dialogs.product.unit_of_measures import UnitOfMeasuresDialog
        UnitOfMeasuresDialog.new(data.window)

    def cmd_unit_of_measure_groups(self, action, data):
        from gasstation.views.dialogs.product.unit_of_measure_groups import UnitOfMeasureGroupsDialog
        UnitOfMeasureGroupsDialog.new(data.window)

    def cmd_customer_page(self, _, data):
        page = StakeHolderPluginPage.new("Customer")
        data.window.open_page(page)

    def cmd_customer_new_customer(self, _, data):
        from gasstation.views.dialogs.customer import CustomerDialog
        dialog = CustomerDialog(data.window, Session.get_current_book())
        dialog.run()

    def cmd_customer_find_customer(self, action, data):
        pass

    def cmd_customer_new_invoice(self, action, data):
        last_window = data.window
        # gnc_ui_invoice_new (data.window, self.last_customer, Session.get_current_book())

    def cmd_customer_find_invoice(self, action, data):
        last_window = data.window
        # gnc_invoice_search (data.window, None, self.last_customer, Session.get_current_book())

    def cmd_customer_process_payment(self, action, data):
        # gnc_ui_payment_new (data.window, self.last_customer, Session.get_current_book())
        pass

    def cmd_supplier_page(self, action, data):
        page = StakeHolderPluginPage.new("Supplier")
        data.window.open_page(page)

    def cmd_supplier_new_supplier(self, action, data):
        from gasstation.views.dialogs.supplier import SupplierDialog
        SupplierDialog(data.window, Session.get_current_book()).run()

    def cmd_supplier_find_supplier(self, action, data):
        # supplier = gncPersonGetSupplier (self.last_supplier)
        # gnc_supplier_search (data.window, supplier, Session.get_current_book())
        pass

    def cmd_supplier_new_bill(self, action, data):
        last_window = data.window
        # gnc_ui_invoice_new (data.window, self.last_supplier, Session.get_current_book())

    def cmd_supplier_find_bill(self, action, data):
        last_window = data.window
        # gnc_invoice_search (data.window, None, self.last_supplier, Session.get_current_book())

    def cmd_supplier_new_job(self, action, data):
        # gnc_ui_job_new (data.window, self.last_supplier, Session.get_current_book())
        pass

    def cmd_supplier_find_job(self, action, data):
        # gnc_job_search (data.window, None, self.last_supplier, Session.get_current_book())
        pass

    def cmd_supplier_process_payment(self, action, data):
        # gnc_ui_payment_new (data.window, self.last_supplier, Session.get_current_book())
        pass

    def cmd_employee_page(self, action, data):
        page = StakeHolderPluginPage.new("Staff")
        data.window.open_page(page)

    def cmd_employee_new_employee(self, action, data):
        # gnc_ui_employee_new (data.window, Session.get_current_book())
        pass

    def cmd_employee_find_employee(self, action, data):
        # employee = gncPersonGetStaff (self.last_employee)
        # gnc_employee_search (data.window, employee, Session.get_current_book())
        pass

    def cmd_employee_new_expense_voucher(self, action, data):
        last_window = data.window
        # gnc_ui_invoice_new (data.window, self.last_employee, Session.get_current_book())

    def cmd_employee_find_expense_voucher(self, action, data):
        last_window = data.window
        # gnc_invoice_search (data.window, None, self.last_employee, Session.get_current_book())

    def cmd_employee_process_payment(self, action, data):
        # gnc_ui_payment_new (data.window, self.last_employee, Session.get_current_book())
        pass

    def cmd_taxes(self, action, data):
        from gasstation.views.dialogs.tax import TaxDialog
        TaxDialog(data.window, Session.get_current_book()).run()

    def cmd_price_rules(self, action, data):
        pass

    def cmd_locations(self, action, data):
        from gasstation.views.dialogs.location.locations import LocationsDialog
        LocationsDialog.new(data.window)

    def cmd_fuel_tank(self, action, data):
        from gasstation.views.dialogs.tank import TanksDialog
        TanksDialog.new(data.window)

    def cmd_fuel(self, action, data):
        from gasstation.plugins.pages.fuel import FuelPluginPage
        page = FuelPluginPage()
        data.window.open_page(page)

    def cmd_fuel_pump(self, action, data):
        from gasstation.views.dialogs.pump import PumpsDialog
        PumpsDialog.new(data.window)

    def cmd_fuel_nozzle(self, action, data):
        from gasstation.views.dialogs.nozzle import NozzlesDialog
        NozzlesDialog.new(data.window)

    def cmd_payment_terms(self, action, data):
        from gasstation.views.dialogs.term.window import PaymentTermsDialog
        PaymentTermsDialog.new(data.window, Session.get_current_book())

    def cmd_bills_due_reminder(self, action, data):
        # gnc_invoice_remind_bills_due (data.window)
        pass

    def cmd_invoices_due_reminder(self, action, data):
        # gnc_invoice_remind_invoices_due (data.window)
        pass

    @staticmethod
    def assign_payment(parent, trans, person):
        if len(trans) <= 1:
            return
        # gnc_ui_payment_new_with_transaction(parent, person, trans)

    def cmd_assign_payment(self, action, data):
        plugin_page = data.window.get_current_page()
        if not isinstance(plugin_page, RegisterPluginPage):
            return
        gsr = plugin_page.get_gsr()
        if gsr is None:
            return
        reg = gsr.ledger.get_split_register()
        if reg is None:
            return
        split = reg.get_current_split()
        if split is None:
            return
        trans = split.get_transaction()
        if trans is None:
            return
        plugin_business = data.data
        # have_person = gncPersonGetPersonFromTxn (trans, &person)
        # if (have_person)
        #     person_p = &person
        # else if (gnc_ui_payment_is_customer_payment(trans))
        # person_p = plugin_business_self.last_customer
        # else
        # person_p = plugin_business_self.last_supplier
        #
        # gnc_business_assign_payment (data.window,
        # trans, person_p)

    @classmethod
    def update_menus(cls, plugin_page):
        if plugin_page is None:
            return
        is_bus_transaction = False
        is_bus_doc = False
        is_transaction_register = isinstance(plugin_page, RegisterPluginPage)
        window = plugin_page.window
        if window is None:
            return
        action_group = window.get_action_group(PLUGIN_ACTIONS_NAME)
        if is_transaction_register:
            trans = plugin_page.get_current_transaction()
            if trans and len(trans) > 0:
                is_bus_transaction = trans.get_first_payable_receivable_split(True) is not None
                is_bus_doc = trans.get_type() == TransactionType.INVOICE
        update_actions(action_group, register_transaction_actions,
                       "sensitive", is_transaction_register and not is_bus_transaction and not is_bus_doc)
        update_actions(action_group, register_transaction_actions,
                       "visible", is_transaction_register and not is_bus_transaction and not is_bus_doc)
        update_actions(action_group, register_bus_transaction_actions,
                       "sensitive", is_transaction_register and is_bus_transaction and not is_bus_doc)
        update_actions(action_group, register_bus_transaction_actions,
                       "visible", is_transaction_register and is_bus_transaction and not is_bus_doc)

    @classmethod
    def main_window_page_changed(cls, window, page, *user_data):
        cls.update_menus(page)
        cls.update_inactive_actions(page)

    @classmethod
    def split_reg_ui_update(cls, plugin_page):
        cls.main_window_page_changed(None, plugin_page)

    @staticmethod
    def update_inactive_actions(plugin_page):
        if not Session.current_session_exist():
            return
        book = Session.get_current_book()
        is_readwrite = not (True if book is None else book.is_readonly())
        if not isinstance(plugin_page, PluginPage):
            return
        window = plugin_page.window
        action_group = window.get_action_group(PLUGIN_ACTIONS_NAME)
        update_actions(action_group, readonly_inactive_actions, "sensitive", is_readwrite)

    @staticmethod
    def bind_toolbuttons_visibility(mainwindow):
        action_group = mainwindow.get_action_group(PLUGIN_ACTIONS_NAME)
        action = action_group.get_action("ToolbarNewInvoiceAction")
        gs_pref_bind(PREFS_GROUP_INVOICE, PREF_EXTRA_TOOLBUTTONS, action, "visible")

    @staticmethod
    def get_invoice_printreport():
        invoice_printreport_values = ["5123a759ceb9483abf2182d01c140e8d",
                                      "0769e242be474010b4acf264a5512e6e",
                                      "67112f318bef4fc496bdc27d106bbda4",
                                      "3ce293441e894423a2425d7a22dd1ac6"]
        value = gs_pref_get_int(PREFS_GROUP_INVOICE, PREF_INV_PRINT_RPT)
        if 0 <= value < 4:
            return invoice_printreport_values[value]
        else:
            return None
