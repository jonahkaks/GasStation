from gi.repository import Gtk, GObject

from gasstation.utilities.actions import update_actions
from gasstation.utilities.custom_dialogs import show_info
from gasstation.views.dialogs.book_close import CloseBookWindow
from gasstation.views.dialogs.setup import FileAccessType, FileAccessDialog, File
from libgasstation import Session
from .plugin import Plugin
from ..utilities.cursors import gs_set_busy_cursor, gs_unset_busy_cursor

PLUGIN_ACTIONS_NAME = "plugin/basic-commands-actions"
PLUGIN_UI_FILENAME = "plugin/basic-commands-ui.xml"

plugin_important_actions = ["FileSaveAction"]
readwrite_only_active_actions = ["ToolsBookCloseAction"]
dirty_only_active_actions = ["FileSaveAction", "FileRevertAction"]


class BasicCommandsPlugin(Plugin):
    plugin_name = "BasicCommandsPlugin"
    actions_name = PLUGIN_ACTIONS_NAME
    ui_filename = PLUGIN_UI_FILENAME
    important_actions = plugin_important_actions
    __gtype_name__ = "BasicCommandsPlugin"

    def __init__(self):
        super(BasicCommandsPlugin, self).__init__()
        BasicCommandsPlugin.actions = [(
            "FileNewAction", "document-new", "New _File", "<primary>n",
            "Create a new file",
            self.cmd_file_new
        ),
            (
                "FileOpenAction", "document-open", "_Open...", "<primary>o",
                "Open an existing GasStation file",
                self.cmd_file_open
            ),
            (
                "FileSaveAction", "document-save", "_Save", "<primary>s",
                "Save the current file",
                self.cmd_file_save
            ),
            (
                "FileSaveAsAction", "document-save-as", "Save _As...", "<shift><primary>s",
                "Save this file with a different name",
                self.cmd_file_save_as
            ),
            (
                "FileRevertAction", "document-revert", "Re_vert", None,
                "Reload the current database, reverting all unsaved changes",
                self.cmd_file_revert
            ),
            (
                "FileExportAccountsAction", "go-next",
                "Export _Accounts", None,
                "Export the account hierarchy to a new GasStation datafile",
                None
            ),

            (
                "EditFindTransactionsAction", "edit-find", "_Find...", "<primary>f",
                "Find transactions with a search",
                None
            ),
            (
                "EditTaxOptionsAction", None,

                "Ta_x Report Options", None,

                "Setup relevant accounts for tax reports, e.g. US income tax",
                None
            ),

            ("ActionsScheduledTransactionsAction", None, "_Scheduled Transactions", None, None, None),
            (
                "ActionsScheduledTransactionEditorAction", None, "_Scheduled Transaction Editor", None,
                "The list of Scheduled Transactions",
                self.cmd_actions_scheduled_transaction_editor
            ),
            (
                "ActionsSinceLastRunAction", None, "Since _Last Run...", None,
                "Create Scheduled Transactions since the last time run",
                self.cmd_actions_since_last_run
            ),
            (
                "ActionsMortgageLoanAction", None, "_Mortgage & Loan Repayment...", None,
                "Setup scheduled transactions for repayment of a loan",
                None
            ),
            ("ActionsBudgetAction", None, "B_udget", None, None, None),
            (
                "ActionsCloseBooksAction", None, "Close _Books", None,
                "Archive old data using accounting periods",
                None
            ),
            (
                "ToolsPriceEditorAction", None, "_Price Database", None,
                "View and edit the prices for stocks and mutual funds",
                self.cmd_tools_price_editor
            ),
            (
                "ToolsCommodityEditorAction", None, "_Security Editor", None,
                "View and edit the commodities for stocks and mutual funds",
                self.cmd_tools_commodity_editor
            ),
            (
                "ToolsFinancialCalculatorAction", None, "_Loan Repayment Calculator", None,
                "Use the loan/mortgage repayment calculator",
                None
            ),
            (
                "ToolsBookCloseAction", None, "_Close Book", None,
                "Close the Book at the end of the Period",
                self.cmd_close_book
            ),
            (
                "ToolsImapEditorAction", None, "_Import Map Editor", None,
                "View and Delete Bayesian and Non Bayesian information",
                self.cmd_import_map
            ),
            (
                "ToolsTransAssocAction", None, "_Transaction Associations", None,
                "View all Transaction Associations",
                self.cmd_tools_trans_assoc
            ),

            (
                "HelpTipsOfTheDayAction", None, "_Tips Of The Day", None,
                "View the Tips of the Day",
                None
            )]

    @classmethod
    def new(cls):
        return GObject.new(cls)

    def add_to_window(self, window, _type):
        window.connect("page_changed", self.main_window_page_changed)

    @staticmethod
    def update_inactive_actions(plugin_page):
        if not Session.current_session_exist():
            return
        book = Session.get_current_book()
        is_readwrite = not (True if book is None else book.is_readonly())
        is_dirty = (False if book is None else book.session_not_saved())
        window = plugin_page.window
        if window is None: return
        action_group = window.get_action_group(PLUGIN_ACTIONS_NAME)
        update_actions(action_group, readwrite_only_active_actions, "sensitive", is_readwrite)
        update_actions(action_group, dirty_only_active_actions, "sensitive", is_dirty)

    @classmethod
    def main_window_page_changed(cls, window, page, *user_data):
        if page is not None:
            cls.update_inactive_actions(page)

    def cmd_file_new(self, action, data):
        if not data.window.all_finish_pending():
            return
        File.new(data.window)

    def cmd_file_open(self, action, data):
        if not data.window.all_finish_pending():
            return
        data.window.set_progressbar_window(data.window)
        dialog = FileAccessDialog(data.window, FileAccessType.OPEN)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            url = dialog.geturl()
            if url is not None:
                File.open_file(data.window, url.__to_string__(False), False)
        dialog.destroy()

    def cmd_file_save(self, action, data):
        if not data.window.all_finish_pending():
            return
        data.window.set_progressbar_window(data.window)
        File.save(data.window)

    def cmd_file_save_as(self, action, data):
        if not data.window.all_finish_pending():
            return
        data.window.set_progressbar_window(data.window)
        FileAccessDialog.save_as(data.window)

    def cmd_file_revert(self, action, data):
        if not data.window.all_finish_pending():
            return
        data.window.set_progressbar_window(data.window)
        File.revert(data.window)

    def cmd_tools_trans_assoc(self, action, data):
        from gasstation.views.dialogs.trans_assoc import AssocDialog
        gs_set_busy_cursor(None, True)
        AssocDialog.new(data.window)
        gs_unset_busy_cursor(None)

    def cmd_import_map(self, action, data):
        from gasstation.views.dialogs.file_import import ImportDialog
        gs_set_busy_cursor(None, True)
        d = ImportDialog(data.window)
        d.present()
        gs_unset_busy_cursor(None)

    def cmd_close_book(self, action, data):
        CloseBookWindow.new(Session.get_current_book(), data.window)

    def cmd_tools_price_editor(self, action, data):
        from gasstation.views.dialogs.price.prices import PriceDbDialog
        gs_set_busy_cursor(None, True)
        dialog = PriceDbDialog(data.window, Session.get_current_book())
        dialog.run()
        gs_unset_busy_cursor(None)

    def cmd_tools_commodity_editor(self, action, data):
        from gasstation.views.dialogs.commodity.window import CommodityListWindow
        gs_set_busy_cursor(None, True)
        window = CommodityListWindow(data.window)
        window.show()
        gs_unset_busy_cursor(None)

    def cmd_actions_since_last_run(self, action, data):
        from gasstation.views.dialogs.scheduled.since_last_run import Summary, ScheduledInstanceModel, \
            ScheduledSinceLastRunDialog
        auto_created_transactions = None
        nothing_to_do_msg = "There are no Scheduled Transactions to be entered at this time."
        window = data.window
        if Session.get_current_book().is_readonly():
            return
        summary = Summary()
        inst_model = ScheduledInstanceModel.get_current_instances()
        inst_model.summarize(summary)
        inst_model.effect_change(True, auto_created_transactions, [])

        if summary.need_dialog:
            d = ScheduledSinceLastRunDialog(data.window, inst_model, auto_created_transactions)
            d.run()
        else:
            if summary.num_auto_create_no_notify_instances != 0:
                show_info(window, nothing_to_do_msg)
            else:
                show_info(window,
                          "There are no Scheduled Transactions to be entered at this time. "
                          "(%d transactions automatically created)" %
                          summary.num_auto_create_no_notify_instances)

    def cmd_actions_scheduled_transaction_editor(self, action, data):
        from gasstation.plugins.pages.scheduled_list import ScheduledTransactionListPluginPage
        page = ScheduledTransactionListPluginPage.new()
        data.window.open_page(page)
