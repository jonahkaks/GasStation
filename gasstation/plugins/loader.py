import collections
import logging
import os
from importlib import *

logging.basicConfig(level=logging.DEBUG)


class PluginLoader:
    '''
    A simple plugin manager
    '''

    def __init__(self, plugin_folder, main_module='__init__', log=logging):
        self.logging = log
        self.plugin_folder = plugin_folder
        self.main_module = main_module
        self.loaded_plugins = collections.OrderedDict({})

    def get_available_plugins(self):
        '''
        Returns a dictionary of plugins available in the plugin folder
        '''
        plugins = {}
        for possible in os.listdir(self.plugin_folder):
            # print(possible)
            location = os.path.join(self.plugin_folder, possible)
            if os.path.isfile(location) and self.main_module + '.py' in os.listdir(self.plugin_folder):
                from importlib.util import find_spec
                info = find_spec(self.main_module, [location])
                plugins[possible] = {
                    'name': possible,
                    'info': info
                }
        return plugins

    def get_loaded_plugins(self):
        '''
        Returns a dictionary of the loaded plugin modules
        '''
        return self.loaded_plugins.copy()

    def load_plugin(self, plugin_name):
        '''
        Loads a plugin module
        '''
        plugins = self.get_available_plugins()
        if plugin_name in plugins:
            if plugin_name not in self.loaded_plugins:
                print(plugin_name, plugins[plugin_name]['info'])
                module = import_module(plugin_name, plugins[plugin_name]['info'])
                self.loaded_plugins[plugin_name] = {
                    'name': plugin_name,
                    'info': plugins[plugin_name]['info'],
                    'module': module
                }
                self.logging.info('plugin "%s" loaded' % plugin_name)
            else:
                self.logging.warning('plugin "%s" already loaded' % plugin_name)
        else:
            self.logging.error('cannot locate plugin "%s"' % plugin_name)
            raise Exception('cannot locate plugin "%s"' % plugin_name)

    def unload_plugin(self, plugin_name):
        '''
        Unloads a plugin module
        '''
        del self.loaded_plugins[plugin_name]
        self.logging.info('plugin "%s" unloaded' % plugin_name)

    def execute_action_hook(self, hook_name, hook_params={}):
        '''
        Executes action hook functions of the form action_hook_name contained in
        the loaded plugin modules.
        '''
        for key, plugin_info in self.loaded_plugins.items():
            module = plugin_info['module']
            hook_func_name = 'action_%s' % hook_name
            if hasattr(module, hook_func_name):
                hook_func = getattr(module, hook_func_name)
                hook_func(hook_params)

    def execute_filter_hook(self, hook_name, hook_params={}):
        '''
        Filters the hook_params through filter hook functions of the form
        filter_hook_name contained in the loaded plugin modules.
        '''
        hook_params_keys = hook_params.keys()
        for key, plugin_info in self.loaded_plugins.items():
            module = plugin_info['module']
            hook_func_name = 'filter_%s' % hook_name
            if hasattr(module, hook_func_name):
                hook_func = getattr(module, hook_func_name)
                hook_params = hook_func(hook_params)
                for nkey in hook_params_keys:
                    if nkey not in hook_params.keys():
                        msg = 'function "%s" in plugin "%s" is missing "%s" in the dict it returns' % (
                            hook_func_name, plugin_info['name'], nkey)
                        self.logging.error(msg)
                        raise Exception(msg)
        return hook_params


if __name__ == '__main__':
    a = PluginLoader("/home/newton/Projects/GasStation/gasstation/plugins")
    # for plugin in a.get_available_plugins():
    #     a.load_plugin(plugin)
    print(a.get_available_plugins())
