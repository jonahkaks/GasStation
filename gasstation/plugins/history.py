from gasstation.utilities.file_history import *
from gasstation.utilities.preferences import *
from gasstation.views.dialogs.setup import File
from .plugin import *

PLUGIN_FILE_HISTORY_NAME = "file-history"

PREFS_GROUP_HISTORY = "history"
PREF_HISTORY_MAXFILES = "maxfiles"

PLUGIN_ACTIONS_NAME = "plugin/file-history-actions"
PLUGIN_UI_FILENAME = "plugin/file-history-ui.xml"


class FileHistoryPlugin(Plugin):
    plugin_name = PLUGIN_FILE_HISTORY_NAME
    actions_name = PLUGIN_ACTIONS_NAME
    ui_filename = PLUGIN_UI_FILENAME
    _data = defaultdict(dict)
    __gtype_name__ = "FileHistoryPlugin"

    def __init__(self):
        super().__init__()
        self.actions = [
            ("RecentFile0Action", None, "", None, None, self.cmd_open_file),
            ("RecentFile1Action", None, "", None, None, self.cmd_open_file),
            ("RecentFile2Action", None, "", None, None, self.cmd_open_file),
            ("RecentFile3Action", None, "", None, None, self.cmd_open_file),
            ("RecentFile4Action", None, "", None, None, self.cmd_open_file),
            ("RecentFile5Action", None, "", None, None, self.cmd_open_file),
            ("RecentFile6Action", None, "", None, None, self.cmd_open_file),
            ("RecentFile7Action", None, "", None, None, self.cmd_open_file),
            ("RecentFile8Action", None, "", None, None, self.cmd_open_file),
            ("RecentFile9Action", None, "", None, None, self.cmd_open_file)
        ]
        self.action_group = None


    @staticmethod
    def get_user_data(obj, key):
        return FileHistoryPlugin._data.get(obj, {}).get(key, None)

    @staticmethod
    def set_user_data(obj, key, userdata):
        FileHistoryPlugin._data[obj][key] = userdata

    def add_to_window(self, window, _type):
        gs_pref_register_cb(PREFS_GROUP_HISTORY, None, self.list_changed)
        self.action_group = window.get_action_group(PLUGIN_ACTIONS_NAME)
        self.update_menus()

    def remove_from_window(self, window, _type):
        gs_pref_remove_cb_by_func(PREFS_GROUP_HISTORY, None, self.list_changed)

    def list_changed(self, prefs, pref):
        if pref == PREF_HISTORY_MAXFILES:
            self.update_menus()
            return
        index = FileHistory.pref_name_to_index(pref)
        if index < 0:
            return
        filename = gs_pref_get_string(PREFS_GROUP_HISTORY, pref)
        self.update_action(index, filename)

    def cmd_open_file(self, action, data):
        if action is None or data is None:
            return
        filename = self.get_user_data(action, FILENAME_STRING)
        data.window.set_progressbar_window(data.window)
        File.open_file(data.window, filename, False)
        data.window.set_progressbar_window(None)

    def update_action(self, index, filename):
        action_group = self.action_group
        action_name = "RecentFile%dAction" % index
        action = action_group.get_action(action_name)
        if action is None:
            return
        limit = gs_pref_get_int(PREFS_GROUP_HISTORY, PREF_HISTORY_MAXFILES)
        if filename is not None and len(filename) > 0 and index < limit:
            label_name = FileHistory.generate_label(index, filename)
            tooltip = FileHistory.generate_tooltip(index, filename)
            action.set_property("label", label_name)
            action.set_property("tooltip", tooltip)
            action.set_property("visible", True)
            self.set_user_data(action, FILENAME_STRING, filename)
        else:
            action.set_visible(False)

    def update_menus(self):
        for i in range(MAX_HISTORY_FILES):
            pref = FileHistory.index_to_pref_name(i)
            filename = gs_pref_get_string(PREFS_GROUP_HISTORY, pref)
            self.update_action(i, filename)
