from collections import namedtuple

from gi.repository import GObject

from gasstation.utilities.actions import set_important_actions
from libgasstation.core.component import *

action_toolbar_labels = namedtuple("action_toolbar_labels", "action_name, label")


class Plugin(GObject.Object):
    plugin_name = None
    actions_name = None
    actions = None
    toggle_actions = None
    important_actions = None
    ui_filename = None
    __gsignals__ = {
        "merge-actions": (GObject.SignalFlags.RUN_FIRST, None, (int,)),
        "unmerge-actions": (GObject.SignalFlags.RUN_FIRST, None, (int,)),
    }

    def __init__(self):
        super().__init__()
        Tracking.remember(self)

    def do_add_to_window(self, window, _type):
        if self.actions_name is not None:
            window.merge_actions(self.actions_name,
                                 self.actions,
                                 self.toggle_actions,
                                 self.ui_filename, self)
            if self.important_actions is not None:
                action_group = window.lookup_group(self.actions_name)
                set_important_actions(action_group, self.important_actions)
        self.add_to_window(window, _type)

    def do_remove_from_window(self, window, _type):
        self.remove_from_window(window, _type)
        if self.actions_name and not window.just_self_prefs:
            window.unmerge_actions(self.actions_name)

    def remove_from_window(self, window, _type):
        pass

    def add_to_window(self, window, _type):
        pass

    def get_name(self):
        return self.plugin_name

    def __del__(self):
        Tracking.forget(self)
