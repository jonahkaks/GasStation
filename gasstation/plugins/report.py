from gasstation.plugins.pages.report import ReportPluginPage
from gasstation.views.dialogs.stylesheets import StyleSheetsDialog
from gasstation.views.reports import *
from .plugin import Plugin

PLUGIN_ACTIONS_NAME = "plugin/stylesheets-actions"
PLUGIN_UI_FILENAME = "plugin/stylesheets-ui.xml"


class StyleSheetsPlugin(Plugin):
    actions_name = PLUGIN_ACTIONS_NAME
    ui_filename = PLUGIN_UI_FILENAME
    __gtype_name__ = "StyleSheetsPlugin"
    initialized = False

    def __init__(self):
        super().__init__()
        self.actions = [("EditStyleSheetsAction", None, "St_yle Sheets", None,
                         "Edit report style sheets", self.cmd_edit_style_sheet)]

    def __new__(cls, *args, **kwargs):
        if not cls.initialized:
            WebKitWebView.register_stream_handlers(URL_TYPE_HELP, cls.file_stream_cb)
            WebKitWebView.register_stream_handlers(URL_TYPE_FILE, cls.file_stream_cb)
            WebKitWebView.register_stream_handlers(URL_TYPE_REPORT, cls.report_stream_cb)

            WebKitWebView.register_url_handlers(URL_TYPE_OPTIONS, cls.options_url_cb)
            WebKitWebView.register_url_handlers(URL_TYPE_REPORT, cls.report_url_cb)
            WebKitWebView.register_url_handlers(URL_TYPE_HELP, cls.help_url_cb)
            report_menu_setup()
        return super().__new__(cls)

    def cmd_edit_style_sheet(self, _, data):
        StyleSheetsDialog.open(data.window)

    @staticmethod
    def file_stream_cb(location):
        pass

    @staticmethod
    def report_stream_cb(location):
        data = Report.run_id_string(location)
        if not data:
            data = "<html><body><h3>%s</h3>" % "<p>%s</p><pre>%s</pre></body></html>" % \
                   ("Report error", "An error occurred while running the report.")
            report_finished()
        return data

    @staticmethod
    def report_url_cb(location, label, new_window, result):

        if result is None or location is None:
            return False

        if new_window:
            url = WebKitWebView.build_url(URL_TYPE_REPORT, location, label)
            ReportPluginPage.open_report_url(url, result.parent)
            result.load_to_stream = False
        else:
            result.load_to_stream = True
        return True

    @staticmethod
    def options_url_cb(location, label, new_window, result):
        if location is None or result is None:
            return False

        result.load_to_stream = False
        if location.startswith("report-id="):
            try:
                report_id = int(location[10:])
            except ValueError:
                result.error_message = "Badly formed options URL: %s" % location
                return False

            report = Report.find(report_id)
            if report is None:
                result.error_message = "Badly formed options URL: %s" % location
                return False
            report.edit_options(result.parent)
            return True
        else:
            result.error_message = "Badly formed options URL: %s" % location
            return False

    @staticmethod
    def help_url_cb(location, label, new_window, result):
        pass
        # {
        # g_return_val_if_fail (location != NULL, FALSE)
        #
        # if (label && (*label != '\0'))
        #     gnc_gnome_help (GTK_WINDOW(result->parent), location, label)
        #     else
        #     gnc_gnome_help (GTK_WINDOW(result->parent), location, NULL)
        #     return TRUE
        # }
