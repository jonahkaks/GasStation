from functools import cmp_to_key

from gi.repository import Gtk, GLib

from gasstation.utilities.extension import Extentions
from .plugin import *

PLUGIN_MENU_ADDITIONS_NAME = "plugin/menu-additions"
PLUGIN_ACTIONS_NAME = "plugin/menu-additions-actions"


class MenuAdditionsPluginPerWindow:
    def __init__(self):
        self.window = None
        self.ui_manager = None
        self.group = None
        self.merge_id = 0


class MenuAdditionsPlugin(Plugin):
    plugin_name = PLUGIN_MENU_ADDITIONS_NAME
    actions_name = PLUGIN_ACTIONS_NAME
    __gtype_name__ = "MenuAdditionsPlugin"

    def __init__(self):
        super().__init__()

    @classmethod
    def new(cls):
        plugin = GObject.new(cls)
        return plugin

    @staticmethod
    def action_cb(action, data):
        Extentions.invoke_cb(data.data, data.window)

    @staticmethod
    def sort(a, b):
        if a.type == b.type:
            return GLib.strcmp0(a.sort_key, b.sort_key)
        elif a.type == Gtk.UIManagerItemType.MENU:
            return -1
        elif b.type == Gtk.UIManagerItemType.MENU:
            return 1
        else:
            return 0

    @staticmethod
    def do_preassigned_accel(info, table):
        if info.accel_assigned:
            return
        if not GLib.utf8_validate(info.ae.get_label().encode()):
            info.accel_assigned = True
            return

        ptr = GLib.utf8_strchr(info.ae.get_label(), -1, '_')

        if ptr is None:
            return
        accel_key = GLib.utf8_strdown(GLib.utf8_find_next_char(ptr), -1)
        mp = table.get(info.path, "")
        new_map = mp + accel_key
        table[info.path] = new_map
        info.accel_assigned = True

    @staticmethod
    def assign_accel(info, table):
        if info.accel_assigned:
            return
        mp = table.get(info.path)
        if mp is None:
            mp = ""
        uni = None
        i = 0
        for i, uni in enumerate(info.ae.get_label()):
            if not uni.isalpha():
                continue
            uni = GLib.unichar_tolower(uni)

            if not GLib.utf8_strchr(mp, -1, uni):
                break

        if uni is None or i == 0:
            info.accel_assigned = True
            return
        start = info.ae.get_label()[:i]
        new_label = start + "_" + uni
        info.ae.set_label(new_label)
        new_map = mp
        table[info.path] = new_map
        info.accel_assigned = True
        pass

    def menu_setup_one(self, ext_info, per_window):
        from gasstation.views.main_window import MainWindowActionData
        cb_data = MainWindowActionData()
        cb_data.window = per_window.window
        cb_data.data = ext_info.extension
        action = ext_info.ae
        if ext_info.type == Gtk.UIManagerItemType.MENUITEM:
            action.connect('activate', self.action_cb, cb_data)
        per_window.group.add_action(action)
        per_window.ui_manager.add_ui(per_window.merge_id, ext_info.path, ext_info.ae.get_label(),
                                     ext_info.ae.get_name(), ext_info.type, False)
        per_window.ui_manager.ensure_update()

    def add_to_window(self, window, _type, **kwargs):
        per_window = MenuAdditionsPluginPerWindow()
        per_window.window = window
        per_window.ui_manager = window.ui_merge
        per_window.group = Gtk.ActionGroup.new(name="MenuAdditions")
        per_window.merge_id = window.ui_merge.new_merge_id()
        per_window.ui_manager.insert_action_group(per_window.group, 0)
        menu_list = sorted(Extentions.get_menu_list(), key=cmp_to_key(self.sort))

        table = {}
        for k in menu_list:
            self.do_preassigned_accel(k, table)
        for k in menu_list:
            self.assign_accel(k, table)

        for k in menu_list:
            self.menu_setup_one(k, per_window)
        window.manual_merge_actions(PLUGIN_ACTIONS_NAME, per_window.group, per_window.merge_id)

    def remove_from_window(self, window, _type, **kwargs):
        group = window.get_action_group(PLUGIN_ACTIONS_NAME)
        if group is not None and not window.just_plugin_prefs:
            window.ui_merge.remove_action_group(group)
