from .pages.account import *
from .plugin import Plugin

PLUGIN_ACCOUNT_TREE_NAME = "plugin/account-tree"
PLUGIN_ACTIONS_NAME = "plugin/account-tree-actions"
PLUGIN_UI_FILENAME = "plugin/account-tree-ui.xml"


class AccountTreePlugin(Plugin):
    plugin_name = PLUGIN_ACCOUNT_TREE_NAME
    actions_name = PLUGIN_ACTIONS_NAME
    ui_filename = PLUGIN_UI_FILENAME
    __gtype_name__ = "AccountTreePlugin"

    def __init__(self):
        super().__init__()
        self.actions = [("ViewAccountTreeAction", None, "New Accounts _Page", None, "Open a new Account Tree page",
                         self.cmd_new_account_tree)]

    @staticmethod
    def cmd_new_account_tree(_, data):
        page = AccountTreePluginPage()
        data.window.open_page(page)


GObject.type_register(AccountTreePlugin)
