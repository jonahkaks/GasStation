from gasstation.plugins.pages.register import *
from gasstation.plugins.plugin import Plugin

PLUGIN_REGISTER_NAME = "plugin/register"
PLUGIN_ACTIONS_NAME = "plugin/register2-actions"
PLUGIN_UI_FILENAME = "plugin/register2-ui.xml"


class RegisterPlugin(Plugin):
    plugin_name = PLUGIN_REGISTER_NAME
    actions_name = PLUGIN_ACTIONS_NAME
    ui_filename = PLUGIN_UI_FILENAME
    __gtype__name = "RegisterPlugin"

    def __init__(self):
        super().__init__()

    @staticmethod
    def new():
        plugin = GObject.new(RegisterPlugin)
        plugin.actions = [
            ("ToolsGeneralJournal2Action", None, "_General Journal", None, "Open a general journal window",
             plugin.cmd_general_ledger), ("Register2TestAction", None, "_Register2", None, None, None),
            ("Register2TestGLAction", None, "Register2 Open GL Account", None, "Register2 Open GL Account",
             RegisterPlugin.cmd_general_ledger)]
        return plugin

    def add_to_window(self, window, _type, **kwargs):
        gs_pref_register_cb(PREFS_GROUP_GENERAL_REGISTER, None,
                            self.pref_changed, window)

    def remove_from_window(self, window, _type, **kwargs):
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL_REGISTER, None,
                                  self.pref_changed, window)

    @staticmethod
    def cmd_general_ledger(action, data):
        if data is None:
            return
        page = RegisterPluginPage.new_gl()
        data.window.open_page(page)

    def pref_changed(self, prefs, pref, *args):
        ComponentManager.refresh_all()
