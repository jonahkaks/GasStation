from dateutil.relativedelta import relativedelta

from gasstation.utilities.date import print_datetime
from gasstation.utilities.preferences import *
from libgasstation import Account
from libgasstation.core.commodity import COMMODITY_NAMESPACE_NAME_TEMPLATE
from libgasstation.core.component import Tracking
from libgasstation.core.session import Session
from libgasstation.core.transaction import *
from .model import *

TREE_MODEL_SPLIT_REG_CM_CLASS = "tree-model-split-reg"
TANROW = "#F6FFDA"
SPLITROW = "#EDE7D3"
GREENROW = "#BFDEB9"
YELLOWROW = "#FFEF98"
NUM_OF_TRANS = 10
PREF_MAX_TRANS = "max-transactions"


class RegisterModelType(GObject.GEnum):
    BANK_REGISTER = 0
    CASH_REGISTER = 1
    ASSET_REGISTER = 2
    CREDIT_REGISTER = 3
    LIABILITY_REGISTER = 4
    INCOME_REGISTER = 5
    EXPENSE_REGISTER = 6
    EQUITY_REGISTER = 7
    STOCK_REGISTER = 8
    CURRENCY_REGISTER = 9
    RECEIVABLE_REGISTER = 10
    PAYABLE_REGISTER = 11
    TRADING_REGISTER = 12
    NUM_SINGLE_REGISTER_TYPES = 13
    GENERAL_JOURNAL = NUM_SINGLE_REGISTER_TYPES
    INCOME_LEDGER = 14
    PORTFOLIO_LEDGER = 15
    SEARCH_LEDGER = 16
    NUM_REGISTER_TYPES = 17


def get_model_type_from_account_type(account_type):
    return {'AReceivable': RegisterModelType.RECEIVABLE_REGISTER,
            'Mutual': RegisterModelType.STOCK_REGISTER,
            'Cash': RegisterModelType.CASH_REGISTER,
            'Asset': RegisterModelType.ASSET_REGISTER,
            'Bank': RegisterModelType.BANK_REGISTER,
            'Stock': RegisterModelType.STOCK_REGISTER,
            'Purchase': RegisterModelType.ASSET_REGISTER,
            'Credit': RegisterModelType.CREDIT_REGISTER,
            'Liability': RegisterModelType.LIABILITY_REGISTER,
            'APayable': RegisterModelType.PAYABLE_REGISTER,
            "Income": RegisterModelType.INCOME_REGISTER,
            "Sale": RegisterModelType.INCOME_REGISTER,
            "Expense": RegisterModelType.EXPENSE_REGISTER,
            'Trading': RegisterModelType.TRADING_REGISTER,
            "Equity": RegisterModelType.EQUITY_REGISTER}.get(account_type, RegisterModelType.GENERAL_JOURNAL)


class RegisterModelStyle(GObject.GEnum):
    LEDGER = 0
    AUTO_LEDGER = 1
    JOURNAL = 2


class RegisterModelColumn(GObject.GEnum):
    GUID = 0
    ENTER_DATE = 1
    DATE = 2
    DUEDATE = 3
    NUMACT = 4
    DESCNOTES = 5
    TRANSFERVOID = 6
    RECN = 7
    DEBIT = 8
    CREDIT = 9
    LAST_VISIBLE = CREDIT
    RO = 10
    NUM_VIS = 11
    ACT_VIS = 12
    NUM_COLUMNS = 13


class RegisterModelUpdate(GObject.GEnum):
    HOME = 0
    UP = 1
    PGUP = 2
    GOTO = 3
    PGDOWN = 4
    DOWN = 5
    END = 6


class RegisterModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = "RegisterModel"
    __gsignals__ = {
        "refresh_trans": (GObject.SignalFlags.RUN_LAST, None, (object,)),
        "refresh_status_bar": (GObject.SignalFlags.RUN_LAST, None, ()),
        "refresh_view": (GObject.SignalFlags.RUN_LAST, None, (int,)),
        "scroll_sync": (GObject.SignalFlags.RUN_LAST, None, ()),
        "selection_move_delete": (GObject.SignalFlags.RUN_LAST, None, (object,)),
    }

    def __init__(self, reg_type, style, use_double_line, is_template):
        GObject.Object.__init__(self)
        self.anchor = None
        self.full_tlist = []
        self.tlist = []
        self.tlist_start = 0
        self.template_account = None
        self.sort_depth = 1
        self.sort_col = 1
        self.separator_changed = False
        self.current_trans = None
        self.current_row = -1
        self.load_time = 0
        self.number_of_trans_in_full_tlist = 0
        self.position_of_trans_in_full_tlist = -1
        self.expanded_path = 0
        self.user_data = {}
        self.bsplit_parent_node = None
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_ACCOUNTING_LABELS, self.prefs_changed)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_ACCOUNT_SEPARATOR, self.prefs_changed)
        gs_pref_register_cb(PREFS_GROUP_GENERAL_REGISTER, PREF_MAX_TRANS, self.limit_changed_cb)
        self.display_gl = False
        self.display_subacc = False
        self.book = Session.get_current_book()
        self.type = reg_type
        self.style = style
        self.use_double_line = use_double_line
        self.is_template = is_template
        self.sort_direction = Gtk.SortType.ASCENDING
        self.btrans = Transaction(self.book)
        self.bsplit = Split(self.book)

        self.use_gs_color_theme = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_USE_GASSTATION_COLOR_THEME)
        self.alt_colors_by_transaction = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_ALT_COLOR_BY_TRANS)
        self.limit = gs_pref_get_float(PREFS_GROUP_GENERAL_REGISTER, PREF_MAX_TRANS)
        self.action_list = Gtk.ListStore(str)
        self.notes_list = Gtk.ListStore(str)
        self.description_list = Gtk.ListStore(str, object)
        self.memo_list = Gtk.ListStore(str)
        self.acct_list = Gtk.ListStore(str, str)
        self.event_handler_id = Event.register_handler(self.event_handler)
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        Tracking.remember(self)

    def iter_is_valid(self, iter):
        return iter.user_data is not None and self.get_trans_from_iter(iter) is not None \
               and self.stamp == iter.stamp and ((IS_SPLIT(iter) and
                                                  iter.user_data3)
                                                 or (IS_BLANK_SPLIT(
                    iter) and self.get_trans_from_iter(iter) == self.btrans)) \
               or (not IS_SPLIT(iter) and self.get_trans_from_iter(iter)) or (
                       IS_BLANK_TRANS(iter) and iter.user_data3 is None
               )

    def get_user_data(self, key):
        return self.user_data.get(key, None)

    def set_user_data(self, key, value):
        self.user_data[key] = value

    @staticmethod
    def is_blank_trans(iter):
        return IS_BLANK_TRANS(iter)

    def do_get_flags(self):
        return 0

    def do_get_n_columns(self):
        return RegisterModelColumn.NUM_COLUMNS

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def make_iter(self, f, tnode: int, snode: int):
        _iter = Gtk.TreeIter()
        _iter.stamp = self.stamp
        _iter.user_data = f
        _iter.user_data2 = tnode
        _iter.user_data3 = snode
        return _iter

    def set_use_double_line(self, double=True):
        self.use_double_line = double

    def get_anchor(self):
        return self.anchor

    def get_description_list(self):
        return self.description_list

    def get_notes_list(self):
        return self.notes_list

    def get_memo_list(self):
        return self.memo_list

    def get_action_list(self):
        return self.action_list

    def get_acct_list(self):
        return self.acct_list

    def prefs_changed(self, _, pref):
        if pref is None:
            return
        if pref.endswith(PREF_ACCOUNT_SEPARATOR):
            self.separator_changed = True

    def __load(self, model_update, num_of_rows):
        if model_update == RegisterModelUpdate.HOME:
            self.tlist_start = 0
            self.tlist = self.full_tlist[:num_of_rows]

        elif model_update == RegisterModelUpdate.END:
            self.tlist_start = len(self.full_tlist) - num_of_rows
            self.tlist = self.full_tlist[self.tlist_start:]

        elif model_update == RegisterModelUpdate.GOTO:
            self.tlist_start = int(num_of_rows - NUM_OF_TRANS * 1.5)
            self.tlist = self.full_tlist[self.tlist_start:self.tlist_start + (NUM_OF_TRANS * 3)]

    def foreach_function(self, model, path, _iter, rr_list):

        row_ref = Gtk.TreeRowReference.new(model, path)
        rr_list.append(row_ref)
        return True

    def remove_all_rows(self):
        rr_list = []
        self.foreach(self.foreach_function, rr_list)
        rr_list.reverse()
        for ref in rr_list:
            path = ref.get_path()
            self.row_deleted(path)
        self.increment_stamp()

    def load(self, slist, account):
        self.remove_all_rows()
        self.full_tlist = None
        self.tlist = None
        self.full_tlist = Split.get_unique_transactions(slist)
        self.current_trans = self.btrans
        self.full_tlist.append(self.current_trans)

        if self.sort_direction != Gtk.SortType.ASCENDING:
            self.full_tlist.reverse()
        self.sync_scrollbar()
        self.number_of_trans_in_full_tlist = len(self.full_tlist)

        if len(self.full_tlist) < NUM_OF_TRANS * 3:
            self.tlist = self.full_tlist.copy()
        else:
            if self.position_of_trans_in_full_tlist < (NUM_OF_TRANS * 3):
                self.__load(RegisterModelUpdate.HOME, NUM_OF_TRANS * 3)
            elif self.position_of_trans_in_full_tlist > len(self.full_tlist) - (NUM_OF_TRANS * 3):
                self.__load(RegisterModelUpdate.END, NUM_OF_TRANS * 3)
            else:
                self.__load(RegisterModelUpdate.GOTO, self.position_of_trans_in_full_tlist)
        GLib.idle_add(self.update_completion_lists, priority=GLib.PRIORITY_HIGH_IDLE)
        self.anchor = account
        self.bsplit_parent_node = None

    def move(self, model_update):
        if len(self.full_tlist) < NUM_OF_TRANS * 3:
            return
        if model_update == RegisterModelUpdate.UP and self.current_row < NUM_OF_TRANS and self.tlist_start > 0:
            iblock_start = self.tlist_start - NUM_OF_TRANS
            iblock_end = self.tlist_start - 1
            dblock_start = self.tlist_start + (NUM_OF_TRANS * 2)
            if iblock_start < 0:
                iblock_start = 0
            icount = iblock_end - iblock_start + 1
            dcount = icount
            dblock_end = dblock_start + dcount - 1
            self.tlist_start = iblock_start
            for trans in self.full_tlist[:iblock_end][::-1][:icount]:
                self.insert_trans(trans, True)
            for trans in self.full_tlist[:dblock_end][::-1][:dcount]:
                self.delete_trans(trans)
            self.emit("refresh_view", 20)

        elif model_update == RegisterModelUpdate.DOWN and \
                self.current_row > NUM_OF_TRANS * 2 and \
                self.tlist_start < (len(self.full_tlist) - NUM_OF_TRANS * 3):
            iblock_start = self.tlist_start + NUM_OF_TRANS * 3
            iblock_end = iblock_start + NUM_OF_TRANS - 1
            dblock_start = self.tlist_start
            if iblock_start < 0:
                iblock_start = 0

            if iblock_end > len(self.full_tlist):
                iblock_end = len(self.full_tlist) - 1

            icount = iblock_end - iblock_start + 1
            dcount = icount
            dblock_end = dblock_start + dcount
            self.tlist_start = dblock_end
            for trans in self.full_tlist[iblock_start:][:icount]:
                self.insert_trans(trans, False)
            for trans in self.full_tlist[dblock_start:][:dcount]:
                self.delete_trans(trans)
            self.emit("refresh_view", 20)

    def get_first_trans(self):
        trans = self.full_tlist[0]
        if trans == self.btrans:
            trans = self.full_tlist[-1]
        return trans

    def trans_is_in_view(self, trans):
        return trans in self.tlist

    def get_tooltip(self, position=0):
        trans = self.full_tlist[position]
        if trans is not None and trans != self.btrans:
            date = trans.get_post_date()
            desc_text = trans.get_description()
            self.current_trans = trans
            if date is not None:
                date_text = print_datetime(date)
            else:
                date_text = ""
            return "".join([date_text, "\n", str(desc_text)])

    def set_current_trans_by_position(self, position):
        trans = self.full_tlist[position]
        if trans is None:
            trans = self.full_tlist[-1]
        self.current_trans = trans

    def sync_scrollbar(self):
        try:
            self.position_of_trans_in_full_tlist = self.full_tlist.index(self.current_trans)
        except ValueError:
            self.position_of_trans_in_full_tlist = -1
        self.emit("scroll_sync")

    def set_template_account(self, template_account):
        self.template_account = template_account

    def get_template_account(self):
        return self.template_account

    def get_template(self):
        return self.is_template

    def limit_changed_cb(self, pref, p):
        limit = gs_pref_get_float(PREFS_GROUP_GENERAL_REGISTER, PREF_MAX_TRANS)
        if limit != 0 and RegisterModelType.SEARCH_LEDGER != self.type:
            self.limit = limit

    def config(self, newtype, newstyle, use_double_line):
        self.type = newtype
        if self.type >= RegisterModelType.NUM_SINGLE_REGISTER_TYPES:
            newstyle = RegisterModelStyle.JOURNAL
        self.style = newstyle
        self.use_double_line = use_double_line

    def is_sub_account_display(self):
        return self.display_subacc

    def do_get_column_type(self, index):
        if index == RegisterModelColumn.GUID:
            return str
        elif index == RegisterModelColumn.ENTER_DATE:
            return int
        elif index in [RegisterModelColumn.DATE,
                       RegisterModelColumn.DUEDATE,
                       RegisterModelColumn.NUMACT,
                       RegisterModelColumn.DESCNOTES,
                       RegisterModelColumn.TRANSFERVOID,
                       RegisterModelColumn.RECN,
                       RegisterModelColumn.DEBIT,
                       RegisterModelColumn.CREDIT]:
            return str
        elif index in [RegisterModelColumn.RO,
                       RegisterModelColumn.NUM_VIS,
                       RegisterModelColumn.ACT_VIS]:
            return bool
        return GObject.TYPE_INVALID

    def do_get_iter(self, path):
        depth = path.get_depth()
        indices = path.get_indices()
        try:
            trans = self.tlist[indices[0]]
        except IndexError:
            _iter = Gtk.TreeIter()
            _iter.stamp = 0
            return False, _iter
        if depth == 1 or depth == 2:
            flags = TROW1 if depth == 1 else TROW2
            if trans == self.btrans:
                flags |= BLANK
                if len(trans) == 0:
                    if trans == self.bsplit_parent_node:
                        i = 0
                    else:
                        i = -1
                else:
                    i = 0
            else:
                i = 0
            _iter = self.make_iter(flags, indices[0], i)
            return True, _iter

        elif depth == 3:
            flags = SPLIT
            if trans == self.bsplit_parent_node and len(trans) == indices[2]:
                flags |= BLANK
                split = self.bsplit
                i = -1
            else:
                split = trans.get_split(indices[2])
                i = indices[2]
            if split is None:
                _iter = Gtk.TreeIter()
                _iter.stamp = 0
                return False, _iter
            _iter = self.make_iter(flags, indices[0], i)
            return True, _iter
        else:
            _iter = Gtk.TreeIter()
            _iter.stamp = 0
            return False, _iter

    def do_get_path(self, _iter):
        path = Gtk.TreePath.new()
        tpos = _iter.user_data2
        path.append_index(tpos)
        trans = self.tlist[tpos]
        if IS_TROW2(_iter):
            path.append_index(0)
        if IS_SPLIT(_iter):
            if trans == self.bsplit_parent_node and IS_BLANK(_iter):
                spos = len(trans)
            else:
                spos = _iter.user_data3
            if spos == -1:
                return None
            path.append_index(0)
            path.append_index(spos)
        return path

    def get_numact_vis(self, trow1, trow2):
        if trow1:
            return True
        if trow2:
            if self.book.use_split_action_for_num_field():
                return True
        return False

    def get_read_only(self, trans):
        if trans is None:
            return True
        if self.book.is_readonly():
            return True
        if self.read_only:
            return True
        if trans.has_split_in_state(SplitState.VOIDED):
            return True
        if trans.is_readonly():
            return True
        if self.book.uses_auto_readonly():
            if trans == self.btrans:
                return False
            else:
                return trans.is_readonly_by_post_date()
        return False

    def get_row_color(self, is_trow1, is_trow2, is_split, num):
        cell_color = None
        if self.use_gs_color_theme:
            if self.use_double_line:
                if self.alt_colors_by_transaction:
                    if num % 2 == 0:
                        if is_trow1 or is_trow2:
                            cell_color = GREENROW
                    else:
                        if is_trow1 or is_trow2:
                            cell_color = TANROW

                else:
                    if is_trow1:
                        cell_color = GREENROW
                    elif is_trow2:
                        cell_color = TANROW
            else:
                if num % 2 == 0:
                    if is_trow1:
                        cell_color = GREENROW
                    elif is_trow2:
                        cell_color = TANROW

                else:
                    if is_trow1:
                        cell_color = TANROW
                    elif is_trow2:
                        cell_color = GREENROW
            if is_split:
                cell_color = SPLITROW
        else:
            cell_color = None
        return cell_color

    def do_get_value(self, _iter, column):
        trans = self.tlist[_iter.user_data2]
        if column == RegisterModelColumn.RO:
            return self.get_read_only(trans)
        elif column == RegisterModelColumn.NUM_VIS:
            return self.get_numact_vis(IS_TROW1(_iter), IS_TROW2(_iter))
        elif column == RegisterModelColumn.ACT_VIS:
            return not self.get_numact_vis(IS_TROW1(_iter), IS_TROW2(_iter))
        elif column == RegisterModelColumn.ENTER_DATE:
            return trans.get_enter_date()
        else:
            return ""

    def do_iter_next(self, _iter):
        if IS_TROW2(_iter):
            _iter.stamp = 0
            return False, _iter
        next_index = _iter.user_data2 + 1
        prev_trans = self.tlist[_iter.user_data2]
        if IS_TROW1(_iter):
            flags = TROW1
            split = -1
            try:
                next_trans = self.tlist[next_index]
                if next_trans == self.btrans:
                    flags |= BLANK
                    if len(next_trans) == 0:
                        if next_trans == self.bsplit_parent_node:
                            split = -1
                    else:
                        split = 0
                else:
                    if len(next_trans) == 0:
                        split = -1
                    else:
                        split = 0
                _iter.user_data = flags
                _iter.user_data2 = next_index
                _iter.user_data3 = split
                return True, _iter
            except IndexError:
                _iter.stamp = 0
                return False, _iter

        elif IS_SPLIT(_iter):
            flags = SPLIT
            if IS_BLANK(_iter):
                _iter.stamp = 0
                return False, _iter

            i = _iter.user_data3 + 1
            if i < len(prev_trans):
                if prev_trans == self.bsplit_parent_node:
                    i = -1
                    flags |= BLANK
            else:
                _iter.stamp = 0
                return False, _iter
            _iter.user_data = flags
            _iter.user_data3 = i
            return True, _iter

    def do_iter_parent(self, child):
        flags = TROW1
        trans = self.tlist[child.user_data2]
        if IS_TROW1(child):
            child.stamp = 0
            return False, child
        if IS_TROW2(child):
            flags = TROW1
        if IS_SPLIT(child):
            flags = TROW2
        if trans == self.btrans:
            flags |= BLANK
        child.user_data = flags
        return True, child

    def get_trans_from_iter(self, _iter):
        if _iter is None:
            return None
        try:
            return self.tlist[_iter.user_data2]
        except ValueError:
            return None

    def get_split_from_iter_and_trans(self, _iter: Gtk.TreeIter, trans: Transaction):
        if _iter is None or trans is None:
            return None
        if trans == self.bsplit_parent_node and _iter.user_data3 > 10000:
            return self.bsplit
        return trans.get_split(_iter.user_data3)

    def event_handler(self, entity, event_type, ed: (EventData, Split) = None):
        if entity is None:
            return
        if entity.get_book() != self.book or not isinstance(entity, (Account, Split, Transaction)):
            return
        if isinstance(entity, Split):
            split = entity
            if event_type == EVENT_MODIFY:
                try:
                    iter1, iter2 = self.get_iter_from_trans_and_split(None, split)
                    self.changed_row_at_iter(iter1)
                    if self.anchor is not None:
                        trans = split.get_transaction()
                        if self.display_subacc:
                            find_split = trans.get_split_equal_to_ancestor(self.anchor)
                        else:
                            find_split = trans.find_split_by_account(self.anchor)

                        if find_split is None:
                            self.delete_trans(trans)
                except ValueError as e:
                    pass
                    return
        elif isinstance(entity, Transaction):
            trans = entity
            if event_type == EVENT_ITEM_ADDED:
                split = ed.node
                if split == self.bsplit:
                    return
                if len(trans) < 2:
                    return
                try:
                    iter1, iter2 = self.get_iter_from_trans_and_split(None, split)
                    self.insert_row_at_iter(iter1)
                except ValueError as e:
                    pass

            elif event_type == EVENT_ITEM_REMOVED:
                split = ed.node
                path = self.get_removal_path(trans, ed.idx)
                if path is not None:
                    if ed.idx == -1:
                        self.delete_trans(trans)
                    else:
                        self.delete_row_at_path(path)
                if split == self.bsplit:
                    self.make_blank()

            elif event_type == EVENT_MODIFY:
                if self.btrans == trans:
                    self.btrans = Transaction(self.book)
                    self.full_tlist.append(self.btrans)
                    self.number_of_trans_in_full_tlist += 1
                    self.insert_trans(self.btrans)
                try:
                    iter1, iter2 = self.get_iter_from_trans_and_split(trans, None)
                    self.changed_row_at_iter(iter1)
                    self.changed_row_at_iter(iter2)
                except ValueError as e:
                    pass
            elif event_type == EVENT_DESTROY:
                if self.btrans == trans:
                    ind = self.tlist.index(self.btrans)
                    self.btrans = Transaction(self.book)
                    self.tlist.insert(ind, self.btrans)
                    iter1 = self.make_iter(TROW1 | BLANK, ind, -1)
                    self.changed_row_at_iter(iter1)
                    iter2 = self.make_iter(TROW2 | BLANK, ind, -1)
                    self.changed_row_at_iter(iter2)
                try:
                    self.emit("selection_move_delete", trans)
                    self.delete_trans(trans)
                except ValueError as e:
                    pass

        elif isinstance(entity, Account):
            if event_type == EVENT_ITEM_ADDED:
                split: Split = ed
                acc = split.get_account()
                trans = split.get_transaction()
                if trans not in self.tlist:
                    if self.display_gl:
                        split_com = acc.get_commodity()
                        if split_com.get_namespace() != COMMODITY_NAMESPACE_NAME_TEMPLATE:
                            self.delete_trans(self.btrans)
                            self.insert_trans(trans)
                            self.insert_trans(self.btrans)
                            i = self.full_tlist.index(self.btrans)
                            self.full_tlist.insert(i, trans)
                    if (acc.has_ancestor(self.anchor) and self.display_subacc) or acc == self.anchor:
                        self.delete_trans(self.btrans)
                        self.insert_trans(trans)
                        self.insert_trans(self.btrans)
                        i = self.full_tlist.index(self.btrans)
                        self.full_tlist.insert(i, trans)
                    self.emit("refresh_view", 20)

            self.emit("refresh_status_bar")

    def do_iter_children(self, parent_iter):
        flags = 0
        split = -1
        if parent_iter is None:
            trans = self.tlist[0]
            if trans is not None:
                flags = TROW1
                if trans == self.btrans:
                    flags |= BLANK
                    if len(trans) == 0:
                        split = -1
                else:
                    split = 0
            else:
                split = 0

            iter_ = self.make_iter(flags, 0, split)
            return True, iter_

        if IS_TROW1(parent_iter):
            flags = TROW2
            trans = self.tlist[parent_iter.user_data2]
            if trans == self.btrans:
                flags |= BLANK
                if len(trans) == 0:
                    split = -1
                else:
                    split = 0
            else:
                split = 0
            parent_iter.user_data = flags
            parent_iter.user_data3 = split
            return True, parent_iter

        if IS_TROW2(parent_iter):
            trans = self.tlist[parent_iter.user_data2]
            if trans == self.btrans and trans != self.bsplit_parent_node:
                parent_iter.stamp = 0
                return False, parent_iter

            elif trans != self.btrans and len(trans) == 0 and trans != self.bsplit_parent_node:
                parent_iter.stamp = 0
                return False, parent_iter
            else:
                flags = SPLIT
                if (trans == self.btrans or len(trans) == 0) and trans == self.bsplit_parent_node:
                    flags |= BLANK
                    split = -1
                else:
                    split = 0
            parent_iter.user_data = flags
            parent_iter.user_data3 = split
            return True, parent_iter

        if IS_SPLIT(parent_iter):
            parent_iter.stamp = 0
            return False, parent_iter

    def update_query(self, query):
        from libgasstation.core.query_private import QueryOp, QUERY_DEFAULT_SORT
        p1 = []
        p2 = []

        standard = [QUERY_DEFAULT_SORT]
        if self.sort_col == RegisterModelColumn.DATE:
            if self.sort_depth == 1:
                p1 = [SPLIT_TRANS, TRANS_DATE_POSTED]
                p2 = standard

            elif self.sort_depth == 2:
                p1 = [SPLIT_TRANS, TRANS_DATE_ENTERED]
                p2 = standard

            elif self.sort_depth == 3:
                p1 = [SPLIT_DATE_RECONCILED, SPLIT_RECONCILE]
                p2 = standard

        elif self.sort_col == RegisterModelColumn.DESCNOTES:
            if self.sort_depth == 1:
                p1 = [SPLIT_TRANS, TRANS_DESCRIPTION]
                p2 = standard

            elif self.sort_depth == 2:
                p1 = [SPLIT_TRANS, TRANS_NOTES]
                p2 = standard

            elif self.sort_depth == 3:
                p1 = [SPLIT_MEMO]
                p2 = standard

        elif self.sort_col == RegisterModelColumn.NUMACT:
            if self.sort_depth == 1:
                p1 = [SPLIT_TRANS, TRANS_NUM]
                p2 = standard

            elif self.sort_depth == 2 or self.sort_depth == 3:
                p1 = [SPLIT_ACTION]
                p2 = standard
        elif self.sort_col == RegisterModelColumn.RECN:
            p1 = [SPLIT_DATE_RECONCILED, SPLIT_RECONCILE]
            p2 = standard

        elif self.sort_col == RegisterModelColumn.DEBIT or self.sort_col == RegisterModelColumn.CREDIT:
            p1 = [SPLIT_VALUE]
            p2 = standard
        else:
            p1 = standard

        if self.display_gl and self.type == RegisterModelType.GENERAL_JOURNAL:
            start = datetime.date.today() - relativedelta(months=1)
            query.add_date_match_tt(True, start, False, 0, QueryOp.AND)
        query.set_sort_order(p1, p2, None)

    def do_iter_has_child(self, _iter):
        trans = self.tlist[_iter.user_data2]
        if IS_TROW1(_iter):
            return True
        if IS_TROW2(_iter) and not IS_BLANK(_iter):
            if len(trans) != 0:
                return True
            else:
                if trans == self.bsplit_parent_node:
                    return True

        if IS_TROW2(_iter) and IS_BLANK(_iter) and trans == self.bsplit_parent_node:
            return True
        return False

    def do_iter_n_children(self, _iter):
        i = 0
        if _iter is None:
            return len(self.tlist)
        if IS_SPLIT(_iter):
            i = 0
        if IS_TROW1(_iter):
            i = 1

        if IS_TROW2(_iter):
            trans = self.tlist[_iter.user_data2]
            i = len(trans)
            if trans == self.bsplit_parent_node:
                i += 1
        return i

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        split = -1
        if parent_iter is None:
            flags = TROW1
            try:
                trans = self.tlist[n]
            except (IndexError, TypeError):
                _iter.stamp = 0
                return False, _iter

            if trans == self.btrans:
                flags |= BLANK
                if len(trans) == 0:
                    if trans == self.bsplit_parent_node:
                        split = -1
                else:
                    split = 0
            else:
                split = 0
            _iter = self.make_iter(flags, n, split)
            return True, _iter

        if IS_SPLIT(parent_iter):
            parent_iter.stamp = 0
            return False, parent_iter

        if IS_TROW1(parent_iter) and (n != 0):
            parent_iter.stamp = 0
            return False, parent_iter

        flags = TROW2
        trans = self.tlist[parent_iter.user_data2]
        if IS_TROW1(parent_iter) and IS_BLANK(parent_iter):
            flags |= BLANK

        if IS_TROW2(parent_iter) and len(trans) < n:
            parent_iter.stamp = 0
            return False, parent_iter
        else:
            if trans is not None:
                if trans == self.btrans:
                    split = -1
                elif trans == self.bsplit_parent_node and len(trans) == n:
                    flags = SPLIT | BLANK
                    split = -1
                else:
                    flags = SPLIT
                    split = n
            else:
                parent_iter.stamp = 0
                return False, parent_iter

        parent_iter.user_data = flags
        parent_iter.user_data3 = split
        return True, parent_iter

    def get_split_and_trans(self, _iter):
        is_trow1 = IS_TROW1(_iter)
        is_trow2 = IS_TROW2(_iter)
        is_split = IS_SPLIT(_iter)
        is_blank = IS_BLANK(_iter)
        trans = self.get_trans_from_iter(_iter)
        split = self.get_split_from_iter_and_trans(_iter, trans)
        return is_trow1, is_trow2, is_split, is_blank, trans, split

    def is_blank_split_parent(self, trans):
        node = self.bsplit_parent_node
        if node is None:
            return False
        if trans == self.bsplit_parent_node:
            return True
        else:
            return False

    def get_is_sub_ac(self):
        return self.display_subacc

    def set_display(self, subacc, gl):
        self.display_subacc = subacc
        self.display_gl = gl

    def get_path_to_split_and_trans(self, split, trans):
        path = Gtk.TreePath.new()
        number = self.do_iter_n_children(None) - 1
        if trans is None:
            if split is None:
                try:
                    tpos = self.tlist.index(self.btrans)
                except ValueError:
                    tpos = number
                path.append_index(tpos)
                return path
            else:
                if split == self.bsplit:
                    trans = self.bsplit_parent_node
                else:
                    trans = split.get_transaction()

        if trans is not None:
            try:
                tpos = self.tlist.index(trans)
            except ValueError:
                tpos = number
            path.append_index(tpos)

        if split is not None:
            spos = trans.get_split_index(split)
            if spos == -1:
                if self.bsplit == split:
                    spos = len(trans)
                else:
                    spos = -1
            path.append_index(0)
            if spos != -1:
                path.append_index(spos)
        return path

    def get_iter_from_trans_and_split(self, trans: Transaction, split: Split):
        if split is not None and trans is None:
            trans = split.get_transaction()
        if trans is None or self.book != trans.get_book():
            raise ValueError("transaction is not of this register or book")
        if split is not None and self.book != split.get_book():
            raise ValueError("Split is not of this book")
        if split is not None and trans is not None and not trans.still_has_split(split):
            raise ValueError("transaction still has other splits")
        if trans not in self.tlist:
            raise ValueError("transaction is not loaded in this register")
        flags1 = TROW1
        flags2 = TROW2
        if trans == self.btrans:
            flags1 |= BLANK
            flags2 |= BLANK
        if split is not None and trans is not None:
            slist = trans.get_splits()
            flags1 = SPLIT
            if split not in slist and split == self.bsplit:
                flags1 |= BLANK
                split = -1
            else:
                raise ValueError("Split doesn't belong to this Transaction")
            split = trans.get_split_index(split)
        trans = self.tlist.index(trans)
        iter1 = self.make_iter(flags1, trans, split)
        iter2 = self.make_iter(flags2, trans, split)
        return iter1, iter2

    def get_blank_split(self):
        return self.bsplit

    def get_blank_trans(self):
        return self.btrans

    def update_account_list(self):
        acct_list = self.acct_list
        acct_list.clear()
        for acc in self.book.get_root_account().get_descendants_sorted():
            if self.anchor != acc and not acc.get_placeholder():
                acct_list.append([acc.get_name(), acc.get_full_name()])

    def update_action_list(self):
        action_list = self.action_list
        action_list.clear()
        t = self.type
        if t in (RegisterModelType.BANK_REGISTER,
                 RegisterModelType.SEARCH_LEDGER):
            action_list.append(["Action Column|Deposit"])
            action_list.append(["Withdraw"])
            action_list.append(["Check"])
            action_list.append(["Interest"])
            action_list.append(["ATM Deposit"])
            action_list.append(["ATM Draw"])
            action_list.append(["Teller"])
            action_list.append(["Charge"])
            action_list.append(["Payment"])
            action_list.append(["Receipt"])
            action_list.append(["Increase"])
            action_list.append(["Decrease"])
            action_list.append(["POS"])
            action_list.append(["Phone"])
            action_list.append(["Online"])
            action_list.append(["AutoDep"])
            action_list.append(["Wire"])
            action_list.append(["Credit"])
            action_list.append(["Direct Debit"])
            action_list.append(["Transfer"])
        elif t == RegisterModelType.CASH_REGISTER:
            action_list.append(["Increase"])
            action_list.append(["Decrease"])
            action_list.append(["Buy"])
            action_list.append(["Sell"])
        elif t == RegisterModelType.ASSET_REGISTER:
            action_list.append(["Buy"])
            action_list.append(["Sell"])
            action_list.append(["Fee"])
        elif t == RegisterModelType.CREDIT_REGISTER:
            action_list.append(["ATM Deposit"])
            action_list.append(["ATM Withdraw"])
            action_list.append(["Buy"])
            action_list.append(["Credit"])
            action_list.append(["Fee"])
            action_list.append(["Interest"])
            action_list.append(["Online"])
            action_list.append(["Sell"])
        elif t == RegisterModelType.LIABILITY_REGISTER:
            action_list.append(["Buy"])
            action_list.append(["Sell"])
            action_list.append(["Loan"])
            action_list.append(["Interest"])
            action_list.append(["Payment"])
        elif t in (RegisterModelType.RECEIVABLE_REGISTER, RegisterModelType.PAYABLE_REGISTER):
            action_list.append(["Invoice"])
            action_list.append(["Payment"])
            action_list.append(["Interest"])
            action_list.append(["Credit"])
        elif t in (RegisterModelType.INCOME_LEDGER, RegisterModelType.INCOME_REGISTER):
            action_list.append(["Increase"])
            action_list.append(["Decrease"])
            action_list.append(["Buy"])
            action_list.append(["Sell"])
            action_list.append(["Interest"])
            action_list.append(["Payment"])
            action_list.append(["Rebate"])
            action_list.append(["Paycheck"])
        elif t in (RegisterModelType.EXPENSE_REGISTER, RegisterModelType.TRADING_REGISTER):
            action_list.append(["Increase"])
            action_list.append(["Decrease"])
            action_list.append(["Buy"])
            action_list.append(["Sell"])
        elif t in (RegisterModelType.EQUITY_REGISTER,
                   RegisterModelType.GENERAL_JOURNAL):
            action_list.append(["Buy"])
            action_list.append(["Sell"])
            action_list.append(["Equity"])
        elif t == (RegisterModelType.STOCK_REGISTER,
                   RegisterModelType.PORTFOLIO_LEDGER,
                   RegisterModelType.CURRENCY_REGISTER):
            action_list.append(["Buy"])
            action_list.append(["Sell"])
            action_list.append(["Price"])
            action_list.append(["Fee"])
            action_list.append(["Dividend"])
            action_list.append(["Interest"])
            action_list.append(["LTCG"])
            action_list.append(["STCG"])
            action_list.append(["Income"])
            action_list.append(["Dist"])
            action_list.append(["Action Column|Split"])
        else:
            action_list.append(["Increase"])
            action_list.append(["Decrease"])
            action_list.append(["Buy"])
            action_list.append(["Sell"])

    def update_completion_lists(self):
        description_list = self.description_list
        description_list.clear()
        memo_list = self.memo_list
        memo_list.clear()
        notes_list = self.notes_list
        notes_list.clear()
        d = []
        for trans in self.tlist:
            if trans != self.btrans:
                string = trans.get_description()
                if string and d.count(string) == 0:
                    d.append(string)
                    description_list.append([string, trans])

                string = trans.get_notes()
                if string and d.count(string) == 0:
                    d.append(string)
                    notes_list.append([string])

                for sp in trans.get_splits():
                    string = sp.get_memo()
                    if string and d.count(string) == 0:
                        d.append(string)
                        memo_list.append([string])

    def update_parent(self, path):
        if path.up():
            t, _iter = self.do_get_iter(path)
            if t:
                self.row_changed(path, _iter)
                trans = self.get_trans_from_iter(_iter)
                if IS_BLANK_TRANS(_iter) and trans == self.btrans and len(self.btrans) == 0:
                    self.row_has_child_toggled(path, _iter)

    def insert_trans(self, trans, before=False, i=0):
        if trans is None:
            return
        if before:
            self.tlist.insert(i, trans)
        else:
            self.tlist.append(trans)
            i = self.tlist.index(trans)
        _iter = self.make_iter(TROW1, i, -1)
        self.insert_row_at_iter(_iter)
        _iter = self.make_iter(TROW2, i, -1)
        self.insert_row_at_iter(_iter)
        path = self.do_get_path(_iter)
        path.up()
        _iter = self.get_iter(path)
        self.row_has_child_toggled(path, _iter)
        for s, split in enumerate(trans.get_splits()):
            if trans.still_has_split(split):
                _iter = self.make_iter(SPLIT, i, s)
                self.insert_row_at_iter(_iter)
        path.down()
        _iter = self.get_iter(path)
        self.row_has_child_toggled(path, _iter)

    def delete_trans(self, trans):
        try:
            i = self.tlist.index(trans)
        except ValueError:
            return
        if trans == self.bsplit_parent_node:
            _iter = self.make_iter(SPLIT | BLANK, i, -1)
            self.delete_row_at_iter(_iter)
            self.bsplit_parent_node = None
        for s, split in enumerate(trans.get_splits()):
            if trans.still_has_split(split):
                _iter = self.make_iter(SPLIT, i, s)
                self.delete_row_at_iter(_iter)
        _iter = self.make_iter(TROW2, i, -1)
        self.delete_row_at_iter(_iter)
        _iter = self.make_iter(TROW1, i, -1)
        self.delete_row_at_iter(_iter)
        self.tlist.remove(trans)

    def get_removal_path(self, trans, idx_of_split):
        if trans.get_book() != self.book:
            return None
        if trans is None:
            return None
        if trans in self.tlist:
            _iter = Gtk.TreeIter()
            _iter.stamp = self.stamp
            _iter.user_data = TROW1
            _iter.user_data2 = self.tlist.index(trans)
            path = self.do_get_path(_iter)
            if idx_of_split >= 0:
                path.append_index(0)
                path.append_index(idx_of_split)
            return path
        return None

    def set_blank_split_parent(self, trans, remove_only):
        _iter = Gtk.TreeIter()
        _iter.stamp = self.stamp
        try:
            if trans is None:
                trans = self.tlist[-1]
                i = self.tlist.index(trans)
            else:
                i = self.tlist.index(trans)
        except ValueError:
            return False
        bs_parent_node = self.bsplit_parent_node
        if trans != bs_parent_node or remove_only:
            moved = (bs_parent_node is not None or remove_only)
            if moved:
                _iter.user_data = SPLIT | BLANK
                _iter.user_data2 = self.tlist.index(bs_parent_node)
                _iter.user_data3 = -1
                self.delete_row_at_iter(_iter)
                self.bsplit_parent_node = None
            if not remove_only:
                self.bsplit_parent_node = trans
                _iter.user_data = SPLIT | BLANK
                _iter.user_data2 = i
                _iter.user_data3 = -1
                self.insert_row_at_iter(_iter)
                self.bsplit.reinit()
        else:
            moved = False
        return moved

    def insert_row_at_iter(self, _iter):
        if _iter is None:
            return
        path = self.do_get_path(_iter)
        if path is None:
            return
        t, _iter = self.do_get_iter(path)
        self.row_inserted(path, _iter)
        self.update_parent(path)

    def delete_row_at_iter(self, _iter):
        path = self.do_get_path(_iter)
        self.delete_row_at_path(path)

    def changed_row_at_iter(self, _iter):
        path = self.do_get_path(_iter)
        t, _iter = self.do_get_iter(path)
        if t:
            self.row_changed(path, _iter)

    def remove_blank(self, _iter):
        trans = self.get_trans_from_iter(_iter)
        if trans == self.btrans:
            self.btrans = None

    def make_blank(self):
        tnode = self.bsplit_parent_node
        if tnode is None:
            return
        self.bsplit = Split(self.book)
        _iter = Gtk.TreeIter()
        _iter.user_data = SPLIT | BLANK
        _iter.user_data2 = self.tlist.index(tnode)
        _iter.user_data3 = -1
        self.insert_row_at_iter(_iter)

    def commit_blank(self):
        bsplit = self.bsplit
        trans = self.bsplit_parent_node
        if trans is None:
            return
        i = trans.get_split_index(bsplit)
        if i == -1:
            return
        if bsplit.get_amount().is_zero():
            imb = trans.get_imbalance_value() * -1
            if not imb.is_zero():
                acct = bsplit.get_account()
                bsplit.set_value(imb)
                if acct.get_commodity().equal(trans.get_currency()):
                    amount = imb
                else:
                    convrate = trans.get_account_conv_rate()
                    amount = imb*convrate
                bsplit.set_amount(amount)
        _iter = self.make_iter(SPLIT, self.tlist.index(trans), i)
        self.changed_row_at_iter(_iter)
        self.make_blank()

    def delete_row_at_path(self, path):
        if path is None:
            return
        self.row_deleted(path)
        depth = path.get_depth()
        if depth == 2:
            self.update_parent(path)
        elif depth == 3:
            self.update_parent(path)
        else:
            t, _iter = self.do_get_iter(path)
            if t:
                trans = self.get_trans_from_iter(_iter)
                if trans == self.bsplit_parent_node:
                    self.bsplit_parent_node = None

    def dispose_cb(self):
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL, PREF_ACCOUNTING_LABELS, self.prefs_changed)
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL, PREF_ACCOUNT_SEPARATOR, self.prefs_changed)
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL_REGISTER, PREF_MAX_TRANS, self.limit_changed_cb)
        if self.event_handler_id:
            Event.unregister_handler(self.event_handler_id)
            self.event_handler_id = 0
        self.description_list = None
        self.notes_list = None
        self.memo_list = None
        self.acct_list = None
        self.action_list = None
        self.user_data = None
        self.bsplit_parent_node = None
        self.bsplit = None
        self.btrans = None
        self.current_trans = None
        self.anchor = None
        self.template_account = None
        self.sort_direction = 0

    def __del__(self):
        Tracking.forget(self)
        self.book = None
        self.tlist = None
        self.full_tlist = None

