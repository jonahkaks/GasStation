from gi import require_version

require_version("Gtk", "3.0")
from gi.repository import GObject, Gtk, GLib

ITER_IS_NAMESPACE = 1
ITER_IS_COMMODITY = 2
ITER_IS_PRICE = 3
from gasstation.utilities.ui import PrintAmountInfo
from libgasstation.core.commodity import *
from libgasstation.core.component import *

TREE_MODEL_COMMODITY_NAME = "CommodityModel"


class CommodityModelColumn(GObject.GEnum):
    NAMESPACE = 0
    MNEMONIC = 1
    USER_SYMBOL = 2
    FULLNAME = 3
    PRINTNAME = 4
    UNIQUE_NAME = 5
    CUSIP = 6
    FRACTION = 7
    QUOTE_FLAG = 8
    QUOTE_SOURCE = 9
    QUOTE_TZ = 10
    LAST_VISIBLE = QUOTE_TZ
    VISIBILITY = 11
    NUM_COLUMNS = 12


class RemoveData:
    model = None
    path = None


pending_removals = []


class CommodityModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = TREE_MODEL_COMMODITY_NAME

    def __init__(self):
        super().__init__()
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        self.print_info = PrintAmountInfo.share_places(6)
        self.cnspace = {}
        self.book = None
        self.commodity_table = None
        self.event_handler_id = 0


    @classmethod
    def new(cls, book, ct):
        item = Tracking.get_list(TREE_MODEL_COMMODITY_NAME)
        for model in item:
            if model.commodity_table == ct:
                return model
        model = GObject.new(cls)
        model.book = book
        model.commodity_table = ct
        model.event_handler_id = Event.register_handler(model.event_handler)
        Tracking.remember(model)
        return model

    def iter_is_namespace(self, _iter):
        if _iter is None: return False
        if _iter.stamp != self.stamp: return False
        return _iter.user_data == ITER_IS_NAMESPACE

    def iter_is_commodity(self, _iter):
        if _iter is None: return False
        if _iter.stamp != self.stamp: return False
        return _iter.user_data == ITER_IS_COMMODITY

    def get_namespace(self, _iter):
        if _iter is None: return None
        if not _iter.user_data: return None
        if _iter.stamp != self.stamp: return None

        if _iter.user_data != ITER_IS_NAMESPACE:
            return None
        return self.cnspace.get(str(_iter.user_data2))

    def get_commodity(self, _iter):
        if _iter is None: return None
        if not _iter.user_data: return None
        if _iter.stamp != self.stamp: return None
        if _iter.user_data != ITER_IS_COMMODITY:
            return None
        return self.cnspace.get(str(_iter.user_data2))

    def do_get_flags(self):
        return 0

    def do_get_n_columns(self):
        return CommodityModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index > CommodityModelColumn.NUM_COLUMNS or index < 0:
            return GObject.TYPE_INVALID
        if index in [
            CommodityModelColumn.MNEMONIC
            , CommodityModelColumn.NAMESPACE
            , CommodityModelColumn.FULLNAME
            , CommodityModelColumn.PRINTNAME
            , CommodityModelColumn.CUSIP
            , CommodityModelColumn.UNIQUE_NAME
            , CommodityModelColumn.QUOTE_SOURCE
            , CommodityModelColumn.QUOTE_TZ
            , CommodityModelColumn.USER_SYMBOL]:
            return str
        elif index == CommodityModelColumn.FRACTION:
            return int

        elif index == CommodityModelColumn.VISIBILITY or index == CommodityModelColumn.QUOTE_FLAG:
            return bool
        else:
            return GObject.TYPE_INVALID

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        depth = path.get_depth()
        if depth == 0:
            return False, _iter
        if depth > 2:
            return False, _iter
        ct = self.commodity_table
        if ct is None:
            return False, _iter
        ns_list = ct.get_namespaces_list()
        i = path.get_indices()[0]
        try:
            name_space = ns_list[i]
        except IndexError:
            return False, _iter

        if depth == 1:
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_NAMESPACE
            _iter.user_data2 = id(name_space)
            _iter.user_data3 = i
            self.cnspace[str(_iter.user_data2)] = name_space
            return True, _iter
        i = path.get_indices()[1]
        cm_list = name_space.get_commodity_list()
        try:
            commodity = cm_list[i]
        except IndexError:
            return False, _iter

        _iter.stamp = self.stamp
        _iter.user_data = ITER_IS_COMMODITY
        _iter.user_data2 = id(commodity)
        _iter.user_data3 = i
        self.cnspace[str(_iter.user_data2)] = commodity
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None:
            return
        if _iter.stamp != self.stamp: return None
        if self.commodity_table is None:
            return
        if _iter.user_data == ITER_IS_NAMESPACE:
            path = Gtk.TreePath.new()
            path.append_index(_iter.user_data3)
            return path
        commodity = self.cnspace.get(str(_iter.user_data2))
        if commodity is None:
            return
        ct = self.commodity_table
        ns_list = ct.get_namespaces_list()
        name_space = commodity.get_namespace_ds()
        path = Gtk.TreePath.new()
        path.append_index(ns_list.index(name_space))
        path.append_index(_iter.user_data3)
        return path

    def do_get_value(self, _iter, column):

        if _iter is None: return
        if _iter.stamp != self.stamp: return

        if _iter.user_data == ITER_IS_NAMESPACE:
            name_space = self.cnspace.get(str(_iter.user_data2))
            if column == CommodityModelColumn.NAMESPACE:
                return name_space.get_gui_name()

            elif column == CommodityModelColumn.VISIBILITY:
                return False
            elif column == CommodityModelColumn.FRACTION:
                return 0
            elif column == CommodityModelColumn.QUOTE_FLAG:
                return False
            else:
                return ""

        commodity = self.cnspace.get(str(_iter.user_data2))
        if column == CommodityModelColumn.MNEMONIC:
            return commodity.get_mnemonic() or ""
        elif column == CommodityModelColumn.FULLNAME:
            return commodity.get_full_name() or ""

        elif column == CommodityModelColumn.PRINTNAME:
            return commodity.get_printname() or ""
        elif column == CommodityModelColumn.CUSIP:
            return commodity.get_cusip() or ""
        elif column == CommodityModelColumn.UNIQUE_NAME:
            return commodity.get_unique_name() or ""
        elif column == CommodityModelColumn.FRACTION:
            return commodity.get_fraction()
        elif column == CommodityModelColumn.QUOTE_FLAG:
            return commodity.get_quote_flag()
        elif column == CommodityModelColumn.QUOTE_SOURCE:
            if commodity.get_quote_flag():
                source = commodity.get_quote_source()
                return source.get_internal_name()
            return ""
        elif column == CommodityModelColumn.QUOTE_TZ:
            if commodity.get_quote_flag():
                return commodity.get_quote_tz()
            return ""
        elif column == CommodityModelColumn.USER_SYMBOL:
            return commodity.get_nice_symbol()

        elif column == CommodityModelColumn.VISIBILITY:
            return True
        else:
            return ""

    def do_iter_next(self, _iter):
        if _iter is None:
            return False, _iter
        if _iter.user_data is None: return False, _iter
        if _iter.stamp != self.stamp: return False, _iter
        if _iter.user_data == ITER_IS_NAMESPACE:
            ct = self.commodity_table
            l = ct.get_namespaces_list()
            n = _iter.user_data3 + 1
            try:
                namespace = l[n]
                _iter.user_data2 = id(namespace)
                self.cnspace[str(_iter.user_data2)] = namespace
            except IndexError:
                return False, _iter
            _iter.user_data3 = n
            return True, _iter
        elif _iter.user_data == ITER_IS_COMMODITY:
            commodity = self.cnspace.get(str(_iter.user_data2))
            if commodity is None: return False, _iter
            name_space = commodity.get_namespace_ds()
            lis = name_space.get_commodity_list()
            n = _iter.user_data3 + 1
            try:
                com = lis[n]
                _iter.user_data2 = id(com)
                self.cnspace[str(_iter.user_data2)] = com
            except IndexError:
                return False, _iter
            _iter.user_data3 = n
            return True, _iter
        else:
            return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is None:
            ct = self.commodity_table
            l = ct.get_namespaces_list()
            if l is None or len(l) == 0:
                return False, _iter
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_NAMESPACE
            name_space = l[0]
            _iter.user_data2 = id(name_space)
            if self.cnspace.get(str(_iter.user_data2)) is None:
                self.cnspace[str(_iter.user_data2)] = name_space
            _iter.user_data3 = 0
            return True, _iter
        elif parent.user_data == ITER_IS_NAMESPACE:
            name_space = self.cnspace.get(str(parent.user_data2))
            l = name_space.get_commodity_list()
            if l is None or len(l) == 0:
                return False, _iter
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_COMMODITY
            try:
                commodity = l[0]
                _iter.user_data2 = id(commodity)
                if self.cnspace.get(str(_iter.user_data2)) is None:
                    self.cnspace[str(_iter.user_data2)] = commodity
            except IndexError:
                return False, _iter
            _iter.user_data3 = 0
            return True, _iter
        return False, _iter

    def do_iter_has_child(self, _iter):
        if _iter is None: return False
        if _iter.user_data != ITER_IS_NAMESPACE:
            return False
        name_space = self.cnspace.get(str(_iter.user_data2))
        lis = name_space.get_commodity_list()
        return any(lis)

    def do_iter_n_children(self, _iter):
        if _iter is None:
            ct = self.commodity_table
            n = len(ct.get_namespaces_list())
            return n
        if _iter.user_data == ITER_IS_NAMESPACE:
            name_space = self.cnspace.get(str(_iter.user_data2))
            n = len(name_space.get_commodity_list())
            return n
        return 0

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def do_iter_nth_child(self, parent, n):
        _iter = Gtk.TreeIter()
        if parent is None:
            ct = self.commodity_table
            lis = ct.get_namespaces_list()
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_NAMESPACE
            _iter.user_data2 = id(lis[n])
            self.cnspace[str(_iter.user_data2)] = lis[n]
            _iter.user_data3 = n
            return True, _iter

        if parent.user_data == ITER_IS_NAMESPACE:
            name_space = self.cnspace.get(str(parent.user_data2))
            l = name_space.get_commodity_list()
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_COMMODITY
            _iter.user_data2 = id(l[n])
            self.cnspace[str(_iter.user_data2)] = l[n]
            _iter.user_data3 = n
            return True, _iter

        _iter.stamp = 0
        return False, _iter

    def do_iter_parent(self, child):

        _iter = Gtk.TreeIter()
        if child is None: return False, _iter
        if child.user_data == ITER_IS_NAMESPACE:
            return False, _iter

        ct = self.commodity_table
        lis = ct.get_namespaces_list()
        commodity = self.cnspace.get(child.user_data2)
        name_space = commodity.get_namespace_ds()
        _iter.stamp = self.stamp
        _iter.user_data = ITER_IS_NAMESPACE
        _iter.user_data2 = id(name_space)
        _iter.user_data3 = lis.index(name_space)
        return True, _iter

    def get_iter_from_commodity(self, commodity):
        _iter = Gtk.TreeIter()
        if commodity is None:
            return False, _iter
        name_space = commodity.get_namespace_ds()
        if name_space is None:
            return False, _iter
        lis = name_space.get_commodity_list()
        if lis is None:
            return False, _iter
        try:
            n = lis.index(commodity)
        except ValueError:
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = ITER_IS_COMMODITY
        _iter.user_data2 = id(commodity)
        if self.cnspace.get(str(_iter.user_data2)) is None:
            self.cnspace[str(_iter.user_data2)] = commodity
        _iter.user_data3 = n
        return True, _iter

    def get_path_from_commodity(self, commodity):
        if commodity is None:
            return None
        t, _iter = self.get_iter_from_commodity(commodity)
        if not t:
            return None
        tree_path = self.get_path(_iter)
        return tree_path

    def get_iter_from_namespace(self, name_space):
        _iter = Gtk.TreeIter()
        if name_space is None:
            return False, _iter
        ct = self.commodity_table
        l = ct.get_namespaces_list()
        if l is None:
            return False, _iter
        try:
            n = l.index(name_space)
        except IndexError:
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = ITER_IS_NAMESPACE
        _iter.user_data2 = id(name_space)
        if self.cnspace.get(str(_iter.user_data2)) is None:
            self.cnspace[str(_iter.user_data2)] = name_space
        _iter.user_data3 = n
        return True, _iter

    def row_add(self, _iter):
        self.increment_stamp()
        _iter.stamp = self.stamp
        path = self.get_path(_iter)
        self.row_inserted(path, _iter)
        if path.up() and path.get_depth() > 0:
            tmp_iter = self.get_iter(path)
            if tmp_iter is not None:
                self.row_changed(path, tmp_iter)
            if self.iter_n_children(tmp_iter) == 1:
                self.row_has_child_toggled(path, tmp_iter)
            while path.up() and path.get_depth() > 0:
                tmp_iter = self.get_iter(path)
                if tmp_iter is not None:
                    self.row_changed(path, tmp_iter)
        if self.iter_has_child(_iter):
            path = self.do_get_path(_iter)
            self.row_has_child_toggled(path, _iter)

    def row_delete(self, path):
        if path is None:
            return
        self.row_deleted(path)
        _iter = None
        if path.up() and path.get_depth() > 0:
            _iter = self.get_iter(path)
            if _iter is not None and self.iter_has_child(_iter):
                self.row_has_child_toggled(path, _iter)

    @staticmethod
    def do_deletions():
        global pending_removals
        for data in pending_removals:
            data.model.row_delete(data.path)
            pending_removals.remove(data)
        return False

    def event_handler(self, entity, event_type, *user_data):
        global pending_removals
        _iter = None
        if pending_removals is not None and len(pending_removals) > 0:
            self.do_deletions()
        if isinstance(entity, Commodity):
            commodity = entity
            if event_type != EVENT_DESTROY:
                t, _iter = self.get_iter_from_commodity(commodity)
                if not t:
                    return

        elif isinstance(entity, CommodityNameSpace):
            name_space = entity
            if event_type != EVENT_DESTROY:
                t, _iter = self.get_iter_from_namespace(name_space)
                if not t:
                    return
        else:
            return
        if event_type == EVENT_ADD:
            self.row_add(_iter)

        elif event_type == EVENT_REMOVE:
            path = self.do_get_path(_iter)
            if path is None:
                return
            data = RemoveData()
            data.model = self
            data.path = path
            pending_removals.append(data)
            GLib.idle_add(self.do_deletions, priority=GLib.PRIORITY_HIGH_IDLE)
            return

        elif event_type == EVENT_MODIFY:

            path = self.do_get_path(_iter)
            if path is None:
                return
            _iter = self.get_iter(path)
            if _iter is None:
                return
            self.row_changed(path, _iter)
            return


GObject.type_register(CommodityModel)
