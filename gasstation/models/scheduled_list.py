from gi.repository import GObject

from gasstation.utilities.ui import *
from libgasstation.core.recurrence import Recurrence


class ScheduledListModelCol(GObject.GEnum):
    NAME = 0
    ENABLED = 1
    FREQUENCY = 2
    LAST_OCCUR = 3
    NEXT_OCCUR = 4


class ScheduledListModel:
    def __init__(self):
        self.disposed = 0
        self.instances = None

        self.orig = Gtk.TreeStore(str, bool, str, str, str)
        self.real = Gtk.TreeModelSort(model=self.orig)
        # self.real.set_sort_function(ScheduledListModelCol.NAME, self._name_comparator)
        # self.real.set_sort_function(ScheduledListModelCol.ENABLED, self._enabled_comparator)
        # self.real.set_sort_function(ScheduledListModelCol.FREQUENCY, self._freq_comparator)
        # self.real.set_sort_function(ScheduledListModelCol.LAST_OCCUR, self._last_occur_comparator)
        # self.real.set_sort_function(ScheduledListModelCol.NEXT_OCCUR, self._next_occur_comparator)
        self.real.set_sort_column_id(ScheduledListModelCol.NEXT_OCCUR, Gtk.SortType.ASCENDING)

    def populate_store(self):
        for insts in self.instances.instance_list:
            frequency_str = Recurrence.list_to_compact_string(insts.scheduled.get_schedule())
            last_occur = insts.scheduled.get_last_occur_date()
            last_occur_date_buf = print_datetime(last_occur) if last_occur is not None else "never"
            next_occur_date_buf = print_datetime(
                insts.next_instance_date) if insts.next_instance_date is not None else "never"
            _iter = self.orig.append(None)
            self.orig.set(_iter,
                          [ScheduledListModelCol.NAME, ScheduledListModelCol.ENABLED, ScheduledListModelCol.FREQUENCY,
                           ScheduledListModelCol.LAST_OCCUR, ScheduledListModelCol.NEXT_OCCUR],
                          [insts.scheduled.get_name(), insts.scheduled.get_enabled(), frequency_str,
                           last_occur_date_buf,
                           next_occur_date_buf])

    def added_cb(self, instances, scheduled_added):
        self.orig.clear()
        self.populate_store()

    def updated_cb(self, instances, scheduled_updated):
        instances.update_scheduled_instances(scheduled_updated)
        self.orig.clear()
        self.populate_store()

    def removing_cb(self, instances, scheduled_removing):
        instances.remove_scheduled_instances(scheduled_removing)
        self.orig.clear()
        self.populate_store()

    @classmethod
    def new(cls, instances):
        rtn = cls()
        rtn.instances = instances
        rtn.populate_store()
        rtn.instances.connect("added", rtn.added_cb)
        rtn.instances.connect("updated", rtn.updated_cb)
        rtn.instances.connect("removing", rtn.removing_cb)
        return rtn

    def get_scheduled_instances_from_orig_iter(self, orig_iter):
        path = self.orig.get_path(orig_iter)
        if path.get_depth() > 1:
            return None
        indices = path.get_indices()
        index = indices[0]
        return self.instances.instance_list[index]

    def get_scheduled_instances(self, sort_iter):
        translated_iter = self.real.convert_iter_to_child_iter(sort_iter)
        return self.get_scheduled_instances_from_orig_iter(translated_iter)
