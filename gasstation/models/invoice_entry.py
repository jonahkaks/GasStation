from gasstation.utilities.ui import *
from libgasstation import InvoiceEntry, Invoice, ID_PRODUCT, ProductType
from .model import *

TANROW = "#F6FFDA"
GREENROW = "#BFDEB9"


class InvoiceEntryModelColumn(GObject.GEnum):
    GUID = 0
    SERVICE_DATE = 1
    PRODUCT = 2
    DESCRIPTION = 3
    QUANTITY = 4
    UOM = 5
    PRICE = 6
    PRICE_RULE = 7
    SUBTOTAL = 8
    NUM_COLUMNS = 9


TREE_MODEL_ENTRY_NAME = "InvoiceEntryModel"


class InvoiceEntryModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = "InvoiceEntryModel"
    __gsignals__ = {
        "refresh_status_bar": (GObject.SignalFlags.RUN_LAST, None, ()),
    }

    def __init__(self, invoice: Invoice, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.entry_list: List[InvoiceEntry] = []
        self.product_list = Gtk.ListStore(str, object)
        self.product_unit_list = Gtk.ListStore(str, object)
        self.is_customer_doc = False
        self.book = invoice.get_book()
        self.blank_entry = InvoiceEntry(self.book)
        self.invoice = invoice
        self.currency = invoice.get_customer().get_currency()
        self.use_gs_color_theme = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_USE_GASSTATION_COLOR_THEME)
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        self.event_id = Event.register_handler(self.event_handler_entry)

    def update_product_list(self, location):
        self.product_list.clear()
        names = list(map(lambda f: f.get_product_name(), self.entry_list))
        for product in self.book.get_collection(ID_PRODUCT).get_all():
            name = product.get_name()
            _type = product.get_type()
            if name not in names:
                if _type == ProductType.INVENTORY and not product.get_quantity_at_hand_on_date(location=location):
                    continue
                self.product_list.append([name, product])

    def get_product_list(self):
        return self.product_list

    def get_product_unit_list(self):
        self.update_product_unit_list()
        return self.product_unit_list
    def update_product_unit_list(self):
        self.product_unit_list.clear()
        prod = self.blank_entry.get_product()
        if prod is None:
            return
        uom = prod.get_unit_of_measure()
        if uom is None:
            return
        sup_unit = uom.get_supplier_unit()
        if sup_unit is None:
            return
        uom_group = sup_unit.get_group()
        for uom in uom_group.get_units():
            self.product_unit_list.append([uom.get_name(), uom])
        return self.product_list

    def make_blank(self):
        self.blank_entry = InvoiceEntry(self.book)
        self.blank_entry.set_date(datetime.date.today())
        self.insert_entry(self.blank_entry)

    def load(self, entries):
        self.clear()
        self.entry_list = entries
        self.entry_list.append(self.blank_entry)

    def get_invoice(self):
        return self.invoice

    def foreach_function(self, model, path, _iter, rr_list):
        row_ref = Gtk.TreeRowReference.new(model, path)
        rr_list.append(row_ref)
        return False

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def clear(self):
        rr_list = []
        self.foreach(self.foreach_function, rr_list)
        rr_list.reverse()
        for ref in rr_list:
            path = ref.get_path()
            if path is not None:
                self.row_deleted(path)
        self.increment_stamp()

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_iter_has_child(self, _iter):
        return False

    def do_iter_has_parent(self, _iter):
        _iter.stamp = 0
        return False, _iter

    def do_get_n_columns(self):
        return InvoiceEntryModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index >= InvoiceEntryModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID
        if index in [InvoiceEntryModelColumn.SERVICE_DATE,
                     InvoiceEntryModelColumn.PRODUCT,
                     InvoiceEntryModelColumn.DESCRIPTION,
                     InvoiceEntryModelColumn.QUANTITY,
                     InvoiceEntryModelColumn.UOM,
                     InvoiceEntryModelColumn.PRICE,
                     InvoiceEntryModelColumn.SUBTOTAL,
                     InvoiceEntryModelColumn.PRICE_RULE,
                     InvoiceEntryModelColumn.GUID]:
            return str

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if self.entry_list is None or i >= len(self.entry_list):
            _iter.stamp = 0
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = i
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return False, _iter
        if _iter.user_data < len(self.entry_list) - 1:
            _iter.user_data += 1
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            _iter.stamp = 0
            return False, _iter
        if len(self.entry_list) > 0:
            _iter.stamp = self.stamp
            _iter.user_data = 0
            return True, _iter
        else:
            return False, _iter

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        _iter.stamp = 0
        if parent_iter is not None:
            return False, _iter
        if len(self.entry_list) > n:
            _iter.stamp = self.stamp
            _iter.user_data = n
            return True, _iter

        else:
            return False, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return len(self.entry_list)
        if self.stamp != _iter.stamp:
            return -1
        return 0

    def get_row_color(self, num):
        cell_color = None
        if self.use_gs_color_theme:
            if num % 2 == 0:
                cell_color = GREENROW
            else:
                cell_color = TANROW
        return cell_color

    def do_get_value(self, _iter, column):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        entry: InvoiceEntry = self.get_entry(_iter)
        if entry is None:
            return ""
        if column == InvoiceEntryModelColumn.GUID:
            return entry.get_guid()

        elif column == InvoiceEntryModelColumn.SERVICE_DATE:
            d = entry.get_date()
            if d is None:
                d = datetime.date.today()
            return print_datetime(d)

        elif column == InvoiceEntryModelColumn.PRODUCT:
            return entry.get_product_name()

        elif column == InvoiceEntryModelColumn.DESCRIPTION:
            return entry.get_description() or ""

        elif column == InvoiceEntryModelColumn.QUANTITY:
            return "{:,.2f}".format(entry.get_quantity())

        elif column == InvoiceEntryModelColumn.UOM:
            unit = entry.get_unit_of_measure()
            return unit.get_name() if unit is not None else ""

        elif column == InvoiceEntryModelColumn.PRICE:
            return "{:,.2f}".format(entry.get_price())

        elif column == InvoiceEntryModelColumn.SUBTOTAL:
            return "{}{:,.2f}".format(self.currency.get_mnemonic(), entry.get_value())

    def get_entry(self, _iter) -> InvoiceEntry or None:
        try:
            return self.entry_list[_iter.user_data]
        except IndexError:
            return None

    def get_iter_from_entry(self, entry):
        if entry is None:
            return None
        if self.entry_list is None or len(self.entry_list) == 0:
            return None
        _iter = Gtk.TreeIter()
        try:
            i = self.entry_list.index(entry)
            _iter = Gtk.TreeIter()
            _iter.stamp = self.stamp
            _iter.user_data = i
            return _iter
        except ValueError:
            return None

    def insert_entry(self, entry):
        self.entry_list.append(entry)
        i = self.entry_list.index(entry)
        _iter = Gtk.TreeIter()
        _iter.stamp = self.stamp
        _iter.user_data = i
        path = self.do_get_path(_iter)
        self.row_inserted(path, _iter)

    def get_path_from_entry(self, entry):
        _iter = self.get_iter_from_entry(entry)
        if _iter is not None:
            return self.do_get_path(_iter)

    def event_handler_entry(self, entity, event_type, *args):
        entry = entity
        if entry.get_book() != self.book or not isinstance(entity, InvoiceEntry):
            return
        if event_type == EVENT_DESTROY:
            path = self.get_path_from_entry(entry)
            self.row_deleted(path)
            self.entry_list.remove(entry)

        elif event_type == EVENT_MODIFY:
            if entry == self.blank_entry:
                self.invoice.add_entry(entry)
                self.blank_entry = InvoiceEntry(self.book)
                self.blank_entry.begin_edit()
                self.insert_entry(self.blank_entry)
            self.emit("refresh_status_bar")
            _iter = self.get_iter_from_entry(entry)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_changed(path, _iter)

    def get_entries(self):
        return self.entry_list

    def destroy(self):
        self.entry_list = None
        self.product_list = None
        self.blank_entry = None
        Event.unregister_handler(self.event_id)


GObject.type_register(InvoiceEntryModel)
