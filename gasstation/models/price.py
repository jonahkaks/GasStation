from gi import require_version

require_version("Gtk", "3.0")
from gi.repository import GObject, Gtk, GLib

ITER_IS_NAMESPACE = 1
ITER_IS_COMMODITY = 2
ITER_IS_PRICE = 3
from gasstation.utilities.ui import PrintAmountInfo, print_datetime, sprintamount
from libgasstation.core.commodity import *
from libgasstation.core.component import *
from libgasstation.core.price_db import Price

TREE_MODEL_PRICE_NAME = "PriceModel"


class PriceModelColumn(GObject.GEnum):
    COMMODITY = 0
    CURRENCY = 1
    DATE = 2
    SOURCE = 3
    TYPE = 4
    VALUE = 5
    LAST_VISIBLE = VALUE
    VISIBILITY = 6
    NUM_COLUMNS = 7


class RemoveData:
    model = None
    path = None


pending_removals = []


class PriceModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = "PriceModel"

    def __init__(self):
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        self.print_info = PrintAmountInfo.share_places(6)
        self.cnspace = {}
        self.book = None
        self.price_db = None
        self.event_handler_id = 0
        GObject.Object.__init__(self)

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    @classmethod
    def new(cls, book, price_db):
        item = Tracking.get_list(TREE_MODEL_PRICE_NAME)
        for model in item:
            if model.price_db == price_db:
                return model
        model = GObject.new(cls)
        model.book = book
        model.price_db = price_db
        model.event_handler_id = Event.register_handler(model.event_handler)
        Tracking.remember(model)
        return model

    def iter_is_namespace(self, _iter):
        if _iter is None: return False
        if _iter.stamp != self.stamp: return False
        return _iter.user_data == ITER_IS_NAMESPACE

    def iter_is_commodity(self, _iter):
        if _iter is None: return False
        if _iter.stamp != self.stamp: return False
        return _iter.user_data == ITER_IS_COMMODITY

    def iter_is_price(self, _iter):
        if _iter is None: return False
        if _iter.stamp != self.stamp: return False
        return _iter.user_data == ITER_IS_PRICE

    def get_namespace(self, _iter):
        if _iter is None: return None
        if not _iter.user_data: return None
        if _iter.stamp != self.stamp: return None

        if _iter.user_data != ITER_IS_NAMESPACE:
            return None
        return self.cnspace.get(str(_iter.user_data2))

    def get_commodity(self, _iter):
        if _iter is None: return None
        if not _iter.user_data: return None
        if _iter.stamp != self.stamp: return None
        if _iter.user_data != ITER_IS_COMMODITY:
            return None
        return self.cnspace.get(str(_iter.user_data2))

    def get_price(self, _iter):
        if _iter is None: return None
        if not _iter.user_data: return None
        if _iter.stamp != self.stamp: return None
        if _iter.user_data != ITER_IS_PRICE:
            return None
        return self.cnspace.get(str(_iter.user_data2))

    def do_get_flags(self):
        return 0

    def do_get_n_columns(self):
        return PriceModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):

        if index > PriceModelColumn.NUM_COLUMNS or index < 0:
            return GObject.TYPE_INVALID
        if index in [
            PriceModelColumn.COMMODITY,
            PriceModelColumn.CURRENCY,
            PriceModelColumn.DATE,
            PriceModelColumn.SOURCE,
            PriceModelColumn.TYPE,
            PriceModelColumn.VALUE]:
            return str

        elif index == PriceModelColumn.VISIBILITY:
            return bool
        else:
            return GObject.TYPE_INVALID

    def do_get_iter(self, path):

        _iter = Gtk.TreeIter()
        depth = path.get_depth()

        if depth == 0:
            return False, _iter
        if depth > 3:
            return False, _iter
        if self.price_db is None:
            return False, _iter
        ct = self.book.get_data(ID_COMMODITY_TABLE)
        ns_list = ct.get_namespaces_list()
        i = path.get_indices()[0]
        try:
            name_space = ns_list[i]
        except IndexError:
            return False, _iter

        if depth == 1:
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_NAMESPACE
            _iter.user_data2 = id(name_space)
            _iter.user_data3 = i
            self.cnspace[str(_iter.user_data2)] = name_space
            return True, _iter
        i = path.get_indices()[1]
        cm_list = name_space.get_commodity_list()
        try:
            commodity = cm_list[i]
        except IndexError:
            return False, _iter
        if depth == 2:
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_COMMODITY
            _iter.user_data2 = id(commodity)
            _iter.user_data3 = i
            self.cnspace[str(_iter.user_data2)] = commodity
            return True, _iter

        i = path.get_indices()[2]

        price = self.price_db.nth_price(commodity, i)
        if price is None:
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = ITER_IS_PRICE
        _iter.user_data2 = id(price)
        _iter.user_data3 = i
        self.cnspace[str(_iter.user_data2)] = price
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None:
            return None
        if _iter.stamp != self.stamp: return None
        if self.price_db is None: return False
        if _iter.user_data == ITER_IS_NAMESPACE:
            path = Gtk.TreePath.new()
            path.append_index(_iter.user_data3)
            return path
        ct = self.book.get_data(ID_COMMODITY_TABLE)
        ns_list = ct.get_namespaces_list()
        if _iter.user_data == ITER_IS_COMMODITY:
            commodity = self.cnspace.get(str(_iter.user_data2))
            if commodity is None:
                return
            name_space = commodity.get_namespace_ds()
            path = Gtk.TreePath.new()
            path.append_index(ns_list.index(name_space))
            path.append_index(_iter.user_data3)
            return path
        price = self.cnspace.get(str(_iter.user_data2))
        commodity = price.get_commodity() if price else None
        if commodity is None:
            return
        name_space = commodity.get_namespace_ds()
        cm_list = name_space.get_commodity_list()
        path = Gtk.TreePath.new()
        path.append_index(ns_list.index(name_space))
        path.append_index(cm_list.index(commodity))
        path.append_index(_iter.user_data3)
        return path

    def do_get_value(self, _iter, column):

        if _iter is None: return
        if _iter.stamp != self.stamp: return

        if _iter.user_data == ITER_IS_NAMESPACE:
            name_space = self.cnspace.get(str(_iter.user_data2))
            if column == PriceModelColumn.COMMODITY:
                return name_space.get_gui_name()

            elif column == PriceModelColumn.VISIBILITY:
                return False
            else:
                return ""
        if _iter.user_data == ITER_IS_COMMODITY:
            commodity = self.cnspace.get(str(_iter.user_data2))
            if column == PriceModelColumn.COMMODITY:
                return commodity.get_printname() if commodity else ""
            elif column == PriceModelColumn.VISIBILITY:
                return False
            else:
                return ""
        price = self.cnspace.get(str(_iter.user_data2))
        if price is None:
            if column == PriceModelColumn.COMMODITY:
                return ""
            elif column == PriceModelColumn.VISIBILITY:
                return False
            else:
                return ""
        if column == PriceModelColumn.COMMODITY:
            commodity = price.get_commodity()
            return commodity.get_printname()
        elif column == PriceModelColumn.CURRENCY:
            commodity = price.get_currency()
            return commodity.get_printname()
        elif column == PriceModelColumn.DATE:
            return print_datetime(price.get_time64())

        elif column == PriceModelColumn.SOURCE:
            return price.get_source_string()
        elif column == PriceModelColumn.TYPE:
            return price.get_type_string()
        elif column == PriceModelColumn.VALUE:
            return sprintamount(price.get_value(), self.print_info)
        elif column == PriceModelColumn.VISIBILITY:
            return True

    def do_iter_next(self, _iter):
        if _iter is None:
            return False, _iter

        if _iter.user_data is None: return False, _iter
        if _iter.stamp != self.stamp: return False, _iter
        if _iter.user_data == ITER_IS_NAMESPACE:
            ct = self.book.get_data(ID_COMMODITY_TABLE)
            l = ct.get_namespaces_list()
            n = _iter.user_data3 + 1
            try:
                namespace = l[n]
                _iter.user_data2 = id(namespace)
                self.cnspace[str(_iter.user_data2)] = namespace
            except IndexError:
                return False, _iter
            _iter.user_data3 = n
            return True, _iter
        elif _iter.user_data == ITER_IS_COMMODITY:
            commodity = self.cnspace.get(str(_iter.user_data2))
            if commodity is None: return False, _iter
            name_space = commodity.get_namespace_ds()
            lis = name_space.get_commodity_list()
            n = _iter.user_data3 + 1
            try:
                com = lis[n]
                _iter.user_data2 = id(com)
                self.cnspace[str(_iter.user_data2)] = com
            except IndexError:
                return False, _iter
            _iter.user_data3 = n
            return True, _iter

        elif _iter.user_data == ITER_IS_PRICE:
            price = self.cnspace.get(str(_iter.user_data2))
            commodity = price.get_commodity()
            n = _iter.user_data3 + 1
            pp = self.price_db.nth_price(commodity, n)
            if pp is None:
                return False, _iter
            _iter.user_data2 = id(pp)
            self.cnspace[str(_iter.user_data2)] = pp
            _iter.user_data3 = n
            return True, _iter
        else:
            return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is None:
            ct = self.book.get_data(ID_COMMODITY_TABLE)
            l = ct.get_namespaces_list()
            if l is None or len(l) == 0:
                return False, _iter
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_NAMESPACE
            name_space = l[0]
            _iter.user_data2 = id(name_space)
            if self.cnspace.get(str(_iter.user_data2)) is None:
                self.cnspace[str(_iter.user_data2)] = name_space
            _iter.user_data3 = 0
            return True, _iter
        elif parent.user_data == ITER_IS_NAMESPACE:
            name_space = self.cnspace.get(str(parent.user_data2))
            l = name_space.get_commodity_list()
            if l is None or len(l) == 0:
                return False, _iter
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_COMMODITY
            try:
                commodity = l[0]
                _iter.user_data2 = id(commodity)
                if self.cnspace.get(str(_iter.user_data2)) is None:
                    self.cnspace[str(_iter.user_data2)] = commodity
            except IndexError:
                return False, _iter
            _iter.user_data3 = 0
            return True, _iter

        elif parent.user_data == ITER_IS_COMMODITY:
            commodity = self.cnspace.get(str(parent.user_data2))
            price = self.price_db.nth_price(commodity, 0)
            if price is None:
                return False, _iter
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_PRICE
            _iter.user_data2 = id(price)
            self.cnspace[str(_iter.user_data2)] = price
            _iter.user_data3 = 0
            return True, _iter
        return False, _iter

    def do_iter_has_child(self, _iter):
        if _iter is None: return False
        if _iter.user_data == ITER_IS_PRICE:
            return False
        if _iter.user_data == ITER_IS_NAMESPACE:
            name_space = self.cnspace.get(str(_iter.user_data2))
            if name_space is None:
                return False
            l = name_space.get_commodity_list()
            return any(l)
        if _iter.user_data == ITER_IS_COMMODITY:
            commodity = self.cnspace.get(str(_iter.user_data2))
            result = self.price_db.has_prices(commodity, None)
            return result
        return False

    def do_iter_n_children(self, _iter):
        if _iter is None:
            ct = self.book.get_data(ID_COMMODITY_TABLE)
            n = len(ct.get_namespaces_list())
            return n
        if _iter.user_data == ITER_IS_NAMESPACE:
            name_space = self.cnspace.get(str(_iter.user_data2))
            n = len(name_space.get_commodity_list())
            return n
        if _iter.user_data == ITER_IS_COMMODITY:
            commodity = self.cnspace.get(str(_iter.user_data2))
            n = self.price_db.get_num_prices(commodity)
            return n
        return 0

    def do_iter_nth_child(self, parent, n):
        _iter = Gtk.TreeIter()
        if parent is None:
            ct = self.book.get_data(ID_COMMODITY_TABLE)
            list = ct.get_namespaces_list()
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_NAMESPACE
            _iter.user_data2 = id(list[n])
            self.cnspace[str(_iter.user_data2)] = list[n]
            _iter.user_data3 = n
            return True, _iter
        if parent.user_data == ITER_IS_NAMESPACE:
            name_space = self.cnspace.get(str(parent.user_data2))
            l = name_space.get_commodity_list()
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_COMMODITY
            _iter.user_data2 = id(l[n])
            self.cnspace[str(_iter.user_data2)] = l[n]
            _iter.user_data3 = n
            return True, _iter

        if parent.user_data == ITER_IS_COMMODITY:
            commodity = self.cnspace.get(parent.user_data2)
            price = self.price_db.nth_price(commodity, n)
            if price is None:
                return False, _iter
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_PRICE
            _iter.user_data2 = id(price)
            self.cnspace[str(_iter.user_data2)] = price
            _iter.user_data3 = n
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_parent(self, child):
        _iter = Gtk.TreeIter()
        if child is None: return False, _iter
        if child.user_data == ITER_IS_NAMESPACE:
            return False, _iter
        if child.user_data == ITER_IS_COMMODITY:
            ct = self.book.get_data(ID_COMMODITY_TABLE)
            lis = ct.get_namespaces_list()
            commodity = self.cnspace.get(child.user_data2)
            name_space = commodity.get_namespace_ds()
            _iter.stamp = self.stamp
            _iter.user_data = ITER_IS_NAMESPACE
            _iter.user_data2 = id(name_space)
            _iter.user_data3 = lis.index(name_space)
            return True, _iter
        commodity = self.cnspace.get(child.user_data2)
        name_space = commodity.get_namespace_ds()
        lis = name_space.get_commodity_list()
        _iter.stamp = self.stamp
        _iter.user_data = ITER_IS_COMMODITY
        _iter.user_data2 = id(commodity)
        _iter.user_data3 = lis.index(commodity)
        return True, _iter

    def get_iter_from_price(self, price):
        _iter = Gtk.TreeIter()
        if price is None: return False, _iter
        commodity = price.get_commodity()
        if commodity is None:
            return False, _iter
        list = self.price_db.get_prices(commodity, None)
        if list is None:
            return False, _iter
        try:
            n = list.index(price)
        except ValueError:
            list.destroy()
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = ITER_IS_PRICE
        _iter.user_data2 = id(price)
        _iter.user_data3 = n
        list.destroy()
        return True, _iter

    def get_path_from_price(self, price):
        if price is None: return None
        t, _iter = self.get_iter_from_price(price)
        if not t:
            return None
        tree_path = self.get_path(_iter)
        return tree_path

    def get_iter_from_commodity(self, commodity):
        _iter = Gtk.TreeIter()
        if commodity is None: return False, _iter
        name_space = commodity.get_namespace_ds()
        if name_space is None:
            return False, _iter
        list = name_space.get_commodity_list()
        if list is None:
            return False, _iter
        try:
            n = list.index(commodity)
        except ValueError:
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = ITER_IS_COMMODITY
        _iter.user_data2 = id(commodity)
        _iter.user_data3 = n
        return True, _iter

    def get_iter_from_namespace(self, name_space):
        _iter = Gtk.TreeIter()
        if name_space is None:
            return False, _iter
        ct = self.book.get_data(ID_COMMODITY_TABLE)
        list = ct.get_namespaces_list()
        if list is None:
            return False, _iter
        try:
            n = list.index(name_space)
        except ValueError:
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = ITER_IS_NAMESPACE
        _iter.user_data2 = id(name_space)
        _iter.user_data3 = n
        return True, _iter

    def row_add(self, _iter):
        _iter.stamp = self.stamp
        path = self.get_path(_iter)
        self.row_inserted(path, _iter)
        if path.up():
            tmp_iter = self.get_iter(path)
            if path.get_depth() > 0 and tmp_iter is not None:
                self.row_changed(path, tmp_iter)
            if self.iter_n_children(tmp_iter) == 1:
                self.row_has_child_toggled(path, tmp_iter)
            while path.up():
                tmp_iter = self.get_iter(path)
                if path.get_depth() > 0 and tmp_iter is not None:
                    self.row_changed(path, tmp_iter)
        if self.iter_has_child(_iter):
            path = self.get_path(_iter)
            self.row_has_child_toggled(path, _iter)

    def row_delete(self, path):
        if path is None:
            return
        self.row_deleted(path)
        _iter = None
        if path.up():
            _iter = self.get_iter(path)
            if path.get_depth() > 0 and _iter is not None:
                self.row_changed(path, _iter)
            if not self.iter_has_child(_iter):
                self.row_has_child_toggled(path, _iter)
            while path.up():
                _iter = self.do_get_iter(path)[1]
                if path.get_depth() > 0 and _iter is not None:
                    self.row_changed(path, _iter)

    @staticmethod
    def do_deletions():
        global pending_removals
        for data in pending_removals:
            data.model.row_delete(data.path)
            pending_removals.remove(data)
        return False

    def event_handler(self, entity, event_type, *user_data):
        global pending_removals
        _iter = None
        if pending_removals is not None and len(pending_removals) > 0:
            self.do_deletions()
        if isinstance(entity, Commodity):
            commodity = entity
            if event_type != EVENT_DESTROY:
                t, _iter = self.get_iter_from_commodity(commodity)
                if not t:
                    return

        elif isinstance(entity, CommodityNameSpace):
            name_space = entity
            if event_type != EVENT_DESTROY:
                t, _iter = self.get_iter_from_namespace(name_space)
                if not t:
                    return
        elif isinstance(entity, Price):
            price = entity
            if event_type != EVENT_DESTROY:
                t, _iter = self.get_iter_from_price(price)
                if not t:
                    return
        else:
            return
        if event_type == EVENT_ADD:
            self.row_add(_iter)

        elif event_type == EVENT_REMOVE:
            path = self.do_get_path(_iter)
            if path is None:
                return
            data = RemoveData()
            data.model = self
            data.path = path
            pending_removals.append(data)
            GLib.idle_add(self.do_deletions, priority=GLib.PRIORITY_HIGH_IDLE)
            return

        elif event_type == EVENT_MODIFY:

            path = self.get_path(_iter)
            if path is None:
                return
            _iter = self.get_iter(path)
            if _iter is None:
                return
            self.row_changed(path, _iter)
            return


GObject.type_register(PriceModel)
