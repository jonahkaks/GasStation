from libgasstation.core import Nozzle, ID_NOZZLE
from libgasstation.core.event import *
from libgasstation.core.session import Session
from .model import *


class NozzleModelColumn(GObject.GEnum):
    NAME = 0
    PUMP = 1
    TANK = 2
    LOCATION = 3
    NUM_COLUMNS = 4


class NozzleModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = "NozzleModel"

    def __init__(self):
        super().__init__()
        self.nozzle_list = None
        self.book = None
        self.event_id = 0
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)

    def foreach_function(self, model, path, _iter, rr_list):
        row_ref = Gtk.TreeRowReference.new(model, path)
        rr_list.append(row_ref)
        return False

    def clear(self):
        rr_list = []
        self.foreach(self.foreach_function, rr_list)
        rr_list.reverse()
        for ref in rr_list:
            path = ref.get_path()
            if path is not None:
                self.row_deleted(path)

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_iter_has_child(self, _iter):
        return False

    def do_iter_has_parent(self, _iter):
        _iter.stamp = 0
        return False, _iter

    def do_get_n_columns(self):
        return NozzleModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index >= NozzleModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID
        if index in [NozzleModelColumn.NAME,
                     NozzleModelColumn.PUMP,
                     NozzleModelColumn.TANK,
                     NozzleModelColumn.LOCATION,
                     ]:
            return str
        return GObject.TYPE_INVALID

    @classmethod
    def new(cls):
        model = GObject.new(cls)
        model.book = Session.get_current_book()
        model.nozzle_list = model.book.get_collection(ID_NOZZLE).get_all()
        model.event_id = Event.register_handler(model.event_handler)
        return model

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if self.nozzle_list is None or i >= len(self.nozzle_list):
            _iter.stamp = 0
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = i
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return False, _iter
        if _iter.user_data < len(self.nozzle_list) - 1:
            _iter.user_data += 1
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            _iter.stamp = 0
            return False, _iter
        if len(self.nozzle_list) > 0:
            _iter.stamp = self.stamp
            _iter.user_data = 0
            return True, _iter
        else:
            return False, _iter

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        _iter.stamp = 0
        if parent_iter is not None:
            return False, _iter
        if len(self.nozzle_list) > n:
            _iter.stamp = self.stamp
            _iter.user_data = n
            return True, _iter
        else:
            return False, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return len(self.nozzle_list)
        if self.stamp != _iter.stamp:
            return -1
        return 0

    def do_get_value(self, _iter, column):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        nozzle = self.get_nozzle_at_iter(_iter)
        if nozzle is None: return
        if column == NozzleModelColumn.NAME:
            return nozzle.get_name()
        elif column == NozzleModelColumn.TANK:
            return nozzle.get_tank_str()

        elif column == NozzleModelColumn.PUMP:
            return nozzle.get_pump_str()
        elif column == NozzleModelColumn.LOCATION:
            return nozzle.get_location().get_name()
        else:
            return ""

    def get_nozzle(self, _iter):
        inv = self.get_nozzle_at_iter(_iter)
        return inv

    def get_path_from_nozzle(self, nozzle):
        _iter = self.get_iter_from_nozzle(nozzle)
        return self.do_get_path(_iter)

    def get_nozzle_at_iter(self, _iter):
        path = self.get_path(_iter)
        if path is not None and _iter.stamp == self.stamp:
            return self.get_nozzle_at_path(path)

    def get_nozzle_at_path(self, path):
        i = path.get_indices()[0]
        try:
            return self.nozzle_list[i]
        except IndexError:
            return None

    def get_iter_from_nozzle(self, nozzle):
        if nozzle is None:
            return None
        if self.nozzle_list is None or len(self.nozzle_list) == 0:
            return None
        _iter = Gtk.TreeIter()
        try:
            i = self.nozzle_list.index(nozzle)
            _iter = Gtk.TreeIter()
            _iter.stamp = self.stamp
            _iter.user_data = i
            return _iter
        except ValueError:
            return None

    def event_handler(self, entity, event_type, ed=None):
        if not isinstance(entity, Nozzle):
            return
        nozzle = entity
        if nozzle.get_book() != self.book:
            return
        if event_type == EVENT_CREATE:
            self.nozzle_list = self.book.get_collection(ID_NOZZLE).get_all()
            _iter = self.get_iter_from_nozzle(nozzle)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_inserted(path, _iter)

        elif event_type == EVENT_DESTROY:
            path = self.get_path_from_nozzle(nozzle)
            if path is None:
                return
            try:
                self.row_deleted(path)
                self.nozzle_list.remove(nozzle)
            except ValueError:
                pass

        elif event_type == EVENT_MODIFY:
            _iter = self.get_iter_from_nozzle(nozzle)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_changed(path, _iter)

    def get_category_model(self):
        return self.cat_model


GObject.type_register(NozzleModel)
