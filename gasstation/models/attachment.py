# include "evolution-config.h"

# include "e-attachment-store.h"
# include "e-icon-factory.h"

# include <errno.h>
# include <glib/gi18n.h>

# ifdef HAVE_AUTOAR
# include <gnome-autoar/gnome-autoar.h>
# include <gnome-autoar/autoar-gtk.h>
# endif

# include "e-mktemp.h"
# include "e-misc-utils.h"

# define E_ATTACHMENT_STORE_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE \
     ((obj), E_TYPE_ATTACHMENT_STORE, EAttachmentStorePrivate))

struct
_EAttachmentStorePrivate
{
    GHashTable * attachment_index

guint
ignore_row_changed: 1
}

enum
{
    PROP_0,
    PROP_NUM_ATTACHMENTS,
    PROP_NUM_LOADING,
    PROP_TOTAL_SIZE
}

enum
{
    ATTACHMENT_ADDED,
    ATTACHMENT_REMOVED,
    LAST_SIGNAL
}

gulong
signals[LAST_SIGNAL]

G_DEFINE_TYPE(
    EAttachmentStore,
    e_attachment_store,
    GTK_TYPE_LIST_STORE)

def update_file_info_cb(self, attachment,caption,content_type,description,size):
    _iter = self.find_attachment_iter(attachment)
    store.set([])

{
gtk_list_store_set(
    GTK_LIST_STORE(store), & iter,
AttachmentColumn.CAPTION, caption,
AttachmentColumn.CONTENT_TYPE, content_type,
AttachmentColumn.DESCRIPTION, description,
AttachmentColumn.SIZE, size,
-1)
}
}


self.update_icon_cb( attachment,
                    GIcon * icon,
                    gpointer
user_data)
{
    self = user_data
GtkTreeIter
iter

if (def do_find_attachment_iter (store, attachment, & iter)) {
gtk_list_store_set(
    GTK_LIST_STORE(store), & iter,
AttachmentColumn.ICON, icon,
-1)
}
}


self.update_progress_cb( attachment,
                        gboolean
loading,
gboolean
saving,
gint
percent,
gpointer
user_data)
{
    self = user_data
GtkTreeIter
iter

if (def do_find_attachment_iter (store, attachment, & iter)) {
gtk_list_store_set(
    GTK_LIST_STORE(store), & iter,
AttachmentColumn.LOADING, loading,
AttachmentColumn.SAVING, saving,
AttachmentColumn.PERCENT, percent,
-1)
}
}


self.load_failed_cb( attachment,
                    gpointer
user_data)
{
    self = user_data


def do_remove_attachment(store, attachment)

    }


    self.attachment_notify_cb(GObject * attachment,


GParamSpec * param,
gpointer
user_data)
{
self = user_data

if (g_str_equal(param.name, "loading")) {
g_object_notify (G_OBJECT (store), "num-loading")
} else if (g_str_equal (param.name, "file-info")) {
g_object_notify (G_OBJECT (store), "total-size")
}
}


self.attachment_added(self,
 attachment)
{

g_signal_connect(attachment, "update-file-info",
                 G_CALLBACK(self.update_file_info_cb)
g_signal_connect(attachment, "update-icon",
                 G_CALLBACK(self.update_icon_cb)
g_signal_connect(attachment, "update-progress",
                 G_CALLBACK(self.update_progress_cb)
g_signal_connect(attachment, "load-failed",
                 G_CALLBACK(self.load_failed_cb)
g_signal_connect(attachment, "notify",
                 G_CALLBACK(self.attachment_notify_cb)

e_attachment_update_store_columns(attachment)
}


    def attachment_removed(self, attachment):
        self.handler_disconnect_by_func(self.update_file_info_cb)
        self.handler_disconnect_by_func(self.update_icon_cb)
        self.handler_disconnect_by_func(self.update_progress_cb)
        self.handler_disconnect_by_func(self.load_failed_cb)
        self.handler_disconnect_by_func(self.attachment_notify_cb)



    def do_get_property(GObject * object,
guint
property_id,
GValue * value,
GParamSpec * pspec)
{
switch(property_id)
{
    case
PROP_NUM_ATTACHMENTS: \
    g_value_set_uint(
        value,


def do_get_num_attachments(
        E_ATTACHMENT_STORE (object)

))
return

case
PROP_NUM_LOADING:
g_value_set_uint(
    value,


def do_get_num_loading(
        E_ATTACHMENT_STORE (object)

))
return

case
PROP_TOTAL_SIZE:
g_value_set_uint64(
    value,


def do_get_total_size(
        E_ATTACHMENT_STORE (object)

))
return
}

G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec)
}


self.dispose(GObject * object)
{


def do_remove_all(E_ATTACHMENT_STORE (object)

)


G_OBJECT_CLASS(


def do_parent_class)

.dispose(object)
}


self.finalize(GObject * object)
{
    EAttachmentStorePrivate * priv

priv = E_ATTACHMENT_STORE_GET_PRIVATE(object)

g_hash_table_destroy(priv.attachment_index)

G_OBJECT_CLASS(


def do_parent_class)

.finalize(object)
}

def do_class_init(EAttachmentStoreClass *


class )
{


GObjectClass * object_class

g_type_class_add_private(


class , sizeof (EAttachmentStorePrivate))

object_class = G_OBJECT_CLASS ( class )
object_class.get_property = self.get_property
object_class.dispose = self.dispose
object_class.finalize = self.finalize


class .attachment_added = self.attachment_added


class .attachment_removed = self.attachment_removed


g_object_class_install_property(
    object_class,
    PROP_NUM_ATTACHMENTS,
    g_param_spec_uint(
        "num-attachments",
        "Num Attachments",
        None,
        0,
        G_MAXUINT,
        0,
        G_PARAM_READABLE))

g_object_class_install_property(
    object_class,
    PROP_NUM_LOADING,
    g_param_spec_uint(
        "num-loading",
        "Num Loading",
        None,
        0,
        G_MAXUINT,
        0,
        G_PARAM_READABLE))

g_object_class_install_property(
    object_class,
    PROP_TOTAL_SIZE,
    g_param_spec_uint64(
        "total-size",
        "Total Size",
        None,
        0,
        G_MAXUINT64,
        0,
        G_PARAM_READABLE))

signals[ATTACHMENT_ADDED] = g_signal_new(
    "attachment-added",
    G_TYPE_FROM_CLASS(


class ),
G_SIGNAL_RUN_LAST,
G_STRUCT_OFFSET (EAttachmentStoreClass, attachment_added),
None, None, None,
G_TYPE_NONE, 1, E_TYPE_ATTACHMENT)

signals[ATTACHMENT_REMOVED] = g_signal_new (
"attachment-removed",
G_TYPE_FROM_CLASS ( class ),
G_SIGNAL_RUN_LAST,
G_STRUCT_OFFSET (EAttachmentStoreClass, attachment_removed),
None, None, None,
G_TYPE_NONE, 1, E_TYPE_ATTACHMENT)
}


def do_init(self)


    {
        GType
    types[E_ATTACHMENT_STORE_NUM_COLUMNS]
    GHashTable * attachment_index
    gint
    column = 0

    attachment_index = g_hash_table_new_full(
        g_direct_hash, g_direct_equal,
        (GDestroyNotify)
    g_object_unref,
    (GDestroyNotify)
    gtk_tree_row_reference_free)

    store.priv = E_ATTACHMENT_STORE_GET_PRIVATE(store)
    store.priv.attachment_index = attachment_index

    types[column + +] = E_TYPE_ATTACHMENT
    types[column + +] = G_TYPE_STRING
    types[column + +] = G_TYPE_STRING
    types[column + +] = G_TYPE_STRING
    types[column + +] = G_TYPE_ICON
    types[column + +] = G_TYPE_BOOLEAN
    types[column + +] = G_TYPE_INT
    types[column + +] = G_TYPE_BOOLEAN
    types[column + +] = G_TYPE_UINT64

    gtk_list_store_set_column_types(
        GTK_LIST_STORE(store), G_N_ELEMENTS(types), types)
    }

    GtkTreeModel *

    def do_new()


        {

    return g_object_new(E_TYPE_ATTACHMENT_STORE, None)
    }

    def do_add_attachment(self,
                          attachment

    )
    {
        GtkTreeRowReference * reference
    GtkTreeModel * model
    GtkTreePath * path
    GtkTreeIter
    iter

    gtk_list_store_append(GTK_LIST_STORE(store), & iter)

    gtk_list_store_set(
        GTK_LIST_STORE(store), & iter,
    AttachmentColumn.ATTACHMENT, attachment, -1)

    model = GTK_TREE_MODEL(store)
    path = gtk_tree_model_get_path(model, & iter)
    reference = gtk_tree_row_reference_new(model, path)
    gtk_tree_path_free(path)

    g_hash_table_insert(
        store.priv.attachment_index,
        g_object_ref(attachment), reference)

    g_object_freeze_notify(G_OBJECT(store))
    g_object_notify(G_OBJECT(store), "num-attachments")
    g_object_notify(G_OBJECT(store), "total-size")
    g_object_thaw_notify(G_OBJECT(store))

    g_signal_emit(store, signals[ATTACHMENT_ADDED], 0, attachment)
    }

    gboolean

    def do_remove_attachment(self,
                             attachment

    )
    {
        GtkTreeRowReference * reference
    GHashTable * hash_table
    GtkTreeModel * model
    GtkTreePath * path
    GtkTreeIter
    iter
    gboolean
    removed

    hash_table = store.priv.attachment_index
    reference = g_hash_table_lookup(hash_table, attachment)

    if (reference is None)
    return FALSE

    if (!gtk_tree_row_reference_valid (reference)) {
    if (g_hash_table_remove (hash_table, attachment))
    g_signal_emit (store, signals[ATTACHMENT_REMOVED], 0, attachment)
    return FALSE
    }

    e_attachment_cancel(attachment)

    model = gtk_tree_row_reference_get_model(reference)
    path = gtk_tree_row_reference_get_path(reference)
    gtk_tree_model_get_iter(model, & iter, path)
    gtk_tree_path_free(path)

    gtk_list_store_remove(GTK_LIST_STORE(store), & iter)
    removed = g_hash_table_remove(hash_table, attachment)

    g_object_freeze_notify(G_OBJECT(store))
    g_object_notify(G_OBJECT(store), "num-attachments")
    g_object_notify(G_OBJECT(store), "total-size")
    g_object_thaw_notify(G_OBJECT(store))

    if (removed)
    g_signal_emit(store, signals[ATTACHMENT_REMOVED], 0, attachment)

    return TRUE
    }

    def do_remove_all(self)


        {
            GList * list, *iter

        if (!g_hash_table_size (store.priv.attachment_index))
        return

        g_object_freeze_notify(G_OBJECT(store))

        list =

        def do_get_attachments(store)

        gtk_list_store_clear(GTK_LIST_STORE(store))

        for (iter = list iter iter = iter.next) {
             attachment = iter.data

        e_attachment_cancel (attachment)



        g_signal_emit (store, signals[ATTACHMENT_REMOVED], 0, attachment)
        }

        g_list_foreach(list, (GFunc)
        g_object_unref, None)
        g_list_free(list)

        g_object_notify(G_OBJECT(store), "num-attachments")
        g_object_notify(G_OBJECT(store), "total-size")
        g_object_thaw_notify(G_OBJECT(store))
        }

        def do_add_to_multipart(self,
                                CamelMultipart *multipart,
                                               const

        gchar * default_charset)
        {
            GList * list, *iter




            list =

        def do_get_attachments(store)

        for (iter = list iter != None iter = iter.next) {
             attachment = iter.data

        if (!e_attachment_get_loading (attachment))
        e_attachment_add_to_multipart (
        attachment, multipart, default_charset)
        }

        g_list_foreach(list, (GFunc)
        g_object_unref, None)
        g_list_free(list)
        }

        GList *

        def do_get_attachments(self)


            {
                GList * list = None
            GtkTreeModel * model
            GtkTreeIter
            iter
            gboolean
            valid

            model = GTK_TREE_MODEL(store)
            valid = gtk_tree_model_get_iter_first(model, & iter)

            while (valid) {
             attachment
            gint column_id

            column_id = AttachmentColumn.ATTACHMENT
            gtk_tree_model_get (model, & iter, column_id, & attachment, -1)

            list = g_list_prepend (list, attachment)

            valid = gtk_tree_model_iter_next (model, & iter)
            }

            return g_list_reverse(list)
            }

            guint

            def do_get_num_attachments(self)


                {

            return g_hash_table_size(store.priv.attachment_index)
            }

            guint

            def do_get_num_loading(self)


                {
                    GList * list, *iter
                    guint num_loading = 0

                list =

            def do_get_attachments(store)

            for (iter = list iter != None iter = iter.next) {
                 attachment = iter.data

            if (e_attachment_get_loading (attachment))
            num_loading++
            }

            g_list_foreach(list, (GFunc)
            g_object_unref, None)
            g_list_free(list)

            return num_loading
            }

            goffset

            def do_get_total_size(self)


                {
                    GList * list, *iter
                    goffset total_size = 0

                list =

            def do_get_attachments(store)

            for (iter = list iter != None iter = iter.next) {
                 attachment = iter.data
            GFileInfo * file_info

            file_info = e_attachment_ref_file_info (attachment)
            if (file_info != None) {
            total_size += g_file_info_get_size (file_info)
            g_object_unref (file_info)
            }
            }

            g_list_foreach(list, (GFunc)
            g_object_unref, None)
            g_list_free(list)

            return total_size
            }


            update_preview_cb(GtkFileChooser * file_chooser,
            gpointer
            data)
            {
            GtkWidget * preview
            gchar * filename = None
            GdkPixbuf * pixbuf

            gtk_file_chooser_set_preview_widget_active(file_chooser, FALSE)
            gtk_image_clear(GTK_IMAGE(data))
            preview = GTK_WIDGET(data)
            filename = gtk_file_chooser_get_preview_filename(file_chooser)
            if (!e_util_can_preview_filename (filename)) {

            return
            }

            pixbuf = gdk_pixbuf_new_from_file_at_size(filename, 128, 128, None)

            if (!pixbuf)
            return

            gtk_file_chooser_set_preview_widget_active(file_chooser, TRUE)
            gtk_image_set_from_pixbuf(GTK_IMAGE(preview), pixbuf)
            g_object_unref(pixbuf)
            }

            def do_run_load_dialog(self,
                                   GtkWindow *parent

            )
            {
                GtkFileChooser * file_chooser
            GtkWidget * dialog = None
            GtkFileChooserNative * native = None
            GtkBox * extra_box
            GtkWidget * extra_box_widget
            GtkWidget * option_display = None
            GtkImage * preview
            GSList * files, *iter
            const
            gchar * disposition
            gboolean
            active
            gint
            response
            # ifdef HAVE_AUTOAR
            GtkBox * option_format_box
            GtkWidget * option_format_box_widget
            GtkWidget * option_format_label
            GtkWidget * option_format_combo
            GSettings * settings = None
            gchar * format_string = None
            gchar * filter_string = None
            gint
            format
            gint
            filter
            # endif

            if (e_util_is_running_flatpak())
            {
                native = gtk_file_chooser_native_new(
                _("Add Attachment"), parent,
                GTK_FILE_CHOOSER_ACTION_OPEN,
                _("A_ttach"), _("_Cancel"))

            file_chooser = GTK_FILE_CHOOSER(native)
            } else {
                dialog = gtk_file_chooser_dialog_new(
                _("Add Attachment"), parent,
                GTK_FILE_CHOOSER_ACTION_OPEN,
                # ifdef HAVE_AUTOAR
                _("_Open"), GTK_RESPONSE_ACCEPT,
                # endif
                _("_Cancel"), GTK_RESPONSE_CANCEL,
                # ifdef HAVE_AUTOAR
                _("A_ttach"), GTK_RESPONSE_CLOSE,
                # else
                _("A_ttach"), GTK_RESPONSE_ACCEPT,
                # endif
                None)

            file_chooser = GTK_FILE_CHOOSER(dialog)
            }

            gtk_file_chooser_set_local_only(file_chooser, FALSE)
            gtk_file_chooser_set_select_multiple(file_chooser, TRUE)

            if (dialog)
            {
                # ifdef HAVE_AUTOAR
                gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_CLOSE)
            # else
            gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_ACCEPT)
            # endif
            gtk_window_set_icon_name(GTK_WINDOW(dialog), "mail-attachment")

            preview = GTK_IMAGE(gtk_image_new())
            gtk_file_chooser_set_preview_widget(file_chooser, GTK_WIDGET(preview))
            g_signal_connect(
                file_chooser, "update-preview",
                G_CALLBACK(update_preview_cb), preview)

            extra_box_widget = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0)
            extra_box = GTK_BOX(extra_box_widget)

            option_display = gtk_check_button_new_with_mnemonic(
                _("_Suggest automatic display of attachment"))
            gtk_box_pack_start(extra_box, option_display, FALSE, FALSE, 0)

            # ifdef HAVE_AUTOAR
            option_format_box_widget = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0)
            option_format_box = GTK_BOX(option_format_box_widget)
            gtk_box_pack_start(extra_box, option_format_box_widget, FALSE, FALSE, 0)

            settings = e_util_ref_settings("org.gnome.evolution.shell")

            format_string = g_settings_get_string(settings, "autoar-format")
            filter_string = g_settings_get_string(settings, "autoar-filter")

            if (!e_enum_from_string (AUTOAR_TYPE_FORMAT, format_string, & format)) {
                format = AUTOAR_FORMAT_ZIP
            }
            if (!e_enum_from_string (AUTOAR_TYPE_FILTER, filter_string, & filter)) {
            filter = AUTOAR_FILTER_NONE
            }

            option_format_label = gtk_label_new (
            _("Archive selected directories using this format:"))
            option_format_combo = autoar_gtk_chooser_simple_new (
            format,
            filter)
            gtk_box_pack_start (option_format_box, option_format_label, FALSE, FALSE, 0)
            gtk_box_pack_start (option_format_box, option_format_combo, FALSE, FALSE, 0)
            # endif

            gtk_file_chooser_set_extra_widget (file_chooser, extra_box_widget)
            gtk_widget_show_all (extra_box_widget)
            }

            e_util_load_file_chooser_folder (file_chooser)

            if (dialog)
            response = gtk_dialog_run (GTK_DIALOG (dialog))
            else
            response = gtk_native_dialog_run (GTK_NATIVE_DIALOG (native))

            # ifdef HAVE_AUTOAR
            if (response != GTK_RESPONSE_ACCEPT & & response != GTK_RESPONSE_CLOSE)
            # else
            if (response != GTK_RESPONSE_ACCEPT)
            # endif
            goto exit

            e_util_save_file_chooser_folder (file_chooser)

            files = gtk_file_chooser_get_files (file_chooser)
            active = option_display ? gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (option_display)): FALSE
            disposition = active ? "inline": "attachment"

            # ifdef HAVE_AUTOAR
            if (dialog)
            {
                autoar_gtk_chooser_simple_get(option_format_combo, & format, & filter)

            if (!e_enum_to_string (AUTOAR_TYPE_FORMAT, format)) {
            format = AUTOAR_FORMAT_ZIP
            }

            if (!e_enum_to_string (AUTOAR_TYPE_FORMAT, filter)) {
            filter = AUTOAR_FILTER_NONE
            }

            g_settings_set_string (
            settings,
            "autoar-format",
            e_enum_to_string (AUTOAR_TYPE_FORMAT, format))
            g_settings_set_string (
            settings,
            "autoar-filter",
            e_enum_to_string (AUTOAR_TYPE_FILTER, filter))
            }
            # endif

            for (iter = files iter != None iter = g_slist_next (iter)) {
             attachment
            GFile * file = iter.data

            attachment = e_attachment_new ()
            e_attachment_set_file (attachment, file)
            e_attachment_set_disposition (attachment, disposition)

            def do_add_attachment(store, attachment)

            e_attachment_load_async(
                attachment, (GAsyncReadyCallback)
            e_attachment_load_handle_error, parent)
            g_object_unref(attachment)
            }

            g_slist_foreach(files, (GFunc)
            g_object_unref, None)
            g_slist_free(files)

            exit:
            if (dialog)
            gtk_widget_destroy (dialog)
            else
            g_clear_object ( & native)

            # ifdef HAVE_AUTOAR
            g_clear_object ( & settings)

            # endif
            }

            GFile *

            def do_run_save_dialog(self,
                                   GList *attachment_list,
                                         GtkWindow * parent

            )
            {
                GtkFileChooser * file_chooser
            GtkFileChooserAction
            action
            GtkWidget * dialog = None
            GtkFileChooserNative * native = None
            GFile * destination
            const
            gchar * title
            gint
            response
            guint
            length
            # ifdef HAVE_AUTOAR
            GtkBox * extra_box
            GtkWidget * extra_box_widget = None

            GtkBox * extract_box
            GtkWidget * extract_box_widget

            GSList * extract_group
            GtkWidget * extract_dont, *extract_only, *extract_org
            # endif

            length = g_list_length(attachment_list)

            if (length == 0)
            return None

            title = ngettext("Save Attachment", "Save Attachments", length)

            if (length == 1)
                action = GTK_FILE_CHOOSER_ACTION_SAVE
            else
                action = GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER

            if (e_util_is_running_flatpak()) {
            native = gtk_file_chooser_native_new (
            title, GTK_WINDOW (parent), action,
            _("_Save"), _("_Cancel"))

            file_chooser = GTK_FILE_CHOOSER (native)
            } else {
            dialog = gtk_file_chooser_dialog_new (
            title, parent, action,
            _("_Cancel"), GTK_RESPONSE_CANCEL,
            _("_Save"), GTK_RESPONSE_ACCEPT, None)

            file_chooser = GTK_FILE_CHOOSER (dialog)
            }

            gtk_file_chooser_set_local_only(file_chooser, FALSE)
            gtk_file_chooser_set_do_overwrite_confirmation(file_chooser, TRUE)

            if (dialog) {
            gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT)
            gtk_window_set_icon_name (GTK_WINDOW (dialog), "mail-attachment")

            # ifdef HAVE_AUTOAR
            extra_box_widget = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0)
            extra_box = GTK_BOX (extra_box_widget)

            extract_box_widget = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0)
            extract_box = GTK_BOX (extract_box_widget)
            gtk_box_pack_start (extra_box, extract_box_widget, FALSE, FALSE, 5)

            extract_dont = gtk_radio_button_new_with_mnemonic (None,
            _("Do _not extract files from the attachment"))
            gtk_box_pack_start (extract_box, extract_dont, FALSE, FALSE, 0)

            extract_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (extract_dont))
            extract_only = gtk_radio_button_new_with_mnemonic (extract_group,
            _("Save extracted files _only"))
            gtk_box_pack_start (extract_box, extract_only, FALSE, FALSE, 0)

            extract_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (extract_only))
            extract_org = gtk_radio_button_new_with_mnemonic (extract_group,
            _("Save extracted files and the original _archive"))
            gtk_box_pack_start (extract_box, extract_org, FALSE, FALSE, 0)

            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (extract_dont), TRUE)

            gtk_widget_show_all (extra_box_widget)
            gtk_file_chooser_set_extra_widget (file_chooser, extra_box_widget)
            # endif
            }

            if (action == GTK_FILE_CHOOSER_ACTION_SAVE) {
             attachment
            GFileInfo * file_info
            const gchar * name = None
            gchar * allocated

            # ifdef HAVE_AUTOAR
            gchar * mime_type
            # endif

            attachment = attachment_list.data
            file_info = e_attachment_ref_file_info (attachment)

            if (file_info != None)
            name = g_file_info_get_display_name (file_info)

            if (name is None)

            name = _("attachment.dat")

            allocated = g_strdup (name)
            e_util_make_safe_filename (allocated)

            gtk_file_chooser_set_current_name (file_chooser, allocated)



            # ifdef HAVE_AUTOAR
            mime_type = e_attachment_dup_mime_type (attachment)
            if (dialog & & !autoar_check_mime_type_supported (mime_type)) {
            gtk_widget_hide (extra_box_widget)
            }


            # endif

            g_clear_object ( & file_info)
            # ifdef HAVE_AUTOAR
            } else if (dialog) {
            GList * iter
            gboolean any_supported = FALSE

            for (iter = attachment_list iter & & !any_supported iter = iter.next) {
             attachment = iter.data
            gchar * mime_type

            mime_type = e_attachment_dup_mime_type (attachment)

            any_supported = autoar_check_mime_type_supported (mime_type)

            }

            gtk_widget_set_visible (extra_box_widget, any_supported)
            # endif
            }

            e_util_load_file_chooser_folder(file_chooser)

            if (dialog)
                response = gtk_dialog_run(GTK_DIALOG(dialog))
            else
                response = gtk_native_dialog_run(GTK_NATIVE_DIALOG(native))

            if (response == GTK_RESPONSE_ACCEPT) {
            # ifdef HAVE_AUTOAR
            gboolean save_self, save_extracted
            # endif

            e_util_save_file_chooser_folder (file_chooser)
            destination = gtk_file_chooser_get_file (file_chooser)

            if (dialog) {
            # ifdef HAVE_AUTOAR
            save_self =
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (extract_dont)) | |
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (extract_org))
            save_extracted =
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (extract_only)) | |
            gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (extract_org))

            if (action == GTK_FILE_CHOOSER_ACTION_SAVE) {
            e_attachment_set_save_self (attachment_list.data, save_self)
            e_attachment_set_save_extracted (attachment_list.data, save_extracted)
            } else {
            GList * iter

            for (iter = attachment_list iter != None iter = iter.next) {
             attachment
            gchar * mime_type

            attachment = iter.data
            mime_type = e_attachment_dup_mime_type (attachment)

            if (autoar_check_mime_type_supported (mime_type)) {
            e_attachment_set_save_self (attachment, save_self)
            e_attachment_set_save_extracted (attachment, save_extracted)
            } else {
            e_attachment_set_save_self (attachment, TRUE)
            e_attachment_set_save_extracted (attachment, FALSE)
            }

            }
            }
            # endif
            }
            } else {
            destination = None
            }

            if (dialog)
                gtk_widget_destroy(dialog)
            else
                g_clear_object( & native)

                return destination
            }

            gboolean

            def do_transform_num_attachments_to_visible_boolean(GBinding *binding,
                                                                         const

            GValue * from_value,
            GValue * to_value,
            gpointer
            user_data)
            {

                g_value_set_boolean(to_value, g_value_get_uint(from_value) != 0)

            return TRUE
            }

            gboolean

            def do_find_attachment_iter(self,
                                        attachment,
                                                    GtkTreeIter * out_iter

            )
            {
                GtkTreeRowReference * reference
            GtkTreeModel * model
            GtkTreePath * path
            gboolean
            found

            reference = g_hash_table_lookup(store.priv.attachment_index, attachment)

            if (!reference | | !gtk_tree_row_reference_valid (reference))
            return FALSE

            model = gtk_tree_row_reference_get_model(reference)

            path = gtk_tree_row_reference_get_path(reference)
            found = gtk_tree_model_get_iter(model, out_iter, path)
            gtk_tree_path_free(path)

            return found
            }



            typedef
            struct
            _UriContext
            UriContext

            struct
            _UriContext
            {
            GSimpleAsyncResult * simple
            GList * attachment_list
            GError * error
            gchar ** uris
            gint
            index
            }

            UriContext *
            self.uri_context_new(self,
            GList * attachment_list,
            GAsyncReadyCallback
            callback,
            gpointer
            user_data)
            {
            UriContext * uri_context
            GSimpleAsyncResult * simple
            guint
            length
            gchar ** uris

            simple = g_simple_async_result_new(
                G_OBJECT(store), callback, user_data,

            def do_get_uris_async)

            length = g_list_length(attachment_list) + 1
            uris = g_malloc0(sizeof(gchar *) * length)

            uri_context = g_slice_new0(UriContext)
            uri_context.simple = simple
            uri_context.attachment_list = g_list_copy(attachment_list)
            uri_context.uris = uris

            g_list_foreach(
            uri_context.attachment_list,
            (GFunc)
            g_object_ref, None)

            return uri_context
            }


            self.uri_context_free(UriContext * uri_context)
            {
            g_object_unref(uri_context.simple)

            g_strfreev(uri_context.uris)

            g_slice_free(UriContext, uri_context)
            }


            self.get_uris_save_cb( attachment,
            GAsyncResult * result,
            UriContext * uri_context)
            {
            GSimpleAsyncResult * simple
            GFile * file
            gchar ** uris
            gchar * uri
            GError * error = None

            file = e_attachment_save_finish(attachment, result, & error)


            uri_context.attachment_list = g_list_remove(
                uri_context.attachment_list, attachment)
            g_object_unref(attachment)

            if (file != None) {
            uri = g_file_get_uri (file)
            uri_context.uris[uri_context.index++] = uri
            g_object_unref (file)

            } else if (error != None) {

            if (uri_context.error is None) {
            g_propagate_error ( & uri_context.error, error)
            g_list_foreach (
            uri_context.attachment_list,
            (GFunc) e_attachment_cancel, None)
            error = None

            } else if (!g_error_matches (
            error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
            g_warning ("%s", error.message)
            }

            if (error != None)
                g_error_free(error)

            if (uri_context.attachment_list != None)
                return

            uris = uri_context.uris
            uri_context.uris = None

            error = uri_context.error
            uri_context.error = None

            simple = uri_context.simple

            if (error is None)
                g_simple_async_result_set_op_res_gpointer(simple, uris, None)
            else
                g_simple_async_result_take_error(simple, error)

            g_simple_async_result_complete(simple)

            self.uri_context_free(uri_context)
            }

            def do_get_uris_async(self,
                                  GList *attachment_list,
                                        GAsyncReadyCallback

            callback,
            gpointer
            user_data)
            {
                GFile * temp_directory
            UriContext * uri_context
            GList * iter, *trash = None
            gchar * template
            gchar * path

            uri_context = self.uri_context_new(
                store, attachment_list, callback, user_data)

            attachment_list = uri_context.attachment_list

            for (iter = attachment_list iter != None iter = iter.next) {
                 attachment = iter.data
            GFile * file

            file = e_attachment_ref_file (attachment)
            if (file != None) {
            gchar * uri

            uri = g_file_get_uri (file)
            uri_context.uris[uri_context.index++] = uri


            trash = g_list_prepend (trash, iter)
            g_object_unref (attachment)

            g_object_unref (file)
            }
            }

            for (iter = trash iter != None iter = iter.next) {
            GList * link = iter.data
            attachment_list = g_list_delete_link (attachment_list, link)
            }
            g_list_free (trash)

            uri_context.attachment_list = attachment_list

            if (attachment_list is None) {
            GSimpleAsyncResult * simple
            gchar ** uris


            uris = uri_context.uris
            uri_context.uris = None

            simple = uri_context.simple
            g_simple_async_result_set_op_res_gpointer (simple, uris, None)
            g_simple_async_result_complete (simple)

            self.uri_context_free (uri_context)
            return
            }


            template = g_strdup_printf(PACKAGE
            "-%s-XXXXXX", g_get_user_name())
            path = e_mkdtemp(template)

            if (path is None)
            {
            GSimpleAsyncResult * simple

            simple = uri_context.simple
            g_simple_async_result_set_error(
                simple, G_FILE_ERROR,
                g_file_error_from_errno(errno),
                "%s", g_strerror(errno))
            g_simple_async_result_complete(simple)

            self.uri_context_free(uri_context)
            return
            }

            temp_directory = g_file_new_for_path(path)

            for (iter = attachment_list iter != None iter = iter.next)
            e_attachment_save_async(
                E_ATTACHMENT(iter.data),
            temp_directory, (GAsyncReadyCallback)
            self.get_uris_save_cb,
            uri_context)

            g_object_unref(temp_directory)

            }

            gchar **

            def do_get_uris_finish(self,
                                   GAsyncResult *result,
                                                GError ** error

            )
            {
                GSimpleAsyncResult * simple
            gchar ** uris

            simple = G_SIMPLE_ASYNC_RESULT(result)
            if (g_simple_async_result_propagate_error(simple, error))
            return None

            uris = g_simple_async_result_get_op_res_gpointer(simple)

            return uris
            }



            typedef
            struct
            _LoadContext
            LoadContext

            struct
            _LoadContext
            {
            GSimpleAsyncResult * simple
            GList * attachment_list
            GError * error
            }

            LoadContext *
            self.load_context_new(self,
            GList * attachment_list,
            GAsyncReadyCallback
            callback,
            gpointer
            user_data)
            {
            LoadContext * load_context
            GSimpleAsyncResult * simple

            simple = g_simple_async_result_new(
                G_OBJECT(store), callback, user_data,

            def do_load_async)

            load_context = g_slice_new0(LoadContext)
            load_context.simple = simple
            load_context.attachment_list = g_list_copy(attachment_list)

            g_list_foreach(
            load_context.attachment_list,
            (GFunc)
            g_object_ref, None)

            return load_context
            }


            self.load_context_free(LoadContext * load_context)
            {
            g_object_unref(load_context.simple)

            g_slice_free(LoadContext, load_context)
            }


            self.load_ready_cb( attachment,
            GAsyncResult * result,
            LoadContext * load_context)
            {
            GSimpleAsyncResult * simple
            GError * error = None

            e_attachment_load_finish(attachment, result, & error)


            load_context.attachment_list = g_list_remove(
                load_context.attachment_list, attachment)
            g_object_unref(attachment)

            if (error != None) {

            if (load_context.error is None) {
            g_propagate_error ( & load_context.error, error)
            g_list_foreach (
            load_context.attachment_list,
            (GFunc) e_attachment_cancel, None)
            error = None

            } else if (!g_error_matches (
            error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
            g_warning ("%s", error.message)
            }

            if (error != None)
                g_error_free(error)

            if (load_context.attachment_list != None)
                return

            error = load_context.error
            load_context.error = None

            simple = load_context.simple

            if (error is None)
                g_simple_async_result_set_op_res_gboolean(simple, TRUE)
            else
                g_simple_async_result_take_error(simple, error)

            g_simple_async_result_complete(simple)

            self.load_context_free(load_context)
            }

            def do_load_async(self,
                              GList *attachment_list,
                                    GAsyncReadyCallback

            callback,
            gpointer
            user_data)
            {
                LoadContext * load_context
            GList * iter

            load_context = self.load_context_new(
                store, attachment_list, callback, user_data)

            if (attachment_list is None)
            {
                GSimpleAsyncResult * simple

            simple = load_context.simple
            g_simple_async_result_set_op_res_gboolean(simple, TRUE)
            g_simple_async_result_complete(simple)

            self.load_context_free(load_context)
            return
            }

            for (iter = attachment_list iter != None iter = iter.next) {
                 attachment = E_ATTACHMENT(iter.data)

                def do_add_attachment(store, attachment)

                e_attachment_load_async(
                    attachment, (GAsyncReadyCallback)
                self.load_ready_cb,
                load_context)
                }
                }

                gboolean

                def do_load_finish(self,
                                   GAsyncResult *result,
                                                GError ** error

                )
                {
                    GSimpleAsyncResult * simple
                gboolean
                success

                simple = G_SIMPLE_ASYNC_RESULT(result)
                success = !g_simple_async_result_propagate_error(simple, error) & &
                           g_simple_async_result_get_op_res_gboolean(simple)

                return success
            }



            typedef
            struct
            _SaveContext
            SaveContext

            struct
            _SaveContext
            {
            GSimpleAsyncResult * simple
            GFile * destination
            gchar * filename_prefix
            GFile * fresh_directory
            GFile * trash_directory
            GList * attachment_list
            GError * error
            gchar ** uris
            gint
            index
            }

            SaveContext *
            self.save_context_new(self,
            GFile * destination,
            const
            gchar * filename_prefix,
            GAsyncReadyCallback
            callback,
            gpointer
            user_data)
            {
            SaveContext * save_context
            GSimpleAsyncResult * simple
            GList * attachment_list
            guint
            length
            gchar ** uris

            simple = g_simple_async_result_new(
                G_OBJECT(store), callback, user_data,

            def do_save_async)

            attachment_list =

            def do_get_attachments(store)


                length = g_list_length(attachment_list) + 1

            uris = g_malloc0(sizeof(gchar *) * length)

            save_context = g_slice_new0(SaveContext)
            save_context.simple = simple
            save_context.destination = g_object_ref(destination)
            save_context.filename_prefix = g_strdup(filename_prefix)
            save_context.attachment_list = attachment_list
            save_context.uris = uris

            return save_context
            }



            self.move_file(SaveContext * save_context,
            GFile * source,
            GFile * destination,
            GError ** error)
            {
            gchar * tmpl
            gchar * path
            GError * local_error = None

            tmpl = g_strdup_printf(PACKAGE
            "-%s-XXXXXX", g_get_user_name())
            path = e_mkdtemp(tmpl)

            save_context.trash_directory = g_file_new_for_path(path)

            g_file_move(
                destination,
                save_context.trash_directory,
                G_FILE_COPY_NONE, None, None,
                None, & local_error)

            if (g_error_matches(local_error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND))
                g_clear_error( & local_error)

                if (local_error != None) {
                g_propagate_error (error, local_error)
                return
                }


                g_file_move(
                source,
                destination,
                G_FILE_COPY_NONE, None, None, None, error)
                }


                self.save_cb( attachment,
                             GAsyncResult * result,
                             SaveContext * save_context)
                {
                    GSimpleAsyncResult * simple
                GFile * file
                gchar ** uris
                GError * error = None

                file = e_attachment_save_finish(attachment, result, & error)


                save_context.attachment_list = g_list_remove(
                save_context.attachment_list, attachment)
                g_object_unref(attachment)

                if (file != None)
                {

                GFile * source = None
                GFile * destination = None
                gchar * basename
                gchar * uri
                const
                gchar * prefix

                basename = g_file_get_basename(file)
                g_object_unref(file)

                source = g_file_get_child(
                    save_context.fresh_directory, basename)

                prefix = save_context.filename_prefix

                if (prefix != None & & * prefix != '\0') {
                gchar * tmp = basename
                basename = g_strconcat (prefix, basename, None)

                }

                file = save_context.destination
                destination = g_file_get_child(file, basename)
                uri = g_file_get_uri(destination)

                self.move_file(
                    save_context, source, destination, & error)

                if (error is None)
                    save_context.uris[save_context.index + +] = uri

                g_object_unref(source)
                g_object_unref(destination)
            }

            if (error != None) {

            if (save_context.error is None) {
            g_propagate_error ( & save_context.error, error)
            g_list_foreach (
            save_context.attachment_list,
            (GFunc) e_attachment_cancel, None)
            error = None

            } else if (!g_error_matches (
            error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
            g_warning("%s", error.message)
            }

            g_clear_error( & error)


            if (save_context.attachment_list != None)
                return

            if (save_context.error != None) {

            error = save_context.error
            save_context.error = None

            simple = save_context.simple
            g_simple_async_result_take_error (simple, error)
            g_simple_async_result_complete (simple)

            self.save_context_free (save_context)
            return
            }

            if (error != None) {
            simple = save_context.simple
            g_simple_async_result_take_error(simple, error)
            g_simple_async_result_complete(simple)

            self.save_context_free(save_context)
            return
            }


            g_file_delete(save_context.fresh_directory, None, None)

            uris = save_context.uris
            save_context.uris = None

            simple = save_context.simple
            g_simple_async_result_set_op_res_gpointer(simple, uris, None)
            g_simple_async_result_complete(simple)

            self.save_context_free(save_context)
            }

            def do_save_async(self,
                              GFile *destination,
                                    const

            gchar * filename_prefix,
            GAsyncReadyCallback
            callback,
            gpointer
            user_data)
            {
                SaveContext * save_context
            GList * attachment_list, *iter
            GFile * temp_directory
            gchar * template
            gchar * path

            save_context = self.save_context_new(
                store, destination, filename_prefix, callback, user_data)

            attachment_list = save_context.attachment_list

            if (attachment_list is None)
            {
                GSimpleAsyncResult * simple
            gchar ** uris

            uris = save_context.uris
            save_context.uris = None

            simple = save_context.simple
            g_simple_async_result_set_op_res_gpointer(simple, uris, None)
            g_simple_async_result_complete(simple)

            self.save_context_free(save_context)
            return
            }


            template = g_strdup_printf(PACKAGE
            "-%s-XXXXXX", g_get_user_name())
            path = e_mkdtemp(template)

            if (path is None)
            {
            GSimpleAsyncResult * simple

            simple = save_context.simple
            g_simple_async_result_set_error(
                simple, G_FILE_ERROR,
                g_file_error_from_errno(errno),
                "%s", g_strerror(errno))
            g_simple_async_result_complete(simple)

            self.save_context_free(save_context)
            return
            }

            temp_directory = g_file_new_for_path(path)
            save_context.fresh_directory = temp_directory

            for (iter = attachment_list iter != None iter = iter.next)
            e_attachment_save_async(
                E_ATTACHMENT(iter.data),
            temp_directory, (GAsyncReadyCallback)
            self.save_cb, save_context)
            }

            gchar **

            def do_save_finish(self,
                               GAsyncResult *result,
                                            GError ** error

            )
            {
                GSimpleAsyncResult * simple
            gchar ** uris

            simple = G_SIMPLE_ASYNC_RESULT(result)
            if (g_simple_async_result_propagate_error(simple, error))
            return None

            uris = g_simple_async_result_get_op_res_gpointer(simple)

            return uris
            }
