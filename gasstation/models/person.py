from gi.repository import GdkPixbuf

from gasstation.utilities.ui import *
from libgasstation import Customer, Supplier, Person, AddressType, Invoice, Bill
from libgasstation.core.business import Business
from libgasstation.core.component import *
from libgasstation.core.phone import PhoneType
from libgasstation.core.staff import Staff
from .model import *
from ..utilities.avatar_utils import generate_default_avatar, round_image


class PersonModelColumn(GObject.GEnum):
    AVATAR = 0
    ID = 1
    FULL_NAME = 2
    CURRENCY = 3
    ADDRESS = 4
    PHONE = 5
    FAX = 6
    EMAIL = 7
    BALANCE = 8
    BALANCE_REPORT = 9
    NOTES = 10
    ACTIVE = 11
    LAST_VISIBLE = ACTIVE
    COLOR_BALANCE = 12
    NUM_COLUMNS = 13


TREE_MODEL_PERSON_NAME = "PersonModel"


class PersonModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = TREE_MODEL_PERSON_NAME

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        use_red = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED)
        if use_red:
            self.negative_color = "red"
        else:
            self.negative_color = None
        self.person_list = None
        self.book = None
        self.event_id = 0
        self.person_type = None
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED, self.update_color)

    def update_color(self, gsettings, key):
        use_red = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED)
        if use_red:
            self.negative_color = "red"
        else:
            self.negative_color = None

    def foreach_function(self, model, path, _iter, rr_list):
        row_ref = Gtk.TreeRowReference.new(model, path)
        rr_list.append(row_ref)
        return False

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def clear(self):
        rr_list = []
        self.foreach(self.foreach_function, rr_list)
        rr_list.reverse()
        for ref in rr_list:
            path = ref.get_path()
            if path is not None:
                self.row_deleted(path)
        self.increment_stamp()

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_iter_has_child(self, _iter):
        return False

    def do_iter_has_parent(self, _iter):
        _iter.stamp = 0
        return False, _iter

    def do_get_n_columns(self):
        return PersonModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index >= PersonModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID
        if index in [
            PersonModelColumn.FULL_NAME,
            PersonModelColumn.ID,
            PersonModelColumn.CURRENCY,
            PersonModelColumn.ADDRESS,
            PersonModelColumn.PHONE,
            PersonModelColumn.FAX,
            PersonModelColumn.EMAIL,
            PersonModelColumn.BALANCE,
            PersonModelColumn.BALANCE_REPORT,
            PersonModelColumn.NOTES,
            PersonModelColumn.COLOR_BALANCE]:
            return str
        elif index == PersonModelColumn.ACTIVE:
            return bool
        elif index == PersonModelColumn.AVATAR:
            return GdkPixbuf.Pixbuf

    @classmethod
    def new(cls, person_type):
        items = Tracking.get_list(TREE_MODEL_PERSON_NAME)
        for item in items:
            if item.person_type == person_type:
                return item
        model = GObject.new(cls)
        model.book = Session.get_current_book()
        model.person_type = person_type
        model.person_list = Business.get_person_list(model.book, person_type, True)
        model.event_id = Event.register_handler(model.event_handler_person)
        Tracking.remember(model)
        return model

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if i >= len(self.person_list):
            _iter.stamp = 0
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = i
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return False, _iter
        if _iter.user_data < len(self.person_list) - 1:
            _iter.user_data += 1
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            _iter.stamp = 0
            return False, _iter
        if len(self.person_list) > 0:
            _iter.stamp = self.stamp
            _iter.user_data = 0
            return True, _iter
        else:
            return False, _iter

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        _iter.stamp = 0
        if parent_iter is not None:
            return False, _iter
        if len(self.person_list) > n:
            _iter.stamp = self.stamp
            _iter.user_data = n
            return True, _iter

        else:
            return False, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return len(self.person_list)
        if self.stamp != _iter.stamp:
            return -1
        return 0

    def do_get_value(self, _iter, column):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        person: Person = self.get_person(_iter)
        contact = person.get_contact()

        if person is None or contact is None:
            return ""
        display_name = contact.get_display_name() or contact.get_full_name()
        if column == PersonModelColumn.AVATAR:
            source_pixbuf = None
            image_url = contact.get_image_url()
            if image_url is not None:
                try:
                    source_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(image_url,
                                                                           32, 32
                                                                           )
                except GLib.Error:
                    pass
            if source_pixbuf is None and display_name is not None:
                source_pixbuf = generate_default_avatar(display_name.upper(), 32)

            if source_pixbuf is None:
                icon_theme = Gtk.IconTheme.get_default()
                source_pixbuf = icon_theme.load_icon("avatar-default-symbolic", 32, Gtk.IconLookupFlags.FORCE_SIZE)

            return round_image(source_pixbuf)

        elif column == PersonModelColumn.FULL_NAME:
            return display_name.upper() if display_name is not None else ""

        elif column == PersonModelColumn.ID:
            return person.get_id() or ""

        elif column == PersonModelColumn.CURRENCY:
            return person.get_currency().get_full_name()

        elif column == PersonModelColumn.ADDRESS:
            addresses = list(filter(lambda a: a.type == AddressType.WORK, contact.addresses))
            return str(addresses[0]) if any(addresses) else ""

        elif column == PersonModelColumn.PHONE:
            works = list(filter(lambda a: a.type == PhoneType.WORK, contact.phones))
            return str(works[0].get_number()) if any(works) else ""

        elif column == PersonModelColumn.FAX:
            works = list(filter(lambda a: a.type == PhoneType.WORK_FAX, contact.phones))
            return str(works[0].get_number()) if any(works) else ""

        elif column == PersonModelColumn.EMAIL:
            return ",".join([ea.get_address() for ea in contact.emails])

        elif column == PersonModelColumn.BALANCE:
            bal, negative = gs_ui_person_get_print_balance(person)
            return bal
        elif column == PersonModelColumn.BALANCE_REPORT:
            bal, negative = gs_ui_person_get_print_report_balance(person)
            return bal

        elif column == PersonModelColumn.COLOR_BALANCE:
            bal, negative = gs_ui_person_get_print_balance(person)
            return self.negative_color if negative else "black"

        elif column == PersonModelColumn.NOTES:
            return person.get_notes()

        elif column == PersonModelColumn.ACTIVE:
            return person.get_active()
        return ""

    def get_person(self, _iter):
        try:
            return self.person_list[_iter.user_data]
        except IndexError:
            return

    def get_iter_from_person(self, person):
        if person is None:
            return None
        if self.person_list is None or len(self.person_list) == 0:
            return None
        _iter = Gtk.TreeIter()
        try:
            i = self.person_list.index(person)
            _iter = Gtk.TreeIter()
            _iter.stamp = self.stamp
            _iter.user_data = i
            return _iter
        except ValueError:
            return None

    def get_path_from_person(self, person):
        _iter = self.get_iter_from_person(person)
        if _iter is not None:
            return self.do_get_path(_iter)

    def event_handler_person(self, entity, event_type, ed=None):
        if event_type == EVENT_MODIFY:
            if isinstance(entity, Invoice):
                entity = entity.get_customer()
                entity.set_cached_balance(None)
            elif isinstance(entity, Bill):
                entity = entity.get_supplier()
                entity.set_cached_balance(None)

        if not isinstance(entity, (Customer, Supplier, Staff)):
            return
        elif event_type == EVENT_MODIFY:
            entity.set_cached_balance(None)

        if entity.__class__.__name__ != self.person_type:
            return
        person = entity
        if person.get_book() != self.book:
            return
        if event_type == EVENT_CREATE:
            self.person_list = Business.get_person_list(self.book, self.person_type, True)
            _iter = self.get_iter_from_person(person)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_inserted(path, _iter)

        elif event_type == EVENT_DESTROY:
            path = self.get_path_from_person(person)
            self.row_deleted(path)
            self.person_list.remove(person)

        elif event_type == EVENT_MODIFY:
            _iter = self.get_iter_from_person(person)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_changed(path, _iter)

    def destroy(self):
        if self.event_id > 0:
            Event.unregister_handler(self.event_id)
            self.event_id = 0
        self.clear()
        self.person_list = None
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED, self.update_color)
        self.stamp = 0
        self.book = None


GObject.type_register(PersonModel)
