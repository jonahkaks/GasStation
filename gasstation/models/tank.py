from libgasstation.core import Tank, ID_TANK, ID_PRODUCT
from libgasstation.core.event import *
from .model import *


class TankModelColumn(GObject.GEnum):
    NAME = 0
    INVENTORY = 1
    CAPACITY = 2
    LOCATION = 3
    NUM_COLUMNS = 4


class TankModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = "TankModel"

    def __init__(self):
        super().__init__()
        self.tank_list = None
        self.book = None
        self.event_id = 0
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        self.product_model = Gtk.ListStore(str, object)

    def get_product_model(self):
        return self.product_model

    def foreach_function(self, model, path, _iter, rr_list):
        row_ref = Gtk.TreeRowReference.new(model, path)
        rr_list.append(row_ref)
        return False

    def clear(self):
        rr_list = []
        self.foreach(self.foreach_function, rr_list)
        rr_list.reverse()
        for ref in rr_list:
            path = ref.get_path()
            if path is not None:
                self.row_deleted(path)

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_iter_has_child(self, _iter):
        return False

    def do_iter_has_parent(self, _iter):
        _iter.stamp = 0
        return False, _iter

    def do_get_n_columns(self):
        return TankModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index >= TankModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID
        if index in [TankModelColumn.NAME,
                     TankModelColumn.INVENTORY,
                     TankModelColumn.CAPACITY,
                     TankModelColumn.LOCATION]:
            return str
        return GObject.TYPE_INVALID

    @classmethod
    def new(cls):
        from libgasstation.core.session import Session
        model = GObject.new(cls)
        model.book = Session.get_current_book()
        model.tank_list = model.book.get_collection(ID_TANK).get_all()
        model.event_id = Event.register_handler(model.event_handler)
        model.book.get_collection(ID_PRODUCT).foreach(lambda t: model.product_model.append([t.get_name(), t]))
        return model

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if self.tank_list is None or i >= len(self.tank_list):
            _iter.stamp = 0
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = i
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return False, _iter
        if _iter.user_data < len(self.tank_list) - 1:
            _iter.user_data += 1
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            _iter.stamp = 0
            return False, _iter
        if len(self.tank_list) > 0:
            _iter.stamp = self.stamp
            _iter.user_data = 0
            return True, _iter
        else:
            return False, _iter

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        _iter.stamp = 0
        if parent_iter is not None:
            return False, _iter
        if len(self.tank_list) > n:
            _iter.stamp = self.stamp
            _iter.user_data = n
            return True, _iter
        else:
            return False, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return len(self.tank_list)
        if self.stamp != _iter.stamp:
            return -1
        return 0

    def do_get_value(self, _iter, column):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        tank = self.get_tank_at_iter(_iter)
        if tank is None: return
        if column == TankModelColumn.NAME:
            return tank.get_name()
        elif column == TankModelColumn.INVENTORY:
            return tank.get_product_str()
        elif column == TankModelColumn.CAPACITY:
            return "{:,.2f}".format(tank.get_capacity())
        elif column == TankModelColumn.LOCATION:
            loc = tank.get_location()
            return loc.get_name() if loc is not None else ""
        else:
            return ""

    def get_tank(self, _iter):
        inv = self.get_tank_at_iter(_iter)
        return inv

    def get_path_from_tank(self, tank):
        _iter = self.get_iter_from_tank(tank)
        return self.do_get_path(_iter)

    def get_tank_at_iter(self, _iter):
        path = self.get_path(_iter)
        if path is not None and _iter.stamp == self.stamp:
            return self.get_tank_at_path(path)

    def get_tank_at_path(self, path):
        i = path.get_indices()[0]
        try:
            return self.tank_list[i]
        except IndexError:
            return None

    def get_iter_from_tank(self, tank):
        if tank is None:
            return None
        if self.tank_list is None or len(self.tank_list) == 0:
            return None
        _iter = Gtk.TreeIter()
        try:
            i = self.tank_list.index(tank)
            _iter = Gtk.TreeIter()
            _iter.stamp = self.stamp
            _iter.user_data = i
            return _iter
        except ValueError:
            return None

    def event_handler(self, entity, event_type, ed=None):
        if not isinstance(entity, Tank):
            return
        tank = entity
        if tank.get_book() != self.book:
            return
        if event_type == EVENT_CREATE:
            self.tank_list = self.book.get_collection(ID_TANK).get_all()
            _iter = self.get_iter_from_tank(tank)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_inserted(path, _iter)

        elif event_type == EVENT_DESTROY:
            path = self.get_path_from_tank(tank)
            if path is None:
                return
            try:
                self.row_deleted(path)
                self.tank_list.remove(tank)
            except ValueError:
                pass

        elif event_type == EVENT_MODIFY:
            _iter = self.get_iter_from_tank(tank)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_changed(path, _iter)

    def dispose_cb(self):
        self.tank_list = None
        self.product_model = None


GObject.type_register(TankModel)
