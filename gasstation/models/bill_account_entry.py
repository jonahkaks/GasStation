from gasstation.utilities.ui import *
from libgasstation import BillAccountEntry
from .model import *

TANROW = "#F6FFDA"
GREENROW = "#BFDEB9"


class BillAccountEntryModelColumn(GObject.GEnum):
    GUID = 0
    SERVICE_DATE = 1
    ACCOUNT = 2
    DESCRIPTION = 3
    AMOUNT = 4
    NUM_COLUMNS = 5


TREE_MODEL_ENTRY_NAME = "BillAccountEntryModel"


class BillAccountEntryModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = "BillAccountEntryModel"
    __gsignals__ = {
        "refresh_status_bar": (GObject.SignalFlags.RUN_LAST, None, ()),
    }

    def __init__(self, bill, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.entry_list: List[BillAccountEntry] = []
        self.product_list = Gtk.ListStore(str, object)
        self.is_supplier_doc = False
        self.book = bill.get_book()
        self.blank_entry = BillAccountEntry(self.book)
        self.bill = bill
        self.currency = bill.get_supplier().get_currency()
        self.use_gs_color_theme = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_USE_GASSTATION_COLOR_THEME)
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        self.event_id = Event.register_handler(self.event_handler_entry)
        self.acct_list = Gtk.ListStore(str)

    def update_account_list(self):
        self.acct_list.clear()
        for acc in self.book.get_root_account().get_descendants_sorted():
            if acc.get_type() == AccountType.EXPENSE and not acc.get_placeholder():
                self.acct_list.append([acc.get_name()])

    def get_account_list(self):
        return self.acct_list

    def make_blank(self):
        self.blank_entry = BillAccountEntry(self.book)
        tm = datetime.datetime.now()
        self.blank_entry.date = tm
        self.insert_entry(self.blank_entry)

    def load(self, entries):
        self.clear()
        self.entry_list = None
        self.entry_list = entries
        self.entry_list.append(self.blank_entry)

    def get_bill(self):
        return self.bill

    def foreach_function(self, model, path, _iter, rr_list):
        row_ref = Gtk.TreeRowReference.new(model, path)
        rr_list.append(row_ref)
        return False

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def clear(self):
        rr_list = []
        self.foreach(self.foreach_function, rr_list)
        rr_list.reverse()
        for ref in rr_list:
            path = ref.get_path()
            if path is not None:
                self.row_deleted(path)
        self.increment_stamp()

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_iter_has_child(self, _iter):
        return False

    def do_iter_has_parent(self, _iter):
        _iter.stamp = 0
        return False, _iter

    def do_get_n_columns(self):
        return BillAccountEntryModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index >= BillAccountEntryModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID
        if index in [BillAccountEntryModelColumn.SERVICE_DATE,
                     BillAccountEntryModelColumn.ACCOUNT,
                     BillAccountEntryModelColumn.DESCRIPTION,
                     BillAccountEntryModelColumn.AMOUNT,
                     BillAccountEntryModelColumn.GUID]:
            return str

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if self.entry_list is None or i >= len(self.entry_list):
            _iter.stamp = 0
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = i
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return False, _iter
        if _iter.user_data < len(self.entry_list) - 1:
            _iter.user_data += 1
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            _iter.stamp = 0
            return False, _iter
        if len(self.entry_list) > 0:
            _iter.stamp = self.stamp
            _iter.user_data = 0
            return True, _iter
        else:
            return False, _iter

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        _iter.stamp = 0
        if parent_iter is not None:
            return False, _iter
        if len(self.entry_list) > n:
            _iter.stamp = self.stamp
            _iter.user_data = n
            return True, _iter

        else:
            return False, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return len(self.entry_list)
        if self.stamp != _iter.stamp:
            return -1
        return 0

    def get_row_color(self, num):
        cell_color = None
        if self.use_gs_color_theme:
            if num % 2 == 0:
                cell_color = GREENROW
            else:
                cell_color = TANROW
        return cell_color

    def do_get_value(self, _iter, column):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        entry: BillAccountEntry = self.get_entry(_iter)
        if entry is None:
            return ""
        if column == BillAccountEntryModelColumn.GUID:
            return entry.get_guid()

        elif column == BillAccountEntryModelColumn.SERVICE_DATE:
            return print_datetime(entry.get_date())

        elif column == BillAccountEntryModelColumn.ACCOUNT:
            acc = entry.get_account()
            return acc.get_name() if acc is not None else ""

        elif column == BillAccountEntryModelColumn.DESCRIPTION:
            return entry.get_description() or ""

        elif column == BillAccountEntryModelColumn.AMOUNT:
            return "{:,.2f}".format(entry.get_value())

    def get_entry(self, _iter) -> BillAccountEntry or None:
        try:
            return self.entry_list[_iter.user_data]
        except IndexError:
            return None

    def get_iter_from_entry(self, entry):
        if entry is None:
            return None
        if self.entry_list is None or len(self.entry_list) == 0:
            return None
        _iter = Gtk.TreeIter()
        try:
            i = self.entry_list.index(entry)
            _iter = Gtk.TreeIter()
            _iter.stamp = self.stamp
            _iter.user_data = i
            return _iter
        except ValueError:
            return None

    def insert_entry(self, entry):
        self.entry_list.append(entry)
        i = self.entry_list.index(entry)
        _iter = Gtk.TreeIter()
        _iter.stamp = self.stamp
        _iter.user_data = i
        path = self.do_get_path(_iter)
        self.row_inserted(path, _iter)

    def get_path_from_entry(self, entry):
        _iter = self.get_iter_from_entry(entry)
        if _iter is not None:
            return self.do_get_path(_iter)

    def event_handler_entry(self, entity, event_type, *args):
        entry = entity
        if entry.get_book() != self.book or not isinstance(entity, BillAccountEntry):
            return
        if event_type == EVENT_DESTROY:
            path = self.get_path_from_entry(entry)
            self.row_deleted(path)
            self.entry_list.remove(entry)

        elif event_type == EVENT_MODIFY:
            if entry == self.blank_entry:
                self.bill.add_account_entry(entry)
                self.blank_entry = BillAccountEntry(self.book)
                self.blank_entry.begin_edit()
                self.insert_entry(self.blank_entry)
            self.emit("refresh_status_bar")
            _iter = self.get_iter_from_entry(entry)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_changed(path, _iter)

    def get_entries(self):
        return self.entry_list

    def destroy(self):
        self.entry_list = None
        self.acct_list = None
        self.blank_entry = None
        Event.unregister_handler(self.event_id)


GObject.type_register(BillAccountEntryModel)
