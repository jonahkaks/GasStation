import datetime
from typing import List

from gi.repository import GObject

from libgasstation.core.scheduled_transaction import Recurrence, ScheduledTransactionRecurrence


class DenseCalendarEndType(GObject.GEnum):
    NEVER_END = 0
    END_ON_DATE = 1
    END_AFTER_N_OCCS = 2
    BAD_END = 3


class DenseCalendarModel(GObject.Object):
    __gtype_name__ = "DenseCalendarModel"
    __gsignals__ = {
        "added": (
            GObject.SignalFlags.RUN_LAST | GObject.SignalFlags.NO_RECURSE | GObject.SignalFlags.NO_HOOKS, None,
            (GObject.TYPE_INT64,)),
        "update": (
            GObject.SignalFlags.RUN_LAST | GObject.SignalFlags.NO_RECURSE | GObject.SignalFlags.NO_HOOKS, None,
            (GObject.TYPE_INT64,)),
        "removing": (
            GObject.SignalFlags.RUN_LAST | GObject.SignalFlags.NO_RECURSE | GObject.SignalFlags.NO_HOOKS, None,
            (GObject.TYPE_INT64,)),
    }

    def get_contained(self):
        raise NotImplementedError

    def get_name(self, tag):
        raise NotImplementedError

    def get_info(self, tag):
        raise NotImplementedError

    def get_instance_count(self, tag):
        raise NotImplementedError

    def get_instance(self, tag, instance_index):
        raise NotImplementedError


GObject.type_register(DenseCalendarModel)


class DenseCalendarStore(DenseCalendarModel):
    def __init__(self):
        DenseCalendarModel.__init__(self)
        self.start_date = datetime.date.today()
        self.end_type = DenseCalendarEndType.NEVER_END
        self.end_date = datetime.date.today()
        self.n_occurrences = 0
        self.name = None
        self.info = None
        self.num_marks = 0
        self.num_real_marks = 0
        self.cal_marks = []

    @classmethod
    def new(cls, num_marks):
        model = GObject.new(cls)
        model.num_marks = num_marks
        for _ in range(num_marks):
            model.cal_marks.append(datetime.date.today())
        model.num_real_marks = 0
        model.end_type = DenseCalendarEndType.NEVER_END
        model.n_occurrences = 0
        return model

    def clear(self):
        self.num_real_marks = 0
        self.emit("update", 1)

    def update_name(self, name):
        self.name = name

    def update_info(self, info):
        self.info = info

    def generic_update_recurrences(self, start,
                                   recurrences:List[ScheduledTransactionRecurrence]):
        recurrences = map(lambda a:a.recurrence, recurrences)
        n = Recurrence.list_next_instance(recurrences, start)
        i = 0
        while ((i < self.num_marks) and ((self.end_type == DenseCalendarEndType.NEVER_END)
                                         or (self.end_type == DenseCalendarEndType.END_ON_DATE
                                             and (n - self.end_date).days <= 0)
                                         or (self.end_type == DenseCalendarEndType.END_AFTER_N_OCCS
                                             and i < self.n_occurrences))):
            self.cal_marks[i] = n
            i += 1
            n = Recurrence.list_next_instance(recurrences, n)
        self.num_real_marks = i
        self.emit("update", 1)

    def update_recurrences_no_end(self, start, recurrences):
        self.end_type = DenseCalendarEndType.NEVER_END
        self.generic_update_recurrences(start, recurrences)

    def update_recurrences_count_end(self, start, recurrences, num_occur):
        self.end_type = DenseCalendarEndType.END_AFTER_N_OCCS
        self.n_occurrences = num_occur
        self.generic_update_recurrences(start, recurrences)

    def update_recurrences_date_end(self, start, recurrences, end_date):
        self.end_type = DenseCalendarEndType.END_ON_DATE
        self.end_date = end_date
        self.generic_update_recurrences(start, recurrences)

    def get_contained(self):
        return [1]

    def get_name(self, tag):
        return self.name

    def get_info(self, tag):
        return self.info

    def get_instance_count(self, tag):
        return self.num_real_marks

    def get_instance(self, tag, instance_index):
        return self.cal_marks[instance_index]
