import datetime

from dateutil.relativedelta import relativedelta
from gi.repository import GObject, GLib

from libgasstation import Split
from libgasstation.core.helpers import *
from libgasstation.core.scheduled_transaction import ScheduledTransaction
from libgasstation.core.scheduled_transaction_list import *
from libgasstation.core.scrub import Scrub
from libgasstation.core.session import Session


class ScheduledInstanceState(GObject.GEnum):
    IGNORED = 0
    POSTPONED = 1
    TO_CREATE = 2
    REMINDER = 3
    CREATED = 4
    MAX_STATE = 5


class ScheduledTxnCreationData:
    def __init__(self):
        self.instance = None
        self.created_transaction_guids = None
        self.creation_errors = None


class Summary:
    def __init__(self):
        self.need_dialog = False
        self.num_instances = 0
        self.num_to_create_instances = 0
        self.num_auto_create_instances = 0
        self.num_auto_create_no_notify_instances = 0

    def __repr__(self):
        return "Summary(num_instances: %d num_to_create: %d num_auto_create_instances: %d " \
               "num_auto_create_no_notify_instances: %d need dialog? %s" % (self.num_instances,
                                                                            self.num_to_create_instances,
                                                                            self.num_auto_create_instances,
                                                                            self.num_auto_create_no_notify_instances,
                                                                            "true" if self.need_dialog else "false")


class ScheduledVariable:
    def __init__(self, name):
        self.name = name
        self.value = Decimal(0)
        self.editable = True

    @classmethod
    def new(cls, name):
        return cls(name)

    @classmethod
    def new_full(cls, name, value, editable):
        var = cls(name)
        var.value = value
        var.editable = editable
        return var

    def copy(self):
        return self.new_full(self.name, self.value, self.editable)

    def __eq__(self, other):
        return self.name == other.name


class ScheduledAllCashflow:
    def __init__(self):
        self.hash = None
        self.creation_errors = None
        self.range_start = None
        self.range_end = None


class ScheduledInstances:
    def __init__(self):
        self.scheduled = None
        self.variable_names_parsed = False
        self.next_instance_date = None
        self.instance_list = []

    @staticmethod
    def var_to_raw_numeric(name, var, parser_var_hash):
        parser_var_hash[name] = var.value

    @staticmethod
    def var_numeric_to_scheduled_var(name, num, scheduled_var_hash):
        pvar = scheduled_var_hash.get(name)
        if pvar is None:
            pvar = ScheduledVariable(name)
            scheduled_var_hash[name] = pvar
        pvar.value = num

    def parse_vars_from_formula(self, f, h):
        parsed_vars = {k: v.value for k, v in h.items()}

    def get_variables(self, var_hash):
        scheduled_template_acct = ScheduledInstanceModel.get_template_transaction_account(self.scheduled)
        scheduled_template_acct.foreach_transaction(self._get_vars_helper, var_hash)

    def _get_vars_helper(self, transaction, var_hash):
        split_list = transaction.get_splits()
        if not any(split_list):
            return 1
        for s in split_list:
            split_is_marker = True
            first_cmdty = None
            credit_formula = s.scheduled_credit_formula
            debit_formula = s.scheduled_debit_formula
            acct = s.scheduled_account
            split_cmdty = acct.get_commodity() if acct is not None else None

            if credit_formula and len(credit_formula) != 0:
                self.parse_vars_from_formula(credit_formula, var_hash)
                split_is_marker = False
            if debit_formula and len(debit_formula) != 0:
                self.parse_vars_from_formula(debit_formula, var_hash)
                split_is_marker = False

            if not split_is_marker and first_cmdty is None:
                first_cmdty = split_cmdty

            if not split_is_marker and not split_cmdty.equal(first_cmdty):
                split_mnemonic = split_cmdty.get_mnemonic()
                first_mnemonic = first_cmdty.get_mnemonic()
                var_name = "%s -> %s" % (split_mnemonic if split_mnemonic else "(null)",
                                         first_mnemonic if first_mnemonic else "(null)")
                var = ScheduledVariable.new(var_name)
                var_hash[var_name] = var
        return 0

    @staticmethod
    def _set_var_to_random_value(key, var):
        var.value = Decimal(GLib.random_int_range(1, 1000))

    def randomize_variables(self, vars):
        for k, v in vars:
            self._set_var_to_random_value(k, v)


class ScheduledVariableNeeded:
    def __init__(self):
        self.instance = None
        self.variable = None


class ScheduledInstance:
    def __init__(self, parent: ScheduledInstances, state: int,
                 date: datetime.date, temporal_state, sequence_num: int):
        self.parent = parent
        self.temporal_state = temporal_state.clone()
        self.orig_state = state
        self.state = state
        self.date = date
        self.variable_bindings = {}
        # if not parent.variable_names_parsed:
        #     parent.variable_names = {}
        #     parent.get_variables(parent.variable_names)
        #     for k, v in parent.variable_names.items():
        #         self._wipe_parsed_scheduled_var(k, v)
        #     parent.variable_names_parsed = True
        # for k, v in parent.variable_names.items():
        #     self._clone_scheduled_var_hash_entry(k, v, self.variable_bindings)
        instance_i_value = parent.scheduled.get_instance_count(self.temporal_state)
        i_num = Decimal(instance_i_value)
        as_var = ScheduledVariable.new_full("i", i_num, False)
        self.variable_bindings["i"] = as_var

    def __repr__(self):
        return u"ScheduledInstance()"

    @staticmethod
    def _wipe_parsed_scheduled_var(key, var):
        var.value = Numeric.error(NumericErrorCode.ARG)

    @staticmethod
    def _clone_scheduled_var_hash_entry(key, to_copy, to):
        var = to_copy.copy()
        to[key] = var

    def get_variables(self):
        l = list(self.variable_bindings.values())
        l.sort()
        return l


class ScheduledInstanceModel(GObject.Object):
    __gtype_name__ = "ScheduledInstanceModel"
    __gsignals__ = {
        "removing": (GObject.SignalFlags.RUN_FIRST, None, (object,)),
        "updated": (GObject.SignalFlags.RUN_FIRST, None, (object,)),
        "added": (GObject.SignalFlags.RUN_FIRST, None, (object,)),
    }

    def __init__(self):
        super().__init__()
        self.disposed = False
        self.event_handler_id = Event.register_handler(self.event_handler)
        self.range_end = None
        self.include_disabled = False
        self.instance_list = None

    @staticmethod
    def scrub_scheduled_split_numeric(split, debcred):
        pass
        # gboolean is_credit = g_strcmp0(debcred, "credit") == 0
        # char *formula = is_credit ?
        # "scheduled-credit-formula" : "scheduled-debit-formula"
        # char *numeric = is_credit ?
        # "scheduled-credit-numeric" : "scheduled-debit-numeric"
        # char *formval
        # numeric *numval = None
        # GHashTable *parser_vars = g_hash_table_new(g_str_hash, g_str_equal)
        # char *error_loc
        # numeric amount = Numeric.zero(
        # gboolean parse_result = False
        # gboolean num_val_changed = False
        # qof_instance_get(QOF_INSTANCE (split),
        #                  formula, &formval,
        # numeric, &numval,
        #           None)
        # parse_result =
        # exp_parser_parse_separate_vars(formval, &amount,
        # &error_loc, parser_vars)
        # if (!parse_result || g_hash_table_size(parser_vars) != 0)
        # amount = Numeric.zero(
        # g_hash_table_unref(parser_vars)
        # if (!numval || !numeric_eq(amount, *numval)) {
        # qof_instance_set(QOF_INSTANCE (split),
        # numeric, &amount,
        # None)
        # num_val_changed = True
        # }
        #
        # return num_val_changed
        # }

    @staticmethod
    def scrub_split_numerics(split, puser):
        trans = split.get_transaction()
        trans.begin_edit()
        changed = ScheduledInstanceModel.scrub_scheduled_split_numeric(split,
                                                                       "credit") + ScheduledInstanceModel.scrub_scheduled_split_numeric(
            split, "debit")
        if not changed:
            trans.rollback_edit()
        else:
            trans.commit_edit()

    @staticmethod
    def _gen_instances(scheduled, range_end):
        instances = ScheduledInstances()
        temporal_state = scheduled.create_temporal_state()
        instances.scheduled = scheduled
        creation_end = range_end
        creation_end += relativedelta(days=scheduled.get_advance_creation())
        remind_end = creation_end
        remind_end += relativedelta(days=scheduled.get_advance_reminder())
        postponed = scheduled.get_defer_instances()
        for scheduled in postponed:
            inst_date = scheduled.get_next_instance()
            seq_num = scheduled.get_instance_count()
            inst = ScheduledInstance(instances, ScheduledInstanceState.POSTPONED, inst_date, scheduled, seq_num)
            instances.instance_list.append(inst)
            temporal_state = scheduled.clone_temporal_state()
            scheduled.incr_temporal_state(temporal_state)

        cur_date = scheduled.get_next_instance(temporal_state)
        instances.next_instance_date = cur_date
        while cur_date is not None and cur_date <= creation_end:
            seq_num = scheduled.get_instance_count(temporal_state)
            inst = ScheduledInstance(instances, ScheduledInstanceState.TO_CREATE, cur_date, temporal_state, seq_num)
            instances.instance_list.append(inst)
            scheduled.incr_temporal_state(temporal_state)
            cur_date = scheduled.get_next_instance(temporal_state)

        while cur_date is not None and cur_date <= remind_end:
            seq_num = scheduled.get_instance_count(temporal_state)
            inst = ScheduledInstance(instances, ScheduledInstanceState.REMINDER, cur_date, temporal_state, seq_num)
            instances.instance_list.append(inst)
            scheduled.incr_temporal_state(temporal_state)
            cur_date = scheduled.get_next_instance(temporal_state)
        return instances

    @classmethod
    def get_current_instances(cls):
        return cls.get_instances(datetime.date.today(), False)

    @classmethod
    def get_instances(cls, range_end: datetime.date, include_disabled: bool):
        all_schedules = ScheduledTransactionList.get_all(Session.get_current_book())

        instances = cls.new()
        instances.include_disabled = include_disabled
        instances.range_end = range_end
        if include_disabled:
            instances.instance_list = list(
                map(lambda scheduled: cls._gen_instances(scheduled, range_end), all_schedules))
        else:
            instances.instance_list = list(map(lambda scheduled: cls._gen_instances(scheduled, range_end),
                                               filter(lambda scheduled: scheduled.get_enabled(), all_schedules)))
        return instances

    @staticmethod
    def get_template_transaction_account(scheduled):
        template_root = Session.get_current_book().get_template_root()
        scheduled_guid = scheduled.get_guid()
        scheduled_template_acct = template_root.lookup_by_name(str(scheduled_guid))
        return scheduled_template_acct

    @classmethod
    def new(cls):
        return GObject.new(cls)

    def find_custom(self, scheduled):
        try:
            return next(filter(lambda ins: ins.scheduled == scheduled, self.instance_list))
        except StopIteration:
            return None

    def event_handler(self, ent, event_type, *event_data):
        if not isinstance(ent, (ScheduledTransactionList, ScheduledTransaction)):
            return
        if isinstance(ent, ScheduledTransaction):
            scheduled_is_in_model = self.find_custom(ent) is not None
            if event_type & EVENT_MODIFY:
                if scheduled_is_in_model:
                    if self.include_disabled or ent.get_enabled():
                        self.emit("updated", ent)
                    else:
                        self.emit("removing", ent)
                else:
                    all_schedules = ScheduledTransactionList.get_all(Session.get_current_book())
                    if all_schedules.__contains__(ent) and (not self.include_disabled and ent.get_enabled()):
                        self.instance_list.append(self._gen_instances(ent, self.range_end))
                    self.emit("added", ent)

        elif isinstance(ent, ScheduledTransactionList):
            scheduled = event_data[0]
            if event_type & EVENT_ITEM_REMOVED:
                link = self.find_custom(scheduled)
                if link is not None:
                    self.emit("removing", scheduled)

            elif event_type & EVENT_ITEM_ADDED:
                if self.include_disabled or scheduled.get_enabled():
                    self.instance_list.append(self._gen_instances(scheduled, self.range_end))
                    self.emit("added", scheduled)

    def update_scheduled_instances(self, scheduled):
        existing = self.find_custom(scheduled)
        if existing is None:
            return
        new_instances = self._gen_instances(scheduled, self.range_end)
        existing.scheduled = new_instances.scheduled
        existing.next_instance_date = new_instances.next_instance_date
        existing_inst = None
        new_inst = None
        for existing_inst, new_inst in zip(existing.instance_list, new_instances.instance_list):
            same_instance_date = existing_inst.date == new_inst.date
            if not same_instance_date:
                break

        existing_remain = existing_inst is not None
        new_remain = new_inst is not None
        if existing_remain:
            i = existing.instance_list.index(existing_inst)
            existing.instance_list = existing.instance_list[:i]
        if new_remain:
            i = new_instances.instance_list.index(new_inst)
            new_copy = new_instances.instance_list[i:]
            for inst in new_copy:
                inst.parent = existing
                existing.instance_list.append(inst)

    def remove_scheduled_instances(self, scheduled):
        instance_link = self.find_custom(scheduled)
        if instance_link is not None:
            self.instance_list.remove(instance_link)

    @staticmethod
    def increment_scheduled_state(inst, last_occur_date, remain_occur_count):
        if last_occur_date is None or not last_occur_date is not None or (
                last_occur_date is not None and last_occur_date <= inst.date):
            last_occur_date = inst.date
        instance_count = inst.parent.scheduled.get_instance_count(inst.temporal_state) + 1
        if remain_occur_count > 0:
            remain_occur_count -= 1
        return last_occur_date, instance_count, remain_occur_count

    @staticmethod
    def _get_template_split_account(scheduled: ScheduledTransaction, template_split, creation_errors):
        acct_guid = template_split.scheduled_account_guid
        split_acct = template_split.scheduled_account
        if split_acct is None:
            err = "Unknown account for guid [%s], cancelling SX [%s] creation." % (acct_guid, scheduled.get_name())
            creation_errors.append(err)
            return None
        return split_acct

    @staticmethod
    def _get_scheduled_formula_value(scheduled, template_split: Split, creation_errors, key, variable_bindings):
        if key == "debit":
            formula_str = template_split.scheduled_debit_formula
            numeric_val = template_split.scheduled_debit
        else:
            formula_str = template_split.scheduled_credit_formula
            numeric_val = template_split.scheduled_credit
        if variable_bindings is None or not any(variable_bindings) and numeric_val is not None and \
                numeric_val is not None and not numeric_val.is_zero():
            return numeric_val
        if formula_str is not None and len(formula_str) != 0:
            try:
                return parse_exp_decimal(formula_str)
            except:
                creation_errors.append("Error parsing SX %s formula %s" % (scheduled.get_name(), formula_str))
        else:
            return Decimal(0)

    def _get_credit_formula_value(self, instance, template_split, creation_errors):
        return self._get_scheduled_formula_value(instance.parent.scheduled, template_split, creation_errors, "credit",
                                                 instance.variable_bindings)

    def _get_debit_formula_value(self, instance, template_split, creation_errors):
        return self._get_scheduled_formula_value(instance.parent.scheduled, template_split, creation_errors, "debit",
                                                 instance.variable_bindings)

    def split_apply_formulas(self, split, creation_data):
        credit_num = self._get_credit_formula_value(creation_data.instance, split, creation_data.creation_errors)
        debit_num = self._get_debit_formula_value(creation_data.instance, split, creation_data.creation_errors)
        final = debit_num - credit_num
        return final

    def split_apply_exchange_rate(self, split, bindings, first_cmdty, split_cmdty, final):
        exchange_rate = Decimal(1, 1)
        exchange_rate_var_name = "%s . %s" % (first_cmdty.get_mnemonic(), split_cmdty.get_mnemonic())
        exchange_rate_var = bindings.get(exchange_rate_var_name)
        if exchange_rate_var is not None:
            exchange_rate = exchange_rate_var.value

        if not split_cmdty.is_currency():
            amt = Numeric.div(final, exchange_rate, split_cmdty.get_fraction(),
                              NumericRound.HALF_UP)
        else:
            amt = Numeric.mul(final, exchange_rate, 1000, NumericRound.HALF_UP)
        split.set_amount(amt)

    def get_transaction_currency(self, creation_data, scheduled, template_transaction):
        err_flag = False
        first_cmdty = None
        first_currency = None
        transaction_cmdty_in_splits = False
        transaction_cmdty = template_transaction.get_currency()
        transaction_splits = template_transaction.get_splits()
        for t_split in transaction_splits:
            split_account = self._get_template_split_account(scheduled, t_split, creation_data.creation_errors)
            if split_account is None:
                err_flag = True
                break

            split_cmdty = split_account.get_commodity()
            if transaction_cmdty is None:
                transaction_cmdty = split_cmdty
            if first_cmdty is None:
                first_cmdty = split_cmdty
            if split_cmdty == transaction_cmdty:
                transaction_cmdty_in_splits = True
            if first_currency is None and split_cmdty.is_currency():
                first_currency = split_cmdty
        if err_flag:
            return None
        if first_currency and not transaction_cmdty_in_splits or not transaction_cmdty.is_currency():
            return first_currency
        if not transaction_cmdty_in_splits:
            return first_cmdty
        return transaction_cmdty

    def create_each_transaction_helper(self, template_transaction, creation_data):
        scheduled = creation_data.instance.parent.scheduled
        transaction_cmdty = self.get_transaction_currency(creation_data, scheduled, template_transaction)
        if transaction_cmdty is None:
            return False
        new_transaction = template_transaction.clone_no_kvp()
        new_transaction.begin_edit()
        if template_transaction.get_notes() is not None:
            new_transaction.set_notes(template_transaction.get_notes())
        d = creation_data.instance.date
        new_transaction.set_post_date(d)
        template_splits = template_transaction.get_splits()
        transaction_splits = new_transaction.get_splits()
        if not any(template_splits) or not any(transaction_splits):
            new_transaction.destroy()
            new_transaction.commit_edit()
            return False

        if transaction_cmdty is None:
            new_transaction.destroy()
            new_transaction.commit_edit()
            return False
        new_transaction.set_currency(transaction_cmdty)

        for template_split, copying_split in zip(template_splits, transaction_splits):
            split_acct = self._get_template_split_account(scheduled, template_split, creation_data.creation_errors)
            split_cmdty = split_acct.get_commodity()
            copying_split.set_account(split_acct)
            final = self.split_apply_formulas(template_split, creation_data)
            copying_split.set_value(final)

            if not split_cmdty == transaction_cmdty:
                self.split_apply_exchange_rate(copying_split, creation_data.instance.variable_bindings,
                                               transaction_cmdty, split_cmdty, final)
            # Scrub.split(copying_split)
            Scrub.split(copying_split)

        new_transaction.scheduled_transaction = creation_data.instance.parent.scheduled
        new_transaction.commit_edit()

        if creation_data.created_transaction_guids is not None:
            creation_data.created_transaction_guids.append(new_transaction.get_guid())
        return False

    def create_transactions_for_instance(self, instance: ScheduledInstance, created_transaction_guids, creation_errors):
        creation_data = ScheduledTxnCreationData()
        scheduled_template_account = self.get_template_transaction_account(instance.parent.scheduled)
        creation_data.instance = instance
        creation_data.created_transaction_guids = created_transaction_guids
        creation_data.creation_errors = creation_errors
        Event.suspend()
        scheduled_template_account.foreach_transaction(self.create_each_transaction_helper, creation_data)
        Event.resume()

    def effect_change(self, auto_create_only, created_transaction_guids, creation_errors):
        instance_errors = []
        book = Session.get_current_book()
        if book.is_readonly():
            return
        be = book.get_backend()
        if be is not None:
            with be.add_lock():
                for instances in self.instance_list:
                    if len(instances.instance_list) == 0:
                        continue
                    last_occur_date = instances.scheduled.get_last_occur_date()
                    instance_count = instances.scheduled.get_instance_count(None)
                    remain_occur_count = instances.scheduled.get_rem_occur()
                    for inst in instances.instance_list:
                        scheduled_is_auto_create, c = inst.parent.scheduled.get_auto_create()
                        if auto_create_only and not scheduled_is_auto_create:
                            if inst.state != ScheduledInstanceState.TO_CREATE:
                                break
                            continue

                        if inst.orig_state == ScheduledInstanceState.POSTPONED and inst.state != ScheduledInstanceState.POSTPONED:
                            inst.parent.scheduled.remove_defer_instance(inst.temporal_state)
                        if inst.state == ScheduledInstanceState.IGNORED:
                            last_occur_date, instance_count, remain_occur_count = self.increment_scheduled_state(inst,
                                                                                                                 last_occur_date,
                                                                                                                 remain_occur_count)

                        elif inst.state == ScheduledInstanceState.POSTPONED:
                            if inst.orig_state != ScheduledInstanceState.POSTPONED:
                                instances.scheduled.add_defer_instance(inst.temporal_state.clone())
                            last_occur_date, instance_count, remain_occur_count = self.increment_scheduled_state(inst,
                                                                                                                 last_occur_date,
                                                                                                                 remain_occur_count)

                        elif inst.state == ScheduledInstanceState.TO_CREATE:
                            self.create_transactions_for_instance(inst, created_transaction_guids, instance_errors)
                            if not any(instance_errors):
                                last_occur_date, instance_count, remain_occur_count = self.increment_scheduled_state(
                                    inst,
                                    last_occur_date,
                                    remain_occur_count)
                                self.change_instance_state(inst, ScheduledInstanceState.CREATED)
                            else:
                                creation_errors.extend(instance_errors)

                    if last_occur_date is not None:
                        instances.scheduled.set_last_occur_date(last_occur_date)
                    instances.scheduled.set_instance_count(instance_count)
                    instances.scheduled.set_rem_occur(remain_occur_count)

    def change_instance_state(self, instance: ScheduledInstance, new_state):
        if instance.state == new_state:
            return
        instance.state = new_state
        inst_index = instance.parent.instance_list.index(instance)
        if instance.state != ScheduledInstanceState.REMINDER:
            for prev_inst in instance.parent.instance_list[:inst_index]:
                if prev_inst.state != ScheduledInstanceState.REMINDER:
                    continue
                prev_inst.state = ScheduledInstanceState.POSTPONED
        else:
            for next_inst in instance.parent.instance_list[inst_index:]:
                if next_inst.state == ScheduledInstanceState.REMINDER:
                    continue
                next_inst.state = ScheduledInstanceState.REMINDER
        self.emit("updated", instance.parent.scheduled)

    def set_variable(self, instance, variable, new_value):
        if Numeric.equal(variable.value, new_value):
            return
        variable.value = new_value
        self.emit("updated", instance.parent.scheduled)

    def check_variables(self):
        rtn = []
        for instances in self.instance_list:
            for inst in instances.instance_list:
                if inst.state != ScheduledInstanceState.TO_CREATE:
                    continue
                var_list = list(inst.variable_bindings.values())
                for var in var_list:
                    if var.value is None or var.value.is_zero():
                        need = ScheduledVariableNeeded()
                        need.instance = inst
                        need.variable = var
                        rtn.append(need)
        return rtn

    def summarize(self, summary):
        if summary is None:
            return
        for instances in self.instance_list:
            scheduled_is_auto_create, scheduled_notify = instances.scheduled.get_auto_create()
            for inst in instances.instance_list:
                summary.num_instances += 1
                if inst.state == ScheduledInstanceState.TO_CREATE:
                    if scheduled_is_auto_create:
                        if not scheduled_notify:
                            summary.num_auto_create_no_notify_instances += 1
                        else:
                            summary.num_auto_create_instances += 1
                    else:
                        summary.num_to_create_instances += 1
        summary.need_dialog = (
                summary.num_instances != 0 and summary.num_auto_create_no_notify_instances != summary.num_instances)

    @staticmethod
    def add_to_hash_amount(h, guid, amount):
        elem = h.get(guid)
        if elem is None:
            elem = Decimal(0)
            h[guid] = elem
        if amount.check() != NumericErrorCode.OK:
            return
        if elem.check() != NumericErrorCode.OK:
            return
        elem = Numeric.add(elem, amount, DENOM_AUTO, NumericDenom.REDUCE | NumericRound.NEVER)
        if elem.check() != NumericErrorCode.OK:
            return

    @staticmethod
    def create_cashflow_helper(template_transaction, creation_data):
        template_splits = template_transaction.get_splits()
        if not any(template_splits):
            return False
        first_cmdty = None
        for template_split in template_splits:
            split_acct = ScheduledInstanceModel._get_template_split_account(creation_data.scheduled, template_split,
                                                                            creation_data.creation_errors)
            if split_acct is None:
                break
            split_cmdty = split_acct.get_commodity()
            if first_cmdty is None:
                first_cmdty = split_cmdty
            credit_num = ScheduledInstanceModel._get_scheduled_formula_value(creation_data.scheduled, template_split,
                                                                             "credit",
                                                                             creation_data.creation_errors)
            debit_num = ScheduledInstanceModel._get_scheduled_formula_value(creation_data.scheduled, template_split,
                                                                            "debit",
                                                                            creation_data.creation_errors)
            final_once = Numeric.sub_fixed(debit_num, credit_num)
            final = Numeric.mul(final_once, creation_data.count, final_once.get_denom(), NumericRound.HALF_UP)

            gncn_error = final.check()
            if gncn_error != NumericErrorCode.OK:
                creation_data.creation_errors.append(
                    "Error %d in SX [%s] final numeric value, using 0 instead." % creation_data.scheduled.get_name())
                final = Decimal(0)

            if split_cmdty != first_cmdty:
                creation_data.creation_errors.append(
                    "No exchange rate available in SX [%s] for %s . %s, value is zero." % (
                        creation_data.scheduled.get_name(), split_cmdty.get_mnemonic(),
                        first_cmdty.get_mnemonic()))
                final = Decimal(0)
            ScheduledInstanceModel.add_to_hash_amount(creation_data.hash, split_acct.get_guid(), final)
        return False

    @staticmethod
    def instantiate_cashflow_internal(scheduled, mp, creation_errors, count):
        create_cashflow_data = ScheduledAllCashflow()
        scheduled_template_account = ScheduledInstanceModel.get_template_transaction_account(scheduled)
        if scheduled_template_account is None:
            return
        if not scheduled.get_enabled():
            return
        create_cashflow_data.hash = mp
        create_cashflow_data.creation_errors = creation_errors
        create_cashflow_data.scheduled = scheduled
        create_cashflow_data.count = Decimal(count, 1)
        scheduled_template_account.foreach_transaction(ScheduledInstanceModel.create_cashflow_helper,
                                                       create_cashflow_data)

    @staticmethod
    def instantiate_cashflow_cb(scheduled: ScheduledTransaction, userdata):
        count = scheduled.get_num_occur_daterange(scheduled, userdata.range_start, userdata.range_end)
        if count > 0:
            ScheduledInstanceModel.instantiate_cashflow_internal(scheduled, userdata.hash, userdata.creation_errors,
                                                                 count)

    @staticmethod
    def all_instantiate_cashflow(all_schedules, range_start, range_end, mp, creation_errors):
        userdata = ScheduledAllCashflow()
        userdata.hash = mp
        userdata.creation_errors = creation_errors
        userdata.range_start = range_start
        userdata.range_end = range_end
        for scheduled in all_schedules:
            ScheduledInstanceModel.instantiate_cashflow_cb(scheduled, userdata)

    @staticmethod
    def all_instantiate_cashflow_all(range_start, range_end):
        result_map = {}
        all_schedules = ScheduledTransactionList.get_all(Session.get_current_book())
        ScheduledInstanceModel.scheduled_all_instantiate_cashflow(all_schedules, range_start, range_end, result_map, [])
        return result_map
