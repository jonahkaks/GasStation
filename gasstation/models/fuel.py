from gasstation.utilities.preferences import *
from libgasstation.core.fuel import *
from libgasstation.core.session import Session
from .model import *

TANROW = "#F6FFDA"
GREENROW = "#BFDEB9"


class FuelModelColumn(GObject.GEnum):
    GUID = 0
    DATE = 1
    NOZZLE = 2
    OPENING = 3
    CLOSING = 4
    LITRES = 5
    PRICE = 6
    DEBTORS = 7
    AMOUNT = 8
    NUM_COLUMNS = 8


class FuelModel(Model, Gtk.TreeModel):
    def __init__(self):
        super().__init__()
        self.flist = []
        self.bfuel = None
        self.nozzle_model = None
        self.book = None
        self.used = {}
        self.location = None
        self.current_date = 0
        self.use_gs_color_theme = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_USE_GASSTATION_COLOR_THEME)
        gs_pref_register_cb(PREFS_GROUP_GENERAL_REGISTER, PREF_USE_GASSTATION_COLOR_THEME, self.prefs_changed)
        self.event_handler_id = Event.register_handler(self.event_handler)

    def prefs_changed(self, _, pref):
        if pref is None:
            return
        if pref.endswith(PREF_USE_GASSTATION_COLOR_THEME):
            self.use_gs_color_theme = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                       PREF_USE_GASSTATION_COLOR_THEME)

    def do_get_n_columns(self):
        return FuelModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index >= FuelModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID

        if index in [FuelModelColumn.GUID,
                     FuelModelColumn.NOZZLE,
                     FuelModelColumn.OPENING,
                     FuelModelColumn.CLOSING,
                     FuelModelColumn.LITRES,
                     FuelModelColumn.PRICE,
                     FuelModelColumn.DEBTORS,
                     FuelModelColumn.AMOUNT]:
            return str
        elif index == FuelModelColumn.DATE:
            return int
        return GObject.TYPE_INVALID

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_iter_has_child(self, _iter):
        return False

    def do_iter_has_parent(self, _iter):
        _iter.stamp = 0
        return False, _iter

    @classmethod
    def new(cls):
        model = GObject.new(cls)
        model.book = Session.get_current_book()
        model.nozzle_model = Gtk.ListStore(str, object)
        return model

    def refresh(self, sdate=None, location=None, *_):
        self.clear()
        self.flist.clear()
        if sdate is None:
            sdate = datetime.date.today()
        self.current_date = sdate
        self.location = location
        query = QueryPrivate.create_for(ID_FUEL)
        query.set_book(Session.get_current_book())
        query.add_term([FUEL_DATE], QueryCore.date_predicate(QueryCompare.EQUAL, DateMatch.NORMAL, sdate), QueryOp.AND)
        query.add_term([FUEL_LOCATION, PARAM_GUID], QueryCore.guid_predicate(GuidMatch.ANY, [location.guid]), QueryOp.AND)
        query.set_sort_order([FUEL_DATE], None, None)
        for f in query.run():
            self.insert_fuel(f)
        self.make_blank(location)
        query.destroy()

    def event_handler(self, entity, event_type, *args):
        if not isinstance(entity, Fuel):
            return
        if event_type not in [EVENT_CREATE, EVENT_MODIFY, EVENT_DESTROY]:
            return
        fl = entity
        if fl.get_book() != self.book:
            return
        if event_type == EVENT_DESTROY:
            path = self.get_path_from_fuel(fl)
            if path is None:
                return
            self.row_deleted(path)
            return

        elif event_type == EVENT_MODIFY:
            if fl == self.bfuel:
                self.make_blank(self.location)
            path = self.get_path_from_fuel(fl)
            if path is None:
                return
            _iter = self.do_get_iter(path)[1]
            if _iter is None:
                return
            self.row_changed(path, _iter)

    def get_row_color(self, num):
        cell_color = None
        if self.use_gs_color_theme:
            if num % 2 == 0:
                cell_color = GREENROW
            else:
                cell_color = TANROW
        return cell_color

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if self.flist is not None and i < len(self.flist):
            _iter.stamp = self.stamp
            _iter.user_data = i
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_get_path(self, _iter):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp or _iter.user_data >= len(self.flist) - 1:
            _iter.stamp = 0
            return False
        _iter.user_data += 1
        return True, _iter

    def do_iter_previous(self, _iter):
        if _iter is None or _iter.stamp != self.stamp or _iter.user_data2 == 0:
            if _iter is not None:
                _iter.stamp = 0
            return False, _iter
        _iter.user_data -= 1
        return True, _iter

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        if parent_iter is not None or n >= len(self.flist):
            _iter.stamp = 0
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = n
        return True, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return len(self.flist)
        if _iter.stamp != self.stamp:
            return -1
        return 0

    def do_get_value(self, _iter, column):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        fuel = self.get_fuel_at_iter(_iter)
        if fuel is None:
            return ""
        if column == FuelModelColumn.NOZZLE:
            return fuel.get_nozzle_name()
        elif column == FuelModelColumn.OPENING:
            return "{:,.2f}".format(fuel.get_opening())

        elif column == FuelModelColumn.CLOSING:
            return "{:,.2f}".format(fuel.get_closing())

        elif column == FuelModelColumn.LITRES:
            return "{:,.2f}".format(fuel.get_litres())

        elif column == FuelModelColumn.PRICE:
            return "{:,.2f}".format(fuel.get_price())

        elif column == FuelModelColumn.DEBTORS:
            return "{:,.2f}".format(fuel.get_debtor_amount())

        elif column == FuelModelColumn.AMOUNT:
            return "{:,.2f}".format(fuel.get_amount())
        return ""

    @staticmethod
    def get_last_fuel_by_nozzle(nozzle, cdate):
        query = QueryPrivate.create_for(ID_FUEL)
        query.set_book(Session.get_current_book())
        pred_data = QueryCore.guid_predicate(GuidMatch.ANY, [nozzle.get_guid()])
        if pred_data is None:
            return
        param_list = [FUEL_NOZZLE, PARAM_GUID]
        query.add_term(param_list, pred_data, QueryOp.AND)
        pred_data = QueryCore.date_predicate(QueryCompare.LT, DateMatch.NORMAL, cdate)
        query.add_term([FUEL_DATE], pred_data, QueryOp.AND)
        query.set_sort_order([FUEL_DATE], None, None)
        results = query.run()
        if len(results) > 0:
            return results[-1]
        return None

    def update_models(self, location):
        self.nozzle_model.clear()
        names = list(map(lambda f: f.get_nozzle_name(), self.flist))
        for n in self.book.get_collection(ID_NOZZLE).get_all():
            name = n.get_name()
            if name not in names and n.get_location() == location:
                self.nozzle_model.append([name, n])

    def get_nozzle_model(self):
        return self.nozzle_model

    def get_fuel_totals(self):
        litres = Decimal(0)
        amount = Decimal(0)
        for f in filter(lambda x: x.nozzle is not None, self.flist):
            litres += f.get_litres()
            amount += f.get_amount()
        return litres, amount

    def get_nozzle_from_text(self, new_text):
        _iter = self.nozzle_model.get_iter_first()
        while _iter is not None:
            text = self.nozzle_model.get_value(_iter, 0)
            if text == new_text:
                break
            _iter = self.nozzle_model.iter_next(_iter)
        if _iter is not None:
            return self.nozzle_model.get_value(_iter, 1)

    def get_fuel_at_iter(self, _iter) -> Fuel:
        if _iter.stamp != self.stamp or _iter.user_data < 0 or _iter.user_data >= len(self.flist):
            return None
        return self.flist[_iter.user_data]

    def get_fuel(self, _iter):
        return self.get_fuel_at_iter(_iter)

    def get_path_from_fuel(self, fuel):
        if fuel in self.flist:
            p = Gtk.TreePath.new()
            p.append_index(self.flist.index(fuel))
            return p
        return None

    def get_fuel_at_path(self, path):
        i = path.get_indices()[0]
        try:
            return self.flist[i]
        except IndexError:
            return None

    def make_blank(self, location):
        self.bfuel = Fuel(self.book)
        self.bfuel.begin_edit()
        self.bfuel.set_date(self.current_date)
        self.bfuel.set_location(location)
        self.insert_fuel(self.bfuel)

    def insert_fuel(self, fuel):
        self.flist.append(fuel)
        i = self.flist.index(fuel)
        _iter = Gtk.TreeIter()
        _iter.stamp = self.stamp
        _iter.user_data = i
        path = self.do_get_path(_iter)
        self.row_inserted(path, _iter)

    def get_blank_fuel(self):
        return self.bfuel

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            _iter.stamp = 0
            return False, _iter
        if len(self.flist) > 0:
            _iter.stamp = self.stamp
            _iter.user_data = 0
            return True, _iter
        else:
            return False, _iter

    def destroy(self):
        if self.event_handler_id:
            Event.unregister_handler(self.event_handler_id)
        self.event_handler_id = 0
        self.book = None
        if self.bfuel is not None:
            self.bfuel.dispose()
            self.bfuel = None
        self.flist.clear()


GObject.type_register(FuelModel)
