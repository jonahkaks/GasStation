from gasstation.utilities.avatar_utils import *
from libgasstation.core._declbase import ID_PRODUCT
from libgasstation.core.event import *
from libgasstation.core.product import Product
from libgasstation.core.session import Session
from .model import *


class ProductModelColumn(GObject.GEnum):
    ID = 0
    AVATAR = 1
    NAME = 2
    SKU = 3
    TYPE = 4
    DESCRIPTION = 5
    CATEGORY = 6
    NUM_COLUMNS = 7


class ProductModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = "ProductModel"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.product_list = None
        self.book = None
        self.event_id = 0
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)

    def foreach_function(self, model, path, _iter, rr_list):
        row_ref = Gtk.TreeRowReference.new(model, path)
        rr_list.append(row_ref)
        return False

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_iter_has_child(self, _iter):
        return False

    def do_iter_has_parent(self, _iter):
        _iter.stamp = 0
        return False, _iter

    def clear(self):
        rr_list = []
        self.foreach(self.foreach_function, rr_list)
        rr_list.reverse()
        for ref in rr_list:
            path = ref.get_path()
            if path is not None:
                self.row_deleted(path)
        self.increment_stamp()

    def do_get_n_columns(self):
        return ProductModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index >= ProductModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID
        if index in [ProductModelColumn.ID,
                     ProductModelColumn.AVATAR,
                     ProductModelColumn.NAME,
                     ProductModelColumn.SKU,
                     ProductModelColumn.TYPE,
                     ProductModelColumn.CATEGORY,
                     ProductModelColumn.DESCRIPTION,
                     ]:
            return str
        return GObject.TYPE_INVALID

    @classmethod
    def new(cls):
        model = GObject.new(cls)
        model.book = Session.get_current_book()
        model.product_list = model.book.get_collection(ID_PRODUCT).get_all()
        model.event_id = Event.register_handler(model.event_handler)
        return model

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if self.product_list is None or i >= len(self.product_list):
            _iter.stamp = 0
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = i
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return False, _iter
        if _iter.user_data < len(self.product_list) - 1:
            _iter.user_data += 1
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            _iter.stamp = 0
            return False, _iter
        if len(self.product_list) > 0:
            _iter.stamp = self.stamp
            _iter.user_data = 0
            return True, _iter
        else:
            return False, _iter

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        _iter.stamp = 0
        if parent_iter is not None:
            return False, _iter
        if len(self.product_list) > n:
            _iter.stamp = self.stamp
            _iter.user_data = n
            return True, _iter
        else:
            return False, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return len(self.product_list)
        if self.stamp != _iter.stamp:
            return -1
        return 0

    def do_get_value(self, _iter, column):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        product = self.get_product_at_iter(_iter)
        if product is None:
            return ""
        if column == ProductModelColumn.AVATAR:
            source_pixbuf = None
            name = product.get_name().upper()
            image_url = product.get_image_url()
            if image_url is not None:
                try:
                    source_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(image_url,
                                                                           32, 32
                                                                           )
                except GLib.Error:
                    pass
            if source_pixbuf is None and name is not None:
                source_pixbuf = generate_default_avatar(name, 32)

            if source_pixbuf is None:
                icon_theme = Gtk.IconTheme.get_default()
                source_pixbuf = icon_theme.load_icon("avatar-default-symbolic", 32, Gtk.IconLookupFlags.FORCE_SIZE)

            return round_image(source_pixbuf)

        elif column == ProductModelColumn.ID:
            return product.get_id() or ""

        elif column == ProductModelColumn.NAME:
            return "\n".join([product.get_name() or "", product.get_description() or ""]).strip().upper()
        elif column == ProductModelColumn.SKU:
            return product.get_sku() or ""

        elif column == ProductModelColumn.TYPE:
            return product.get_type().value

        elif column == ProductModelColumn.CATEGORY:
            cat = product.get_category()
            return cat.get_name() if cat is not None else ""

    def get_product(self, _iter):
        inv = self.get_product_at_iter(_iter)
        return inv

    def get_path_from_product(self, product):
        _iter = self.get_iter_from_product(product)
        if _iter is not None:
            return self.do_get_path(_iter)

    def get_product_at_iter(self, _iter):
        path = self.get_path(_iter)
        if path is not None and _iter.stamp == self.stamp:
            return self.get_product_at_path(path)

    def get_product_at_path(self, path):
        i = path.get_indices()[0]
        try:
            return self.product_list[i]
        except IndexError:
            return None

    def get_iter_from_product(self, product):
        if product is None:
            return None
        if self.product_list is None or len(self.product_list) == 0:
            return None
        _iter = Gtk.TreeIter()
        try:
            i = self.product_list.index(product)
            _iter = Gtk.TreeIter()
            _iter.stamp = self.stamp
            _iter.user_data = i
            return _iter
        except ValueError:
            return None

    def event_handler(self, entity, event_type, ed=None):
        if not isinstance(entity, Product):
            return
        product = entity
        if product.get_book() != self.book:
            return
        if event_type == EVENT_CREATE:
            self.product_list = self.book.get_collection(ID_PRODUCT).get_all()
            _iter = self.get_iter_from_product(product)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_inserted(path, _iter)

        elif event_type == EVENT_DESTROY:
            path = self.get_path_from_product(product)
            self.row_deleted(path)
            self.product_list.remove(product)

        elif event_type == EVENT_MODIFY:
            _iter = self.get_iter_from_product(product)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_changed(path, _iter)


GObject.type_register(ProductModel)
