from gasstation.utilities.preferences import *
from libgasstation import Fuel
from libgasstation.core.dip import *
from libgasstation.core.session import Session
from .model import *

TANROW = "#F6FFDA"
GREENROW = "#BFDEB9"


class DipsModelColumn(GObject.GEnum):
    GUID = 0
    DATE = 1
    TANK = 2
    OPENING = 3
    CLOSING = 4
    TOTAL = 5
    METER = 6
    STOCK_LOSS = 7
    NUM_COLUMNS = 8


class DipsModel(Model, Gtk.TreeModel):
    __gtype_name__ = "DipsModel"
    used = {}

    def __init__(self):
        super(DipsModel, self).__init__()
        self.tank_fuel = {}
        self.dips_list = None
        self.bdips = None
        self.tank_model = None
        self.tanks = {}
        self.book = None
        self.current_date = datetime.datetime.now()
        self.use_gs_color_theme = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_USE_GASSTATION_COLOR_THEME)
        gs_pref_register_cb(PREFS_GROUP_GENERAL_REGISTER, PREF_USE_GASSTATION_COLOR_THEME, self.prefs_changed)
        self.event_handler_id = Event.register_handler(self.event_handler)

    def prefs_changed(self, _, pref):
        if pref is None:
            return
        if pref.endswith(PREF_USE_GASSTATION_COLOR_THEME):
            self.use_gs_color_theme = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                       PREF_USE_GASSTATION_COLOR_THEME)

    def do_get_n_columns(self):
        return DipsModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index >= DipsModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID

        if index in [DipsModelColumn.GUID,
                     DipsModelColumn.TANK,
                     DipsModelColumn.OPENING,
                     DipsModelColumn.CLOSING,
                     DipsModelColumn.METER,
                     DipsModelColumn.TOTAL,
                     DipsModelColumn.STOCK_LOSS]:
            return str
        elif index == DipsModelColumn.DATE:
            return int
        return GObject.TYPE_INVALID

    def get_row_color(self, num):
        cell_color = None
        if self.use_gs_color_theme:
            if num % 2 == 0:
                cell_color = GREENROW
            else:
                cell_color = TANROW
        return cell_color

    def get_dips_totals(self):
        a = Decimal(0)
        b = Decimal(0)
        for dp in self.dips_list:
            a += dp.get_tank_sales()
            b += dp.get_stock_loss()
        return a, b

    @classmethod
    def new(cls):
        model = GObject.new(cls)
        model.book = Session.get_current_book()
        model.tank_model = Gtk.ListStore(str, object)
        model.dips_list = []
        return model

    def refresh(self, sdate=None, location=None):
        self.clear()
        self.dips_list.clear()
        if sdate is None:
            sdate = datetime.date.today()
        self.current_date = sdate
        query = QueryPrivate.create_for(ID_DIPS)
        book = Session.get_current_book()
        query.set_book(book)
        pred_data = QueryCore.date_predicate(QueryCompare.EQUAL, DateMatch.NORMAL, sdate)
        param_list = [DIPS_DATE]
        query.add_term(param_list, pred_data, QueryOp.AND)
        query.add_term([DIPS_LOCATION, PARAM_GUID], QueryCore.guid_predicate(GuidMatch.ANY, [location.guid]), QueryOp.AND)
        query.set_sort_order([DIPS_DATE], None, None)
        for f in query.run():
            self.insert_dips(f)
        self.make_blank(location)
        query.destroy()

    def event_handler(self, entity, event_type, *ed):
        if not isinstance(entity, (Dip, Fuel)):
            return
        if entity.get_book() != self.book:
            return
        if event_type == EVENT_DESTROY:
            if isinstance(entity, Dip):
                path = self.get_path_from_dips(entity)
                if path is None:
                    return
                self.row_deleted(path)
                return
            elif isinstance(entity, Fuel):
                Dip.set_meter_sales(entity, True)

        elif event_type == EVENT_MODIFY:
            if isinstance(entity, Fuel):
                fl = Dip.from_fuel(entity)
                if fl is not None and any(fl):
                    entity = fl[0]
                else:
                    return
            if entity == self.bdips:
                self.make_blank(self.bdips.get_location())
            _iter = self.get_iter_from_dips(entity)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_changed(path, _iter)

    def update_values(self, fuel):
        Dip.set_meter_sales(fuel)
        if fuel.nozzle is None:
            return
        if fuel.nozzle.tank is None:
            return
        if not len(list(filter(lambda d: d.tank == fuel.nozzle.tank, self.dips_list))):
            d = self.bdips
            tank = fuel.nozzle.tank
            d.set_tank(tank)
            last_dips = self.get_last_dips_by_tank(tank, d.get_date())
            if last_dips is not None:
                d.set_opening(last_dips.get_closing())
            path = self.get_path_from_dips(d)
            if path is not None:
                _iter = self.get_iter(path)
                if _iter is not None:
                    self.row_changed(path, _iter)

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_iter_has_child(self, _iter):
        return False

    def do_iter_has_parent(self, _iter):
        _iter.stamp = 0
        return False, _iter

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if self.dips_list is None or i >= len(self.dips_list):
            _iter.stamp = 0
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = i
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return False, _iter
        if _iter.user_data < len(self.dips_list) - 1:
            _iter.user_data += 1
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            _iter.stamp = 0
            return False, _iter
        if len(self.dips_list) > 0:
            _iter.stamp = self.stamp
            _iter.user_data = 0
            return True, _iter
        else:
            return False, _iter

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        _iter.stamp = 0
        if parent_iter is not None:
            return False, _iter
        if len(self.dips_list) > n:
            _iter.stamp = self.stamp
            _iter.user_data = n
            return True, _iter
        else:
            return False, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return len(self.dips_list)
        if self.stamp != _iter.stamp:
            return -1
        return 0

    def do_get_value(self, _iter, column):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        dips = self.get_dips_at_iter(_iter)
        if dips is None or not isinstance(dips, Dip):
            return
        if column == DipsModelColumn.TANK:
            return dips.get_tank_name()
        elif column == DipsModelColumn.OPENING:
            return "{:,.2f}".format(dips.get_opening())
        elif column == DipsModelColumn.CLOSING:
            return "{:,.2f}".format(dips.get_closing())
        elif column == DipsModelColumn.METER:
            return "{:,.2f}".format(dips.get_meter_sales())
        elif column == DipsModelColumn.TOTAL:
            return "{:,.2f}".format(dips.get_tank_sales())
        elif column == DipsModelColumn.STOCK_LOSS:
            return "{:,.2f}".format(dips.get_stock_loss())
        return ""

    @staticmethod
    def get_last_dips_by_tank(tank, cdate):
        query = QueryPrivate.create_for(ID_DIPS)
        query.set_book(Session.get_current_book())
        pred_data = QueryCore.guid_predicate(GuidMatch.ANY, [tank.get_guid()])
        if pred_data is None:
            return
        param_list = [DIPS_TANK, PARAM_GUID]
        query.add_term(param_list, pred_data, QueryOp.AND)
        pred_data = QueryCore.date_predicate(QueryCompare.LT, DateMatch.NORMAL, cdate)
        query.add_term([DIPS_DATE], pred_data, QueryOp.AND)
        query.set_sort_order([DIPS_DATE], None, None)
        results = query.run()
        if len(results) > 0:
            return results[-1]
        return None

    def update_models(self):
        self.tank_model.clear()
        names = list(map(lambda f: f.get_tank_name(), self.dips_list))
        for n in self.book.get_collection(ID_TANK).get_all():
            name = n.get_name()
            if name not in names:
                self.tank_model.append([name, n])

    def get_tank_from_name(self, name):
        _iter = self.tank_model.get_iter_first()
        while _iter is not None:
            text = self.tank_model.get_value(_iter, 0)
            if text == name:
                break
            _iter = self.tank_model.iter_next(_iter)
        if _iter is not None:
            return self.tank_model.get_value(_iter, 1)

    def get_tank_model(self):
        return self.tank_model

    def get_dips(self, _iter):
        inv = self.get_dips_at_iter(_iter)
        return inv

    def get_path_from_dips(self, dips):
        _iter = self.get_iter_from_dips(dips)
        return self.do_get_path(_iter)

    def get_dips_at_iter(self, _iter):
        path = self.get_path(_iter)
        if path is not None and _iter.stamp == self.stamp:
            return self.get_dips_at_path(path)

    def get_dips_at_path(self, path):
        i = path.get_indices()[0]
        try:
            return self.dips_list[i]
        except IndexError:
            return None

    def get_iter_from_dips(self, dips):
        if dips is None:
            return None
        if self.dips_list is None or len(self.dips_list) == 0:
            return None
        _iter = Gtk.TreeIter()
        try:
            i = self.dips_list.index(dips)
            _iter = Gtk.TreeIter()
            _iter.stamp = self.stamp
            _iter.user_data = i
            return _iter
        except ValueError:
            return None

    def make_blank(self, loc):
        self.bdips = Dip(self.book)
        self.bdips.begin_edit()
        self.bdips.set_location(loc)
        self.bdips.set_date(self.current_date)
        self.insert_dips(self.bdips)

    def insert_dips(self, dips):
        if not isinstance(dips, Dip):
            return
        self.dips_list.append(dips)
        _iter = self.get_iter_from_dips(dips)
        path = self.do_get_path(_iter)
        self.row_inserted(path, _iter)

    def get_blank_dips(self):
        return self.bdips

    def destroy(self):
        if self.event_handler_id:
            Event.unregister_handler(self.event_handler_id)
        self.event_handler_id = 0
        self.book = None
        if self.bdips is not None:
            self.bdips.dispose()
            self.bdips = None
        self.dips_list.clear()


GObject.type_register(DipsModel)
