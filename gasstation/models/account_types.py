from gi import require_version

from libgasstation.core.account import *

require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, GObject

TYPE_MASK = "type-mask"
account_types_tree_model = None


class AccountTypesModelColumn(GObject.GEnum):
    TYPE = 0
    NAME = 1
    SELECTED = 2
    NUM_COLUMNS = 3


class AccountTypesModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = "AccountTypesModel"

    def __init__(self):
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        self.selected = 0
        self.valid_types = None
        GObject.Object.__init__(self)

    @classmethod
    def new(cls, selected):
        model = GObject.new(cls)
        model.selected = selected
        return model

    @classmethod
    def master(cls):
        global account_types_tree_model
        if account_types_tree_model is None:
            account_types_tree_model = cls.new(0)
        return account_types_tree_model

    @staticmethod
    def is_valid(model, _iter, data):
        _type = model.get_value(_iter, AccountTypesModelColumn.TYPE)
        return True if model.valid_types & (1 << _type) else False

    @staticmethod
    def filter_using_mask(types):
        model = AccountTypesModel.master()
        model.valid_types = types
        f_model = model.filter_new()
        f_model.set_visible_func(model.is_valid)
        return f_model

    @staticmethod
    def set_mask(f_model, types):

        if f_model is None:
            return
        f_model.get_model().valid_types = types
        f_model.refilter()

    @staticmethod
    def get_mask(f_model):
        return f_model.get_model().valid_types

    @staticmethod
    def get_selection(sel):
        bits = 0
        if sel is None:
            return
        view = sel.get_tree_view()
        if view is None:
            return 0
        f_model, plist = sel.get_selected_rows()
        if f_model is not None:
            f_model = view.get_model()
        for node in plist:
            path = f_model.convert_path_to_child_path(node)
            if path is None or path.get_depth() != 1:
                continue
            bits |= 1 << path.get_indices()[0]
        return bits

    @staticmethod
    def get_selection_single(sel):
        selected = AccountTypesModel.get_selection(sel)
        for i in range(AccountType.NUM_ACCOUNT_TYPES):
            if selected & (1 << i):
                return AccountType.from_int(i)
        return AccountType.NONE

    @staticmethod
    def set_selection(sel, selected):
        if not isinstance(sel, Gtk.TreeSelection):
            return
        view = sel.get_tree_view()
        if view is None:
            return
        f_model = view.get_model()
        sel.unselect_all()
        path = Gtk.TreePath.new_first()
        for i in range(AccountType.NUM_ACCOUNT_TYPES):
            if selected & (1 << i):
                f_path = f_model.convert_child_path_to_path(path)
                if f_path is not None:
                    sel.select_path(f_path)
                    view.scroll_to_cell(f_path, None, False, 0.0, 0.0)
            path.next()

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_get_n_columns(self):
        return AccountTypesModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if 0 > index >= AccountTypesModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID
        if index == AccountTypesModelColumn.NAME:
            return str
        elif index == AccountTypesModelColumn.TYPE:
            return int
        elif index == AccountTypesModelColumn.SELECTED:
            return bool
        return GObject.TYPE_INVALID

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if i < AccountType.NUM_ACCOUNT_TYPES:
            _iter.stamp = self.stamp
            _iter.user_data = i
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_get_path(self, _iter):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_get_value(self, _iter, column):

        if _iter.stamp != self.stamp or _iter is None:
            return None
        if column == AccountTypesModelColumn.TYPE:
            return _iter.user_data

        elif column == AccountTypesModelColumn.NAME:
            return AccountType(_iter.user_data).to_string()

        elif column == AccountTypesModelColumn.SELECTED:
            return self.selected & (1 << _iter.user_data)

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            _iter.stamp = 0
            return False, _iter

        if _iter.user_data < AccountType.NUM_ACCOUNT_TYPES - 1:
            _iter.user_data += 1
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return AccountType.NUM_ACCOUNT_TYPES
        if _iter.stamp != self.stamp:
            return -1
        return 0

    def do_iter_nth_child(self, parent, n):
        _iter = Gtk.TreeIter()
        if parent is not None:
            return False, _iter
        if n < AccountType.NUM_ACCOUNT_TYPES:
            _iter.stamp = self.stamp
            _iter.user_data = n
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = 0

        return True, _iter

    def do_iter_has_child(self, _iter):
        return False


GObject.type_register(AccountTypesModel)
