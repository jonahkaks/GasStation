from libgasstation.core.product import UnitOfMeasureGroup, ID_UNIT_OF_MEASURE_GROUP
from libgasstation.core.session import Session
from .model import *


class UnitOfMeasureGroupModelColumn(GObject.GEnum):
    NAME = 0
    PRIMARY_UNIT = 1
    NUM_COLUMNS = 2


from libgasstation.core.event import *


class UnitOfMeasureGroupModel(GObject.Object, Gtk.TreeModel):
    __gtype_name__ = "UnitOfMeasureGroupModel"

    def __init__(self):
        self.plist = None
        self.book = None
        self.event_id = 0
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        GObject.Object.__init__(self)

    def foreach_function(self, model, path, _iter, rr_list):
        row_ref = Gtk.TreeRowReference.new(model, path)
        rr_list.append(row_ref)
        return False

    def clear(self):
        rr_list = []
        self.foreach(self.foreach_function, rr_list)
        rr_list.reverse()
        for ref in rr_list:
            path = ref.get_path()
            if path is not None:
                self.row_deleted(path)

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_iter_has_child(self, _iter):
        return False

    def do_get_n_columns(self):
        return UnitOfMeasureGroupModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if -1 < index >= UnitOfMeasureGroupModelColumn.NUM_COLUMNS:
            return GObject.TYPE_INVALID
        return GObject.TYPE_STRING

    @classmethod
    def new(cls):
        model = GObject.new(cls)
        model.book = Session.get_current_book()
        model.plist = model.book.get_collection(ID_UNIT_OF_MEASURE_GROUP).get_all()
        model.event_id = Event.register_handler(model.event_handler)
        return model

    def do_get_iter(self, path):
        _iter = Gtk.TreeIter()
        i = path.get_indices()[0]
        if self.plist is None or i >= len(self.plist):
            _iter.stamp = 0
            return False, _iter
        _iter.stamp = self.stamp
        _iter.user_data = i
        return True, _iter

    def do_get_path(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data)
        return path

    def do_iter_next(self, _iter):
        if _iter is None or _iter.stamp != self.stamp:
            return False, _iter
        if _iter.user_data < len(self.plist) - 1:
            _iter.user_data += 1
            return True, _iter
        _iter.stamp = 0
        return False, _iter

    def do_iter_children(self, parent):
        _iter = Gtk.TreeIter()
        if parent is not None:
            _iter.stamp = 0
            return False, _iter
        if len(self.plist) > 0:
            _iter.stamp = self.stamp
            _iter.user_data = 0
            return True, _iter
        else:
            return False, _iter

    def do_iter_nth_child(self, parent_iter, n):
        _iter = Gtk.TreeIter()
        _iter.stamp = 0
        if parent_iter is not None:
            return False, _iter
        if len(self.plist) > n:
            _iter.stamp = self.stamp
            _iter.user_data = n
            return True, _iter
        else:
            return False, _iter

    def do_iter_n_children(self, _iter):
        if _iter is None:
            return len(self.plist)
        if self.stamp != _iter.stamp:
            return -1
        return 0

    def do_get_value(self, _iter, column):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        unit_of_measure_group: UnitOfMeasureGroup = self.get_unit_of_measure_group_at_iter(_iter)
        if unit_of_measure_group is None:
            return ""
        if column == UnitOfMeasureGroupModelColumn.NAME:
            return unit_of_measure_group.get_name() or ""
        elif column == UnitOfMeasureGroupModelColumn.PRIMARY_UNIT:
            pu = unit_of_measure_group.get_primary_unit()
            return pu.get_name() if pu is not None else ""

    def get_unit_of_measure_group(self, _iter):
        inv = self.get_unit_of_measure_group_at_iter(_iter)
        return inv

    def get_path_from_unit_of_measure_group(self, unit_of_measure_group):
        _iter = self.get_iter_from_unit_of_measure_group(unit_of_measure_group)
        return self.do_get_path(_iter)

    def get_unit_of_measure_group_at_iter(self, _iter):
        path = self.get_path(_iter)
        if path is not None and _iter.stamp == self.stamp:
            return self.get_unit_of_measure_group_at_path(path)

    def get_unit_of_measure_group_at_path(self, path):
        i = path.get_indices()[0]
        try:
            return self.plist[i]
        except IndexError:
            return None

    def get_iter_from_unit_of_measure_group(self, unit_of_measure_group):
        if unit_of_measure_group is None:
            return None
        if self.plist is None or len(self.plist) == 0:
            return None
        _iter = Gtk.TreeIter()
        try:
            i = self.plist.index(unit_of_measure_group)
            _iter = Gtk.TreeIter()
            _iter.stamp = self.stamp
            _iter.user_data = i
            return _iter
        except ValueError:
            return None

    def event_handler(self, entity, event_type, ed=None):
        if not isinstance(entity, UnitOfMeasureGroup):
            return
        unit_of_measure_group = entity
        if unit_of_measure_group.get_book() != self.book:
            return
        if event_type == EVENT_CREATE:
            self.plist = self.book.get_collection(ID_UNIT_OF_MEASURE_GROUP).get_all()
            _iter = self.get_iter_from_unit_of_measure_group(unit_of_measure_group)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_inserted(path, _iter)

        elif event_type == EVENT_DESTROY:
            path = self.get_path_from_unit_of_measure_group(unit_of_measure_group)
            if path is None:
                return
            try:
                self.row_deleted(path)
                self.plist.remove(unit_of_measure_group)
            except ValueError:
                pass

        elif event_type == EVENT_MODIFY:
            _iter = self.get_iter_from_unit_of_measure_group(unit_of_measure_group)
            if _iter is None:
                return
            path = self.do_get_path(_iter)
            if path is None:
                return
            self.row_changed(path, _iter)


GObject.type_register(UnitOfMeasureGroupModel)
