from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import Gdk, GObject
from gasstation.utilities.ui import *
from libgasstation.core.session import *
from gasstation.utilities.accounting_period import *
from libgasstation.core.component import Tracking


class AccountModelColumn(GObject.GEnum):
    NAME = 0
    TYPE = 1
    COMMODITY = 2
    CODE = 3
    DESCRIPTION = 4
    LAST_NUM = 5
    PRESENT = 6
    PRESENT_REPORT = 7
    BALANCE = 8
    BALANCE_REPORT = 9
    BALANCE_PERIOD = 10
    CLEARED = 11
    CLEARED_REPORT = 12
    RECONCILED = 13
    RECONCILED_REPORT = 14
    RECONCILED_DATE = 15
    FUTURE_MIN = 16
    FUTURE_MIN_REPORT = 17
    TOTAL = 18
    TOTAL_REPORT = 19
    TOTAL_PERIOD = 20
    NOTES = 21
    TAX_INFO = 22
    TAX_INFO_SUB_ACCT = 23
    PLACEHOLDER = 24
    HIDDEN = 25
    LAST_VISIBLE = PLACEHOLDER
    COLOR_PRESENT = 26
    COLOR_ACCOUNT = 27
    COLOR_BALANCE = 28
    COLOR_BALANCE_PERIOD = 29
    COLOR_CLEARED = 30
    COLOR_RECONCILED = 31
    COLOR_FUTURE_MIN = 32
    COLOR_TOTAL = 33
    COLOR_TOTAL_PERIOD = 34
    NUM_COLUMNS = 35


def get_negative_color():
    label = Gtk.Label.new("Color")
    context = label.get_style_context()
    context.add_class("negative-numbers")
    color = context.get_color(Gtk.StateFlags.NORMAL)
    return Gdk.RGBA.to_string(color)


class AccountModel(GObject.Object, Gtk.TreeModel, Gtk.TreeDragSource, Gtk.TreeDragDest):
    __gtype_name__ = "AccountModel"

    def __init__(self):
        super().__init__()
        self.accounts = None
        self.root = None
        self.book = None
        self.stamp = 0
        self.event_id = 0
        self.account_values_hash = None
        self.negative_color = None
        Tracking.remember(self)

    @classmethod
    def new(cls, root):
        items = Tracking.get_list(cls.__name__)
        for model in items:
            if model.root == root:
                return model
        m = cls()
        m.root = root
        m.book = Session.get_current_book()
        m.event_id = Event.register_handler(m.event_handler_account)
        m.accounts = weakref.WeakValueDictionary()
        m.stamp = GLib.random_int_range(-2147483648, 2147483647)
        m.account_values_hash = {}
        use_red = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED)
        if use_red:
            m.negative_color = get_negative_color()
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED, m.update_color)
        Tracking.remember(m)
        return m

    def update_color(self, *_):
        self.account_values_hash.clear()
        use_red = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED)
        if use_red:
            self.negative_color = get_negative_color()
        else:
            self.negative_color = None

    def do_drag_data_get(self, path, selection_data):
        print('MyTreeViewModel.do_drag_data_get', selection_data)
        uri = self[path][1]
        print('- setting uri        ', uri)
        selection_data.set_text(uri, -1)
        return True

    def do_row_draggable(self, path):
        print('do_row_draggable', path)
        return True

    def do_drag_data_delete(self, path):
        print('do_drag_data_delete', path)
        del self[path]
        return True

    def do_drag_data_received(self, dest_path, selection_data):
        print('MyTreeViewModel.do_drag_data_received')
        received_data = selection_data.get_text()

        print('-', received_data)

        idx = dest_path.get_indices()[0]
        self.insert(idx, row=(os.path.basename(received_data), received_data))

        return True

    def do_row_drop_possible(self, dest_path, selection_data):
        print('is possible?')
        return True


    def do_get_flags(self):
        return 0

    def do_get_n_columns(self):
        return AccountModelColumn.NUM_COLUMNS

    def do_get_column_type(self, index):
        if index in [AccountModelColumn.NAME,
                     AccountModelColumn.TYPE,
                     AccountModelColumn.COMMODITY,
                     AccountModelColumn.CODE,
                     AccountModelColumn.DESCRIPTION,
                     AccountModelColumn.PRESENT,
                     AccountModelColumn.PRESENT_REPORT,
                     AccountModelColumn.BALANCE,
                     AccountModelColumn.BALANCE_REPORT,
                     AccountModelColumn.BALANCE_PERIOD,
                     AccountModelColumn.CLEARED,
                     AccountModelColumn.CLEARED_REPORT,
                     AccountModelColumn.RECONCILED,
                     AccountModelColumn.RECONCILED_REPORT,
                     AccountModelColumn.RECONCILED_DATE,
                     AccountModelColumn.FUTURE_MIN,
                     AccountModelColumn.FUTURE_MIN_REPORT,
                     AccountModelColumn.TOTAL,
                     AccountModelColumn.TOTAL_REPORT,
                     AccountModelColumn.TOTAL_PERIOD,
                     AccountModelColumn.NOTES,
                     AccountModelColumn.TAX_INFO,
                     AccountModelColumn.TAX_INFO_SUB_ACCT,
                     AccountModelColumn.LAST_NUM,
                     AccountModelColumn.COLOR_PRESENT,
                     AccountModelColumn.COLOR_ACCOUNT,
                     AccountModelColumn.COLOR_BALANCE,
                     AccountModelColumn.COLOR_BALANCE_PERIOD,
                     AccountModelColumn.COLOR_CLEARED,
                     AccountModelColumn.COLOR_RECONCILED,
                     AccountModelColumn.COLOR_FUTURE_MIN,
                     AccountModelColumn.COLOR_TOTAL,
                     AccountModelColumn.COLOR_TOTAL_PERIOD]:
            return GObject.TYPE_STRING

        elif index == AccountModelColumn.PLACEHOLDER or index == AccountModelColumn.HIDDEN:
            return GObject.TYPE_BOOLEAN
        return GObject.TYPE_INVALID

    def do_get_iter(self, path):
        i = 0
        _iter = Gtk.TreeIter()
        depth = path.get_depth()
        if depth <= 0:
            _iter.stamp = 0
            return False, _iter
        indices = path.get_indices()
        if indices[0] != 0:
            _iter.stamp = 0
            return False, _iter
        parent = None
        account = self.root

        for i in range(1, depth, 1):
            parent = account
            account = parent.get_nth_child(indices[i])
            if account is None:
                _iter.stamp = 0
                return False, _iter
        _iter.stamp = self.stamp
        if account is None:
            _iter.stamp = 0
            return False, _iter
        self.accounts[id(account)] = account
        if parent is not None:
            _iter.user_data2 = id(parent)
            self.accounts[id(parent)] = parent
        _iter.user_data = id(account)
        _iter.user_data3 = indices[i - 1]
        return True, _iter

    def do_get_path(self, _iter):
        if self.root is None:
            return None
        account = self.accounts.get(_iter.user_data)
        parent = self.accounts.get(_iter.user_data2)
        path = Gtk.TreePath.new()
        while parent is not None:
            i = parent.get_child_index(account)
            if i == -1:
                return None
            path.prepend_index(i)
            account = parent
            parent = account.get_parent()
        path.prepend_index(0)
        return path

    def event_handler_account(self, entity, event_type, ed=None):
        if not isinstance(entity, Account):
            return
        account = entity
        if event_type != EVENT_ADD:
            self.clear_cached_values(account)
        if account.get_book() != self.book:
            return
        account_root = account.get_root()
        if account_root != self.root:
            return

        if event_type == EVENT_REMOVE:
            if ed is None or not isinstance(ed, EventData):
                return
            parent = ed.node if ed.node is not None else self.root
            path = self.get_path_from_account(parent)
            if path is None:
                return
            path.append_index(ed.idx)
            self.accounts.pop(id(account))
            self.row_deleted(path)
            self.propagate_change(path, 0)

        elif event_type == EVENT_MODIFY:
            path = self.get_path_from_account(account)
            if path is None:
                return
            _iter = self.do_get_iter(path)[1]
            if _iter is None:
                return
            self.row_changed(path, _iter)
            self.propagate_change(path, -1)

    def compute_period_balance(self, acct, recurse):
        if acct == self.root:
            return "", False
        t1 = AccountingPeriod.fiscal_start()
        t2 = AccountingPeriod.fiscal_end()
        if t1 > t2:
            return "", False
        b3 = acct.get_balance_change_for_period(t1, t2, recurse)
        return gs_account_print_amount(b3, PrintAmountInfo.account(acct, True)), b3 < 0

    def clear_cache(self):
        self.account_values_hash.clear()
        self.foreach(lambda model, path, _iter: self.row_changed(path, _iter))

    def clear_account_cached_values(self, account):
        if account is None:
            return
        _iter = self.get_iter_from_account(account)
        acct_guid_str = account.get_guid()
        for col in range(AccountModelColumn.NUM_COLUMNS):
            key = "%s,%d" % (acct_guid_str, col)
            try:
                self.account_values_hash.pop(key)
            except KeyError:
                pass
        if _iter is not None:
            path = self.do_get_path(_iter)
            if path is not None:
                self.row_changed(path, _iter)

    def clear_cached_values(self, account):

        if self.account_values_hash is None or not any(self.account_values_hash) or account is None:
            return
        self.clear_account_cached_values(account)
        parent = account.get_parent()
        while parent is not None:
            self.clear_account_cached_values(parent)
            parent = parent.get_parent()

    def set_cached_value(self, account, column, value):

        if self.account_values_hash is None or account is None:
            return None
        acct_guid_str = account.get_guid()
        key = "%s,%d" % (acct_guid_str, column)
        self.account_values_hash[key] = value

    def do_get_value(self, _iter, column):

        account = self.accounts.get(_iter.user_data)
        if account is None:
            return ""
        key = "{},{}".format(account.get_guid(), column)
        try:
            cached_string = self.account_values_hash[key]
            return cached_string
        except (KeyError, TypeError):
            pass

        if column == AccountModelColumn.NAME:
            cached_string = "New top level account" if account == self.root else account.name

        elif column == AccountModelColumn.TYPE:
            cached_string = account.get_type().to_string()

        elif column == AccountModelColumn.CODE:
            cached_string = account.get_code()

        elif column == AccountModelColumn.COMMODITY:
            cached_string = account.commodity.fullname if account.commodity is not None else ""

        elif column == AccountModelColumn.DESCRIPTION:
            cached_string = account.get_description()

        elif column == AccountModelColumn.PRESENT:
            cached_string = gs_ui_account_get_print_balance(Account.get_present_balance_in_currency, account, True)[0]

        elif column == AccountModelColumn.PRESENT_REPORT:
            cached_string = gs_ui_account_get_print_report_balance(Account.get_present_balance_in_currency,
                                                                   account, True)[0]

        elif column == AccountModelColumn.COLOR_PRESENT:
            negative = gs_ui_account_get_print_report_balance(Account.get_present_balance_in_currency, account, True)[1]
            cached_string = self.negative_color if negative else "black"

        elif column == AccountModelColumn.BALANCE:
            cached_string = gs_ui_account_get_print_balance(Account.get_balance_in_currency, account, False)[0]

        elif column == AccountModelColumn.BALANCE_REPORT:
            cached_string = gs_ui_account_get_print_report_balance(Account.get_balance_in_currency, account, False)[0]

        elif column == AccountModelColumn.COLOR_BALANCE:
            negative = gs_ui_account_get_print_balance(Account.get_balance_in_currency, account, False)[1]
            cached_string = self.negative_color if negative else "black"

        elif column == AccountModelColumn.BALANCE_PERIOD:
            cached_string = self.compute_period_balance(account, False)[0]

        elif column == AccountModelColumn.COLOR_BALANCE_PERIOD:
            negative = self.compute_period_balance(account, False)[1]
            cached_string = self.negative_color if negative else "black"

        elif column == AccountModelColumn.CLEARED:
            cached_string = gs_ui_account_get_print_balance(Account.get_cleared_balance_in_currency, account, False)[0]

        elif column == AccountModelColumn.CLEARED_REPORT:
            cached_string = gs_ui_account_get_print_report_balance(Account.get_cleared_balance_in_currency, account,
                                                                   False)[0]

        elif column == AccountModelColumn.COLOR_CLEARED:
            negative = gs_ui_account_get_print_balance(Account.get_cleared_balance_in_currency, account,
                                                       False)[1]
            cached_string = self.negative_color if negative else "black"

        elif column == AccountModelColumn.RECONCILED:
            cached_string = gs_ui_account_get_print_balance(Account.get_reconciled_balance_in_currency, account, False)[
                0]

        elif column == AccountModelColumn.RECONCILED_REPORT:
            cached_string = gs_ui_account_get_print_report_balance(Account.get_reconciled_balance_in_currency, account,
                                                                   False)[0]

        elif column == AccountModelColumn.COLOR_RECONCILED:
            negative = gs_ui_account_get_print_balance(Account.get_reconciled_balance_in_currency, account, False)[1]
            cached_string = self.negative_color if negative else "black"

        elif column == AccountModelColumn.FUTURE_MIN:
            cached_string = gs_ui_account_get_print_balance(Account.get_projected_minimum_balance_in_currency, account,
                                                            False)[0]

        elif column == AccountModelColumn.FUTURE_MIN_REPORT:
            cached_string = gs_ui_account_get_print_report_balance(Account.get_projected_minimum_balance_in_currency,
                                                                   account, False)[0]

        elif column == AccountModelColumn.COLOR_FUTURE_MIN:
            negative = \
                gs_ui_account_get_print_balance(Account.get_projected_minimum_balance_in_currency, account, False)[1]
            cached_string = self.negative_color if negative else "black"

        elif column == AccountModelColumn.TOTAL:
            cached_string = gs_ui_account_get_print_balance(Account.get_balance_in_currency, account, True)[0]

        elif column == AccountModelColumn.TOTAL_REPORT:
            cached_string = gs_ui_account_get_print_report_balance(Account.get_balance_in_currency, account, True)[0]

        elif column == AccountModelColumn.COLOR_TOTAL:
            negative = gs_ui_account_get_print_balance(Account.get_balance_in_currency, account, True)[1]
            cached_string = self.negative_color if negative else "black"

        elif column == AccountModelColumn.TOTAL_PERIOD:
            cached_string = self.compute_period_balance(account, True)[0]

        elif column == AccountModelColumn.COLOR_TOTAL_PERIOD:
            negative = self.compute_period_balance(account, True)[1]
            cached_string = self.negative_color if negative else "black"

        elif column == AccountModelColumn.COLOR_ACCOUNT:
            cached_string = account.color if account.color is not None else "black"

        elif column == AccountModelColumn.NOTES:
            cached_string = account.notes

        elif column == AccountModelColumn.TAX_INFO:
            cached_string = ""

        elif column == AccountModelColumn.TAX_INFO_SUB_ACCT:
            account_descendants = account.get_descendants()
            if any(account_descendants):
                sub_acct_tax_number = 0
                for descendant in account_descendants:
                    if descendant.get_tax_related():
                        sub_acct_tax_number += 1
                cached_string = "" if sub_acct_tax_number == 0 else "(Tax-related subaccounts: %d" % sub_acct_tax_number
            else:
                cached_string = ""

        elif column == AccountModelColumn.LAST_NUM:
            cached_string = int(account.get_last_num() or 0)

        elif column == AccountModelColumn.PLACEHOLDER:
            cached_string = account.placeholder
        elif column == AccountModelColumn.HIDDEN:
            cached_string = account.hidden
        else:
            cached_string = ""
        self.set_cached_value(account, column, cached_string)
        return cached_string

    def do_iter_next(self, _iter):

        parent = self.accounts.get(_iter.user_data2, None)
        if parent is None:
            _iter = Gtk.TreeIter()
            _iter.stamp = 0
            return False, _iter
        i = _iter.user_data3 + 1
        account = parent.get_nth_child(i)
        if account is None:
            _iter = Gtk.TreeIter()
            _iter.stamp = 0
            return False, _iter
        self.accounts[id(account)] = account
        self.accounts[id(parent)] = parent
        _iter.user_data = id(account)
        _iter.user_data2 = id(parent)
        _iter.user_data3 = i
        return True, _iter

    def do_iter_children(self, parent_iter):
        if self.root is None:
            parent_iter.stamp = 0
            return False, parent_iter
        if parent_iter is None:
            parent_iter.user_data = id(self.root)
            parent_iter.stamp = self.stamp
            parent_iter.user_data2 = -1
            parent_iter.user_data3 = 0
            return True, parent_iter
        parent = self.get_account(parent_iter)
        if parent is None:
            parent_iter.stamp = 0
            return False, parent_iter
        else:
            account = parent.get_nth_child(0)
            if account is None:
                parent_iter.stamp = 0
                return False, parent_iter
        parent_iter.user_data = id(account)
        parent_iter.user_data2 = id(parent)
        parent_iter.user_data3 = 0
        parent_iter.stamp = self.stamp
        return True, parent_iter

    def do_iter_has_child(self, _iter):

        account = self.get_account(_iter)
        if account is None:
            return False
        return bool(len(account))

    def do_iter_n_children(self, _iter):

        if _iter is None:
            return 1
        account = self.get_account(_iter)
        if account is None:
            return 0
        return len(account)

    def do_iter_nth_child(self, parent_iter, n):

        _iter = Gtk.TreeIter()
        if parent_iter is None:
            if n != 0:
                return False, _iter
            _iter.user_data = id(self.root)
            _iter.user_data2 = 0
            _iter.user_data3 = n
            _iter.stamp = self.stamp
            return True, _iter

        parent = self.get_account(parent_iter)
        account = parent.get_nth_child(n)
        if account is None:
            _iter.stamp = 0
            return False, _iter
        _iter.user_data = id(account)
        _iter.user_data2 = id(parent)
        self.accounts[id(account)] = account
        self.accounts[id(parent)] = parent
        _iter.user_data3 = n
        _iter.stamp = self.stamp
        return True, _iter

    def do_iter_parent(self, child):

        account = self.accounts.get(child.user_data, None)
        account = account.get_parent()
        if account is None:
            child.stamp = 0
            return False, child
        parent = account.get_parent()
        if parent is None:
            i = 0
        else:
            i = parent.get_child_index(account)
        child.user_data = id(account)
        child.user_data2 = id(parent)
        self.accounts[id(account)] = account
        self.accounts[id(parent)] = parent
        child.user_data3 = i
        return True, child

    def get_account(self, _iter):

        return self.accounts.get(_iter.user_data)

    def get_iter_from_account(self, account):

        if account is None:
            return None
        _iter = Gtk.TreeIter()
        _iter.user_data = id(account)
        _iter.stamp = self.stamp
        if account == self.root:
            _iter.user_data2 = 0
            _iter.user_data3 = 0
            return _iter
        if self.root != account.get_root():
            return None
        parent = account.get_parent()
        i = parent.get_child_index(account)
        _iter.user_data2 = id(parent)
        _iter.user_data3 = i
        self.accounts[id(account)] = account
        self.accounts[id(parent)] = parent
        return _iter

    def get_path_from_account(self, account):

        _iter = self.get_iter_from_account(account)
        if _iter is None:
            return None
        return self.do_get_path(_iter)

    def propagate_change(self, path, toggle_if_num):

        if not path.up():
            return
        if path.get_depth() == 0:
            return
        _iter = self.get_iter(path)
        if _iter is not None:
            self.row_changed(path, _iter)
            if self.iter_n_children(_iter) == toggle_if_num:
                self.row_has_child_toggled(path, _iter)
        while path.up():
            if path.get_depth() > 0:
                _iter = self.get_iter(path)
                if _iter is not None:
                    self.row_changed(path, _iter)

    def dispose_cb(self):
        if self.event_id > 0:
            Event.unregister_handler(self.event_id)
            self.event_id = 0
        self.accounts.clear()
        self.account_values_hash = None
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED, self.update_color)
        self.stamp = 0
        self.negative_color = None

    def __del__(self):
        Tracking.forget(self)
        self.book = None
        self.root = None


GObject.type_register(AccountModel)
