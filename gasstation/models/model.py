from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, GObject

TROW1 = 1
BLANK = 8
TROW2 = 2
SPLIT = 4


def IS_TROW1(x):
    return bool(x.user_data & TROW1)


def IS_BLANK(x):
    return bool(x.user_data & BLANK)


def IS_SPLIT(x):
    return bool(x.user_data & SPLIT)


def IS_TROW2(x):
    return bool(x.user_data & TROW2)


def IS_BLANK_SPLIT(x):
    return bool(IS_BLANK(x) and IS_SPLIT(x))


def IS_BLANK_TRANS(x):
    return IS_BLANK(x) and any([IS_TROW1(x), IS_TROW2(x)])


class Model(GObject.Object):
    def __init__(self):
        self.stamp = GLib.random_int_range(-2147483648, 2147483647)
        GObject.Object.__init__(self)

    def foreach_function(self, model, path, _iter, rr_list):
        row_ref = Gtk.TreeRowReference.new(model, path)
        rr_list.append(row_ref)
        return False

    def clear(self):
        rr_list = []
        self.foreach(self.foreach_function, rr_list)
        rr_list.reverse()
        for ref in rr_list:
            path = ref.get_path()
            self.row_deleted(path)
        self.increment_stamp()

    def do_get_flags(self):
        return Gtk.TreeModelFlags.LIST_ONLY

    def do_get_path(self, _iter):
        if _iter.stamp != self.stamp or _iter is None:
            return None
        path = Gtk.TreePath.new()
        path.append_index(_iter.user_data2)
        return path

    def increment_stamp(self):
        self.stamp += 1
        while self.stamp == 0:
            self.stamp += 1

    def insert_row_at_iter(self, _iter ):
        if _iter is None:
            return
        path = self.do_get_path(_iter)
        if path is None:
            return
        t, _iter= self.do_get_iter(path)
        self.row_inserted(path, _iter)
        self.update_parent(path)

    def delete_row_at_path(self, path):
        if path is None:
            return
        self.row_deleted(path)
        depth = path.get_depth()
        if depth == 2:
            self.update_parent(path)
        elif depth == 3:
            self.update_parent(path)

    def delete_row_at_iter(self, _iter):
        path = self.do_get_path(_iter)
        self.delete_row_at_path(path)

    def changed_row_at_iter(self, _iter):
        path = self.do_get_path(_iter)
        t, _iter = self.do_get_iter(path)
        if t:
            self.row_changed(path, _iter)


GObject.type_register(Model)
