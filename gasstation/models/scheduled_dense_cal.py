from .dense_calendar import DenseCalendarModel, Recurrence


class ScheduledDenseCal(DenseCalendarModel):

    def __init__(self):
        DenseCalendarModel.__init__(self)
        self.instances = None
        self.disposed = False

    def get_contained(self):
        l = []
        for scheduled_instances in self.instances.instance_list:
            if scheduled_instances.scheduled.get_enabled():
                l.append(id(scheduled_instances.scheduled))
        return l

    def get_name(self, tag):
        a = list(filter(lambda a: self.find_scheduled_with_tag(a, tag), self.instances.instance_list))
        if any(a):
            return a[0].scheduled.get_name()

    def get_info(self, tag):
        a = list(filter(lambda a: self.find_scheduled_with_tag(a, tag), self.instances.instance_list))
        if any(a):
            schedule = a[0].scheduled.get_schedule()
            return Recurrence.list_to_compact_string(schedule)

    def get_instance_count(self, tag):
        a = list(filter(lambda a: self.find_scheduled_with_tag(a, tag), self.instances.instance_list))
        if any(a):
            return len(a[0].instance_list)
        return 0

    def get_instance(self, tag, instance_index):
        a = list(filter(lambda a: self.find_scheduled_with_tag(a, tag), self.instances.instance_list))
        if any(a):
            return a[0].instance_list[instance_index].date

    def added_cb(self, model, scheduled_added):
        if scheduled_added.get_enabled():
            self.emit("added", id(scheduled_added))

    def updated_cb(self, model, scheduled_updated):
        model.update_scheduled_instances(scheduled_updated)
        if scheduled_updated.get_enabled():
            self.emit("update", id(scheduled_updated))
        else:
            self.emit("removing", id(scheduled_updated))

    def removing_cb(self, model, scheduled_to_be_removed):
        self.emit("removing", id(scheduled_to_be_removed))
        model.remove_scheduled_instances(scheduled_to_be_removed)

    @classmethod
    def new(cls, instances):
        adapter = cls()
        adapter.instances = instances
        instances.connect("added", adapter.added_cb)
        instances.connect("updated", adapter.updated_cb)
        instances.connect("removing", adapter.removing_cb)
        return adapter

    @staticmethod
    def find_scheduled_with_tag(scheduled_instances, find_data):
        return 1 if id(scheduled_instances.scheduled) == find_data else 0
