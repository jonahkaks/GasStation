from typing import Tuple

from libgasstation import PriceDB
from libgasstation.core.euro import gs_is_euro_currency, gs_convert_from_euro, gs_convert_to_euro
from .utilities import *


def get_commodities (accountlst, exclude_commodity=None):
    newdct = {}
    for acc in accountlst:
        comod = acc.get_commodity()
        comnm = comod.get_mnemonic()
        if not comnm in newdct:
            newdct[comod] = acc
    if exclude_commodity is not None:
        if exclude_commodity in newdct:
            del newdct[exclude_commodity]
    return sorted(newdct.keys())



def get_all_splits(accounts, end_date):
    query = Query.create_for(ID_SPLIT)
    book = Session.get_current_book()
    query.set_book(book)
    query_set_match_non_voids_only(query, book)
    query.add_account_match(accounts, GuidMatch.ANY, QueryOp.AND)
    query.add_date_match_tt(False, 0,
                            end_date is not None, end_date if end_date is not None else 0, QueryOp.AND)
    splits = query.run()
    query.destroy()
    return splits


def get_match_commodity_splits(currency_accounts, end_date, commodity):
    def filter_match(s):
        transaction_comm = s.get_transaction().get_currency()
        acc_comm = s.get_account().get_commodity()
        if not Commodity.equiv(transaction_comm, acc_comm) or (commodity is not None and Commodity.equiv(commodity, transaction_comm)):
            return Commodity.equiv(commodity, acc_comm)
        return True

    return list(filter(filter_match, get_all_splits(currency_accounts, end_date)))


def get_match_commodity_splits_sorted(currency_accounts, end_date, commodity):
    return sorted(get_match_commodity_splits(currency_accounts, end_date, commodity),
                  key=lambda s: s.get_transaction().get_post_date())


def get_all_commodity_splits(currency_accounts, end_date):
    return get_match_commodity_splits(currency_accounts, end_date, None)


def exchange_by_euro_numeric(foreign_commodity, foreign_numeric, domestic, date):
    exchange_by_euro((foreign_commodity, foreign_numeric), domestic, date)


def exchange_by_euro(foreign: Tuple[Commodity, Decimal], domestic: Commodity, date):
    if gs_is_euro_currency(domestic) and gs_is_euro_currency(foreign[0]):
        return (domestic, gs_convert_from_euro(domestic, gs_convert_to_euro(foreign[0],
                                                                            foreign[1])))


def exchange_if_same(foreign: Tuple[Commodity, Decimal], domestic: Commodity):
    return Commodity.equiv(foreign[0], domestic) and foreign


def make_exchange_function(exchange_alist):
    def exc(foreign, domestic):
        if foreign or exchange_by_euro(foreign, domestic, None):
            return exchange_if_same
        foreign_comm = foreign[0]
        pair = (foreign_comm, exchange_alist)
        return domestic, pair[1] * foreign[1] if pair else 0

    return exc


def exchange_by_pricedb_latest(foreign: Tuple[Commodity, Decimal], domestic: Commodity):
    if foreign is not None or exchange_by_euro(foreign, domestic, None):
        return exchange_if_same(foreign, domestic)
    return (domestic,
            PriceDB.get_db(Session.get_current_book()).convert_balance_latest_price(foreign[1],
                                                                                      foreign[0],
                                                                                      domestic))


def exchange_by_pricedb_nearest(foreign: Tuple[Commodity, Decimal], domestic: Commodity, date: datetime.date):
    if foreign is not None and exchange_by_euro(foreign, domestic, None):
        return exchange_if_same(foreign, domestic)
    ex = PriceDB.get_db(Session.get_current_book()).convert_balance_nearest_price_t64(foreign[1],
                                                                                        foreign[0],
                                                                                        domestic, date)
    return domestic, ex


class ExchangeCost(object):

    def __init__(self, report_commodity, end_date):

        pass
        # for exvl in

    def make_alist(self, e):
        exch_val = e[1][0].total / e[1][1].total
        newlst = [e[0], exch_val]

    def get_all_commodity_splits(self, currency_accounts, end_date_tp):
        return get_match_commodity_splits(currency_accounts, end_date_tp, None)

    def exchange_cost_totals(self, report_commodity, end_date):


        curr_accts = Session.get_current_root().get_descendants_sorted()
        sumlist = [[report_commodity, []]]

        if len(curr_accts) > 0:

            all_splts = []

            for acct in curr_accts:
                pass


class ExchangeFn(object):

    def __init__(self, date=None):
        self.date = date
        self.run = self.lambda_exchange_fn

    def lambda_exchange_fn(self, foreign, domestic):
        raise NotImplementedError


class AverageCostFn(ExchangeFn):

    def __init__(self, date, exchange_alist=None):
        super(AverageCostFn, self).__init__(date)
        self.exchange_alist = exchange_alist

    def exchange_fn(self, foreign, domestic):
        pass

    def lambda_exchange_fn(self, foreign, domestic):
        return self.exchange_fn(foreign, domestic)


class WeightedAverageFn(ExchangeFn):

    def __init__(self, date, exchange_alist=None):
        super(WeightedAverageFn, self).__init__(date)
        self.exchange_alist = exchange_alist

    def exchange_fn(self, foreign, domestic):
        pass

    def lambda_exchange_fn(self, foreign, domestic):
        return self.exchange_fn(foreign, domestic)


class PriceDBLatestFn(ExchangeFn):

    def __init__(self, date, exchange_alist=None):
        super(PriceDBLatestFn, self).__init__(date)
        self.exchange_alist = exchange_alist

    def lambda_exchange_fn(self, foreign, domestic):
        return exchange_by_pricedb_latest(foreign, domestic)


class PriceDBNearestFn(ExchangeFn):

    def __init__(self, date, exchange_alist=None):
        super(PriceDBNearestFn, self).__init__(date)
        self.exchange_alist = exchange_alist

    def lambda_exchange_fn(self, foreign, domestic):
        return exchange_by_pricedb_nearest(foreign, domestic, self.date)


class ExchangeTimeFn(object):

    def __init__(self, date):
        self.date = date
        self.run = self.lambda_exchange_fn

    def lambda_exchange_fn(self, foreign, domestic, date):
        raise NotImplementedError


class AverageExchangeTimeFn(ExchangeTimeFn):

    def __init__(self, date, exchange_alist=None):
        super(AverageExchangeTimeFn, self).__init__(date)
        self.exchange_alist = exchange_alist

    def exchange_fn(self, foreign, domestic):
        self.exchange_cost = ExchangeCost()

    def lambda_exchange_fn(self, foreign, domestic, date):
        self.exchange_fn(foreign, domestic)


class WeightedAverageTimeFn(ExchangeTimeFn):

    def __init__(self, date, exchange_alist=None):
        super(WeightedAverageTimeFn, self).__init__(date)
        self.exchange_alist = exchange_alist

    def lambda_exchange_fn(self, foreign, domestic, date):
        pass


class ActualTransactionsTimeFn(ExchangeTimeFn):

    def __init__(self, date, exchange_alist=None):
        super(ActualTransactionsTimeFn, self).__init__(date)
        self.exchange_alist = exchange_alist

    def lambda_exchange_fn(self, foreign, domestic, date):
        pass


class PriceDBLatestTimeFn(ExchangeTimeFn):

    def __init__(self, date, exchange_alist=None):
        super(PriceDBLatestTimeFn, self).__init__(date)
        self.exchange_alist = exchange_alist

    def lambda_exchange_fn(self, foreign, domestic, date):
        return exchange_by_pricedb_latest(foreign, domestic)


class PriceDBNearestTimeFn(ExchangeTimeFn):

    def __init__(self, date, exchange_alist=None):
        super(PriceDBNearestTimeFn, self).__init__(date)
        self.exchange_alist = exchange_alist

    def lambda_exchange_fn(self, foreign, domestic, date):
        return exchange_by_pricedb_nearest(foreign, domestic, date)


def case_exchange_fn(source_option, report_currency, to_date_tp):
    def default(foreign, domestic):
        return exchange_by_pricedb_nearest(foreign, domestic, to_date_tp)
    if source_option == 'average-cost':
        exchange_fn = AverageCostFn(report_currency, to_date_tp)
    elif source_option == 'weighted-average':
        exchange_fn = WeightedAverageFn(report_currency, to_date_tp)
    elif source_option == 'pricedb-latest':
        exchange_fn = PriceDBLatestFn(to_date_tp)
    elif source_option == 'pricedb-nearest':
        exchange_fn = PriceDBNearestFn(to_date_tp)
    else:
        exchange_fn = default
    return exchange_fn


def case_exchange_time_fn(source_option, report_currency, commodity_list, to_date_tp, start_percent, delta_percent):
    #

    # this also seems to store the date even though all functions when called
    # are passed the date

    if source_option == 'average-cost':
        exchange_fn = AverageExchangeTimeFn(report_currency, to_date_tp)
    elif source_option == 'weighted-average':
        exchange_fn = WeightedAverageTimeFn(report_currency, to_date_tp)
    elif source_option == 'actual-transactions':
        exchange_fn = ActualTransactionsTimeFn(report_currency, to_date_tp)
    elif source_option == 'pricedb-latest':
        exchange_fn = PriceDBLatestTimeFn(to_date_tp)
    elif source_option == 'pricedb-nearest':
        exchange_fn = PriceDBNearestTimeFn(to_date_tp)

    return exchange_fn
