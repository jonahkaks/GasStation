from gasstation.views.window import *
from libgasstation import AccountType, Customer, Supplier
from libgasstation.core.staff import Staff
from libgasstation.core.transquery import *
from libgasstation.html import *

from .webkit import *


def filter_accountlist_type(_types, accounts):
    return list(filter(lambda a: a.get_type() in _types, accounts))


def decompose_accountlist(accounts):
    return dict(map(lambda x: (x[0], filter_accountlist_type(x[1], accounts)),
                    [(AccountType.CURRENT_ASSET, [AccountType.CURRENT_ASSET, AccountType.FIXED_ASSET, AccountType.BANK, AccountType.CASH
                        , AccountType.CHECKING, AccountType.SAVINGS
                        , AccountType.MONEYMRKT, AccountType.RECEIVABLE
                        , AccountType.STOCK, AccountType.MUTUAL
                        , AccountType.CURRENCY]),
                     (AccountType.CURRENT_LIABILITY, [AccountType.CURRENT_LIABILITY, AccountType.LONG_TERM_LIABILITY,
                                                      AccountType.PAYABLE, AccountType.CREDIT,AccountType.CREDITLINE]),
                     (AccountType.EQUITY, [AccountType.EQUITY]),
                     (AccountType.INCOME, [AccountType.INCOME]),
                     (AccountType.EXPENSE, [AccountType.EXPENSE]),
                     (AccountType.TRADING, [AccountType.TRADING])]))


def account_get_type_string_plural(_type):
    return {AccountType.BANK: "Bank",
            AccountType.CASH: "Cash",
            AccountType.CREDIT: "Credits",
            AccountType.CURRENT_ASSET: "Current Assets",
            AccountType.FIXED_ASSET: "Fixed Assets",
            AccountType.CURRENT_LIABILITY: "Current Liabilities",
            AccountType.LONG_TERM_LIABILITY: "Long Term Liabilities",
            AccountType.STOCK: "Stocks",
            AccountType.MUTUAL: "Mutual Funds",
            AccountType.CURRENCY: "Currencies",
            AccountType.INCOME: "Income",
            AccountType.EXPENSE: "Expenses",
            AccountType.EQUITY: "Equities",
            AccountType.CHECKING: "Checking",
            AccountType.SAVINGS: "Savings",
            AccountType.MONEYMRKT: "Money Market",
            AccountType.RECEIVABLE: "Accounts Receivable",
            AccountType.PAYABLE: "Accounts Payable",
            AccountType.CREDITLINE: "Credit Lines",
            AccountType.TRADING: "Trading Accounts"}.get(_type)


def accounts_get_commodities(accounts, exclude_commodity):
    a = list(sorted(set(map(lambda a: a.get_commodity(), accounts)), key=lambda a: a.get_unique_name()))
    a.remove(exclude_commodity)
    return a


def cmp_to_key(mycmp, *args):
    'Convert a cmp= function into a key= function'

    class K:
        def __init__(self, obj, *args):
            self.obj = obj

        def __lt__(self, other):
            return mycmp(self.obj, other.obj, *args) < 0

        def __gt__(self, other):
            return mycmp(self.obj, other.obj, *args) > 0

        def __eq__(self, other):
            return mycmp(self.obj, other.obj, *args) == 0

        def __le__(self, other):
            return mycmp(self.obj, other.obj, *args) <= 0

        def __ge__(self, other):
            return mycmp(self.obj, other.obj, *args) >= 0

        def __ne__(self, other):
            return mycmp(self.obj, other.obj, *args) != 0

    return K


def report_starting(report_name):
    Window.show_progress("Building {} report ...".format(report_name), 0)


def report_render_starting(report_name):
    Window.show_progress("Rendering {} report ...".format(report_name), 0)


def report_percent_done(percent):
    if percent > 100:
        logging.warning("report more than 100% finished. " + str(percent))
    Window.show_progress("", percent)


def report_finished():
    Window.show_progress("", -1)


def accounts_count_splits(accounts):
    return sum(map(lambda acc: len(acc.splits), accounts))


def get_all_subaccounts(accountlst):
    newlst = []
    for acc in accountlst:
        subacc = acc.get_descendants_sorted()
        newlst.extend(subacc)
    return newlst


class ValueCollector:
    def __init__(self):
        self.value = Decimal(0)

    def add(self, amount: Decimal):
        self.value += amount

    def total(self):
        return self.value

    def __repr__(self):
        return "ValueCollector(%s)" % self.value


class CommodityCollector:

    def __init__(self):
        self.commoditylist = {}

    def add_commodity_value(self, commodity, value):
        if not commodity.get_unique_name() in self.commoditylist:
            cmdpr = [commodity, ValueCollector()]
            self.commoditylist[commodity.get_unique_name()] = cmdpr
        else:
            cmdpr = self.commoditylist[commodity.get_unique_name()]
        cmdpr[1].add(value)

    def add_commodity_clist(self, clist):
        for cmdpr in clist.values():
            self.add_commodity_value(cmdpr[0], cmdpr[1].total())

    def minus_commodity_clist(self, clist):
        for cmdpr in clist.values():
            self.add_commodity_value(cmdpr[0], -cmdpr[1].total())

    def process_commodity_list(self, fn):
        for pair in self.commoditylist.values():
            fn(pair[0], pair[1].total())

    def add(self, commodity, amount):
        self.add_commodity_value(commodity, amount)

    def merge(self, commodity_list):
        self.add_commodity_clist(commodity_list.commoditylist)

    def minusmerge(self, commodity_list):
        self.minus_commodity_clist(commodity_list.commoditylist)

    def format(self, commodity_fn):
        # hmm - commodity doesnt appear to be a commodity but a function??
        self.process_commodity_list(commodity_fn)

    def reset(self):
        self.commoditylist = {}

    def getpair(self, commod, sign=False):
        if commod.get_unique_name() in self.commoditylist:
            tmpcmdpr = self.commoditylist[commod.get_unique_name()]
            tmpval = tmpcmdpr[1].total()
            return [commod, -tmpval if sign else tmpval]
        else:
            return [commod, Decimal(0)]

    # these are separate definitions in scheme but would seem to be in this
    # class as all take collector as 1st argument
    def map(self, commodity_fn):
        self.format(commodity_fn)

    def assoc(self, commodity, sign=False):
        return self.getmonetary(commodity, sign)

    def assoc_pair(self, commodity, sign=False):
        return self.getpair(commodity, sign)

    # Im really confused here - not sorted out what objects are what
    # - in scheme the arguments are not consistent - commodity/currency/commoditycollector

    def getmonetary(self, curr, sign=False):

        # pdb.set_trace()

        # what is this doing?
        # ah - it looks up c in commoditylist
        # (pair (assoc c commoditylist))

        if curr.get_unique_name() in self.commoditylist:
            tmpcmdpr = self.commoditylist[curr.get_unique_name()]
            if sign:
                tmpval = tmpcmdpr[1].total() * -1
            else:
                tmpval = tmpcmdpr[1].total()
        else:
            tmpval = Decimal(0)

        return curr, tmpval

    def sum(self, report_currency, exchange_fn):
        sumcmd = self.sum_collector_commodity(report_currency, exchange_fn)
        return sumcmd

    def sum_collector_commodity(self, domestic, exchange_fn):
        balance = CommodityCollector()

        def commodity_fn(curr, val):
            if curr.equiv(domestic):
                balance.add(domestic, val)
            else:
                # pdb.set_trace()
                if callable(exchange_fn):
                    tmpval = exchange_fn((curr, val), domestic)
                else:
                    tmpval = exchange_fn.run((curr, val), domestic)
                balance.add(domestic, tmpval.amount)

        self.format(commodity_fn)
        return balance.getmonetary(domestic)


def query_set_match_non_voids_only(query, book):
    temp_query = Query.create_for(ID_SPLIT)
    temp_query.set_book(book)
    temp_query.add_cleared_match(ClearedMatch.VOIDED, QueryOp.AND)
    inv_query = temp_query.invert()
    query.merge_in_place(inv_query, QueryOp.AND)
    inv_query.destroy()
    temp_query.destroy()


def query_set_match_voids_only(query, book):
    temp_query = Query.create_for(ID_SPLIT)
    temp_query.set_book(book)
    temp_query.add_cleared_match(ClearedMatch.VOIDED, QueryOp.AND)
    query.merge_in_place(temp_query, QueryOp.AND)
    temp_query.destroy()


def account_get_trans_type_splits_interval(account_list, _type, start_date, end_date):
    if _type is None:
        _type = {}
    query = Query.create_for(ID_SPLIT)
    get_val = _type.get
    matchstr = get_val("str")
    case_sens = get_val("cased")
    regexp = get_val("regexp")
    closing = get_val("closing")
    book = Session.get_current_book()
    query.set_book(book)
    query_set_match_non_voids_only(query, book)
    query.add_account_match(account_list, GuidMatch.ANY, QueryOp.AND)
    query.add_date_match_tt(start_date is not None, start_date if start_date is not None else 0,
                            end_date is not None, end_date if end_date is not None else 0, QueryOp.AND)
    if matchstr is not None or closing is not None:
        query2 = Query.create_for(ID_SPLIT)
        if matchstr:
            query2.add_description_match(matchstr, case_sens, regexp, QueryCompare.CONTAINS, QueryOp.OR)
        if closing:
            query2.add_closing_trans_match(1, QueryOp.OR)
        query.merge_in_place(query2, QueryOp.AND)
        query2.destroy()
    splits = query.run()
    query.destroy()
    return splits


def account_get_trans_type_balance_interval(account_list, _type, start_date, end_date):
    total = CommodityCollector()
    for split in account_get_trans_type_splits_interval(account_list, _type, start_date, end_date):
        if not split.get_transaction().is_closing():
            total.add(split.get_account().get_commodity(), split.get_amount())
    return total


def account_get_trans_type_balance_interval_with_closing(account_list, _type, start_date, end_date):
    total = CommodityCollector()
    for split in account_get_trans_type_splits_interval(account_list, _type, start_date, end_date):
        total.add(split.get_account().get_commodity(), split.get_amount())
    return total


def account_get_comm_balance_interval(account, _from, to, include_children):
    accts = [account]
    if include_children:
        accts.extend(account.get_descendants_sorted())
    return account_get_trans_type_balance_interval(accts, None, _from, to)


def account_get_comm_balance_at_date(account, date, include_children):
    balance_collector = CommodityCollector()
    accounts = [account]
    if include_children:
        accounts.extend(account.get_descendants())
    for acct in accounts:
        balance_collector.add(acct.get_commodity(), acct.get_balance_as_of_date(date))
    return balance_collector


def accountlist_get_comm_balance_interval(accountlist, _from, to):
    return account_get_trans_type_balance_interval(accountlist, [], _from, to)


def accountlist_get_comm_balance_interval_with_closing(accountlist, _from, to):
    account_get_trans_type_balance_interval_with_closing(accountlist, [], _from, to)


def accountlist_get_comm_balance_at_date(accountlist, date):
    account_get_trans_type_balance_interval(accountlist, [], None, date)


def accountlist_get_comm_balance_at_date_with_closing(accountlist, date):
    account_get_trans_type_balance_interval_with_closing(accountlist, [], None, date)


def get_current_account_tree_depth():
    root = Session.get_current_root()
    return root.get_tree_depth()


def register_guid(_type, guid):
    return WebKitWebView.build_url(URL_TYPE_REGISTER, _type + str(guid), "")


def guid_ref(idstr, _type, guid):
    return WebKitWebView.build_url(_type, idstr + str(guid), "")


def account_anchor_text(acct):
    return register_guid("acct-guid=", str(acct.get_guid()))


def split_anchor_text(split):
    return register_guid("split-guid=", str(split.get_guid()))


def transaction_anchor_text(trans):
    return register_guid("trans-guid=", str(trans.get_guid()))


def report_anchor_text(report_id):
    return WebKitWebView.build_url(URL_TYPE_REPORT, "id=%d" % report_id)


def price_anchor_text(price):
    return WebKitWebView.build_url(URL_TYPE_PRICE, "price-guid=%s" % str(price.get_guid()))


def customer_anchor_text(customer):
    return guid_ref("customer=", URL_TYPE_CUSTOMER, customer.get_guid())


def job_anchor_text(job):
    return guid_ref("job=", URL_TYPE_JOB, job.get_guid())


def supplier_anchor_text(supplier):
    return guid_ref("supplier=", URL_TYPE_SUPPLIER, supplier.get_guid())


def staff_anchor_text(staff):
    return guid_ref("staff=", URL_TYPE_STAFF, staff.get_guid())


def invoice_anchor_text(invoice):
    return guid_ref("invoice=", URL_TYPE_INVOICE, invoice.get_guid())


def person_anchor_text(person):
    if isinstance(person, Customer):
        return customer_anchor_text(person)
    elif isinstance(person, Supplier):
        return customer_anchor_text(person)
    elif isinstance(person, Staff):
        return staff_anchor_text(person)


class Anchors:
    @staticmethod
    def report(report_name, src_report,option_list):
        from .report import Report
        src_options = src_report.options
        t = Report.find_template(report_name)
    #  (options (gnc:make-report-options reportname)))
    # (if options
    #  (begin
    #  (gnc:options-copy-values src-options options)
    #     (for-each
    #     (lambda (l)
    #      (let ((o (gnc:lookup-option options (car l) (cadr l))))
    # (if o
    # (gnc:option-set-value o (caddr l))
    # (warn "gnc:make-report-anchor:" reportname
    # " No such option: " (car l) (cadr l)))))
    # optionlist)
    # (let ((id (gnc:make-report reportname options)))
    # (gnc:report-anchor-text id)))
    # (warn "gnc:make-report-anchor: No such report: " reportname))))

    @staticmethod
    def account(acc):
        return ANCHOR(acc.get_name() if acc is not None else "", _href=account_anchor_text(acc))

    @staticmethod
    def split(split, text):
        return ANCHOR(text if text is not None else "", _href=split_anchor_text(split))

    @staticmethod
    def transaction(trans, text):
        return ANCHOR(text if text is not None else "", _href=transaction_anchor_text(trans))

    @staticmethod
    def person(person):
        contact = person.get_contact()
        text = contact.get_display_name() if contact is not None else ""
        return ANCHOR(text, _href=person_anchor_text(person))

    @staticmethod
    def invoice(invoice, text):
        return ANCHOR(text if text is not None else "",
                      _href=guid_ref("invoice=", URL_TYPE_INVOICE, invoice.get_guid()))


def make_options_link(report_id):
    return P(ANCHOR("Edit report options", _href=WebKitWebView.build_url(URL_TYPE_OPTIONS,
                                                                           "report-id=%d" % report_id)))


def multiline_to_html_text(text):
    if text is None:
        return []
    r = []
    for t in text.split("\n"):
        r.append(t)
        r.append(BR())
    return r


def make_generic_simple_warning(report_title_string, message):
    return [H3(report_title_string + ":"), H3(""), P(message)]


def make_generic_warning(report_title_string, report_id, warning_title_string, warning_string):
    return [H3(report_title_string + ":"), H3(warning_title_string), P(warning_string), make_options_link(report_id)]


def make_generic_options_warning(report_title_string, report_id):
    return make_generic_warning(report_title_string, report_id, "",
                                "This report requires you to specify certain report options.")


def make_no_account_warning(report_title_string, report_id):
    return make_generic_warning(report_title_string, report_id, "No accounts selected",
                                "This report requires accounts to be selected in the report options.")


def make_empty_data_warning(report_title_string, report_id):
    return make_generic_warning(report_title_string, report_id, "No data",
                                "The selected accounts contain no data/transactions (or only zeroes) for the selected "
                                "time period")
