import json

from gasstation.utilities.cursors import *
from gasstation.utilities.custom_dialogs import show_error, show_info, show_warning
from gasstation.utilities.extension import Extentions
from gasstation.views.dialogs.options import OptionDialog
from libgasstation.core.options import *
from libgasstation.html.stylesheets import HtmlStyleSheet

menu_name_reports = "Reports/StandardReports"
menu_name_asset_liability = "_Assets & Liabilities"
menu_name_income_expense = "_Income & Expense"
menu_name_customer_supplier = "_Customer & Supplier"
menu_name_fuel = "_Fuel"
menu_name_inventory = "Inventory"
menu_name_budget = "B_udget"
menu_name_taxes = "_Taxes"
menu_name_utility = "_Sample & Custom"
menu_name_experimental = "_Experimental"
menu_name_custom = "_Custom"
pagename_general = "General"
pagename_accounts = "Accounts"
pagename_display = "Display"
optname_reportname = "Report name"
optname_reportcolor = "Report color"
optname_stylesheet = "Stylesheet"
menu_name_business_reports = "_Business"
optname_invoice_number = "Invoice Number"

rpterr_dupe = "One of your reports has a report-guid that is a duplicate. " \
              "Please check the report system, especially your saved reports, for a report with this report-guid: "
rpterr_upgraded = "The GasStation report system has been upgraded. Your old saved reports have been transferred" \
                  " into a new format. If you experience trouble with saved reports," \
                  " please contact the GasStation development team."
rpterr_guid1 = "Wrong report definition: "
rpterr_guid2 = " Report is missing a GUID."
rptwarn_legacy = "Some reports stored in a legacy format were found. " \
                 "This format is not supported anymore so these reports may not have been restored properly."


class ReportDefaultParams:
    def __init__(self):
        self.db = None
        self.cur_report = None
        self.options = None
        self.win = None


class ReportTemplate:
    templates = {}

    def __init__(self, version: int = 0, name: str = None, guid: str = None, parent_type: str = None,
                 report_template: str = None, in_menu: bool = True, menu_path: str = None, menu_name: str = None,
                 menu_tooltip: str = None, export_types: list = None):
        self.version = version
        self.name = name
        self.guid = guid
        self.parent_type = parent_type
        self.report_template = report_template
        self.in_menu = in_menu
        self.export_types = export_types
        self.menu_name = menu_name
        self.menu_path = menu_path
        self.menu_tooltip = menu_tooltip
        if menu_tooltip is None:
            self.menu_tooltip = "Display a report %s" % menu_name
        self.ctext = None
        self.options = None
        self.__class__.templates[guid] = self

    def export(self, report_obj, export_types, filename):
        raise NotImplementedError

    def render(self, report_obj):
        raise NotImplementedError

    def generate_options(self):
        if self.options is not None:
            return
        namer = StringOption(pagename_general, optname_reportname, "0a", "Enter a descriptive name for this report.",
                             self.name)
        ns = list(map(lambda ss: (ss.get_name(), ss.get_name(), "%s stylesheet." % ss.get_name()),
                      HtmlStyleSheet.get_sheets()))
        stylesheet = MultiChoiceOption(pagename_general, optname_stylesheet, "0b",
                                       "Select a stylesheet for the report.", "Default",
                                       ns)
        options = NewOptions()
        if options.lookup_option(pagename_general, optname_reportname) is None:
            options.register_option(namer)
        if options.lookup_option(pagename_general, optname_stylesheet) is None:
            options.register_option(stylesheet)
        self.options = options

    def get_options(self):
        return self.options

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def set_parent_type(self, ptype):
        self.parent_type = ptype

    def get_parent_type(self):
        return self.parent_type

    def get_guid(self):
        return self.guid

    def set_guid(self, guid):
        self.guid = guid

    def get_menu_name(self):
        return self.menu_name if self.menu_name is not None else self.name

    def get_menu_path(self):
        return self.menu_path

    def get_menu_tip(self):
        return self.menu_tooltip

    def get_in_menu(self):
        return self.in_menu

    def get_version(self):
        return self.version

    def options_changed_cb(self, *args):
        return

    @classmethod
    def find(cls, tname):
        return cls.templates.get(tname)

    @classmethod
    def get(cls):
        return list(cls.templates.values())

    def __repr__(self):
        return u"ReportTemplate(%s)" % self.name


class Report:
    reports = []

    def __init__(self, template):
        self.dirty = True
        self.needs_save = False
        self.editor_widget = None
        self.ctext = None
        self.options = None
        self.template = template
        Report.reports.append(self)
        self.id = Report.reports.index(self)

    def get_export_types(self):
        return self.template.export_types

    def get_export_cb(self):
        return self.template.export

    def serialize(self):
        return json.dumps({"template": self.template.get_guid(), "options": self.get_options().serialize()})

    @classmethod
    def deserialize(cls, text):
        h = json.loads(text)
        template = cls.find_template(h["template"])
        if template is None:
            return None
        report = cls(template)
        options = h.get("options")
        if options is not None:
            report.get_options().deserialize(options)
        return report

    def get_options(self):
        if self.options is None:
            if self.template.options is None:
                self.template.generate_options()
            self.options = self.template.options
        return self.options

    def get_embedded_list(self):
        opt = self.options.lookup_option(pagename_general, "report-list")
        if opt is not None:
            return opt.get_value()
        return []

    def get_name(self):
        return self.get_options().lookup_option(pagename_general, optname_reportname).get_value()

    def get_stylesheet(self):
        name = self.get_options().lookup_option(pagename_general, optname_stylesheet).get_value()
        return HtmlStyleSheet.find(name)

    def set_stylesheet(self, sheet: HtmlStyleSheet):
        self.get_options().lookup_option(pagename_general, optname_stylesheet).set_value(sheet.get_name())

    def get_ctext(self):
        return self.ctext

    def set_ctext(self, ctext):
        self.ctext = ctext

    def get_type(self):
        return self.template.get_guid() if self.template is not None else ""

    def set_dirty(self, flag=True):
        self.dirty = flag

    def get_dirty(self):
        return self.dirty

    def set_needs_save(self, save):
        self.needs_save = save

    def get_needs_save(self):
        return self.needs_save

    def get_id(self):
        return self.id

    def set_id(self, _id):
        self.id = _id

    def get_editor_widget(self):
        return self.editor_widget

    def set_editor_widget(self, ed):
        self.editor_widget = ed

    def get_template(self):
        return self.template

    def set_template(self, custom):
        self.template = custom

    @classmethod
    def find(cls, _id):
        try:
            return cls.reports[_id]
        except IndexError:
            return None

    @classmethod
    def remove_by_id(cls, _id):
        try:
            cls.reports.pop(_id)
        except IndexError:
            pass

    def render_html(self, headers):
        if not self.dirty and self.ctext is not None:
            return self.ctext
        stylesheet = self.get_stylesheet()
        doc = self.template.render(self)
        html = None
        if doc is not None:
            if isinstance(doc, str):
                html = doc
            else:
                doc.set_stylesheet(stylesheet)
                html = doc.render(headers)
        self.ctext = html
        self.dirty = False
        return html

    @classmethod
    def run(cls, _id):
        report = cls.find(_id)
        gs_set_busy_cursor(None, True)
        html = None
        if report is not None:
            html = report.render_html(True)
        gs_unset_busy_cursor(None)
        return html

    @classmethod
    def run_id_string(cls, id_string):
        if id_string[:3] != "id=":
            return False
        _id = int(id_string[3:])
        return cls.run(_id)

    @classmethod
    def templates_foreach(cls, cb):
        for k, v in ReportTemplate.templates.items():
            cb(k, v)

    @classmethod
    def find_template(cls, template_id):
        return ReportTemplate.find(template_id)

    @classmethod
    def get(cls):
        return cls.reports

    def raise_editor(self):
        editor = self.get_editor_widget()
        if editor is not None:
            editor.present()
            return True
        else:
            return False

    def edit_options(self, parent):
        def default_params_editor(options, report, parent):
            def apply_cb(_, win):
                if win is None:
                    return
                results = win.db.commit()
                for mess in results:
                    show_error(win.win.window, mess)
                win.cur_report.set_dirty(True)

            def help_cb(_, prm):
                parent = prm.win.widget()
                show_info(parent, "Set the report options you want using this dialog.")

            def close_cb(_, win):
                win.cur_report.set_editor_widget(None)
                win.win.destroy()
                win.db.destroy()

            title = ""
            if report.raise_editor():
                return None
            else:
                prm = ReportDefaultParams()
                prm.options = options
                prm.cur_report = report
                prm.db = OptionDB(options)

                ptr = report.get_template()
                if ptr is not None:
                    ptr = ptr.get_name()
                    if isinstance(ptr, str):
                        title = ptr
                prm.win = OptionDialog.new(title, parent)
                prm.win.build_contents(prm.db)
                prm.db.clean()
                prm.win.set_apply_cb(apply_cb, prm)
                prm.win.set_help_cb(help_cb, prm)
                prm.win.set_close_cb(close_cb, prm)
                return prm.win.window

        if self.raise_editor():
            return True
        options = self.get_options()
        if options is None:
            show_warning(parent, "There are no options for this report.")
            return False
        rpt_type = self.get_type()
        if rpt_type == "d8ba4a2e89e8479ca9f6eccdeb164588":
            # options_widget = column_view_edit_options(options, report)
            options_widget = None
        else:
            options_widget = default_params_editor(options, self, parent)
        self.set_editor_widget(options_widget)
        return True


def report_menu_cb(window, report_id):
    from gasstation.plugins.pages import ReportPluginPage
    ReportPluginPage.open_report(Report(Report.find_template(report_id)).id, window)


def add_report_template_menu_items():
    template_items = {}
    Report.templates_foreach(lambda r, temp: template_items.__setitem__(temp.get_menu_name(), temp))
    for menu_name, template in sorted(filter(lambda temp: temp[1].in_menu, template_items.items())):
        report_guid = template.get_guid()
        menu_tip = template.get_menu_tip()
        menu_path = [menu_name_reports, template.get_menu_path()]
        Extentions.add_extention(Extentions.make_menu_item(menu_name, report_guid, menu_tip, menu_path,
                                                               report_menu_cb, report_guid))


def report_menu_setup():
    from libgasstation.core.hook import Hook, HOOK_REPORT
    asset_liability_menu = Extentions.make_menu(menu_name_asset_liability, [menu_name_reports])
    income_expense_menu = Extentions.make_menu(menu_name_income_expense, [menu_name_reports])
    customer_supplier_menu = Extentions.make_menu(menu_name_customer_supplier, [menu_name_reports])
    fuel_menu = Extentions.make_menu(menu_name_fuel, [menu_name_reports])
    inventory_menu = Extentions.make_menu(menu_name_inventory, [menu_name_reports])
    budget_menu = Extentions.make_menu(menu_name_budget, [menu_name_reports])
    utility_menu = Extentions.make_menu(menu_name_utility, [menu_name_reports])
    experimental_menu = Extentions.make_menu(menu_name_experimental, [menu_name_reports])
    tax_menu = Extentions.make_menu(menu_name_taxes, [menu_name_reports])
    business_menu = Extentions.make_menu(menu_name_business_reports, [menu_name_reports])
    Extentions.add_extention(income_expense_menu)
    Extentions.add_extention(customer_supplier_menu)
    Extentions.add_extention(asset_liability_menu)
    Extentions.add_extention(fuel_menu)
    Extentions.add_extention(inventory_menu)
    Extentions.add_extention(budget_menu)
    Extentions.add_extention(tax_menu)
    Extentions.add_extention(utility_menu)
    Extentions.add_extention(experimental_menu)
    Extentions.add_extention(business_menu)
    Extentions.add_extention(Extentions.make_menu_item("Saved Report Configurations",
                                                           "4d3dcdc8890b11df99dd94cddfd72085",
                                                           "Manage and run saved report configurations",
                                                           ["Reports/SavedReportConfigs"], lambda window: window))

    Hook.run(HOOK_REPORT)
    add_report_template_menu_items()
    Extentions.add_extention(Extentions.make_menu_item("Welcome Sample Report",
                                                           "ad80271c890b11dfa79f2dcedfd72085",
                                                           "Welcome-to-GasStation report screen",
                                                           [menu_name_reports, menu_name_utility],
                                                           lambda window: window))
