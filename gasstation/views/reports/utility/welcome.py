from gasstation.views.reports.report import Report, menu_name_utility


class Welcome(Report):
    def __init__(self):
        super().__init__(version=1, name="Welcome to GasStation", menu_path=menu_name_utility)


Report.type_register(Welcome)
