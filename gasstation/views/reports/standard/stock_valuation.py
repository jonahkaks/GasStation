import itertools

from gasstation.utilities.options import *
from ..report import ReportTemplate, menu_name_inventory, pagename_general
from ..utilities import *

optname_from_date = "Start Date"
optname_to_date = "End Date"
optname_location = "Location"
stock_valuation_report_guid = "c818ed95ce0245b3a2a1aee1c6179ge0"
from collections import defaultdict


class StockValuation(ReportTemplate):

    def __init__(self):
        super().__init__(version=1, name="Stock Valuation", menu_name="Inventory", guid=stock_valuation_report_guid,
                         menu_path=menu_name_inventory, export_types=[("CSV", "csv"), ("ODS", "ods"), ("XLS", "xls")])

    def export(self, report_obj, export_types, filename):
        pass

    def make_table(self, product_list, location, from_date, to_date):
        table = TABLE()
        self.table = table
        inv = defaultdict(dict)
        book = Session.get_current_book()
        types = []
        for f in fuel_list:
            if f.nozzle is not None and f.nozzle.tank is not None:
                name = f.nozzle.tank.product.name
                loc = f.get_location()
                types.append(name)
                date = f.date
                inv[date].setdefault(loc.get_name(), defaultdict(list))
                inv[date][loc.get_name()][name].append((f.get_litres(), f.get_amount()))

        types = set(types)
        table.append(THEAD(TR(TH(), *(TH(n.upper(), _colspan=2, _class="column_heading_center") for n in types)),
                           TR(TD("LOCATION"), *[TD("LITRES", _class="number_header"),
                                                TD("AMOUNT", _class="number_header")] * len(types))))
        tbody = TBODY()
        table.append(tbody)
        y = [0, 0] * len(types)
        info = book.default_currency.get_mnemonic()
        for i, k in enumerate(sorted(inv.keys())):
            tbody.append(TR(TD(print_datetime(k), _class="primary-subheading date-cell", _colspan=len(types) * 2 + 1)))
            for loc, data in inv[k].items():
                x = list(itertools.chain.from_iterable([self.resolve(data.get(n)) for n in types]))
                row_style = 'normal-row' if math.fmod(i + 1, 2) else 'alternate-row'

                tbody.append(TR(TD(loc, _class="text-cell"),
                                *[TD("{:,.2f}".format(f), _class="number-cell") for f in x], _class=row_style))
                y = map(sum, zip(y, x))
        table.append(TFOOT(TR(TH("TOTAL", _class="total-label-cell"),
                              *[TH(
                                  "{}{:,.2f}".format(info, f),
                                  _class="total-number-cell") for f in y], _class="grand-total")))
        return table

    def render(self, report_obj):
        report_starting(self.name)

        def opt_value(section, name):
            option = report_obj.options.lookup_option(section, name)
            if option is not None:
                return option.get_value()

        to_date = DateOption.absolute_time(opt_value("General", optname_to_date))
        from_date = DateOption.absolute_time(opt_value("General", optname_from_date))
        document = HtmlDocument()
        title = opt_value(pagename_general, "Title")
        location = opt_value(pagename_general, optname_location)
        query = QueryPrivate.create_for(ID_PRODUCT)
        query.set_book(Session.get_current_book())
        table = self.make_table(query.run(), location, from_date, to_date)
        document.set_title(title)
        document.add_object(table)
        query.destroy()
        report_finished()
        return document

    def generate_options(self):
        super().generate_options()
        reg = self.options.register_option
        OptionUtils.add_date_interval(self.options, "General", optname_from_date, optname_to_date, "a")
        reg(StringOption(pagename_general, "Title", "b", "The title of the report.", "Inventory Valuation Report"))
        reg(LocationOption(pagename_general, optname_location, "c", "", None))

        self.options.set_default_section("General")


StockValuation()
