# from gasstation.utilities.ui import *
# from gasstation.utilities.options import OptionUtils
# from libgasstation.html import HtmlDocument
# from ..commodity_utilities import *
# from ..report import *
# from libgasstation.core.account import AccountType
# from libgasstation.core.numeric import *
# from libgasstation.core.options import *
#
# units_denom = 100000000
# price_denom = 100000000
#
#
# class AdvancedPortfolio(ReportTemplate):
#
#     def __init__(self):
#
#         super(AdvancedPortfolio, self).__init__()
#
#         # need to set the  base variables
#         self.version = 1
#         self.name = "Advanced Portfolio"
#         self.report_guid = "68b8ab5b9524466a89840b9076876a6e"
#         self.menu_path = "_Assets & Liabilities"
#
#     def export(self, report_obj, export_types, filename):
#         pass
#
#     def generate_options(self):
#         self.options = NewOptions()
#
#         OptionUtils.add_report_date(self.options, 'General', "Date", "a")
#
#         OptionUtils.add_currency(self.options, 'General', "Report's currency", "c")
#
#         # explicit version used with just 2 options - latest or nearest
#         # should the list be a dict or not??
#         OptionUtils.add_price_source(self.options, 'General', "Price Source", "d", 'pricedb-latest')
#         self.options.register_option(MultiChoiceOption('General', "Price Source", "d",
#                                                        "The source of price information.",
#                                                        'pricedb-nearest',
#                                                        [['pricedb-latest', "Most recent",
#                                                          "The most recent recorded price."],
#                                                         ['pricedb-nearest', "Nearest in time",
#                                                          "The price recorded nearest in time to the report date."],
#                                                         ]))
#
#         self.options.register_option(MultiChoiceOption('General', "Basis calculation method", "e",
#                                                        "Basis calculation method.",
#                                                        'average-basis',
#                                                        [['average-basis', "Average",
#                                                          "Use average cost of all shares for basis."],
#                                                         ['fifo-basis', "FIFO",
#                                                          "Use first-in first-out method for basis."],
#                                                         ['lifo-basis', "LIFO",
#                                                          "Use last-in first-out method for basis."],
#                                                         ]))
#
#         self.options.register_option(SimpleBooleanOption("General", "Set preference for price list data", "e",
#
#                                                          "Prefer use of price editor pricing over transactions, where applicable.",
#                                                          True))
#
#         self.options.register_option(MultiChoiceOption('General', "How to report brokerage fees", "e",
#                                                        "How to report commissions and other brokerage fees.",
#                                                        'include-in-basis',
#                                                        [['include-in-basis', "Include in basis",
#                                                          "Include brokerage fees in the basis for the asset."],
#                                                         ['include-in-gain', "Include in gain",
#                                                          "Include brokerage fees in the gain and loss but not in the basis."],
#                                                         ['ignore-brokerage', "Ignore",
#                                                          "Ignore brokerage fees entirely."],
#                                                         ]))
#
#         self.options.register_option(SimpleBooleanOption("Display", "Show ticker symbols", "a",
#                                                          "Display the ticker symbols.", True))
#
#         self.options.register_option(SimpleBooleanOption("Display", "Show listings", "b",
#                                                          "Display exchange listings.", True))
#
#         self.options.register_option(SimpleBooleanOption("Display", "Show number of shares", "c",
#                                                          "Display numbers of shares in accounts.", True))
#
#         self.options.register_option(NumberRangeOption("Display", "Share decimal places", "d",
#                                                        "The number of decimal places to use for share numbers.", 2,
#                                                        0, 6, 0, 1))
#
#         self.options.register_option(SimpleBooleanOption("Display", "Show prices", "e",
#                                                          "Display share prices.", True))
#
#         self.options.register_option(AccountListOption("Accounts", "Accounts", "b",
#                                                        "Stock Accounts to report on.",
#                                                        lambda: self.local_filter_accountlist_type(),lambda accounts: self.local_account_validator(accounts), True))
#
#         # self.options.add_account_selection('Accounts', "Account Display Depth", "Always show sub-accounts", "Account", "a", '2',
#         #              lambda : self.local_filter_accountlist_type(, False)
#
#         self.options.register_option(SimpleBooleanOption("Accounts", "Include accounts with no shares", "e",
#                                                          "Include accounts that have a zero share balances.",
#                                                          False))
#
#         self.options.register_option(SimpleBooleanOption("General", "Show Exchange Rates","d",
#                                                          "Show the exchange rates used.", False))
#
#         self.options.register_option(SimpleBooleanOption("General", "Show Full Account Names","e",
#                                                          "Show full account names (including parent accounts).", True))
#
#         self.options.set_default_section('General')
#
#         return self.options
#
#     @staticmethod
#     def local_filter_accountlist_type():
#         #
#         accnts = Session.get_current_root().get_descendants_sorted()
#         retacc = filter_accountlist_type([AccountType.STOCK, AccountType.MUTUAL], accnts)
#         return retacc
#
#     @staticmethod
#     def local_account_validator(accounts):
#         #
#         retacc = filter_accountlist_type([AccountType.STOCK, AccountType.MUTUAL], accounts)
#         return [True, retacc]
#
#     @staticmethod
#     def is_split_account_type(split, type):
#         return split.get_Account().get_Type() == type
#
#     @staticmethod
#     def is_same_account(a1, a2):
#         return (a1.get_guid().compare(a2.get_guid()) == 0)
#
#     @staticmethod
#     def is_same_split(s1, s2):
#         return (s1.get_guid().compare(s2.get_guid()) == 0)
#
#     @staticmethod
#     def account_in_list(acnt, acclst):
#         for acc in acclst:
#             if AdvancedPortfolio.is_same_account(acnt, acc):
#                 return True
#         return False
#
#     @staticmethod
#     def account_in_alist(acnt, alst):
#         for acpr in alst:
#             # scheme uses caar to access the list
#             # but this seems right - acpr[0] is an account
#             # ah - caar - access first element of list then first element of that first element
#             if AdvancedPortfolio.is_same_account(acnt, acpr[0]):
#                 return acpr
#         return None
#
#     @staticmethod
#     def sum_basis(blist, currency_frac):
#         # ;; sum up the contents of the b-list built by basis-builder below
#         # ah - for blist element index 0 is number of units, index 1 is price/value
#         #
#         bsum = Decimal(0, 1)
#         for bitm in blist:
#             bsum = bsum.add(bitm[0].mul(bitm[1], currency_frac, NumericRound.ROUND), currency_frac, NumericRound.ROUND)
#         return bsum
#
#     @staticmethod
#     def units_basis(blist):
#         # ;; sum up the total number of units in the b-list built by basis-builder below
#         #
#         bsum = Decimal(0, 1)
#         for bitm in blist:
#             bsum = bsum.add(bitm[0], units_denom, NumericRound.ROUND)
#         return bsum
#
#     @staticmethod
#     def apply_basis_ratio(blist, units_ratio, value_ratio):
#         # ;; apply a ratio to an existing basis-list, useful for splits/mergers and spinoffs
#         # ;; I need to get a brain and use (map) for this.
#         #
#         print("Applying ratio", units_ratio.to_string())
#         bsum = Decimal(0, 1)
#         newlst = []
#         for bitm in blist:
#             urt = units_ratio.mul(bitm[0], units_denom, NumericRound.ROUND)
#             vrt = value_ratio.mul(bitm[1], units_denom, NumericRound.ROUND)
#             newlst.append((urt, vrt))
#         return newlst
#
#     @staticmethod
#     def dump_basis_list(blist):
#         strlst = []
#         strlst.append("[")
#         for bunits, bval in blist:
#             strlst.append("(%s, %s)," % (str(bunits.to_double()), str(bval.to_double())))
#         strlst.append("]")
#         return "".join(strlst)
#
#     def basis_builder(self, blist, bunits, bvalue, bmethod, currency_frac):
#         # ;; this builds a list for basis calculation and handles average, fifo and lifo methods
#         # ;; the list is cons cells of (units-of-stock . price-per-unit)... average method produces only one
#         # ;; cell that mutates to the new average. Need to add a date checker so that we allow for prices
#         # ;; coming in out of order, such as a transfer with a price adjusted to carryover the basis.
#         #
#
#         print("actually in basis-builder")
#         print("b-list is", self.dump_basis_list(blist))
#         print("b-units is", bunits)
#         print("b-value is", bvalue)
#         print("b-method is", bmethod)
#
#         # unlike scheme blist must be a list on entry - even if empty
#
#         # looks like this returns a blist
#
#         # ;; if there is no b-value, then this is a split/merger and needs special handling
#
#         # scheme cond
#
#         # ;; we have value and positive units, add units to basis
#         if not bvalue.is_zero() and bunits> 0:
#
#             if bmethod == 'average-basis':
#
#                 if len(blist) > 0:
#
#                     # what if blist has more than 1 element??
#                     # maybe not possible if doing average??
#                     if len(blist) > 1:
#                         print("average basis and blist > 1!!")
#                         print("average basis and blist > 1!!")
#
#                     #
#
#                     vl1 = bunits.add(blist[0][0], units_denom, NumericRound.ROUND)
#                     tvl1 = bvalue.add(blist[0][0].mul(blist[0][1], DENOM_AUTO, NumericDenom.REDUCE),
#                                       DENOM_AUTO, NumericDenom.REDUCE)
#                     tvl2 = bunits.add(blist[0][0], DENOM_AUTO, NumericDenom.REDUCE)
#                     vl2 = tvl1.div(tvl2, price_denom, NumericRound.ROUND)
#
#                     nwvl = (vl1, vl2)
#                     return [nwvl]
#
#                 else:
#
#                     vl2 = bvalue.div(bunits, price_denom, NumericRound.ROUND)
#                     nwvl = (bunits, vl2)
#                     blist.append(nwvl)
#
#             else:
#
#                 vl2 = bvalue.div(bunits, price_denom, NumericRound.ROUND)
#                 nwvl = (bunits, vl2)
#                 blist.append(nwvl)
#
#             return blist
#
#         # ;; we have value and negative units, remove units from basis
#         elif not bvalue.is_zero() and bunits < 0:
#
#             if len(blist) > 0:
#
#                 #
#
#                 if bmethod == 'fifo-basis':
#
#                     cmpval = bunits.abs().compare(blist[0][0])
#
#                     if cmpval == -1:
#
#                         # ;; Sold less than the first lot, create a new first lot from the remainder
#                         # ah - by definition sell means negative units so the add actually is a subtract!!
#                         new_units = bunits.add(blist[0][0], units_denom, NumericRound.ROUND)
#                         nwvl = (new_units, blist[0][1])
#                         # blist = nwvl + blist[1:]
#                         blist[0] = nwvl
#
#                     elif cmpval == 0:
#
#                         # ;; Sold all of the first lot
#                         blist.pop(0)
#
#                     elif cmpval == 1:
#
#                         # ;; Sold more than the first lot, delete it and recurse
#                         #
#                         newblst = blist[1:]
#                         blist = self.basis_builder(newblst, bunits.add(blist[0][0], units_denom, NumericRound.ROUND),
#                                                    bvalue,
#                                                    # ;; Only the sign of b-value matters since the new b-units is negative
#                                                    bmethod, currency_frac)
#
#
#                 elif bmethod == 'lifo-basis':
#
#                     # this reverses in place
#
#                     blist.reverse()
#
#                     cmpval = bunits.abs().compare(blist[0][0])
#
#                     if cmpval == -1:
#                         # ;; Sold less than the last lot
#                         new_units = bunits.add(blist[0][0], units_denom, NumericRound.ROUND)
#                         nwvl = (new_units, blist[0][1])
#                         blist[0] = nwvl
#                         blist.reverse()
#                     elif cmpval == 0:
#                         # ;; Sold all of the last lot
#                         blist = blist[1:]
#                         blist.reverse()
#                     elif cmpval == 1:
#                         # ;; Sold more than the last lot
#
#                         tmpblst = blist[1:]
#                         tmpblst.reverse()
#                         blist = self.basis_builder(tmpblst, bunits.add(blist[0][0], units_denom, NumericRound.ROUND),
#                                                    bvalue, bmethod, currency_frac)
#
#
#                 elif bmethod == 'average-basis':
#
#                     vl1 = blist[0][0].add(bunits, units_denom, NumericRound.ROUND)
#                     vl2 = blist[0][1]
#                     blist = [(vl1, vl2)]
#
#             return blist
#
#         # ;; no value, just units, this is a split/merge...
#         elif bvalue.is_zero() and not bunits.is_zero():
#
#             print("Split/Merging")
#
#             current_units = self.units_basis(blist)
#
#             #
#
#             advl = bunits.add(current_units, DENOM_AUTO, NumericDenom.REDUCE)
#             units_ratio = advl.div(current_units, DENOM_AUTO, NumericDenom.REDUCE)
#             # ;; If the units ratio is zero the stock is worthless and the value should be zero too
#             if units_ratio.is_zero():
#                 value_ratio = Decimal(0, 1)
#             else:
#                 value_ratio = Decimal(1, 1).div(units_ratio, DENOM_AUTO, NumericDenom.REDUCE)
#
#             newblst = self.apply_basis_ratio(blist, units_ratio, value_ratio)
#
#             return newblst
#
#         # ;; If there are no units, just a value, then its a spin-off,
#         # ;; calculate a ratio for the values, but leave the units alone
#         # ;; with a ratio of 1
#         elif bunits.is_zero() and not bvalue.is_zero():
#
#             print("Spinoff")
#
#             current_value = self.sum_basis(blist, DENOM_AUTO)
#
#             advl = bvalue.add(current_value, DENOM_AUTO, NumericDenom.REDUCE)
#             value_ratio = advl.div(current_value, DENOM_AUTO, NumericDenom.REDUCE)
#
#             newblst = self.apply_basis_ratio(blist, Decimal(1, 1), value_ratio)
#
#             return newblst
#
#         # ;; when all else fails, just send the b-list back
#         else:
#
#             return blist
#
#     def find_price(self, price_list, currency):
#
#         # ;; Given a price list and a currency find the price for that currency on the list.
#         # ;; If there is none for the requested currency, return the first one.
#         # ;; The price list is released but the price returned is ref counted.
#
#         # note according to me if there are multiple prices for a currency the last one
#         # is returned - not sure about this though
#
#         if len(price_list) == 0: return None
#
#         price = None
#         for prcitm in price_list:
#             if Commodity.equiv(prcitm.get_currency(), currency):
#                 price = prcitm
#
#         if price == None:
#             price = price_list[0]
#
#         return price
#
#
#     def is_parent_or_sibling(self, a1, a2):
#         a1parent = a1.get_parent()
#         a2parent = a2.get_parent()
#         if self.is_same_account(a2parent, a1) or self.is_same_account(a1parent, a2) or self.is_same_account(a1parent,
#                                                                                                             a2parent):
#             return True
#         return False
#
#     def is_spin_off(self, split, current):
#         # ;; Test whether the given split is the source of a spin off transaction
#         # ;; This will be a no-units split with only one other split.
#         # ;; xaccSplitget_OtherSplit only returns on a two-split transaction.  It's not a spinoff
#         # ;; is the other split is in an income or expense account.
#         # need exception wrap for python
#         try:
#             other_split = split.get_OtherSplit()
#         except Exception as errexc:
#             other_split = None
#
#         if split.get_Amount().is_zero() and \
#                 self.is_same_account(current, split.get_Account()) and \
#                 other_split is not None and \
#                 self.is_split_account_type(other_split, AccountType.EXPENSE) and \
#                 self.is_split_account_type(other_split, AccountType.INCOME):
#             return True
#
#         return False
#
#     def table_add_stock_rows(self, accounts, to_date_tp, from_date_tp, report_currency, exchange_fn, price_fn,
#                              price_source, include_empty, show_symbol, show_listing, show_shares, show_price,
#                              basis_method,
#                              prefer_pricelist, handle_brokerage_fees,
#                              total_basis, total_value, total_moneyin, total_moneyout, total_income, total_gain,
#                              total_ugain, total_brokerage):
#
#         optobj = self.options.lookup_option('Display', 'Share decimal places')
#         num_places = optobj.get_option_value()
#         share_print_info = PrintAmountInfo.share_places(num_places)
#         work_to_do = len(accounts)
#         work_done = 0
#
#         # try adjusting the date to day+1 for the balance - currently doesnt seem to include
#         # the ending day - even though the time is set to 23:59:59
#         # nice idea but no go - we have to treat the case of going to zero specially
#         # well it appears the original scheme works off by day 1 - so a partial sale
#         # is not shown on the sale day but on the day after - and a full sale
#         # still shows unrealized gain on the sale day - and removed from list the
#         # day after
#         # so the big question is why does the python version give wrong results
#         # if end on the sale day - we have both the realized gain (post sale)
#         # and the unrealised gain (pre sale)
#
#         # day1 = to_date_tp + datetime.timedelta(days=1)
#         # day1 = day1.replace(hour=0,minute=0,second=0,microsecond=0)
#         day1 = to_date_tp.replace(hour=23, minute=59, second=59)
#
#         for row, curacc in enumerate(accounts):
#
#             ## for debug limit to just WFM
#             # if curacc.get_Name().find('WFM') < 0:
#             #    continue
#
#             print()
#             print("Doing account row", curacc.get_Name(), curacc)
#
#             if (row % 2) == 0:
#                 new_row = self.document.StyleSubElement(self.new_table, 'normal-row')
#             else:
#                 new_row = self.document.StyleSubElement(self.new_table, 'alternate-row')
#             new_row.text = "\n"
#             new_row.tail = "\n"
#
#             commod = curacc.get_commodity()
#
#             #
#             ticker_symbol = commod.get_mnemonic()
#             listing = commod.get_namespace()
#
#             # unit_collector = account_get_comm_balance_at_date(curacc, to_date_tp)
#             unit_collector = account_get_comm_balance_at_date(curacc, day1, True)
#
#             units = unit_collector.getpair(commod)[1]
#
#             print("units", units.to_string())
#
#             # ;; Counter to keep track of stuff
#             brokeragecoll = CommodityCollector()
#             dividendcoll = CommodityCollector()
#             moneyincoll = CommodityCollector()
#             moneyoutcoll = CommodityCollector()
#             gaincoll = CommodityCollector()
#
#             # ;; the price of the commodity at the time of the report
#             # for placeholder stock accounts we get an empty list
#             #
#             price_list = price_fn(commod, report_currency, to_date_tp)
#             if len(price_list) > 0:
#                 price = price_list[0]
#             else:
#                 price = None
#
#             # ;; the value of the commodity, expressed in terms of
#             # ;; the report's currency.
#             value = Monetary(report_currency, Decimal(0, 1))  # ;; Set later
#             currency_frac = report_currency.get_fraction()
#
#             pricing_transaction = False
#             use_transaction = False
#             basis_list = []
#             # ;; setup an alist for the splits we've already seen.
#             # - in python make a dict - we look up entries by guid key
#             seen_trans = {}
#             # ;; Account used to hold remainders from income reinvestments and
#             # ;; running total of amount moved there
#             drp_holding_account = False
#             drp_holding_amount = Decimal(0, 1)
#
#             def my_exchange_fn(fromunits, tocurrency):
#
#                 #
#
#                 if Commodity.equiv(report_currency, tocurrency) and \
#                         Commodity.equiv(fromunits.commodity, commod):
#                     # ;; Have a price for this commodity, but not necessarily in the report's
#                     # ;; currency.  get_ the value in the commodity's currency and convert it to
#                     # ;; report currency.
#                     prccmd = price.commodity if use_transaction else price.get_currency()
#                     prcamt = price.amount if use_transaction else price.get_value()
#                     prcval = fromunits.amount.mul(prcamt, currency_frac, NumericRound.ROUND)
#                     new_val = Monetary(prccmd, prcval)
#                     runval = exchange_fn.run(new_val, tocurrency)
#                 else:
#                     runval = exchange_fn.run(fromunits, tocurrency)
#                 return runval
#
#             print("Starting account ", curacc.get_Name(), ", initial price: ", end='')
#             if price != None:
#                 print(Monetary(price.get_currency(), price.get_value()).to_currency_string())
#             else:
#                 print("None")
#
#             # ;; If we have a price that can't be converted to the report currency
#             # ;; don't use it
#             if price != None:
#                 tmpprc = exchange_fn.run(Monetary(price.get_currency(), Decimal(100, 1)),
#                                          report_currency)
#                 if tmpprc.amount.is_zero():
#                     price = None
#
#             # ;; If we are told to use a pricing transaction, or if we don't have a price
#             # ;; from the price DB, find a good transaction to use.
#             if not use_transaction and (price == None or not prefer_pricelist):
#                 #
#                 if price_source == 'pricedb-latest':
#                     prctim = datetime.now().date()
#                 elif price_source == 'pricedb-nearest':
#                     prctim = to_date_tp
#                 else:
#                     prctim = datetime.now().date()  # ;; error, but don't crash
#                 # yes - curacc is current single account - need list for match_commodity_splits_sorted
#                 split_list = gnc_commodity_utilities.match_commodity_splits_sorted([curacc], prctim)
#
#                 # so we sort early to late then reverse
#                 split_list.reverse()
#
#                 # ;; Find the first (most recent) one that can be converted to report currency
#                 spltindx = 0
#                 while not use_transaction and spltindx < len(split_list):
#
#                     split = split_list[spltindx]
#
#                     if not split.get_Amount().is_zero() and not split.get_Value().is_zero():
#
#                         trans = split.get_Parent()
#                         trans_currency = trans.get_Currency()
#                         trans_price = exchange_fn.run(
#                             Monetary(trans_currency, split.get_share_price()), report_currency)
#                         if not trans_price.amount.is_zero():
#                             # ;; We can exchange the price from this transaction into the report currency
#                             pricing_transaction = trans
#                             price = trans_price
#                             use_transaction = True
#
#                     spltindx += 1
#
#                 if price != None:
#                     try:
#                         print("Using non-transaction price", price.to_currency_string())
#                     except Exception as errexc:
#                         print("pricing error")
#
#                         print("pricing error")
#
#             # ;; If we still don't have a price, use a price of 1 and complain later
#             if price == None:
#                 price = Monetary(report_currency, Decimal(1, 1))
#                 # ;; If use-transaction is set, but pricing-transaction isn't set, it's a bogus price
#                 use_transaction = True
#                 pricing_transaction = False
#
#             # ;; Now that we have a pricing transaction if needed, set the value of the asset
#             value = my_exchange_fn(Monetary(commod, units), report_currency)
#
#             print("Value ", value.to_currency_string())
#             print(" from ", Monetary(commod, units).to_currency_string())
#
#             #
#
#             # so it looks as though what I need to do to properly handle total sale days
#             # is to detect if the zeroing splits occur on to_date_tp
#             # (the units from above balance call will be 0 so the later test for units being non
#             #  zero is not good)
#             is_sale_on_date = False
#             shares_sold_on_date = Decimal(0, 1)
#             prev_basis_list = []
#
#             # ;; we're looking at each split we find in the account. these splits
#             # ;; could refer to the same transaction, so we have to examine each
#             # ;; split, determine what kind of split it is and then act accordingly.
#             for split_acc in curacc.get_split_list():
#
#                 work_done += 1
#
#                 report_percent_done((work_done / float(work_to_do)) * 100.0)
#
#                 #
#                 parent = split_acc.get_Parent()
#                 transaction_date = parent.RetDatePosted().date()
#                 commod_currency = parent.get_Currency()
#                 commod_currency_frac = commod_currency.get_fraction()
#
#                 if transaction_date <= to_date_tp.date() and \
#                         not parent.get_guid() in seen_trans:
#
#                     # this is pure date comparison (ie no time) - so includes to_date_tp
#                     if transaction_date == to_date_tp.date():
#                         is_sale_on_date = True
#
#                     trans_income = Decimal(0, 1)
#                     trans_brokerage = Decimal(0, 1)
#                     trans_shares = Decimal(0, 1)
#                     shares_bought = Decimal(0, 1)
#                     trans_sold = Decimal(0, 1)
#                     trans_bought = Decimal(0, 1)
#                     trans_spinoff = Decimal(0, 1)
#                     trans_drp_residual = Decimal(0, 1)
#                     trans_drp_account = None
#
#                     # ;; Add this transaction to the list of processed transactions so we don't
#                     # ;; do it again if there is another split in it for this account
#                     seen_trans[parent.get_guid()] = 1
#
#                     #
#
#                     # ;; Go through all the splits in the transaction to get an overall idea of
#                     # ;; what it does in terms of income, money in or out, shares bought or sold, etc.
#                     for split in parent.get_split_list():
#
#                         print("split for", split.get_Account().get_Name(), split.get_Value().to_string())
#
#                         split_units = split.get_Amount()
#                         split_value = split.get_Value()
#
#                         # even basic call fails
#                         # - so wrap with try then test
#                         try:
#                             other_split = split.get_OtherSplit()
#                         except Exception as errexc:
#                             other_split = None
#                         print("other split for", other_split)
#
#                         if self.is_split_account_type(split, AccountType.EXPENSE):
#                             # ;; Brokerage expense unless a two split transaction with other split
#                             # ;; in the stock account in which case it's a stock donation to charity.
#                             # why does this work in Scheme - get_OtherSplit returns None - or null list in scheme
#                             # if more than 2 splits in a transaction
#                             #
#                             # other_split = split.get_OtherSplit()
#                             try:
#                                 other_split = split.get_OtherSplit()
#                             except Exception as errexc:
#                                 other_split = None
#                             if other_split == None or \
#                                     not self.is_same_account(curacc, split.get_OtherSplit().get_Account()):
#                                 trans_brokerage = trans_brokerage.add(split_value, commod_currency_frac,
#                                                                       NumericRound.ROUND)
#                         elif self.is_split_account_type(split, AccountType.INCOME):
#                             trans_income = trans_income.sub(split_value, commod_currency_frac, NumericRound.ROUND)
#                         elif self.is_same_account(curacc, split.get_Account()):
#                             trans_shares = trans_shares.add(split_units.abs(), units_denom, NumericRound.ROUND)
#                             if split_units.is_zero():
#                                 if self.is_spin_off(split, curacc):
#                                     # ;; Count money used in a spin off as money out
#                                     if split_value < 0:
#                                         trans_spinoff = trans_spinoff.sub(split_value, commod_currency_frac,
#                                                                           NumericRound.ROUND)
#                                 else:
#                                     if split_value.is_zero():
#                                         # ;; Gain/loss split (amount zero, value non-zero, and not spinoff).  There will be
#                                         # ;; a corresponding income split that will incorrectly be added to trans- income
#                                         # ;; Fix that by subtracting it here
#                                         trans_income = trans_income.sub(split_value, commod_currency_frac,
#                                                                         NumericRound.ROUND)
#                             else:
#                                 # ;; Non-zero amount, add the value to the sale or purchase total.
#                                 if split_value> 0:
#                                     trans_bought = trans_bought.add(split_value, commod_currency_frac,
#                                                                     NumericRound.ROUND)
#                                     shares_bought = shares_bought.add(split_units, units_denom, NumericRound.ROUND)
#                                 else:
#                                     trans_sold = trans_sold.sub(split_value, commod_currency_frac, NumericRound.ROUND)
#                                     if is_sale_on_date:
#                                         shares_sold_on_date = shares_sold_on_date.add(split_units, units_denom,
#                                                                                       NumericRound.ROUND)
#                         elif self.is_split_account_type(split, AccountType.ASSET):
#                             # ;; If all the asset accounts mentioned in the transaction are siblings of each other
#                             # ;; keep track of the money transfered to them if it is in the correct currency
#                             if trans_drp_account == None:
#                                 trans_drp_account = split.get_Account()
#                                 if Commodity.equiv(commod_currency, trans_drp_account.get_commodity()):
#                                     trans_drp_residual = split_value
#                                 else:
#                                     trans_drp_account = None
#                             else:
#                                 if trans_drp_account != None:
#                                     if self.is_parent_or_sibling(trans_drp_account, split.get_Account()):
#                                         trans_drp_residual = trans_drp_residual.add(split_value, commod_currency_frac,
#                                                                                     NumericRound.ROUND)
#                                     else:
#                                         trans_drp_account = None
#
#                     print("Income: ", trans_income.to_string())
#                     print(" Brokerage: ", trans_brokerage.to_string())
#                     print(" Shares traded: ", trans_shares.to_string())
#                     print(" Shares bought: ", shares_bought.to_string())
#                     print(" Value sold: ", trans_sold.to_string())
#                     print(" Value purchased: ", trans_bought.to_string())
#                     print(" Spinoff value ", trans_spinoff.to_string())
#                     print(" Trans DRP residual: ", trans_drp_residual.to_string())
#
#                     # ;; We need to calculate several things for this transaction:
#                     # ;; 1. Total income: this is already in trans-income
#                     # ;; 2. Change in basis: calculated by loop below that looks at every
#                     # ;;    that acquires or disposes of shares
#                     # ;; 3. Realized gain: also calculated below while calculating basis
#                     # ;; 4. Money in to the account: this is the value of shares bought
#                     # ;;    except those purchased with reinvested income
#                     # ;; 5. Money out: the money received by disposing of shares.   This
#                     # ;;    is in trans-sold plus trans-spinoff
#                     # ;; 6. Brokerage fees: this is in trans-brokerage
#
#                     # ;; Income
#                     dividendcoll.add(commod_currency, trans_income)
#
#                     # ;; Brokerage fees.  May be either ignored or part of basis, but that
#                     # ;; will be dealt with elsewhere.
#                     brokeragecoll.add(commod_currency, trans_brokerage)
#
#                     # ;; Add brokerage fees to trans-bought if not ignoring them and there are any
#                     if handle_brokerage_fees != 'ignore-brokerage' and \
#                             trans_brokerage> 0 and trans_shares> 0:
#                         fee_frac = shares_bought.div(trans_shares, DENOM_AUTO, NumericDenom.REDUCE)
#                         fees = trans_brokerage.mul(fee_frac, commod_currency_frac, NumericRound.ROUND)
#                         trans_bought = trans_bought.add(fees, commod_currency_frac, NumericRound.ROUND)
#
#                     # ;; Update the running total of the money in the DRP residual account.  This is relevant
#                     # ;; if this is a reinvestment transaction (both income and purchase) and there seems to
#                     # ;; asset accounts used to hold excess income.
#                     if trans_drp_account != None and trans_income> 0 and trans_bought> 0:
#
#                         if drp_holding_account == None:
#
#                             drp_holding_account = trans_drp_account
#                             drp_holding_amount = trans_drp_residual
#
#                         else:
#
#                             if drp_holding_account != None and self.is_parent_or_sibling(trans_drp_account,
#                                                                                          drp_holding_account):
#                                 drp_holding_amount = drp_holding_amount.add(trans_drp_residual, commod_curency_frac,
#                                                                             NumericRound.ROUND)
#                             else:
#                                 # ;; Wrong account (or no account), assume there isn't a DRP holding account
#                                 drp_holding_account = None
#                                 trans_drp_residual = Decimal(0, 1)
#                                 drp_holding_amount = Decimal(0, 1)
#
#                     # ;; Set trans-bought to the amount of money moved in to the account which was used to
#                     # ;; purchase more shares.  If this is not a DRP transaction then all money used to purchase
#                     # ;; shares is money in.
#                     if trans_income> 0 and trans_bought> 0:
#
#                         trans_bought = trans_bought.sub(trans_income, commod_currency_frac, NumericRound.ROUND)
#                         trans_bought = trans_bought.add(trans_drp_residual, commod_currency_frac, NumericRound.ROUND)
#                         trans_bought = trans_bought.sub(drp_holding_amount, commod_currency_frac, NumericRound.ROUND)
#
#                         # ;; If the DRP holding account balance is negative, adjust it by the amount
#                         # ;; used in this transaction
#                         if drp_holding_amount < 0 and trans_bought> 0:
#                             drp_holding_amount = drp_holding_amount.add(trans_bought, commod_currency_frac,
#                                                                         NumericRound.ROUND)
#
#                         # ;; Money in is never more than amount spent to purchase shares
#                         if trans_bought < 0:
#                             trans_bought = Decimal(0, 1)
#
#                     print("Adjusted trans-bought ", trans_bought.to_string())
#                     print(" DRP holding account ", drp_holding_amount.to_string())
#
#                     moneyincoll.add(commod_currency, trans_bought)
#                     moneyoutcoll.add(commod_currency, trans_sold)
#                     moneyoutcoll.add(commod_currency, trans_spinoff)
#
#                     # ;; Look at splits again to handle changes in basis and realized gains
#
#                     for split in parent.get_split_list():
#
#                         # ;; get the split's units and value
#                         split_units = split.get_Amount()
#                         split_value = split.get_Value()
#
#                         print("Pass 2: split units ", split_units.to_string(), " split-value ",
#                               split_value.to_string(), " commod-currency ",
#                               commod_currency.get_printname())
#
#                         if not split_units.is_zero() and self.is_same_account(curacc, split.get_Account()):
#                             # ;; Split into subject account with non-zero amount.  This is a purchase
#                             # ;; or a sale, adjust the basis
#                             split_value_currency = my_exchange_fn(
#                                 Monetary(commod_currency, split_value),
#                                 report_currency).amount
#                             orig_basis = self.sum_basis(basis_list, currency_frac)
#                             # ;; proportion of the fees attributable to this split
#                             fee_ratio = split_units.abs().div(trans_shares, DENOM_AUTO, NumericDenom.REDUCE)
#                             # ;; Fees for this split in report currency
#                             fees_currency = my_exchange_fn(Monetary(commod_currency,
#                                                                     fee_ratio.mul(
#                                                                         trans_brokerage,
#                                                                         commod_currency_frac,
#                                                                         NumericRound.ROUND)),
#                                                            report_currency).amount
#                             if handle_brokerage_fees == 'include-in-basis':
#                                 # ;; Include brokerage fees in basis
#                                 split_value_with_fees = split_value_currency.add(fees_currency, currency_frac,
#                                                                                  NumericRound.ROUND)
#                             else:
#                                 split_value_with_fees = split_value_currency
#
#                             print("going in to basis list ", self.dump_basis_list(basis_list), " ",
#                                   split_units.to_string(), " ",
#                                   split_value_with_fees.to_string())
#
#                             # store basis list before sale - needed if the last report day is the sale day
#                             # (a new basis is only useful for later sales)
#                             if is_sale_on_date: prev_basis_list = basis_list
#
#                             # ;; adjust the basis
#                             basis_list = self.basis_builder(basis_list, split_units, split_value_with_fees,
#                                                             basis_method, currency_frac)
#
#                             print("coming out of basis list ", self.dump_basis_list(basis_list))
#
#                             # ;; If it's a sale or the stock is worthless, calculate the gain
#                             if not split_value> 0:
#                                 # ;; Split value is zero or negative.  If it's zero it's either a stock split/merge
#                                 # ;; or the stock has become worthless (which looks like a merge where the number
#                                 # ;; of shares goes to zero).  If the value is negative then it's a disposal of some sort.
#                                 new_basis = self.sum_basis(basis_list, currency_frac)
#                                 if new_basis.is_zero() or split_value < 0:
#                                     # ;; Split value is negative or new basis is zero (stock is worthless),
#                                     # ;; Capital gain is money out minus change in basis
#                                     gain = split_value_with_fees.abs().sub(
#                                         orig_basis.sub(new_basis, currency_frac, NumericRound.ROUND), currency_frac,
#                                         NumericRound.ROUND)
#                                     print("Old basis=", orig_basis.to_string())
#                                     print(" New basis=", new_basis.to_string())
#                                     print(" Gain=", gain.to_string())
#                                     gaincoll.add(report_currency, gain)
#
#
#                         # ;; here is where we handle a spin-off transaction. This will be a no-units
#                         # ;; split with only one other split. xaccSplitget_OtherSplit only
#                         # ;; returns on a two-split transaction.  It's not a spinoff is the other split is
#                         # ;; in an income or expense account.
#                         elif self.is_spin_off(split, curacc):
#                             print("before spin-off basis list ", self.dump_basis_list(basis_list))
#                             basis_list = self.basis_builder(basis_list, split_units,
#                                                             my_exchange_fn(
#                                                                 Monetary(commod_currency,
#                                                                          split_value),
#                                                                 report_currency).amount,
#                                                             basis_method, currency_frac)
#                             print("after spin-off basis list ", self.dump_basis_list(basis_list))
#
#             # ;; Look for income and expense transactions that don't have a split in the
#             # ;; the account we're processing.  We do this as follow
#             # ;; 1. Make sure the parent account is a currency-valued asset or bank account
#             # ;; 2. If so go through all the splits in that account
#             # ;; 3. If a split is part of a two split transaction where the other split is
#             # ;;    to an income or expense account and the leaf name of that account is the
#             # ;;    same as the leaf name of the account we're processing, add it to the
#             # ;;    income or expense accumulator
#             # ;;
#             # ;; In other words with an account structure like
#             # ;;
#             # ;;   Assets (type ASSET)
#             # ;;     Broker (type ASSET)
#             # ;;       Widget Stock (type STOCK)
#             # ;;   Income (type INCOME)
#             # ;;     Dividends (type INCOME)
#             # ;;       Widget Stock (type INCOME)
#             # ;;
#             # ;; If you are producing a report on "Assets:Broker:Widget Stock" a
#             # ;; transaction that debits the Assets:Broker account and credits the
#             # ;; "Income:Dividends:Widget Stock" account will count as income in
#             # ;; the report even though it doesn't have a split in the account
#             # ;; being reported on.
#
#             #
#             parent_account = curacc.get_parent()
#             account_name = curacc.get_Name()
#
#             if parent_account is not None and \
#                     parent_account.get_type() in [AccountType.ASSET, AccountType.BANK] and \
#                     parent_account.get_commodity().is_currency():
#
#                 for split in parent_account.get_split_list():
#
#                     other_split = split.get_OtherSplit()
#                     # ;; This is safe because xaccSplitget_Account returns null for a null split
#                     other_acct = other_split.get_Account()
#                     parent = split.get_Parent()
#                     transaction_date = parent.RetDatePosted().date()
#
#                     if other_acct is not None and \
#                             transaction_date <= to_date_tp.date() and \
#                             other_acct.get_name() == account_name and \
#                             other_acct.get_commodity().is_currency():
#                         # ;; This is a two split transaction where the other split is to an
#                         # ;; account with the same name as the current account.  If it's an
#                         # ;; income or expense account accumulate the value of the transaction
#                         val = split.get_Value()
#                         curr = other_acct.get_commodity()
#                         if self.is_split_account_type(other_split, AccountType.INCOME):
#                             print("More income ", val.to_string())
#                             dividendcoll.add(curr, val)
#                         elif self.is_split_account_type(other_split, AccountType.EXPENSE):
#                             print("More expense ", val*-1.to_string())
#                             brokeragecoll.add(curr, val*-1)
#
#             print("units ", units.to_double())
#             print("pricing transaction is ", pricing_transaction)
#             print("use transaction is ", use_transaction)
#             print("prefer-pricelist is ", prefer_pricelist)
#             try:
#                 if use_transaction:
#                     print("price   transaction is ", pricing_transaction, price, price.to_currency_string())
#                 else:
#                     print("price       is ", pricing_transaction, price,
#                           Monetary(price.get_currency(),
#                                    price.get_value()).to_currency_string())
#             except Exception as errexc:
#
#                 print("junk")
#
#             #
#
#             print("basis we're using to build rows is ", self.sum_basis(basis_list, currency_frac).to_string())
#             print("but the actual basis list is ", self.dump_basis_list(basis_list))
#
#             if handle_brokerage_fees == 'include-in-gain':
#                 gaincoll.minusmerge(brokeragecoll, False)
#
#             # note we dont by default include a row for shares which we have sold completely previously
#             # so what happens to the gain - looks as though its ignored
#
#             # if curacc.get_Name().find('WFM') >= 0:
#
#             if include_empty or not units.is_zero() or is_sale_on_date:
#
#                 moneyin = moneyincoll.sum(report_currency, my_exchange_fn)
#                 moneyout = moneyoutcoll.sum(report_currency, my_exchange_fn)
#                 brokerage = brokeragecoll.sum(report_currency, my_exchange_fn)
#                 income = dividendcoll.sum(report_currency, my_exchange_fn)
#                 # ;; just so you know, gain == realized gain, ugain == un-realized gain, bothgain, well..
#                 gain = gaincoll.sum(report_currency, my_exchange_fn)
#                 # this fixup should handle total sale days (where units ends as 0) - with original code
#                 # we got both a realized gain and unrealised gain!! (because value is total sale)
#                 if units.is_zero() and is_sale_on_date:
#                     ugain = Monetary(report_currency, Decimal(0, 1))
#                 else:
#                     ugain = Monetary(report_currency,my_exchange_fn(value, report_currency).amount.sub(
#                         self.sum_basis(basis_list,
#                                        report_currency.get_fraction()),
#                         currency_frac, NumericRound.ROUND))
#                 bothgain = Monetary(report_currency,
#                                     gain.amount.add(ugain.amount, currency_frac,
#                                                     NumericRound.ROUND))
#                 totalreturn = Monetary(report_currency,
#                                        bothgain.amount.add(income.amount, currency_frac,
#                                                            NumericRound.ROUND))
#
#                 # the following does gnc:html-account-anchor
#                 accurl = gnc_html_utilities.account_anchor_text(curacc)
#
#                 # need to figure new way to do this
#                 # original created a list
#                 # not clear how to do this
#                 # could try makeing elements then adding them later?
#                 # still dont know if can change/add parent to ET elements
#                 # do I need to - essentially the ET document is creating the
#                 # equivalent of the activecols list
#
#                 new_col = self.document.doc.SubElement(new_row, "td")
#
#                 anchor_markup = self.document.doc.SubElement(new_col, "a")
#                 anchor_markup.attrib['href'] = accurl
#                 anchor_markup.text = curacc.get_name() + "\n"
#                 anchor_markup.tail = "\n"
#
#                 # activecols = [anchor_markup]
#
#                 # ;; If we're using the transaction, warn the user
#                 if use_transaction:
#                     if pricing_transaction:
#                     self.warn_price_dirty = True
#                 else:
#                     self.warn_no_price = True
#
#                 total_value.add(value.commodity, value.amount)
#                 total_moneyin.merge(moneyincoll)
#                 total_moneyout.merge(moneyoutcoll)
#                 total_brokerage.merge(brokeragecoll)
#                 total_income.merge(dividendcoll)
#                 total_gain.merge(gaincoll)
#                 total_ugain.add(ugain.commodity, ugain.amount)
#                 total_basis.add(report_currency, self.sum_basis(basis_list, currency_frac))
#
#                 # ;; build a list for the row  based on user selections
#
#                 if show_symbol:
#                     new_col = self.document.StyleSubElement(new_row, 'text-cell')
#                 new_col.text = ticker_symbol
#                 new_col.tail = "\n"
#                 # activecols.append(new_col)
#
#                 if show_listing:
#                     new_col = self.document.StyleSubElement(new_row, 'text-cell')
#                 new_col.text = listing
#                 new_col.tail = "\n"
#                 # activecols.append(new_col)
#                 if show_shares:
#                     new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = sprintamount(units, share_print_info)
#                 new_col.tail = "\n"
#                 # activecols.append(new_col)
#                 if show_price:
#                     new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.tail = "\n"
#
#                 #
#
#                 if use_transaction:
#                     if not isinstance(price, Monetary):
#                 if pricing_transaction:
#                 # this does gnc:html-transaction-anchor
#                     prcurl = transaction_anchor_text(pricing_transaction)
#                 prcstr = price.to_currency_string()
#                 else:
#                 prcurl = None
#                 prcstr = price.to_currency_string()
#                 else:
#                 # this does gnc:html-price-anchor
#                 prcurl = price_anchor_text(price)
#                 prcstr = Monetary(price.get_currency(),
#                 price.get_value()).to_currency_string()
#
#                 if prcurl != None:
#                     anchor_markup = self.document.doc.SubElement(new_col, "a")
#                 anchor_markup.attrib['href'] = prcurl
#                 anchor_markup.text = prcstr
#                 anchor_markup.tail = "\n"
#                 else:
#                 # this appears to be what code says
#                 new_col.text = prcstr
#
#                 # activecols.append(new_col)
#
#                 # now add lots more
#                 if use_transaction:
#                     if pricing_transaction:
#                         txtstr = "*"
#                     else:
#                         txtstr = "**"
#                 else:
#                     txtstr = " "
#
#                 new_col = self.document.StyleSubElement(new_row, 'text-cell')
#                 new_col.text = txtstr
#                 new_col.tail = "\n"
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 # so with this for a partial sale the basis shown is the basis after the sale
#                 if units.is_zero() and is_sale_on_date:
#                     new_col.text = Monetary(report_currency, self.sum_basis(prev_basis_list,
#                                                                             currency_frac)).to_currency_string()
#                 else:
#                     new_col.text = Monetary(report_currency, self.sum_basis(basis_list,
#                                                                             currency_frac)).to_currency_string()
#                 new_col.tail = "\n"
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = value.to_currency_string()
#                 new_col.tail = "\n"
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = moneyin.to_currency_string()
#                 new_col.tail = "\n"
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = moneyout.to_currency_string()
#                 new_col.tail = "\n"
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = gain.to_currency_string()
#                 new_col.tail = "\n"
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = ugain.to_currency_string()
#                 new_col.tail = "\n"
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = bothgain.to_currency_string()
#                 new_col.tail = "\n"
#
#                 moneyinvalue = moneyin.amount.to_double()
#                 bothgainvalue = bothgain.amount.to_double()
#                 if moneyinvalue == 0.0:
#                     prcntstr = ""
#                 else:
#                     prcntstr = "%.2f%%" % ((bothgainvalue / moneyinvalue) * 100)
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = prcntstr
#                 new_col.tail = "\n"
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = income.to_currency_string()
#                 new_col.tail = "\n"
#
#                 if handle_brokerage_fees != 'ignore-brokerage':
#                     new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = brokerage.to_currency_string()
#                 new_col.tail = "\n"
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = totalreturn.to_currency_string()
#                 new_col.tail = "\n"
#
#                 moneyinvalue = moneyin.amount.to_double()
#                 totalreturnvalue = totalreturn.amount.to_double()
#                 if moneyinvalue == 0.0:
#                     prcntstr = ""
#                 else:
#                     prcntstr = "%.2f%%" % ((totalreturnvalue / moneyinvalue) * 100)
#
#                 new_col = self.document.StyleSubElement(new_row, 'number-cell')
#                 new_col.text = prcntstr
#                 new_col.tail = "\n"
#
#                 #
#
#                 #
#                 # well scheme clearly returns total-value - because not passed??
#                 # return total_value
#         return
#
#
#     def price_db_latest_fn(self, foreign, domestic, date):
#
#         #
#
#         find_price = self.pricedb.lookup_latest_any_currency(foreign)
#
#         return find_price
#
#     def price_db_nearest_fn(self, foreign, domestic, date):
#
#         #
#
#         # find_price = self.pricedb.lookup_nearest_in_time_any_currency(foreign,timespecCanonicalDayTime(date))
#         find_price = self.pricedb.lookup_nearest_in_time_any_currency_t64(foreign, date)
#
#         return find_price
#
#     def render(self, report):
#         report_starting(self.name)
#         print("Starting report", self.name)
#
#         starttime = datetime.datetime.now()
#         optobj = self.options.lookup_option('General', 'Date')
#         to_date_tp = optobj.get_option_value()
#
#         optobj = self.options.lookup_option('General','Start Date')
#         from_date_tp = optobj.get_option_value()
#         optobj = self.options.lookup_option('General','End Date')
#         to_date_tp = optobj.get_option_value()
#
#         from_date_tp = None
#
#         accobj = self.options.lookup_option('Accounts', 'Accounts')
#         accounts = accobj.get_option_value()
#
#         optobj = self.options.lookup_option('General', "Report's currency")
#         report_currency = optobj.get_value()
#
#         optobj = self.options.lookup_option('General', 'Price Source')
#         price_source = optobj.get_option_value()
#
#         rptopt = self.options.lookup_option('General', 'Report name')
#         rptttl = rptopt.get_value()
#
#         optobj = self.options.lookup_option('Accounts', 'Include accounts with no shares')
#         include_empty = optobj.get_value()
#
#         optobj = self.options.lookup_option('Display', 'Show ticker symbols')
#         show_symbol = optobj.get_value()
#
#         optobj = self.options.lookup_option('Display', 'Show listings')
#         show_listing = optobj.get_value()
#
#         optobj = self.options.lookup_option('Display', 'Show number of shares')
#         show_shares = optobj.get_value()
#
#         optobj = self.options.lookup_option('Display', 'Show prices')
#         show_price = optobj.get_value()
#
#         optobj = self.options.lookup_option('General', 'Basis calculation method')
#         basis_method = optobj.get_value()
#
#         optobj = self.options.lookup_option('General', 'Set preference for price list data')
#         prefer_pricelist = optobj.get_value()
#
#         optobj = self.options.lookup_option('General', 'How to report brokerage fees')
#         handle_brokerage_fees = optobj.get_value()
#
#         optobj = self.options.lookup_option('General','Show Full Account Names')
#         show_full_names = optobj.get_value()
#
#         optobj = self.options.lookup_option('Accounts','Account Display Depth')
#         display_depth = optobj.get_option_value()
#
#         shwobj = self.options.lookup_option('Accounts','Always show sub-accounts')
#         shwopt = shwobj.get_value()
#         if shwopt:
#            subacclst = accounts.get_descendants_sorted()
#            for subacc in subacclst:
#                if not self.account_in_list(subacc,accounts):
#                    accounts.append(subacc)
#
#         total_basis = CommodityCollector()
#         total_value = CommodityCollector()
#         total_moneyin = CommodityCollector()
#         total_moneyout = CommodityCollector()
#         total_income = CommodityCollector()
#         total_gain = CommodityCollector()  # ;; realized gain
#         total_ugain = CommodityCollector()  # ;; unrealized gain
#         total_brokerage = CommodityCollector()
#
#         # now for html creation
#         # where do we actually instantiate the Html object
#
#         # in scheme created new scheme html doc
#         # in python probably should pass the report xml document
#         # does the possibility of having new HtmlDocument make sense??
#         self.document = HtmlDocument()
#         self.document.set_stylesheet(report.get_stylesheet())
#         # temporary measure - set the document style
#         # some this should be done auto by the stylesheet set
#         # but the StyleTable is not currently stored in the stylesheet
#         # or at least not the right one
#         self.document.style = report.style
#
#         # self.document.title = rptttl + " - " + "%s to %s"%(gnc_print_date(from_date_tp),gnc_print_date(to_date_tp)))
#         self.document.title = rptttl + " " + "%s" % print_datetime(to_date_tp)
#
#         self.warn_price_dirty = False
#         self.warn_no_price = False
#
#         # Scheme uses a whole subclass of functions for dealing with
#         # tables - indirectly creating all table elements
#         # ignoring all this for now - just using CSS styles
#         # self.table = HtmlTable()
#         self.new_table = self.document.StyleElement('table')
#         self.new_table.text = "\n"
#
#         if len(accounts) > 0:
#
#         # ; at least 1 account selected
#
#         book = Session.get_current_book()
#
#         self.exchange_fn = case_exchange_fn(price_source, report_currency, to_date_tp)
#         self.pricedb = book.get_price_db()
#
#         if price_source == 'pricedb-latest':
#             price_fn = self.price_db_latest_fn
#         elif price_source == 'pricedb-nearest':
#             price_fn = self.price_db_nearest_fn
#
#         # have to deal with these laters
#         headercols = ["Account"]
#
#         # this has markup in the scheme
#         # we will start - but have to remember to add markup later
#         # we could plausibly use ET appending to add an ET list to the document later
#         # actually we can because thats hows its programmed in gnc_html_document
#         # - problem is we need to update interface to create isolated elements
#         totalscols = ["Total"]
#         # totalscols = [self.document.StyleElementNew('total-label-cell',text="Total"),tail="\n")]
#
#         sum_total_moneyin = Decimal(0, 1)
#         sum_total_income = Decimal(0, 1)
#         sum_total_both_gains = Decimal(0, 1)
#         sum_total_gain = Decimal(0, 1)
#         sum_total_ugain = Decimal(0, 1)
#         sum_total_brokerage = Decimal(0, 1)
#         sum_total_totalreturn = Decimal(0, 1)
#
#         # ;;begin building lists for which columns to display
#         if show_symbol:
#             headercols.append("Symbol")
#         totalscols.append(" ")
#
#         if show_listing:
#             headercols.append("Listing")
#         totalscols.append(" ")
#
#         if show_shares:
#             headercols.append("Shares")
#         totalscols.append(" ")
#
#         if show_price:
#             headercols.append("Price")
#         totalscols.append(" ")
#
#         headercols.extend([" ",
#                            "Basis",
#                            "Value",
#                            "Money In",
#                            "Money Out",
#                            "Realized Gain",
#                            "Unrealized Gain",
#                            "Total Gain",
#                            "Rate of Gain",
#                            "Income",
#                            ])
#
#         if handle_brokerage_fees != 'ignore-brokerage':
#             headercols.append("Brokerage Fees")
#
#         headercols.append("Total Return")
#         headercols.append("Rate of Return")
#
#         totalscols.append(" ")
#
#         new_row = self.document.doc.SubElement(self.new_table, "tr")
#         new_row.tail = "\n"
#
#         # this is essentially doing gnc:html-table-set-col-headers
#         for colhdr in headercols:
#             new_hdr = self.document.doc.SubElement(new_row, "th", attrib={'rowspan': "1", 'colspan': "1"})
#             new_hdr.text = colhdr
#             new_hdr.tail = "\n"
#
#         #
#
#         self.table_add_stock_rows(accounts, to_date_tp, from_date_tp, report_currency, self.exchange_fn, price_fn,
#     #     price_source, include_empty, show_symbol, show_listing, show_shares, show_price,
#     #     basis_method,
#     #     prefer_pricelist, handle_brokerage_fees,
#     #     total_basis, total_value, total_moneyin, total_moneyout, total_income, total_gain,
#     #     total_ugain, total_brokerage)
#     #     sum_total_moneyin = total_moneyin.sum(report_currency, self.exchange_fn)
#     #     sum_total_income = total_income.sum(report_currency, self.exchange_fn)
#     #     sum_total_gain = total_gain.sum(report_currency, self.exchange_fn)
#     #     sum_total_ugain = total_ugain.sum(report_currency, self.exchange_fn)
#     #     sum_total_both_gains = Monetary(report_currency,
#     #     sum_total_gain.amount.add(sum_total_ugain.amount,
#     #                               report_currency.get_fraction(),
#     #                               NumericRound.ROUND))
#     #     sum_total_brokerage = total_brokerage.sum(report_currency, self.exchange_fn)
#     #     sum_total_totalreturn = Monetary(report_currency,
#     #     sum_total_both_gains.amount.add(
#     #         sum_total_income.amount,
#     #         report_currency.get_fraction(),
#     #         NumericRound.ROUND))
#     #
#     #     # (gnc:make-html-table-cell/size 1 17 (gnc:make-html-text (gnc:html-markup-hr)))
#     #     # this is labelled grand-total for some reason - just draws a line
#     #     new_row = self.document.doc.SubElement(self.new_table, "tr")
#     #     new_row.tail = "\n"
#     #     new_data = self.document.doc.SubElement(new_row, "td", attrib={'rowspan': "1", 'colspan': "17"})
#     #     new_ruler = self.document.doc.SubElement(new_data, "hr")
#     #
#     #     #
#     #
#     #     # do the totals row explicitly
#     #
#     #     new_row = self.document.doc.SubElement(self.new_table, "tr")
#     #     new_row.text = "\n"
#     #     new_row.tail = "\n"
#     #
#     #     new_col = self.document.StyleSubElement(new_row, 'total-label-cell')
#     #     new_col.text = "Total")
#     #
#     #     for itm in totalscols[1:]:
#     #         new_col = self.document.StyleSubElement(new_row, 'total-label-cell')
#     #         new_col.text = itm
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = total_basis.sum(report_currency, self.exchange_fn).to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = total_value.sum(report_currency, self.exchange_fn).to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = sum_total_moneyin.to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = total_moneyout.sum(report_currency, self.exchange_fn).to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = sum_total_gain.to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = sum_total_ugain.to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = sum_total_both_gains.to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #         totalinvalue = sum_total_moneyin.amount.to_double()
#     #         totalgainvalue = sum_total_both_gains.amount.to_double()
#     #         if totalinvalue == 0.0:
#     #             prcntstr = ""
#     #         else:
#     #             prcntstr = "%.2f%%" % ((totalgainvalue / totalinvalue) * 100)
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = prcntstr
#     #         new_col.tail = "\n"
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = sum_total_income.to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #     if handle_brokerage_fees != 'ignore-brokerage':
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = sum_total_brokerage.to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = sum_total_totalreturn.to_currency_string()
#     #         new_col.tail = "\n"
#     #
#     #         totalinvalue = sum_total_moneyin.amount.to_double()
#     #         totalreturnvalue = sum_total_totalreturn.amount.to_double()
#     #         if totalinvalue == 0.0:
#     #             prcntstr = ""
#     #         else:
#     #             prcntstr = "%.2f%%" % ((totalreturnvalue / totalinvalue) * 100)
#     #
#     #         new_col = self.document.StyleSubElement(new_row, 'total-number-cell')
#     #         new_col.text = prcntstr
#     #         new_col.tail = "\n"
#     #
#     #     if self.warn_price_dirty:
#     #         new_text = self.document.doc.Element("p")
#     #         new_text.text =
#     #         "* this commodity data was built using transaction pricing instead of the price list."
#     #         new_text.tail = "\n"
#     #
#     #         new_text = self.document.doc.Element("br")
#     #         new_text.text = ""
#     #         new_text.tail = "\n"
#     #
#     #         new_text = self.document.doc.Element("p")
#     #         new_text.text = "If you are in a multi-currency situation, the exchanges may not be correct."
#     #         new_text.tail = "\n"
#     #
#     #     if self.warn_no_price:
#     #         new_text = self.document.doc.Element("br")
#     #         new_text.text = None
#     #         new_text.tail = ""
#     #
#     #         new_text = self.document.doc.Element("p")
#     #         new_text.text = "** this commodity has no price and a price of 1 has been used."
#     #         new_text.tail = "\n"
#     #
#     #     else:
#     #         self.make_no_account_warning(rpttl, self.report_guid)
#     #
#     #     report_finished()
#     #
#     #     endtime = datetime.datetime.now()
#     #
#     #     print("time taken", endtime - starttime)
#     #
#     #     return self.document
#     #
#     # def make_no_account_warning(self, report_title_string, report_id):
#     #
#     #     self.make_generic_warning(report_title_string, report_id,
#     #                               "No accounts selected",
#     #                               "This report requires accounts to be selected in the report options.")
#     #
#     # def make_generic_warning(self, report_title_string, report_id, warning_title_string, warning_string):
#     #
#     #     new_elm = self.document.doc.Element("h2")
#     #     new_elm.text = report_title_string + ":"
#     #     new_elm.tail = "\n"
#     #
#     #     new_elm = self.document.doc.Element("h2")
#     #     new_elm.text = warning_title_string
#     #     new_elm.tail = "\n"
#     #
#     #     new_elm = self.document.doc.Element("p")
#     #     new_elm.text = warning_string
#     #     new_elm.tail = "\n"
#     #
#     #     # this is makes a link
#     #     # the url not correctly implemented yet
#     #     new_elm = self.document.doc.SubElement("a")
#     #     new_elm.attrib['href'] = "report-id=%s" % report_id
#     #     new_elm.text = "Edit report options"
#     #     new_elm.tail = "\n"
