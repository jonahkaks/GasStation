from gasstation.utilities.options import OptionUtils
from gasstation.utilities.ui import *
from libgasstation.core.bill import *
from libgasstation.core.helpers import *
from libgasstation.core.invoice import *
from ..report import *
from ..utilities import *

optname_from_date = "From"
optname_to_date = "To"
optname_date_driver = "Due or Post Date"

person_page = pagename_general
date_header = "Date"
due_date_header = "Due Date"
reference_header = "Reference"
type_header = "Type"
desc_header = "Description"
sale_header = "Sale"
tax_header = "Tax"
credit_header = "Credits"
debit_header = "Debits"
balance_header = "Balance"
doclink_header = "Document Links"
linked_transactions_header = "Transaction Links"

employee_report_guid = "08ae9c2e884b4f9787144f47eacd7f44_old"
vendor_report_guid = "d7d1e53505ee4b1b82efad9eacedaea0_old"
customer_report_guid = "c146317be32e4948a561ec7fc89d15c1_old"

acct_string = "Account"
javascript = """
    <script>
      function getID(cell) { return cell.getAttribute('link-id'); }
    
      function clicky() {
          var id = getID(this);
          var ishighlighted = this.classList.contains('highlight');
          TDs.forEach (function (item, idx) {
              item.classList.remove('highlight')});
          if (ishighlighted) return;
          TDs.forEach (function (item, idx) {
              if (getID(item) == id)
                  item.classList.add('highlight')})}
    
      var TDs = document.getElementsByTagName('td');
      TDs = Array.prototype.slice.call (TDs);
      TDs = TDs.filter (getID);
      TDs.forEach(function (item, idx) {
          item.addEventListener('click', clicky)});
    </script>
"""


def date_col(columns_used):
    return columns_used[0]


def date_due_col(columns_used):
    return columns_used[1]


def num_col(columns_used):
    return columns_used[2]


def type_col(columns_used):
    return columns_used[3]


def memo_col(columns_used):
    return columns_used[4]


def sale_col(columns_used):
    return columns_used[5]


def tax_col(columns_used):
    return columns_used[6]


def debit_col(columns_used):
    return columns_used[7]


def credit_col(columns_used):
    return columns_used[8]


def balance_col(columns_used):
    return columns_used[9]


columns_used_size = 10


class PersonReport(ReportTemplate):
    def export(self, report_obj, export_types, filename):
        pass

    def __init__(self, name, report_guid, account_types, person_type, reverse):
        super().__init__(version=1, name=name, menu_path=menu_name_customer_supplier,
                         guid=report_guid, in_menu=gs_pref_is_extra_enabled())
        self.account_types = account_types
        self.person_type = person_type
        self.obj = Customer if person_type == "Customer" else Supplier if person_type == "Supplier" else None
        self.reverse = reverse

    def person_string(self):
        return self.person_type

    def invalid_selection_string(self):
        if self.person_type == "Customer":
            return "This report requires a customer to be selected."
        elif self.person_type == "Staff":
            return "This report requires a employee to be selected."
        else:
            return "This report requires a company to be selected."

    def invalid_selection_title_string(self):
        if self.person_type == "Customer":
            return "No valid customer selected."
        elif self.person_type == "Staff":
            return "No valid staff member selected."
        else:
            return "No valid company selected."

    def make_no_person_warning(self, report_title_string, report_id):
        return make_generic_warning(report_title_string, report_id, self.invalid_selection_title_string(),
                                    self.invalid_selection_string())

    @staticmethod
    def make_no_valid_account_warning(report_title_string, report_id):
        return make_generic_warning(report_title_string, report_id, "No valid account selected",
                                    "This report requires a valid account to be selected.")

    @staticmethod
    def build_column_used(options):
        count = 0
        used = []
        for a in map(lambda name: options.lookup_option("Display Columns", name).get_value(),
                     [date_header, due_date_header, reference_header, type_header, desc_header, sale_header,
                      tax_header, debit_header, credit_header, balance_header]):
            if a:
                count += 1
                used.append(count)
            else:
                used.append(0)
        return used

    def make_heading_list(self, column_vector):
        hlist = []
        formal = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_ACCOUNTING_LABELS)
        if date_col(column_vector):
            hlist.append(TH(date_header))
        if date_due_col(column_vector):
            hlist.append(due_date_header)
        if num_col(column_vector):
            hlist.append(reference_header)
        if type_col(column_vector):
            hlist.append(type_header)
        if memo_col(column_vector):
            hlist.append(desc_header)
        if sale_col(column_vector):
            hlist.append(sale_header)
        if tax_col(column_vector):
            hlist.append(tax_header)
        if credit_col(column_vector):
            hlist.append(Account.get_credit_string(self.account_types[0], formal))
        if debit_col(column_vector):
            hlist.append(Account.get_debit_string(self.account_types[0], formal))

        if balance_col(column_vector):
            hlist.append(balance_header)
        return hlist

    @staticmethod
    def make_row(column_vector, date, due_date, num, type_str, memo, monetary, credit, debit, sale, tax):
        row_contents = []
        if date_col(column_vector):
            row_contents.append(TD(print_datetime(date) if date is not None else "", _class="date-cell"))
        if date_due_col(column_vector):
            row_contents.append(TD(print_datetime(due_date) if due_date is not None else "", _class="date-cell"))
        if num_col(column_vector):
            row_contents.append(TD(str(num) if num is not None else ""))
        if type_col(column_vector):
            row_contents.append(TD(type_str))
        if memo_col(column_vector):
            row_contents.append(TD(memo))
        if sale_col(column_vector):
            row_contents.append(TD(sale, _class="number-cell"))
        if tax_col(column_vector):
            row_contents.append(TD(tax, _class="number-cell"))
        if credit_col(column_vector):
            row_contents.append(TD(credit, _class="number-cell"))
        if debit_col(column_vector):
            row_contents.append(TD(debit, _class="number-cell"))
        if balance_col(column_vector):
            row_contents.append(TD(monetary, _class="number-cell"))
        return row_contents

    def add_balance_row(self, table, column_vector, transaction, odd_row, printed, start_date, total):
        if not printed:
            printed = True
            if balance_col(column_vector) and not total.is_zero():
                row = self.make_row(column_vector, start_date, False, "", "Balance", "",
                                    sprintamount(total, PrintAmountInfo.commodity(transaction.get_currency(), True)),
                                    "", "", "", "")
                row_style = "normal-row" if odd_row else "alternate-row"
                table.append(TR(row, _class=row_style))
        return printed

    def add_transaction_row(self, table: TABLE, transaction: Transaction, acc: Account,
                            column_vector: List, odd_row: bool, printed: bool, reverse, start_date: datetime.date,
                            total: Decimal):
        _type = transaction.get_type()
        date = transaction.get_post_date()
        due_date = None
        value = transaction.get_account_value(acc)
        sale = Decimal(0)
        tax = Decimal(0)
        split = transaction.get_split(0)
        invoice = Invoice.from_transaction(
            transaction) if self.person_type == "Customer" else Bill.from_transaction(
            transaction)
        currency = transaction.get_currency()
        type_str = "UnKnown"
        if _type == TransactionType.INVOICE:
            if invoice is not None:
                type_str = Anchors.invoice(invoice,
                                           "Invoice" if self.person_type == "Customer" else "Bill" if self.person_type == "Supplier" else "Voucher")
        elif _type == TransactionType.PAYMENT:
            type_str = Anchors.split(split, "Payment")
        if reverse:
            value *= -1

        if start_date <= date:
            printed = self.add_balance_row(table, column_vector, transaction, odd_row, printed, start_date, total)
            if invoice is not None:
                due_date = invoice.get_date_due()
                sale = invoice.get_subtotal()
                tax = invoice.get_total_tax()

                if (self.person_type == "Customer" and invoice.get_is_credit_note()) or (
                        self.person_type == "Supplier" and invoice.get_is_debit_note()):
                    tax *= -1
                    sale *= -1
            info = PrintAmountInfo.commodity(currency, True)
            row = self.make_row(column_vector, date, due_date, gs_get_num_action(transaction, split),
                                type_str, split.get_memo() or "",
                                sprintamount(value + total, info),
                                sprintamount(value, info) if value >= 0 else "",
                                sprintamount(value, info) if value < 0 else "",
                                sprintamount(sale, info) if invoice is not None else "",
                                sprintamount(tax, info) if invoice is not None else "",
                                )
            table.append(TR(row, _class="normal-row" if odd_row else "alternate-row"))
            odd_row = not odd_row
        return printed, value, odd_row, sale, tax

    @staticmethod
    def make_myname_table(book: Book, date_format):
        name = company_info(book, company_name)
        addy = company_info(book, company_addy)
        return DIV(name or "", *multiline_to_html_text(addy) if addy is not None else "",
                   P(datetime.date.today().strftime(date_format)), _style="float:right")

    def get_person_transactions(self, person, end_date, date_type):
        txns = []
        opening_transaction = person.get_opening_balance_transaction()
        if opening_transaction is not None:
            txns.append(opening_transaction)
        if isinstance(person, Customer):
            txns.extend(map(lambda inv: inv.get_posted_txn(), person.invoices))
        elif isinstance(person, Supplier):
            txns.extend(map(lambda bill: bill.get_posted_txn(), person.bills))
        elif isinstance(person, Staff):
            txns.extend(map(lambda inv: inv.get_posted_txn(), person.payslips))
        txns.extend(
                map(lambda customer_payment: customer_payment.payment.get_posted_transaction(), person.payments))
        return txns

    def make_transaction_table(self, options, acc, start_date, end_date, date_type, person):
        transactions = self.get_person_transactions(person, end_date, date_type)
        used_columns = self.build_column_used(options)
        total = Decimal(0)
        debit = Decimal(0)
        credit = Decimal(0)
        tax = Decimal(0)
        sale = Decimal(0)
        currency = acc.get_commodity()
        table = TABLE()
        reverse = self.reverse

        headings = self.make_heading_list(used_columns)
        table.append(THEAD(TR(*headings)))
        transactions.sort()
        printed = False
        odd_row = True
        for transaction in transactions:
            _type = transaction.get_type()
            if _type == TransactionType.INVOICE or _type == TransactionType.PAYMENT:
                printed, value, odd_row, fsale, ftax = self.add_transaction_row(table, transaction, acc, used_columns,
                                                                                odd_row, printed,
                                                                                reverse, start_date, total)
                if printed and total != 0:
                    sale += fsale
                    tax += ftax
                if value < 0:
                    debit += value
                else:
                    credit += value
                total += value
        if len(transactions):
            self.add_balance_row(table, used_columns, transactions[0], odd_row, printed, start_date, total)
        info = PrintAmountInfo.commodity(currency, True)
        if sale_col(used_columns) or tax_col(used_columns) or credit_col(used_columns) or debit_col(used_columns):
            row_contents = []
            prespan = 0
            if balance_col(used_columns):
                row_contents.append(TD(sprintamount(total, info), _class="total-number-cell"))
            if debit_col(used_columns):
                row_contents.append(TD(sprintamount(debit, info), _class="total-number-cell"))
            if credit_col(used_columns):
                row_contents.append(TD(sprintamount(credit, info), _class="total-number-cell"))
            if tax_col(used_columns):
                row_contents.append(TD(sprintamount(tax, info), _class="total-number-cell"))
            if sale_col(used_columns):
                row_contents.append(TD(sprintamount(sale, info), _class="total-number-cell"))
            if memo_col(used_columns):
                prespan += 1
            if type_col(used_columns):
                prespan += 1
            if num_col(used_columns):
                prespan += 1
            if date_due_col(used_columns):
                prespan += 1
            if date_col(used_columns):
                prespan += 1
            if prespan >= 2:
                row_contents.append(TD(_colspan=prespan - 1, _rowspan=1))
            row_contents.reverse()
            row_contents.insert(0, TD("Period Totals", _class="total-label-cell"))
            table.append(TR(*row_contents, _class="grand-total"))
        if balance_col(used_columns):
            table.append(TR(TD("Total Credit" if total < 0 else "Total Due", _class="total-label-cell"),
                            TD(sprintamount(total, info), _rowspan=1, _colspan=balance_col(used_columns),
                               _class="total-number-cell"),
                            _class="grand-total"))
        return table

    def generate_options(self):
        super().generate_options()
        reg = self.options.register_option
        reg(ReferenceOption(person_page, self.person_string(), "v", "The company for this report.", None, self.obj))
        reg(AccountSelLimitedOption(person_page, acct_string, "w", "The account to search for transactions.",
                                    self.account_types))
        OptionUtils.add_date_interval(self.options, pagename_general, optname_from_date, optname_to_date, "a")
        self.options.lookup_option(pagename_general, optname_to_date).set_default(["relative", "today"])
        reg(SimpleBooleanOption("Display Columns", date_header, "b", "Display the transaction date?", True))
        reg(SimpleBooleanOption("Display Columns", due_date_header, "c", "Display the transaction date?", True))
        reg(SimpleBooleanOption("Display Columns", reference_header, "d", "Display the transaction reference?", True))
        reg(SimpleBooleanOption("Display Columns", type_header, "g", "Display the transaction type?", True))
        reg(SimpleBooleanOption("Display Columns", desc_header, "ha", "Display the transaction description?", True))
        reg(SimpleBooleanOption("Display Columns", sale_header, "haa", "Display the sale amount column?", False))
        reg(SimpleBooleanOption("Display Columns", tax_header, "hab", "Display the tax column?", False))
        reg(SimpleBooleanOption("Display Columns", credit_header, "hac", "Display the period credits column?", True))
        reg(SimpleBooleanOption("Display Columns", debit_header, "had", "Display the period debits column?", True))
        reg(SimpleBooleanOption("Display Columns", balance_header, "hb", "Display the running balance?", True))
        reg(MultiChoiceOption(pagename_general, optname_date_driver, "k", "Leading date.", "duedate",
                              [("duedate", "Due Date", "Due date is leading."),
                               ("postdate", "Post Date", "Post date is leading.")]))
        self.options.set_default_section("General")

    @staticmethod
    def setup_query(q: Query, person, account: Account, end_date: datetime.date):
        guid = person.get_guid()
        if isinstance(person, Customer):
            QueryPrivate.add_guid_match(q, [SPLIT_TRANS, INVOICE_FROM_TXN, INVOICE_CUSTOMER, PARAM_GUID], guid,
                                        QueryOp.OR)
        elif isinstance(person, Supplier):
            QueryPrivate.add_guid_match(q, [SPLIT_TRANS, BILL_FROM_TXN, BILL_SUPPLIER, PARAM_GUID], guid, QueryOp.OR)
        q.add_single_account_match(account, QueryOp.AND)
        q.add_date_match_tt(False, end_date, True, end_date, QueryOp.AND)
        q.set_book(Session.get_current_book())

    def render(self, report_obj):
        opt_val = lambda section, name: self.options.lookup_option(section, name).get_value()
        document = HtmlDocument()
        account = opt_val(person_page, acct_string)
        start_date = DateOption.absolute_time(opt_val(pagename_general, optname_from_date))
        end_date = DateOption.absolute_time(opt_val(pagename_general, optname_to_date))
        book = Session.get_current_book()
        date_format = fancy_date(book)
        person_descr = self.person_string()
        date_type = opt_val(pagename_general, optname_date_driver)
        person = opt_val(person_page, person_descr)

        report_title = person_descr + " " + "Report"
        if person is None:
            document.add_object(self.make_no_person_warning(report_title, report_obj.get_id()))
        else:
            report_title = report_title + ": " + person.contact.get_display_name()
            if account is None:
                document.add_object(self.make_no_valid_account_warning(report_title, report_obj.get_id()))
            else:

                document.set_title(report_title)
                document.set_headline(SPAN(person_descr + " Report ", Anchors.person(person)))
                table = self.make_transaction_table(report_obj.options, account, start_date, end_date, date_type,
                                                    person)
                table.set_style("table", {"border": 1, "cellspacing": 0, "cellpadding": 4})
                document.add_object(self.make_myname_table(book, date_format))
                document.add_object(BR())
                document.add_object(DIV(*multiline_to_html_text(person.get_name_and_address()), _style="float:left"))
                document.add_object(P("Date Range:{} - {}".format(print_datetime(start_date), print_datetime(end_date)),
                                      _style="clear:both;font-weight:bold;"))
                document.add_object(table)
        return document

    @classmethod
    def new(cls, person, account):
        return cls.new_with_enddate(person, account, None)

    @classmethod
    def new_with_enddate(cls, person, account, enddate):
        if isinstance(person, Customer):
            guid = customer_report_guid
        elif isinstance(person, Supplier):
            guid = vendor_report_guid
        elif isinstance(person, Staff):
            guid = employee_report_guid
        else:
            raise TypeError("Person is not Valid")
        template = ReportTemplate.find(guid)
        template.generate_options()
        options = template.options
        person_op = options.lookup_option(person_page, template.person_string())
        account_op = options.lookup_option(person_page, acct_string)
        account_op.set_value(account)
        date_op = options.lookup_option(pagename_general, optname_to_date)
        person_op.set_value(person)
        if enddate is not None:
            date_op.set_value(["absolute", enddate])
        return Report(template).id


PersonReport(name="Customer Report", report_guid=customer_report_guid, account_types=[AccountType.RECEIVABLE],
             person_type="Customer", reverse=False)
PersonReport(name="Supplier Report", report_guid=vendor_report_guid, account_types=[AccountType.PAYABLE],
             person_type="Supplier", reverse=True)
PersonReport(name="Staff Report", report_guid=employee_report_guid, account_types=[AccountType.PAYABLE],
             person_type="Staff", reverse=True)
