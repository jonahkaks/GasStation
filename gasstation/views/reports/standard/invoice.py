from gasstation.utilities.ui import *
from libgasstation.core.entry import *
from ..commodity_utilities import *
from ..report import *

base_css = """.div-align-right { float: right; }
.div-align-right .maybe-align-right { text-align: right }
                                        .entries-table * { border-width: 1px; border-style:solid; border-collapse: collapse}
.entries-table > table { width: 100% }
                           .company-table > table * { padding: 0px; }
.client-table > table * { padding: 0px; }
.invoice-details-table > table * { padding: 0px; }
@media print { .main-table > table { width: 100%; }}
"""

invoice_report_guid = "5123a759ceb9483abf2182d01c140e8d"
easy_invoice_guid = "67112f318bef4fc496bdc27d106bbda4"
fancy_invoice_guid = "3ce293441e894423a2425d7a22dd1ac6"


def date_col(columns_used):
    return columns_used[0]


def description_col(columns_used):
    return columns_used[1]


def action_col(columns_used):
    return columns_used[2]


def quantity_col(columns_used):
    return columns_used[3]


def price_col(columns_used):
    return columns_used[4]


def discount_col(columns_used):
    return columns_used[5]


def tax_col(columns_used):
    return columns_used[6]


def taxvalue_col(columns_used):
    return columns_used[7]


def value_col(columns_used):
    return columns_used[8]


def num_columns_required(columns_used):
    return len(list(filter(lambda a: a, columns_used)))


variant_list = {'invoice': {"1a": "none", "1b": "invoice", "2a": "client", "2b": "company",
                            "3a": "none", "3b": "today", "css": base_css},
                "easy-invoice": {"1a": "none", "1b": "invoice", "2a": "client", "2b": "company", "3a": "none",
                                 "3b": "today",
                                 "css": base_css + """.invoice-in-progress { color:red }
                               .invoice-title { font-weight: bold; text-decoration: underline }
                                .main-table > table { margin: auto }
                                .invoice-details-table > table { display: block; }
                                .invoice-notes { margin-top: 20px }
                                .entries-table > table { min-width: 600px }"""},
                "fancy-invoice": {"1a": "company", "1b": "invoice", "2a": "client",
                                  "2b": "company", "3a": "none", "3b": "none",
                                  "css": base_css + """
                                .company-name {font-size: x-large; }
                                .client-name {font-size: x-large; }"""}}

layout_key_list = {"client": "Client or vendor name, address and ID",
                   "company": "Company name, address and tax-ID",
                   "invoice": "Invoice date, due date, billing ID, terms, job details",
                   "today": "Today's date",
                   "picture": "Picture",
                   "none": "Empty space"}


class InvoiceReport(ReportTemplate):
    def export(self, report_obj, export_types, filename):
        pass

    def __init__(self, name, report_id, menu_path, variant):
        super().__init__(version=1, name=name, guid=report_id, menu_path=menu_path, in_menu=True)
        self.variant = variant

    @staticmethod
    def build_column_used(options):
        def opt_val(section, name):
            option = options.lookup_option(section, name)
            if option is not None:
                return option.get_value()

        return [opt_val("Display Columns", "Date"), opt_val("Display Columns", "Description"),
                opt_val("Display Columns", "Action"), opt_val("Display Columns", "Quantity"),
                opt_val("Display Columns", "Price"),
                opt_val("Display Columns", "Discount"), opt_val("Display Columns", "Taxable"),
                opt_val("Display Columns", "Tax Amount"),
                opt_val("Display Columns", "Total")]

    @staticmethod
    def make_heading_list(column_vector):
        a = []
        if date_col(column_vector):
            a.append("Date")
        if description_col(column_vector):
            a.append("Description")
        if action_col(column_vector):
            a.append("Action")
        if quantity_col(column_vector):
            a.append("Quantity")
        if price_col(column_vector):
            a.append("Unit Price")
        if discount_col(column_vector):
            a.append("Discount")
        if tax_col(column_vector):
            a.append("Taxable")
        if taxvalue_col(column_vector):
            a.append("Tax Amount")
        if value_col(column_vector):
            a.append("Total")
        return a

    def generate_options(self):
        super().generate_options()
        reg = self.options.register_option

        reg(InvoiceOption(pagename_general, optname_invoice_number, "x", "", None))
        reg(StringOption(pagename_general, "Custom Title", "z",
                         "A custom string to replace Invoice, Bill or Expense Voucher.", ""))
        reg(TextOption(
            "Layout", "CSS", "zz", "CSS code. This field specifies the CSS code for styling the invoice."
                                   " Please see the exported report for the CSS class names.",
            variant_list.get(self.variant, {}).get("css")))

        reg(PixMapOption("Layout", "Picture Location", "zy", "Location for Picture", ""))
        reg(SimpleBooleanOption("Display Columns", "Date", "b", "Display the date?", True))
        reg(SimpleBooleanOption("Display Columns", "Description", "d", "Display the description?", True))
        reg(SimpleBooleanOption("Display Columns", "Action", "g", "Display the action?", True))
        reg(SimpleBooleanOption("Display Columns", "Quantity", "ha", "Display the quantity of items?", True))
        reg(SimpleBooleanOption("Display Columns", "Price", "hb", "Display the price per item?", True))
        reg(SimpleBooleanOption("Display Columns", "Discount", "k", "Display the entry's discount?", True))
        reg(SimpleBooleanOption("Display Columns", "Taxable", "l", "Display the entry's taxable status?", True))
        reg(SimpleBooleanOption("Display Columns", "Tax Amount", "m", "Display each entry's total total tax?", False))
        reg(SimpleBooleanOption("Display Columns", "Total", "n", "Display the entry's value?", True))
        reg(SimpleBooleanOption("Display", "Due Date", "c", "Display due date?", True))
        reg(SimpleBooleanOption("Display", "Subtotal", "d", "Display the subtotals?", True))
        cb = ComplexBooleanOption("Display", "Payable to", "ua1", "Display the Payable to: information.", False, False)
        cb.set_value = lambda x: self.options.set_option_selectable_by_name("Display", "Payable to string", x)
        reg(cb)

        reg(TextOption(
            "Display", "Payable to string",
            "ua2", "The phrase for specifying to whom payments should be made.", "Please make all checks payable to"))

        cb = ComplexBooleanOption(
            "Display", "Company contact",
            "ub1", "Display the Company contact information.", False, False,
            )
        cb.set_value = lambda x: self.options.set_option_selectable_by_name("Display", "Company contact string", x)
        reg(cb)
        reg(TextOption(
            "Display", "Company contact string",
            "ub2", "The phrase used to introduce the company contact.",
            "Please direct all enquiries to"))

        reg(NumberRangeOption(
            "Display", "Minimum # of entries",
            "zz", "The minimum number of invoice entries to display.", 1, 0, 23, 0, 1))

        reg(SimpleBooleanOption("Display", "Use Detailed Tax Summary", "o", "Display all tax categories separately"
                                                                            " (one per line) instead of one single tax line.?",
                                False))

        reg(InternalOption("Display", "Totals", True))
        reg(SimpleBooleanOption("Display", "References", "s", "Display the invoice references?", True))
        reg(SimpleBooleanOption("Display", "Payment Terms", "t", "Display the invoice billing terms?", True))
        reg(SimpleBooleanOption("Display", "Billing ID", "ta", "Display the billing id?", True))
        reg(SimpleBooleanOption("Display", "Invoice person ID", "tam", "Display the customer/vendor id?", False))
        reg(SimpleBooleanOption("Display", "Invoice Notes", "tb", "Display the invoice notes?", False))
        reg(SimpleBooleanOption("Display", "Payments", "tc", "Display the payments applied to this invoice?", True))
        reg(SimpleBooleanOption("Display", "Job Details", "td", "Display the job name for this invoice?", False))
        reg(TextOption("Display", "Extra Notes", "u", "Extra notes to put on the invoice.",
                       "Thank you for your patronage!"))
        reg(MultiChoiceOption("Layout", "Row 1 Left", "1a", "1st row, left",
                              variant_list.get(self.variant, {}).get("1a"),
                              list(layout_key_list.items())))

        reg(MultiChoiceOption("Layout", "Row 1 Right", "1b", "1st row, right",
                              variant_list.get(self.variant, {}).get("1b"),
                              list(layout_key_list.items())))

        reg(MultiChoiceOption(
            "Layout", "Row 2 Left",
            "2a", "2nd row, left",
            variant_list.get(self.variant, {}).get("2a"),
            list(layout_key_list.items())))

        reg(MultiChoiceOption(
            "Layout", "Row 2 Right",
            "2b", "2nd row, right",
            variant_list.get(self.variant, {}).get("2b"),
            list(layout_key_list.items())))

        reg(MultiChoiceOption(
            "Layout", "Row 3 Left",
            "3a", "3rd row, left",
            variant_list.get(self.variant, {}).get("3a"),
            list(layout_key_list.items())))

        reg(MultiChoiceOption(
            "Layout", "Row 3 Right",
            "3b", "3rd row, right",
            variant_list.get(self.variant, {}).get("3b"),
            list(layout_key_list.items())))
        self.options.set_default_section("General")

    def make_entry_table(self, invoice, options, cust_doc, credit_note):
        def opt_val(section, name):
            option = options.lookup_option(section, name)
            if option is not None:
                return option.get_value()

        show_payments = opt_val("Display", "Payments")
        display_all_taxes = opt_val("Display", "Use Detailed Tax Summary")
        display_subtotal = opt_val("Display", "Subtotal")
        lot = invoice.get_posted_lot()
        transaction = invoice.get_posted_transaction()
        currency = invoice.get_currency()

        # (reverse-payments? (not (gncInvoiceAmountPositive invoice))))
        # 
        # (define (display-subtotal monetary used-columns)
        #     (if (value-col used-columns)
        # monetary
        # (let ((amt (gnc-monetary-amount monetary)))
        #     (if amt
        #     (if (negative? amt)
        #     (monetary-neg monetary)
        # monetary)
        # monetary))))
        # 
        # (define (add-payment-row table used-columns split total-collector reverse-payments?)
        # (let* ((t (xaccSplitGetParent split))
        # (currency (xaccTransGetCurrency t))
        # ;; Depending on the document type, the payments may need to be sign-reversed
        # (amt (make-gnc-monetary currency
        #     (if reverse-payments?
        # (- (xaccSplitGetValue split))
        # (xaccSplitGetValue split)))))
        # 
        # (total-collector 'add
        # (gnc-monetary-commodity amt)
        # (gnc-monetary-amount amt))
        # 
        # (html-table-append-row/markup!
        # table "grand-total"
        # (append
        # if date-col used-columns)
        # (qof-print-date (xaccTransGetDate t)))
        # 
        # if description-col used-columns)
        # "Payment, thank you!"))
        # 
        # (list (make-html-table-cell/size/markup
        # 1 (- (max 3 (num-columns-required used-columns))
        # (if (date-col used-columns) 1 0)
        # (if (description-col used-columns) 1 0))
        # "total-number-cell"
        # (display-subtotal amt used-columns)))))))
        # 
        table = TABLE()
        used_columns = self.build_column_used(options)
        entries = invoice.get_entries()

        def add_entry_row(entry, row_style):
            row_contents = []
            # (html-table-append-row/markup!
            # table row-style
            # (append
            if date_col(used_columns):
                row_contents.append(TD(print_datetime(entry.get_date())))

            if description_col(used_columns):
                row_contents.append(TD(entry.get_description()))

            if quantity_col(used_columns):
                row_contents.append(TD(entry.get_quantity() if isinstance(entry, (BillEntry, InvoiceEntry)) else 1,
                                       _class="number-cell"))

            if price_col(used_columns):
                row_contents.append(TD(sprintamount(entry.get_price() if isinstance(entry,
                                                                                    (BillEntry,
                                                                                     InvoiceEntry)) else entry.get_amount(),
                                                    PrintAmountInfo.commodity(currency)), _class="number-cell"))

            # if discount_col(used_columns):
            #     (if cust_doc?
            # (make_html_table_cell/markup
            # "number_cell"
            # (monetary_or_percent (gncEntryGetInvDiscount entry)
            # currency
            # (gncEntryGetInvDiscountType entry)))
            # ""))

            # if tax_col(used_columns):
            #     (if (if cust_doc?
            #          (and (gncEntryGetInvTaxable entry)
            # (gncEntryGetInvTax entry))
            # (and (gncEntryGetBillTaxable entry)
            # (gncEntryGetBillTax entry)))
            # ;; Translators: This "T" is displayed in the taxable column, if this entry contains tax
            # "T",""))
            #
            # if taxvalue_col(used_columns):
            # (make_html_table_cell/markup
            # "number_cell"
            # (make_gnc_monetary
            # currency (gncEntryGetDocTaxValue entry #t cust_doc? credit_note?))))

            if value_col(used_columns):
                row_contents.append(TD(entry.get_value(), _class="number_cell"))
            table.append(TR(*row_contents, _class=row_style))

        def add_subtotal_row(subtotal, subtotal_style, subtotal_label):
            table.append(TR(TD(subtotal_label, _class="total-label-cell"),
                            TD(sprintamount(subtotal, PrintAmountInfo.commodity(currency, True)),
                               _class="total-number-cell", _colspan=max(3, num_columns_required(used_columns))),
                            _class=subtotal_style))

        headings = self.make_heading_list(used_columns)
        table.append(THEAD(TR(*headings)))

        # (let do-rows-with-subtotals ((entries entries)
        # (odd-row? #t)
        # (num-entries 0))
        # (if (null? entries)
        # 
        # ;; all entries done, add subtotals
        # (let ((total-collector (make-commodity-collector)))
        # 
        # ;; minimum number of entries- replicating fancy-invoice option
        # (let loop ((num-entries-left (- (opt_val("Display", "Minimum # of entries" ) num-entries))
        # (odd-row? odd-row?))
        # (when (positive? num-entries-left)
        # (html-table-append-row/markup!
        # table (if odd-row? "normal-row", "alternate-row")
        # (html-make-empty-cells (num-columns-required used-columns)))
        # (loop (1- num-entries-left)
        #  (not odd-row?))))
        # 
        # (if display-subtotal?
        #  (add-subtotal-row (gncInvoiceGetTotalSubtotal invoice)
        # "grand-total" "Net Price")))
        # 
        # (if display-all-taxes
        #  (for-each
        #  (lambda (parm)
        # (let ((value (cdr parm))
        #  (acct (car parm)))
        # (add-subtotal-row value
        # "grand-total" (xaccAccountGetName acct))))
        # (gncInvoiceGetTotalTaxList invoice))
        # 
        # ;; nope, just show the total tax.
        add_subtotal_row(invoice.get_total_tax(), "grand-total", "Tax")
        add_subtotal_row(invoice.get_total(), "grand-total", "Total Price")

        # (total-collector 'add currency (gncInvoiceGetTotal invoice))
        # 
        # (if (and show-payments (not (null? lot)))
        # (let ((splits (sort-list!
        # (gnc-lot-get-split-list lot)
        #       (lambda (s1 s2)
        #       (let ((t1 (xaccSplitGetParent s1))
        #        (t2 (xaccSplitGetParent s2)))
        # (< (xaccTransOrder t1 t2) 0))))))
        # (for-each
        # (lambda (split)
        #  (if (not (equal? (xaccSplitGetParent split) transaction))
        # (add-payment-row table used-columns
        # split total-collector
        # reverse-payments?)))
        # splits)))
        # 
        # (add-subtotal-row (cadr (total-collector 'getpair currency #f))
        # "grand-total" "Amount Due")))
        # 
        # (begin
        # 
        #  (add-entry-row (car entries)
        #     (if odd-row? "normal-row", "alternate-row"))
        # 
        # (do-rows-with-subtotals (cdr entries)
        # (not odd-row?)
        # (1+ num-entries)))))
        # 
        return table

    def make_invoice_details_table(self, invoice, options):
        def opt_val(section, name):
            option = options.lookup_option(section, name)
            if option is not None:
                return option.get_value()

        invoice_details_table = TABLE()
        book = invoice.get_book()
        date_format = fancy_date(book)
        # (jobnumber (gncJobGetID (gncPersonGetJob (gncInvoiceGetPerson invoice))))
        # (jobname (gncJobGetName (gncPersonGetJob (gncInvoiceGetPerson invoice)))))
        # 
        # (if (gncInvoiceIsPosted invoice)
        # 
        # (begin
        #  (html-table-append-row!
        # invoice-details-table
        # (make-date-row "Date") (gncInvoiceGetDatePosted invoice) date-format))
        # 
        # (if (opt_val("Display", "Due Date")
        # (html-table-append-row!
        # invoice-details-table
        # (make-date-row "Due Date") (gncInvoiceGetDateDue invoice) date-format))))
        # 
        # (html-table-append-row! invoice-details-table
        # (make-html-table-cell/size
        # 1 2 (make-html-span/markup
        # "invoice-in-progress"
        # (make-html-text
        # "Invoice in progress..."))))))
        # 
        # (if (opt_val("Display", "Billing ID")
        # (let ((billing-id (gncInvoiceGetBillingID invoice)))
        #     (if (and billing-id (not (string-null? billing-id)))
        # (begin
        #  (html-table-append-row! invoice-details-table
        #  (list
        #  "Reference:")
        # (make-html-div/markup
        # "div-align-right"
        # (multiline-to-html-text billing-id))))
        # (html-table-append-row! invoice-details-table '())))))
        # 
        # (if (opt_val("Display", "Billing Terms")
        # (let* ((term (gncInvoiceGetTerms invoice))
        # (terms (gncBillTermGetDescription term)))
        # (if (and terms (not (string-null? terms)))
        # (html-table-append-row! invoice-details-table
        # (list
        # "Terms:")
        # (make-html-div/markup
        # "div-align-right"
        # (multiline-to-html-text terms)))))))
        # 
        # ;; Add job number and name to invoice if requested and if it exists
        # (if (and (opt_val("Display", "Job Details")
        # (not (string-null? jobnumber)))
        # (begin
        # (html-table-append-row! invoice-details-table
        # (list "Job number:")
        # (make-html-div/markup
        # "div-align-right"
        # jobnumber)))
        # (html-table-append-row! invoice-details-table
        # (list "Job name:")
        # (make-html-div/markup
        # "div-align-right"
        # jobname)))))
        # invoice-details-table))
        # 

    @staticmethod
    def make_client_table(person: Person):
        table = TABLE()
        table.append(TR(TD(DIV(person.get_name(), _class="maybe-align-right client-name"))))
        table.append(
            TR(TD(DIV(multiline_to_html_text(person.get_address()), _class="maybe-align-right client-address"))))
        table.append(TR(TD(DIV(person.get_id(), _class="maybe-align-right client-id"))))
        return table

    @staticmethod
    def make_date_row(label, date, date_format):
        return DIV(label + ":" + date.strftime(date_format))

    @staticmethod
    def make_company_table(book):
        table = TABLE()
        name = company_info(book, company_name)
        addy = company_info(book, company_addy)
        phone = company_info(book, company_phone)
        fax = company_info(book, company_fax)
        email = company_info(book, company_email)
        url = company_info(book, company_url)
        # taxnr = option_get_value(book, tax_label,tax_nr_label)
        taxid = company_info(book, company_id)

        if name is not None:
            table.append(TR(TD(DIV(name, _class="maybe-align-right company-name"))))

        if addy is not None:
            table.append(TR(TD(DIV(*multiline_to_html_text(addy), _class="maybe-align-right company-address"))))

        if phone is not None:
            table.append(TR(TD(DIV(phone, _class="maybe-align-right company-phone"))))

        if fax is not None:
            table.append(TR(TD(DIV(fax, _class="maybe-align-right company-fax"))))

        if email is not None:
            table.append(TR(TD(DIV(email, _class="maybe-align-right company-email"))))
        if url is not None:
            table.append(TR(TD(DIV(url, _class="maybe-align-right company-url"))))

        if taxid is not None:
            table.append(TR(TD(DIV(taxid, _class="maybe-align-right company-tax-id"))))

        # (if (and taxnr (not (string-null? taxnr)))
        # (html-table-append-row!
        # table (list (make-html-div/markup
        # "maybe-align-right company-tax-nr" taxnr))))

        return table

    def render(self, report_obj):
        def opt_val(section, name):
            option = report_obj.options.lookup_option(section, name)
            if option is not None:
                return option.get_value()

        document = HtmlDocument()
        invoice = opt_val(pagename_general, optname_invoice_number)
        references = opt_val("Display", "References")
        custom_title = opt_val(pagename_general, "Custom Title")
        if invoice is None or not isinstance(invoice, (Invoice, Bill)):
            document.add_object(make_generic_warning("Invoice", report_obj.id,
                                                     "", "No valid invoice selected. Click on the Options"
                                                         " button and select the invoice to use."))
        else:
            book = invoice.get_book()
            person = invoice.get_customer() if isinstance(invoice, Invoice) else invoice.get_supplier() if isinstance(
                invoice, Bill) else invoice.get_employee()
            cust_doc = isinstance(invoice, Invoice)

            credit_note = invoice.get_is_credit_note() if isinstance(invoice, Invoice) else invoice.get_is_debit_note()
            orders = []
            # (orders (if references? (delete-duplicates (map gncEntryGetOrder (gncInvoiceGetEntries invoice))) '()))
            default_title = ""
            if cust_doc:
                if credit_note:
                    default_title = "Credit Note"
                else:
                    default_title = "Invoice"
            else:
                if isinstance(invoice, Bill):
                    if credit_note:
                        default_title = "Debit Note"
                    else:
                        default_title = "Bill"
            invoice_title = "{} #{}".format(
                default_title if custom_title is None or len(custom_title) == 0 else custom_title, invoice.get_id())
            layout_lookup_table = {"picture": DIV(IMG(_src=opt_val("Layout", "Picture Location")), _class="picture"),
                                   "invoice": DIV(self.make_invoice_details_table(invoice, report_obj.options),
                                                  _class="invoice-details-table"),
                                   "client": DIV(self.make_client_table(person),
                                                 _class="client-table"),
                                   "company": DIV(self.make_company_table(book),
                                                  _class="company-table"),
                                   "today": print_datetime(datetime.date.today()),
                                   "none": None

                                   }
            layout_lookup = lambda loc: layout_lookup_table.get(opt_val("Layout", loc))
            document.set_style_text(opt_val("Layout", "CSS"))
            main_table = TABLE()
            main_table.append(TR(TD(DIV(invoice_title, _class="invoice-title"), _colspan=2)))
            main_table.append(TR(TD(layout_lookup("Row 1 Left")),
                                 TD(DIV(layout_lookup("Row 1 Right"), _class="div-align-right"))))
            main_table.append(TR(TD(layout_lookup("Row 2 Left")),
                                 TD(DIV(layout_lookup("Row 2 Right"), _class="div-align-right"))))
            main_table.append(TR(TD(layout_lookup("Row 3 Left")),
                                 TD(DIV(layout_lookup("Row 3 Right"), _class="div-align-right"))))
            main_table.append(
                TR(TD(DIV(self.make_entry_table(invoice, self.options, cust_doc, credit_note), _class="entries-table"))))

            if opt_val("Display", "Invoice Notes"):
                notes = invoice.get_notes()
                main_table.append(TR(TD(multiline_to_html_text(notes), _colspan=2), _class="invoice-notes"))
            if opt_val("Display", "Payable to"):
                name = company_info(book, company_name)
                name_str = opt_val("Display", "Payable to string")
                if name is not None and len(name):
                    main_table.append(TR(TD(DIV(multiline_to_html_text(name_str + ": " + name),
                                                _class="invoice-footer-payable-to"))))
            if opt_val("Display", "Company contact"):
                contact = company_info(book, company_contact)
                contact_str = opt_val("Display", "Company contact string")
                if contact is not None and len(contact):
                    main_table.append(TR(TD(DIV(multiline_to_html_text(contact_str + ": " + contact),
                                                _class="invoice-footer-payable-to"))))
            main_table.append(TR(TD(DIV(multiline_to_html_text(opt_val("Display", "Extra Notes")),
                                        _class="invoice-notes"), _colspan=2)))
            document.add_object(DIV(main_table, _class="main-table"))
        return document


InvoiceReport(name="Printable Invoice", report_id=invoice_report_guid, menu_path=menu_name_business_reports,
              variant="invoice")
InvoiceReport(name="Easy Invoice", report_id=easy_invoice_guid, menu_path=menu_name_business_reports,
              variant="easy-invoice")
InvoiceReport(name="Fancy Invoice", report_id=fancy_invoice_guid, menu_path=menu_name_business_reports,
              variant="fancy-invoice")
