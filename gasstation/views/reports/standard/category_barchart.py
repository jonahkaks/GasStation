from gasstation.utilities.options import *
from ..commodity_utilities import *
from ..report import *

menuname_income = "Income Chart"
menuname_expense = "Expense Chart"
menuname_assets = "Asset Chart"
menuname_liabilities = "Liability Chart"
menutip_income = "Shows a chart with the Income per interval developing over time"
menutip_expense = "Shows a chart with the Expenses per interval developing over time"
menutip_assets = "Shows a chart with the Assets developing over time"
menutip_liabilities = "Shows a chart with the Liabilities developing over time"
reportname_income = "Income Over Time"
reportname_expense = "Expense Over Time"
reportname_assets = "Assets Over Time"
reportname_liabilities = "Liabilities Over Time"
optname_from_date = "Start Date"
optname_to_date = "End Date"
optname_stepsize = "Step Size"
optname_report_currency = "Report's currency"
optname_price_source = "Price Source"
optname_accounts = "Accounts"
optname_levels = "Show Accounts until level"
optname_fullname = "Show long account names"
optname_chart_type = "Chart Type"
optname_stacked = "Use Stacked Charts"
optname_slices = "Maximum Bars"
optname_plot_width = "Plot Width"
optname_plot_height = "Plot Height"
optname_sort_method = "Sort Method"
optname_averaging = "Show Average"
opthelp_averaging = "Select whether the amounts should be shown over the full time period or rather as the average e.g. per month."


class CategoryBarChart(ReportTemplate):
    def export(self, report_obj, export_types, filename):
        pass

    def __init__(self, name, report_guid, account_types, income_expense, menuname, menutip, reverse_balance):
        super().__init__(version=1, name=name, menu_name=menuname, guid=report_guid,
                         menu_path=menu_name_income_expense if income_expense else menu_name_asset_liability,
                         menu_tooltip=menutip)
        self.account_types = account_types
        self.reverse_balance = reverse_balance
        self.do_intervals = income_expense

    def generate_options(self):
        super().generate_options()
        add_option = self.options.register_option
        OptionUtils.add_date_interval(self.options, pagename_general, optname_from_date, optname_to_date, "a")
        OptionUtils.add_interval_choice(self.options, pagename_general, optname_stepsize, "b", "MonthDelta")
        OptionUtils.add_currency(self.options, pagename_general, optname_report_currency, "c")
        OptionUtils.add_price_source(self.options, pagename_general, optname_price_source, "c", "weighted-average")
        if self.do_intervals:
            add_option(MultiChoiceOption(pagename_general, optname_averaging, "e", opthelp_averaging, "None",
                                         [("None", "No Averaging", "Just show the amounts, without any averaging."),
                                          ("YearDelta", "Yearly",
                                           "Show the average yearly amount during the reporting period."),
                                          ("MonthDelta", "Monthly", "Show the average monthly amount during the"
                                                                    " reporting period."),
                                          ("WeekDelta", "Weekly", "Show the average weekly amount during the "
                                                                  "reporting period.")]))
        add_option(AccountListOption(pagename_accounts, optname_accounts, "a", "Report on these accounts, if chosen "
                                                                               "account level allows.",
                                     lambda: filter_accountlist_type(self.account_types,
                                                                     Session.get_current_root().get_descendants_sorted()),
                                     lambda accounts: filter_accountlist_type(self.account_types, accounts),
                                     True))
        OptionUtils.add_account_levels(self.options, pagename_accounts, optname_levels, "c",
                                       "Show accounts to this depth and not further.", 2)
        add_option(SimpleBooleanOption(pagename_display, optname_fullname, "a", "Show the full account name ", False))
        add_option(
            MultiChoiceOption(pagename_general, optname_chart_type, "b", "Select the chart type to use", "barchart",
                              [("barchart", "Bar Chart", "Use bar chars"),
                               ("linechart", "Line Chart", "Use line charts")]))
        add_option(SimpleBooleanOption(pagename_display, optname_stacked, "c", "Show charts as stacked charts?", True))
        add_option(NumberRangeOption(pagename_display, optname_slices, "d", "Maximum number of stacks in the chart.", 8,
                                     2, 24, 0, 1))
        add_option(
            SimpleBooleanOption(pagename_display, "Show table", "c", "Display a table of the selected data.", False))
        OptionUtils.add_plot_size(self.options, pagename_display, optname_plot_width, optname_plot_height,
                                  "f", ["percent", 100.0], ["percent", 100.0])
        OptionUtils.add_sort_method(self.options, pagename_display, optname_sort_method, "g", "amount")
        self.options.set_default_section(pagename_general)

    def render(self, report_obj):
        get_option = lambda section, name: report_obj.options.lookup_option(section, name).get_value()
        report_starting(self.name)
        to_date = DateOption.absolute_time(get_option(pagename_general, optname_to_date))
        from_date = DateOption.absolute_time(get_option(pagename_general, optname_from_date))
        interval = get_option(pagename_general, optname_stepsize)
        report_currency = get_option(pagename_general, optname_report_currency)
        price_source = get_option(pagename_general, optname_price_source)
        report_title = get_option(pagename_general, optname_reportname)
        averaging_selection = get_option(pagename_general, optname_averaging) if self.do_intervals else None
        accounts = get_option(pagename_accounts, optname_accounts)
        account_levels = get_option(pagename_accounts, optname_levels)
        chart_type = get_option(pagename_display, optname_chart_type)
        stacked = get_option(pagename_display, optname_stacked)
        show_fullname = get_option(pagename_display, optname_fullname)
        max_slices = int(get_option(pagename_display, optname_slices))
        height = get_option(pagename_display, optname_plot_height)
        width = get_option(pagename_display, optname_plot_width)
        sort_method = get_option(pagename_display, optname_sort_method)
        averaging_multiplier = 1
        work_to_do = 0

        show_table = get_option(pagename_display, "Show table")
        document = HtmlDocument()
        chart = HtmlChart()
        topl_accounts = filter_accountlist_type(self.account_types, Session.get_current_root().get_children_sorted())
        tree_depth = get_current_account_tree_depth() if account_levels == "all" else int(account_levels)
        show_acct = lambda a: a in accounts
        work_done_s = 0
        if len(accounts) == 0:
            document.add_object(make_no_account_warning(report_title, report_obj.get_id()))
        elif to_date <= from_date:
            document.add_object(make_generic_warning(report_title, report_obj.get_id(), "Invalid Dates",
                                                     "Start date must be earlier than End date"))

        else:
            commodity_list = get_commodities(get_all_subaccounts(accounts), report_currency)
            exchange_fn = case_exchange_time_fn(price_source, report_currency, commodity_list, to_date, 5, 15)
            averaging_fraction_func = date_get_fraction_func(averaging_selection)
            interval_fraction_func = date_get_fraction_func(interval)
            # (averaging_multiplier
            #     (if averaging_fraction_func
            #
            #  (let* ((start_frac_avg (averaging_fraction_func from_date_t64))
            # (end_frac_avg (averaging_fraction_func (1+ to_date_t64)))
            # (diff_avg (_ end_frac_avg start_frac_avg))
            # (diff_avg_numeric
            #     (/ (inexact_>exact (round (* diff_avg 1000000)))
            # ;; 6 decimals precision
            # 1000000))
            # (start_frac_int (interval_fraction_func from_date_t64))
            # (end_frac_int (interval_fraction_func (1+ to_date_t64)))
            # (diff_int (_ end_frac_int start_frac_int))
            # (diff_int_numeric (inexact_>exact diff_int)))
            # ;; Extra sanity check to ensure a number smaller than 1
            #     (if (> diff_avg diff_int)
            #             (/ diff_int_numeric diff_avg_numeric)
            # 1))
            # 1))
            #
            if averaging_selection ==AveragingSelection.MonthDelta:
                report_title = " ".join([report_title, "Monthly Average"])
            elif averaging_selection ==AveragingSelection.WeekDelta:
                report_title = " ".join([report_title, "Weekly Average"])
            elif averaging_selection ==AveragingSelection.DayDelta:
                report_title = " ".join([report_title, "Daily Average"])

            # (currency_frac (gnc_commodity_get_fraction report_currency))
            # ;; This is the list of date intervals to calculate.
            # (dates_list (make_date_list
            # ((if do_intervals?
            #   time64_start_day_time
            #   time64_end_day_time) from_date_t64)
            # (time64_end_day_time to_date_t64)
            # (deltasym_to_delta interval)))
            # (other_anchor ""))
            #

        def count_accounts(current_depth, accts):
            if current_depth < tree_depth:
                return sum(map(lambda acc: count_accounts(current_depth + 1, acc.get_children()) + 1,
                               filter(show_acct, accts)))
            else:
                return len(list(filter(show_acct, accts)))

        def collector_to_double(c):
            return c.sum(report_currency, exchange_fn)[1] * averaging_multiplier

        account_balance = lambda a, subaccts: collector_to_double(profit_fn(a, subaccts))

        def traverse_accounts(current_depth, accts):
            cur_work_done = 0
            work_done = 0
            if current_depth < tree_depth:
                work_done += 1
                res = []
                if not any(accts):
                    return cur_work_done, res
                for cur in accts:
                    report_percent_done(100 * (cur_work_done / work_to_do))
                    subaccts_work, subaccts, = traverse_accounts(current_depth + 1, cur.get_children_sorted())
                    res.extend(subaccts)
                    if show_acct(cur):
                        res.append((account_balance(cur, False), cur))
                    cur_work_done = subaccts_work + 1
                return cur_work_done, res
            else:
                new_accts = []
                for a in filter(show_acct, accts):
                    work_done += 1
                    report_percent_done(100 * (work_done / work_to_do))
                    new_accts.append((account_balance(a, True), a))
                return work_done, new_accts

        if any(accounts):
            pass
        else:
            pass
        report_finished()
        return document
