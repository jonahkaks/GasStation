from gasstation.utilities.options import *
from ..commodity_utilities import *
from ..report import *

optname_to_date = "To"
optname_sort_by = "Sort By"
optname_sort_order = "Sort Order"
optname_report_currency = "Report's currency"
optname_price_source = "Price Source"
optname_show_zeros = "Show zero balance items"
optname_date_driver = "Due or Post Date"
optname_addr_source = "Address Source"
addr_options_list = [("Display Name", "b", "Display Address Name. This, and other fields, may be useful if "
                                           "copying this report to a spreadsheet for use in a mail merge."),
                     ("Address", "c", "Display Address."),
                     ("Phone", "d", "Display Phone."),
                     ("Fax", "e", "Display Fax."),
                     ("Email", "f", "Display Email."),
                     ("Active", "g", "Display Active status.")]

no_APAR_account = "No valid A/Payable or A/Receivable " \
                  "account found. Please ensure valid AP/AR account exists."

empty_APAR_accounts = "A/Payable or A/Receivable accounts exist but have no suitable transactions."

num_buckets = 6


class NewAging(ReportTemplate):
    def export(self, report_obj, export_types, filename):
        pass

    def __init__(self, name, report_guid, account_type):
        super().__init__(version=1, name=name, guid=report_guid,
                         menu_path=menu_name_business_reports, in_menu=True)
        self.type = account_type

    @staticmethod
    def setup_query(query: Query, accounts: list, date):
        query.set_book(Session.get_current_book())
        query.add_cleared_match(ClearedMatch.ALL & ~ClearedMatch.VOIDED, QueryOp.AND)
        query.add_account_match(accounts, GuidMatch.ANY, QueryOp.AND)
        query.add_date_match_tt(False, 0, True, date, QueryOp.AND)
        query.set_sort_order([SPLIT_TRANS, TRANS_DATE_POSTED], [], [])
        query.set_sort_increasing(True, True, True)

    def generate_options(self):
        super().generate_options()
        add_option = self.options.register_option
        # add_option(InternalOption("__report", "reverse-balance?", self.reverse_balance))
        OptionUtils.add_report_date(self.options, pagename_general, optname_to_date, "a")
        self.options.lookup_option(pagename_general, optname_to_date).set_value(["relative", "today"])
        add_option(MultiChoiceOption(pagename_general, optname_sort_by, "i", "Sort companies by.", "name",
                                     [("name", "Name", "Name of the company."),
                                      ("total", "Total Owed", "Total amount owed to/from Company."),
                                      ("oldest-bracket", "Bracket Total Owed",
                                       "Amount owed in oldest bracket - if same go to next oldest.")]))
        add_option(MultiChoiceOption(pagename_general, optname_sort_order, "ia", "Sort order.", "increasing",
                                     [("increasing", "Increasing", "Alphabetical order"),
                                      ("decreasing", "Decreasing", "Reverse alphabetical order")]))
        add_option(SimpleBooleanOption(pagename_display, optname_show_zeros,
                                       "j", "Show all vendors/customers even if they have a zero balance.", False))
        add_option(MultiChoiceOption(pagename_general, optname_date_driver, "k", "Leading date.", "duedate",
                                     [("duedate", "Due Date", "Due date is leading."),
                                      ("postdate", "Post Date", "Post date is leading.")]))
        self.options.set_default_section(pagename_general)
        for opt_name, sort, help_text in addr_options_list:
            add_option(SimpleBooleanOption(pagename_display, opt_name, sort, help_text, False))
        if self.type == AccountType.RECEIVABLE:
            add_option(MultiChoiceOption(pagename_display, optname_addr_source, "a", "Address source.","billing",
                                         [("billing","Billing address"), ("shipping","Shipping address")]))
            
    @staticmethod
    def split_person(split):
        return 

    @staticmethod
    def split_is_not_business(split):
        t = split.get_transaction().get_type()
        return t not in [TransactionType.INVOICE, TransactionType.PAYMENT]

    def split_has_person(self, split, person):
        return self.split_person(split) == person
    
    def split_person_is_invalid(self, split):
        return not self.split_person(split)

    @staticmethod
    def split_from_acct(split, acct):
        return acct == split.get_account()
      
    def list_split(self, lst, fn, cmp):
        # let-values (((list-yes list-no) (partition (lambda (elt) (fn elt cmp)) lst)))
        # (cons list-yes list-no)))
        return []

    def render(self, report_obj):
        receivable = self.type == AccountType.RECEIVABLE
        opt_val = lambda section, name: self.options.lookup_option(section, name).get_value()
        heading_list = ["Company","Pre-Payment", "Current","0-30 days","31-60 days","61-90 days","91+ days","Total"]
        accounts = list(filter(lambda acc: acc.get_type() == self.type, 
                               Session.get_current_root().get_descendants_sorted()))

        report_title=opt_val(pagename_general,optname_reportname)
        report_date = DateOption.absolute_time(opt_val(pagename_general,optname_to_date))
        sort_order=opt_val(pagename_general,optname_sort_order)
        sort_by=opt_val(pagename_general,optname_sort_by)
        show_zeros=opt_val(pagename_general,optname_show_zeros)
        date_type=opt_val(pagename_general,optname_date_driver)
        query = Query.create_for(ID_SPLIT)
        document = HtmlDocument()

        # (define (sort_aging<? a b)
        # (match_let* (((own1 aging1 aging_total1) a)
        #              ((own2 aging2 aging_total2) b)
        # (increasing? (eq? sort_order 'increasing))
        # (op_str (if increasing? string_locale<? string_locale>?))
        # (op_num (if increasing? < >)))
        # (case sort_by
        # ((name)  (op_str (gncPersonGetName own1) (gncPersonGetName own2)))
        # ((total) (op_num aging_total1 aging_total2))
        # (else
        #  (let lp ((aging1 aging1) (aging2 aging2))
        # (cond
        #  ((null? aging1) (op_str (gncPersonGetName own1) (gncPersonGetName own2)))
        # ((= (car aging1) (car aging2)) (lp (cdr aging1) (cdr aging2)))
        # (else (op_num (car aging1) (car aging2)))))))))
        
        document.set_title("{} _ {}".format(report_title, print_datetime(report_date)))
        if accounts is None or len(accounts) == 0:
            document.add_object(P(no_APAR_account))
        else:
            self.setup_query(query, accounts, report_date)
            splits = query.get_splits_unique_trans()
            query.destroy()
        
        report_finished()
        return document


payables_aging_guid = "e57770f2dbca46619d6dac4ac5469b50"
receivables_aging_guid = "9cf76bed17f14401b8e3e22d0079cb98"

NewAging(name="Payable Aging", report_guid=payables_aging_guid, account_type=AccountType.PAYABLE)
NewAging(name="Receivable Aging", report_guid=payables_aging_guid, account_type=AccountType.RECEIVABLE)
