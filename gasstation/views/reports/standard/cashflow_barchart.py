from gasstation.utilities.options import *
from gasstation.utilities.ui import *
from ..commodity_utilities import *
from ..report import *

cashflow_barchart_uuid = "5426e4d987f6444387fe70880e5b28a0"
report_name = "Cash Flow Barchart"
optname_accounts = "Accounts"
optname_include_trading_accounts = "Include Trading Accounts in report"
optname_show_in = "Show Money In"
optname_show_out = "Show Money Out"
optname_show_net = "Show Net Flow"
optname_show_table = "Show Table"
optname_plot_width = "Plot Width"
optname_plot_height = "Plot Height"
optname_from_date = "Start Date"
optname_to_date = "End Date"
optname_stepsize = "Step Size"
optname_report_currency = "Report's currency"
optname_price_source = "Price Source"


class CashFlowBarChart(ReportTemplate):
    def __init__(self):
        super().__init__(version=1, name=report_name, guid=cashflow_barchart_uuid,
                         menu_tooltip="Shows a b(with cash flow over time", menu_name=report_name,
                         menu_path=menu_name_income_expense)
        self.money_in_alist = []
        self.money_in_accounts = []
        self.money_in_collector = CommodityCollector()
        self.time_exchange_fn = None
        self.money_out_alist = []
        self.money_out_accounts = []
        self.money_out_collector = CommodityCollector()

    def render(self, report_obj):
        def get_option(section, name):
            option = report_obj.options.lookup_option(section, name)

            if option is not None:
                return option.get_value()

        def to_currency_string(monetary: Tuple[Commodity, Decimal]):
            return sprintamount(monetary[1], PrintAmountInfo.commodity(monetary[0], True))

        accounts = get_option(pagename_accounts, optname_accounts)
        include_trading_accounts = get_option(pagename_accounts, optname_include_trading_accounts)
        work_done = 0
        work_to_do = 0
        report_currency = get_option(pagename_general, optname_report_currency)
        price_source = get_option(pagename_general, optname_price_source)
        from_date = DateOption.absolute_time(get_option(pagename_general, optname_from_date))
        to_date = DateOption.absolute_time(get_option(pagename_general, optname_to_date))
        exchange_fn = case_exchange_fn(price_source, report_currency, to_date)
        interval = get_option(pagename_general, optname_stepsize)
        show_in = get_option(pagename_display, optname_show_in)
        show_out = get_option(pagename_display, optname_show_out)
        show_net = get_option(pagename_display, optname_show_net)
        show_table = get_option(pagename_display, optname_show_table)
        height = get_option(pagename_display, optname_plot_height)
        width = get_option(pagename_display, optname_plot_width)
        dates_list = []
        # make_date_interval_list
        # (time64_start_day_time from_date)
        # (time64_end_day_time to_date)
        # (deltasym_to_delta interval)))
        report_title = get_option(pagename_general, optname_reportname)
        doc = HtmlDocument()
        chart = HtmlChart()
        # (non_zeros #f))
        if len(accounts) > 0:
            commodity_list = accounts_get_commodities(accounts, report_currency)
            time_exchange_fn = case_exchange_time_fn(price_source, report_currency, commodity_list, to_date, 0, 0)
            date_string_list = list(map(print_datetime, dates_list))
            in_list = []
            out_list = []
            net_list = []
            total_in = None
            total_out = None
            total_net = None

            def to_report_currency(currency, amount, date):
                return time_exchange_fn(currency, amount, report_currency, date)

            def sum_collector(collector: CommodityCollector):
                return collector.sum_collector_commodity(report_currency, exchange_fn)

            def make_cashflow_url(idx):
                return Anchors.report("f8748b813fab4220ba26e743aedf38da", report_obj,
                                      [("General", "Start Date", ("absolute", dates_list[idx][0])),
                                       ("General", "End Date", ("absolute", dates_list[idx][1])),
                                       ("Accounts", "Account", accounts)])

            def cashflow_urls():
                return list(map(lambda iota: make_cashflow_url(iota), range(len(dates_list))))

            work_done = 0
            work_to_do = len(dates_list)
            for date_pair in dates_list:
                work_done += 1
                report_percent_done(80 * (work_done / work_to_do))
                settings = {"accounts": accounts,
                            "from_date": date_pair[0],
                            "to_date": date_pair[1],
                            "report_currency": report_currency,
                            "include_trading_accounts": include_trading_accounts,
                            "to_report_currency": to_report_currency}
                result = cash_flow_calc_money_in_out(settings)
                money_in_collector = result["money_in_collector"]
                money_out_collector = result["money_out_collector"]
                money_in = sum_collector(money_in_collector)
                money_out = sum_collector(money_out_collector)
                money_net = money_in - money_out
                in_list.append(money_in)
                out_list.append(money_out)
                net_list.append(money_net)

            if show_in:
                in_list.reverse()
                total_in = sum(in_list)

            if show_out:
                out_list.reverse()
                total_out = sum(out_list)

            if show_net:
                net_list.reverse()
                total_net = sum(net_list)

            report_percent_done(90)

            chart.set_title(report_title + "{} to {}".format(print_datetime(from_date), print_datetime(to_date)))
            chart.set_width(width)
            chart.set_height(height)
            chart.set_y_axis_label(report_currency.get_mnemonic())
            chart.set_currency_iso(report_currency.get_mnemonic())
            chart.set_currency_symbol(report_currency.get_nice_symbol())
            chart.set_data_labels(date_string_list)
            if show_in:
                chart.add_data_series("Money In", in_list, "#0074D9", urls=cashflow_urls)
            if show_out:
                chart.add_data_series("Money Out", out_list, "#FF4136", urls=cashflow_urls)
            if show_net:
                chart.add_data_series("Net Flow", net_list, "#2ECC40", urls=cashflow_urls)
            report_percent_done(95)

            non_zeros = filter(lambda a: a != 0, in_list + out_list + net_list)

            if non_zeros:
                doc.add_object(chart)
            else:
                doc.add_object(make_empty_data_warning(report_title, report_obj.get_id()))

            if non_zeros and show_table:
                table = TABLE()

                def add_row(date, _in, out, net):
                    table.append(TR(TD(date),
                                    TD(_in if show_in else "", _class="number_cell"),
                                    TD(out if show_out else "", _class="number_cell"),
                                    TD(net if show_net else "", _class="number_cell")))

                table.append(
                    THEAD(TR(TH("Date"), TH("Money In" if show_in else ""), TH("Money Out" if show_out else ""),
                             TH("Net Flow" if show_net else ""))))
                doc.add_object(H3("Overview:"))
                for date, _in, out, net in zip(date_string_list, in_list, out_list, net_list):
                    add_row(date, _in, out, net)
                add_row("Total", total_in, total_out, total_net)
                doc.add_object(table)
        else:
            doc.add_object(make_no_account_warning(report_title, report_obj.get_id()))
        report_finished()
        return document

    def export(self, report_obj, export_types, filename):
        pass

    def generate_options(self):
        super().generate_options()
        add_option = self.options.register_option
        OptionUtils.add_date_interval(self.options, pagename_general, optname_from_date, optname_to_date, "a")
        OptionUtils.add_interval_choice(self.options, pagename_general, optname_stepsize, "b", "MonthDelta")
        OptionUtils.add_currency(self.options, pagename_general, optname_report_currency, "c")
        OptionUtils.add_price_source(self.options, pagename_general, optname_price_source, "d", "pricedb-nearest")
        add_option(AccountListOption(pagename_accounts, optname_accounts, "a", "Report on these accounts.",
                                     lambda: filter_accountlist_type(
                                         [AccountType.BANK, AccountType.CASH, AccountType.CURRENT_ASSET,
                                          AccountType.FIXED_ASSET, AccountType.STOCK,AccountType.MUTUAL,
                                          ],
                                         Session.get_current_root().get_descendants_sorted()), None, None))

        add_option(SimpleBooleanOption(pagename_accounts, optname_include_trading_accounts, "b",
                                       "Include transfers to and from Trading Accounts in the report.", False))
        add_option(SimpleBooleanOption(pagename_display, optname_show_in, "b", "Show money in?", True))
        add_option(SimpleBooleanOption(pagename_display, optname_show_net, "c", "Show net money flow?", True))
        add_option(SimpleBooleanOption(pagename_display, optname_show_table, "d",
                                       "Display a table of the selected data.", False))
        OptionUtils.add_plot_size(self.options, pagename_display, optname_plot_width, optname_plot_height, "e",
                                  ["percent", 100.0], ["percent", 100.0])

        self.options.set_default_section(pagename_general)


CashFlowBarChart()
