from gasstation.utilities.options import *
from gasstation.utilities.ui import *
from gasstation.views.dialogs.location.location import LocationDialog
from ..commodity_utilities import *
from ..report import *

report_name = "Cash Flow"
optname_from_date = "Start Date"
optname_to_date = "End Date"
optname_display_depth = "Account Display Depth"
optname_show_subaccounts = "Always show sub-accounts"
optname_accounts = "Account"

optname_report_currency = "Report's currency"
optname_price_source = "Price Source"
optname_show_rates = "Show Exchange Rates"
optname_location = "Use this location"
optname_all_location = "Use All locations"
optname_show_full_names = "Show Full Account Names"
optname_include_trading_accounts = "Include Trading Accounts in report"


class CashFlow(ReportTemplate):
    def __init__(self):
        super().__init__(version=1, name=report_name, guid="f8748b813fab4220ba26e743aedf38da",
                         menu_name=report_name,
                         menu_path=menu_name_income_expense)
        self.money_in_alist = []
        self.money_in_accounts = []
        self.money_in_collector = CommodityCollector()
        self.time_exchange_fn = None
        self.money_out_alist = []
        self.money_out_accounts = []
        self.money_out_collector = CommodityCollector()

    @staticmethod
    def local_filter_accountlist_type():
        accnts = Session.get_current_root().get_descendants_sorted()
        return filter_accountlist_type([AccountType.BANK, AccountType.CASH, AccountType.CURRENT_ASSET,
                                        AccountType.FIXED_ASSET,AccountType.STOCK, AccountType.MUTUAL], accnts)

    @staticmethod
    def same_account(a1, a2):
        return a1.guid == a2.guid

    @staticmethod
    def same_split(s1, s2):
        return s1.guid == s2.guid

    @staticmethod
    def account_in_list(acnt, acclst):
        for acc in acclst:
            if CashFlow.same_account(acnt, acc):
                return True
        return False

    @staticmethod
    def account_in_alist(acnt, alst):
        for acpr in alst:
            if CashFlow.same_account(acnt, acpr[0]):
                return acpr
        return None

    def calc_money_in_out(self, accounts: List[Account],location, to_date_tp: datetime.date, from_date_tp: datetime.date,
                          report_currency: Commodity):
        def split_in_list(split, splits):
            if len(splits) <= 0:
                return False
            for splt in splits:
                if CashFlow.same_split(splt, split):
                    return True
            return False

        def to_report_currency(curr, amnt, date):
            retval = self.time_exchange_fn.run((curr, amnt), report_currency, date)
            return retval[1]

        splits_to_do = accounts_count_splits(accounts)
        for acc in accounts:
            seen_split_list = []
            splt: Split
            for work_done, splt in enumerate(acc.get_splits()):
                report_percent_done((work_done / float(splits_to_do)) * 85.0)
                transaction: Transaction = splt.get_transaction()
                if location is not None and transaction.get_location() != location:
                    continue

                date_posted = transaction.get_post_date()
                if from_date_tp <= date_posted <= to_date_tp:
                    parent_currency = transaction.get_currency()
                    trnval = Decimal(0)
                    spltval = splt.get_value()
                    trnsplts = transaction.get_splits()

                    for trnsplt in trnsplts:
                        psv = trnsplt.get_value()
                        if psv > 0:
                            trnval += psv

                    for trnsplt in trnsplts:

                        s_acc: Account = trnsplt.get_account()
                        s_val = trnsplt.get_value()

                        if s_acc is not None and not self.account_in_list(s_acc, accounts) \
                                and s_val * spltval < 0:
                            if not split_in_list(trnsplt, seen_split_list):
                                if trnval.is_zero():
                                    split_trans_ratio = Decimal(1)
                                else:
                                    split_trans_ratio = abs(spltval / trnval)
                                s_upd = split_trans_ratio * s_val
                                s_val = s_upd
                                seen_split_list.append(trnsplt)

                                if s_val < 0:
                                    pr = self.account_in_alist(s_acc, self.money_in_alist)
                                    if pr is None:
                                        pr = [s_acc, CommodityCollector()]
                                        self.money_in_alist.append(pr)
                                        self.money_in_accounts.append(s_acc)
                                    s_acc_in_collector = pr[1]
                                    s_report_value = to_report_currency(parent_currency, -s_val, date_posted)
                                    self.money_in_collector.add(report_currency, s_report_value)
                                    s_acc_in_collector.add(report_currency, s_report_value)

                                else:

                                    pr = CashFlow.account_in_alist(s_acc, self.money_out_alist)
                                    if pr is None:
                                        pr = [s_acc, CommodityCollector()]
                                        self.money_out_alist.append(pr)
                                        self.money_out_accounts.append(s_acc)
                                    s_acc_out_collector = pr[1]
                                    s_report_value = to_report_currency(parent_currency, s_val, date_posted)
                                    self.money_out_collector.add(report_currency, s_report_value)
                                    s_acc_out_collector.add(report_currency, s_report_value)

    def render(self, report_obj):
        def get_option(section, name):
            option = report_obj.options.lookup_option(section, name)

            if option is not None:
                return option.get_value()

        def to_currency_string(monetary: Tuple[Commodity, Decimal]):
            return sprintamount(monetary[1], PrintAmountInfo.commodity(monetary[0], True))

        document = HtmlDocument()
        report_starting(report_name)
        display_depth = get_option(pagename_accounts, optname_display_depth)
        show_subaccts = get_option(pagename_accounts, optname_show_subaccounts)
        accounts = get_option(pagename_accounts, optname_accounts)
        include_trading_accounts = get_option(pagename_accounts, optname_include_trading_accounts)
        report_currency = get_option(pagename_general, optname_report_currency)
        price_source = get_option(pagename_general, optname_price_source)
        show_rates = get_option(pagename_general, optname_show_rates)
        show_full_names = get_option(pagename_general, optname_show_full_names)
        from_date = DateOption.absolute_time(get_option(pagename_general, optname_from_date))
        to_date = DateOption.absolute_time(get_option(pagename_general, optname_to_date))
        exchange_fn = case_exchange_fn(price_source, report_currency, to_date)
        location = get_option(pagename_general, optname_location)
        show_all_locations = get_option(pagename_general, optname_all_location)
        if not any(accounts):
            raise ValueError("NO acconts")
        rpt_ttl = get_option(pagename_general, optname_reportname) + " - " + "%s to %s" % (print_datetime(from_date),
                                                                                           print_datetime(to_date))
        document.set_title(rpt_ttl)

        if show_subaccts:
            subacclst = get_all_subaccounts(accounts)
            for subacc in subacclst:
                if not CashFlow.account_in_list(subacc, accounts):
                    accounts.append(subacc)
        if len(accounts) > 0:
            tree_depth = get_current_account_tree_depth() if display_depth == "all" else int(display_depth)
            money_diff_collector = CommodityCollector()

            commodity_list = []

            self.time_exchange_fn = case_exchange_time_fn(price_source, report_currency,
                                                          commodity_list, to_date, 0.0, 0.0)

            self.money_in_alist = []
            self.money_in_accounts = []
            self.money_in_collector = CommodityCollector()
            self.money_out_alist = []
            self.money_out_accounts = []
            self.money_out_collector = CommodityCollector()

            self.calc_money_in_out(accounts, None if show_all_locations else location, to_date, from_date, report_currency)

            # pdb.set_trace()

            # in scheme there is a second argument generally False
            # I think this is because there are in general 2 arguments (collector, value)
            # to these functions but I think the merge function only uses the collector
            money_diff_collector.merge(self.money_in_collector)
            money_diff_collector.minusmerge(self.money_out_collector)

            # need to figure out how to sort by full name
            # well this is useless - we need the account name!!
            accounts.sort(key=lambda a: a.get_name())
            self.money_in_accounts.sort(key=lambda a: a.get_name())
            self.money_out_accounts.sort(key=lambda a: a.get_name())
            body = BODY(H3("Selected Accounts\n"))
            document.add_object(body)
            ultxt = UL()
            body.append(ultxt)
            work_done = 0
            work_to_do = len(accounts)

            for acc in accounts:
                work_done += 1
                report_percent_done((work_done / float(work_to_do)) * 85.0)

                if acc.get_tree_depth() <= tree_depth:

                    if show_full_names:
                        accnm = acc.get_full_name()
                    else:
                        accnm = acc.get_name()
                    if acc.get_tree_depth() == tree_depth and \
                            not len(acc.get_children()) == 0:
                        if show_subaccts:
                            acctxt = " and subaccounts"
                        else:
                            acctxt = " and selected subaccounts"
                    else:
                        acctxt = ""

                    ultxt.append(LI(ANCHOR(accnm, _href=account_anchor_text(acc)), acctxt))
            new_table = TABLE()
            body.append(new_table)
            new_table.append(TR(TD(HR(), _rowspan=1, _colspan=2)))
            new_table.append(TR(TD("Money into selected accounts comes from"), TD(), _class='primary-subheading'))

            work_to_do = len(self.money_in_alist)

            for row_num, acc in enumerate(self.money_in_accounts):
                report_percent_done((row_num / float(work_to_do)) * 5.0 + 90.0)
                accpr = CashFlow.account_in_alist(acc, self.money_in_alist)
                current_row_style = 'normal-row' if row_num % 2 else 'alternate-row'
                if show_full_names:
                    accnm = acc.get_full_name()
                else:
                    accnm = acc.get_name()
                colval = accpr[1].sum(report_currency, exchange_fn)
                new_table.append(TR(TD(ANCHOR(accnm, _href=account_anchor_text(acc))),
                                    TD(to_currency_string(colval), _class='number-cell'), _class=current_row_style))
            colval = self.money_in_collector.sum(report_currency, exchange_fn)
            new_table.append(TR(TD("Money In", _class="text-cell"), TD(to_currency_string(colval),
                                                                       _class='total-number-cell'),
                                _class='grand-total'))

            new_table.append(TR(TD(HR(), _rowspan=1, _colspan=2)))
            new_table.append(TR(TD("Money out of selected accounts goes to"), TD(), _class='primary-subheading'))

            work_to_do = len(self.money_out_alist)

            for row_num, acc in enumerate(self.money_out_accounts):
                report_percent_done((row_num / float(work_to_do)) * 5.0 + 90.0)
                accpr = CashFlow.account_in_alist(acc, self.money_out_alist)
                current_row_style = 'normal-row' if row_num % 2 else 'alternate-row'
                if show_full_names:
                    accnm = acc.get_full_name()
                else:
                    accnm = acc.get_name()
                colval = accpr[1].sum(report_currency, exchange_fn)
                new_table.append(TR(TD(ANCHOR(accnm, _href=account_anchor_text(acc))),
                                    TD(to_currency_string(colval), _class='number-cell'),
                                    _class=current_row_style)
                                 )
            colval = self.money_out_collector.sum(report_currency, exchange_fn)
            new_table.append(TR(TD("Money Out", _class="text-cell"),
                                TD(to_currency_string(colval), _class='total-number-cell'),
                                _class='grand-total')
                             )

            new_table.append(TR(TD(HR(), _rowspan=1, _colspan=2)))
            colval = money_diff_collector.sum(report_currency, exchange_fn)
            new_table.append(TR(TD("Difference", _class="text-cell"),
                                TD(to_currency_string(colval), _class='total-number-cell'),
                                _class='grand-total')
                             )

        report_finished()
        return document

    def export(self, report_obj, export_types, filename):
        pass

    def generate_options(self):
        super().generate_options()
        add_option = self.options.register_option
        OptionUtils.add_date_interval(self.options, pagename_general, optname_from_date, optname_to_date, "a")
        OptionUtils.add_currency(self.options, pagename_general, optname_report_currency, "b")
        OptionUtils.add_price_source(self.options, pagename_general, optname_price_source, "c", "pricedb-nearest")
        add_option(
            SimpleBooleanOption(pagename_general, optname_show_rates, "d", "Show the exchange rates used.", False))
        add_option(SimpleBooleanOption(pagename_general, optname_show_full_names, "e",
                                       "Show full account names (including parent accounts).", True))

        add_option(SimpleBooleanOption(pagename_general, optname_all_location, "f", "Use all locations", True))
        add_option(ReferenceOption(pagename_general, optname_location, "g", "Select a location", None, Location,
                                   LocationDialog))
        OptionUtils.add_account_selection(self.options, pagename_accounts, optname_display_depth,
                                          optname_show_subaccounts, optname_accounts, "a", 2,
                                          lambda: filter_accountlist_type(
                                              [AccountType.BANK, AccountType.CASH, AccountType.CURRENT_ASSET,
                                               AccountType.FIXED_ASSET, AccountType.STOCK,
                                               AccountType.MUTUAL,
                                               ],
                                              Session.get_current_root().get_descendants_sorted()),
                                          False)
        add_option(SimpleBooleanOption(pagename_accounts, optname_include_trading_accounts,
                                       "b", "Include transfers to and from Trading Accounts in the report.", False))
        self.options.set_default_section(pagename_general)


CashFlow()
