from typing import Tuple

from gasstation.utilities.ui import *
from gasstation.views.reports.report import Report, ReportTemplate
from libgasstation.core.helpers import *
from ..utilities import *

register_report_guid = "22104e02654c4adba844ee75a3f8d173"


def date_col(columns_used):
    return columns_used[0]


def num_col(columns_used):
    return columns_used[1]


def description_col(columns_used):
    return columns_used[2]


def memo_col(columns_used):
    return columns_used[3]


def account_col(columns_used):
    return columns_used[4]


def shares_col(columns_used):
    return columns_used[5]


def price_col(columns_used):
    return columns_used[6]


def amount_single_col(columns_used):
    return columns_used[7]


def debit_col(columns_used):
    return columns_used[8]


def credit_col(columns_used):
    return columns_used[9]


def balance_col(columns_used):
    return columns_used[10]


def value_single_col(columns_used):
    return columns_used[11]


def value_debit_col(columns_used):
    return columns_used[12]


def value_credit_col(columns_used):
    return columns_used[13]


def lot_col(columns_used):
    return columns_used[14]


def num_used_cols(columns_used):
    return len(list(filter(lambda a: a, columns_used)))


class RegisterReport(ReportTemplate):
    def export(self, report_obj, export_types, filename):
        pass

    def __init__(self):
        super().__init__(version=1, name="Register", guid=register_report_guid, in_menu=False)

    @staticmethod
    def make_heading_list(column_vector, debit_string, credit_string, amount_string,
                          multi_rows, action_for_num, ledger_type):
        hlist = []
        if date_col(column_vector):
            hlist.append(TH("Date"))
        if num_col(column_vector):
            if action_for_num:
                d = "T_Num" if ledger_type else "Num/Action"
            else:
                d = "Num"
            hlist.append(TH(d))
        if description_col(column_vector):
            hlist.append(TH("Description"))
        if memo_col(column_vector):
            hlist.append(TH("Memo"))
        if account_col(column_vector):
            hlist.append(TH("Account" if multi_rows else "Transfer"))
        if shares_col(column_vector):
            hlist.append(TH("Shares"))
        if lot_col(column_vector):
            hlist.append(TH("Lot"))
        if price_col(column_vector):
            hlist.append(TH("Price"))
        if amount_single_col(column_vector):
            hlist.append(TH(amount_string))
        if debit_col(column_vector):
            hlist.append(TH(debit_string))
        if credit_col(column_vector):
            hlist.append(TH(credit_string))
        if value_single_col(column_vector):
            hlist.append(TH("Value"))
        if value_debit_col(column_vector):
            hlist.append(TH("Debit Value"))
        if value_credit_col(column_vector):
            hlist.append(TH("Credit Value"))
        if balance_col(column_vector):
            hlist.append(TH("Balance"))
        return hlist

    def build_column_used(self, options):
        def opt_value(section, name):
            option = options.lookup_option(section, name)
            if option is not None:
                return option.get_value()

        col_vector = [False, False, False, False, False, False, False, False, False, False, False, False, False, False,
                      False]
        p = 1

        def set_col(used, index):
            nonlocal p
            if used:
                col_vector[index] = p
                p += 1
            else:
                col_vector[index] = False

        set_col(opt_value("Display", "Date"), 0)
        set_col(opt_value("Display", "Num") if opt_value("Display", "Num") is not None else opt_value("Display",
                                                                                                      "Num/Action"), 1)
        if opt_value("Display", "Memo") or opt_value("__reg", "journal") or opt_value("Display", "Description") or \
                opt_value("__reg", "double"):
            set_col(opt_value("Display", "Description"), 2)
        set_col(False if opt_value("__reg", "journal") else opt_value("Display", "Memo"), 3)
        set_col(opt_value("Display", "Account"), 4)
        set_col(opt_value("Display", "Shares"), 5)
        set_col(opt_value("Display", "Lot"), 14)
        set_col(opt_value("Display", "Price"), 6)
        amount_setting = opt_value("Display", "Amount")
        if amount_setting == "single":
            set_col(True, 7)
        else:
            set_col(True, 8)
            set_col(True, 9)
        if opt_value("Display", "Value"):
            if amount_single_col(col_vector):
                set_col(True, 11)
            else:
                set_col(True, 12)
                set_col(True, 13)
        set_col(opt_value("Display", "Running Balance"), 10)
        return col_vector

    @staticmethod
    def add_split_row(table: TBODY, split, column_vector, row_style, transaction_info, split_info, action_for_num,
                      ledger_type, double, memo, description, total_collector):
        row_contents = []
        parent = split.get_transaction()
        account = split.get_account()
        currency = account.get_commodity()
        trans_currency = parent.get_currency()
        damount = split.get_amount()
        dvalue = split.get_value()
        split_abs_amount = sprintamount(abs(damount),
                                        PrintAmountInfo.commodity(currency, True))
        split_abs_value = sprintamount(abs(dvalue),
                                       PrintAmountInfo.commodity(trans_currency, True))
        if date_col(column_vector):
            row_contents.append(TD(print_datetime(parent.get_post_date()) if transaction_info else " ",
                                   _class="date-cell"))
        if num_col(column_vector):
            if transaction_info:
                if action_for_num and ledger_type:
                    row_contents.append(TD(gs_get_num_action(parent, None) or "", _class="text-cell"))
                else:
                    row_contents.append(TD(gs_get_num_action(parent, split) or "", _class="text-cell"))
            elif split_info:
                row_contents.append(TD(gs_get_num_action(None, split) or "", _class="text-cell"))

        if description_col(column_vector):
            d = ""
            if transaction_info:
                if description:
                    d = parent.get_description() or ""
                elif split_info and memo:
                    d = split.get_memo() or ""
            row_contents.append(TD(d, _class="text-cell"))
        if memo_col(column_vector):
            row_contents.append(TD(split.get_memo() or "" if transaction_info else " ", _class="text-cell"))
        if account_col(column_vector):
            if not split_info:
                row_contents.append(TD(" ", _class="text-cell"))
            elif not transaction_info:
                row_contents.append(TD(account.get_full_name() or "", _class="text-cell"))
            else:
                c = len(split.get_transaction())
                if c == 2:
                    row_contents.append(TD(split.get_other_split().get_account().get_full_name(),
                                           _class="text-cell"))
                elif c == 1:
                    row_contents.append(TD(" ", _class="text-cell"))
                else:
                    row_contents.append(TD("-- Split Transaction --", _class="text-cell"))

        if shares_col(column_vector):
            row_contents.append(TD(sprintamount(split.get_amount(),
                                                PrintAmountInfo.default()) if split_info else "",
                                   _class="text-cell"))
        if lot_col(column_vector):
            row_contents.append(TD(split.get_lot().get_title() if split_info and split.get_lot() is not None else "#",
                                   _class="text-cell"))
        if price_col(column_vector):
            row_contents.append(TD(sprintamount(split.get_share_price(),
                                                PrintAmountInfo.price(trans_currency)) if split_info else "",
                                   _class="text-cell"))
        if amount_single_col(column_vector):
            if split_info:
                row_contents.append(TD(sprintamount(damount * -1 if reverse else damount,
                                                    PrintAmountInfo.commodity(trans_currency, True)),
                                       _class="number-cell"))
        if debit_col(column_vector):
            if split_info and damount > 0:
                row_contents.append(TD(Anchors.split(split, split_abs_amount), _class="number-cell"))
            else:
                row_contents.append(TD(" ", _class="number-cell"))
        if credit_col(column_vector):
            if split_info and not damount > 0:
                row_contents.append(TD(Anchors.split(split, split_abs_amount), _class="number-cell"))
            else:
                row_contents.append(TD(" ", _class="number-cell"))
        if value_single_col(column_vector):
            if split_info:
                row_contents.append(TD(sprintamount(dvalue * -1 if reverse else dvalue,
                                                    PrintAmountInfo.commodity(trans_currency, True)),
                                       _class="number-cell"))
        if value_debit_col(column_vector):
            if split_info and dvalue > 0:
                row_contents.append(TD(Anchors.split(split, split_abs_value), _class="number-cell"))
            else:
                row_contents.append(TD(" ", _class="number-cell"))
        if value_credit_col(column_vector):
            if split_info and not dvalue > 0:
                row_contents.append(TD(Anchors.split(split, split_abs_value), _class="number-cell"))
            else:
                row_contents.append(TD(" ", _class="number-cell"))
        if balance_col(column_vector):
            if transaction_info:
                if ledger_type:
                    v = total_collector.getpair(currency, False)[1]
                else:
                    v = split.get_balance()
                    if gs_reverse_balance(account):
                        v = v * -1

                row_contents.append(TD(Anchors.split(split,
                                                     sprintamount(v, PrintAmountInfo.commodity(currency, True))),
                                       _class="number-cell"))
            else:
                row_contents.append(TD(" ", _class="number-cell"))
        table.append(TR(*row_contents, _class=row_style))
        if double and transaction_info:
            if num_col(column_vector) or description_col(column_vector):
                count = 0
                row_contents = []
                if date_col(column_vector):
                    count += 1
                    row_contents.append(TD(" "))
                if num_col(column_vector) and description_col(column_vector):
                    count += 1
                    row_contents.append(TD(gs_get_num_action(parent, None) or ""
                                           if action_for_num and ledger_type else "", _class="text-cell"))
                if description_col(column_vector):
                    d = TD(parent.get_notes() or "" if parent else " ", _class="text-cell",
                           _colspan=num_used_cols(column_vector) - count)
                    row_contents.append(d)
                table.append(TR(*row_contents, _class=row_style))

    def make_split_table(self, splits, options, debit_string, credit_string, amount_string):
        def opt_value(section, name):
            option = options.lookup_option(section, name)
            if option is not None:
                return option.get_value()

        def add_subtotal_row(label, leader, tfoot, used_columns, subtotal_collector: CommodityCollector,
                             subtotal_style, value):
            currency_totals = []
            subtotal_collector.format(lambda com, value: currency_totals.append((com, value)))
            single_col = value_single_col(used_columns) if value else amount_single_col(used_columns)
            credit_co = value_credit_col(used_columns) if value else credit_col(used_columns)
            debit_co = value_debit_col(used_columns) if value else debit_col(used_columns)

            def colspan(mon: Tuple[Commodity, Decimal]):
                if single_col:
                    return single_col - 2
                elif mon[1] < 0:
                    return credit_co - 2
                else:
                    return debit_co - 2

            def display_subtotal(mon: Tuple[Commodity, Decimal]):
                value = mon[1]
                if single_col:
                    if leader is not None:
                        value = value * -1
                if mon[1] < 0:
                    value = value * -1
                return sprintamount(value, PrintAmountInfo.commodity(mon[0], True))

            if single_col or credit_co or debit_co:
                for monetary in currency_totals:
                    tfoot.append(TR(TD(label, _class="total-label-cell"), TD("", _class="total-label-cell",
                                                                             _colspan=colspan(monetary)),
                                    TD(display_subtotal(monetary), _class="total-number-cell"),
                                    *(TD() * (num_used_cols(used_columns) - (colspan(monetary) + 2))),
                                    _class=subtotal_style))

        def accumulate_totals(split, total_amount, total_value, debit_amount, debit_value, credit_amount, credit_value):
            parent = split.get_transaction()
            account = split.get_account()
            split_currency = account.get_commodity()
            split_amount = split.get_amount()
            trans_currency = parent.get_currency()
            split_value = split.get_value()
            if split_amount > 0:
                debit_amount.add(split_currency, split_amount)
            else:
                credit_amount.add(split_currency, split_amount)
            if split_value > 0:
                debit_value.add(split_currency, split_value)
            else:
                credit_value.add(split_currency, split_value)
            total_amount.add(split_currency, split_amount)
            total_value.add(trans_currency, split_value)

        def splits_leader(splits):
            accounts = list(map(lambda a: a.get_account(), splits))
            return accounts[0] if any(accounts) and len(accounts) == 2 and accounts[0] == accounts[1] else None

        journal = opt_value("__reg", "journal")
        multi_rows = journal
        ledger_type = opt_value("__reg", "ledger-type")
        double = opt_value("__reg", "double")
        show_totals = opt_value("Display", "Totals")

        table = TABLE()
        used_columns = self.build_column_used(options)
        leader = splits_leader(splits)
        total_collector = CommodityCollector()
        debit_collector = CommodityCollector()
        credit_collector = CommodityCollector()
        total_value = CommodityCollector()
        debit_value = CommodityCollector()
        credit_value = CommodityCollector()
        action_for_num = Session.get_current_book().get_option(["Accounts", "Use Split Action Field for Number"])
        work_to_do = len(splits)
        headings = self.make_heading_list(used_columns, debit_string, credit_string, amount_string,
                                          multi_rows, action_for_num, ledger_type)
        table.append(THEAD(TR(*headings)))
        tbody = TBODY()
        disp_memo = opt_value("Display", "Memo")
        disp_desc = opt_value("Display", "Description")
        for i, split in enumerate(splits):
            if split.get_account() is None:
                continue
            current_row_style = 'normal-row' if math.fmod(i + 1, 2) or multi_rows else 'alternate-row'
            for sp in split.get_transaction().get_splits() if multi_rows else [split]:
                accumulate_totals(sp, total_collector, total_value, debit_collector, debit_value, credit_collector,
                                  credit_value)
            self.add_split_row(tbody, split, used_columns, current_row_style, True, not multi_rows, action_for_num,
                               ledger_type, double, disp_memo, disp_desc, total_collector)
            if multi_rows:
                for sp in split.get_transaction().get_splits():
                    self.add_split_row(tbody, sp, used_columns, "alternate-row", False, True, action_for_num,
                                       ledger_type, False, disp_memo, disp_desc, total_collector)
            report_percent_done((100 * i) / work_to_do)
        table.append(tbody)
        if any(splits):
            tfoot = TFOOT()
            if show_totals:
                add_subtotal_row("Total Debits", leader, tfoot, used_columns, debit_collector, "grand-total", False)
                add_subtotal_row("Total Credits", leader, tfoot, used_columns, credit_collector, "grand-total", False)
                add_subtotal_row("Total Value Debits", leader, tfoot, used_columns, debit_value, "grand-total", True)
                add_subtotal_row("Total Value Credits", leader, tfoot, used_columns, credit_value, "grand-total", True)
            if ledger_type:
                add_subtotal_row("Net Change", leader, tfoot, used_columns, total_collector, "grand-total", False)
            table.append(tfoot)
        table.set_style("table", {"border": 1, "cellspacing": 0, "cellpadding": 4})
        report_percent_done(100)
        return table

    def render(self, report_obj):
        def opt_value(section, name):
            option = report_obj.options.lookup_option(section, name)
            if option is not None:
                return option.get_value()

        document = HtmlDocument()
        query_i = opt_value("__reg", "query")
        journal = opt_value("__reg", "journal")
        debit_string = opt_value("__reg", "debit-string")
        credit_string = opt_value("__reg", "credit-string")
        title = opt_value("General", "Title")
        report_starting("Register")
        query = query_i.copy()
        query.set_book(Session.get_current_book())
        splits = query.get_splits_unique_trans() if journal else query.run()
        table = self.make_split_table(splits, report_obj.options, debit_string, credit_string, "Amount")
        document.set_title(title)
        document.add_object(table)
        query.destroy()
        report_finished()
        return document

    def generate_options(self):
        super().generate_options()
        reg = self.options.register_option
        reg(QueryOption("__reg", "query", None))
        reg(InternalOption("__reg", "journal", False))
        reg(InternalOption("__reg", "ledger-type", None))
        reg(InternalOption("__reg", "double", False))
        reg(InternalOption("__reg", "debit-string", "Debit"))
        reg(InternalOption("__reg", "credit-string", "Credit"))
        reg(StringOption("General", "Title", "a", "The title of the report.", "Register Report"))
        reg(SimpleBooleanOption("Display", "Date", "b", "Display the date?", True))
        if Session.get_current_book().use_split_action_for_num_field():
            reg(SimpleBooleanOption("Display", "Num/Action", "c", "Display the check number/action?", True))
        reg(SimpleBooleanOption("Display", "Num", "c", "Display the check number?", True))
        reg(SimpleBooleanOption("Display", "Description", "d", "Display the description?", True))
        reg(SimpleBooleanOption("Display", "Memo", "e", "Display the memo?", True))
        reg(SimpleBooleanOption("Display", "Account", "g", "Display the account?", True))
        reg(SimpleBooleanOption("Display", "Shares", "ha", "Display the number of shares?", False))
        reg(SimpleBooleanOption("Display", "Lot", "hb", "Display the name of lot the shares are in?", False))
        reg(SimpleBooleanOption("Display", "Price", "hc", "Display the shares price?", False))
        reg(MultiChoiceOption("Display", "Amount", "ia", "Display the amount?", "double",
                              [("single", "Single", "Single Column Display."),
                               ("double", "Double", "Two Column Display.")]))
        reg(SimpleBooleanOption("Display", "Value", "ib", "Display the value in transaction currency?", False))
        reg(SimpleBooleanOption("Display", "Running Balance", "k", "Display a running balance?", True))
        reg(SimpleBooleanOption("Display", "Totals", "l", "Display the totals?", True))
        self.options.set_default_section("General")

    @classmethod
    def internal(cls, invoice, query, journal, ledger_type, double, title, debit_string, credit_string):
        template = Report.find_template(register_report_guid)
        if template is not None and template.options is None:
            template.generate_options()
        options = template.get_options()
        query_op = options.lookup_option("__reg", "query")
        journal_op = options.lookup_option("__reg", "journal")
        ledger_type_op = options.lookup_option("__reg", "ledger-type")
        double_op = options.lookup_option("__reg", "double")
        title_op = options.lookup_option("General", "Title")
        debit_op = options.lookup_option("__reg", "debit-string")
        credit_op = options.lookup_option("__reg", "credit-string")
        account_op = options.lookup_option("Display", "Account")
        if invoice:
            journal = False
            account_op.set_value(None)
        query_op.set_value(query)
        journal_op.set_value(journal)
        ledger_type_op.set_value(ledger_type)
        double_op.set_value(double)
        title_op.set_value(title)
        debit_op.set_value(debit_string)
        credit_op.set_value(credit_string)
        r = Report(template)
        return r.id


RegisterReport()
