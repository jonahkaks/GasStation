from gasstation.utilities.options import *
from ..commodity_utilities import *
from ..report import *

reportname = "Trial Balance"
optname_report_title = "Report Title"
opthelp_report_title = "Title for this report."

optname_party_name = "Company name"
opthelp_party_name = "Name of company/individual."

optname_start_date = "Start of Adjusting/Closing"
optname_end_date = "Date of Report"
optname_report_variant = "Report variation"
opthelp_report_variant = "Kind of trial balance to generate."

optname_accounts = "Accounts"
opthelp_accounts = "Report on these accounts."
optname_depth_limit = "Levels of Subaccounts"
opthelp_depth_limit = "Maximum number of levels in the account tree displayed."
pagename_merchandising = "Merchandising"
optname_gross_adjustment_accounts = "Gross adjustment accounts."
opthelp_gross_adjustment_accounts = "Do not net, but show gross debit/credit adjustments to these accounts." \
                                    " Merchandising businesses will normally select their product accounts here."
optname_income_summary_accounts = "Income summary accounts"
opthelp_income_summary_accounts = "Adjustments made to these accounts are gross adjusted (see above) in the Adjustments," \
                                  " Adjusted Trial Balance, and Income Statement columns. Mostly useful for merchandising" \
                                  " businesses."
pagename_entries = "Entries"
optname_adjusting_pattern = "Adjusting Entries pattern"
opthelp_adjusting_pattern = "Any text in the Description column which identifies adjusting entries."
optname_adjusting_casing = "Adjusting Entries pattern is case_sensitive"
opthelp_adjusting_casing = "Causes the Adjusting Entries Pattern match to be case_sensitive."
optname_adjusting_regexp = "Adjusting Entries Pattern is regular expression"
opthelp_adjusting_regexp = "Causes the Adjusting Entries Pattern to be treated as a regular expression."
optname_closing_pattern = "Closing Entries pattern"
opthelp_closing_pattern = "Any text in the Description column which identifies closing entries."
optname_closing_casing = "Closing Entries pattern is case_sensitive"
opthelp_closing_casing = "Causes the Closing Entries Pattern match to be case_sensitive."
optname_closing_regexp = "Closing Entries Pattern is regular expression"
opthelp_closing_regexp = "Causes the Closing Entries Pattern to be treated as a regular expression."
optname_show_zb_accts = "Include accounts with zero total balances"
opthelp_show_zb_accts = "Include accounts with zero total (recursive) balances in this report."
optname_account_links = "Display accounts as hyperlinks"
opthelp_account_links = "Shows each account in the table as a hyperlink to its register window."
pagename_commodities = "Commodities"
optname_report_commodity = "Report's currency"
optname_price_source = "Price Source"
optname_show_foreign = "Show Foreign Currencies"
opthelp_show_foreign = "Display any foreign currency amount in an account."
optname_show_rates = "Show Exchange Rates"
opthelp_show_rates = "Show the exchange rates used."


class TrialBalance(ReportTemplate):
    def __init__(self):
        super().__init__(version=1, name="Trial Balance", guid="216cd0cf6931453ebcce85415aba7082",
                         menu_name="Trial Balance",
                         menu_path=menu_name_income_expense)

    def render(self, report_obj):
        doc = HtmlDocument()
        table = TABLE()
        table.append(THEAD(TR(TD("Account Name"), TD("Debit"), TD("Credit"))))
        doc.add_object(table)
        return doc

    def export(self, report_obj, export_types, filename):
        pass

    def generate_options(self):
        super(TrialBalance, self).generate_options()
        book = Session.get_current_book()
        add_option = self.options.register_option
        add_option(StringOption("General", optname_report_title, "a", opthelp_report_title, reportname))
        add_option(StringOption("General", optname_party_name, "b", opthelp_party_name,
                                book.get_option(["Business", "Company Name"])))
        OptionUtils.add_date_interval(self.options, pagename_general, optname_start_date, optname_end_date, "c")
        add_option(MultiChoiceOption(pagename_general, optname_report_variant,
                                     "d", opthelp_report_variant, "current",
                                     [("current", "Current Trial Balance",
                                       "Uses the exact balances in the general journal"),
                                      (
                                          "pre_adj", "Pre_adjustment Trial Balance",
                                          "Ignores Adjusting/Closing entries"),
                                      ("work_sheet", "Work Sheet", "Creates a complete end_of_period work sheet")]))
        add_option(AccountListOption(pagename_accounts, optname_accounts, "a", opthelp_accounts,
                                     lambda: filter_accountlist_type(
                                         [AccountType.BANK, AccountType.CASH, AccountType.CREDIT,
                                          AccountType.CURRENT_ASSET, AccountType.FIXED_ASSET,
                                          AccountType.CURRENT_LIABILITY,AccountType.LONG_TERM_LIABILITY,
                                          AccountType.STOCK, AccountType.MUTUAL,
                                          AccountType.CURRENCY, AccountType.PAYABLE,
                                          AccountType.RECEIVABLE, AccountType.EQUITY,
                                          AccountType.INCOME, AccountType.EXPENSE,
                                          AccountType.TRADING],
                                         Session.get_current_root().get_descendants_sorted()),
                                     False, True))

        OptionUtils.add_account_levels(self.options, pagename_accounts, optname_depth_limit, "b", opthelp_depth_limit, 1)
        add_option(AccountListOption(pagename_merchandising, optname_gross_adjustment_accounts, "c",
                                     opthelp_gross_adjustment_accounts, lambda: [], False, True))
        add_option(AccountListOption(pagename_merchandising, optname_income_summary_accounts, "d",
                                     opthelp_income_summary_accounts, lambda: [], False, True))

        OptionUtils.add_currency(self.options, pagename_commodities, optname_report_commodity, "a")
        OptionUtils.add_price_source(self.options, pagename_commodities, optname_price_source, "b", "pricedb-nearest")

        add_option(SimpleBooleanOption(pagename_commodities, optname_show_foreign, "c", opthelp_show_foreign, False))

        add_option(SimpleBooleanOption(pagename_commodities, optname_show_rates,
                                       "d", opthelp_show_rates, False))
        add_option(StringOption(
            pagename_entries, optname_adjusting_pattern,
            "a", opthelp_adjusting_pattern, "Adjusting Entries"))
        add_option(SimpleBooleanOption(pagename_entries, optname_adjusting_casing,
                                       "b", opthelp_adjusting_casing, False))
        add_option(SimpleBooleanOption(pagename_entries, optname_adjusting_regexp,
                                       "c", opthelp_adjusting_regexp, False))
        add_option(StringOption(
            pagename_entries, optname_closing_pattern,
            "d", opthelp_closing_pattern, "Closing Entries"))
        add_option(SimpleBooleanOption(pagename_entries, optname_closing_casing,
                                       "e", opthelp_closing_casing, False))
        add_option(SimpleBooleanOption(pagename_entries, optname_closing_regexp,
                                       "f", opthelp_closing_regexp, False))
        add_option(SimpleBooleanOption(pagename_display, optname_account_links,
                                       "e", opthelp_account_links, True))
        self.options.set_default_section(pagename_display)


TrialBalance()
