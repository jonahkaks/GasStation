from gasstation.utilities.options import *
from gasstation.utilities.ui import *
from ..commodity_utilities import *
from ..report import *

menuname_income = "Income Piechart"
menuname_expense = "Expense Piechart"
menuname_assets = "Asset Piechart"
menuname_securities = "Security Piechart"
menuname_liabilities = "Liability Piechart"

menutip_income = "Shows a piechart with the Income per given time interval"
menutip_expense = "Shows a piechart with the Expenses per given time interval"
menutip_assets = "Shows a piechart with the Assets balance at a given time"
menutip_securities = "Shows a piechart with distribution of assets over securities"
menutip_liabilities = "Shows a piechart with the Liabilities balance at a given time"

reportname_income = "Income Accounts"
reportname_expense = "Expense Accounts"
reportname_assets = "Assets"
reportname_securities = "Securities"
reportname_liabilities = "Liabilities"

optname_from_date = "Start Date"
optname_to_date = "End Date"
optname_report_currency = "Report's currency"
optname_price_source = "Price Source"

optname_accounts = "Accounts"
optname_levels = "Show Accounts until level"

optname_fullname = "Show long names"
optname_show_total = "Show Totals"
optname_show_percent = "Show Percents"
optname_slices = "Maximum Slices"
optname_plot_width = "Plot Width"
optname_plot_height = "Plot Height"
optname_sort_method = "Sort Method"

optname_averaging = "Show Average"
opthelp_averaging = "Select whether the amounts should be shown over the full time period or rather as the average " \
                    "e.g. per month. "


class AccountPieCharts(ReportTemplate):
    def __init__(self, name, report_guid, account_types, income_expense, depth_based, menuname, menutip,
                 reverse_balance):
        super().__init__(version=1, name=name, menu_name=menuname, guid=report_guid,
                         menu_path=menu_name_income_expense if income_expense else menu_name_asset_liability,
                         menu_tooltip=menutip)
        self.account_types = account_types
        self.depth_based = depth_based
        self.reverse_balance = reverse_balance
        self.do_intervals = income_expense

    def export(self, report_obj, export_types, filename):
        pass

    @classmethod
    def sum_securities(cla, account_balance, show_acct, work_to_do, tree_depth, work_done, current_depth, accts):
        table = {}
        pass

    @classmethod
    def traverse_accounts(cls, account_balance, show_acct, work_to_do, tree_depth, work_done, current_depth, accts):
        if current_depth < tree_depth:
            cur_work_done = work_done
            res = []
            if not any(accts):
                return cur_work_done, res
            for cur in accts:
                report_percent_done(100 * (cur_work_done / work_to_do))
                subaccts_work, subaccts, = cls.traverse_accounts(account_balance, show_acct, work_to_do,
                                                                 tree_depth, cur_work_done,
                                                                 current_depth + 1,
                                                                 cur.get_children_sorted())
                res.extend(subaccts)
                if show_acct(cur):
                    res.append((account_balance(cur, False), cur))
                cur_work_done = subaccts_work + 1
            return cur_work_done, res
        else:
            new_accts = []
            for a in filter(show_acct, accts):
                work_done += 1
                report_percent_done(100 * (work_done / work_to_do))
                new_accts.append((account_balance(a, True), a))
            return work_done, new_accts

    def render(self, report_obj):
        get_option = lambda section, name: report_obj.options.lookup_option(section, name).get_value()
        report_starting(self.name)
        to_date = DateOption.absolute_time(get_option(pagename_general, optname_to_date))
        from_date = DateOption.absolute_time(get_option(pagename_general, optname_from_date)) \
            if self.do_intervals else datetime.date.today()
        accounts = get_option(pagename_accounts, optname_accounts)
        account_levels = get_option(pagename_accounts, optname_levels) if self.depth_based else "all"
        report_currency: Commodity = get_option(pagename_general, optname_report_currency)
        price_source = get_option(pagename_general, optname_price_source)
        report_title = get_option(pagename_general, optname_reportname)
        averaging_selection = get_option(pagename_general, optname_averaging) if self.do_intervals else None
        show_fullname = get_option(pagename_display, optname_fullname)
        show_total = get_option(pagename_display, optname_show_total)
        show_percent = get_option(pagename_display, optname_show_percent)
        max_slices = int(get_option(pagename_display, optname_slices))
        height = get_option(pagename_display, optname_plot_height)
        width = get_option(pagename_display, optname_plot_width)
        sort_method = get_option(pagename_display, optname_sort_method)
        reverse_balance = get_option("__report", "reverse-balance?")
        document = HtmlDocument()
        fix_signs = lambda combined: map(lambda p: (p[0] * -1 if reverse_balance else p[0], p[1]), combined)
        show_acct = lambda a: a in accounts
        topl_accounts = filter_accountlist_type(self.account_types, Session.get_current_root().get_children_sorted())
        exchange_fn = case_exchange_fn(price_source, report_currency, to_date)
        averaging_multiplier = 1

        def profit_fn(account, subaccts):
            if self.do_intervals:
                return account_get_comm_balance_interval(account, from_date, to_date, subaccts)
            else:
                return account_get_comm_balance_at_date(account, to_date, subaccts)

        tree_depth = get_current_account_tree_depth() if account_levels == "all" else int(account_levels)

        def collector_to_double(c:CommodityCollector):
            return c.sum(report_currency, exchange_fn)[1] * averaging_multiplier

        account_balance = lambda a, subaccts: collector_to_double(profit_fn(a, subaccts))
        if self.depth_based:
            get_data = self.traverse_accounts
        else:
            get_data = self.sum_securities
        display_name = self.display_name_accounts if self.depth_based else self.display_name_security

        def count_accounts(current_depth, accts):
            if current_depth < tree_depth:
                return sum(map(lambda acc: count_accounts(current_depth + 1, acc.get_children()) + 1,
                               filter(show_acct, accts)))
            else:
                return len(list(filter(show_acct, accts)))

        work_to_do = count_accounts(1, topl_accounts)
        if any(accounts):
            combined = list(filter(lambda p: p[0] > 0, fix_signs(get_data(account_balance, show_acct,
                                                                          work_to_do, tree_depth, 0, 1,
                                                                          topl_accounts)[1])))
            combined.sort(key=cmp_to_key(lambda a, b: self.sort_comparator_accounts(a, b, sort_method, show_fullname)
            if self.depth_based else self.sort_comparator_security(a, b, sort_method, show_fullname)
                                         ))
            if len(combined) > max_slices:
                others = combined[max_slices - 1:]
                combined = combined[:max_slices] + [(sum(list(zip(*others))[0]), "Other")]
            if any(combined):
                chart = HtmlChart(currency_iso=report_currency.get_mnemonic(),
                                  currency_symbol=report_currency.get_nice_symbol())
                chart.set_type("pie")
                chart.set_title([report_title, "".join(["%s to %s" % (print_datetime(from_date),
                                                                      print_datetime(to_date)) if self.do_intervals else
                                                        "Balance at %s" % print_datetime(to_date)])])
                chart.set_width(width)
                chart.set_height(height)
                chart.set_axes_display(False)
                data = list(zip(*combined))[0]
                total = sum(data)
                data = list(map(lambda a: float(a), data))
                chart.add_data_series("", data, color=chart.get_colors(data))
                legend_labels = []
                for a, l in combined:
                    st = (l if isinstance(l, str) else display_name(show_fullname, l)) + " - "
                    if show_total:
                        st += sprintamount(a, PrintAmountInfo.commodity(report_currency, True))
                    if show_percent:
                        st += "   ({:.2f} %)".format((a / total) * 100)
                    legend_labels.append(st)
                chart.set_data_labels(legend_labels)
                document.add_object(chart)
            else:
                document.add_object(make_empty_data_warning(report_title, report_obj.get_id()))
        else:
            document.add_object(make_no_account_warning(report_title, report_obj.get_id()))
        report_finished()
        return document

    @staticmethod
    def display_name_accounts(show_fullname, acc):
        if show_fullname:
            return acc.get_full_name()
        else:
            return acc.get_name()

    @staticmethod
    def display_name_security(show_fullname, sec: Commodity):
        if show_fullname:
            return sec.get_full_name()
        else:
            return sec.get_mnemonic()

    @staticmethod
    def sort_comparator_accounts(a, b, sort_method, show_fullname):
        if sort_method == "acct-code":
            ac = a[1].get_code()
            bc = b[1].get_code()
            return -1 if ac < bc else 1 if ac > bc else 0
        elif sort_method == "alphabetical":
            af = AccountPieCharts.display_name_accounts(show_fullname, a[1])
            bf = AccountPieCharts.display_name_accounts(show_fullname, b[1])
            return  -1 if af < bf else 1 if af > bf else 0
        else:
            return a[0] - b[0]

    @staticmethod
    def sort_comparator_security(a, b, sort_method, show_fullname):
        if sort_method == "acct-code":
            ac = a[1].get_mnemonic()
            bc = b[1].get_mnemonic()
            return -1 if ac < bc else 1 if ac > bc else 0
        elif sort_method == "alphabetical":
            af = AccountPieCharts.display_name_security(show_fullname, a[1])
            bf = AccountPieCharts.display_name_security(show_fullname, b[1])
            return  -1 if af < bf else 1 if af > bf else 0
        else:
            return a[0] - b[0]

    def generate_options(self):
        super().generate_options()
        add_option = self.options.register_option
        add_option(InternalOption("__report", "reverse-balance?", self.reverse_balance))
        if self.do_intervals:
            OptionUtils.add_date_interval(self.options, pagename_general, optname_from_date, optname_to_date, "a")
        else:
            OptionUtils.add_report_date(self.options, pagename_general, optname_to_date, "a")
        self.options.lookup_option(pagename_general, optname_stylesheet).set_value("Default")
        OptionUtils.add_currency(self.options, pagename_general, optname_report_currency, "b")
        OptionUtils.add_price_source(self.options, pagename_general, optname_price_source, "c", "pricedb-nearest")
        if self.do_intervals:
            add_option(MultiChoiceOption(pagename_general, optname_averaging, "f", opthelp_averaging, "None",
                                         [("None", "No Averaging", "Just show the amounts, without any averaging."),
                                          ("YearDelta", "Yearly",
                                           "Show the average yearly amount during the reporting period."),
                                          ("MonthDelta", "Monthly", "Show the average monthly amount during the"
                                                                    " reporting period."),
                                          ("WeekDelta", "Weekly", "Show the average weekly amount during the "
                                                                  "reporting period.")]))
        add_option(AccountListOption(pagename_accounts, optname_accounts, "a", "Report on these accounts, if chosen "
                                                                               "account level allows.",
                                     lambda: filter_accountlist_type(self.account_types,
                                                                     Session.get_current_root().get_descendants_sorted()),
                                     lambda accounts: filter_accountlist_type(self.account_types, accounts),
                                     True))
        if self.depth_based:
            OptionUtils.add_account_levels(self.options, pagename_accounts, optname_levels, "b",
                                           "Show accounts to this depth and not further.", 2)
        add_option(SimpleBooleanOption(pagename_display, optname_fullname, "a", "Show the full account name "
                                                                                "in legend?" if self.depth_based else
        "Show the full security name in the legend?", False))
        add_option(SimpleBooleanOption(pagename_display, optname_show_total,
                                       "b", "Show the total balance in legend?", True))
        add_option(SimpleBooleanOption(pagename_display, optname_show_percent,
                                       "b", "Show the percentage in legend?", True))
        add_option(NumberRangeOption(pagename_display, optname_slices, "c", "Maximum number of slices in pie.", 7,
                                     2, 24, 0, 1))

        OptionUtils.add_plot_size(self.options, pagename_display, optname_plot_width, optname_plot_height,
                                  "d", ["percent", 100.0], ["percent", 100.0])

        OptionUtils.add_sort_method(self.options, pagename_display, optname_sort_method, "e", "amount")
        self.options.set_default_section(pagename_general)


def build_report(name, report_guid, account_types, income_expense, depth_based, menuname, menutip, reverse_balance):
    AccountPieCharts(name, report_guid, account_types, income_expense, depth_based, menuname, menutip,
                     reverse_balance)


build_report(reportname_income, "e1bd09b8a1dd49dd85760db9d82b045c",
             [AccountType.INCOME], True, True, menuname_income, menutip_income, True)
build_report(reportname_expense, "9bf1892805cb4336be6320fe48ce5446",
             [AccountType.EXPENSE], True, True, menuname_expense, menutip_expense, False)
build_report(reportname_assets, "5c7fd8a1fe9a4cd38884ff54214aa88a",
             [AccountType.CURRENT_ASSET, AccountType.FIXED_ASSET, AccountType.BANK,
              AccountType.CASH,AccountType.CHECKING, AccountType.SAVINGS,
              AccountType.MONEYMRKT, AccountType.RECEIVABLE,
              AccountType.STOCK, AccountType.MUTUAL, AccountType.CURRENCY],
             False, True, menuname_assets, menutip_assets, False)
build_report(reportname_securities, "e9418ff64f2c11e5b61d1c7508d793ed",
             [AccountType.CURRENT_ASSET, AccountType.FIXED_ASSET, AccountType.BANK,
              AccountType.CASH,AccountType.CHECKING, AccountType.SAVINGS,
              AccountType.MONEYMRKT, AccountType.RECEIVABLE,
              AccountType.STOCK, AccountType.MUTUAL,
              AccountType.CURRENCY], False, False,
             menuname_securities, menutip_securities, True)

build_report(reportname_liabilities, "3fe6dce77da24c66bdc8f8efdea7f9ac",
             [AccountType.CURRENT_LIABILITY,AccountType.LONG_TERM_LIABILITY, AccountType.PAYABLE,
              AccountType.CREDIT, AccountType.CREDITLINE], False,
             True, menuname_liabilities, menutip_liabilities, True)
