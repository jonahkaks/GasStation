# from gasstation.utilities.options import *
# from ..commodity_utilities import *
# from ..report import *
#
# FOOTER_TEXT= TD("WARNING: Foreign currency conversions, and unrealized gains "
#                 "calculations are not confirmed correct. This report may be modified "
#                 "without notice. Bug reports are very welcome at https://bugs.gnucash.org/")
#
#
# optname_startdate ="Start Date"
# optname_enddate ="End Date"
#
# optname_period ="Period duration"
# opthelp_period ="Duration between time periods"
#
# optname_reverse_chrono ="Period order is most recent first"
# opthelp_reverse_chrono ="Period order is most recent first"
#
# optname_dual_columns ="Enable dual columns"
# opthelp_dual_columns ="Selecting this option will enable double_column \
# reporting."
#
# optname_disable_amount_indent ="Disable amount indenting"
# opthelp_disable_amount_indent ="Selecting this option will disable amount indenting, and condense amounts into a single column."
#
# optname_options_summary ="Add options summary"
# opthelp_options_summary ="Add summary of options."
#
# optname_account_full_name ="Account full name instead of indenting"
# opthelp_account_full_name ="Selecting this option enables full account name instead, and disables indenting account names."
#
# optname_accounts ="Accounts"
# opthelp_accounts ="Report on these accounts, if display depth allows."
#
# optname_depth_limit ="Levels of Subaccounts"
# opthelp_depth_limit ="Maximum number of levels in the account tree displayed."
#
# optname_parent_balance_mode ="Parent account amounts include children"
# opthelp_parent_balance_mode ="If this option is enabled, subtotals are \
# displayed within parent amounts, and if parent has own amount, it is displayed on \
# the next row as a child account. If this option is disabled, subtotals are displayed \
# below parent and children groups."
#
# optname_show_zb_accts ="Include accounts with zero total balances"
# opthelp_show_zb_accts ="Include accounts with zero total (recursive) balances in this report."
#
# optname_omit_zb_bals ="Omit zero balance figures"
# opthelp_omit_zb_bals ="Show blank space in place of any zero balances which would be shown."
#
# optname_account_links ="Display accounts as hyperlinks"
# opthelp_account_links ="Shows each account in the table as a hyperlink to its register window."
#
# optname_amount_links ="Display amounts as hyperlinks"
# opthelp_amount_links ="Shows each amounts in the table as a hyperlink to a register or report."
#
#
# optname_label_sections ="Label sections"
# opthelp_label_sections ="Whether or not to include a label for sections."
# optname_total_sections ="Include totals"
# opthelp_total_sections ="Whether or not to include a line indicating total amounts."
#
# pagename_commodities ="Commodities"
# optname_include_chart ="Enable chart"
# opthelp_include_chart ="Enable link to chart"
#
# optname_common_currency ="Common Currency"
# opthelp_common_currency ="Convert all amounts to a single currency."
#
# optname_report_commodity ="Report's currency"
#
# optname_price_source ="Price Source"
#
# optname_show_foreign ="Show original currency amount"
# opthelp_show_foreign ="Also show original currency amounts"
#
# optname_include_overall_period ="If more than 1 period column, include overall period?"
# opthelp_include_overall_period ="If several profit & loss period columns are shown, \
# also show overall period profit & loss."
#
# optname_show_rates ="Show Exchange Rates"
# opthelp_show_rates ="Show the exchange rates used."
#
# trep_uuid = "2fe3b9833af044abb929a88d5a59620f"
# networth_barchart_uuid = "cbba1696c8c24744848062c7f1cf4a72"
# pnl_barchart_uuid = "80769921e87943adade887b9835a7685"
#
#
# class MultiColumn(ReportTemplate):
#     def __init__(self):
#         super().__init__(version=1, name=reportname, guid="c4173ac99b2b448289bf4d11c731af13",
#                          menu_name=reportname,
#                          menu_path=menu_name_asset_liability)
#
#     def render(self, report_obj):
#         def get_option(section, name):
#             option = report_obj.options.lookup_option(section, name)
#             if option is not None:
#                 return option.get_value()
#
#
#     def export(self, report_obj, export_types, filename):
#         pass
#
#     def generate_options(self):
#         super().generate_options()
#         add_option = self.options.register_option
#         book = Session.get_current_book()
#         OptionUtils.add_date_interval(self.options,pagename_general, optname_startdate, optname_enddate, "c")
#
#         add_option(MultiChoiceCallBackOption(pagename_general, optname_period,"c2", opthelp_period,"disabled",[
#      ("disabled","Disabled"),
#      ("YearDelta","One Year"),
#      ("HalfYearDelta","Half Year"),
#      ("QuarterDelta","Quarter Year"),
#      ("MonthDelta","One Month"),
#      ("TwoWeekDelta","Two Weeks"),
#      ("WeekDelta","One Week")], False,
#      (lambda (x)
#     (let ((x (not (eq? x 'disabled))))
#     (gnc_option_db_set_option_selectable_by_name
#     options
#     pagename_general, optname_disable_amount_indent
#     (not x))
#            (gnc_option_db_set_option_selectable_by_name
#            options
#            pagename_general, optname_dual_columns
#            (not x))
#      (gnc_option_db_set_option_selectable_by_name
#     options pagename_general, optname_reverse_chrono x)
#     (case report_type
#     ((balsheet)
#     (gnc_option_db_set_option_selectable_by_name
#     options pagename_general, optname_include_chart x)
#
#     (gnc_option_db_set_option_selectable_by_name
#      options pagename_general, optname_startdate x))
#
#     ((pnl)
#      (gnc_option_db_set_option_selectable_by_name
#      options pagename_general, optname_include_overall_period x)))))))
#
#         add_option(
#     SimpleBooleanOption(
#      pagename_general, optname_disable_amount_indent
#      ",c3", opthelp_disable_amount_indent, False))
#
#         add_option(
#      SimpleBooleanOption(
#      pagename_general, optname_include_chart
#      ",c5", opthelp_include_chart, False))
#
#         add_option(
#      SimpleBooleanOption(
#      pagename_general, optname_dual_columns
#      ",c4", opthelp_dual_columns, True))
#
#         add_option(
#      SimpleBooleanOption(
#      pagename_general, optname_reverse_chrono
#      ",c5", opthelp_reverse_chrono, True))
#
#         add_option(
#      (make_multichoice_option
#      pagename_general, optname_options_summary
#      ,"d", opthelp_options_summary
#      'never
#      (list ("always":"Always"))
#      ("never":"Never")))))
#
#      ;; accounts to work on
#         add_option(
#      (make_account_list_option
#      pagename_accounts, optname_accounts
#      "a"
#      opthelp_accounts
#      (lambda ()
#     (filter_accountlist_type
#     [AccountType.BANK ,AccountType.CASH ,AccountType.CREDIT
#     ,AccountType.CURRENT_ASSET, AccountType.FIXED_ASSET ,AccountType.CURRENT_LIABILITY,AccountType.LONG_TERM_LIABILITY,AccountType.STOCK ,AccountType.MUTUAL ,AccountType.CURRENCY
#     ,AccountType.PAYABLE ,AccountType.RECEIVABLE
#     ,AccountType.EQUITY ,AccountType.INCOME ,AccountType.EXPENSE
#     ,AccountType.TRADING]
#     (gnc_account_get_descendants_sorted (gnc_get_current_root_account))))
#    , False, True))
#
#     ;; the depth_limit option is not well debugged; it may be better
#     ;; to disable it altogether
#     OptionUtils.add_account_levels!
#     options pagename_accounts, optname_depth_limit
#     ,"b", opthelp_depth_limit 'all)
#
#     ;; all about currencies
#         add_option(
#     (make_complex_boolean_option
#     pagename_commodities, optname_common_currency
#     ,"b", opthelp_common_currency, False, False
#     (lambda (x)
#      (for_each
#      (lambda (optname)
#       (gnc_option_db_set_option_selectable_by_name
#        options pagename_commodities optname x))
#      (list, optname_report_commodity
#    , optname_show_rates
#    , optname_show_foreign
#    , optname_price_source)))))
#
#     OptionUtils.add_currency!
#     options pagename_commodities
#    , optname_report_commodity "c")
#
#     OptionUtils.add_price_source!
#     options pagename_commodities
#    , optname_price_source "d" 'pricedb_nearest)
#
#         add_option(
#     SimpleBooleanOption(
#     pagename_commodities, optname_show_foreign
#     ,"e", opthelp_show_foreign, True))
#
#         add_option(
#     SimpleBooleanOption(
#     pagename_commodities, optname_show_rates
#     ,"f", opthelp_show_rates, True))
#
#
#             add_option(
#         SimpleBooleanOption(
#     pagename_display, optname_show_zb_accts
#     ,"a", opthelp_show_zb_accts, True))
#
#         add_option(SimpleBooleanOption(pagename_display, optname_omit_zb_bals, ,"b", opthelp_omit_zb_bals,, False))
#
#         add_option(
#     SimpleBooleanOption(
#     pagename_display, optname_parent_balance_mode
#     ,"c", opthelp_parent_balance_mode, True))
#
#     ;; some detailed formatting options
#         add_option(
#     SimpleBooleanOption(
#     pagename_display, optname_account_links
#     ,"e", opthelp_account_links, True))
#
#         add_option(
#     SimpleBooleanOption(
#     pagename_display, optname_amount_links
#     ",e5", opthelp_amount_links, True))
#
#         add_option(
#     SimpleBooleanOption(
#     pagename_display, optname_account_full_name
#     ,"f", opthelp_account_full_name, False))
#
#         add_option(
#     SimpleBooleanOption(
#     pagename_display, optname_label_sections ,"g", opthelp_label_sections, True))
#
#         add_option(
#     SimpleBooleanOption(
#     pagename_display, optname_total_sections ,"h", opthelp_total_sections, True))
#
#     (when (eq? report_type 'pnl) \
#
#         add_option(
#     SimpleBooleanOption(
#      pagename_general, optname_include_overall_period
#      ",c6", opthelp_include_overall_period, False)))
#
#      OptionUtils.set_default_section options pagename_general)
#
#     options))
