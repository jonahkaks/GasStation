class StandardReports:
    report_hash = {}
    @classmethod
    def register_report_hook(cls,acct_type, split, create_fcn):
        # (let ((type-info (hash-ref gnc:*register-report-hash* acct-type (make-acct-type))))
        # (if split?
        #  (set-split type-info create-fcn)
        # (set-non-split type-info create-fcn))
        # (hash-set! gnc:*register-report-hash* acct-type type-info)))
        pass

    @classmethod
    def lookup_register_report(cls, acct_type, split):
        return

    @classmethod
    def register_report(cls, account, split, query, journal, ledger_type,double,title, debit_string, credit_string):
        from .register import RegisterReport

        acct_type = account.get_type() if account is not None else None
        create_fcn= cls.lookup_register_report(acct_type, split)
        if create_fcn is not None:
            create_fcn(account, split, query, journal, double,title, debit_string, credit_string)
        return RegisterReport.internal(False, query, journal, ledger_type, double, title, debit_string, credit_string)