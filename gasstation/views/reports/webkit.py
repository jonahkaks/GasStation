import weakref

from gi import require_version

try:
    require_version('WebKit2', '4.0')
    from gi.repository import WebKit2
except ImportError:
    pass
from gasstation.utilities.print_settings import *
from gasstation.utilities.preferences import *
from gasstation.utilities.custom_dialogs import show_error
import re
import tempfile
import os

error_404_format = "<html><body><h3>%s</h3><p>%s</body></html>"
error_404_title = "Not found"
error_404_body = "The specified URL could not be loaded."
BASE_URI_NAME = "base-uri"
PREF_RPT_DFLT_ZOOM = "default-zoom"

URL_TYPE_FILE = "file"
URL_TYPE_JUMP = "jump"
URL_TYPE_HTTP = "http"
URL_TYPE_FTP = "ftp"
URL_TYPE_SECURE = "secure"
URL_TYPE_REGISTER = "register"
URL_TYPE_INVOICE = "invoice"
URL_TYPE_BILL = "bill"
URL_TYPE_ACCTTREE = "accttree"
URL_TYPE_REPORT = "report"
URL_TYPE_OPTIONS = "options"
URL_TYPE_SCHEME = "scheme"
URL_TYPE_HELP = "help"
URL_TYPE_XMLDATA = "xmldata"
URL_TYPE_PRICE = "price"
URL_TYPE_CUSTOMER ="customer"
URL_TYPE_JOB ="job"
URL_TYPE_SUPPLIER = "supplier"
URL_TYPE_STAFF = "staff"
URL_TYPE_OTHER = "other"
URL_TYPE_BUDGET = "budget"


def webview_new():
    view = WebKit2.WebView()
    style = view.get_style_context()
    default_font_family = None
    state = style.get_state()
    font = style.get_font(state)
    if font is not None:
        default_font_family = font.get_family()
    webkit_settings = view.get_settings()
    webkit_settings.set_properties(
        default_charset="utf_8",
        allow_file_access_from_file_urls=True if WebKit2.MINOR_VERSION >= 10 else False,
        allow_universal_access_from_file_urls=True if WebKit2.MINOR_VERSION >= 14 else False,
        enable_java=False,
        enable_page_cache=False,
        enable_plugins=False,
        enable_site_specific_quirks=False,
        enable_xss_auditor=False,
        enable_developer_extras=True)
    if default_font_family is not None:
        webkit_settings.set_property("default-font-family", default_font_family)
    return view


def extract_machine_name(path):
    machine_rexp = r"^(//[^/]*)/*(.*)?$"
    if path is None:
        return None
    compiled_m = re.compile(machine_rexp, re.X)
    match = compiled_m.split(path)
    return match


class UrlResult:
    def __init__(self):
        self.load_to_stream = False
        self.url_type = URL_TYPE_FILE
        self.location = None
        self.label = None
        self.base_type = None
        self.base_location = None
        self.parent = None
        self.error_message = None


class HtmlHistoryNode:
    def __init__(self, _type, location, label):
        self.type = _type
        self.location = location
        self.label = label


class HtmlHistory:
    def __init__(self):
        self.nodes = []
        self.current = -1
        self.last = -1

    def append(self, node):
        self.nodes.append(node)
        self.current += 1

    def get_current(self):
        try:
            return self.nodes[self.current]
        except IndexError:
            return None

    def forward(self):
        self.current += 1

    def forward_p(self):
        return self.current < len(self.nodes) - 1

    def back(self):
        self.current -= 1

    def back_p(self):
        return self.current > 0


class WebKitWebView(Gtk.Bin):
    proto_hash = None
    type_hash = None
    object_handlers = weakref.WeakValueDictionary()
    stream_handlers = weakref.WeakValueDictionary()
    url_handlers = {}

    def __init__(self, **properties):
        super().__init__(**properties)
        self.web_view = webview_new()
        self._parent = None
        self.contain = Gtk.ScrolledWindow()
        self.contain.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        self.contain.add(self.web_view)
        self.current_link = None
        self.base_type = None
        self.base_location = None
        self.request_info = {}
        self.urltype_cb = None
        self.load_cb = None
        self.load_cb_data = None
        self.flyover_cb = None
        self.flyover_cb_data = None
        self.button_cb = None
        self.history = HtmlHistory()
        self.html_string = None
        zoom = gs_pref_get_float(PREFS_GROUP_GENERAL_REPORT, PREF_RPT_DFLT_ZOOM)
        self.web_view.set_zoom_level(zoom)
        self.web_view.connect("decide-policy", self.decide_policy_cb)
        self.web_view.connect("mouse-target-changed", self.mouse_target_cb)
        self.web_view.connect("show-notification", self.notification_cb)
        gs_pref_register_cb(PREFS_GROUP_GENERAL_REPORT, PREF_RPT_DFLT_ZOOM, self.default_zoom_changed)

    def set_parent(self, p):
        self._parent = p

    def decide_policy_cb(self, web_view, decision, decision_type):
        if decision_type != WebKit2.PolicyDecisionType.NAVIGATION_ACTION:
            decision.use()
            return True
        return self.perform_navigation_policy(web_view, decision)

    def perform_navigation_policy(self, webview, decision):
        ignore = False
        action = decision.get_navigation_action()
        if action.get_navigation_type() != WebKit2.NavigationType.LINK_CLICKED:
            decision.use()
            return True
        req = action.get_request()
        uri = req.get_uri()
        scheme, location, label = self.parse_url(self, uri)
        if scheme != URL_TYPE_FILE:
            self.show_url(scheme, location, label, False)
            ignore = True
        if ignore:
            decision.ignore()
        else:
            decision.use()
        return True

    def mouse_target_cb(self, web_view, hit, modifiers):
        if not hit.context_is_link():
            return
        uri = hit.get_link_uri()
        self.current_link = uri
        if self.flyover_cb is not None:
            self.flyover_cb(self, uri, self.flyover_cb_data)

    def notification_cb(self, web_view, note):
        top = web_view.get_toplevel()
        dialog = Gtk.MessageDialog(transient_for=top, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.WARNING,
                                   buttons=Gtk.ButtonsType.CLOSE,
                                   message_format="%s\n%s" % (note.get_title(), note.get_body()))
        dialog.run()
        dialog.destroy()
        return True

    def get_widget(self):
        return self.contain

    def set_urltype_cb(self, cb):
        self.urltype_cb = cb

    def set_load_cb(self, loac_cb, *data):
        self.load_cb = loac_cb
        self.load_cb_data = data

    def set_flyover_cb(self, flyover_cb, *data):
        self.flyover_cb = flyover_cb
        self.flyover_cb_data = data

    def get_history(self):
        return self.history

    @classmethod
    def register_urltype(cls, _type, protocol):
        if cls.proto_hash is None:
            cls.proto_hash = {}
            cls.type_hash = {}
        if protocol is None:
            return False
        lc_type = _type.lower()
        if cls.proto_hash.get(lc_type) is not None:
            return False
        lc_proto = protocol.lower()
        cls.proto_hash[lc_type] = lc_proto
        if lc_proto is not None:
            cls.type_hash[lc_proto] = lc_type
            return True

    @classmethod
    def initialize(cls):
        for x in [(URL_TYPE_FILE, "file"),
                  (URL_TYPE_JUMP, ""),
                  (URL_TYPE_HTTP, "http"),
                  (URL_TYPE_FTP, "ftp"),
                  (URL_TYPE_SECURE, "https"),
                  (URL_TYPE_INVOICE, "gs-invoice"),
                  (URL_TYPE_BILL, "gs-bill"),
                  (URL_TYPE_CUSTOMER, "gs-customer"),
                  (URL_TYPE_SUPPLIER, "gs-supplier"),
                  (URL_TYPE_REGISTER, "gs-register"),
                  (URL_TYPE_ACCTTREE, "gs-acct-tree"),
                  (URL_TYPE_REPORT, "gs-report"),
                  (URL_TYPE_OPTIONS, "gs-options"),
                  (URL_TYPE_SCHEME, "gs-scm"),
                  (URL_TYPE_HELP, "gs-help"),
                  (URL_TYPE_XMLDATA, "gs-xml"),
                  (URL_TYPE_PRICE, "gs-price"),
                  (URL_TYPE_BUDGET, "gs-budget"),
                  (URL_TYPE_OTHER, "")]:
            cls.register_urltype(*x)

    @classmethod
    def build_url(cls, _type, location, label=None):
        lc_type = _type.lower()
        type_name = cls.proto_hash.get(lc_type)
        if type_name is None:
            type_name = ""
        if label is not None:
            return "%s%s%s#%s" % (type_name, ":" if type_name else "",
                                  location if location is not None else "",
                                  label if label is not None else "")
        else:
            return "%s%s%s" % (type_name, ":" if type_name else "", location if location is not None else "")

    @classmethod
    def parse_url(cls, self, url):
        protocol = None
        path = None
        label = None
        found_protocol = False
        found_path = False
        found_label = False
        uri_rexp = r"^(([^:][^:]+):)?([^#]+)?(#(.*))?$"
        compiled = re.compile(uri_rexp)
        match = compiled.match(url).groups()
        if match:
            if match[1] is not None:
                protocol = match[1]
                found_protocol = protocol
            if match[2] is not None:
                path = match[2]
                found_path = True
            if match[4] is not None:
                label = match[4]
                found_label = True
        if found_protocol:
            retval = cls.type_hash.get(protocol)
            if retval is None:
                retval = URL_TYPE_OTHER

        elif found_label and not found_path:
            retval = URL_TYPE_JUMP
        else:
            if self is not None:
                retval = self.base_type
            else:
                retval = URL_TYPE_FILE

        if retval == URL_TYPE_FILE:
            if not found_protocol and path is not None and self is not None and self.base_location is not None:
                if GLib.path_is_absolute(path):
                    url_location = path
                else:
                    url_location = GLib.build_filenamev([self.base_location, path])
            else:
                url_location = path

        elif retval == URL_TYPE_JUMP:
            url_location = None
        else:
            if not found_protocol and path is not None and self is not None and self.base_location is not None:
                if GLib.path_is_absolute(path):
                    url_location = GLib.build_filenamev([extract_machine_name(self.base_location), path])
                else:
                    url_location = GLib.build_filenamev([self.base_location, path])
            else:
                url_location = path

        url_label = label
        return retval, url_location, url_label

    @classmethod
    def load_to_stream(cls, self, _type, location, label):
        stream_handler = cls.stream_handlers.get(_type)
        if stream_handler is not None:
            fdata = stream_handler(location)
            if fdata is not None and len(fdata):
                self.html_string = fdata
                self.show_data()
            else:
                fdata = error_404_format % (error_404_title, error_404_body)
                self.webview.load_html(fdata, BASE_URI_NAME)

            if label is not None:
                while Gtk.events_pending():
                    Gtk.main_iteration()
            return

    def show_data(self):
        fd, filename = tempfile.mkstemp(dir=GLib.get_tmp_dir(), prefix="gs-report-", suffix=".html")
        self.export_to_file(filename)
        os.close(fd)
        uri = "file://%s" % filename
        self.web_view.load_uri(uri)

    def export_to_file(self, filepath):
        if filepath is None or filepath == "":
            return False
        if self.html_string is None:
            return False
        with open(filepath, "w") as fh:
            fh.write(self.html_string)
            return True

    def print(self, jobname):
        op = WebKit2.PrintOperation.new(self.web_view)
        print_settings = PrintOperation.get_settings()
        print_settings.set(Gtk.PRINT_SETTINOUTPUT_BASENAME, jobname)
        op.set_print_settings(print_settings)
        top = self.web_view.get_toplevel()
        op.run_dialog(top)

    def default_zoom_changed(self, prefs, pref):
        zoom = gs_pref_get_float(PREFS_GROUP_GENERAL_REPORT, PREF_RPT_DFLT_ZOOM)
        self.web_view.set_zoom_level(zoom)

    def cancel(self):
        pass

    def reload(self, force_rebuild):
        if force_rebuild:
            n = self.history.get_current()
            if n is not None:
                self.show_url(n.type, n.location, n.label, 0)
        else:
            self.web_view.reload()

    def show_url(self, _type, location, label, new_window_hint):
        if location is None:
            return
        if new_window_hint == 0:
            if self.urltype_cb:
                new_window = not self.urltype_cb(_type)
            else:
                new_window = False
        else:
            new_window = True
        if not new_window:
            self.cancel()
        url_handler = self.__class__.url_handlers.get(_type)
        print("URL HANDLER IS None", url_handler)
        if url_handler is not None:
            result = UrlResult()
            result.load_to_stream = False
            result.url_type = _type
            result.location = None
            result.label = None
            result.base_type = URL_TYPE_FILE
            result.base_location = None
            result.error_message = None
            result.parent = self._parent
            ok = url_handler(location, label, new_window, result)
            if not ok:
                if result.error_message is not None:
                    show_error(self._parent, result.error_message)
                else:
                    show_error(self._parent, "There was an error accessing %s." % location)
                if self.load_cb:
                    self.load_cb(result.url_type, location, label, self.load_cb_data)
            elif result.load_to_stream:
                new_location = result.location if result.location is not None else location
                new_label = result.label if result.label is not None else label
                hnode = HtmlHistoryNode(result.url_type, new_location, new_label)
                self.history.append(hnode)
                self.base_type = result.base_type
                self.base_location = self.extract_base_name(result.base_type, new_location)
                self.load_to_stream(self, result.url_type, new_location, new_label)
                if self.load_cb is not None:
                    self.load_cb(result.url_type, new_location, new_label, self.load_cb_data)
            return

        if _type == URL_TYPE_JUMP:
            pass
        elif _type == URL_TYPE_SECURE or _type == URL_TYPE_HTTP or _type == URL_TYPE_FILE:
            if _type == URL_TYPE_SECURE or _type == URL_TYPE_HTTP:
                if not self.https_allowed():
                    show_error(self._parent, "Secure HTTP access is disabled. "
                                             "You can enable it in the Network section of "
                                             "the Preferences dialog.")

            self.base_type = _type
            self.base_location = self.extract_base_name(_type, location)

            self.history.append(HtmlHistoryNode(_type, location, label))
            self.load_to_stream(self, _type, location, label)
        print("NOW CALLING LOAD CALLBACK")
        if self.load_cb is not None:
            self.load_cb(_type, location, label, self.load_cb_data)

    @staticmethod
    def extract_base_name(_type, path):
        machine_rexp = "^(//[^/]*)/*(/.*)?$"
        path_rexp = "^/*(.*)/+([^/]*)$"
        compiled_m = re.compile(machine_rexp)
        compiled_p = re.compile(path_rexp)
        location = None
        machine = None
        base = None
        if _type == "http" or _type == "https" or _type == "ftp":
            match = compiled_m.match(path)
            if match is not None:
                match = match.groups()
                machine = match[0]
                location = match[1]
        else:
            location = path

        if location is not None:
            match = compiled_p.match(location)
            if match is not None:
                match = match.groups()
                if match[1] is not None:
                    base = match[1]
                else:
                    base = None

        if machine is not None:
            if base and len(base) > 0:
                basename = machine + "/" + base + "/"
            else:
                basename = machine + "/"
        else:
            if base and len(base) > 0:
                basename = base
            else:
                basename = None
        return basename

    @staticmethod
    def http_allowed():
        return True

    @staticmethod
    def https_allowed():
        return True

    @classmethod
    def register_url_handlers(cls, _type, handler):
        if _type is None or handler is None:
            raise ValueError("CANT REGISTER")
        cls.url_handlers[_type] = handler

    @classmethod
    def unregister_url_handlers(cls, _type):
        cls.url_handlers.pop(_type)

    @classmethod
    def register_stream_handlers(cls, _type, handler):
        if _type is None or handler is None or _type == "":
            return
        cls.stream_handlers[_type] = handler

    @classmethod
    def unregister_stream_handlers(cls, _type):
        cls.stream_handlers.pop(_type)

    @classmethod
    def register_object_handlers(cls, _type, handler):
        if _type is None or handler is None or _type == "":
            return
        cls.object_handlers[_type] = handler


WebKitWebView.initialize()
