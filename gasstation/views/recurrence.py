from gi.repository import GObject, Gtk

from gasstation.views.selections.date_edit import DateSelection
from libgasstation.core.recurrence import *


class UiPeriodType(Enum):
    DAY = 0
    WEEK = 1
    MONTH = 2
    YEAR = 3


class Recurrence(Gtk.Box):
    __gsignals__ = {
        'changed': (GObject.SignalFlags.RUN_LAST, None, (int,))}

    def __init__(self):
        super().__init__()
        self.gde_start = None
        self.gcb_period = None
        self.gcb_eom = None
        self.gsb_mult = None
        self.nth_weekday = None
        self.recurrence = None

    def get_period_type_ui(self):
        return self.gcb_period.get_active()

    def set_period_type_ui(self, pt):
        if pt == RecurrencePeriodType.DAY:
            idx = 0

        elif pt == RecurrencePeriodType.WEEK:
            idx = 1

        elif pt == RecurrencePeriodType.MONTH or pt == RecurrencePeriodType.END_OF_MONTH \
                or pt == RecurrencePeriodType.NTH_WEEKDAY or pt == RecurrencePeriodType.LAST_WEEKDAY:
            idx = 2

        elif pt == RecurrencePeriodType.YEAR:
            idx = 3

        else:
            return
        self.gcb_period.set_active(idx)
        self.nth_weekday.set_active(pt == RecurrencePeriodType.NTH_WEEKDAY or pt == RecurrencePeriodType.LAST_WEEKDAY)
        self.gcb_eom.set_active(pt == RecurrencePeriodType.END_OF_MONTH or pt == RecurrencePeriodType.LAST_WEEKDAY)

    @staticmethod
    def is_ambiguous_relative(date):
        d = date.get_day()
        dim = date.get_days_in_month(date.get_month(), date.get_year())
        return ((d - 1) / 7 == 3) and (dim - d < 7)

    @staticmethod
    def is_ambiguous_absolute(date):
        return date.is_last_of_month() and date.get_day() < 31

    def something_changed(self, widget):
        pt = self.get_period_type_ui()
        start = self.gde_start.get_gdate()

        if pt == UiPeriodType.MONTH:
            self.nth_weekday.set_property("visible", True)
        else:
            self.nth_weekday.set_property("visible", False)
            self.nth_weekday.set_active(False)

        use_wd = self.nth_weekday.get_active()
        if pt == UiPeriodType.MONTH:
            if use_wd:
                show_last = self.is_ambiguous_relative(start)
            else:
                show_last = self.is_ambiguous_absolute(start)
        else:
            show_last = False
            self.gcb_eom.set_active(False)
        self.gcb_eom.set_property("visible", show_last)
        self.emit("changed")

    def init(self):
        self.recurrence = Recurrence()
        self.recurrence.set(1, RecurrencePeriodType.MONTH, None, RecurrenceWeekendAdjust.NONE)
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/gs-recurrence.ui",
                                          "GCB_PeriodType_liststore")
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/gs-recurrence.ui",
                                          "GSB_Mult_Adj")
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/gs-recurrence.ui", "RecurrenceEntryVBox")
        vb = builder.get_object("RecurrenceEntryVBox")
        hb = builder.get_object("Startdate_hbox")
        w = DateSelection()
        hb.pack_start(w, True, True, 0)
        w.show()
        self.gde_start = w
        self.gde_start.set_no_show_all(True)
        self.gcb_period = builder.get_object("GCB_PeriodType")
        self.gsb_mult = builder.get_object("GSB_Mult")
        self.gcb_eom = builder.get_object("GCB_EndOfMonth")
        self.nth_weekday = builder.get_object("GCB_NthWeekday")
        self.gcb_eomTrue.set_no_show_all(True)
        self.nth_weekday.set_no_show_all(True)
        self.add(vb)
        self.set(self.recurrence)
        self.something_changed(None)
        self.gde_start.connect("changed", self.something_changed)
        self.gcb_period.connect("changed", self.something_changed)
        self.gsb_mult.connect("value-changed", self.something_changed)
        self.gcb_eom.connect("toggled", self.something_changed)
        self.nth_weekday.connect("toggled", self.something_changed)
        # self.show_all()

    def set(self, r):
        pt = r.get_period_type()
        mult = r.get_multiplier()
        start = r.get_period_start()
        self.gsb_mult.set_value(float(mult))
        self.gde_start.set_date(start)
        self.set_period_type_ui(pt)

    def get(self):
        mult = int(self.gsb_mult.get_value_as_int())
        start = self.gde_start.get_gdate()
        period = self.get_period_type_ui()
        if period == UiPeriodType.DAY:
            pt = RecurrencePeriodType.DAY

        elif period == UiPeriodType.WEEK:
            pt = RecurrencePeriodType.WEEK

        elif period == UiPeriodType.MONTH:
            rel = self.nth_weekday.get_active()
            if rel:
                if self.is_ambiguous_relative(start):
                    use_eom = self.gcb_eom.get_active()
                else:
                    d = start.get_day()
                    use_eom = ((d - 1) / 7 == 4)

                if use_eom:
                    pt = RecurrencePeriodType.LAST_WEEKDAY
                else:
                    pt = RecurrencePeriodType.NTH_WEEKDAY
            else:
                if start.is_last_of_month() and start.get_day() < 31:
                    use_eom = self.gcb_eom.get_active()
                else:
                    use_eom = start.is_last_of_month()

                if use_eom:
                    pt = RecurrencePeriodType.END_OF_MONTH
                else:
                    pt = RecurrencePeriodType.MONTH

        elif period == UiPeriodType.YEAR:
            pt = RecurrencePeriodType.YEAR

        else:
            pt = RecurrencePeriodType.INVALID

        self.recurrence.set(mult, pt, start, RecurrenceWeekendAdjust.NONE)
        return self.recurrence
