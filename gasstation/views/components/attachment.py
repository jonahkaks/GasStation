

#define NUM_VIEWS 2

class AttachmentBarPrivate {
    GtkTreeModel *model
GtkWidget *vbox
GtkWidget *expander
GtkWidget *combo_box
GtkWidget *icon_view
GtkWidget *tree_view
GtkWidget *icon_frame
GtkWidget *tree_frame
GtkWidget *status_icon
GtkWidget *status_label
GtkWidget *save_all_button
GtkWidget *save_one_button
GtkWidget *icon_scrolled_window
                                        GtkWidget *tree_scrolled_window

                                                                                gint active_view
guint expanded : 1
}

enum {
    PROP_0,
    PROP_ACTIVE_VIEW,
    PROP_DRAGGING,
    PROP_EDITABLE,
    PROP_EXPANDED,
    PROP_STORE
}


def do_interface_init
(EAttachmentViewInterface *iface)

G_DEFINE_TYPE_WITH_CODE(
    EAttachmentBar,
    e_attachment_bar,
    GTK_TYPE_BOX,
    G_IMPLEMENT_INTERFACE(
        E_TYPE_ATTACHMENT_VIEW,
        def do_interface_init))

ef do_update_status(self)
{
    EAttachmentStore *store
GtkActivatable *activatable
GtkAction *action
GtkLabel *label
gint num_attachments
guint64 total_size
gchar *display_size
gchar *markup

store = E_ATTACHMENT_STORE(self.model)
label = GTK_LABEL(self.status_label)

num_attachments = e_attachment_store_get_num_attachments(store)
total_size = e_attachment_store_get_total_size(store)
display_size = g_format_size(total_size)

if(total_size > 0)
markup = g_strdup_printf(
    "<b>%d</b> %s(%s)", num_attachments, ngettext(
        "Attachment", "Attachments", num_attachments),
    display_size)
else
markup = g_strdup_printf(
    "<b>%d</b> %s", num_attachments, ngettext(
        "Attachment", "Attachments", num_attachments))
gtk_label_set_markup(label, markup)
g_free(markup)

activatable = GTK_ACTIVATABLE(self.save_all_button)
action = gtk_activatable_get_related_action(activatable)
gtk_action_set_visible(action, num_attachments > 1)

activatable = GTK_ACTIVATABLE(self.save_one_button)
action = gtk_activatable_get_related_action(activatable)
gtk_action_set_visible(action,(num_attachments == 1))

g_free(display_size)
}

ef do_notify_vadjustment_upper_cb(GObject *object,
                                            GParamSpec *param,
                                            gpointer user_data)
{
    self = user_data
GtkAdjustment *adjustment
gint max_upper, max_content_height = -2
gint request_height = -1



adjustment = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(self.icon_scrolled_window))
max_upper = gtk_adjustment_get_upper(adjustment)

adjustment = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(self.tree_scrolled_window))
max_upper = MAX(max_upper, gtk_adjustment_get_upper(adjustment))

gtk_widget_style_get(GTK_WIDGET(bar), "max-content-height", &max_content_height, NULL)

if((max_content_height >= 0 && max_content_height < 50) || max_content_height <= -2)
max_content_height = 50

if(max_content_height == -1) {
request_height = max_upper
} else if(max_content_height < max_upper) {
request_height = max_content_height
} else {
request_height = max_upper
}

gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(self.icon_scrolled_window),
request_height)
gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(self.tree_scrolled_window),
request_height)
}

ef do_set_store(self,
                          EAttachmentStore *store)
{
    g_return_if_fail(E_IS_ATTACHMENT_STORE(store))

self.model = GTK_TREE_MODEL(g_object_ref(store))

gtk_icon_view_set_model(
    GTK_ICON_VIEW(self.icon_view),
self.model)
gtk_tree_view_set_model(
    GTK_TREE_VIEW(self.tree_view),
self.model)

e_signal_connect_notify_object(
    self.model, "notify::num-attachments",
               G_CALLBACKdef do_update_status), bar,
               G_CONNECT_SWAPPED)

e_signal_connect_notify_object(
    self.model, "notify::total-size",
               G_CALLBACKdef do_update_status), bar,
               G_CONNECT_SWAPPED)

def do_update_status(bar)
}

ef do_set_property(GObject *object,
                             guint property_id,
                                   const GValue *value,
                                         GParamSpec *pspec)
{
    switch(property_id) {
    case PROP_ACTIVE_VIEW: \
def do_set_active_view(
    E_ATTACHMENT_BAR(object),
    g_value_get_int(value))
return

case PROP_DRAGGING:
e_attachment_view_set_dragging(
    E_ATTACHMENT_VIEW(object),
    g_value_get_boolean(value))
return

case PROP_EDITABLE:
e_attachment_view_set_editable(
    E_ATTACHMENT_VIEW(object),
    g_value_get_boolean(value))
return

case PROP_EXPANDED:
def do_set_expanded(
    E_ATTACHMENT_BAR(object),
    g_value_get_boolean(value))
return
case PROP_STOREdef do_set_store(
    E_ATTACHMENT_BAR(object),
    g_value_get_object(value))
return
}

G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec)
}

ef do_get_property(GObject *object,
                             guint property_id,
                                   GValue *value,
                                   GParamSpec *pspec)
{
    switch(property_id) {
case PROP_ACTIVE_VIEW:
g_value_set_int(
    value,
    def do_get_active_view(
        E_ATTACHMENT_BAR(object)))
return

case PROP_DRAGGING:
g_value_set_boolean(
    value,
    e_attachment_view_get_dragging(
        E_ATTACHMENT_VIEW(object)))
return

case PROP_EDITABLE:
g_value_set_boolean(
    value,
    e_attachment_view_get_editable(
        E_ATTACHMENT_VIEW(object)))
return

case PROP_EXPANDED:
g_value_set_boolean(
    value,
    def do_get_expanded(
        E_ATTACHMENT_BAR(object)))
return
case PROP_STORE:
g_value_set_object(
    value,
    def do_get_store(
        E_ATTACHMENT_BAR(object)))
}

G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec)
}

ef do_dispose(GObject *object)
{
    EAttachmentBarPrivate *priv

priv = E_ATTACHMENT_BAR_GET_PRIVATE(object)
g_clear_object(&priv->model)
g_clear_object(&priv->vbox)
g_clear_object(&priv->expander)
g_clear_object(&priv->combo_box)
g_clear_object(&priv->icon_view)
g_clear_object(&priv->tree_view)
g_clear_object(&priv->icon_frame)
g_clear_object(&priv->tree_frame)
g_clear_object(&priv->status_icon)
g_clear_object(&priv->status_label)
g_clear_object(&priv->save_all_button)
g_clear_object(&priv->save_one_button)


G_OBJECT_CLASS(def do_parent_class)->dispose(object)
}

ef do_constructed(GObject *object)
{
    EAttachmentBarPrivate *priv
GSettings *settings

priv = E_ATTACHMENT_BAR_GET_PRIVATE(object)



                               e_binding_bind_property(
object, "active-view",
priv->combo_box, "active",
G_BINDING_BIDIRECTIONAL |
G_BINDING_SYNC_CREATE)

e_binding_bind_property(
object, "dragging",
priv->icon_view, "dragging",
G_BINDING_BIDIRECTIONAL |
G_BINDING_SYNC_CREATE)

e_binding_bind_property(
object, "dragging",
priv->tree_view, "dragging",
G_BINDING_BIDIRECTIONAL |
G_BINDING_SYNC_CREATE)

e_binding_bind_property(
object, "editable",
priv->icon_view, "editable",
G_BINDING_BIDIRECTIONAL |
G_BINDING_SYNC_CREATE)

e_binding_bind_property(
object, "editable",
priv->tree_view, "editable",
G_BINDING_BIDIRECTIONAL |
G_BINDING_SYNC_CREATE)

e_binding_bind_property(
object, "expanded",
priv->expander, "expanded",
G_BINDING_BIDIRECTIONAL |
G_BINDING_SYNC_CREATE)

e_binding_bind_property(
object, "expanded",
priv->combo_box, "visible",
G_BINDING_BIDIRECTIONAL |
G_BINDING_SYNC_CREATE)

e_binding_bind_property(
object, "expanded",
priv->vbox, "visible",
G_BINDING_BIDIRECTIONAL |
G_BINDING_SYNC_CREATE)


                                settings = e_util_ref_settings("org.gnome.evolution.shell")
g_settings_bind(
settings, "attachment-view",
object, "active-view",
G_SETTINGS_BIND_DEFAULT)
g_object_unref(settings)


G_OBJECT_CLASS(def do_parent_class)->constructed(object)
}

static EAttachmentViewPrivate *
def do_get_private(EAttachmentView *view)
{
    self

bar = E_ATTACHMENT_BAR(view)
view = E_ATTACHMENT_VIEW(self.icon_view)

return e_attachment_view_get_private(view)
}

static GtkTreePath *
def do_get_path_at_pos(EAttachmentView *view,
gint x,
gint y)
{
self

bar = E_ATTACHMENT_BAR(view)
view = E_ATTACHMENT_VIEW(self.icon_view)

return e_attachment_view_get_path_at_pos(view, x, y)
}

static EAttachmentStore *
def do_get_store(EAttachmentView *view)
{
return def do_get_store(E_ATTACHMENT_BAR(view))
}

static GList *
def do_get_selected_paths(EAttachmentView *view)
{
self

bar = E_ATTACHMENT_BAR(view)
view = E_ATTACHMENT_VIEW(self.icon_view)

return e_attachment_view_get_selected_paths(view)
}

static gbooleadef do_path_is_selected(EAttachmentView *view,
GtkTreePath *path)
{
self

bar = E_ATTACHMENT_BAR(view)
view = E_ATTACHMENT_VIEW(self.icon_view)

return e_attachment_view_path_is_selected(view, path)
}

ef do_select_path(EAttachmentView *view,
GtkTreePath *path)
{
self

bar = E_ATTACHMENT_BAR(view)
view = E_ATTACHMENT_VIEW(self.icon_view)

e_attachment_view_select_path(view, path)
}

ef do_unselect_path(EAttachmentView *view,
GtkTreePath *path)
{
self

bar = E_ATTACHMENT_BAR(view)
view = E_ATTACHMENT_VIEW(self.icon_view)

e_attachment_view_unselect_path(view, path)
}

ef do_select_all(EAttachmentView *view)
{
self

bar = E_ATTACHMENT_BAR(view)
view = E_ATTACHMENT_VIEW(self.icon_view)

e_attachment_view_select_all(view)
}

ef do_unselect_all(EAttachmentView *view)
{
self

bar = E_ATTACHMENT_BAR(view)
view = E_ATTACHMENT_VIEW(self.icon_view)

e_attachment_view_unselect_all(view)
}

ef do_update_actions(EAttachmentView *view)
{
self

bar = E_ATTACHMENT_BAR(view)
view = E_ATTACHMENT_VIEW(self.icon_view)

e_attachment_view_update_actions(view)
}

static gbooleadef do_button_press_event(GtkWidget *widget,
GdkEventButton *event)
{

GTK_WIDGET_CLASS(def do_parent_class)->button_press_event(widget, event)


return TRUE
}

static gbooleadef do_button_release_event(GtkWidget *widget,
GdkEventButton *event)
{

GTK_WIDGET_CLASS(def do_parent_class)->button_release_event(widget, event)


return TRUE
}

static gbooleadef do_motion_notify_event(GtkWidget *widget,
GdkEventMotion *event)
{

GTK_WIDGET_CLASS(def do_parent_class)->motion_notify_event(widget, event)


return TRUE
}


def do_class_init(EAttachmentBarClass *class)
{
GObjectClass *object_class
GtkWidgetClass *widget_class

g_type_class_add_private(class, sizeof(EAttachmentBarPrivate))

object_class = G_OBJECT_CLASS(class)
object_class->set_property def do_set_property
object_class->get_property def do_get_property
object_class->dispose def do_dispose
object_class->constructed def do_constructed

widget_class = GTK_WIDGET_CLASS(class)
widget_class->button_press_event def do_button_press_event
widget_class->button_release_event def do_button_release_event
widget_class->motion_notify_event def do_motion_notify_event

gtk_widget_class_set_css_name(widget_class, G_OBJECT_CLASS_NAME(class))

g_object_class_install_property(
object_class,
PROP_ACTIVE_VIEW,
g_param_spec_int(
"active-view",
"Active View",
NULL,
0,
NUM_VIEWS,
0,
G_PARAM_READWRITE |
G_PARAM_CONSTRUCT))

g_object_class_install_property(
    object_class,
    PROP_EXPANDED,
    g_param_spec_boolean(
"expanded",
"Expanded",
NULL,
FALSE,
G_PARAM_READWRITE |
G_PARAM_CONSTRUCT))

g_object_class_install_property(
    object_class,
    PROP_STORE,
    g_param_spec_object(
"store",
"Attachment Store",
NULL,
E_TYPE_ATTACHMENT_STORE,
G_PARAM_READWRITE |
G_PARAM_CONSTRUCT_ONLY))

g_object_class_override_property(
    object_class, PROP_DRAGGING, "dragging")

g_object_class_override_property(
    object_class, PROP_EDITABLE, "editable")

gtk_widget_class_install_style_property(
    widget_class,
    g_param_spec_int(
"max-content-height",
"Max Content Height",
NULL,
-1, G_MAXINT, 150,
G_PARAM_READABLE | G_PARAM_STATIC_STRINGS))
}


def do_interface_init(EAttachmentViewInterface *iface)
{
    iface->get_private def do_get_private
iface->get_store def do_get_store
iface->get_path_at_pos def do_get_path_at_pos
iface->get_selected_paths def do_get_selected_paths
iface->path_is_selected def do_path_is_selected
iface->select_path def do_select_path
iface->unselect_path def do_unselect_path
iface->select_all def do_select_all
iface->unselect_all def do_unselect_all
iface->update_actions def do_update_actions
}


def do_init(self)
{
    EAttachmentView *view
GtkSizeGroup *size_group
GtkWidget *container
GtkWidget *widget
GtkAction *action
GtkAdjustment *adjustment

gtk_widget_set_name(GTK_WIDGET(bar), "e-attachment-bar")

bar->priv = E_ATTACHMENT_BAR_GET_PRIVATE(bar)

gtk_orientable_set_orientation(GTK_ORIENTABLE(bar), GTK_ORIENTATION_VERTICAL)


                                                    size_group = gtk_size_group_new(GTK_SIZE_GROUP_VERTICAL)



                            container = GTK_WIDGET(bar)

widget = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0)
gtk_box_pack_end(GTK_BOX(container), widget, FALSE, FALSE, 0)
self.vbox = g_object_ref(widget)
gtk_widget_show(widget)

container = self.vbox

widget = gtk_frame_new(NULL)
gtk_frame_set_shadow_type(GTK_FRAME(widget), GTK_SHADOW_NONE)
gtk_box_pack_start(GTK_BOX(container), widget, TRUE, TRUE, 0)
self.icon_frame = g_object_ref(widget)
gtk_widget_show(widget)

container = widget

widget = gtk_scrolled_window_new(NULL, NULL)
gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(widget), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC)
gtk_container_add(GTK_CONTAINER(container), widget)
self.icon_scrolled_window = widget
gtk_widget_show(widget)

widget = e_attachment_icon_view_new()
gtk_widget_set_can_focus(widget, TRUE)
gtk_icon_view_set_model(GTK_ICON_VIEW(widget), self.model)
gtk_container_add(GTK_CONTAINER(self.icon_scrolled_window), widget)
self.icon_view = g_object_ref(widget)
gtk_widget_show(widget)

container = self.vbox

widget = gtk_frame_new(NULL)
gtk_frame_set_shadow_type(GTK_FRAME(widget), GTK_SHADOW_NONE)
gtk_box_pack_start(GTK_BOX(container), widget, TRUE, TRUE, 0)
self.tree_frame = g_object_ref(widget)
gtk_widget_hide(widget)

container = widget

widget = gtk_scrolled_window_new(NULL, NULL)
gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(widget), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC)
gtk_container_add(GTK_CONTAINER(container), widget)
self.tree_scrolled_window = widget
gtk_widget_show(widget)

widget = e_attachment_tree_view_new()
gtk_widget_set_can_focus(widget, TRUE)
gtk_tree_view_set_model(GTK_TREE_VIEW(widget), self.model)
gtk_container_add(GTK_CONTAINER(self.tree_scrolled_window), widget)
self.tree_view = g_object_ref(widget)
gtk_widget_show(widget)



                 container = GTK_WIDGET(bar)

widget = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 12)
gtk_container_set_border_width(GTK_CONTAINER(widget), 6)
gtk_box_pack_end(GTK_BOX(container), widget, FALSE, FALSE, 0)
gtk_widget_show(widget)

container = widget

widget = gtk_expander_new(NULL)
gtk_expander_set_spacing(GTK_EXPANDER(widget), 0)
gtk_widget_set_valign(widget, GTK_ALIGN_CENTER)
gtk_box_pack_start(GTK_BOX(container), widget, FALSE, FALSE, 0)
self.expander = g_object_ref(widget)
gtk_widget_show(widget)


widget = gtk_button_new()
view = E_ATTACHMENT_VIEW(self.icon_view)
action = e_attachment_view_get_action(view, "save-all")
gtk_button_set_image(GTK_BUTTON(widget), gtk_image_new())
gtk_activatable_set_related_action(GTK_ACTIVATABLE(widget), action)
gtk_box_pack_start(GTK_BOX(container), widget, FALSE, FALSE, 0)
self.save_all_button = g_object_ref(widget)
gtk_widget_show(widget)


widget = gtk_button_new()
view = E_ATTACHMENT_VIEW(self.icon_view)
action = e_attachment_view_get_action(view, "save-one")
gtk_button_set_image(GTK_BUTTON(widget), gtk_image_new())
gtk_activatable_set_related_action(GTK_ACTIVATABLE(widget), action)
gtk_box_pack_start(GTK_BOX(container), widget, FALSE, FALSE, 0)
self.save_one_button = g_object_ref(widget)
gtk_widget_show(widget)

widget = gtk_alignment_new(1.0, 0.5, 0.0, 0.0)
gtk_box_pack_start(GTK_BOX(container), widget, TRUE, TRUE, 0)
gtk_widget_show(widget)

container = widget

widget = gtk_combo_box_text_new()
gtk_size_group_add_widget(size_group, widget)
gtk_combo_box_text_append_text(
GTK_COMBO_BOX_TEXT(widget), _("Icon View"))
gtk_combo_box_text_append_text(
GTK_COMBO_BOX_TEXT(widget), _("List View"))
gtk_container_add(GTK_CONTAINER(container), widget)
self.combo_box = g_object_ref(widget)
gtk_widget_show(widget)

container = self.expander

widget = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 6)
gtk_size_group_add_widget(size_group, widget)
gtk_expander_set_label_widget(GTK_EXPANDER(container), widget)
gtk_widget_show(widget)

container = widget

widget = gtk_image_new_from_icon_name(
"mail-attachment", GTK_ICON_SIZE_MENU)
gtk_box_pack_start(GTK_BOX(container), widget, FALSE, FALSE, 0)
self.status_icon = g_object_ref(widget)
gtk_widget_show(widget)

widget = gtk_label_new(NULL)
gtk_label_set_use_markup(GTK_LABEL(widget), TRUE)
gtk_box_pack_start(GTK_BOX(container), widget, FALSE, FALSE, 0)
self.status_label = g_object_ref(widget)
gtk_widget_show(widget)

g_object_unref(size_group)

adjustment = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(self.icon_scrolled_window))
e_signal_connect_notify(adjustment, "notify::upper",
G_CALLBACKdef do_notify_vadjustment_upper_cb), bar)

adjustment = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(self.tree_scrolled_window))
e_signal_connect_notify(adjustment, "notify::upper",
G_CALLBACKdef do_notify_vadjustment_upper_cb), bar)
}

GtkWidget *
def do_new(EAttachmentStore *store)
{
g_return_val_if_fail(E_IS_ATTACHMENT_STORE(store), NULL)

return g_object_new(
    E_TYPE_ATTACHMENT_BAR,
    "editable", FALSE,
    "store", store, NULL)
}

gint
def do_get_active_view(self)
{
g_return_val_if_fail(E_IS_ATTACHMENT_BAR(bar), 0)

return self.active_view
}

void
def do_set_active_view(self,
gint active_view)
{
EAttachmentView *source
EAttachmentView *target


g_return_if_fail(active_view >= 0 && active_view < NUM_VIEWS)

if(active_view == self.active_view)
    return

self.active_view = active_view

if(active_view == 0) {
gtk_widget_show(self.icon_frame)
gtk_widget_hide(self.tree_frame)
} else {
gtk_widget_hide(self.icon_frame)
gtk_widget_show(self.tree_frame)
}


if(active_view == 0) {

source = E_ATTACHMENT_VIEW(self.tree_view)
target = E_ATTACHMENT_VIEW(self.icon_view)
} else {

source = E_ATTACHMENT_VIEW(self.icon_view)
target = E_ATTACHMENT_VIEW(self.tree_view)
}

e_attachment_view_sync_selection(source, target)

self.emit("active-view")
}

    def do_get_expanded(self):
        return self.expanded

    def do_set_expanded(self,expanded):
        if self.expanded == expanded:
            return
        self.expanded = expanded
        self.emit("expanded")


def do_get_store(self):
{
g_return_val_if_fail(E_IS_ATTACHMENT_BAR(bar), NULL)

return E_ATTACHMENT_STORE(self.model)
}
