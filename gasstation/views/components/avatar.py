from gi.repository import GObject

from gasstation.utilities.avatar_utils import *
from gasstation.utilities.dialog import gs_widget_set_style_context
from .avatar_chooser import AvatarChooser


class Avatar(Gtk.MenuButton):
    __gtype_name__ = "Avatar"
    __gsignals__ = {
        'image-selected': (GObject.SignalFlags.RUN_LAST, None, (str,)), }

    def __init__(self):
        super().__init__()
        self.url = None
        self.fullname = None
        self.icon_name = None
        image = Gtk.Image()
        self.add(image)
        self.set_can_focus(False)
        gs_widget_set_style_context(self, "user-icon-button")
        gs_widget_set_style_context(image, "user-icon-button")
        image.set_pixel_size(128)

        image.connect("notify::scale-factor", self.render_image)
        image.connect("notify::pixel-size", self.render_image)
        self.image = image
        image.show()
        chooser = AvatarChooser(self.get_toplevel())
        chooser.connect("closed", lambda *_: self.set_url(chooser.get_selected_url()))
        self.chooser = chooser
        self.connect("clicked", chooser.setup_photo_popup)
        self.set_popover(chooser)

    def render_avatar(self, icon_size, scale):
        surface = None
        pixbuf = None
        source_pixbuf = None
        if icon_size < 12:
            return
        icon_file = self.url
        if icon_file is not None:
            try:
                source_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(icon_file,
                                                                       icon_size * scale,
                                                                       icon_size * scale,
                                                                       )
            except GLib.Error:
                pass

        if source_pixbuf is None and self.fullname is not None:
            source_pixbuf = generate_default_avatar(self.fullname, icon_size * scale)

        icon_theme = Gtk.IconTheme.get_default()
        if source_pixbuf is None and self.icon_name is not None:
            try:
                source_pixbuf = icon_theme.load_icon(self.icon_name, 128, Gtk.IconLookupFlags.FORCE_SIZE)
            except GLib.GError:
                pass
        if source_pixbuf is None:
            source_pixbuf = icon_theme.load_icon("emblem-downloads", 128, Gtk.IconLookupFlags.FORCE_SIZE)

        if source_pixbuf is not None:
            pixbuf = round_image(source_pixbuf)

        if pixbuf is not None:
            surface = Gdk.cairo_surface_create_from_pixbuf(pixbuf, scale, None)
        return surface

    def render_image(self, image, *_):
        pixel_size = image.get_pixel_size()
        scale = image.get_scale_factor()
        surface = self.render_avatar(pixel_size if pixel_size > 0 else 48, scale)
        image.set_from_surface(surface)

    def set_url(self, url):
        if url != self.url and url is not None:
            self.url = url
            self.render_image(self.image)
            self.emit("image-selected", url)

    def get_url(self):
        return self.url

    def set_fullname(self, name):
        if name is not None and name != self.fullname:
            self.fullname = name
            self.chooser.set_fullname(self.fullname)
            self.render_image(self.image)

    def set_icon_name(self, name):
        if name is not None and name != self.icon_name:
            self.icon_name = name
            self.render_image(self.image)


GObject.type_register(Avatar)
