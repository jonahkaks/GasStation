import cairo
from gi.repository import Gdk, GObject, Gtk, GdkPixbuf, GLib


def CLAMP(x, low, high):
    return high if x > high else low if x < low else x


class Range(GObject.GEnum):
    BELOW = 0
    LOWER = 1
    BETWEEN = 2
    UPPER = 3
    ABOVE = 4


class CropLocation(GObject.GEnum):
    OUTSIDE = 0
    INSIDE = 1
    TOP = 2
    TOP_LEFT = 3
    TOP_RIGHT = 4
    BOTTOM = 5
    BOTTOM_LEFT = 6
    BOTTOM_RIGHT = 7
    LEFT = 8
    RIGHT = 9


class CropArea(Gtk.DrawingArea):

    def __init__(self):
        super().__init__()
        self.browse_pixbuf = None
        self.pixbuf = None
        self.color_shifted = None

        self.image = Gdk.Rectangle()
        self.crop = Gdk.Rectangle()
        self.last_press_x = 0
        self.last_press_y = 0

        self.add_events(
            Gdk.EventMask.POINTER_MOTION_MASK | Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK)
        self.scale = 0.0
        self.image.x = 0
        self.image.y = 0
        self.image.width = 0
        self.image.height = 0
        self.active_region = CropLocation.OUTSIDE
        self.base_width = 48
        self.base_height = 48
        self.aspect = 1
        self.source_id = 0
        self.current_cursor = None

    @staticmethod
    def find_range(x, minimum, maximum):
        tolerance = 12
        if x < minimum - tolerance:
            return Range.BELOW
        if x <= minimum + tolerance:
            return Range.LOWER
        if x < maximum - tolerance:
            return Range.BETWEEN
        if x <= maximum + tolerance:
            return Range.UPPER
        return Range.ABOVE

    @staticmethod
    def find_location(rect, x, y):
        location = [
            (CropLocation.OUTSIDE, CropLocation.OUTSIDE, CropLocation.OUTSIDE, CropLocation.OUTSIDE,
             CropLocation.OUTSIDE),
            (CropLocation.OUTSIDE, CropLocation.TOP_LEFT, CropLocation.TOP, CropLocation.TOP_RIGHT,
             CropLocation.OUTSIDE),
            (CropLocation.OUTSIDE, CropLocation.LEFT, CropLocation.INSIDE, CropLocation.RIGHT, CropLocation.OUTSIDE),
            (CropLocation.OUTSIDE, CropLocation.BOTTOM_LEFT, CropLocation.BOTTOM, CropLocation.BOTTOM_RIGHT,
             CropLocation.OUTSIDE),
            (CropLocation.OUTSIDE, CropLocation.OUTSIDE, CropLocation.OUTSIDE, CropLocation.OUTSIDE,
             CropLocation.OUTSIDE)]
        x_range = CropArea.find_range(x, rect.x, rect.x + rect.width)
        y_range = CropArea.find_range(y, rect.y, rect.y + rect.height)
        return location[y_range][x_range]

    def update_cursor(self, x, y):
        region = self.active_region
        if region == CropLocation.OUTSIDE:
            crop = self.crop_to_widget()
            region = self.find_location(crop, x, y)
        if region == CropLocation.OUTSIDE:
            cursor_type = Gdk.CursorType.LEFT_PTR

        elif region == CropLocation.TOP_LEFT:
            cursor_type = Gdk.CursorType.TOP_LEFT_CORNER

        elif region == CropLocation.TOP:
            cursor_type = Gdk.CursorType.TOP_SIDE

        elif region == CropLocation.TOP_RIGHT:
            cursor_type = Gdk.CursorType.TOP_RIGHT_CORNER

        elif region == CropLocation.LEFT:
            cursor_type = Gdk.CursorType.LEFT_SIDE

        elif region == CropLocation.INSIDE:
            cursor_type = Gdk.CursorType.FLEUR

        elif region == CropLocation.RIGHT:
            cursor_type = Gdk.CursorType.RIGHT_SIDE

        elif region == CropLocation.BOTTOM_LEFT:
            cursor_type = Gdk.CursorType.BOTTOM_LEFT_CORNER

        elif region == CropLocation.BOTTOM:
            cursor_type = Gdk.CursorType.BOTTOM_SIDE

        elif region == CropLocation.BOTTOM_RIGHT:
            cursor_type = Gdk.CursorType.BOTTOM_RIGHT_CORNER
        else:
            return

        if cursor_type != self.current_cursor:
            cursor = Gdk.Cursor.new_for_display(self.get_display(), cursor_type)
            self.get_window().set_cursor(cursor)
            self.current_cursor = cursor_type

    @staticmethod
    def eval_radial_line(center_x, center_y, bounds_x, bounds_y, user_x):
        decision_slope = (bounds_y - center_y) / (bounds_x - center_x)
        decision_intercept = -(decision_slope * bounds_x)
        return int(decision_slope * user_x + decision_intercept)

    def crop_to_widget(self):
        crop = Gdk.Rectangle()
        crop.x = self.image.x + self.crop.x * self.scale
        crop.y = self.image.y + self.crop.y * self.scale
        crop.width = self.crop.width * self.scale
        crop.height = self.crop.height * self.scale
        return crop

    def do_draw(self, cr: cairo.Context):
        if self.browse_pixbuf is None:
            return False
        self.update_pixbufs()

        width = self.pixbuf.get_width()
        height = self.pixbuf.get_height()
        crop = self.crop_to_widget()
        ix = self.image.x
        iy = self.image.y
        Gdk.cairo_set_source_pixbuf(cr, self.color_shifted, ix, iy)
        cr.rectangle(ix, iy, width, crop.y - iy)
        cr.rectangle(ix, crop.y, crop.x - ix, crop.height)
        cr.rectangle(crop.x + crop.width, crop.y, width - crop.width - (crop.x - ix), crop.height)
        cr.rectangle(ix, crop.y + crop.height, width, height - crop.height - (crop.y - iy))
        cr.fill()

        Gdk.cairo_set_source_pixbuf(cr, self.pixbuf, ix, iy)
        cr.rectangle(crop.x, crop.y, crop.width, crop.height)
        cr.fill()

        if self.active_region != CropLocation.OUTSIDE:
            cr.set_source_rgb(1, 1, 1)
            cr.set_line_width(1.0)
            x1 = crop.x + crop.width / 3.0
            x2 = crop.x + 2 * crop.width / 3.0
            y1 = crop.y + crop.height / 3.0
            y2 = crop.y + 2 * crop.height / 3.0

            cr.move_to(x1 + 0.5, crop.y)
            cr.line_to(x1 + 0.5, crop.y + crop.height)

            cr.move_to(x2 + 0.5, crop.y)
            cr.line_to(x2 + 0.5, crop.y + crop.height)

            cr.move_to(crop.x, y1 + 0.5)
            cr.line_to(crop.x + crop.width, y1 + 0.5)

            cr.move_to(crop.x, y2 + 0.5)
            cr.line_to(crop.x + crop.width, y2 + 0.5)
            cr.stroke()

        cr.set_source_rgb(0, 0, 0)
        cr.set_line_width(1.0)
        cr.rectangle(
            crop.x + 0.5,
            crop.y + 0.5,
            crop.width - 1.0,
            crop.height - 1.0)
        cr.stroke()

        cr.set_source_rgb(1, 1, 1)
        cr.set_line_width(2.0)
        cr.rectangle(crop.x + 2.0,
                     crop.y + 2.0,
                     crop.width - 4.0,
                     crop.height - 4.0)
        cr.stroke()
        return False

    def get_picture(self):
        width = self.browse_pixbuf.get_width()
        height = self.browse_pixbuf.get_height()
        width = min(self.crop.width, width - self.crop.x)
        height = min(self.crop.height, height - self.crop.y)
        return self.browse_pixbuf.new_subpixbuf(self.crop.x, self.crop.y, width, height)

    def set_picture(self, pixbuf):
        if self.browse_pixbuf is not None:
            self.browse_pixbuf = None
        if pixbuf is not None:
            self.browse_pixbuf = pixbuf
            width = pixbuf.get_width()
            height = pixbuf.get_height()
        else:
            width = 0
            height = 0

        self.crop.width = 2 * self.base_width
        self.crop.height = 2 * self.base_height
        self.crop.x = (width - self.crop.width) / 2
        self.crop.y = (height - self.crop.height) / 2

        self.scale = 0.0
        self.image.x = 0
        self.image.y = 0
        self.image.width = 0
        self.image.height = 0
        self.queue_draw()

    def set_min_size(self, width, height):
        self.base_width = width
        self.base_height = height
        self.set_size_request(width, height)

        if self.aspect > 0:
            self.aspect = self.base_width / self.base_height

    def set_constrain_aspect(self, constrain):
        if constrain:
            self.aspect = self.base_width / self.base_height
        else:
            self.aspect = -1

    @staticmethod
    def shift_color_byte(b, shift):
        return CLAMP(b + shift, 0, 255)

    @staticmethod
    def shift_colors(pixbuf, red, green, blue, alpha):
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        rowstride = pixbuf.get_rowstride()
        pixels = pixbuf.get_pixels()
        channels = pixbuf.get_n_channels()
        # print(pixels)
        # for y in range(height):
        #     y_offset = y * rowstride
        #     for x in range(width):
        #         offset = y_offset + x * channels
        #         if red != 0:
        #             pixels[offset] = CropArea.shift_color_byte(pixels[offset], red)
        #         if green != 0:
        #             pixels[offset + 1] = CropArea.shift_color_byte(pixels[offset + 1], green)
        #         if blue != 0:
        #             pixels[offset + 2] = CropArea.shift_color_byte(pixels[offset + 2], blue)
        #         if alpha != 0 and channels >= 4:
        #             pixels[offset + 3] = CropArea.shift_color_byte(pixels[offset + 3], blue)

    def update_pixbufs(self):
        allocation = self.get_allocation()
        width = self.browse_pixbuf.get_width()
        height = self.browse_pixbuf.get_height()
        scale = allocation.height / height
        if scale * width > allocation.width:
            scale = allocation.width / width

        dest_width = width * scale
        dest_height = height * scale

        if (self.pixbuf is None or self.pixbuf.get_width() != allocation.width or
                self.pixbuf.get_height() != allocation.height):
            del self.pixbuf
            self.pixbuf = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, self.browse_pixbuf.get_has_alpha(), 8,
                                               dest_width, dest_height)
            self.pixbuf.fill(0x0)
            self.browse_pixbuf.scale(self.pixbuf, 0, 0, dest_width, dest_height, 0, 0, scale, scale,
                                     GdkPixbuf.InterpType.BILINEAR)

            self.color_shifted = self.pixbuf.copy()
            self.shift_colors(self.color_shifted, -32, -32, -32, 0)

            if self.scale == 0.0:
                scale_to_80 = min(self.pixbuf.get_width() * 0.8 / self.base_width,
                                  self.pixbuf.get_height() * 0.8 / self.base_height)
                scale_to_image = min(dest_width / self.base_width,
                                     dest_height / self.base_height)
                crop_scale = min(scale_to_80, scale_to_image)

                self.crop.width = crop_scale * self.base_width / scale
                self.crop.height = crop_scale * self.base_height / scale
                self.crop.x = (self.browse_pixbuf.get_width() - self.crop.width) / 2
                self.crop.y = (self.browse_pixbuf.get_height() - self.crop.height) / 2
            self.scale = scale
            self.image.x = (allocation.width - dest_width) / 2
            self.image.y = (allocation.height - dest_height) / 2
            self.image.width = dest_width
            self.image.height = dest_height

    def do_motion_notify_event(self, event):
        if self.browse_pixbuf is None:
            return False

        self.update_cursor(event.x, event.y)
        damage = self.crop_to_widget()
        GLib.idle_add(self.queue_draw_area, damage.x - 1, damage.y - 1,
                             damage.width + 2, damage.height + 2)
        pb_width = self.browse_pixbuf.get_width()
        pb_height = self.browse_pixbuf.get_height()
        x = (event.x - self.image.x) / self.scale
        y = (event.y - self.image.y) / self.scale

        delta_x = x - self.last_press_x
        delta_y = y - self.last_press_y
        self.last_press_x = x
        self.last_press_y = y

        left = self.crop.x
        right = self.crop.x + self.crop.width - 1
        top = self.crop.y
        bottom = self.crop.y + self.crop.height - 1

        center_x = (left + right) / 2.0
        center_y = (top + bottom) / 2.0

        if self.active_region == CropLocation.INSIDE:
            width = right - left + 1
            height = bottom - top + 1

            left += delta_x
            right += delta_x
            top += delta_y
            bottom += delta_y

            if left < 0:
                left = 0
            if top < 0:
                top = 0
            if right > pb_width:
                right = pb_width
            if bottom > pb_height:
                bottom = pb_height

            adj_width = right - left + 1
            adj_height = bottom - top + 1
            if adj_width != width:
                if delta_x < 0:
                    right = left + width - 1
                else:
                    left = right - width + 1

            if adj_height != height:
                if delta_y < 0:
                    bottom = top + height - 1
                else:
                    top = bottom - height + 1

        elif self.active_region == CropLocation.TOP_LEFT or (self.aspect < 0):
            top = y
            left = x
            if y < self.eval_radial_line(center_x, center_y, left, top, x):
                top = y
                new_width = (bottom - top) * self.aspect
                left = right - new_width

            else:
                left = x
                new_height = (right - left) / self.aspect
                top = bottom - new_height

        elif self.active_region == CropLocation.TOP:
            top = y
            if self.aspect > 0:
                new_width = (bottom - top) * self.aspect
                right = left + new_width

        elif self.active_region == CropLocation.TOP_RIGHT or (self.aspect < 0):
            top = y
            right = x

            if y < self.eval_radial_line(center_x, center_y, right, top, x):
                top = y
                new_width = (bottom - top) * self.aspect
                right = left + new_width

            else:
                right = x
                new_height = (right - left) / self.aspect
                top = bottom - new_height

        elif self.active_region == CropLocation.LEFT:
            left = x
            if self.aspect > 0:
                new_height = (right - left) / self.aspect
                bottom = top + new_height

        elif self.active_region == CropLocation.BOTTOM_LEFT or (self.aspect < 0):
            bottom = y
            left = x

            if y < self.eval_radial_line(center_x, center_y, left, bottom, x):
                left = x
                new_height = (right - left) / self.aspect
                bottom = top + new_height

            else:
                bottom = y
                new_width = (bottom - top) * self.aspect
                left = right - new_width

        elif self.active_region == CropLocation.RIGHT:
            right = x
            if self.aspect > 0:
                new_height = (right - left) / self.aspect
                bottom = top + new_height

        elif self.active_region == CropLocation.BOTTOM_RIGHT or (self.aspect < 0):
            bottom = y
            right = x

            if y < self.eval_radial_line(center_x, center_y, right, bottom, x):
                right = x
                new_height = (right - left) / self.aspect
                bottom = top + new_height

            else:
                bottom = y
                new_width = (bottom - top) * self.aspect
                right = left + new_width

        elif self.active_region == CropLocation.BOTTOM:
            bottom = y
            if self.aspect > 0:
                new_width = (bottom - top) * self.aspect
                right = left + new_width
        else:
            return False

        min_width = self.base_width / self.scale
        min_height = self.base_height / self.scale

        width = right - left + 1
        height = bottom - top + 1
        if self.aspect < 0:
            if left < 0:
                left = 0
            if top < 0:
                top = 0
            if right > pb_width:
                right = pb_width
            if bottom > pb_height:
                bottom = pb_height

            width = right - left + 1
            height = bottom - top + 1

            if self.active_region == CropLocation.LEFT or self.active_region == CropLocation.TOP_LEFT or self.active_region == CropLocation.BOTTOM_LEFT:
                if width < min_width:
                    left = right - min_width
            elif self.active_region == CropLocation.RIGHT or self.active_region == CropLocation.TOP_RIGHT or self.active_region == CropLocation.BOTTOM_RIGHT:
                if width < min_width:
                    right = left + min_width

            elif self.active_region == CropLocation.TOP or self.active_region == CropLocation.TOP_LEFT or self.active_region == CropLocation.TOP_RIGHT:
                if height < min_height:
                    top = bottom - min_height
            elif self.active_region == CropLocation.BOTTOM or self.active_region == CropLocation.BOTTOM_LEFT or self.active_region == CropLocation.BOTTOM_RIGHT:
                if height < min_height:
                    bottom = top + min_height
        else:
            if (left < 0 or top < 0 or
                    right > pb_width or bottom > pb_height or
                    width < min_width or height < min_height):
                left = self.crop.x
                right = self.crop.x + self.crop.width - 1
                top = self.crop.y
                bottom = self.crop.y + self.crop.height - 1

        self.crop.x = left
        self.crop.y = top
        self.crop.width = right - left + 1
        self.crop.height = bottom - top + 1

        damage = self.crop_to_widget()
        GLib.idle_add(self.queue_draw_area,damage.x - 1, damage.y - 1,
                             damage.width + 2, damage.height + 2)

        return False

    def do_button_press_event(self, event):
        if self.browse_pixbuf is None:
            return False

        crop = self.crop_to_widget()

        self.last_press_x = (event.x - self.image.x) / self.scale
        self.last_press_y = (event.y - self.image.y) / self.scale
        self.active_region = self.find_location(crop, event.x, event.y)
        GLib.idle_add(self.queue_draw_area, crop.x - 1, crop.y - 1,
                             crop.width + 2, crop.height + 2)

        return False

    def do_button_release_event(self, event):
        if self.browse_pixbuf is None:
            return False
        crop = self.crop_to_widget()
        self.last_press_x = -1
        self.last_press_y = -1
        self.active_region = CropLocation.OUTSIDE
        GLib.idle_add(self.queue_draw_area, crop.x - 1, crop.y - 1,
                             crop.width + 2, crop.height + 2)

        return False

# def snapshot (self,snapshot):
#     if self.paintable is None:
#         return
# self.update_image_and_crop()
# snapshot.save()
# snapshot.translate(&GRAPHENE_POINT_INIT (self.image.x, self.image.y))
# self.paintable.snapshot (self.paintable, snapshot, self.image.width, self.image.height)
# cr = gtk_snapshot_append_cairo (snapshot, &GRAPHENE_RECT_INIT (0, 0, self.image.width, self.image.height))
#
# crop = self.get_scaled_crop()
# crop.x -= self.image.x
# crop.y -= self.image.y
#
# cr.save()
# cr.arc(crop.x + crop.width / 2, crop.y + crop.width / 2, crop.width / 2, 0, 2 * math.pi)
# cr.rectangle(0, 0, self.image.width, self.image.height)
# cr.set_source_rgba(0, 0, 0, 0.4)
# cr.set_fill_rule(cairo.FILL_RULE_EVEN_ODD)
# cr.fill()
# cr.restore()
#
# cr.set_source_rgb(1, 1, 1)
# cr.set_line_width(4.0)
# cr.move_to(crop.x + 15, crop.y)
# cr.line_to(crop.x, crop.y)
# cr.line_to(crop.x, crop.y + 15)
#
# cr.move_to(crop.x + crop.width - 15, crop.y)
# cr.line_to(crop.x + crop.width, crop.y)
# cr.line_to(crop.x + crop.width, crop.y + 15)
#
# cr.move_to(crop.x + crop.width - 15, crop.y + crop.height)
# cr.line_to(crop.x + crop.width, crop.y + crop.height)
# cr.line_to(crop.x + crop.width, crop.y + crop.height - 15)
# cr.move_to(crop.x + 15, crop.y + crop.height)
# cr.line_to(crop.x, crop.y + crop.height)
# cr.line_to(crop.x, crop.y + crop.height - 15)
# cr.stroke()
# snapshot.restore()
#
# def on_motion(self, controller, event_x, event_y):
#     if self.paintable is None:
#         return False
#     self.update_cursor(event_x, event_y)
#     return False
#
# def on_drag_begin(self, gesture, start_x, start_y):
#     if self.paintable is None:
#         return
#     self.update_cursor(start_x, start_y)
#     crop = self.get_scaled_crop()
#     self.active_region = self.find_location(crop, start_x, start_y)
#     self.drag_offx = 0.0
#     self.drag_offy = 0.0
#
# def on_drag_update(self, gesture, offset_x, offset_y):
#     pb_width = self.paintable.get_intrinsic_width()
#     pb_height = self.paintable.get_intrinsic_height()
#
#     start_x, start_y = gesture.get_start_point()
#     x = (start_x + offset_x - self.image.x) / self.scale
#     y = (start_y + offset_y - self.image.y) / self.scale
#     delta_x = (offset_x - self.drag_offx) / self.scale
#     delta_y = (offset_y - self.drag_offy) / self.scale
#
#     left = self.crop.x
#     right = self.crop.x + self.crop.width - 1
#     top = self.crop.y
#     bottom = self.crop.y + self.crop.height - 1
#
#     center_x = (left + right) / 2.0
#     center_y = (top + bottom) / 2.0
#     if self.active_region == CropLocation.INSIDE:
#         width = right - left + 1
#         height = bottom - top + 1
#
#         left = max(left + delta_x, 0)
#         right = min(right + delta_x, pb_width)
#         top = max(top + delta_y, 0)
#         bottom = min(bottom + delta_y, pb_height)
#
#         adj_width = right - left + 1
#         adj_height = bottom - top + 1
#         if adj_width != width:
#             if delta_x < 0:
#                 right = left + width - 1
#             else:
#                 left = right - width + 1
#
#         if adj_height != height:
#             if delta_y < 0:
#                 bottom = top + height - 1
#             else:
#                 top = bottom - height + 1
#
#     elif self.active_region == CropLocation.TOP_LEFT or y < self.eval_radial_line(center_x, center_y, left, top, x):
#         top = y
#         new_width = bottom - top
#         left = right - new_width
#     else:
#         left = x
#         new_height = right - left
#         top = bottom - new_height
#
#     elif self.active_region == CropLocation.TOP:
#     top = y
#     new_width = bottom - top
#     right = left + new_width
#
# elif self.active_region == CropLocation.TOP_RIGHT or y < self.eval_radial_line(center_x, center_y, right, top, x):
# top = y
# new_width = bottom - top
# right = left + new_width
# else:
# right = x
# new_height = right - left
# top = bottom - new_height
#
# elif self.active_region == CropLocation.LEFT:
# left = x
# new_height = right - left
# bottom = top + new_height
#
#
# elif self.active_region == CropLocation.BOTTOM_LEFT or y < self.eval_radial_line(center_x, center_y, left, bottom, x):
# left = x
# new_height = right - left
# bottom = top + new_height
# else:
# bottom = y
# new_width = bottom - top
# left = right - new_width
#
# elif self.active_region == CropLocation.RIGHT:
# right = x
# new_height = right - left
# bottom = top + new_height
#
#
# elif self.active_region == CropLocation.BOTTOM_RIGHT or y < self.eval_radial_line(center_x, center_y, right, bottom, x):
# right = x
# new_height = right - left
# bottom = top + new_height
# else:
# bottom = y
# new_width = bottom - top
# right = left + new_width
# elif self.active_region == CropLocation.BOTTOM:
# bottom = y
# new_width = bottom - top
# right = left + new_width
# else:
# return
#
# min_width = self.min_crop_width / self.scale
# min_height = self.min_crop_height / self.scale
#
# width = right - left + 1
# height = bottom - top + 1
# if (left < 0 or top < 0 or right > pb_width or bottom > pb_height or width < min_width or height < min_height):
#     left = self.crop.x
#     right = self.crop.x + self.crop.width - 1
#     top = self.crop.y
#     bottom = self.crop.y + self.crop.height - 1
# self.crop.x = left
# self.crop.y = top
# self.crop.width = right - left + 1
# self.crop.height = bottom - top + 1
# self.drag_offx = offset_x
# self.drag_offy = offset_y
# self.queue_draw()
#
# def on_drag_end(self, gesture, offset_x, offset_y):
#     self.active_region = CropLocation.OUTSIDE
#     self.drag_offx = 0.0
#     self.drag_offy = 0.0
#
# def on_drag_cancel(self, gesture, sequece):
#     self.active_region = CropLocation.OUTSIDE
#     self.drag_offx = 0
#     self.drag_offy = 0

# def update_image_and_crop(self):
#     if self.paintable is None:
#         return
#
#     allocation = self.get_allocation()
#     width = self.paintable.get_intrinsic_width()
#     height = self.paintable.get_intrinsic_height()
#
#     scale = allocation.height / height
#     if scale * width > allocation.width:
#         scale = allocation.width / width
#     dest_width = width * scale
#     dest_height = height * scale
#
#     if self.scale == 0.0:
#         scale_to_80 = min(dest_width * 0.8, dest_height * 0.8)
#         scale_to_image = min(self.min_crop_width, self.min_crop_height)
#         crop_scale = max(scale_to_80, scale_to_image)
#
#         self.crop.width = crop_scale / scale
#         self.crop.height = crop_scale / scale
#         self.crop.x = (width - self.crop.width) / 2
#         self.crop.y = (height - self.crop.height) / 2
#
#     self.scale = scale
#     self.image.x = (allocation.width - dest_width) / 2
#     self.image.y = (allocation.height - dest_height) / 2
#     self.image.width = dest_width
#     self.image.height = dest_height
