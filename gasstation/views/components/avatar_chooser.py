import logging
import os.path

import gi

gi.require_version('Cheese', '3.0')
gi.require_version('GnomeDesktop', '3.0')
from gi.repository import Gtk, GnomeDesktop, GdkPixbuf, Gio, GLib, Cheese
from gasstation.utilities.avatar_utils import round_image, generate_default_avatar, IMAGE_SIZE, save_icon
from .avator_crop_area import CropArea

ROW_SPAN = 5
AVATAR_CHOOSER_PIXEL_SIZE = 80
PIXEL_SIZE = 512


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/components/avatar.ui')
class AvatarChooser(Gtk.Popover):
    __gtype_name__ = "AvatarChooser"
    user_flowbox: Gtk.FlowBox = Gtk.Template.Child()
    flowbox: Gtk.FlowBox = Gtk.Template.Child()
    take_picture_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, transient_for):
        super().__init__()
        self.faces = None
        self.images_paths = []
        self.transient_for = transient_for
        self.thumb_factory: GnomeDesktop.DesktopThumbnailFactory = GnomeDesktop.DesktopThumbnailFactory.new(
            GnomeDesktop.DesktopThumbnailSize.NORMAL)
        self.crop_area = None
        self.selected_image = None
        self.fullname = None
        self.cancellable = Gio.Cancellable.new()
        self.monitor = None
        self.num_cameras = 0


    def dialog_response(self, dialog, response):
        if response != Gtk.ResponseType.ACCEPT:
            self.crop_area = None
            dialog.destroy()
            return
        pb = self.crop_area.get_picture()
        pb2 = pb.scale_simple(PIXEL_SIZE, PIXEL_SIZE, GdkPixbuf.InterpType.BILINEAR)
        self.selected_image = pb2
        self.crop_area = None
        dialog.destroy()
        self.popdown()

    def crop(self, pixbuf):
        dialog: Gtk.Dialog = Gtk.Dialog(title="", parent=self.transient_for,
                                        destroy_with_parent=True, modal=True,
                                        buttons=("_Cancel", Gtk.ResponseType.CANCEL,
                                                 "_Select", Gtk.ResponseType.ACCEPT))
        dialog.set_modal(True)
        dialog.set_icon_name("system-users")
        dialog.connect("response", self.dialog_response)
        self.crop_area = CropArea()
        self.crop_area.show()
        self.crop_area.set_min_size(48, 48)
        self.crop_area.set_constrain_aspect(True)
        self.crop_area.set_picture(pixbuf)
        dialog.get_content_area().pack_start(self.crop_area, True, True, 8)
        self.crop_area.set_hexpand(True)
        self.crop_area.set_vexpand(True)
        dialog.set_default_size(400, 300)
        dialog.show()

    def file_chooser_response(self, chooser, response):
        if response != Gtk.ResponseType.ACCEPT:
            chooser.destroy()
            return
        filename = chooser.get_filename()
        pixbuf = None
        try:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(filename)
        except GLib.Error as error:
            if pixbuf is None:
                logging.warning("Failed to load %s: %s", filename, error)
            return

        pixbuf2 = pixbuf.apply_embedded_orientation()
        chooser.destroy()
        GLib.idle_add(self.crop, pixbuf2)

    def update_preview(self, chooser):
        uri = chooser.get_uri()
        mime_type = None
        pixbuf = None
        if uri is not None:
            preview = chooser.get_preview_widget()
            file = Gio.File.new_for_uri(uri)
            file_info = file.query_info("standard::*", flags=Gio.FileQueryInfoFlags.NONE, )
            if file_info is not None and file_info.get_file_type() != Gio.FileType.DIRECTORY:
                mime_type = file_info.get_content_type()

            if mime_type is not None:
                pixbuf = self.thumb_factory.generate_thumbnail(uri, mime_type)
            chooser.set_response_sensitive(Gtk.ResponseType.ACCEPT, pixbuf is not None)
            if pixbuf is not None:
                preview.set_from_pixbuf(pixbuf)
            else:
                preview.set_from_icon_name("dialog-question", Gtk.IconSize.DIALOG)
        chooser.set_preview_widget_active(True)

    @Gtk.Template.Callback()
    def select_file(self, _):
        chooser = Gtk.FileChooserDialog(title="Browse for more pictures", parent=self.transient_for,
                                        action=Gtk.FileChooserAction.OPEN, buttons=("_Cancel", Gtk.ResponseType.CANCEL,
                                                                                    "_Open", Gtk.ResponseType.ACCEPT))
        chooser.set_modal(True)
        preview = Gtk.Image()
        preview.set_size_request(128, -1)
        chooser.set_preview_widget(preview)
        chooser.set_use_preview_label(False)
        preview.show()

        folder = Gio.File.new_for_path(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_PICTURES))
        if folder is not None:
            chooser.set_current_folder("")
        filter = Gtk.FileFilter()
        filter.add_pixbuf_formats()
        chooser.set_filter(filter)
        chooser.connect_after("selection-changed", self.update_preview)
        chooser.connect("response", self.file_chooser_response)
        chooser.present()

    def face_widget_activated(self, flowbox, child):
        image = child.get_child()
        self.selected_image = image
        self.popdown()

    def create_face_widget(self, item: Gio.File):
        image_path = item.get_path()
        source_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(image_path, AVATAR_CHOOSER_PIXEL_SIZE,
                                                               AVATAR_CHOOSER_PIXEL_SIZE)
        if source_pixbuf is None:
            return None
        pixbuf = round_image(source_pixbuf)
        image = Gtk.Image.new_from_pixbuf(pixbuf)
        image.set_pixel_size(AVATAR_CHOOSER_PIXEL_SIZE)
        image.show()
        return image

    @staticmethod
    def get_settings_facesdirs():
        settings: Gio.Settings = Gio.Settings.new("org.gnome.desktop.interface")
        settings_dirs = settings.get_strv("avatar-directories")
        return settings_dirs

    @staticmethod
    def get_system_facesdirs():
        return [os.path.join(d, "pixmaps", "faces") for d in GLib.get_system_data_dirs()]

    @staticmethod
    def add_faces_from_dirs(faces: Gtk.ListStore, facesdirs: list, add_all: bool):
        added_faces = False

        for d in facesdirs:
            directory = Gio.File.new_for_path(d)
            try:
                enumerator = directory.enumerate_children(Gio.FILE_ATTRIBUTE_STANDARD_NAME + "," +
                                                          Gio.FILE_ATTRIBUTE_STANDARD_TYPE + "," +
                                                          Gio.FILE_ATTRIBUTE_STANDARD_IS_SYMLINK + "," +
                                                          Gio.FILE_ATTRIBUTE_STANDARD_SYMLINK_TARGET,
                                                          Gio.FileQueryInfoFlags.NONE, None)
            except GLib.Error:
                continue
            while 1:
                info = enumerator.next_file(None)
                if info is None:
                    break
                file_type = info.get_file_type()
                if file_type != Gio.FileType.REGULAR and file_type != Gio.FileType.SYMBOLIC_LINK:
                    continue
                target = info.get_symlink_target()
                if target is not None and target.startswith("legacy/"):
                    continue
                file = directory.get_child(info.get_name())
                faces.append(file)
                added_faces = True
            enumerator.close()
            if added_faces and not add_all:
                break
        return added_faces

    def setup_photo_popup(self, _):
        if self.faces is None:
            self.faces = Gio.ListStore.new(Gio.File)
            self.flowbox.bind_model(self.faces, self.create_face_widget)
            self.flowbox.connect("child-activated", self.face_widget_activated)
            settings_facesdirs = self.get_settings_facesdirs()
            if not self.add_faces_from_dirs(self.faces, settings_facesdirs, True):
                system_facesdirs = self.get_system_facesdirs()
                self.add_faces_from_dirs(self.faces, system_facesdirs, False)
        if self.monitor is None:
            self.take_picture_button.set_visible(True)
            Cheese.CameraDeviceMonitor.new_async(self.cancellable, self.cheese_camera_device_monitor_new_cb)

    def user_flowbox_activated(self, *_):
        self.selected_image = generate_default_avatar(self.fullname, IMAGE_SIZE)
        self.popdown()

    def set_fullname(self, fullname):
        if self.fullname is not None:
            child = self.user_flowbox.get_child_at_index(0)
            while child is not None:
                next_child = child.get_next_sibling()
                if child is not None:
                    self.user_flowbox.remove(child)
                child = next_child
            print("CHOOSER FULL NAME*********************************", fullname)
            self.fullname = fullname
            source_pixbuf = generate_default_avatar(fullname, AVATAR_CHOOSER_PIXEL_SIZE)
            pixbuf = round_image(source_pixbuf)
            image = Gtk.Image.new_from_pixbuf(pixbuf)
            image.set_pixel_size(AVATAR_CHOOSER_PIXEL_SIZE)
            image.show()
            self.user_flowbox.add(image)
            self.user_flowbox.connect("child-activated", self.user_flowbox_activated)
            self.selected_image = image

    def get_selected_url(self):
        if self.selected_image is not None:
            return save_icon(self.selected_image)

    def webcam_response_cb(self, response, dialog):
        if response == Gtk.ResponseType.ACCEPT:
            # g_object_get (G_OBJECT (dialog), "pixbuf", &pb, NULL)
            pb = None
            pb2 = pb.scale_simple(PIXEL_SIZE, PIXEL_SIZE, GdkPixbuf.InterpType.BILINEAR)
            self.selected_image = pb2
        if response != Gtk.ResponseType.DELETE_EVENT and response != Gtk.ResponseType.NONE:
            GLib.idle_add(dialog.destroy)
        self.popdown()

    @Gtk.Template.Callback()
    def webcam_icon_selected(self, _):
        # window = Cheese.AvatarChooser()
        # window.set_transient_for(self.get_toplevel())
        # window.set_modal(True)
        # window.connect("response", self.webcam_response_cb)
        # window.show()
        pass

    def update_photo_menu_status(self):
        if self.num_cameras == 0:
            self.take_picture_button.set_visible(False)
        else:
            self.take_picture_button.set_sensitive(True)

    def device_added(self, monitor, device):
        self.num_cameras += 1
        self.update_photo_menu_status()

    def device_removed(self,  monitor, device):
        self.num_cameras -= 1
        self.update_photo_menu_status()

    def setup_cheese_camera_device_monitor(self):
        self.monitor.connect("added", self.device_added)
        self.monitor.connect("removed", self.device_removed)
        self.monitor.coldplug()
        self.update_photo_menu_status()

    def cheese_camera_device_monitor_new_cb(self, source_object, result, *userdata):
        try:
            self.monitor = Cheese.CameraDeviceMonitor.new_finish(result)
            self.setup_cheese_camera_device_monitor()
        except GLib.GError:
            pass

