from gi.repository import Gtk, GObject, GLib

from gasstation.views.selections import CountrySelection, EntryCompletionSelection
from libgasstation import Address, Session, ID_ADDRESS


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/components/address.ui')
class AddressFrame(Gtk.Frame):
    __gtype_name__ = "AddressFrame"
    street_entry: Gtk.TextView = Gtk.Template.Child()
    city_entry: EntryCompletionSelection = Gtk.Template.Child()
    state_entry: EntryCompletionSelection = Gtk.Template.Child()
    country_entry: CountrySelection = Gtk.Template.Child()
    postal_entry: Gtk.Entry = Gtk.Template.Child()
    grid: Gtk.Grid = Gtk.Template.Child()

    def __init__(self):
        super().__init__()
        self.address = None
        self.street_entry_buffer = self.street_entry.get_buffer()
        self.country_entry.connect("changed", lambda w: GLib.idle_add(self.state_entry.reload_model,
                                                                      self.country_entry.get_selected_cities()))
        self.state_entry.connect("changed", lambda w: GLib.idle_add(self.city_entry.reload_model,
                                                                    self.load_towns(w.get_text())))

    def load_towns(self, district):
        book = Session.get_current_book()
        return [addr.get_city() for addr in book.get_collection(ID_ADDRESS).get_all() if district == addr.get_state()]

    def set_address(self, address: Address):
        self.street_entry_buffer.set_text(address.get_line1() or "")
        self.city_entry.set_text(address.get_city() or "")
        self.country_entry.set_country(address.get_country() or "")
        self.postal_entry.set_text(address.get_postal_code() or "")
        self.state_entry.set_text(address.get_state() or "")
        self.address = address

    def set_grid_sensitive(self, sensitive):
        self.grid.set_sensitive(sensitive)

    def copy_to_address(self, ba):
        start = self.street_entry_buffer.get_start_iter()
        end = self.street_entry_buffer.get_end_iter()
        ba.set_line1(self.street_entry_buffer.get_text(start, end, False).strip())
        ba.set_city(self.city_entry.get_text().strip())
        ba.set_country(self.country_entry.get_country().strip())
        ba.set_postal_code(self.postal_entry.get_text().strip())
        ba.set_state(self.state_entry.get_text().strip())

    def share_buffer(self, other):
        self.street_entry.set_buffer(other.street_entry_buffer if other is not None else Gtk.TextBuffer())
        self.city_entry.set_buffer(other.city_entry.get_buffer() if other is not None else Gtk.EntryBuffer())
        self.state_entry.set_buffer(other.state_entry.get_buffer() if other is not None else Gtk.EntryBuffer())
        self.country_entry.set_buffer(other.country_entry.get_buffer() if other is not None else Gtk.EntryBuffer())
        self.postal_entry.set_buffer(other.postal_entry.get_buffer() if other is not None else Gtk.EntryBuffer())

    def validate(self):
        start = self.street_entry_buffer.get_start_iter()
        end = self.street_entry_buffer.get_end_iter()
        return self.street_entry_buffer.get_text(start, end, False) != ""


GObject.type_register(AddressFrame)
