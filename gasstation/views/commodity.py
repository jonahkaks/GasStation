# -*- coding: utf-8 -*-
from gasstation.models.commodity import *
from .tree import *


class FilterUserData:
    user_ns_fn = None
    user_cm_fn = None
    user_pc_fn = None
    user_data = None
    user_destroy = None


class CommodityView(TreeView):
    __gtype_name__ = "CommodityView"

    def __init__(self, book, *args, **kwargs):
        super().__init__(*args, **kwargs)
        ct = CommodityTable.get_table(book)
        model = CommodityModel.new(book, ct)
        f_model = model.filter_new()
        s_model = Gtk.TreeModelSort.new_with_model(f_model)
        self.set_name("commodity_tree")
        self.set_model(s_model)
        self.set_headers_visible(False)
        self.add_text_column(0, title="Namespace", pref_name="namespace", sizing_text="NASDAQ",
                             data_col=CommodityModelColumn.NAMESPACE,
                             sort_func=self.sort_by_commodity_string)
        self.add_text_column(1, title="Symbol", pref_name="symbol", sizing_text="ACMEACME", visible_default=True,
                             data_col=CommodityModelColumn.MNEMONIC, visible_col=CommodityModelColumn.VISIBILITY,
                             sort_func=self.sort_by_commodity_string)
        self.add_text_column(2, title="Name", pref_name="name", sizing_text="Acme Corporation, Inc.",visible_default=True,
                             data_col=CommodityModelColumn.FULLNAME, visible_col=CommodityModelColumn.VISIBILITY,
                             sort_func=self.sort_by_commodity_string)
        self.add_text_column(3, title="Print Name", pref_name="print-name",
                             sizing_text="ACMEACME (Acme Corporation, Inc.)", data_col=CommodityModelColumn.PRINTNAME,
                             visible_col=CommodityModelColumn.VISIBILITY, sort_func=self.sort_by_commodity_string)
        self.add_text_column(4, title="Display symbol", pref_name="user-symbol", sizing_text="ACME",
                             data_col=CommodityModelColumn.USER_SYMBOL, visible_col=CommodityModelColumn.VISIBILITY,
                             sort_func=self.sort_by_commodity_string)
        self.add_text_column(5, title="Unique Name", pref_name="unique-name", sizing_text="NASDAQ::ACMEACME",
                             data_col=CommodityModelColumn.UNIQUE_NAME, visible_col=CommodityModelColumn.VISIBILITY,
                             sort_func=self.sort_by_commodity_string)
        self.add_text_column(6, title="ISIN/CUSIP", pref_name="cusip-code", sizing_text="US1234567890",
                             data_col=CommodityModelColumn.CUSIP, visible_col=CommodityModelColumn.VISIBILITY,
                             sort_func=self.sort_by_commodity_string)
        self.add_numeric_column(7, title="Fraction", pref_name="fraction", sizing_text="10000",
                                data_col=CommodityModelColumn.FRACTION, visible_col=CommodityModelColumn.VISIBILITY,
                                sort_func=self.sort_by_fraction)
        self.add_toggle_column(8, title="Get Quotes", short_title="Q", pref_name="quote-flag",
                               data_col=CommodityModelColumn.QUOTE_FLAG,
                               visible_col=CommodityModelColumn.VISIBILITY, sort_func=self.sort_by_quote_flag)
        self.add_text_column(9, title="Source", pref_name="quote-source", sizing_text="alphavantage",
                             data_col=CommodityModelColumn.QUOTE_SOURCE,
                             visible_col=CommodityModelColumn.VISIBILITY,
                             sort_func=self.sort_by_commodity_string)
        self.add_text_column(10, title="Timezone", pref_name="quote-timezone", sizing_text="America/New_York",
                             data_col=CommodityModelColumn.QUOTE_TZ, visible_col=CommodityModelColumn.VISIBILITY,
                             sort_func=self.sort_by_commodity_string)

        self.configure_columns()
        if not s_model.get_sort_column_id():
            s_model.set_sort_column_id(CommodityModelColumn.FULLNAME, Gtk.SortType.ASCENDING)
        self.show()

    @staticmethod
    def get_commodities_w_iters(f_model, f_iter_a, f_iter_b):
        model = f_model.get_model()
        iter_a = f_model.convert_iter_to_child_iter(f_iter_a)
        iter_b = f_model.convert_iter_to_child_iter(f_iter_b)

        if not model.iter_is_commodity(iter_a):
            return False
        if not model.iter_is_commodity(iter_b):
            return False
        comm_a = model.get_commodity(iter_a)
        comm_b = model.get_commodity(iter_b)
        return model, iter_a, iter_b, comm_a, comm_b

    @classmethod
    def get_commodities(cls, f_model, f_iter_a, f_iter_b):
        model, iter_a, iter_b, comm_a, comm_b = cls.get_commodities_w_iters(f_model, f_iter_a, f_iter_b)
        return comm_a, comm_b

    @staticmethod
    def sort_namespace(f_model, f_iter_a, f_iter_b):
        model = f_model.get_model()
        iter_a = f_model.convert_iter_to_child_iter(f_iter_a)
        iter_b = f_model.convert_iter_to_child_iter(f_iter_b)
        ns_a = model.get_namespace(iter_a)
        ns_b = model.get_namespace(iter_b)
        return GLib.utf8_collate(ns_a.get_gui_name(), ns_b.get_gui_name())

    @staticmethod
    def default_sort(comm_a, comm_b):
        result = GLib.utf8_collate(comm_a.get_namespace(),
                                   comm_b.get_namespace())
        if result != 0:
            return result

        result = GLib.utf8_collate(comm_a.get_mnemonic(),
                                   comm_b.get_mnemonic())
        if result != 0: return result

        result = GLib.utf8_collate(comm_a.get_full_name(),
                                   comm_b.get_full_name())
        if result != 0: return result

        result = GLib.utf8_collate(comm_a.get_cusip(),
                                   comm_b.get_cusip())
        if result != 0: return result

        fraction_a = comm_a.get_fraction()
        fraction_b = comm_b.get_fraction()

        if fraction_a < fraction_b:
            return -1
        if fraction_b < fraction_a:
            return 1

        return 0

    @classmethod
    def sort_by_commodity_string(cls, f_model, f_iter_a, f_iter_b, column):
        if not cls.get_commodities_w_iters(f_model, f_iter_a, f_iter_b):
            return cls.sort_namespace(f_model, f_iter_a, f_iter_b)
        model, iter_a, iter_b, comm_a, comm_b = cls.get_commodities_w_iters(f_model, f_iter_a, f_iter_b)
        str1 = model.get_value(iter_a, column)
        str2 = model.get_value(iter_b, column)
        result = GLib.utf8_collate(str1, str2)
        if result != 0:
            return result
        return cls.default_sort(comm_a, comm_b)

    @classmethod
    def sort_by_fraction(cls, f_model, f_iter_a, f_iter_b, column):
        if not cls.get_commodities(f_model, f_iter_a, f_iter_b):
            return cls.sort_namespace(f_model, f_iter_a, f_iter_b)
        comm_a, comm_b = cls.get_commodities(f_model, f_iter_a, f_iter_b)
        flag_a = comm_a.get_fraction()
        flag_b = comm_b.get_fraction()

        if flag_a < flag_b:
            return -1
        elif flag_a > flag_b:
            return 1
        return cls.default_sort(comm_a, comm_b)

    @classmethod
    def sort_by_quote_flag(cls, f_model, f_iter_a, f_iter_b, column):
        if not cls.get_commodities(f_model, f_iter_a, f_iter_b):
            return cls.sort_namespace(f_model, f_iter_a, f_iter_b)
        comm_a, comm_b = cls.get_commodities(f_model, f_iter_a, f_iter_b)
        flag_a = comm_a.get_quote_flag()
        flag_b = comm_b.get_quote_flag()

        if flag_a < flag_b:
            return -1
        elif flag_a > flag_b:
            return 1
        return cls.default_sort(comm_a, comm_b)

    def get_iter_from_price(self, price):
        if price is None: return False
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()
        t, _iter = model.get_iter_from_price(price)
        if not t: return False
        f_iter = f_model.convert_child_iter_to_iter(_iter)
        s_iter = s_model.convert_child_iter_to_iter(f_iter)
        return s_iter

    @staticmethod
    def filter_destroy(fd):
        if fd.user_destroy:
            fd.user_destroy(fd.user_data)

    @staticmethod
    def filter_helper(model, _iter, fd):
        if model is None:
            return False
        if _iter is None:
            return False
        if model.iter_is_namespace(_iter):
            if fd.user_ns_fn:
                name_space = model.get_namespace(_iter)
                return fd.user_ns_fn(name_space, fd.user_data)
            return True
        if model.iter_is_commodity(_iter):
            if fd.user_cm_fn:
                commodity = model.get_commodity(_iter)
                return fd.user_cm_fn(commodity, fd.user_data)
            return True
        return False

    def set_filter(self, ns_func, cm_func, data, destroy):
        if ns_func is None or cm_func is None: return
        fd = FilterUserData()
        fd.user_ns_fn = ns_func
        fd.user_cm_fn = cm_func
        fd.user_data = data
        fd.user_destroy = destroy
        s_model = self.get_model()
        f_model = s_model.get_model()
        f_model.set_visible_func(self.filter_helper, fd)
        f_model.refilter()

    def refilter(self):
        s_model = self.get_model()
        f_model = s_model.get_model()
        f_model.refilter()

    def get_selected_commodity(self):
        selection = self.get_selection()
        s_model, s_iter = selection.get_selected()
        if s_iter is None: return False
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        comm = model.get_commodity(_iter)
        return comm

    def set_selected_commodity(self, commodity):
        selection = self.get_selection()
        selection.unselect_all()
        if commodity is None:
            return
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()
        path = model.get_path_from_commodity(commodity)
        if path is None:
            return
        f_path = f_model.convert_child_path_to_path(path)
        if f_path is None: return
        s_path = s_model.convert_child_path_to_path(f_path)
        if s_path is None: return
        parent_path = s_path.copy()
        if parent_path.up():
            self.expand_to_path(parent_path)
        selection.select_path(s_path)
        self.scroll_to_cell(s_path, None, False, 0.0, 0.0)


GObject.type_register(CommodityView)
