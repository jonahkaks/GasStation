from .core import SearchCore
from .core_utils import SearchCombo
from libgasstation.core.query_core import *
from gasstation.utilities.custom_dialogs import show_error
from gi.repository import Gtk
import re


class SearchStringType(IntEnum):
    CONTAINS = 0
    NOT_CONTAINS = 1
    MATCHES_REGEX = 2
    NOT_MATCHES_REGEX = 3
    EQUAL = 4
    NOT_EQUAL = 5


class SearchString(SearchCore):
    type_name = TYPE_STRING

    def __init__(self):
        super().__init__()
        self.how = SearchStringType.CONTAINS
        self.value = None
        self.ign_case = True
        self.entry = None
        self.parent = None

    def validate(self):
        if self.value is None or self.value == "":
            show_error(parent=self.parent, info="You need to enter some search text.")
            return False
        if self.how == SearchStringType.NOT_MATCHES_REGEX or self.how == SearchStringType.MATCHES_REGEX:
            flags = re.RegexFlag.X
            if self.ign_case:
                flags |= re.RegexFlag.IGNORECASE
            try:
                re.compile(self.value, flags)
            except re.error as e:
                show_error(self.parent, e.args[1])
                return False
        return True

    def set_value(self, value):
        if isinstance(value, str):
            self.value = value

    def set_how(self, how):
        if isinstance(how, SearchStringType):
            self.how = how

    def set_case(self, ignore_case: bool):
        self.ign_case = ignore_case

    def toggle_changed(self, button):
        self.ign_case = not button.get_active()

    def pass_parent(self, parent):
        self.parent = parent

    def entry_changed(self):
        self.value = self.entry.get_text()

    def make_menu(self):
        combo = SearchCombo()
        combo.append("contains", SearchStringType.CONTAINS)
        combo.append("equals", SearchStringType.EQUAL)
        combo.append("matches regex", SearchStringType.MATCHES_REGEX)
        combo.append("does not match regex", SearchStringType.NOT_MATCHES_REGEX)
        combo.changed(self.set_how)
        combo.set_ref(self.how if self.how else SearchStringType.CONTAINS)
        return combo

    def grab_focus(self):
        if self.entry is not None:
            self.entry.grab_focus()

    def editable_enters(self):
        if self.entry is not None:
            self.entry.set_activates_default(True)

    def get_widget(self):
        box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 3)
        box.set_homogeneous(False)
        menu = self.make_menu()
        box.pack_start(menu, False, False, 3)
        entry = Gtk.Entry()
        if self.value is not None:
            entry.set_text(self.value)
        entry.connect("changed", self.entry_changed)
        box.pack_start(entry, False, False, 3)
        self.entry = entry

        toggle = Gtk.CheckButton.new_with_label("Match case")
        toggle.connect("toggled", self.toggle_changed)
        box.pack_start(toggle, False, False, 3)
        return box

    def get_predicate(self):
        options = StringMatch.NORMAL
        is_regex = False
        if self.how == SearchStringType.MATCHES_REGEX:
            is_regex = True
            how = QueryCompare.CONTAINS
        elif self.how == SearchStringType.CONTAINS:
            how = QueryCompare.CONTAINS
        elif self.how == SearchStringType.EQUAL:
            how = QueryCompare.EQUAL
        elif self.how == SearchStringType.NOT_MATCHES_REGEX:
            is_regex = True
            how = QueryCompare.NCONTAINS
        elif self.how == SearchStringType.NOT_CONTAINS:
            how = QueryCompare.NCONTAINS
        elif self.how == SearchStringType.NOT_EQUAL:
            how = QueryCompare.NEQ
        else:
            return None
        if self.ign_case:
            options = StringMatch.CASEINSENSITIVE
        return QueryCore.string_predicate(how, self.value, options, is_regex)

    def clone(self):
        se = SearchString()
        se.set_value(self.value)
        se.set_how(self.how)
        se.set_case(self.ign_case)
        return se


SearchCore.register(SearchString)
