from gi.repository import Gtk

from gasstation.views.selections.amount_edit import AmountEntry
from libgasstation.core.query_private import *
from .core import SearchCore
from .core_utils import SearchCombo


class SearchDouble(SearchCore):
    type_name = TYPE_DOUBLE

    def __init__(self):
        super().__init__()
        self.how = QueryCompare.EQUAL
        self.value = 0.0
        self.entry = None
        self.parent = None

    def set_value(self, value):
        if isinstance(value, (int, float)):
            self.value = value

    def set_how(self, how):
        if isinstance(how, QueryCompare):
            self.how = how

    def pass_parent(self, parent):
        self.parent = parent

    def entry_changed(self):
        self.value = float(self.entry.get_amount())

    def make_menu(self):
        combo = SearchCombo()
        combo.append("is less than", QueryCompare.LT)
        combo.append("is less than or equal to", QueryCompare.LTE)
        combo.append("equals", QueryCompare.EQUAL)
        combo.append("does not equal", QueryCompare.NEQ)
        combo.append("is greater than", QueryCompare.GT)
        combo.append("is greater than or equal to", QueryCompare.GTE)
        combo.changed(self.set_how)
        combo.set_ref(self.how if self.how else QueryCompare.LT)
        return combo

    def grab_focus(self):
        if self.entry is not None:
            self.entry.grab_focus()

    def editable_enters(self):
        if self.entry is not None:
            self.entry.set_activates_default(True)

    def get_widget(self):
        box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 3)
        box.set_homogeneous(False)
        menu = self.make_menu()
        box.pack_start(menu, False, False, 3)
        entry = AmountEntry()
        if self.value:
            entry.set_amount(Decimal(self.value))
        entry.connect("amount_changed", self.entry_changed)
        box.pack_start(entry, False, False, 3)
        self.entry = entry
        return box

    def get_predicate(self):
        self.entry_changed()
        return QueryCore.double_predicate(self.how, self.value)

    def clone(self):
        se = SearchDouble()
        se.set_value(self.value)
        se.set_how(self.how)
        return se


SearchCore.register(SearchDouble)
