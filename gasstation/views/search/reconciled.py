from gi.repository import Gtk

from libgasstation.core.query_private import *
from libgasstation.core.transaction import RECONCILED_MATCH_TYPE, SplitState
from .core import SearchCore
from .core_utils import SearchCombo


class SearchReconciled(SearchCore):
    type_name = RECONCILED_MATCH_TYPE

    def __init__(self):
        super().__init__()
        self.how = EnumMatch.ANY
        self.value = [SplitState.UNRECONCILED]
        self.entry = None
        self.parent = None

    def set_value(self, value):
        self.value = value

    def set_how(self, how):
        self.how = how

    def pass_parent(self, parent):
        self.parent = parent

    def make_toggle(self, label, option):
        toggle = Gtk.CheckButton.new_with_label(label)
        toggle.set_active(option in self.value)
        toggle.connect("toggled", self.toggle_changed, option)
        return toggle

    def toggle_changed(self, button, value):
        is_on = button.get_active()
        if is_on:
            if value not in self.value:
                self.value.append(value)
        else:
            if value in self.value:
                self.value.remove(value)

    def make_menu(self):
        combo = SearchCombo()
        combo.append("is", EnumMatch.ANY)
        combo.append("is not", EnumMatch.NONE)
        combo.changed(self.set_how)
        combo.set_ref(self.how if self.how else EnumMatch.ANY)
        return combo

    def grab_focus(self):
        if self.entry is not None:
            self.entry.grab_focus()

    def editable_enters(self):
        if self.entry is not None:
            self.entry.set_activates_default(True)

    def get_widget(self):
        box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 3)
        box.set_homogeneous(False)
        menu = self.make_menu()
        box.pack_start(menu, False, False, 3)
        toggle = self.make_toggle("Not Cleared", SplitState.UNRECONCILED)
        box.pack_start(toggle, False, False, 3)
        toggle = self.make_toggle("Cleared", SplitState.CLEARED)
        box.pack_start(toggle, False, False, 3)
        toggle = self.make_toggle("Reconciled", SplitState.RECONCILED)
        box.pack_start(toggle, False, False, 3)
        toggle = self.make_toggle("Frozen", SplitState.FROZEN)
        box.pack_start(toggle, False, False, 3)
        toggle = self.make_toggle("Voided", SplitState.VOIDED)
        box.pack_start(toggle, False, False, 3)
        return box

    def get_predicate(self):
        return QueryCore.enum_predicate(self.how, self.value)

    def clone(self):
        se = SearchReconciled()
        se.set_value(self.value)
        se.set_how(self.how)
        return se


SearchCore.register(SearchReconciled)
