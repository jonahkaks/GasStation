class SearchCore:
    type_name = "SEARCH"
    __type_table = {}

    def __init__(self):
        self.parent = None
        self.param = None

    def grab_focus(self):
        return

    def editable_enters(self):
        return

    def pass_parent(self, parent):
        raise NotImplementedError

    def validate(self):
        return True

    def clone(self):
        raise NotImplementedError

    def get_widget(self):
        raise NotImplementedError

    def get_predicate(self):
        raise NotImplementedError

    @classmethod
    def register(cls, obj_class):
        cls.__type_table[obj_class.type_name] = obj_class

