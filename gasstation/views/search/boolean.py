from gi.repository import Gtk

from libgasstation.core.query_private import *
from .core import SearchCore


class SearchBoolean(SearchCore):
    type_name = TYPE_BOOLEAN

    def __init__(self):
        super().__init__()
        self.value = True
        self.parent = None

    def set_value(self, value):
        self.value = value

    def pass_parent(self, parent):
        self.parent = parent

    def toggle_changed(self, button):
        self.value = button.get_active()

    def get_widget(self):
        box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 3)
        box.set_homogeneous(False)
        toggle = Gtk.CheckButton()
        toggle.set_active(self.value)
        toggle.connect("toggled", self.toggle_changed)
        box.pack_start(toggle, False, False, 3)
        return box

    def get_predicate(self):
        return QueryCore.boolean_predicate(QueryCompare.EQUAL, self.value)

    def clone(self):
        se = SearchBoolean()
        se.set_value(self.value)
        return se


SearchCore.register(SearchBoolean)
