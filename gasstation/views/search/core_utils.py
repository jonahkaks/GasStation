from gi.repository import Gtk


class SearchCombo(Gtk.ComboBox):
    def __init__(self):
        self.store = Gtk.ListStore(str, int)
        super().__init__(model=self.store)
        renderer = Gtk.CellRendererText()
        self.pack_start(renderer, True)
        self.add_attribute(renderer, "text", 0)

    def append(self, text, value):
        self.store.append([text, value])

    def get_ref(self):
        _iter = self.get_active_iter()
        if _iter is None:
            return 0
        return self.store.get_value(_iter, 1)

    def set_ref(self, value):
        _iter = self.store.get_iter_first()
        while _iter is not None:
            row_value = self.store.get_value(_iter, 1)
            if row_value == value:
                self.set_active_iter(_iter)
                return
            _iter = self.model.iter_next(_iter)
        super().set_active(0)

    def changed(self, setter):
        self.connect("changed", self.combo_changed, setter)

    def combo_changed(self, widget, setter):
        _iter = self.get_active_iter()
        if _iter is None:
            return
        v = self.store.get_value(_iter, 1)
        setter(v)
