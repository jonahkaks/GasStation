from gasstation.models.scheduled_list import ScheduledListModel, ScheduledListModelCol
from .tree import *


class ScheduledListView(TreeView):
    def __init__(self, scheduled_instances, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tree_model = None
        self.set_property("name", "scheduled_list_tree")
        self.tree_model = ScheduledListModel.new(scheduled_instances)
        self.set_model(self.tree_model.real)
        self.add_text_column(0, title="Name", pref_name="name", sizing_text="Semi-Monthly Paycheck",
                             data_col=ScheduledListModelCol.NAME, visible_always=True)
        self.add_toggle_column(1, title="Enabled", short_title="E", pref_name="enabled",
                               data_col=ScheduledListModelCol.ENABLED, visible_always=True)
        self.add_text_column(2, title="Frequency", pref_name="frequency", sizing_text="Weekly (x3): -------",
                             visible_default=True, data_col=ScheduledListModelCol.FREQUENCY)
        self.add_text_column(3, title="Last Occur", pref_name="last-occur", sizing_text="2007-01-02",
                             visible_default=True, data_col=ScheduledListModelCol.LAST_OCCUR)
        self.add_text_column(4, title="Next Occur", pref_name="next-occur", sizing_text="2007-01-02",
                             visible_default=True, data_col=ScheduledListModelCol.NEXT_OCCUR)
        self.configure_columns()
        self.show()

    def get_scheduled_from_path(self, path):
        _iter = self.tree_model.real.get_iter(path)
        return self.tree_model.get_scheduled_instances(_iter).scheduled
