from gasstation.utilities.custom_renderers import *
from gasstation.utilities.dialog import *
from gasstation.utilities.state import *
from libgasstation.core.component import *

STATE_KEY = "state-key"
STATE_KEY_SORT_COLUMN = "sort_column"
STATE_KEY_SORT_ORDER = "sort_order"
STATE_KEY_COLUMN_ORDER = "column_order"
STATE_KEY_SUFF_VISIBLE = "visible"
STATE_KEY_SUFF_WIDTH = "width"
MODEL_COLUMN = "model_column"
REAL_TITLE = "real_title"
PREF_NAME = "pref-name"
ALWAYS_VISIBLE = "always-visible"
DEFAULT_VISIBLE = "default-visible"
TREE_VIEW_COLUMN_DATA_NONE = -1
TREE_VIEW_COLUMN_COLOR_NONE = -1
TREE_VIEW_COLUMN_VISIBLE_ALWAYS = -1


def tree_view_column_set_default_width(view, column, sizing_text):
    column_title = column.get_title()
    layout = view.create_pango_layout(column_title)
    title_width = layout.get_pixel_size()
    layout = view.create_pango_layout(sizing_text)
    default_width = layout.get_pixel_size()
    default_width = max(default_width[0], title_width[0])
    if default_width:
        default_width += 10
        column.set_properties(fixed_width=default_width, sizing=Gtk.TreeViewColumnSizing.FIXED)


class ColDef:
    def __init__(self, vcol, mcol, title, pref_name, sizer, vmccol, avcol, ecb, escb, sfn):
        self.viewcol = vcol
        self.modelcol = mcol
        self.title = title
        self.pref_name = pref_name
        self.sizer = sizer
        self.visibility_model_col = vmccol
        self.always_visible_col = avcol
        self.edited_cb = ecb
        self.editing_started_cb = escb
        self.sort_fn = sfn


class TreeView(Gtk.TreeView):
    __gtype_name__ = "TreeView"
    __gproperties__ = {'state-section': (str,
                                         "State Section",
                                         "The section name in the saved state to use for (re)storing the treeview's "
                                         "visual state (visible columns, sort order,...",
                                         None, GObject.ParamFlags.READWRITE),
                       'show-column-menu': (bool,
                                            "Show Column Menu",
                                            "Show the column menu so user can change what columns are visible.",
                                            False, GObject.ParamFlags.READWRITE)}

    def __init__(self, *args, **kwargs):
        self.state_section = None
        self.show_column_menu = False
        self.reference_ids = defaultdict(list)
        self.__user_data = defaultdict(dict)
        self.column_menu_column = None
        self.column_menu = None
        self.editing_started_cb = None
        self.editing_finished_cb = None
        self.editing_cb_data = None
        self.sort_model = None
        self.seen_state_visibility = True
        self.columns_changed_cb_id = 0
        self.sort_column_changed_cb_id = 0
        self.size_allocate_cb_id = 0
        self.stop_cell_move = False
        self.temp_cr = None
        super().__init__(*args, **kwargs)
        Tracking.remember(self)
        self.set_column_drag_function(self.drop_ok_cb)
        self.set_grid_lines(gs_tree_view_get_grid_lines_pref())
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_GRID_LINES_HORIZONTAL, self.update_grid_lines)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_GRID_LINES_VERTICAL, self.update_grid_lines)
        icon = Gtk.Image.new_from_icon_name("pan-down-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
        self.column_menu_icon_box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 0)
        self.column_menu_icon_box.set_homogeneous(False)
        icon.set_margin_start(5)
        self.column_menu_icon_box.pack_end(icon, False, False, 0)
        sep = Gtk.Separator.new(orientation=Gtk.Orientation.VERTICAL)
        self.column_menu_icon_box.pack_end(sep, False, False, 0)
        self.column_menu_icon_box.show_all()
        column = self.add_text_column(-1)
        column.set_properties(clickable=True, widget=self.column_menu_icon_box, expand=True, alignment=1.0)
        self.editing_now = False
        self.column_menu_column = column
        mybox = icon.get_parent()
        walign = mybox.get_parent()
        box = walign.get_parent()
        button = box.get_parent()
        if not isinstance(button, Gtk.Button):
            self.reference_ids[column].append(column.connect("clicked", self.select_column_cb))
        else:
            button.set_events(Gdk.EventMask.BUTTON_PRESS_MASK)
            self.reference_ids[button].append(button.connect("button_press_event", self.select_column_icon_cb))
        column.set_sizing(Gtk.TreeViewColumnSizing.FIXED)
        self.reference_ids[self].append(self.connect("destroy", self.dispose_cb))

    @staticmethod
    def get_editing(col):
        cell_editing0 = False
        cell_editing1 = False
        editing = False
        cr1 = None
        renderers = col.get_cells()
        cr0 = renderers[0]
        if len(renderers) == 2:
            cr1 = renderers[1]
        if cr0.get_visible():
            cell_editing0 = cr0.get_property("editing")

        if cr1 is not None and cr1.get_visible():
            cell_editing1 = cr1.get_property("editing")

        if cell_editing0 or cell_editing1:
            editing = True
        return editing

    def drop_ok_cb(self, column, prev_column, next_column, *data):
        if prev_column is None: return True
        if next_column is None: return False
        pref_name = self.get_user_data(prev_column, PREF_NAME)
        if pref_name is None:
            return False
        return True

    def select_column_cb(self, column):
        menu = self.column_menu
        if menu is None:
            return
        menu.foreach(self.update_column_menu_item)
        menu.show_all()
        self.column_menu.popup_at_pointer(None)

    def select_column_icon_cb(self, widget, event: Gdk.Event):
        stylectxt = widget.get_style_context()
        if event.button != 1:
            return False
        padding = stylectxt.get_padding(Gtk.StateFlags.NORMAL)
        if widget.get_direction() == Gtk.TextDirection.RTL:
            if event.x < (self.column_menu_icon_box.get_allocated_width() + padding.left):
                self.select_column_cb(self.column_menu_column)
        else:
            if event.x > (
                    widget.get_allocated_width() - (self.column_menu_icon_box.get_allocated_width() + padding.right)):
                self.select_column_cb(self.column_menu_column)
        return False

    def update_column_menu_item(self, checkmenuitem):
        if checkmenuitem is None: return
        if self.get_user_data(checkmenuitem, ALWAYS_VISIBLE):
            visible = True
        else:
            binding = self.get_user_data(checkmenuitem, "column-binding")
            column = binding.get_target()
            visible = column.get_visible()
        checkmenuitem.set_active(visible)

    def update_grid_lines(self, prefs, pref):
        self.set_grid_lines(gs_tree_view_get_grid_lines_pref())

    def get_state_section(self):
        return self.state_section

    def get_sort_column(self):
        s_model = self.get_model()
        if s_model is None:
            return
        current, order = s_model.get_sort_column_id()
        if current is None:
            return
        column = self.find_by_model_id(current)
        if column is None:
            return None
        name = self.get_user_data(column, PREF_NAME)
        return name

    def set_sort_column(self, name):
        s_model = self.get_model()
        if s_model is None:
            return
        column = self.find_column_by_name(name)
        if column is None:
            s_model.set_sort_column_id(-1, Gtk.SortType.ASCENDING)
            return

        model_column = self.get_user_data(column, MODEL_COLUMN)
        if model_column == TREE_VIEW_COLUMN_DATA_NONE:
            return
        current, order = s_model.get_sort_column_id()
        if current is None or order is None:
            order = Gtk.SortType.ASCENDING
        s_model.set_sort_column_id(current, order)

    def get_sort_order(self):
        s_model = self.get_model()
        if s_model is None:
            return "ascending"
        current, order = s_model.get_sort_column_id()
        if current is None:
            return "ascending"
        return "ascending" if order == Gtk.SortType.ASCENDING else "descending"

    def set_sort_order(self, name):
        order = Gtk.SortType.ASCENDING
        s_model = self.get_model()
        if s_model is None:
            return
        if name == "descending":
            order = Gtk.SortType.DESCENDING
        current = s_model.get_sort_column_id()[0]
        if current is None:
            current = -1
        s_model.set_sort_column_id(current, order)

    def get_column_order(self):
        return [self.get_user_data(col, PREF_NAME) for col in self.get_columns() if
                self.get_user_data(col, PREF_NAME) is not None]

    def set_column_order(self, column_names):
        columns = []
        prev = None
        for n in column_names:
            col = self.find_column_by_name(n)
            if col is not None:
                columns.append(col)
        for col in columns:
            self.move_column_after(col, prev)
            prev = col

    def find_by_model_id(self, wanted):
        found = None
        column_list = self.get_columns()
        for col in column_list:
            if self.get_user_data(col, MODEL_COLUMN) == wanted:
                found = col
                break
        return found

    def do_set_property(self, prop, value):
        if prop.name == "state-section":
            self.set_state_section(value)
        elif prop.name == "show-column-menu":
            self.set_show_column_menu(value)

    def remove_state_information(self):
        state_file = State.get_current()
        if not self.state_section:
            return
        try:
            state_file.remove_group(self.state_section)
        except GLib.Error:
            pass
        self.state_section = None

    def set_state_section(self, section):
        if self.state_section is not None:
            self.remove_state_information()
        if section is None or section == "":
            return
        self.state_section = section
        state_file = State.get_current()
        if state_file.has_group(self.state_section):
            keys = state_file.get_keys(self.state_section)
            for key in keys[0]:
                if key == STATE_KEY_SORT_COLUMN:
                    self.set_sort_column(state_file.get_string(self.state_section, key))
                elif key == STATE_KEY_SORT_ORDER:
                    self.set_sort_order(state_file.get_string(self.state_section, key))
                elif key == STATE_KEY_COLUMN_ORDER:
                    columns = state_file.get_string_list(self.state_section, key)
                    self.set_column_order(columns)
                else:
                    column_name = key
                    type_name = column_name.split("_")
                    if len(type_name) > 1:
                        column_name = type_name[0]
                        type_name = type_name[1]
                        if type_name == STATE_KEY_SUFF_VISIBLE:
                            column = self.find_column_by_name(column_name)
                            if column is not None:
                                if not self.get_user_data(column, ALWAYS_VISIBLE):
                                    v = state_file.get_boolean(self.state_section, key)
                                    self.set_user_data(column, DEFAULT_VISIBLE, int(v))
                                    column.set_visible(v)

                        elif type_name == STATE_KEY_SUFF_WIDTH:
                            width = state_file.get_integer(self.state_section, key)
                            column = self.find_column_by_name(column_name)
                            if column is not None:
                                if width and width != column.get_width():
                                    column.set_fixed_width(width)
        self.build_column_menu()

    def save_state(self):
        if self.state_section is not None:
            state_file = State.get_current()
            sort_column = self.get_sort_column()
            sort_order = self.get_sort_order()
            col_order = self.get_column_order()
            if sort_column and sort_column == "name":
                try:
                    state_file.remove_key(self.state_section, STATE_KEY_SORT_COLUMN)
                except GLib.Error:
                    pass
                state_file.set_string(self.state_section, STATE_KEY_SORT_COLUMN, sort_column)

            if sort_order == "descending":
                try:
                    state_file.remove_key(self.state_section, STATE_KEY_SORT_ORDER)
                except GLib.Error:
                    pass
                state_file.set_string(self.state_section, STATE_KEY_SORT_ORDER, sort_order)

            if col_order and len(col_order) > 0:
                try:
                    state_file.remove_key(self.state_section, STATE_KEY_COLUMN_ORDER)
                except GLib.Error:
                    pass
                state_file.set_string_list(self.state_section, STATE_KEY_COLUMN_ORDER, col_order)

            column_list = self.get_columns()
            for column in column_list:
                name = self.get_user_data(column, PREF_NAME)
                if name is None:
                    continue
                if self.get_user_data(column, ALWAYS_VISIBLE) is None:
                    key = "_".join([name, STATE_KEY_SUFF_VISIBLE])
                    state_file.set_boolean(self.state_section, key, column.get_visible())
                key = "_".join([name, STATE_KEY_SUFF_WIDTH])
                if self.get_user_data(column, "default-width") and self.get_user_data(column,
                                                                                      "default-width") != column.get_width():
                    try:
                        state_file.remove_key(self.state_section, key)
                    except GLib.Error:
                        pass
                    state_file.set_integer(self.state_section, key, column.get_width())

    def dispose_cb(self, widget):
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL, PREF_GRID_LINES_HORIZONTAL,
                                  self.update_grid_lines)
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL, PREF_GRID_LINES_VERTICAL,
                                  self.update_grid_lines)
        if self.state_section is not None:
            self.save_state()
        for obj, ids in self.reference_ids.items():
            for _id in ids:
                obj.disconnect(_id)
        self.reference_ids = None
        self.set_column_drag_function(None)
        for col in self.get_columns():
            col.set_cell_data_func(self.column_get_renderer(col), None)
        self.state_section = None
        if self.column_menu is not None:
            self.column_menu = None
        if self.sort_model is not None:
            self.sort_model = None
        self.set_model(None)
        self.__user_data = None

    @staticmethod
    def column_get_renderer(column):
        renderers = column.get_cells()
        if len(renderers) > 0:
            return renderers[0]
        return None

    @staticmethod
    def get_model_from_sort_model(s_model):
        if s_model is None: return
        f_model = s_model.get_model()
        model = f_model.get_model()
        return model

    def get_user_data(self, obj, key):
        return self.__user_data.get(obj, {}).get(key)

    def set_user_data(self, obj, key, userdata):
        if userdata is not None:
            self.__user_data[obj][key] = userdata
        else:
            self.__user_data[obj].pop(key)

    def get_column_next_to(self, col, backward=False):
        columns = []
        cos = [col for col in self.get_columns() if col.get_visible()]
        for p in cos:
            renderers = p.get_cells()
            if len(renderers) and renderers[0].get_property("editable"):
                columns.append(p)
        try:
            col_num = columns.index(col)
        except ValueError:
            col_num = -1
        try:
            if backward:
                return columns[col_num - 1], False
            else:
                return columns[col_num + 1], False
        except IndexError:
            return columns[-1] if backward else columns[0], True

    def path_is_valid(self, path):
        model = self.get_model()
        try:
            t = model.get_iter(path)
        except ValueError:
            return False
        return True if t is not None else False

    def update_visibility(self, column):
        if column is None:
            return
        visible = self.column_visible(column, None)
        column.set_visible(visible)

    def column_visible(self, column, pref_name):
        col_name = pref_name
        if column is not None:
            if self.get_user_data(column, ALWAYS_VISIBLE):
                return True
            col_name = self.get_user_data(column, PREF_NAME)
        if col_name is None:
            return True
        if self.state_section:
            state_file = State.get_current()
            key = "%s_%s" % (col_name, STATE_KEY_SUFF_VISIBLE)
            try:
                visible = state_file.get_boolean(self.state_section, key)
                return visible
            except GLib.Error:
                pass
        return bool(self.get_user_data(column, DEFAULT_VISIBLE)) if column is not None else False

    def count_visible_columns(self):
        count = 0
        columns = self.get_columns()
        for col in columns:
            if self.get_user_data(col, DEFAULT_VISIBLE) is not None or self.get_user_data(col,
                                                                                          ALWAYS_VISIBLE) is not None:
                count += 1
        return count

    def configure_columns(self):
        columns = self.get_columns()
        if len(columns) == 0:
            raise ValueError("CONFIGURE COLUMNS MUST BE CALLED AFTER ADDING COLUMNS")
        for col in columns:
            self.update_visibility(col)
        if self.state_section is not None:
            self.seen_state_visibility = True
        hide_menu_column = self.count_visible_columns() == 1
        column = self.get_column(0)
        column.set_expand(hide_menu_column)
        self.column_menu_column.set_visible(not hide_menu_column)

    def column_properties(self, column, pref_name, data_column, default_width, **kwargs):
        width = 0
        if pref_name:
            self.set_user_data(column, PREF_NAME, pref_name)
        if data_column == 0:
            self.set_user_data(column, ALWAYS_VISIBLE, 1)
        self.set_user_data(column, MODEL_COLUMN, data_column)
        visible = self.column_visible(column, pref_name)
        kwargs["visible"] = visible
        kwargs["resizable"] = kwargs.get("resizable", False) and pref_name is not None
        kwargs["reorderable"] = pref_name is not None

        if default_width == 0:
            kwargs["sizing"] = kwargs.pop("sizing", Gtk.TreeViewColumnSizing.AUTOSIZE)
        else:
            if width == 0:
                width = default_width + 10
            if width == 0:
                width = 10
            kwargs["sizing"] = kwargs.pop("sizing", Gtk.TreeViewColumnSizing.FIXED)
            width = kwargs.pop("fixed_width", width)
            kwargs["fixed_width"] = width
            self.set_user_data(column, "default-width", width)

        s_model = self.get_model()
        sort_func = kwargs.pop("sort_func", None)
        if isinstance(s_model, Gtk.TreeModelSort) or self.sort_model is not None:
            column.set_sort_column_id(data_column)
            if sort_func is not None:
                s_model.set_sort_func(data_column, sort_func, data_column)
        column.set_properties(**kwargs)
        if pref_name is not None:
            self.create_menu_item(column)

    def add_combo_column(self, view_col, title=None, pref_name=None,
                         sizing_text=None,
                         visible_default=False, visible_always=False,
                         model=None, model_col=0, has_entry=False, **kwargs):
        column = Gtk.TreeViewColumn(title)
        column.set_spacing(10)
        renderer = Gtk.CellRendererCombo()
        column.pack_start(renderer, True)
        self.set_user_data(column, DEFAULT_VISIBLE, visible_default)
        self.set_user_data(column, ALWAYS_VISIBLE, visible_always)
        self.set_user_data(column, REAL_TITLE, kwargs.pop(REAL_TITLE, title))
        self.set_user_data(renderer, "view_column", view_col)
        edited_cb = kwargs.pop("edited", None)
        if edited_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("edited", edited_cb))
        renderer.set_property("editable", kwargs.pop("editable", edited_cb is not None))
        editing_started_cb = kwargs.pop("editing_started", None)
        if editing_started_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("editing-started", editing_started_cb))
        editing_canceled_cb = kwargs.pop("editing_canceled", None)
        if editing_canceled_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("editing-canceled", editing_canceled_cb))
        renderer.set_property("xalign", kwargs.pop("xalign", 0.0))
        data_column = kwargs.pop("data_col", TREE_VIEW_COLUMN_DATA_NONE)
        if data_column != TREE_VIEW_COLUMN_DATA_NONE:
            column.add_attribute(renderer, "text", data_column)
        visible_column = kwargs.pop("visible_col", None)
        if visible_column is not None:
            column.add_attribute(renderer, "visible", visible_column)
        data_func = kwargs.pop("data_func", None)
        if data_func is not None:
            column.set_cell_data_func(renderer, data_func)
        layout = self.create_pango_layout(title)
        title_width = layout.get_pixel_size()
        layout = self.create_pango_layout(sizing_text)
        default_width = layout.get_pixel_size()
        default_width = max(default_width[0], title_width[0])
        if default_width:
            default_width += 10
        self.column_properties(column, pref_name, data_column, default_width, **kwargs)
        if model is not None:
            renderer.set_property("model", model)
            renderer.set_property("has-entry", has_entry)
            renderer.set_property("text-column", model_col)
        self.append_column(column)
        return column

    def add_date_column(self, view_col, title="", pref_name=None, sizing_text=None, icon_name=None, visible_always=0,
                        visible_default=0, **kwargs):
        column = Gtk.TreeViewColumn(title)
        self.set_user_data(column, DEFAULT_VISIBLE, visible_default)
        self.set_user_data(column, ALWAYS_VISIBLE, visible_always)
        self.set_user_data(column, REAL_TITLE, kwargs.pop(REAL_TITLE, title))
        if icon_name is not None:
            renderer = Gtk.CellRendererPixbuf()
            renderer.set_property("icon-name", icon_name)
            column.pack_start(renderer, False)
        renderer = CellRendererDate(True)
        column.pack_start(renderer, True)
        self.set_user_data(renderer, "view_column", view_col)
        edited_cb = kwargs.pop("edited", None)
        if edited_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("edited", edited_cb))
        renderer.set_property("editable", kwargs.pop("editable", edited_cb is not None))
        editing_started_cb = kwargs.pop("editing_started", None)
        if editing_started_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("editing-started", editing_started_cb))
        editing_canceled_cb = kwargs.pop("editing_canceled", None)
        if editing_canceled_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("editing-canceled", editing_canceled_cb))
        renderer.set_property("xalign", kwargs.pop("xalign", 1.0))
        data_column = kwargs.pop("data_col", TREE_VIEW_COLUMN_DATA_NONE)
        if data_column != TREE_VIEW_COLUMN_DATA_NONE:
            column.add_attribute(renderer, "text", data_column)
        visible_column = kwargs.pop("visible_col", None)
        if visible_column is not None:
            column.add_attribute(renderer, "visible", visible_column)
        color_column = kwargs.pop("color_col", None)
        if color_column is not None:
            column.add_attribute(renderer, "foreground", color_column)
        data_func = kwargs.pop("data_func", None)
        if data_func is not None:
            column.set_cell_data_func(renderer, data_func)
        layout = self.create_pango_layout(title)
        title_width = layout.get_pixel_size()
        layout = self.create_pango_layout(sizing_text)
        default_width = layout.get_pixel_size()
        default_width = max(default_width[0], title_width[0])
        if default_width:
            default_width += 10
        self.column_properties(column, pref_name, data_column, default_width, **kwargs)
        self.append_column(column)
        return column

    def add_text_column(self, view_col, title="", pref_name=None, icon_name=None, pixbuf_col=None, sizing_text=None,
                        visible_always=0,
                        visible_default=0, **kwargs):
        column = Gtk.TreeViewColumn(title)
        self.set_user_data(column, DEFAULT_VISIBLE, visible_default)
        self.set_user_data(column, ALWAYS_VISIBLE, visible_always)
        self.set_user_data(column, REAL_TITLE, kwargs.pop(REAL_TITLE, title))
        if icon_name is not None or pixbuf_col is not None:
            renderer = Gtk.CellRendererPixbuf()
            column.pack_start(renderer, False)
            if icon_name is not None:
                renderer.set_property("icon-name", icon_name)
            else:
                renderer.set_padding(10, 0)
                column.add_attribute(renderer, 'pixbuf', pixbuf_col)

        renderer = Gtk.CellRendererText()
        column.pack_start(renderer, True)
        self.set_user_data(renderer, "view_column", view_col)
        edited_cb = kwargs.pop("edited", None)
        if edited_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("edited", edited_cb))
        renderer.set_property("editable", kwargs.pop("editable", edited_cb is not None))
        editing_started_cb = kwargs.pop("editing_started", None)
        if editing_started_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("editing-started", editing_started_cb))
        editing_canceled_cb = kwargs.pop("editing_canceled", None)
        if editing_canceled_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("editing-canceled", editing_canceled_cb))
        renderer.set_property("xalign", kwargs.pop("xalign", 0.0))
        data_column = kwargs.pop("data_col", TREE_VIEW_COLUMN_DATA_NONE)
        if data_column != TREE_VIEW_COLUMN_DATA_NONE:
            column.add_attribute(renderer, "text", data_column)
        visible_column = kwargs.pop("visible_col", None)
        if visible_column is not None:
            column.add_attribute(renderer, "visible", visible_column)
        color_column = kwargs.pop("color_col", None)
        if color_column is not None:
            column.add_attribute(renderer, "foreground", color_column)
        data_func = kwargs.pop("data_func", None)
        if data_func is not None:
            column.set_cell_data_func(renderer, data_func)
        layout = self.create_pango_layout(title)
        title_width = layout.get_pixel_size()
        layout = self.create_pango_layout(sizing_text)
        default_width = layout.get_pixel_size()
        default_width = max(default_width[0], title_width[0])
        if default_width:
            default_width += 10
        kwargs["resizable"] = True
        self.column_properties(column, pref_name, data_column, default_width, **kwargs)
        self.append_column(column)
        return column

    def add_numeric_column(self, *args, **kwargs):
        kwargs["alignment"] = 1.0
        kwargs["xalign"] = 1.0
        return self.add_text_column(*args, **kwargs)

    def add_toggle_column(self, view_col, title="", short_title="", pref_name=None, icon_name=None,
                          sizing_text=None, visible_always=0,
                          visible_default=0, **kwargs):
        column = Gtk.TreeViewColumn(short_title)
        column.set_title(title)
        self.set_user_data(column, DEFAULT_VISIBLE, visible_default)
        self.set_user_data(column, ALWAYS_VISIBLE, visible_always)
        self.set_user_data(column, REAL_TITLE, kwargs.pop(REAL_TITLE, title))
        if icon_name is not None:
            renderer = Gtk.CellRendererPixbuf()
            renderer.set_property("icon-name", icon_name)
            column.pack_start(renderer, False)
        kwargs["resizable"] = False
        renderer = Gtk.CellRendererToggle()
        column.pack_start(renderer, True)
        self.set_user_data(renderer, "view_column", view_col)
        edited_cb = kwargs.pop("edited", None)
        if edited_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("toggled", edited_cb))
        renderer.set_property("sensitive", kwargs.pop("editable", edited_cb is not None))
        editing_started_cb = kwargs.pop("editing_started", None)
        if editing_started_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("editing-started", editing_started_cb))
        editing_canceled_cb = kwargs.pop("editing_canceled", None)
        if editing_canceled_cb is not None:
            self.reference_ids[renderer].append(renderer.connect("editing-canceled", editing_canceled_cb))
        renderer.set_property("xalign", kwargs.pop("xalign", 1.0))
        data_column = kwargs.pop("data_col", TREE_VIEW_COLUMN_DATA_NONE)
        if data_column != TREE_VIEW_COLUMN_DATA_NONE:
            column.add_attribute(renderer, "active", data_column)
        visible_column = kwargs.pop("visible_col", None)
        if visible_column is not None:
            column.add_attribute(renderer, "visible", visible_column)
        color_column = kwargs.pop("color_col", None)
        if color_column is not None:
            column.add_attribute(renderer, "foreground", color_column)
        data_func = kwargs.pop("data_func", None)
        if data_func is not None:
            column.set_cell_data_func(renderer, data_func)
        layout = self.create_pango_layout(title)
        title_width = layout.get_pixel_size()
        layout = self.create_pango_layout(sizing_text)
        default_width = layout.get_pixel_size()
        default_width = max(default_width[0], title_width[0])
        if default_width:
            default_width += 10
        self.column_properties(column, pref_name, data_column, default_width, **kwargs)
        self.append_column(column)
        column.get_button().set_tooltip_text(title)
        return column

    def append_column(self, column):
        columns = self.get_columns()
        n = len(columns)
        if n >= 1:
            n -= 1
        return self.insert_column(column, n)

    def keynav(self, col, path, event):
        if event.type != Gdk.EventType.KEY_PRESS:
            return None
        if event.keyval in [Gdk.KEY_Tab, Gdk.KEY_ISO_Left_Tab, Gdk.KEY_KP_Tab]:
            shifted = event.state & Gdk.ModifierType.SHIFT_MASK
            col, wrapped = self.get_column_next_to(col, shifted)
            if wrapped:
                depth = path.get_depth()
                if shifted:
                    if not path.prev() and depth > 1:
                        path.up()
                elif self.row_expanded(path):
                    path.down()
                else:
                    path.next()
                    if not self.path_is_valid(path) and depth > 2:
                        path.prev()
                        path.up()
                        path.next()
                    if not self.path_is_valid(path) and depth > 1:
                        path.prev()
                        path.up()
                        path.next()

        elif event.keyval in [Gdk.KEY_Return, Gdk.KEY_KP_Enter]:
            if self.row_expanded(path):
                path.down()
            else:
                depth = path.get_depth()
                path.next()
                if not self.path_is_valid(path) and depth > 1:
                    path.prev()
                    path.up()
                    path.next()
        return col

    def find_column_by_name(self, wanted):
        found = None
        for col in self.get_columns():
            name = self.get_user_data(col, PREF_NAME)
            if name is None or name != wanted:
                continue
            found = col
        return found

    def editing_canceled_cb(self, renderer, *args):
        pass

    def editing_started_cb(self, renderer, *args):
        pass

    def edited_cb(self, cell, path_string, new_text):
        pass

    @staticmethod
    def update_control_cell_renderers_background(col, column, func):
        renderers = col.get_cells()
        for cell in renderers:
            if func is None:
                col.add_attribute(cell, "cell-background", column)
            else:
                col.set_cell_data_func(cell, func)

    def set_control_column_background(self, column, func):
        self.update_control_cell_renderers_background(self.column_menu_column, column, func)

    def build_column_menu(self):
        if self.column_menu:
            self.column_menu = None
        if self.show_column_menu and self.state_section:
            if self.column_menu_column:
                self.column_menu_column.set_visible(True)
            column_list = self.get_columns()
            for c in column_list:
                self.create_menu_item(c)
        else:
            if self.column_menu_column:
                self.column_menu_column.set_visible(False)

    def create_menu_item(self, column):
        if not self.state_section:
            return
        pref_name = self.get_user_data(column, PREF_NAME)
        if pref_name is None:
            return
        if self.column_menu is None:
            self.column_menu = Gtk.Menu.new()
        column_name = self.get_user_data(column, REAL_TITLE)
        if column_name is None:
            column_name = column.get_title()
        widget = Gtk.CheckMenuItem.new_with_label(column_name)
        self.column_menu.append(widget)

        if self.get_user_data(column, ALWAYS_VISIBLE):
            self.set_user_data(widget, ALWAYS_VISIBLE, 1)
            widget.set_sensitive(False)
        binding = widget.bind_property("active", column, "visible", 0)
        self.set_user_data(widget, "column-binding", binding)
        key = "%s_%s" % (pref_name, STATE_KEY_SUFF_VISIBLE)
        self.set_user_data(widget, STATE_KEY, key)

    def set_show_column_menu(self, visible):
        self.show_column_menu = visible
        self.build_column_menu()

    def get_show_column_menu(self):
        return self.show_column_menu

    def expand_columns(self, *args):
        columns = self.get_columns()
        for column in columns:
            pref_name = self.get_user_data(column, PREF_NAME)
            if not pref_name:
                column.set_expand(False)
        for col in args:
            if col:
                column = self.find_column_by_name(col)
                if column is not None:
                    column.set_expand(True)

    def set_editing_started_cb(self, editing_started_cb, *editing_cb_data):
        if editing_started_cb is None:
            return
        self.editing_started_cb = editing_started_cb
        self.editing_cb_data = editing_cb_data

    def set_editing_finished_cb(self, editing_finished_cb, *editing_cb_data):
        if editing_finished_cb is None: return
        self.editing_finished_cb = editing_finished_cb
        self.editing_cb_data = editing_cb_data

    def remove_edit_combo(self, ce):
        if self.temp_cr is not None:
            popup_entry = self.get_user_data(self.temp_cr, "cell-editable")
            if popup_entry.get_property("has-entry"):
                entry: Gtk.Entry = popup_entry.get_child()
                new_string = entry.get_text()
                entry.set_completion(None)
                entry.destroy()
                popup_entry.destroy()
            else:
                new_string = ""
                _iter = popup_entry.get_active_iter()
                m = popup_entry.get_model()
                if m is not None and _iter is not None:
                    new_string = m.get_value(_iter, 0)
            current_string = self.get_user_data(self.temp_cr, "current-string")
            if not self.get_user_data(self.temp_cr, "edit-canceled") and GLib.strcasecmp(new_string,
                                                                                         current_string):
                self.set_user_data(self, "data-edited", True)
            self.set_user_data(self.temp_cr, "cell-editable", None)
            self.temp_cr = None
            self.editing_now = False

    def remove_edit_entry(self, ce):
        if self.temp_cr is not None:
            new_string = ce.get_text()
            current_string = self.get_user_data(self.temp_cr, "current-string") or ""
            if not self.get_user_data(self.temp_cr, "edit-canceled") and GLib.strcasecmp(new_string,
                                                                                         current_string) != 0:
                self.set_user_data(self, "data-edited", True)
            if self.get_user_data(self.temp_cr, "flags") is not None:
                self.set_user_data(self.temp_cr, "flags", None)
            self.set_user_data(self.temp_cr, "cell-editable", None)
            self.temp_cr = None
            self.editing_now = False
            ce.destroy()

    def remove_edit_date(self, ce):
        if self.temp_cr is not None:
            popup_entry = self.get_user_data(self.temp_cr, "cell-editable")
            new_string = popup_entry.get_text()
            current_string = self.get_user_data(self.temp_cr, "current-string") or ""
            if not self.get_user_data(self.temp_cr, "edit-canceled") and GLib.strcasecmp(new_string,
                                                                                         current_string):
                self.set_user_data(self, "data-edited", True)
            date = self.parse_date(new_string)
            date_string = self.get_date_help(date)
            self.help_text = date_string
            self.emit("help_signal", 0)
            self.set_user_data(self.temp_cr, "cell-editable", None)
            self.temp_cr = None
            self.editing_now = False

    def finish_edit(self):
        if self.temp_cr is None:
            return
        ce = self.get_user_data(self.temp_cr, "cell-editable")
        if ce is not None:
            ce.editing_done()
            ce.remove_widget()

    def get_model_from_view(self):
        s_model = self.get_model()
        if s_model is None:
            return
        f_model = s_model.get_model()
        if f_model is None:
            return s_model
        if isinstance(f_model, Gtk.TreeModelFilter):
            return f_model.get_model()
        return f_model

    def get_model_iter_from_view_string(self, path_string):
        s_model = self.get_model()
        s_iter = s_model.get_iter_from_string(path_string)
        if s_iter is None:
            return None
        return s_model.convert_iter_to_child_iter(s_iter)

    @staticmethod
    def get_model_iter_from_selection(sel):
        s_model, s_iter = sel.get_selected()
        if s_iter is None:
            return False, None
        return True, s_model.convert_iter_to_child_iter(s_iter)

    def get_sort_path_from_model_path(self, mpath):
        if mpath is None:
            return None
        s_model = self.get_model()
        return s_model.convert_child_path_to_path(mpath)

    def get_model_path_from_sort_path(self, spath):
        if spath is None:
            return None
        s_model = self.get_model()
        mpath = s_model.convert_path_to_child_path(spath)
        return mpath

    def __del__(self):
        Tracking.forget(self)


GObject.type_register(TreeView)
