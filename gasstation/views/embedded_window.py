from gi.repository import GObject

from gasstation.utilities.dialog import *
from libgasstation.core.component import Tracking
from .window import *


class EmbeddedWindow(Gtk.Box, Window):
    __gtype_name__ = "EmbeddedWindow"
    __gsignals__ = {
        "page_changed": (GObject.SignalFlags.RUN_FIRST, None, (object,)),
    }

    def __init__(self, action_group_name, action_entries, ui_filename, enclosing_win, add_accelerators, *user_data):
        Gtk.Box.__init__(self)

        self.toolbar = None
        self.action_group = None
        self.page = None
        self.parent_window = None
        self.set_orientation(Gtk.Orientation.VERTICAL)
        gs_widget_set_style_context(self, "EmbeddedWindow")
        self.menu_dock = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        self.pack_start(self.menu_dock, False, True, 0)
        self.menu_dock.set_homogeneous(False)
        self.menu_dock.show()

        self.statusbar = Gtk.Statusbar()
        self.pack_end(self.statusbar, False, True, 0)
        self.statusbar.show()

        self.ui_merge = Gtk.UIManager()
        self.ui_merge.connect("add_widget", self.add_widget)
        self.action_group = None
        self.parent_window = enclosing_win
        self.action_group = Gtk.ActionGroup.new(action_group_name)
        # gtk_action_group_set_translation_domain(self.action_group, PROJECT_NAME)
        self.action_group.add_actions(action_entries, user_data)
        self.ui_merge.insert_action_group(self.action_group, 0)
        try:
            self.ui_merge.add_ui_from_resource('/org/jonah/Gasstation/ui/' + ui_filename)
        except GLib.GError:
            return
        if add_accelerators:
            enclosing_win.add_accel_group(self.ui_merge.get_accel_group())
        self.ui_merge.ensure_update()
        Tracking.remember(self)

    def open_page(self, page):
        if self.page is not None:
            return
        self.page = page
        page.window = self
        page.notebook_page = page.create_widget()
        self.pack_end(page.notebook_page, True, True, 2)
        page.inserted()
        page.merge_actions(self.ui_merge)

    def close_page(self, page):
        if self.page != page:
            return
        if page.notebook_page is None:
            return
        self.remove(page.notebook_page)
        self.page = None
        page.removed()
        page.unmerge_actions(self.ui_merge)
        self.ui_merge.ensure_update()
        page.destroy_widget()

    def get_page(self):
        return self.page

    def add_widget(self, merge, widget):
        if isinstance(widget, Gtk.Toolbar):
            self.toolbar = widget
            self.toolbar.set_style(Gtk.ToolbarStyle.BOTH)
            self.toolbar.set_icon_size(Gtk.IconSize.SMALL_TOOLBAR)
        self.menu_dock.pack_start(widget, False, False, 0)
        widget.show()

    def get_gtk_window(self, *args):
        return self.parent_window

    def get_statusbar(self):
        return self.statusbar
