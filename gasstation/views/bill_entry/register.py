from gi.repository import GObject, Gtk

from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.utilities.ui import add_toolbar_label
from .account_view import BillAccountEntryView
from .view import BillEntryView


class BillEntryRegister(Gtk.Box):
    __gsignals__ = {
        'help-changed': (GObject.SignalFlags.RUN_LAST, None, (int,))
    }

    def __init__(self, ledger, parent, **kwargs):
        Gtk.Box.__init__(self, **kwargs)
        self.scroll_bar = None
        self.total_label = None
        self.total_subtotal_label = None
        self.total_tax_label = None
        self.summary_bar = None
        self.scroll_adj = None
        self.set_orientation(Gtk.Orientation.VERTICAL)
        self.set_vexpand(True)
        self.ledger = ledger
        self.window = parent
        self.group = "invoice"
        self.create_table()

    def create_summary_bar(self):
        tbar = Gtk.Toolbar()
        gs_widget_set_style_context(tbar, "summary-tbar")
        tbar.set_icon_size(Gtk.IconSize.MENU)
        tbar.set_style(Gtk.ToolbarStyle.ICONS)
        tbar.get_style_context().add_class(Gtk.STYLE_CLASS_INLINE_TOOLBAR)

        self.total_label = add_toolbar_label(tbar, "Total:")
        self.total_subtotal_label = add_toolbar_label(tbar, "Subtotal:")
        self.total_tax_label = add_toolbar_label(tbar, "Tax:")

        toolitem = Gtk.SeparatorToolItem()
        toolitem.set_expand(True)
        toolitem.set_draw(False)
        tbar.insert(toolitem, -1)

        tbar.show_all()
        self.summary_bar = tbar
        return tbar

    def emit_help_changed(self, view, p):
        self.emit("help-changed", 0)

    def create_table(self):
        model = self.ledger.get_model()
        view = BillEntryView(model)
        view.set_properties(state_section="bill_entry_", show_column_menu=True)
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scrolled_window.add(view)
        self.pack_start(scrolled_window, True, True, 0)
        scrolled_window.show()
        view.show()
        summary_bar = self.create_summary_bar()
        self.pack_start(summary_bar, False, False, 0)
        self.ledger.set_view(view)
        view.expand_columns("product", "desc", "qty", "price", "sub-total")
        view.set_headers_visible(True)
        self.show_all()
        model.connect_after("refresh_status_bar", self.redraw_all_cb)

    def redraw_all_cb(self, *args):
        bill = self.ledger.bill
        currency = bill.get_currency()
        if self.total_label is not None:
            self.total_label.set_text("{}{:,.2f}".format(currency.get_mnemonic(), bill.get_entry_total()))

    def ld_destroy(self):
        self.ledger.set_user_data(None)

    def get_parent(self):
        return self.window

    def get_register(self):
        return self.ledger.get_view()

    def get_summary_bar(self):
        return self.summary_bar

    def get_read_only(self):
        return self.read_only

    def raise_window(self):
        if self.window is not None:
            self.window.present()


class BillAccountEntryRegister(Gtk.Box):
    __gsignals__ = {
        'help-changed': (GObject.SignalFlags.RUN_LAST, None, (int,))
    }

    def __init__(self, ledger, parent, **kwargs):
        Gtk.Box.__init__(self, **kwargs)
        self.scroll_bar = None
        self.total_label = None
        self.total_subtotal_label = None
        self.summary_bar = None
        self.scroll_adj = None
        self.set_orientation(Gtk.Orientation.VERTICAL)
        self.set_vexpand(True)
        self.ledger = ledger
        self.window = parent
        self.group = "bill"
        self.create_table()

    def create_summary_bar(self):
        tbar = Gtk.Toolbar()
        gs_widget_set_style_context(tbar, "summary-tbar")
        tbar.set_icon_size(Gtk.IconSize.MENU)
        tbar.set_style(Gtk.ToolbarStyle.ICONS)
        tbar.get_style_context().add_class(Gtk.STYLE_CLASS_INLINE_TOOLBAR)

        self.total_label = add_toolbar_label(tbar, "Total:")

        toolitem = Gtk.SeparatorToolItem()
        toolitem.set_expand(True)
        toolitem.set_draw(False)
        tbar.insert(toolitem, -1)

        tbar.show_all()
        self.summary_bar = tbar
        return tbar

    def emit_help_changed(self, view, p):
        self.emit("help-changed", 0)

    def create_table(self):
        model = self.ledger.get_account_model()
        view = BillAccountEntryView(model)
        view.set_properties(state_section="bill_account", show_column_menu=True)
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        scrolled_window.add(view)
        self.pack_start(scrolled_window, True, True, 0)
        scrolled_window.show()
        view.show()
        summary_bar = self.create_summary_bar()
        self.pack_start(summary_bar, False, False, 0)
        self.ledger.set_account_view(view)
        view.expand_columns("account", "desc", "amount")
        view.set_headers_visible(True)
        self.show_all()
        model.connect_after("refresh_status_bar", self.redraw_all_cb)

    def redraw_all_cb(self, *args):
        model = self.ledger.get_account_model()
        bill = model.get_bill()
        currency = bill.get_currency()
        if self.total_label is not None:
            self.total_label.set_text("{}{:,.2f}".format(currency.get_mnemonic(), bill.get_account_entry_total()))

    def ld_destroy(self):
        self.ledger.set_user_data(None)

    def get_parent(self):
        return self.window

    def get_register(self):
        return self.ledger.get_view()

    def get_summary_bar(self):
        return self.summary_bar

    def get_read_only(self):
        return self.read_only

    def raise_window(self):
        if self.window is not None:
            self.window.present()
