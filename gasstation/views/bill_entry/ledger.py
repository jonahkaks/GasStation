from gasstation.models.bill_account_entry import BillAccountEntryModel
from gasstation.models.bill_entry import BillEntryModel
from libgasstation import EVENT_MODIFY, EVENT_DESTROY, ID_TAXTABLE, EVENT_ITEM_CHANGED, ID_ACCOUNT, ID_BILL, \
    QueryPrivate, ID_BILL_ENTRY, PARAM_GUID, QueryOp, ID_BILL_ACCOUNT_ENTRY, ENTRY_BILL
from libgasstation.core.component import ComponentManager

ENTRYLEDGER_CLASS = "bill-ledger-class"


class BillLedger:
    def __init__(self, book):
        self.book = book
        self.blank_entry_guid = None
        self.blank_entry_edited = False
        self.traverse_to_new = False
        self.loading = False
        self.last_date_entered = 0
        self.hint_entry = None
        self.parent = None
        self.bill = None
        self.query = None
        self.account_query = None
        self.model = None
        self.account_model = None
        self.view = None
        self.account_view = None
        self.prefs_group = None
        self.full_refresh = True
        self.component_id = ComponentManager.register(ENTRYLEDGER_CLASS, self.refresh_handler, self.close_handler)
        if book.is_readonly():
            self.set_readonly(True)

    def set_default_bill(self, bill):
        assert bill is not None, "Bill cannot be Null"
        self.bill = bill
        self.model = BillEntryModel(bill=bill)
        self.account_model = BillAccountEntryModel(bill=bill)
        guid = bill.get_guid()
        if self.query is None and bill is not None:
            self.query = QueryPrivate.create_for(ID_BILL_ENTRY)
            self.query.set_book(bill.get_book())
            self.query.add_guid_match([ENTRY_BILL, PARAM_GUID], guid, QueryOp.AND)
        if self.account_query is not None:
            self.account_query = QueryPrivate.create_for(ID_BILL_ACCOUNT_ENTRY)
            self.account_query.set_book(bill.get_book())
            self.account_query.add_guid_match([ENTRY_BILL, PARAM_GUID], guid, QueryOp.AND)
        self.refresh()

    def set_parent(self, parent):
        self.parent = parent

    def set_view(self, view):
        self.view = view

    def set_account_view(self, account_view):
        self.account_view = account_view

    def set_readonly(self, readonly):
        if not readonly and self.book.is_readonly():
            return
        self.model.set_read_only(readonly)
        self.account_model.set_read_only(readonly)
        if readonly:
            self.model.clear_blank_entry()
            self.account_model.clear_blank_entry()
        self.refresh()

    def get_model(self):
        return self.model

    def get_account_model(self):
        return self.account_model

    def get_entries(self):
        if self.query is not None:
            return self.query.run()
        return []

    def get_account_entries(self):
        if self.account_query is not None:
            return self.account_query.run()
        return []

    def refresh_internal(self, entries, account_entries):
        if self.loading:
            return
        self.loading = True
        self.model.load(entries)
        self.account_model.load(account_entries)
        self.loading = False

    def set_watches(self, entries, account_entries):
        ComponentManager.clear_watches(self.component_id)
        ComponentManager.watch_entity(self.component_id, self.bill.get_supplier().get_guid(), EVENT_MODIFY)
        ComponentManager.watch_entity_type(self.component_id, ID_BILL, EVENT_MODIFY | EVENT_DESTROY)
        ComponentManager.watch_entity_type(self.component_id, ID_ACCOUNT,
                                           EVENT_MODIFY | EVENT_DESTROY | EVENT_ITEM_CHANGED)
        ComponentManager.watch_entity_type(self.component_id, ID_TAXTABLE, EVENT_MODIFY | EVENT_DESTROY)
        p = entries.copy()
        p.extend(account_entries.copy())
        for entry in p:
            ComponentManager.watch_entity(self.component_id, entry.get_guid(), EVENT_MODIFY)

    def refresh_handler(self, changes):
        self.refresh()

    def finish(self):
        ComponentManager.unregister(self.component_id)

    def refresh(self):
        if self.loading:
            return
        entries = self.get_entries()
        account_entries = self.get_entries()
        self.set_watches(entries, account_entries)
        self.refresh_internal(entries, account_entries)

    def close_handler(self, *args):
        ComponentManager.unregister(self.component_id)
        self.view.destroy()
        self.view = None
        self.account_view.destroy()
        self.account_view = None
        self.model.destroy()
        self.model = None
        self.account_model.destroy()
        self.account_model = None
        if self.query is not None:
            self.query.destroy()
        if self.account_query is not None:
            self.account_query.destroy()
        self.query = None
        self.bill = None
