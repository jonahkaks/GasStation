from gasstation.utilities.warnings import *

from gasstation.models.bill_entry import *
from gasstation.views.dialogs.product import *
from gasstation.views.tree import *
from libgasstation.core.entry import BillEntry
from libgasstation.core.helpers import *
from libgasstation.core.product import Product


class BillEntryViewCol(GObject.GEnum):
    END_OF_LIST = -1
    SERVICE_DATE = 0
    PRODUCT = 1
    DESCRIPTION = 2
    QUANTITY = 3
    PRICE = 4
    PRICE_RULE = 5
    SUBTOTAL = 6
    NUM_COLUMNS = 7


class BillEntryConfirm(GObject.GEnum):
    RESET = 0
    ACCEPT = 1
    DISCARD = 2
    CANCEL = 3


class BillEntryView(TreeView):
    __gtype_name__ = "BillEntryView"
    __gsignals__ = {
        'help_signal': (GObject.SignalFlags.RUN_LAST, None, (int,)),
        'update_signal': (GObject.SignalFlags.RUN_LAST, None, (int,))
    }

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_entry = None
        self.reg_closing = False
        self.disposed = True
        self.current_ref = None
        self.change_allowed = False
        self.entry_confirm = BillEntryConfirm.RESET
        self.set_has_tooltip(True)
        self.set_resize_mode(Gtk.ResizeMode.PARENT)
        self.set_hscroll_policy(Gtk.ScrollablePolicy.MINIMUM)
        self.dirty_entry = None
        self.product_string = None
        self.temp_cr = None
        self.help_text = ""
        self.fo_handler_id = 0
        self.auto_complete = False
        self.private_window = None
        self.key_length = 0
        self.single_button_press = 0
        self.uiupdate_cb = None
        self.uiupdate_cb_data = None
        self.product_string = ""
        self.location = None
        self.editing_now = False
        self.key_length = gs_pref_get_float(PREFS_GROUP_GENERAL_REGISTER, "key-length")
        self.use_horizontal_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_HOR_LINES)
        self.use_vertical_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_VERT_LINES)
        gs_pref_register_cb(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_HOR_LINES, self.pref_changed)
        gs_pref_register_cb(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_VERT_LINES, self.pref_changed)
        self.set_property("name", "entry_tree")
        self.bill = model.get_bill()
        self.is_debit_note = self.bill.get_is_debit_note()
        self.book = self.bill.get_book()
        gs_widget_set_style_context(self, "register_grid_lines")
        if self.use_horizontal_lines:
            if self.use_vertical_lines:
                self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
            else:
                self.set_grid_lines(Gtk.TreeViewGridLines.HORIZONTAL)
        elif self.use_vertical_lines:
            self.set_grid_lines(Gtk.TreeViewGridLines.VERTICAL)
        else:
            self.set_grid_lines(Gtk.TreeViewGridLines.NONE)
        self.add_date_column(BillEntryViewCol.SERVICE_DATE, title="Service Date", pref_name="date",
                             sizing_text="12/12/2000", data_col=BillEntryModelColumn.SERVICE_DATE,
                             visible_default=True, editing_started=self.editing_started,
                             edited=self.edited_cb, data_func=self.cdf0)
        self.add_text_column(BillEntryViewCol.PRODUCT, title="Product", pref_name="product",
                             sizing_text="Petroleum Motor Spirit", data_col=BillEntryModelColumn.PRODUCT,
                             visible_always=True, editing_started=self.editing_started, edited=self.edited_cb,
                             data_func=self.cdf0)
        self.add_text_column(BillEntryViewCol.DESCRIPTION, title="Description", pref_name="desc",
                             sizing_text="Petroleum Motor Spirit", data_col=BillEntryModelColumn.DESCRIPTION,
                             editing_started=self.editing_started, edited=self.edited_cb, visible_always=True,
                             data_func=self.cdf0)
        self.add_numeric_column(BillEntryViewCol.QUANTITY, title="Quantity", pref_name="qty", sizing_text="9,999.00",
                                data_col=BillEntryModelColumn.QUANTITY, editing_started=self.editing_started,
                                edited=self.edited_cb, visible_always=True,data_func=self.cdf0)
        self.add_numeric_column(BillEntryViewCol.PRICE, title="Unit Price", pref_name="price", sizing_text="9,999.00",
                                data_col=BillEntryModelColumn.PRICE, editing_started=self.editing_started,
                                edited=self.edited_cb, visible_always=True, data_func=self.cdf0)
        self.add_numeric_column(BillEntryViewCol.SUBTOTAL, title="SubTotal", pref_name="sub-total",
                                sizing_text="9,999.00",data_func=self.cdf0,
                                data_col=BillEntryModelColumn.SUBTOTAL, visible_always=True)
        s_model = Gtk.TreeModelSort.new_with_model(model)
        self.set_model(s_model)
        s_model.set_sort_column_id(BillEntryModelColumn.SERVICE_DATE, Gtk.SortType.ASCENDING)
        self.connect("key-press-event", self.key_press_cb)
        self.configure_columns()
        self.show()

    def location_changed_cb(self, widget, location):
        self.location = location

    def set_location(self, location):
        self.location = location
        model = self.get_model_from_view()
        entries = model.get_entries()
        for entry in entries:
            product = entry.get_product()
            if product is not None:
                for pq in product.quantities:
                    if pq.get_location() == location:
                        if pq.get_current_stock().is_zero():
                            if not entry.get_edit_level() > 0:
                                entry.begin_edit()
                            entry.destroy()
                            entry.commit_edit()
                        entry.set_price(pq.get_selling_price())

    def set_private_window(self, win):
        self.private_window = win

    def motion_cb(self, sel):
        model = self.get_model_from_view()
        if model is None:
            return
        self.help_text = ""
        self.emit("help_signal", 0)
        t, m_iter = self.get_model_iter_from_selection(sel)
        if t:
            mpath = model.get_path(m_iter)
            spath = self.get_sort_path_from_model_path(mpath)
            entry = model.get_entry(m_iter)
            self.set_current_path(mpath)
            old_entry = self.current_entry
            self.current_entry = entry
            model.current_entry = entry
            indices = spath.get_indices()
            model.current_row = indices[0]
            if entry != old_entry and old_entry == self.dirty_entry:
                if self.entry_changed():
                    return

            if self.entry_confirm == BillEntryConfirm.CANCEL:
                return
        self.call_uiupdate_cb()

    def selection_to_blank(self):
        while Gtk.events_pending():
            Gtk.main_iteration()
        if not self.expanded:
            return False
        model = self.get_model_from_view()
        b_entry = model.get_blank_entry()
        b_path = model.get_path_to_entry(b_entry)
        s_path = self.get_sort_path_from_model_path(b_path)
        self.set_cursor(s_path, None, False)
        return False

    def match_selected_cb(self, widget: Gtk.EntryCompletion, search_text, _iter):
        model = widget.get_model()
        col = widget.get_text_column()
        v = model.get_value(_iter, col)
        return v.lower().__contains__(search_text.lower())

    def editing_started(self, cr, editable, path_string):
        if self.location is None:
            self.finish_edit()
            show_error(self.get_toplevel(), "Please select a location")
            return True
        completion = Gtk.EntryCompletion()
        model: BillEntryModel = self.get_model_from_view()
        entry = None
        spath = Gtk.TreePath.new_from_string(path_string)
        indices = spath.get_indices()
        viewcol = self.get_user_data(cr, "view_column")
        self.set_user_data(cr, "cell-editable", editable)
        editable.connect("key-press-event", self.ed_key_press_cb)

        if viewcol == BillEntryViewCol.SERVICE_DATE:
            entry = editable
            entry.connect("key-press-event", self.ed_key_press_cb)
            self.set_user_data(cr, "current-string", entry.get_text())
            editable.connect("remove-widget", self.remove_edit_date)

        elif viewcol == BillEntryViewCol.PRODUCT:
            entry = editable
            entry.connect("key-press-event", self.ed_key_press_cb)
            if self.stop_cell_move:
                entry.insert_text(self.product_string, 0)
                entry.set_position(-1)
            model.update_product_list()
            entry.set_completion(completion)
            product_list = model.get_product_list()
            completion.set_model(product_list)
            completion.set_match_func(self.match_selected_cb)
            completion.set_popup_completion(True)
            completion.set_inline_completion(True)
            completion.set_popup_single_match(True)
            completion.set_inline_selection(True)
            completion.set_text_column(0)
            completion.set_minimum_key_length(0)
            if entry.get_text() == "":
                entry.emit("changed")
            self.set_user_data(cr, "current-string", entry.get_text())
            editable.connect("remove-widget", self.remove_edit_entry)
        else:
            editable.connect("remove-widget", self.remove_edit_entry)
        row_color = model.get_row_color(indices[0])
        color = Gdk.RGBA()
        if row_color is not None and Gdk.RGBA.parse(color, row_color):
            if entry is not None:
                entry.set_name("cellrenderer")
                context = entry.get_style_context()
                css_provider = Gtk.CssProvider()
                col_str = Gdk.RGBA.to_string(color)
                css = "#cellrenderer{\nbackground-image: image(%s); color:black;\n}" % col_str
                css_provider.load_from_data(bytes(css.encode()))
                context.add_provider(css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        self.temp_cr = cr
        self.editing_now = True
        self.help(cr, viewcol)
        self.set_user_data(cr, "editing-canceled", False)

    def idle_product(self):
        spath = self.get_current_path()
        if spath is None:
            return
        columns = self.get_columns()
        for tvc in columns:
            renderers = tvc.get_cells()
            cr0 = renderers[0]
            viewcol = self.get_user_data(cr0, "view_column")
            if viewcol == BillEntryViewCol.PRODUCT:
                self.set_cursor(spath, tvc, True)
        return False

    def get_product_by_name(self, name):
        from gasstation.views.main_window import MainWindow
        missing = "The product %s does not exist.Would you like to create it?"
        if name is None or len(name) == 0:
            return
        product = Product.lookup_name(self.book, name)
        if product is None:
            product = Product.lookup_sku(self.book, name)
        window = MainWindow.get_main_window(self)
        if product is None:
            if not ask_yes_no(window, True, missing % name):
                return None
            d = ProductDialog(parent=window, book=self.book, modal=True, name=name)
            d.run()
            product = Product.lookup_name(self.book, name)
            if product is None:
                return None
        return product

    def edited_cb(self, cell, path_string, new_text):
        if self.location is None:
            show_error(None, "Please select a location to get the current price")
            return
        editable = self.get_user_data(cell, "cell-editable")
        if self.fo_handler_id != 0:
            editable.disconnect(self.fo_handler_id)
            self.fo_handler_id = 0
        self.grab_focus()
        old_text = self.get_user_data(cell, "current-string")
        if old_text == new_text:
            if not self.stop_cell_move:
                return

        m_iter = self.get_model_iter_from_view_string(path_string)
        viewcol = self.get_user_data(cell, "view_column")
        model = self.get_model_from_view()

        entry: BillEntry = model.get_entry(m_iter)
        if viewcol == BillEntryViewCol.SERVICE_DATE:
            self.begin_edit(entry)
            if entry is not None and new_text != "":
                if scan_date(new_text):
                    day, month, year = scan_date(new_text)
                    d = datetime.date(year, month, day)
                    entry.set_date(d)
        elif viewcol == BillEntryViewCol.PRODUCT:
            self.stop_cell_move = False
            self.begin_edit(entry)
            product = self.get_product_by_name(new_text)
            self.product_string = new_text
            if product is None:
                self.stop_cell_move = True
                GLib.idle_add(self.idle_product)
                return
            entry.set_product(product)
            entry.set_description(product.get_description())

        elif viewcol == BillEntryViewCol.DESCRIPTION:
            self.begin_edit(entry)
            entry.set_description(new_text)

        elif viewcol == BillEntryViewCol.QUANTITY:
            self.begin_edit(entry)
            qty = parse_exp_decimal(new_text)
            entry.set_quantity(qty, self.is_debit_note)

        elif viewcol == BillEntryViewCol.PRICE:
            self.begin_edit(entry)
            price = parse_exp_decimal(new_text)
            entry.set_price(price)

        elif viewcol == BillEntryViewCol.PRICE_RULE:
            pass

        if entry.get_bill() is None:
            self.bill.add_entry(entry)

    def get_current_entry(self):
        return self.current_entry

    def get_dirty_entry(self):
        return self.dirty_entry

    def set_dirty_entry(self, entry):
        if entry is None:
            self.set_user_data(self, "data-edited", False)
            self.dirty_entry = None
        else:
            self.set_user_data(self, "data-edited", True)
            self.dirty_entry = entry

    def set_current_path(self, mpath):
        if mpath is None: return
        model = self.get_model_from_view()
        if self.current_ref:
            self.current_ref = None
        self.current_ref = Gtk.TreeRowReference.new(model, mpath)

    def get_current_path(self):
        if self.current_ref is None:
            return None
        return self.current_ref.get_path()

    def cancel_edit(self, _):
        self.finish_edit()
        model = self.get_model_from_view()
        entry = self.dirty_entry
        if entry is not None and entry.is_open():
            self.goto_rel_entry_row(0)
            model.set_blank_split_parent(entry, True)
            self.set_user_data(self, "data-edited", False)
            self.dirty_entry.rollback_edit()
            model.set_blank_split_parent(entry, False)
            self.format_entry(self.dirty_entry)
            split = model.get_blank_split()
            split.reinit()
        self.dirty_entry = None
        self.change_allowed = False
        self.auto_complete = False
        self.call_uiupdate_cb()

    def delete_current_entry(self):
        self.finish_edit()
        entry = self.current_entry
        self.goto_rel_entry_row(1)
        if not entry.is_open():
            entry.begin_edit()
        self.set_dirty_entry(entry)
        entry.destroy()
        entry.commit_edit()
        self.set_dirty_entry(None)

    def enter(self):
        self.finish_edit()
        if self.entry_changed():
            return False
        if self.entry_confirm == BillEntryConfirm.DISCARD:
            return False
        return True

    def block_selection(self, block):
        sel = self.get_selection()
        if block:
            sel.handler_block_by_func(self.motion_cb)
        else:
            sel.handler_unblock_by_func(self.motion_cb)

    def get_parent(self):
        from gasstation.views.main_window import MainWindow
        return MainWindow.get_main_window(self)

    def set_read_only(self, read_only):
        model = self.get_model_from_view()
        model.read_only = read_only

    def cdf0(self, col, cell, s_model, s_iter, path_string):
        viewcol = self.get_user_data(cell, "view_column")
        model = self.get_model_from_view()
        spath = s_model.get_path(s_iter)
        editable = True
        indices = spath.get_indices()
        row_color = model.get_row_color(indices[0])
        if row_color is not None:
            cell.set_property("cell-background", row_color)
            cell.set_property("foreground", "black")
        else:
            cell.set_property("cell-background-set", 0)
            cell.set_property("foreground-set", 0)
        cell.set_property("xalign", 1.0)
        if viewcol == BillEntryViewCol.PRODUCT:
            cell.set_property("xalign", 0.0)
            cell.set_property("placeholder-text", "Select a product or service")
        elif viewcol == BillEntryViewCol.DESCRIPTION:
            cell.set_property("xalign", 0.0)
            cell.set_property("placeholder-text", "Enter a description")
        elif viewcol == BillEntryViewCol.SUBTOTAL:
            editable = False
        cell.set_property("editable", editable and self.location is not None)
        return False

    def entry_changed(self):
        self.entry_confirm = BillEntryConfirm.RESET
        if self.get_user_data(self, "data-edited") and self.entry_changed_confirm(None):
            if self.entry_confirm == BillEntryConfirm.CANCEL:
                if self.dirty_entry is not None and len(self.dirty_entry) > 2:
                    self.jump_to(None, self.dirty_entry.get_split(0), False)
                else:
                    self.jump_to(self.dirty_entry, None, False)
                return True

            if self.entry_confirm == BillEntryConfirm.DISCARD:
                self.dirty_entry = None
        return False

    def entry_changed_confirm(self, new_entry):
        title = "Save the changed entry?"
        message = "The current entry has changed. Would you like to " \
                  "record the changes, or discard the changes?"
        if self.dirty_entry is None or self.dirty_entry == new_entry:
            return False
        model = self.get_model_from_view()
        dialog = Gtk.MessageDialog(transient_for=self.private_window,
                                   modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE, title=title)
        dialog.format_secondary_text("%s" % message)
        dialog.add_buttons("_Discard Changes", Gtk.ResponseType.REJECT,
                           "_Cancel", Gtk.ResponseType.CANCEL,
                           "_Record Changes", Gtk.ResponseType.ACCEPT)
        response = gs_dialog_run(dialog, PREF_WARN_INV_ENTRY_MOD)
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            self.set_user_data(self, "data-edited", False)
            self.dirty_entry.commit_edit()
            self.dirty_entry = None
            self.change_allowed = False
            self.auto_complete = False
            self.entry_confirm = BillEntryConfirm.ACCEPT
            return False

        elif response == Gtk.ResponseType.REJECT:
            if self.dirty_entry is not None and self.dirty_entry.is_open():
                self.goto_rel_entry_row(0)
                self.set_user_data(self, "data-edited", False)
                split = model.get_blank_split()
                split.reinit()
                self.change_allowed = False
                self.auto_complete = False
                self.entry_confirm = BillEntryConfirm.DISCARD
            return True

        elif response == Gtk.ResponseType.CANCEL:
            self.entry_confirm = BillEntryConfirm.CANCEL
            return True
        else:
            return False

    def set_uiupdate_cb(self, cb, *args):
        self.uiupdate_cb = cb
        self.uiupdate_cb_data = args

    def call_uiupdate_cb(self):
        if self.uiupdate_cb is not None:
            self.uiupdate_cb(*self.uiupdate_cb_data)
        return False

    def begin_edit(self, entry):
        if entry != self.dirty_entry:
            t = entry.get_date()
            if not entry.is_open():
                entry.begin_edit()
            self.dirty_entry = entry
            if t is None:
                t = datetime.datetime.now()
                entry.set_date(t)

    def pref_changed(self, _, pref, *args):
        if pref.endswith(PREF_DRAW_HOR_LINES) or pref.endswith(PREF_DRAW_VERT_LINES):
            self.use_horizontal_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                         PREF_DRAW_HOR_LINES)

            self.use_vertical_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                       PREF_DRAW_VERT_LINES)

            if self.use_horizontal_lines:
                if self.use_vertical_lines:
                    self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
                else:
                    self.set_grid_lines(Gtk.TreeViewGridLines.HORIZONTAL)

            elif self.use_vertical_lines:
                self.set_grid_lines(Gtk.TreeViewGridLines.VERTICAL)
            else:
                self.set_grid_lines(Gtk.TreeViewGridLines.NONE)

    def finish_edit(self):
        super().finish_edit()
        while Gtk.events_pending():
            Gtk.main_iteration()

    def editing_canceled_cb(self, renderer, *args):
        if self.get_user_data(self, "data-edited") is None:
            self.dirty_entry = None
        if self.stop_cell_move:
            self.stop_cell_move = False
            GLib.idle_add(self.idle_product)
        self.help_text = " "
        self.emit("help_signal", 0)
        self.set_user_data(renderer, "edit-canceled", True)

    def help(self, _, viewcol):
        help_text = " "
        if viewcol == BillEntryViewCol.SERVICE_DATE:
            current_string = self.get_user_data(self.temp_cr, "current-string")
            help_text = self.get_date_help(self.parse_date(current_string))

        elif viewcol == BillEntryViewCol.PRODUCT:
            help_text = "Enter a product from list"

        elif viewcol == BillEntryViewCol.DESCRIPTION:
            help_text = "Enter description of the product"

        elif viewcol == BillEntryViewCol.QUANTITY:
            help_text = "Enter the quantity"

        elif viewcol == BillEntryViewCol.PRICE:
            help_text = "Enter the selling price"

        elif viewcol == BillEntryViewCol.PRICE_RULE:
            help_text = "Choose the price rule"
        self.help_text = help_text
        self.emit("help_signal", 0)

    def remove_edit_date(self, ce):
        if self.temp_cr:
            popup_entry = self.get_user_data(self.temp_cr, "cell-editable")
            new_string = popup_entry.get_child().get_text()
            current_string = self.get_user_data(self.temp_cr, "current-string")
            if not self.get_user_data(self.temp_cr, "edit-canceled") and GLib.strcasecmp(new_string,
                                                                                         current_string):
                self.set_user_data(self, "data-edited", True)
            date = GLib.Date()
            self.parse_date(date, new_string)
            date_string = self.get_date_help(date)
            self.help_text = date_string
            self.emit("help_signal", 0)
            self.set_user_data(self.temp_cr, "cell-editable", None)
            self.temp_cr = None
            self.editing_now = False

    def get_blank_entry(self):
        model = self.get_model_from_view()
        return model.get_blank_entry()

    def save(self, reg_closing):
        self.finish_edit()
        if reg_closing:
            self.reg_closing = True
        dirty_entry = self.get_dirty_entry()
        blank_entry = self.get_blank_entry()
        entry = self.get_current_entry()
        if entry is None:
            return False
        if not entry.is_open():
            return False

        if entry == dirty_entry:
            if entry != blank_entry:
                entry.commit_edit()
                self.set_dirty_entry(None)
                return True
            else:
                if len(entry) == 0:
                    title = "Not enough information for Blank Transaction?"
                    message = "The blank entry does not have enough information " \
                              "to save it. Would you like to return to the entry" \
                              " to update, or cancel the save?"
                    window = self.private_window
                    dialog = Gtk.MessageDialog(window,
                                               destroy_with_parent=True,
                                               message_type=Gtk.MessageType.QUESTION,
                                               buttons=Gtk.ButtonsType.CANCEL, message_format=title)
                    dialog.format_secondary_text(message)
                    dialog.add_button("_Return", Gtk.ResponseType.ACCEPT)

                    dialog.get_widget_for_response(Gtk.ResponseType.ACCEPT).grab_focus()
                    response = dialog.run()
                    dialog.destroy()
                    if response != Gtk.ResponseType.ACCEPT:
                        return True
                    return False
                entry.commit_edit()
                self.set_dirty_entry(None)
                return True
        return True

    def move_current_entry_updown(self, move_up):
        return self._move_current_entry_updown(move_up, True)

    def is_current_movable_updown(self, move_up):
        return self._move_current_entry_updown(move_up, False)

    def _move_current_entry_updown(self, move_up, really_do_it):
        model = self.get_model_from_view()
        resultvalue = False
        if model is None: return False
        resultvalue = True
        return resultvalue

    @staticmethod
    def parse_date(datestr):
        use_auto_readonly = Session.get_current_book().uses_auto_readonly()
        if datestr is None:
            return
        if not scan_date(datestr):
            today = datetime.date.today()
            day, month, year = today.day, today.month, today.year
        else:
            day, month, year = scan_date(datestr)
        if use_auto_readonly:
            d = datetime.date(day=day, month=month, year=year)
            readonly_threshold = Session.get_current_book().get_auto_readonly_date()
            if d < readonly_threshold:
                dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                           "Cannot store a transaction at this date")
                dialog.format_secondary_text("The entered date of the new transaction is older than the"
                                             " \"Read-Only Threshold\" set for this book. "
                                             "This setting can be changed in File -> Properties -> Accounts.")
                dialog.run()
                dialog.destroy()
                day = readonly_threshold.day
                month = readonly_threshold.month
                year = readonly_threshold.year
        return datetime.date(day=day, month=month, year=year)

    @staticmethod
    def get_date_help(date):
        return date.strftime("%A %d %B %Y")

    def ed_key_press_cb(self, widget, event):
        spath, col = self.get_cursor()
        if event.type != Gdk.EventType.KEY_PRESS:
            return False
        if event.keyval == Gdk.KEY_Up or event.keyval == Gdk.KEY_Down:
            if spath is None:
                return True
            self.finish_edit()
            if self.stop_cell_move:
                return True
            if event.keyval == Gdk.KEY_Up:
                spath.prev()
            else:
                spath.next()
            self.set_cursor(spath, col, True)
        elif event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return:
            if spath is None:
                return True
            if self.enter():
                GLib.idle_add(self.jump_to_blank)
            return True

    def jump_to_blank(self):
        model: BillEntryModel = self.get_model_from_view()
        blank = model.blank_entry
        spath = model.get_path_from_entry(blank)
        self.scroll_to_cell(spath, None, False, 1.0, 0.0)
        self.set_cursor(spath, None, False)

    def key_press_cb(self, widget, event):
        editing = False
        step_off = False
        entry_changed = False
        spath, col = self.get_cursor()
        if event.type != Gdk.EventType.KEY_PRESS:
            return False
        if event.keyval in [Gdk.KEY_plus, Gdk.KEY_minus, Gdk.KEY_KP_Add, Gdk.KEY_KP_Subtract]:
            return True

        elif event.keyval in [Gdk.KEY_Up, Gdk.KEY_Down]:

            if event.keyval == Gdk.KEY_Up:
                pass
            return False

        elif event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return:
            if spath is None:
                return True
            if self.enter():
                GLib.idle_add(self.jump_to_blank)
            return True

        elif event.keyval in [Gdk.KEY_Tab, Gdk.KEY_ISO_Left_Tab, Gdk.KEY_KP_Tab]:
            if spath is None:
                return True
            if event.state & Gdk.ModifierType.CONTROL_MASK:
                self.auto_complete = True
            self.finish_edit()
            if self.stop_cell_move:
                return True
            while not editing and not step_off:
                start_spath = spath.copy()
                start_indices = start_spath.get_indices()
                ncol = self.keynav(col, spath, event)
                next_indices = spath.get_indices()
                if start_indices[0] != next_indices[0]:
                    if self.dirty_entry is not None:
                        entry_changed = True
                    self.change_allowed = False

                if spath is None or not self.path_is_valid(spath) or entry_changed:
                    if self.entry_changed():
                        return True
                    step_off = True
                if self.entry_confirm != BillEntryConfirm.DISCARD:
                    self.set_cursor(spath, ncol, True)
                editing = self.get_editing(ncol)
            return True
        else:
            return False

    def destroy(self):
        model = self.get_model_from_view()
        model.destroy()
        super().destroy()


GObject.type_register(BillEntryView)
