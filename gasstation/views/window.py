import gi

gi.require_version("Gtk", "3.0")
from abc import abstractmethod
from gi.repository import Gtk


class Window:
    progress_bar_hack_window = None

    @abstractmethod
    def get_gtk_window(self):
        pass

    @abstractmethod
    def get_statusbar(self) -> Gtk.Statusbar:
        pass

    @abstractmethod
    def get_progressbar(self) -> Gtk.ProgressBar:
        pass

    @abstractmethod
    def ui_set_sensitive(self, sensitive: bool):
        pass

    def update_status(self, page):
        statusbar = self.get_statusbar()
        message = page.get_statusbar_text()
        statusbar.pop(0)
        statusbar.push(0, message if message else "")

    def set_status(self, page, message):
        if page is None:
            return
        page.set_statusbar_text(message)
        self.update_status(page)

    @classmethod
    def set_progressbar_window(cls, window):
        cls.progress_bar_hack_window = window

    @classmethod
    def get_progressbar_window(cls):
        return cls.progress_bar_hack_window

    @classmethod
    def show_progress(cls, message, percentage):
        window = cls.progress_bar_hack_window
        if window is None:
            return
        progressbar = window.get_progressbar()
        if progressbar is None:
            return
        if percentage < 0:
            progressbar.set_text(" ")
            progressbar.set_fraction(0.0)
            window.ui_set_sensitive(True)
        else:
            if message:
                progressbar.set_text(message)
            if percentage == 0:
                window.ui_set_sensitive(False)
            if percentage <= 100:
                progressbar.set_fraction(percentage / 100)
            else:
                progressbar.pulse()
        while Gtk.events_pending():
            Gtk.main_iteration()
