from gi import require_version

require_version("Gtk", "3.0")
from gi.repository import GObject, GLib
from libgasstation.core.event import *
from gasstation.utilities.gtk import Combo
from gasstation.utilities.custom_dialogs import *
from libgasstation.core.session import Session
from libgasstation.core.account import Account

ACCT_DATA_TAG = "gs-account-sel_acct"


class AccountSelection(Gtk.Box):
    __gtype_name__ = "AccountSelection"
    __gsignals__ = {
        'selection_changed': (GObject.SignalFlags.RUN_FIRST, None, (object,))
    }

    def __init__(self, depend_on=None, depends_cb=None, references_cb=None):
        super().__init__()
        self.is_modal = False
        self.account_commodity_filters = None
        self.new_button = None
        self.new_is_clicked = False
        self.account_type_filters = None
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.depends_on = depend_on
        self.depends_cb = depends_cb
        self.references_cb = references_cb
        if self.depends_on is not None:
            self.set_sensitive(False)
            self.depends_on.connect("selection_changed", self.depends_changed_cb)
        self.store = Gtk.ListStore(str, object)
        self.combo = Gtk.ComboBox.new_with_model_and_entry(self.store)
        self.combo.set_entry_text_column(0)
        self.combo.connect("changed", self.combo_changed_cb)
        self.combo.get_child().connect("focus-out-event", self.combo_focus_cb)
        self.pack_start(self.combo, True, True, 0)
        self.combo.show()
        Combo.require_list_item(self.combo)
        self.__populate_list()
        self.event_handler_id = Event.register_handler(self.event_cb)

    def depends_changed_cb(self, _, ref):
        self.set_sensitive(True)
        self.__populate_list(ref)
        self.combo.set_active(1)

    def combo_focus_cb(self, entry, _):
        from gasstation.views.main_window import MainWindow
        if entry is None:
            return False
        found = None
        text = entry.get_text()
        if text == "":
            return True
        model = self.store
        _iter = model.get_iter_first()
        while _iter is not None:
            name = model.get_value(_iter, 0)
            if text == name:
                found = model.get_value(_iter, 1)
                break
            _iter = model.iter_next(_iter)
        missing = "The account %s does not exist.Would you like to create it?"
        window = MainWindow.get_main_window(self)
        if found is None:
            from gasstation.views.dialogs.account import AccountDialog
            if not ask_yes_no(window, True, missing % text):
                return False
            self.new_is_clicked = True
            dialog = AccountDialog.new_from_name_window_with_types(window, text, self.account_type_filters)
            dialog.run()
        return True

    def combo_changed_cb(self, combo):
        _iter = combo.get_active_iter()
        if _iter is None:
            return False
        acc = self.store.get_value(_iter, 1)
        self.emit("selection_changed", acc)
        return True

    def event_cb(self, entity, event_type, *args):
        if not (event_type == EVENT_CREATE
                or event_type == EVENT_MODIFY
                or event_type == EVENT_DESTROY) or not isinstance(entity, Account):
            return
        self.__populate_list()
        if self.new_is_clicked and event_type == EVENT_MODIFY:
            self.set_account(entity, set_default_acct=False)
            self.new_is_clicked = False
        return True

    def __populate_list(self, benefactor=None):
        active = 0
        entry = self.combo.get_child()
        if entry is None:
            return
        current_sel = entry.get_text()
        self.combo.handler_block_by_func(self.combo_changed_cb)
        root = Session.get_current_root()
        self.store.clear()
        self.store.append(["", None])
        i = 1
        for acc in filter(self.filter_accounts, root.get_descendants_sorted()):
            name = acc.get_full_name()
            if name is None or (self.depends_cb is not None and benefactor is not None and
                                self.depends_cb(acc) != benefactor) \
                    or (self.references_cb is not None and benefactor is not None and self.references_cb(
                benefactor) != acc):
                continue
            self.store.append([name, acc])
            if GLib.utf8_collate(name, current_sel) == 0:
                active = i
            i += 1
        if active != -1:
            self.combo.set_active(active)
        self.combo.handler_unblock_by_func(self.combo_changed_cb)

    def filter_accounts(self, account):
        if account.get_placeholder():
            return False
        if self.account_type_filters is not None:
            if not account.get_type() in self.account_type_filters:
                return False
        if self.account_commodity_filters is not None:
            if not account.get_commodity() in self.account_commodity_filters:
                return False
        return True

    def __find_account(self, model, path, _iter, acct):
        model_acc = model.get_value(_iter, 1)
        if acct != model_acc:
            return False
        self.combo.set_active_iter(_iter)
        self.emit("selection_changed", acct)
        return True

    def set_account(self, acct, set_default_acct=True):
        if set_default_acct and acct is None:
            self.combo.set_active(0)
            if acct is None:
                return
        else:
            self.combo.set_active(-1)
            if acct is None:
                entry = self.combo.get_child()
                entry.delete_text(0, -1)
                return
        self.store.foreach(self.__find_account, acct)

    def get_account(self):
        _iter = self.combo.get_active_iter()
        if _iter is None:
            return None
        acc = self.store.get_value(_iter, 1)
        return acc

    def set_account_filters(self, type_filters=None, commodity_filters=None):
        if self.account_type_filters is not None:
            self.account_type_filters = None
        if self.account_commodity_filters is not None:
            self.account_commodity_filters = None
        if type_filters is None and commodity_filters is None:
            return
        if type_filters is not None:
            self.account_type_filters = type_filters.copy()
        if commodity_filters is not None:
            self.account_commodity_filters = commodity_filters.copy()
        self.__populate_list()

    def set_new_account_ability(self, state):
        if state == self.new_button is not None:
            return
        if self.new_button:
            self.remove(self.new_button)
            self.new_button.destroy()
            self.new_button = None
            return
        self.new_button = Gtk.Button.new_from_icon_name("list-add", Gtk.IconSize.BUTTON)
        self.new_button.connect("clicked", self.new_account_click)
        self.new_button.set_can_focus(False)
        self.pack_start(self.new_button, False, False, 0)
        self.new_button.show()

    def set_new_account_modal(self, state):
        self.is_modal = state

    def new_account_click(self, b):
        from gasstation.views.dialogs.account import AccountDialog
        parent = self.get_toplevel()
        if parent is not None:
            parent = parent.get_toplevel()
        self.new_is_clicked = True
        if self.is_modal:
            dialog = AccountDialog.new_from_name_window_with_types(parent, None, self.account_type_filters)
        else:
            dialog = AccountDialog.new_with_types(parent, Session.get_current_book(), self.account_type_filters)
        dialog.run()

    def get_num_account(self):
        return self.store.iter_n_children(None)

    def purge_account(self, target, recursive):
        model = self.store
        more = True
        _iter = model.get_iter_first()
        if _iter is None:
            return
        if not recursive:
            while _iter is not None:
                acc = model.get_value(_iter, 1)
                if acc == target:
                    self.store.remove(_iter)
                    break
                _iter = model.iter_next(_iter)
        else:
            while more:
                acc = model.get_value(_iter, 1)
                while acc is not None:
                    if acc == target:
                        break
                    acc = acc.get_parent()
                if acc == target:
                    more = self.store.remove(_iter)
                else:
                    _iter = model.iter_next(_iter)
                    if _iter is None:
                        more = False
        self.combo.set_active(0)


GObject.type_register(AccountSelection)
