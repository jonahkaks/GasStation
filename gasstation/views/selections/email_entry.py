import re
email_regex = re.compile(r"^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+("
                         r"\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?("
                         r"\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?("
                         r"\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|["
                         r"\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$")
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, Gdk

ICON_POSITION = Gtk.EntryIconPosition.SECONDARY


class EmailEntry(Gtk.Entry):
    __gtype_name__ = "EmailEntry"

    def __init__(self):
        super().__init__()
        self.set_icon_sensitive(ICON_POSITION, False)
        self.connect("icon-release", self.icon_release_cb)
        self.connect("changed", self.text_to_sensitive)
        self.show()

    def text_to_sensitive(self, widget):
        text = widget.get_text()
        self.set_icon_sensitive(ICON_POSITION, text.strip() != "")
        return True

    def icon_release_cb(self, entry, icon_position, event):
        toplevel = entry.get_toplevel()
        toplevel = toplevel if Gtk.Widget.is_toplevel(toplevel) else None
        if icon_position == ICON_POSITION:
            text = entry.get_text()
            if text is None or text == "":
                return
            Gtk.show_uri_on_window(toplevel, text, Gdk.CURRENT_TIME)

    def set_icon_visible(self, visible):
        if visible:
            self.set_icon_from_icon_name(ICON_POSITION, "go-jump")
            self.set_placeholder_text("Enter a URL here")
        else:
            self.set_icon_from_icon_name(ICON_POSITION, "")
            self.set_placeholder_text(None)

    def get_icon_visible(self):
        return self.get_icon_name(ICON_POSITION) is not None


GObject.type_register(EmailEntry)

