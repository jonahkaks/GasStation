from gi import require_version

from gasstation.utilities.gtk import Combo

require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GLib
from libgasstation.core.event import *
from gasstation.utilities.dialog import gs_widget_set_style_context, gs_widget_remove_style_context
from gasstation.utilities.custom_dialogs import ask_yes_no

from libgasstation.core.session import Session


class ReferenceSelection(Gtk.Box):
    __gsignals__ = {
        'selection_changed': (GObject.SignalFlags.RUN_LAST, None, (object,))
    }

    def __init__(self, obj, new_cb=None, depend_on=None, depends_cb=None, references_cb=None,secondary=False,use_first=True, **kwargs):
        super().__init__(**kwargs)
        self.obj = obj
        self.depends_on = depend_on
        self.depends_cb = depends_cb
        self.secondary = secondary
        self.references_cb = references_cb
        self.use_first = use_first
        if self.depends_on is not None:
            self.set_sensitive(False)
            self.depends_on.connect("selection_changed", self.depends_changed_cb)
        self.new_cb = new_cb
        self.new_is_clicked = False
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.new_button = None
        gs_widget_set_style_context(self, "ReferenceSel")
        self.store = Gtk.ListStore(str, object)
        self.combo: Gtk.ComboBox = Gtk.ComboBox.new_with_model_and_entry(self.store)
        self.combo.set_entry_text_column(0)
        self.combo.get_child().connect("focus-out-event", self.combo_focus_cb)
        self.combo.connect("changed", self.combo_changed_cb)
        self.pack_start(self.combo, True, True, 0)
        self.combo.show()
        Combo.require_list_item(self.combo)
        self.__populate_list()
        self.eventHandlerId = Event.register_handler(self.event_cb)
        self.show()

    def depends_changed_cb(self, widget, ref):
        self.set_sensitive(True)
        self.__populate_list(ref)

    def combo_focus_cb(self, entry, _):
        if entry is None:
            return False
        found = None
        text = entry.get_text()
        if text == "":
            return True
        model = self.store
        _iter = model.get_iter_first()
        while _iter is not None:
            name = model.get_value(_iter, 0)
            if text == name:
                found = model.get_value(_iter, 1)
                break
            _iter = model.iter_next(_iter)

        missing = "%s does not exist in the list.\nWould you like to create it?"
        if found is None:
            from gasstation.views.main_window import MainWindow
            window = MainWindow.get_main_window(self)
            if not ask_yes_no(window, True, missing % text):
                return False
            self.new_is_clicked = True
            dialog = self.new_cb(window, Session.get_current_book(), name=text)
            dialog.run()
        return True

    def combo_changed_cb(self, combo):
        _iter = combo.get_active_iter()
        if _iter is None:
            return False
        acc = self.store.get_value(_iter, 1)
        self.emit("selection_changed", acc)
        return True

    def event_cb(self, entity, event_type, *args):
        if not (event_type == EVENT_CREATE
                or event_type == EVENT_MODIFY
                or event_type == EVENT_DESTROY) or not isinstance(entity, self.obj):
            return
        if self.new_is_clicked and event_type == EVENT_MODIFY:
            self.__populate_list()
            self.set_ref(entity)
            self.new_is_clicked = False

    def __populate_list(self, benefactor=None):
        active = -1
        entry = self.combo.get_child()
        if entry is None:
            return
        current_sel = entry.get_chars(0, -1)
        self.combo.handler_block_by_func(self.combo_changed_cb)
        self.store.clear()
        self.store.append(["", None])
        lis = self.obj.get_list(Session.get_current_book())
        i = 1
        for ref in lis:
            name = ref.get_name()
            if name is None or (self.depends_cb is not None and benefactor is not None
                                and self.depends_cb(ref) != (self.depends_cb(benefactor) if self.secondary else benefactor)) \
                    or (self.references_cb is not None and benefactor is not None and self.references_cb(
                benefactor) != (self.references_cb(ref) if self.secondary else ref)):
                continue
            self.store.append([name, ref])
            if GLib.utf8_collate(name, current_sel) == 0:
                active = i
            i += 1
        if active != -1:
            self.combo.set_active(active)
        elif i > 1 and self.use_first:
            self.combo.set_active(1)
        self.combo.handler_unblock_by_func(self.combo_changed_cb)

    def __find_ref(self, model, path, _iter, acct):
        model_acc = model.get_value(_iter, 1)
        if acct != model_acc:
            return False
        self.combo.set_active_iter(_iter)
        self.emit("selection_changed", acct)
        return True

    def set_ref(self, entity, *_):
        if entity is not None and not isinstance(entity, self.obj):
            raise TypeError("Expected type {} got {}".format(type(object), type(entity)))
        if entity is None:
            entry = self.combo.get_child()
            entry.delete_text(0, -1)
            self.combo.set_active_iter(None)
            return
        self.store.foreach(self.__find_ref, entity)

    def get_ref(self):
        _iter = self.combo.get_active_iter()
        if _iter is None:
            return None
        acc = self.store.get_value(_iter, 1)
        return acc

    def set_new_ability(self, state):
        if state == (self.new_button is not None):
            return
        if self.new_button is not None:
            self.remove(self.new_button)
            gs_widget_remove_style_context(self.combo, "linked")
            self.new_button.destroy()
            self.new_button = None
            return
        gs_widget_set_style_context(self, "linked")
        self.new_button = Gtk.Button.new_from_icon_name("list-add", Gtk.IconSize.BUTTON)
        self.new_button.set_can_focus(False)
        self.new_button.connect("clicked", self.new_click)
        self.pack_start(self.new_button, False, False, 0)
        self.new_button.show()

    def new_click(self, _):
        parent = self.get_toplevel()
        if parent is not None:
            parent = parent.get_toplevel()
        self.new_is_clicked = True
        dialog = self.new_cb(parent, Session.get_current_book())
        dialog.run()

    def get_num_ref(self):
        return self.store.iter_n_children(None)

    def purge_ref(self, target, recursive):
        model = self.store
        more = True
        _iter = model.get_iter_first()
        if _iter is None:
            return
        if not recursive:
            while _iter is not None:
                acc = model.get_value(_iter, 1)
                if acc == target:
                    self.store.remove(_iter)
                    break
                _iter = model.iter_next(_iter)
        else:
            while more:
                acc = model.get_value(_iter, 1)
                while acc is not None:
                    if acc == target:
                        break
                    acc = acc.get_parent()
                if acc == target:
                    more = self.store.remove(_iter)
                else:
                    _iter = model.iter_next(_iter)
                    if _iter is None:
                        more = False
        self.combo.set_active(0)
