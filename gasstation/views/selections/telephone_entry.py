import gi
import tzlocal
from sqlalchemy_utils import PhoneNumber

from .countries import countries

gi.require_version('Gtk', '3.0')
from gasstation.utilities.dialog import gs_widget_set_style_context, gs_draw_arrow_cb
from gi.repository import Gtk, GObject, GLib


class TelNumberEntry(Gtk.Box):
    __gtype_name__ = "TelNumberEntry"
    store = Gtk.ListStore(str, str, str)
    active_item = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        gs_widget_set_style_context(self, "linked")
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        gs_widget_set_style_context(self, "TelNumberEntry")
        self.popup = Gtk.Popover()
        self.button = Gtk.MenuButton()
        self.button.set_relief(Gtk.ReliefStyle.HALF)
        self.button.set_always_show_image(True)
        self.pack_start(self.button, False, False, 0)
        self.button.show()

        arrow = Gtk.Image.new_from_icon_name("pan-down-symbolic", Gtk.IconSize.BUTTON)
        arrow.connect("draw", gs_draw_arrow_cb, 1)
        self.button.add(arrow)
        arrow.show()

        self.entry = Gtk.Entry()
        self.entry.set_input_purpose(Gtk.InputPurpose.PHONE)
        self.entry.set_placeholder_text("(000)-754-4422")
        # self.entry.connect("insert-text", self.insert_text_cb)
        self.pack_start(self.entry, True, True, 0)
        self.entry.show()

        self.button.set_popover(self.popup)
        treeview: Gtk.TreeView = Gtk.TreeView.new_with_model(self.__class__.store)
        selection = treeview.get_selection()
        selection.connect("changed", self.selection_changed_cb)
        treeview.connect("row-activated", lambda *_: self.popup.popdown())
        treeview.set_headers_visible(False)
        col = Gtk.TreeViewColumn()
        col.set_title("Countries")
        render_pixbuf = Gtk.CellRendererPixbuf()
        col.pack_start(render_pixbuf, False)

        col.set_attributes(render_pixbuf, icon_name=0)
        render = Gtk.CellRendererText()
        col.pack_start(render, False)
        col.set_attributes(render, text=1)

        treeview.append_column(col)
        box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 4)
        entry = Gtk.SearchEntry()
        treeview.set_search_entry(entry)
        box.pack_start(entry, False, False, 0)
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_min_content_height(200)
        scrolled_window.set_min_content_height(300)
        scrolled_window.add(treeview)
        scrolled_window.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        frame = Gtk.Frame()
        frame.set_shadow_type(Gtk.ShadowType.NONE)
        frame.add(scrolled_window)
        box.pack_start(frame, True, True, 0)
        self.popup.add(box)
        path = Gtk.TreePath.new_from_indices([self.__class__.active_item])
        selection.select_path(path)
        self.selection = selection
        box.show_all()
        self.show()

    @classmethod
    def build_menu(cls):
        local_zone = tzlocal.get_localzone_name()
        for i, (country, data) in enumerate(countries.items()):
            iso_code = data["iso2"].lower()
            cls.store.append([iso_code, country+" "+data["phone_code"].replace("+", ""), data["phone_code"]])
            if local_zone == data["timezone"]:
                cls.active_item = i

    def selection_changed_cb(self, selection: Gtk.TreeSelection):
        store, _iter = selection.get_selected()
        if _iter is not None:
            country_icon_name = store.get_value(_iter, 0)
            code = store.get_value(_iter, 2)
            self.button.set_image(Gtk.Image.new_from_icon_name(country_icon_name, Gtk.IconSize.BUTTON))
            self.button.set_image_position(Gtk.PositionType.LEFT)
            self.button.set_label(code)

    def get_code(self):
        return self.button.get_label()

    def set_number(self, number: PhoneNumber):
        if number is None:
            return
        if isinstance(number, str):
            try:
                number = PhoneNumber(number)
            except:
                return
        model = self.__class__.store
        _iter = model.get_iter_first()
        while _iter is not None:
            code = model.get_value(_iter, 2)
            if code == number.country_code:
                self.selection.select_iter(_iter)
                break
            _iter = model.iter_next(_iter)
        self.entry.set_text(str(number.national_number))

    def get_number(self):
        nn = self.entry.get_text()
        return self.get_code() + nn if nn != '' else None

    def insert_text_cb(self, entry, text, length, position):
        pos = entry.get_position()
        old_text = entry.get_text()
        ins_dig = ''.join([c for c in text if c.isdigit()])
        # Second we insert digits at pos, truncate extra-digits
        new_text = ''.join([old_text[:pos], ins_dig, old_text[pos:]])[:17]
        # Third we filter digits in `new_text`, fill the rest with underscores
        new_dig = ''.join([c for c in new_text if c.isdigit()]).ljust(13, '_')
        # We are ready to format
        new_text = '({0}) {1} {2}-{3}'.format(new_dig[:3], new_dig[3:5],
                                              new_dig[5:9], new_dig[9:13]).split('_')[0]

        # Find the new cursor position

        # We get the number of inserted digits
        n_dig_ins = len(ins_dig)
        # We get the number of digits before
        n_dig_before = len([c for c in old_text[:pos] if c.isdigit()])
        # We get the unadjusted cursor position
        new_pos = pos + n_dig_ins

        # If there was no text in the entry, we added a '+' sign, therefore move cursor
        new_pos += 1 if not old_text else 0
        # Spacers are before digits 4, 6 and 10
        for i in [4, 6, 10]:
            # Is there spacers in the inserted text?
            if n_dig_before < i <= n_dig_before + n_dig_ins:
                # If so move cursor
                new_pos += 1

        if new_text:
            entry.handler_block_by_func(self.insert_text_cb)
            entry.set_text(new_text)
            entry.handler_unblock_by_func(self.insert_text_cb)
            GLib.idle_add(entry.set_position, new_pos)

        entry.stop_emission_by_name("insert_text")


GObject.type_register(TelNumberEntry)
GLib.idle_add(TelNumberEntry.build_menu)
