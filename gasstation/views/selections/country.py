import tzlocal
from gi import require_version

from .countries import countries

require_version('Gtk', '3.0')
from gi.repository import GObject
from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.utilities.gtk import *


class CountrySelection(Gtk.Entry):
    __gtype_name__ = "CountrySelection"
    store = Gtk.ListStore(str, str, object)

    def __init__(self):
        super().__init__()
        gs_widget_set_style_context(self, "CountrySelection")
        self.set_placeholder_text("Select a country")
        local_zone = tzlocal.get_localzone_name()
        country_name_selected = None
        store = self.__class__.store
        for country, data in countries.items():
            store.append([data["iso2"].lower(), country, data["cities"]])
            if local_zone == data["timezone"]:
                country_name_selected = country
        completion = Gtk.EntryCompletion()
        completion.set_model(store)
        renderer = Gtk.CellRendererPixbuf()
        completion.pack_start(renderer, False)
        completion.add_attribute(renderer, "icon-name", 0)
        completion.set_text_column(1)
        completion.set_inline_completion(True)
        self.set_completion(completion)
        self.connect("changed", self.entry_changed_cb)
        if country_name_selected is not None:
            GLib.idle_add(self.set_text, country_name_selected)

    def set_country(self, country):
        self.set_text(country)

    def get_country(self):
        return self.get_text()

    def get_selected_cities(self):
        text = self.get_text()
        completion = self.get_completion()
        model = completion.get_model()
        _iter = model.get_iter_first()
        while _iter is not None:
            if model.get_value(_iter, 1) == text:
                return model.get_value(_iter, 2)
            _iter = model.iter_next(_iter)
        return []

    def entry_changed_cb(self, entry):
        text = self.get_text()
        self.handler_block_by_func(self.entry_changed_cb)
        completion = self.get_completion()
        model = completion.get_model()
        _iter = model.get_iter_first()
        while _iter is not None:
            if model.get_value(_iter, 1) == text:
                self.set_icon_from_icon_name(Gtk.EntryIconPosition.PRIMARY, model.get_value(_iter, 0))
                break
            _iter = model.iter_next(_iter)
        self.handler_unblock_by_func(self.entry_changed_cb)


GObject.type_register(CountrySelection)


class EntryCompletionSelection(Gtk.Entry):
    __gtype_name__ = "EntryCompletionSelection"

    def __init__(self):
        super().__init__()
        gs_widget_set_style_context(self, "EntryCompletionSelection")
        store = Gtk.ListStore(str)
        self.set_placeholder_text("Select")
        completion = Gtk.EntryCompletion()
        completion.set_model(store)
        completion.set_text_column(0)
        completion.set_inline_completion(True)
        self.set_completion(completion)
        self.store = store

    def reload_model(self, cities):
        self.store.clear()
        for city in sorted(set(cities)):
            self.store.append([city])


GObject.type_register(EntryCompletionSelection)
