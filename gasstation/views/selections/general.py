from gi import require_version

from gasstation.utilities.dialog import gs_widget_set_style_context

require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject


class GeneralSelectType(GObject.GEnum):
    SELECT = 1
    EDIT = 2
    VIEW = 3


class GeneralSelect(Gtk.Box):
    __gtype_name__ = "GeneralSelect"
    __gsignals__ = {
        "changed": (GObject.SignalFlags.RUN_FIRST, None, (int,))}
    get_string = None
    new_select = None
    cb_arg = None

    def __init__(self, _type, *args, **kwargs):
        Gtk.Box.__init__(self, Gtk.Orientation.HORIZONTAL, 0)
        gs_widget_set_style_context(self, "linked")
        self.disposed = False
        self.selected_item = None
        self.entry = Gtk.Entry()
        self.entry.set_editable(False)
        self.pack_start(self.entry, True, True, 0)
        if _type == GeneralSelectType.SELECT:
            self.button = Gtk.Button.new_with_label("Select...")
        elif _type == GeneralSelectType.EDIT:
            self.button = Gtk.Button.new_with_label("Edit...")
        elif _type == GeneralSelectType.VIEW:
            self.button = Gtk.Button.new_with_label("View...")
        self.pack_start(self.button, False, False, 0)
        self.button.connect("clicked", self.select_cb)

    def select_cb(self, button):
        toplevel = button.get_toplevel()
        new_selection = self.new_select(toplevel, self.selected_item, self.cb_arg)
        if new_selection is None:
            return
        self.set_selected(new_selection)

    def __new__(cls, _type, get_string=None, new_select=None, cb_arg=None, *args, **kwargs):
        if get_string is None or new_select is None:
            return
        gsl = super().__new__(cls, _type, *args, **kwargs)
        gsl.get_string = get_string
        gsl.new_select = new_select
        gsl.cb_arg = cb_arg
        return gsl

    def get_printname(self, selection):
        return self.get_string(selection)

    def set_selected(self, selection):
        self.selected_item = selection
        if selection is None:
            text = ""
        else:
            text = self.get_printname(selection)
        self.entry.set_text(text)
        self.emit("changed", 0)

    def get_selected(self):
        return self.selected_item

    def make_mnemonic_target(self, label):
        label.set_mnemonic_widget(self.entry)


GObject.type_register(GeneralSelect)
