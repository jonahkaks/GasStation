from gi.repository import GObject

from gasstation.utilities.dialog import *


class DateSelectionFlags:
    SHOW_TIME = 1
    HR24 = 2


class DateSelection(Gtk.Box):
    __gtype_name__ = "DateSelection"
    __gsignals__ = {
        'date_changed': (GObject.SignalFlags.RUN_FIRST, None, (object,))}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        gs_widget_set_style_context(self, "DateSelection")
        gs_widget_set_style_context(self, "linked")
        self.disposed = False
        self.in_selected_handler = False
        self.popup_in_progress = False
        self.date_entry = Gtk.Entry()
        self.date_entry.connect("key-press-event", self.key_press_entry)
        self.date_entry.connect("focus-out-event", self.date_focus_out_event)

        self.pack_start(self.date_entry, True, True, 0)
        self.date_button = Gtk.MenuButton()
        self.pack_start(self.date_button, False, False, 0)
        self.date_button.set_can_focus(False)

        gs_widget_remove_style_context(self.date_button, "toggle")
        gs_widget_set_style_context(self.date_button, "combo")

        self.date_button.set_image(Gtk.Image.new_from_icon_name("x-office-calendar-symbolic", Gtk.IconSize.BUTTON))
        self.date_entry.show()
        self.date_button.show()

        self.cal_popup = Gtk.Popover()
        self.date_button.set_popover(self.cal_popup)

        self.calendar = Gtk.Calendar()
        self.calendar.set_display_options(
            Gtk.CalendarDisplayOptions.SHOW_DAY_NAMES | Gtk.CalendarDisplayOptions.SHOW_HEADING)
        self.calendar.connect("day-selected", self.day_selected)
        self.calendar.connect("day-selected-double-click", self.day_selected_double_click)
        self.cal_popup.add(self.calendar)
        self.calendar.show()
        self.date_entry.grab_focus()
        self.set_date_internal(datetime.date.today())

    def get_date(self) -> datetime.date:
        return self.get_date_internal().date()

    def set_date_internal(self, t: datetime.date):
        self.calendar.handler_block_by_func(self.day_selected)
        buf = print_datetime(t)
        self.date_entry.set_text(buf)
        self.calendar.select_month(t.month - 1, t.year)
        self.calendar.select_day(t.day)
        self.calendar.handler_unblock_by_func(self.day_selected)
        self.emit("date_changed", t)

    def date_accel_key_press(self, widget, event):
        try:
            self.set_date_internal(
                gs_handle_date_accelerator(event, self.get_date_internal().date(), widget.get_text()))
        except ValueError:
            return False
        self.emit("date_changed", 0)
        return True

    def key_press_entry(self, widget, event):
        if not self.date_accel_key_press(widget, event):
            return False
        widget.stop_emission_by_name("key-press-event")
        return True

    def date_focus_out_event(self, widget, event):
        t = self.get_date_internal()
        self.set_date_internal(t)
        self.emit("date_changed", t)
        return False

    def get_date_internal(self):
        tm = datetime.datetime.now()
        try:
            date = scan_date(self.date_entry.get_text())
            if date:
                tm = tm.replace(day=date[0], month=date[1], year=date[2])
        except ValueError:
            pass
        return tm

    def get_date_end(self):
        tm = self.get_date_internal()
        return tm.replace(hour=23, minute=59, second=59, microsecond=99)

    def do_activate_default(self, state):
        self.date_entry.set_activates_default(state)

    def do_grab_focus(self):
        return self.date_entry.grab_focus()

    def make_mnemonic_target(self, label):
        label.set_mnemonic_widget(self.date_entry)

    def set_date(self, d):
        self.set_date_internal(d)

    def key_press_popup(self, widget, event):
        if event.keyval not in (Gdk.KEY_Escape, Gdk.KEY_Return, Gdk.KEY_KP_Enter,
                                Gdk.KEY_ISO_Enter, Gdk.KEY_3270_Enter):
            return self.date_accel_key_press(self.date_entry, event)
        widget.stop_emission_by_name("key-press-event")
        self.cal_popup.popdown()
        return True

    def day_selected_double_click(self, calendar):
        self.cal_popup.popdown()

    def day_selected(self, calendar):
        self.in_selected_handler = True
        y, m, d = calendar.get_date()
        self.set_date_internal(datetime.datetime(day=d, month=m + 1, year=y))
        self.in_selected_handler = False


GObject.type_register(DateSelection)
