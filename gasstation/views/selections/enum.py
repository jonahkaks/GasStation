from gi import require_version

require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GLib
from gasstation.utilities.dialog import gs_widget_set_style_context

from enum import EnumMeta


class REF_COL(GObject.GEnum):
    NAME = 0
    PTR = 1
    NUM_ACCT_COLS = 2


class EnumSelection(Gtk.ComboBox):
    __gsignals__ = {
        'selection_changed': (GObject.SignalFlags.RUN_FIRST, None, (object,))
    }

    def __init__(self, enum_type, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not isinstance(enum_type, EnumMeta):
            raise TypeError("Value enum type must be of type enum instead of %s" % type(enum_type))
        self.init_done = False
        self.is_modal = False
        self.enum_type = enum_type
        self.init_done = False
        gs_widget_set_style_context(self, "linked")
        gs_widget_set_style_context(self, "ReferenceSel")
        self.store = Gtk.ListStore(str, object)
        self.set_model(self.store)
        renderer = Gtk.CellRendererText()
        self.pack_start(renderer, True)
        self.add_attribute(renderer, "text", REF_COL.NAME)
        self.connect("changed", self.combo_changed_cb)
        self.populate_list()
        self.init_done = True

    def combo_changed_cb(self, combo):
        _iter = combo.get_active_iter()
        if _iter is None:
            return False
        acc = self.store.get_value(_iter, REF_COL.PTR)
        self.emit("selection_changed", acc)

    def populate_list(self):
        active = -1
        self.handler_block_by_func(self.combo_changed_cb)
        _iter = self.get_active_iter()
        current_sel = ""
        if _iter is not None:
            current = self.store.get_value(_iter, REF_COL.PTR)
            if current is not None:
                current_sel = current.value
        self.store.clear()
        lis = map(self.enum_type.__getitem__, self.enum_type.__members__)
        for i, ref in enumerate(lis):
            name = ref.value
            self.store.append([name, ref])
            if GLib.utf8_collate(name, current_sel) == 0:
                active = i
        if active != -1:
            self.set_active(active)
        self.handler_unblock_by_func(self.combo_changed_cb)

    @classmethod
    def new(cls):
        return GObject.new(cls)

    def find_ref(self, model, path, _iter, acct):
        model_acc = model.get_value(_iter, REF_COL.PTR)
        if acct != model_acc:
            return False
        self.set_active_iter(_iter)
        return True

    def set_ref(self, acct, set_default=True):
        if set_default:
            self.set_active(0)
            if acct is None:
                return
        else:
            self.set_active(-1)
        self.store.foreach(self.find_ref, acct)

    def get_ref(self):
        _iter = self.get_active_iter()
        if _iter is None:
            return None
        acc = self.store.get_value(_iter, REF_COL.PTR)
        return acc

    def set_modal(self, state):
        self.is_modal = state

    def get_num_ref(self):
        return self.store.iter_n_children(None)

    def purge_ref(self, target, recursive):
        model = self.store
        more = True
        _iter = model.get_iter_first()
        if _iter is None:
            return
        if not recursive:
            while _iter is not None:
                acc = model.get_value(_iter, REF_COL.PTR)
                if acc == target:
                    self.store.remove(_iter)
                    break
                _iter = model.iter_next(_iter)
        else:
            while more:
                acc = model.get_value(_iter, REF_COL.PTR)
                while acc is not None:
                    if acc == target:
                        break
                    acc = acc.get_parent()
                if acc == target:
                    more = self.store.remove(_iter)
                else:
                    _iter = model.iter_next(_iter)
                    if _iter is None:
                        more = False
        self.set_active(0)
