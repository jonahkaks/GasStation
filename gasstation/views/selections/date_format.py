from gi.repository import GObject

from gasstation.utilities.dialog import *
from gasstation.utilities.ui import *


class DateFormatSelection(Gtk.Box):
    __gtype_name__ = "DateFormatSelection"
    __gsignals__ = {
        'format_changed': (GObject.SignalFlags.RUN_FIRST, None, ()), }

    def __init__(self):
        Gtk.Box.__init__(self)
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        gs_widget_set_style_context(self, "DateFormat")
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/date-format.ui",
                                          ["format-liststore", "date_format_window"])
        builder.connect_signals(self)
        self.label = builder.get_object("widget_label")
        self.format_combobox = builder.get_object("format_combobox")
        self.months_label = builder.get_object("months_label")
        self.months_number = builder.get_object("month_number_button")
        self.months_abbrev = builder.get_object("month_abbrev_button")
        self.months_name = builder.get_object("month_name_button")
        self.years_label = builder.get_object("years_label")
        self.years_button = builder.get_object("years_button")
        self.custom_label = builder.get_object("format_label")
        self.custom_entry = builder.get_object("format_entry")
        self.sample_label = builder.get_object("sample_label")
        self.set_format(DateFormat.UNSET)
        dialog = builder.get_object("date_format_window")
        table = builder.get_object("date_format_table")
        dialog.remove(table)
        self.add(table)
        dialog.destroy()

    @classmethod
    def new_without_label(cls):
        widget = cls.new_with_label(None)
        widget.label.destroy()
        widget.label = None
        return widget

    @classmethod
    def new_with_label(cls, label):
        gdf = cls()
        if label is not None:
            gdf.label.set_text(label)
        gdf.compute_format()
        return gdf

    def set_format(self, fmt):
        self.format_combobox.set_active(fmt)
        self.compute_format()

    def get_format(self):
        return self.format_combobox.get_active()

    def set_months(self, months):
        button = None
        if months == DateMonthFormat.NUMBER:
            button = self.months_number
        elif months == DateMonthFormat.ABBREV:
            button = self.months_abbrev
        elif months == DateMonthFormat.NAME:
            button = self.months_name
        if button is None:
            return
        button.set_active(True)
        self.compute_format()

    def get_months(self):
        if self.months_number.get_active():
            return DateMonthFormat.NUMBER
        if self.months_abbrev.get_active():
            return DateMonthFormat.ABBREV
        if self.months_name.get_active():
            return DateMonthFormat.NAME
        return DateMonthFormat.NUMBER

    def set_years(self, include_century):
        self.years_button.set_active(include_century)
        self.compute_format()

    def get_years(self):
        return self.years_button.get_active()

    def set_custom(self, fmt):
        self.custom_entry.set_text(fmt)
        self.compute_format()

    def get_custom(self):
        return self.custom_entry.get_text()

    def changed_cb(self, *args):
        self.compute_format()

    def enable_month(self, sensitive):
        self.months_label.set_sensitive(sensitive)
        self.months_number.set_sensitive(sensitive)
        self.months_abbrev.set_sensitive(sensitive)
        self.months_name.set_sensitive(sensitive)

    def enable_year(self, sensitive):
        self.years_label.set_sensitive(sensitive)
        self.years_button.set_sensitive(sensitive)

    def enable_format(self, sensitive):
        self.custom_label.set_sensitive(sensitive)
        self.custom_entry.set_sensitive(sensitive)

    def refresh(self):
        sel_option = DateFormat(self.format_combobox.get_active())

        if sel_option == DateFormat.CUSTOM:
            fmt = self.custom_entry.get_text()
            enable_year = enable_month = check_modifiers = False
            enable_custom = True

        elif sel_option == DateFormat.UNSET or sel_option == DateFormat.LOCALE or sel_option == DateFormat.UTC:
            fmt = date_format_get_string(sel_option)
            enable_year = enable_month = check_modifiers = enable_custom = False

        elif sel_option == DateFormat.ISO:
            self.months_number.set_active(True)
            enable_year = check_modifiers = True
            enable_month = enable_custom = False
        else:
            fmt = ""
            enable_year = enable_month = check_modifiers = True
            enable_custom = False
        self.enable_year(enable_year)
        self.enable_month(enable_month)
        self.enable_format(enable_custom)

        if check_modifiers:
            if self.months_number.get_active():
                fmt = date_format_get_string(sel_option)
            else:
                fmt = date_text_format_get_string(sel_option)
                if self.months_name.get_active():
                    fmt = fmt.replace("b", "B")

            if self.years_button.get_active():
                fmt = fmt.replace("y", "Y")
        self.custom_entry.handler_block_by_func(self.changed_cb)
        self.custom_entry.set_text(fmt)
        self.custom_entry.handler_unblock_by_func(self.changed_cb)
        secs_now = datetime.datetime.now()
        self.sample_label.set_text(print_datetime(secs_now))

    def compute_format(self):
        self.refresh()
        self.emit("format_changed")


GObject.type_register(DateFormatSelection)
