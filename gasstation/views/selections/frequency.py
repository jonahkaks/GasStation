from gasstation.views.selections.date_edit import *
from libgasstation.core.recurrence import *
from libgasstation.core.scheduled_transaction import ScheduledTransactionRecurrence

LAST_DAY_OF_MONTH_OPTION_INDEX = 31


class PageType(IntEnum):
    NONE = 0
    ONCE = 1
    DAILY = 2
    WEEKLY = 3
    SEMI_MONTHLY = 4
    MONTHLY = 5


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/frequency.ui")
class FrequencySelection(Gtk.Box):
    __gtype_name__ = "FrequencySelection"
    __gsignals__ = {
        'changed': (GObject.SignalFlags.RUN_FIRST, None, (int,))}
    notebook: Gtk.Notebook = Gtk.Template.Child()
    combo_box: Gtk.ComboBox = Gtk.Template.Child()
    date_grid: Gtk.Grid = Gtk.Template.Child()
    frequency_label: Gtk.Label = Gtk.Template.Child()
    start_date_label: Gtk.Label = Gtk.Template.Child()
    weekly_spin: Gtk.SpinButton = Gtk.Template.Child()
    daily_spin: Gtk.SpinButton = Gtk.Template.Child()
    monthly_spin: Gtk.SpinButton = Gtk.Template.Child()
    semi_monthly_spin: Gtk.SpinButton = Gtk.Template.Child()
    CHECKBOXES: List[Gtk.CheckButton] = [Gtk.Template.Child("wd_check_sun"),
                                         Gtk.Template.Child("wd_check_mon"),
                                         Gtk.Template.Child("wd_check_tue"),
                                         Gtk.Template.Child("wd_check_wed"),
                                         Gtk.Template.Child("wd_check_thu"),
                                         Gtk.Template.Child("wd_check_fri"),
                                         Gtk.Template.Child("wd_check_sat"), ]
    semi_monthly_first: Gtk.ComboBox = Gtk.Template.Child()
    semi_monthly_first_weekend: Gtk.ComboBox = Gtk.Template.Child()
    semi_monthly_second: Gtk.ComboBox = Gtk.Template.Child()
    semi_monthly_second_weekend: Gtk.ComboBox = Gtk.Template.Child()
    monthly_day: Gtk.ComboBox = Gtk.Template.Child()
    monthly_weekend: Gtk.ComboBox = Gtk.Template.Child()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_orientation(Gtk.Orientation.VERTICAL)
        gs_widget_set_style_context(self, "Frequency")
        self.start_date_selection: DateSelection = DateSelection()
        self.date_grid.attach(self.start_date_selection, 4, 0, 1, 1)
        self.start_date_selection.set_vexpand(False)
        self.start_date_selection.set_hexpand(False)
        self.start_date_selection.set_valign(Gtk.Align.CENTER)
        self.start_date_selection.set_halign(Gtk.Align.CENTER)
        self.start_date_selection.set_property("margin", 0)
        self.start_date_selection.connect("date_changed", self.start_date_changed)
        self.show()

    @Gtk.Template.Callback()
    def spin_changed_helper(self, adj):
        self.emit("changed", 0)

    @Gtk.Template.Callback()
    def weekly_days_changed(self, b):
        self.emit("changed", 0)

    @Gtk.Template.Callback()
    def monthly_selection_changed(self, b):
        self.emit("changed", 0)

    @Gtk.Template.Callback()
    def semi_monthly_selection_changed(self, b):
        self.emit("changed", 0)

    @Gtk.Template.Callback()
    def frequency_combo_changed(self, b):
        option_index = self.combo_box.get_active()
        self.notebook.set_current_page(option_index)
        self.emit("changed", 0)

    def start_date_changed(self, gde, *args):
        self.emit("changed", 0)

    def set_frequency_label_text(self, txt):
        if txt is None or len(txt) == 0:
            return
        self.frequency_label.set_text(txt)

    def set_date_label_text(self, txt):
        if txt is None or len(txt) == 0:
            return
        self.start_date_label.set_text(txt)

    def __new__(cls, recurrences: List[ScheduledTransactionRecurrence], start_date):
        self = super().__new__(cls)
        recurrences = list(map(lambda a: a.recurrence, recurrences))
        self.setup(recurrences, start_date)
        return self

    def _setup_weekly_recurrence(self, r: Recurrence):
        multiplier = r.get_multiplier()
        multiplier_spin = self.weekly_spin
        multiplier_spin.set_value(multiplier)
        recurrence_date = r.get_date()
        day_of_week = (recurrence_date.weekday() + 1) % 7
        weekday_checkbox = self.CHECKBOXES[day_of_week]
        weekday_checkbox.set_active(True)

    @staticmethod
    def _get_monthly_combobox_index(r: Recurrence):
        recurrence_date = r.get_date()
        day_of_month_index = recurrence_date.day - 1
        if r.get_period_type() == RecurrencePeriodType.END_OF_MONTH:
            day_of_month_index = LAST_DAY_OF_MONTH_OPTION_INDEX
        elif r.get_period_type() == RecurrencePeriodType.LAST_WEEKDAY:
            day_of_month_index = LAST_DAY_OF_MONTH_OPTION_INDEX + recurrence_date.weekday() + 1
        elif r.get_period_type() == RecurrencePeriodType.NTH_WEEKDAY:
            week = 3 if day_of_month_index / 7 > 3 else day_of_month_index / 7
            day_of_month_index = LAST_DAY_OF_MONTH_OPTION_INDEX + 7 + recurrence_date.weekday() + 1 + (7 * week)
        return day_of_month_index

    def setup(self, recurrences: List[Recurrence], start_date: datetime.date):
        made_changes = False
        if start_date is not None:
            self.start_date_selection.set_date(start_date)
            made_changes = True
        if recurrences is None or not any(recurrences):
            if made_changes:
                self.emit("changed", 0)
            return

        if len(recurrences) > 1:
            if Recurrence.list_is_weekly_multiple(recurrences):
                for r in recurrences:
                    self._setup_weekly_recurrence(r)
                self.notebook.set_current_page(PageType.WEEKLY)
                self.combo_box.set_active(PageType.WEEKLY)
            elif Recurrence.list_is_semi_monthly(recurrences):
                first = recurrences[0]
                second = recurrences[1]
                multiplier_spin = self.semi_monthly_spin
                multiplier_spin.set_value(first.get_multiplier())
                self.semi_monthly_first.set_active(self._get_monthly_combobox_index(first))
                self.semi_monthly_first_weekend.set_active(int(first.get_weekend_adjustment()))
                self.semi_monthly_second.set_active(self._get_monthly_combobox_index(second))
                self.semi_monthly_second_weekend.set_active(int(second.get_weekend_adjustment()))
                self.notebook.set_current_page(PageType.SEMI_MONTHLY)
                self.combo_box.set_active(PageType.SEMI_MONTHLY)
        else:
            r = recurrences[0]
            ptype = r.get_period_type()
            if ptype == RecurrencePeriodType.ONCE:
                self.notebook.set_current_page(PageType.ONCE)
                self.combo_box.set_active(PageType.ONCE)
            elif ptype == RecurrencePeriodType.DAY:
                multiplier = r.get_multiplier()
                self.daily_spin.set_value(multiplier)
                made_changes = True
                self.notebook.set_current_page(PageType.DAILY)
                self.combo_box.set_active(PageType.DAILY)
            elif ptype == RecurrencePeriodType.WEEK:
                self._setup_weekly_recurrence(r)
                self.notebook.set_current_page(PageType.WEEKLY)
                self.combo_box.set_active(PageType.WEEKLY)
            elif ptype == RecurrencePeriodType.END_OF_MONTH or ptype == RecurrencePeriodType.MONTH \
                    or ptype == RecurrencePeriodType.YEAR or ptype == RecurrencePeriodType.LAST_WEEKDAY or ptype == \
                    RecurrencePeriodType.NTH_WEEKDAY:
                multiplier = r.get_multiplier()
                if r.get_period_type() == RecurrencePeriodType.YEAR:
                    multiplier *= 12
                self.monthly_spin.set_value(multiplier)
                self.monthly_day.set_active(self._get_monthly_combobox_index(r))
                self.monthly_weekend.set_active(int(r.get_weekend_adjustment()))
                self.notebook.set_current_page(PageType.MONTHLY)
                self.combo_box.set_active(PageType.MONTHLY)
        if made_changes:
            self.emit("changed", 0)

    @staticmethod
    def _get_multiplier_from_widget(multiplier_spin):
        return multiplier_spin.get_value_as_int()

    @staticmethod
    def _get_day_of_month_recurrence(start_date: datetime.date, multiplier: int, day_of_month_combo: Gtk.ComboBox,
                                     weekend_adjust_combo: Gtk.ComboBox):

        day_of_month_index = day_of_month_combo.get_active()
        if day_of_month_index == -1:
            day_of_month_index = 0
        weekend_adjust = RecurrenceWeekendAdjust(weekend_adjust_combo.get_active())
        r = Recurrence()
        if day_of_month_index > LAST_DAY_OF_MONTH_OPTION_INDEX + 7:
            selected_index = day_of_month_index - LAST_DAY_OF_MONTH_OPTION_INDEX - 7
            day_of_week_date = start_date
            selected_week = 3 if (selected_index - 1) / 7 == 4 else (selected_index - 1) / 7
            selected_day_of_week = selected_index - 7 * selected_week
            day_of_week_date = day_of_week_date.replace(day=1)
            while ((day_of_week_date.weekday() + 1) % 7) != selected_day_of_week:
                day_of_week_date += relativedelta(days=1)
            day_of_week_date += relativedelta(days=7 * selected_week)
            r.set(multiplier, RecurrencePeriodType.NTH_WEEKDAY, day_of_week_date, RecurrenceWeekendAdjust.NONE)
        elif day_of_month_index > LAST_DAY_OF_MONTH_OPTION_INDEX:
            day_of_week_date = start_date
            selected_day_of_week = day_of_month_index - LAST_DAY_OF_MONTH_OPTION_INDEX
            day_of_week_date = day_of_week_date.replace(day=1)
            while ((day_of_week_date.weekday() + 1) % 7) != selected_day_of_week:
                day_of_week_date += relativedelta(days=1)
            r.set(multiplier, RecurrencePeriodType.LAST_WEEKDAY, day_of_week_date, weekend_adjust)
        elif day_of_month_index == LAST_DAY_OF_MONTH_OPTION_INDEX:
            day_of_month = start_date
            r.set(multiplier, RecurrencePeriodType.END_OF_MONTH, day_of_month, weekend_adjust)
        else:
            day_of_month = start_date
            allowable_date = min(day_of_month_index + 1, calendar.monthrange(day_of_month.year, day_of_month.month)[1])
            day_of_month = day_of_month.replace(day=allowable_date)
            r.set(multiplier, RecurrencePeriodType.MONTH, day_of_month, weekend_adjust)
        return r

    def save_to_recurrence(self, recurrences):
        start_date = self.start_date_selection.get_date()
        out_start_date = start_date
        if recurrences is None or len(recurrences):
            return out_start_date
        page_index = PageType(self.notebook.get_current_page())
        if page_index == PageType.NONE:
            return out_start_date
        if page_index == PageType.ONCE:
            r = Recurrence()
            r.set(1, RecurrencePeriodType.ONCE, start_date, RecurrenceWeekendAdjust.NONE)
            recurrences.append(r)
        elif page_index == PageType.DAILY:
            multiplier = self._get_multiplier_from_widget(self.daily_spin)
            r = Recurrence()
            r.set(multiplier, RecurrencePeriodType.DAY, start_date, RecurrenceWeekendAdjust.NONE)
            recurrences.append(r)
        elif page_index == PageType.WEEKLY:
            multiplier = self._get_multiplier_from_widget(self.weekly_spin)
            for checkbox_idx, weekday_checkbox in enumerate(self.CHECKBOXES):
                if not weekday_checkbox.get_active():
                    continue
                day_of_week_aligned_date = start_date
                while ((day_of_week_aligned_date.weekday() + 1) % 7) != checkbox_idx:
                    day_of_week_aligned_date += relativedelta(days=1)
                r = Recurrence()
                r.set(multiplier, RecurrencePeriodType.WEEK, day_of_week_aligned_date, RecurrenceWeekendAdjust.NONE)
                recurrences.append(r)
        elif page_index == PageType.SEMI_MONTHLY:
            multiplier = self._get_multiplier_from_widget(self.semi_monthly_spin)
            recurrences.append(self._get_day_of_month_recurrence(start_date, multiplier, self.semi_monthly_first,
                                                                 self.semi_monthly_first_weekend))
            recurrences.append(self._get_day_of_month_recurrence(start_date, multiplier, self.semi_monthly_second,
                                                                 self.semi_monthly_second_weekend))
        elif page_index == PageType.MONTHLY:
            multiplier = self._get_multiplier_from_widget(self.monthly_spin)
            r = self._get_day_of_month_recurrence(start_date, multiplier, self.monthly_day, self.monthly_weekend)
            recurrences.append(r)
        return out_start_date


GObject.type_register(FrequencySelection)
