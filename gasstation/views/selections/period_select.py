from gasstation.views.selections.date_edit import *

start_strings = [
    "Today",
    "Start of this month",
    "Start of previous month",
    "Start of this quarter",
    "Start of previous quarter",
    "Start of this year",
    "Start of previous year",
    "Start of this accounting period",
    "Start of previous accounting period"]

end_strings = [
    "Today",
    "End of this month",
    "End of previous month",
    "End of this quarter",
    "End of previous quarter",
    "End of this year",
    "End of previous year",
    "End of this accounting period",
    "End of previous accounting period"
]


class PeriodSelect(Gtk.Box):
    selector = None
    start = False
    date_label = None
    __gtype_name__ = "PeriodSelect"
    __gsignals__ = {
        'changed': (GObject.SignalFlags.RUN_FIRST, None, (int,))
    }
    __gproperties__ = {"fy-end": (GLib.Date,
                                  "Fiscal Year End",
                                  "The fiscal year to use for this widget",
                                  GObject.ParamFlags.READWRITE),
                       "show-date": (bool, "Show Date",
                                     "Show the start/end date of the accounting period in this widget",
                                     False,
                                     GObject.ParamFlags.READWRITE),
                       "date-base": (GLib.Date, "Date Base",
                                     "The starting date to use for display calculations",
                                     GObject.ParamFlags.READWRITE),
                       "active": (int,
                                  "Active period",
                                  "The currently selected period in the list of periods",
                                  -1,
                                  GLib.MAXINT,
                                  0,
                                  GObject.ParamFlags.READWRITE), }

    def __init__(self, starting_labels, **kwargs):
        super().__init__(**kwargs)
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        gs_widget_set_style_context(self, "PeriodSelect")
        self.start = True
        self.selector = Gtk.ComboBoxText.new()
        self.start = starting_labels
        self.pack_start(self.selector, True, True, 0)
        self.selector.show()
        self.selector.connect("changed", self.sample_combobox_changed)
        for i in range(AccountingPeriodType.CYEAR_LAST):
            label = start_strings[i] if starting_labels else end_strings[i]
            self.selector.append_text(label)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_DATE_FORMAT, self.sample_new_date_format)

    def changed(self):
        self.emit("changed", 0)

    def sample_update_date_label(self):
        if not self.date_label:
            return
        which = self.get_active()
        if which == -1:
            date = GLib.Date.new_dmy(31, 7, 2013)

        elif self.start:
            date = AccountingPeriod.start_gdate(which, self.fy_end, self.date_base)
        else:
            date = AccountingPeriod.end_gdate(which, self.fy_end, self.date_base)
        self.date_label.set_label(datetime.date(date.get_year(), date.get_month(), date.get_day()).strftime("%d/%m%Y"))

    def sample_combobox_changed(self, box):
        self.set_property("active", box.get_active())

    def sample_new_date_format(self, prefs, pref):
        self.sample_update_date_label()

    def set_active_internal(self, which):
        if which <= 0 or which > AccountingPeriodType.LAST:
            return
        self.selector.handler_block_by_func(self.sample_combobox_changed)
        self.selector.set_active(which)
        self.selector.handler_unblock_by_func(self.sample_combobox_changed)
        self.sample_update_date_label()
        self.changed()

    def get_fy_end(self):
        if not self.fy_end:
            return None
        return GLib.Date.new_dmy(self.fy_end.get_day(), self.fy_end.get_month(), GLib.DATE_BAD_YEAR)

    def set_fy_end(self, fy_end):
        if self.fy_end:
            self.fy_end = None

        if fy_end:
            self.fy_end = GLib.Date.new_dmy(fy_end.get_day(), fy_end.get_month(), GLib.DATE_BAD_YEAR)
        else:
            self.fy_end = None
        if fy_end:
            for i in range(AccountingPeriodType.CYEAR_LAST, AccountingPeriodType.FYEAR_LAST, 1):
                label = start_strings[i] if self.start else end_strings[i]
                self.selector.append_text(label)
        else:
            for i in range(AccountingPeriodType.FYEAR_LAST, AccountingPeriodType.FYEAR_LAST - 1, -1):
                self.selector.remove(i)

    def set_date_common(self, date):
        if date:
            if self.date_base: self.date_base = None
            self.date_base = GLib.Date.new_dmy(date.get_day(), date.get_month(), date.get_year())
            if self.date_label is None:
                self.date_label = Gtk.Label("")
                self.date_label.set_margin_start(6)
                self.pack_start(self.date_label, True, True, 0)
                self.date_label.show_all()
            self.sample_update_date_label()
            return
        if self.date_base:
            self.date_base = None
        self.date_label.destroy()
        self.date_label = None

    def get_show_date(self):
        return self.date_base is not None

    def set_show_date(self, show_date):
        date = GLib.Date()
        if show_date:
            date.clear()
            gdate_set_today(date)
            self.set_date_common(date)

        else:
            self.set_date_common(None)

    def get_date_base(self):
        if not self.date_base: return None
        return GLib.Date.new_dmy(self.date_base.get_day(), self.date_base.get_month(), self.date_base.get_year())

    def set_date_base(self, date_base):
        self.set_date_common(date_base)

    def do_get_property(self, prop):
        n = prop.name
        if n == 'fy-end':
            return self.get_fy_end()
        elif n == "show-date":
            return self.get_show_date()
        elif n == "date-base":
            return self.get_date_base()
        elif n == "active":
            return self.get_active()
        else:
            pass

    def do_set_property(self, prop, value):
        n = prop.name
        if n == 'fy-end':
            self.set_fy_end(value)
        elif n == "show-date":
            self.set_show_date(value)
        elif n == "date-base":
            self.set_date_base(value)
        elif n == "active":
            self.set_active_internal(value)

    @classmethod
    def new_glade(cls, widget_name, string1, string2, int1, int2):
        widget = cls(int1 != 0)
        if int2:
            widget.set_show_date(True)
        widget.show()
        return widget

    def set_active(self, which):
        self.set_property("active", which)

    def get_active(self):
        return AccountingPeriodType(self.selector.get_active())

    def get_date(self):
        which = self.selector.get_active()
        if which == -1:
            return None
        if self.start:
            return AccountingPeriod.start_date(which, self.fy_end, self.date_base)
        return AccountingPeriod.end_date(which, self.fy_end, self.date_base)
