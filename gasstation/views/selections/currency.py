from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import GObject
from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.utilities.gtk import *
from libgasstation.core.session import *
from libgasstation.core.commodity import COMMODITY_NAMESPACE_NAME_CURRENCY


class CurrencySelection(Gtk.ComboBox):
    __gtype_name__ = "CurrencySelection"
    mnemonic = GObject.Property(type=GObject.TYPE_STRING, flags=GObject.ParamFlags.READWRITE)
    store = Gtk.ListStore(str)
    is_realized = False

    def __init__(self):
        super().__init__(has_entry=True)
        store = self.__class__.store
        self.set_model(store)
        gs_widget_set_style_context(self, "CurrencySelection")
        self.connect("notify::mnemonic", self.mnemonic_changed)
        self.set_entry_text_column(0)
        Combo.require_list_item(self)
        self.fill_currencies()
        store.set_sort_column_id(0, Gtk.SortType.ASCENDING)
        self.__class__.is_realized = True

    def do_get_property(self, prop):
        if prop.name == 'mnemonic':
            return self.mnemonic
        else:
            raise AttributeError('unknown property %s' % prop.name)

    def do_set_property(self, prop, value):
        if prop.name == 'mnemonic':
            self.mnemonic = value
        else:
            raise AttributeError('unknown property %s' % prop.name)

    def mnemonic_changed(self, *_):
        currency = Session.get_current_commodity_table().lookup(COMMODITY_NAMESPACE_NAME_CURRENCY, self.mnemonic)
        if currency is None:
            currency = Session.locale_default_currency()
        self.handler_block_by_func(self.mnemonic_changed)
        self.set_currency(currency)
        self.handler_unblock_by_func(self.mnemonic_changed)

    def fill_currencies(self):
        if not self.__class__.is_realized:
            for commodity in Session.get_current_commodity_table().get_commodities(COMMODITY_NAMESPACE_NAME_CURRENCY):
                self.__class__.store.append([commodity.get_printname()])

    def set_currency(self, currency):
        if currency is None:
            return
        printname = currency.get_printname()
        Combo.set_by_string(self, printname)

    def get_currency(self):
        _iter = self.get_active_iter()
        if _iter is not None:
            model = self.get_model()
            mnemonic = model.get_value(_iter, 0)
            return Session.get_current_commodity_table().lookup(COMMODITY_NAMESPACE_NAME_CURRENCY,
                                                                mnemonic.split(" ")[0])
        else:
            return Session.locale_default_currency()


GObject.type_register(CurrencySelection)
