from decimal import Decimal

from gi.repository import Gtk, GObject, Gdk

from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.utilities.ui import PrintAmountInfo, sprintamount
from libgasstation.core.helpers import parse_exp_decimal
from libgasstation.core.locales import Locale


class AmountEntry(Gtk.Entry):
    __gsignals__ = {
        "amount_changed": (GObject.SignalFlags.RUN_FIRST, None, (object,))
    }

    def __init__(self):
        super().__init__()
        self.need_to_parse = False
        self.print_info = PrintAmountInfo.default(False)
        self.evaluate_on_enter = False
        gs_widget_set_style_context(self, "AmountEntry")
        # self.connect("changed", self.changed)
        self.connect("focus-out-event", self.focus_out)
        # self.connect("key-press-event", self.key_press_event)
        self.set_amount(Decimal(0))

    def changed(self, editable):
        self.need_to_parse = True

    def focus_out(self, widget, event):
        self.need_to_parse = True
        self.evaluate()
        self.need_to_parse = False
        return True

    def key_press_event(self, widget, event):
        if event.keyval == Gdk.KEY_KP_Decimal:
            if self.print_info.monetary:
                lc = Locale.conv()
                # event.keyval = lc["mon_decimal_point"][0]
                event.string[0] = lc["mon_decimal_point"]
        result = False

        if event.keyval == Gdk.KEY_Return:
            if not self.evaluate_on_enter:
                if not (event.state & (Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.MOD1_MASK | Gdk.ModifierType.SHIFT_MASK)):
                    return result
        elif event.keyval == Gdk.KEY_KP_Enter:
            pass
        else:
            return result

        self.evaluate()
        return True

    def expr_is_valid(self, empty_ok):
        string = self.get_text()
        amount = Decimal(0)
        if string.strip() == "" or len(string) == 0:
            if empty_ok:
                return amount
            else:
                raise ValueError("Value is zero")
        string = string.replace(",", "")
        try:
            amount = Decimal(string)
        except ValueError as e:
            amount = parse_exp_decimal(string)
        return amount

    def evaluate(self):
        if not self.need_to_parse:
            return True
        try:
            amount = self.expr_is_valid(False)
            print("CONVERTED", amount)
            old_amount = amount
            self.set_amount(amount)
            if amount != old_amount:
                self.need_to_parse = False
                self.emit("amount_changed", 0)
                return True
            return False
        except ValueError:
            return True

    def get_amount(self):
        self.evaluate()
        return self.amount

    def get_damount(self):
        self.evaluate()
        return self.amount

    def set_amount(self, amount:Decimal):
        self.amount = amount
        if amount.is_zero():
            self.set_text("")
            return
        amount_string = sprintamount(amount, self.print_info)
        self.set_text(amount_string)
        self.need_to_parse = False

    def set_print_info(self, print_info):
        self.print_info = print_info
        self.print_info.use_symbol = 0

    def set_evaluate_on_enter(self, evaluate_on_enter):
        self.evaluate_on_enter = evaluate_on_enter
