import calendar
import datetime
import gettext
import math
import time
from enum import IntEnum

import cairo
from dateutil.relativedelta import relativedelta
from gi import require_version

require_version("Gtk", "3.0")
require_version('Gdk', '3.0')
from gasstation.utilities.dialog import gs_widget_set_style_context
from gi.repository import Gtk, Gdk, Pango, GLib

DENSE_CAL_DEFAULT_WIDTH = 15
DENSE_CAL_DEFAULT_HEIGHT = 105
MINOR_BORDER_SIZE = 1
COL_BORDER_SIZE = 3
MONTH_NAME_BUFSIZE = 10


def gs_is_dark_theme(fg_color):
    is_dark = False
    lightness = round(0.299 * fg_color.red + 0.587 * fg_color.green + 0.114 * fg_color.blue, 2)
    if lightness > 0.5:
        is_dark = True
    return is_dark


def month_name(mon):
    arbitrary_year = 1977
    return datetime.date(day=1, year=arbitrary_year, month=mon + 1).strftime("%b")


def day_label(dow):
    return time.strftime("%a", time.struct_time((0, 0, 0, 0, 0, 0, dow + 6, 0, -1)))[:2]


class ViewOptsColumns(IntEnum):
    LABEL = 0,
    NUM_MONTHS = 1


class MonthCoords:
    def __init__(self):
        self.x = 0
        self.y = 0


class MarkData:
    def __init__(self):
        self.name = None
        self.info = None
        self.tag = None
        self.ourMarks = None

    def __repr__(self):
        return u"MarkData(name={}, info={}, tag={})".format(self.name, self.info, self.tag)


def get_monday_weeks_in_year(year: int) -> int:
    d = datetime.date(year, 1, 1)
    if d.weekday() + 1 == 1:
        return 53
    d = d.replace(month=12, day=31)
    if d.weekday() + 1 == 1:
        return 53
    if calendar.isleap(year):
        d = d.replace(month=2, day=1)
        if d.weekday() + 1 == 1:
            return 53
        d = d.replace(month=12, day=30)
        if d.weekday() + 1 == 1:
            return 53

    return 52


def get_sunday_weeks_in_year(year: int) -> int:
    d = datetime.date(year, 1, 1)
    if d.weekday() + 1 == 7:
        return 53
    d = d.replace(month=12, day=31)
    if d.weekday() + 1 == 7:
        return 53
    if calendar.isleap(year):
        d = d.replace(month=2, day=1)
        if d.weekday() + 1 == 7:
            return 53
        d = d.replace(month=12, day=30)
        if d.weekday() + 1 == 7:
            return 53

    return 52


def get_sunday_week_of_year(d):
    return int(d.strftime("%U"))


def get_monday_week_of_year(d):
    return int(d.strftime("%W"))


class DenseCalendar(Gtk.Box):
    view_options = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.surface = None
        self.model = None
        self.monthPositions = []
        self.week_starts_monday = 0
        for i in range(12):
            self.monthPositions.append(MonthCoords())
        context = self.get_style_context()
        self.set_orientation(Gtk.Orientation.VERTICAL)
        gs_widget_set_style_context(self, "calendar")
        self.set_name("dense-cal")
        context.add_class(Gtk.STYLE_CLASS_CALENDAR)
        options = self.get_view_options()
        self.view_options = Gtk.ComboBox.new_with_model(options)
        self.view_options.set_active(0)
        text_rend = Gtk.CellRendererText()
        self.view_options.pack_start(text_rend, True)
        self.view_options.add_attribute(text_rend, "text", ViewOptsColumns.LABEL)
        self.view_options.connect("changed", self.view_option_changed)
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        label = Gtk.Label(label="View")
        hbox.set_homogeneous(False)
        label.set_halign(Gtk.Align.END)
        label.set_margin_end(5)
        hbox.pack_start(label, True, True, 0)
        hbox.pack_start(self.view_options, False, False, 0)

        self.pack_start(hbox, False, False, 0)
        self.cal_drawing_area = Gtk.DrawingArea()

        self.cal_drawing_area.add_events(Gdk.EventMask.EXPOSURE_MASK
                                         | Gdk.EventMask.BUTTON_PRESS_MASK
                                         | Gdk.EventMask.BUTTON_RELEASE_MASK
                                         | Gdk.EventMask.POINTER_MOTION_MASK
                                         | Gdk.EventMask.POINTER_MOTION_HINT_MASK)
        self.pack_start(self.cal_drawing_area, True, True, 0)
        self.cal_drawing_area.connect("draw", self.draw)
        self.cal_drawing_area.connect("realize", self.realize)
        self.cal_drawing_area.connect("configure_event", self.configure)

        self.disposed = False
        self.initialized = False
        self.markData = []
        self.numMarks = 0
        self.num_weeks = 0
        self.marks = None
        self.lastMarkTag = 0
        self.screen_width = 0
        self.screen_height = 0
        self.firstOfMonthOffset = 0
        self.doc = 0
        self.showPopup = False
        self.transPopup = Gtk.Window(type=Gtk.WindowType.POPUP)
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
        vbox.set_homogeneous(False)
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        hbox.set_homogeneous(False)
        self.transPopup.set_name("dense-cal-popup")
        l = Gtk.Label(label="Date: ")
        l.set_margin_start(5)
        hbox.add(l)
        self.date_label = Gtk.Label(label="YY/MM/DD")
        hbox.add(self.date_label)
        vbox.add(hbox)

        vbox.add(Gtk.Separator.new(orientation=Gtk.Orientation.HORIZONTAL))
        tree_data = Gtk.ListStore(str, str)
        tree_view = Gtk.TreeView.new_with_model(tree_data)
        tree_view.insert_column_with_attributes(title="Name", position=-1, cell=Gtk.CellRendererText(), text=0)
        tree_view.insert_column_with_attributes(title="Frequency", position=-1, cell=Gtk.CellRendererText(), text=1)
        tree_view.get_selection().set_mode(Gtk.SelectionMode.NONE)
        self.popup_model = tree_data
        vbox.add(tree_view)
        self.transPopup.add(vbox)
        self.transPopup.set_resizable(False)
        self.transPopup.realize()

        layout = self.create_pango_layout()
        state_flags = context.get_state()
        font_size_reduction_units = 1
        font_desc = context.get_font(state_flags)
        font_size = font_desc.get_size()

        provider = Gtk.CssProvider.new()
        dpi = Gdk.Screen.get_default().get_resolution()
        px_size = ((font_size / Pango.SCALE) - font_size_reduction_units) * (dpi / 72.)
        widget_css = b"*{font-size:%dpx;}" % px_size
        provider.load_from_data(widget_css)
        context.add_provider(provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        max_width = max_height = 0
        for i in range(12):
            layout.set_text(month_name(i), -1)
            w, h = layout.get_pixel_size()
            max_width = max(max_width, w)
            max_height = max(max_height, h)
        self.label_width = max_width
        self.label_height = max_height
        self.month = 0
        self.year = 1970
        self.numMonths = 12
        self.monthsPerCol = 3
        self.leftPadding = 2
        self.topPadding = 2

        now = datetime.date.today()
        self._set_month(now.month, False)
        self._set_year(now.year, False)
        self.recompute_extents()
        self.recompute_mark_storage()

        layout = self.create_pango_layout(None)

        layout.set_text("88", -1)
        width_88, height_88 = layout.get_pixel_size()
        layout.set_text("XXX", -1)
        width_XXX, height_XXX = layout.get_pixel_size()

        self.min_x_scale = self.x_scale = width_88 + 2
        self.min_y_scale = self.y_scale = max(math.floor(float(width_XXX / 3.)), height_88 + 2)
        self.dayLabelHeight = height_88
        self.initialized = True

        week_start_str = gettext.dgettext("gtk20", "calendar:week_start:0")
        parts = week_start_str.split(":", 3)
        if len(parts) == 3:
            if parts[2] == "1":
                self.week_starts_monday = 1
        self.show_all()

    @classmethod
    def get_view_options(cls):
        if cls.view_options is None:
            view_options = Gtk.ListStore(str, int)
            view_options.append(["12 months", 12])
            view_options.append(["6 months", 6])
            view_options.append(["4 months", 4])
            view_options.append(["3 months", 3])
            view_options.append(["2 months", 2])
            view_options.append(["1 month", 1])
            cls.view_options = view_options
        return cls.view_options

    def set_cal_min_size_req(self):
        min_width, min_height = self.compute_min_size()
        self.cal_drawing_area.set_size_request(min_width, min_height)

    @classmethod
    def new_with_model(cls, model):
        self = cls()
        self.set_model(model)
        return self

    def recompute_first_of_month_offset(self):
        self.firstOfMonthOffset = (datetime.date(day=1, month=self.month, year=self.year).weekday() + 1) % 7

    def set_month(self, mon):
        self._set_month(mon, True)

    def _set_month(self, mon, redraw):
        if self.month == mon:
            return
        self.month = mon
        self.recompute_first_of_month_offset()
        self.recompute_extents()
        if redraw and self.cal_drawing_area.get_realized():
            self.recompute_x_y_scales()
            self.draw_to_buffer()
            self.cal_drawing_area.queue_draw()

    def set_year(self, year):
        self._set_year(year, True)

    def _set_year(self, year, redraw):
        if self.year == year:
            return
        self.year = year
        self.recompute_first_of_month_offset()
        self.recompute_extents()
        if redraw and self.cal_drawing_area.get_realized():
            self.recompute_x_y_scales()
            self.draw_to_buffer()
            self.cal_drawing_area.queue_draw()

    def set_num_months(self, num_months):
        options = self.get_view_options()
        closest_index_distance = GLib.MAXINT
        view_opts_iter = options.get_iter_first()
        iter_closest_to_req = None
        if view_opts_iter is None:
            return
        months_val = options.get_value(view_opts_iter, ViewOptsColumns.NUM_MONTHS)
        delta_months = abs(months_val - int(num_months))
        if delta_months < closest_index_distance:
            iter_closest_to_req = view_opts_iter
            closest_index_distance = delta_months

        while closest_index_distance != 0:
            view_opts_iter = options.iter_next(view_opts_iter)
            if view_opts_iter is None:
                break
            months_val = options.get_value(view_opts_iter, ViewOptsColumns.NUM_MONTHS)
            delta_months = abs(months_val - int(num_months))
            if delta_months < closest_index_distance:
                iter_closest_to_req = view_opts_iter
                closest_index_distance = delta_months

        self.view_options.handler_block_by_func(self.view_option_changed)
        self.view_options.set_active_iter(iter_closest_to_req)
        self.view_options.handler_unblock_by_func(self.view_option_changed)
        self.numMonths = num_months
        self.recompute_extents()

        self.recompute_mark_storage()
        if self.cal_drawing_area.get_realized():
            self.recompute_x_y_scales()
            self.draw_to_buffer()
            self.cal_drawing_area.queue_draw()

    def get_num_months(self):
        return self.numMonths

    def set_months_per_col(self, monthsPerCol):
        self.monthsPerCol = monthsPerCol
        self.recompute_x_y_scales()

    def get_month(self):
        return self.month

    def get_year(self):
        return self.year

    def configure(self, widget, event):
        self.recompute_x_y_scales()
        self.reconfig()
        return True

    def realize(self, _):
        self.recompute_x_y_scales()
        self.reconfig()

    def reconfig(self):
        if self.surface is not None:
            self.surface.finish()
            self.surface = None
        alloc = self.cal_drawing_area.get_allocation()
        self.surface = cairo.ImageSurface(cairo.Format.ARGB32,
                                          alloc.width,
                                          alloc.height)
        self.draw_to_buffer()

    def compute_min_size(self):
        min_width = (self.leftPadding * 2) + (
                self.num_cols() * (self.col_width_at(self.min_x_scale) + self.label_width)) + (
                            (self.num_cols() - 1) * COL_BORDER_SIZE)
        min_height = (self.topPadding * 2) + MINOR_BORDER_SIZE + self.dayLabelHeight + (
                self.num_weeks_per_col() * self.week_height_at(self.min_y_scale))
        return min_width, min_height

    def recompute_x_y_scales(self):
        width = DENSE_CAL_DEFAULT_WIDTH
        height = DENSE_CAL_DEFAULT_HEIGHT
        if self.initialized:
            alloc = self.cal_drawing_area.get_allocation()
            width = alloc.width
            height = alloc.height
        denom = 7 * self.num_cols()
        self.x_scale = (int(width - (self.leftPadding * 2) - (self.num_cols() *
                                                              ((8 * MINOR_BORDER_SIZE) + self.label_width)) -
                            ((self.num_cols() - 1) * COL_BORDER_SIZE)) / denom)
        self.x_scale = max(self.x_scale, self.min_x_scale)
        denom = self.num_weeks_per_col()
        self.y_scale = (int(height - (self.topPadding * 2)
                            - MINOR_BORDER_SIZE
                            - self.dayLabelHeight
                            - (self.num_weeks_per_col() - 1
                               * MINOR_BORDER_SIZE)) / denom)
        self.y_scale = max(self.y_scale, self.min_y_scale)
        self.set_cal_min_size_req()

    def recompute_mark_storage(self):
        self.numMarks = self.num_weeks * 7
        self.marks = []
        self.markData = []
        for _ in range(self.numMarks):
            self.marks.append([])
        if self.model is not None:
            self.add_markings()

    def recompute_extents(self):
        date = datetime.date(day=1, month=self.month, year=self.year)
        start_week = get_monday_week_of_year(date) if self.week_starts_monday else get_sunday_week_of_year(date)
        date += relativedelta(months=self.numMonths)
        end_week = get_monday_week_of_year(date) if self.week_starts_monday else get_sunday_week_of_year(date)
        if date.year != self.year:
            end_week += get_monday_weeks_in_year(self.year) if self.week_starts_monday else \
                get_sunday_weeks_in_year(self.year)
        self.num_weeks = end_week - start_week + 1

    def draw(self, _, cr):
        cr.save()
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()
        cr.restore()
        return True

    def draw_to_buffer(self):
        if self.surface is None:
            return
        cr = cairo.Context(self.surface)
        layout = self.create_pango_layout(None)
        stylectxt = self.cal_drawing_area.get_style_context()
        state_flags = stylectxt.get_state()
        stylectxt.add_class(Gtk.STYLE_CLASS_BACKGROUND)
        stylectxt.add_class(Gtk.STYLE_CLASS_CALENDAR)
        Gtk.render_background(stylectxt, cr, 0, 0, self.surface.get_width(), self.surface.get_height())
        stylectxt.remove_class(Gtk.STYLE_CLASS_BACKGROUND)
        class_extension = ""
        color = stylectxt.get_color(Gtk.StateFlags.NORMAL)

        if gs_is_dark_theme(color):
            class_extension = "-dark"

        primary_color_class = "primary" + class_extension
        secondary_color_class = "secondary" + class_extension
        marker_color_class = "markers" + class_extension

        layout.set_text("S", -1)
        self.dayLabelHeight = layout.get_pixel_size()[1]

        for i in range(12):
            self.monthPositions[i].x = self.monthPositions[i].y = -1
        stylectxt.save()

        for i in range(self.numMonths):
            mcList = []
            self.month_coords(i, mcList)
            self.monthPositions[i].x = math.floor(i / self.monthsPerCol) * (self.col_width() + COL_BORDER_SIZE)
            self.monthPositions[i].y = mcList[3].y
            for rect in mcList:
                stylectxt.save()
                if i % 2 == 0:
                    stylectxt.add_class(primary_color_class)
                else:
                    stylectxt.add_class(secondary_color_class)
                Gtk.render_background(stylectxt, cr, rect.x, rect.y, rect.width, rect.height)
                stylectxt.restore()
        stylectxt.restore()

        stylectxt.save()
        stylectxt.add_class(marker_color_class)
        stylectxt.add_class(Gtk.STYLE_CLASS_VIEW)
        stylectxt.set_state(Gtk.StateFlags.SELECTED)

        for i in range(self.numMarks):
            if len(self.marks[i]):
                x1, y1, x2, y2 = self.doc_coords(i)
                center_x = (x1 + x2) / 2
                center_y = (y1 + y2) / 2
                radius = min((x2 - x1), (y2 - y1)) * .75
                if ((y2 - y1) % 2) != 0:
                    center_y = center_y + 1
                if ((x2 - x1) % 2) != 0:
                    center_x = center_x + 1
                Gtk.render_background(stylectxt, cr, center_x - radius - 2, center_y - radius - 1,
                                      (radius * 2) + 4, radius * 2)

        stylectxt.restore()

        for i in range(self.num_cols()):
            cr.save()
            x = self.leftPadding + (i * (self.col_width() + COL_BORDER_SIZE)) + self.label_width + 1
            y = self.topPadding + self.dayLabelHeight
            w = self.col_width() - COL_BORDER_SIZE - self.label_width
            h = self.col_height()
            stylectxt.save()
            stylectxt.add_class(Gtk.STYLE_CLASS_FRAME)
            Gtk.render_frame(stylectxt, cr, x, y, w + 1, h + 1)
            color = stylectxt.get_property("border-color", state_flags)
            cr.set_source_rgb(color.red, color.green, color.blue)
            cr.set_line_width(1)

            for j in range(self.num_weeks_per_col()):
                wy = y + (j * self.week_height())
                cr.move_to(x, wy + 0.5)
                cr.line_to(x + w, wy + 0.5)
                cr.stroke()

            for j in range(1, 7, 1):
                dx = x + (j * self.day_width())
                cr.move_to(dx + 0.5, y)
                cr.line_to(dx + 0.5, y + self.col_height())
                cr.stroke()
            cr.restore()
            stylectxt.restore()
            layout.set_text("88", -1)
            max_width = layout.get_pixel_size()[0]

            if self.x_scale > max_width:
                stylectxt.save()
                stylectxt.add_class(Gtk.STYLE_CLASS_HEADER)
                Gtk.render_background(stylectxt, cr, x, y - self.dayLabelHeight, (self.day_width() * 7) + 1,
                                      self.dayLabelHeight)
                for j in range(7):
                    day_label_str = day_label((j + self.week_starts_monday) % 7)
                    layout.set_text(day_label_str, -1)
                    day_label_width = layout.get_pixel_size()[0]
                    label_x_offset = x + (j * self.day_width()) + (self.day_width() / 2) - (day_label_width / 2)
                    label_y_offset = y - self.dayLabelHeight
                    layout.set_text(day_label_str, -1)
                    Gtk.render_layout(stylectxt, cr, label_x_offset, label_y_offset, layout)
                stylectxt.restore()
        x_offset = self.label_height - (self.leftPadding * 2)
        stylectxt.save()
        stylectxt.add_class(Gtk.STYLE_CLASS_HEADER)

        for i in range(12):
            if self.monthPositions[i].x == -1:
                break
            Gtk.render_background(stylectxt, cr, self.monthPositions[i].x + x_offset + 1, self.topPadding,
                                  self.dayLabelHeight, self.col_height() + self.dayLabelHeight + 1)

        for i in range(12):
            if self.monthPositions[i].x == -1:
                break
            idx = (self.month - 1 + i) % 12
            layout.set_text(month_name(idx), -1)
            cr.save()
            cr.translate(self.monthPositions[i].x + x_offset, self.monthPositions[i].y)
            cr.rotate(-GLib.PI / 2.)
            Gtk.render_layout(stylectxt, cr, 0, 0, layout)
            cr.restore()
        stylectxt.restore()
        stylectxt.save()
        stylectxt.add_class("day-number")
        cr.save()

        d = datetime.date(day=1, month=self.month, year=self.year)
        eoc = d
        eoc += relativedelta(months=self.numMonths)
        doc = 0
        while d < eoc:
            x1, y1, x2, y2 = self.doc_coords(doc)
            day_num_buf = str(d.day)
            layout.set_text(day_num_buf, -1)
            num_w, num_h = layout.get_pixel_size()
            w = (x2 - x1) + 1
            h = (y2 - y1) + 1
            Gtk.render_layout(stylectxt, cr, x1 + (w / 2) - (num_w / 2), y1 + (h / 2) - (num_h / 2), layout)
            d += relativedelta(days=1)
            doc += 1
        cr.restore()
        stylectxt.restore()
        stylectxt.save()
        alloc = self.get_allocation()
        GLib.idle_add(self.queue_draw_area, alloc.x,
                             alloc.y,
                             alloc.width,
                             alloc.height)

    def populate_hover_window(self):
        if self.doc >= 0:
            w = self.date_label
            date = datetime.date(day=1, month=self.month, year=self.year)
            date += relativedelta(days=self.doc)
            strftime_buf = date.strftime("%x")
            w.set_text(strftime_buf)
            model = self.popup_model
            model.clear()
            for gdcmd in self.marks[self.doc]:
                model.append([gdcmd.name if gdcmd.name is not None else "(unnamed)", str(gdcmd.info)])

            if model.iter_n_children(None) == 0:
                model.append()
            while Gtk.events_pending():
                Gtk.main_iteration()

    def do_button_press_event(self, evt):
        win = self.get_screen().get_root_window()
        mon = self.get_display().get_monitor_at_window(win)
        win_xpos = evt.x_root + 5
        win_ypos = evt.y_root + 5
        work_area_size = mon.get_workarea()
        self.screen_width = work_area_size.width
        self.screen_height = work_area_size.height

        self.doc = self.wheres_this(evt.x, evt.y)
        self.showPopup = ~self.showPopup
        if self.showPopup and self.doc >= 0:
            self.transPopup.move(evt.x_root + 5, evt.y_root + 5)
            self.populate_hover_window()
            self.transPopup.queue_resize()
            self.transPopup.show_all()
            alloc = self.transPopup.get_allocation()

            if evt.x_root + 5 + alloc.width > self.screen_width:
                win_xpos = evt.x_root - 2 - alloc.width

            if evt.y_root + 5 + alloc.height > self.screen_height:
                win_ypos = evt.y_root - 2 - alloc.height

            self.transPopup.move(win_xpos, win_ypos)
        else:
            self.doc = -1
            self.transPopup.hide()
        return True

    def do_motion_notify_event(self, event):
        win_xpos = event.x_root + 5
        win_ypos = event.y_root + 5
        if not self.showPopup:
            return False

        doc = self.wheres_this(event.x, event.y)
        if doc >= 0:
            if self.doc != doc:
                self.doc = doc
                self.populate_hover_window()
                self.transPopup.queue_resize()
                self.transPopup.show_all()
            alloc = self.transPopup.get_allocation()
            if event.x_root + 5 + alloc.width > self.screen_width:
                win_xpos = event.x_root - 2 - alloc.width

            if event.y_root + 5 + alloc.height > self.screen_height:
                win_ypos = event.y_root - 2 - alloc.height

            self.transPopup.move(win_xpos, win_ypos)

        else:
            self.doc = -1
            self.transPopup.hide()
        return True

    def view_option_changed(self, widget):
        model = widget.get_model()
        _iter = widget.get_active_iter()
        if _iter is None:
            return
        months_val = model.get_value(_iter, ViewOptsColumns.NUM_MONTHS)
        self.set_num_months(months_val)

    def day_width_at(self, xScale):
        return xScale + MINOR_BORDER_SIZE

    def day_width(self):
        return self.day_width_at(self.x_scale)

    def day_height_at(self, yScale):
        return yScale + MINOR_BORDER_SIZE

    def day_height(self):
        return self.day_height_at(self.y_scale)

    def week_width_at(self, xScale):
        return self.day_width_at(xScale) * 7

    def week_width(self):
        return self.week_width_at(self.x_scale)

    def week_height_at(self, yScale):
        return self.day_height_at(yScale)

    def week_height(self):
        return self.week_height_at(self.y_scale)

    def col_width_at(self, xScale):
        return self.week_width_at(xScale) + self.label_width + COL_BORDER_SIZE

    def col_width(self):
        return self.col_width_at(self.x_scale)

    def col_height(self):
        return self.week_height() * self.num_weeks_per_col()

    def num_cols(self):
        return math.ceil(float(self.numMonths) / float(self.monthsPerCol))

    def num_weeks_per_col(self):
        num_weeks_to_ret = 0
        num_cols = self.num_cols()
        for i in range(num_cols):
            start = datetime.date(day=1, month=((self.month - 1 + (i * self.monthsPerCol)) % 12) + 1,
                                  year=self.year + math.floor((self.month - 1 + (i * self.monthsPerCol)) / 12))
            end = start
            end += relativedelta(
                months=min(self.numMonths, min(self.monthsPerCol, self.numMonths - ((i - 1) * self.monthsPerCol))))
            end -= relativedelta(days=1)
            startWeek = get_monday_week_of_year(start) if self.week_starts_monday else get_sunday_week_of_year(start)
            endWeek = get_monday_week_of_year(end) if self.week_starts_monday else get_sunday_week_of_year(end)
            if endWeek < startWeek:
                endWeek += get_monday_weeks_in_year(start.year) if self.week_starts_monday else \
                    get_sunday_weeks_in_year(start.year)
            num_weeks_to_ret = max(num_weeks_to_ret, (endWeek - startWeek) + 1)
        return num_weeks_to_ret

    def month_coords(self, month_of_cal, out_llist):
        if month_of_cal > self.numMonths:
            return
        col_num = math.floor(month_of_cal / self.monthsPerCol)
        month_offset = col_num * self.monthsPerCol
        previous_months_in_col = max(0, (month_of_cal % self.monthsPerCol))
        start_d = datetime.date(1, 1, 1)
        week_row = 0
        if previous_months_in_col > 0:
            start_d = start_d.replace(day=1, month=((self.month - 1 + month_offset) % 12) + 1,
                                      year=self.year + math.floor((self.month - 1 + month_offset) / 12))
            start_wk = get_monday_week_of_year(start_d) if self.week_starts_monday else get_sunday_week_of_year(start_d)
            end_d = start_d
            end_d += relativedelta(months=previous_months_in_col)
            end_d -= relativedelta(days=1)
            end_wk = get_monday_week_of_year(end_d) if self.week_starts_monday else get_sunday_week_of_year(end_d)
            if end_wk < start_wk:
                end_wk += get_monday_weeks_in_year(start_d.year) if self.week_starts_monday else \
                    get_sunday_weeks_in_year(start_d.year)
            week_row = end_wk - start_wk
            if end_d.weekday() + 1 == (
                    7 if self.week_starts_monday else 6):
                week_row += 1

        start_d = start_d.replace(day=1, month=((self.month - 1 + month_of_cal) % 12) + 1,
                                  year=self.year + math.floor((self.month - 1 + month_of_cal) / 12))
        end_d = start_d
        end_d += relativedelta(months=1)
        end_d -= relativedelta(days=1)
        start = (start_d.weekday() + 1 - self.week_starts_monday) % 7
        rect = Gdk.Rectangle()
        rect.x = self.leftPadding + MINOR_BORDER_SIZE + (col_num * (self.col_width() + COL_BORDER_SIZE)) + \
                 self.label_width + (start * self.day_width())
        rect.y = self.topPadding + self.dayLabelHeight + MINOR_BORDER_SIZE + (week_row * self.week_height())
        rect.width = (7 - start) * self.day_width()
        rect.height = self.week_height()
        out_llist.append(rect)

        week_start = get_monday_week_of_year(start_d) if self.week_starts_monday else get_sunday_week_of_year(
            start_d) + 1
        week_end = get_monday_week_of_year(end_d) if self.week_starts_monday else get_sunday_week_of_year(end_d)
        for i in range(week_start, week_end, 1):
            rect = Gdk.Rectangle()
            rect.x = self.leftPadding + MINOR_BORDER_SIZE + self.label_width + (
                    col_num * (self.col_width() + COL_BORDER_SIZE))
            rect.y = self.topPadding + self.dayLabelHeight + MINOR_BORDER_SIZE + (
                    (week_row + (i - week_start) + 1) * self.week_height())
            rect.width = self.week_width()
            rect.height = self.week_height()
            out_llist.append(rect)

        end_week_of_year = get_sunday_week_of_year(end_d)
        start_week_of_year = get_sunday_week_of_year(start_d)
        if self.week_starts_monday == 1:
            end_week_of_year = get_monday_week_of_year(end_d)
            start_week_of_year = get_monday_week_of_year(start_d)

        rect = Gdk.Rectangle()
        rect.x = self.leftPadding + MINOR_BORDER_SIZE + self.label_width + (
                col_num * (self.col_width() + COL_BORDER_SIZE))
        rect.y = self.topPadding + MINOR_BORDER_SIZE + self.dayLabelHeight + (
                (week_row + (end_week_of_year - start_week_of_year)) * self.week_height())
        rect.width = (((end_d.weekday() + 1 - self.week_starts_monday) % 7) + 1) * self.day_width()
        rect.height = self.week_height()
        out_llist.append(rect)

    def doc_coords(self, calendar_day):
        d = datetime.date(day=1, month=self.month, year=self.year)
        d += relativedelta(days=calendar_day)
        doc_month = d.month
        if d.year != self.year:
            doc_month += 12
        col_num = math.floor(float(doc_month - self.month) / float(self.monthsPerCol))
        day_col = (d.weekday() + 1 - self.week_starts_monday) % 7
        d_week_of_cal = get_sunday_week_of_year(d)
        if self.week_starts_monday == 1:
            d_week_of_cal = get_monday_week_of_year(d)
        d = datetime.date(day=1, month=self.month, year=self.year)
        d += relativedelta(months=col_num * self.monthsPerCol)
        top_of_col_week_of_cal = get_monday_week_of_year(d) if self.week_starts_monday else get_sunday_week_of_year(d)
        if d_week_of_cal < top_of_col_week_of_cal:
            week_offset = get_sunday_weeks_in_year(self.year)
            if self.week_starts_monday == 1:
                week_offset = get_monday_weeks_in_year(self.year)
            d_week_of_cal += week_offset
        weekRow = d_week_of_cal - top_of_col_week_of_cal
        x1 = self.leftPadding + MINOR_BORDER_SIZE + self.label_width + (
                col_num * (self.col_width() + COL_BORDER_SIZE)) + \
             (day_col * self.day_width()) + (self.day_width() / 4)
        y1 = self.topPadding + MINOR_BORDER_SIZE + self.dayLabelHeight + (weekRow * self.week_height()) + (
                self.day_height() / 4)

        x2 = x1 + (self.day_width() / 2)
        y2 = y1 + (self.day_height() / 2)
        return x1, y1, x2, y2

    def wheres_this(self, x, y):
        x -= self.leftPadding
        y -= self.topPadding
        if x < 0 or y < 0:
            return -1
        alloc = self.get_allocation()
        if x >= alloc.width or y >= alloc.height:
            return -1
        if x >= (self.num_cols() * (self.col_width() + COL_BORDER_SIZE)):
            return -1

        if y >= self.dayLabelHeight + self.col_height():
            return -1
        col_num = math.floor(x / (self.col_width() + COL_BORDER_SIZE))

        x %= self.col_width() + COL_BORDER_SIZE
        x -= self.label_width
        if x < 0:
            return -1
        if x >= self.day_width() * 7:
            return -1
        y -= self.dayLabelHeight
        if y < 0:
            return -1
        day_col = math.floor(float(x) / float(self.day_width()))
        week_row = math.floor(float(y) / float(self.week_height()))
        start_d = datetime.date(day=1, month=self.month, year=self.year)
        d = start_d
        d += relativedelta(months=col_num * self.monthsPerCol)
        day_col -= (d.weekday() + 1 - self.week_starts_monday) % 7
        if week_row == 0:
            if day_col < 0:
                return -1
        d += relativedelta(days=day_col + (week_row * 7))
        ccd = datetime.date(day=1, month=self.month, year=self.year)
        ccd += relativedelta(months=(col_num + 1) * self.monthsPerCol)
        if d >= ccd:
            return -1

        day_of_cal = d - start_d
        d -= relativedelta(months=self.numMonths)
        if d >= start_d:
            return -1
        return day_of_cal.days

    def get_doc_offset(self, d: datetime.date):
        soc = datetime.date(day=1, month=self.month, year=self.year)
        if d < soc:
            return -1
        to_ret = (d - soc).days
        soc += relativedelta(months=self.numMonths)
        if d >= soc:
            return -1
        return to_ret

    def add_tag_markings(self, tag):
        name = self.model.get_name(tag)
        info = self.model.get_info(tag)
        num_marks = self.model.get_instance_count(tag)
        if num_marks == 0:
            return
        dates = []
        cal_date = datetime.date(day=1, month=self.month, year=self.year)
        for idx in range(num_marks):
            dates.append(self.model.get_instance(tag, idx))

        if dates[0] is not None:
            if dates[0] < cal_date:
                self._set_month(dates[0].month, False)
                self._set_year(dates[0].year, False)
                self.remove_markings()
                self.add_markings()
            else:
                self.mark_add(tag, name, info, dates)

    def add_markings(self):
        tags = self.model.get_contained()
        for tag in tags:
            self.add_tag_markings(tag)

    def remove_markings(self):
        tags = self.model.get_contained()
        for tag in tags:
            self.mark_remove(tag, False)

    def model_added_cb(self, model, added_tag):
        self.add_tag_markings(added_tag)

    def model_update_cb(self, model, update_tag):
        self.mark_remove(update_tag, False)
        self.add_tag_markings(update_tag)

    def model_removing_cb(self, model, remove_tag):
        self.mark_remove(remove_tag, True)

    def set_model(self, model):
        if self.model is not None:
            self.remove_markings()
            self.model = None
        self.model = model
        self.model.connect("added", self.model_added_cb)
        self.model.connect("update", self.model_update_cb)
        self.model.connect("removing", self.model_removing_cb)
        self.add_markings()

    def mark_add(self, tag, name, info, date_array):
        if not any(date_array):
            return
        if not isinstance(tag, int):
            raise ValueError("Tag must be an integer")
        new_mark = MarkData()
        if name is not None:
            new_mark.name = name
        if info is not None:
            new_mark.info = info
        new_mark.tag = tag
        new_mark.ourMarks = []
        for d in date_array:
            doc = self.get_doc_offset(d)

            if doc < 0:
                continue
            if doc >= self.numMarks:
                break
            self.marks[doc].append(new_mark)
            new_mark.ourMarks.append(doc)
        self.markData.append(new_mark)
        if self.cal_drawing_area.get_realized():
            self.draw_to_buffer()
            self.cal_drawing_area.queue_draw()

    def mark_remove(self, mark_to_remove, redraw):
        mark_data = None
        if int(mark_to_remove) == -1:
            return
        for mark_data in self.markData:
            if mark_data.tag == mark_to_remove:
                break
        if mark_data is None:
            return
        for day_of_cal in mark_data.ourMarks:
            self.marks[day_of_cal].remove(mark_data)
        mark_data.ourMarks.clear()
        self.markData.remove(mark_data)
        if redraw:
            self.draw_to_buffer()
            self.cal_drawing_area.queue_draw()
