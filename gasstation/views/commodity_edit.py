from gasstation.views.dialogs.commodity.selector import CommoditySelectorDialog, CommodityMode


def commodity_edit_get_string(com):
    return com.get_printname()


def commodity_edit_new_select(toplevel, com, arg):
    return CommoditySelectorDialog.modal(toplevel, com, arg[0] if arg is not None else CommodityMode.ALL)
