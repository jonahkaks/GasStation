from gasstation.models.pump import *
from gasstation.views.tree import *


class PumpViewColumn(GObject.GEnum):
    NAME = 0


class PumpView(TreeView):
    __gtype_name__ = "PumpView"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        model = PumpModel.new()
        sel = self.get_selection()
        sel.set_mode(Gtk.SelectionMode.MULTIPLE)
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        self.add_text_column(0, title="Name", pref_name="name",
                             sizing_text="Pump 1", data_col=PumpModelColumn.NAME,
                             visible_always=True)
        self.add_text_column(1, title="Serial No", pref_name="serial",
                             sizing_text="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                             data_col=PumpModelColumn.SERIAL_NO,
                             visible_default=True)
        self.add_text_column(2, title="Location", pref_name="location",
                             sizing_text="Busuula ***********", data_col=PumpModelColumn.LOCATION,
                             visible_default=True)
    
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        selection = self.get_selection()
        selection.unselect_all()
        f_model = model.filter_new()
        s_model = Gtk.TreeModelSort(model=f_model)
        self.set_model(s_model)
        self.configure_columns()
        self.expand_columns("name", "serial", "location")
        self.set_state_section("Pump")
        self.show()

    @staticmethod
    def get_pump_from_iter(s_model, s_iter):
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        return model.get_pump(_iter)

    def cdf0(self, col, cell, s_model, s_iter, user_data):
        path = s_model.get_path(s_iter)
        indices = path.get_indices()[0]
        cell.set_properties(text="{}".format(indices + 1), editable=False, foreground_set=0, visible=True)

    def get_pump_from_path(self, path):
        s_model = self.get_model()
        s_iter = s_model.get_iter(path)
        return self.get_pump_from_iter(s_model, s_iter)

    def get_selected_pumps(self):
        selection = self.get_selection()
        s_model, paths = selection.get_selected_rows()
        if s_model is None or paths is None:
            return []
        _iters = map(s_model.get_iter, paths)
        return list(map(lambda _iter: self.get_pump_from_iter(s_model, _iter), _iters))


GObject.type_register(PumpView)
