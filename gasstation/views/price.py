# -*- coding: utf-8 -*-
from gasstation.models.price import *
from gasstation.utilities.ui import gs_default_currency
from libgasstation.core.price_db import PriceDB
from .tree import *


class FilterUserData:
    user_ns_fn = None
    user_cm_fn = None
    user_pc_fn = None
    user_data = None
    user_destroy = None


class PriceView(TreeView):
    __gtype_name__ = "PriceView"

    def __init__(self, book, *args, **kwargs):
        super().__init__(*args, **kwargs)
        price_db = PriceDB.get_db(book)
        model = PriceModel.new(book, price_db)
        f_model = model.filter_new()
        s_model = Gtk.TreeModelSort.new_with_model(f_model)
        self.set_name("price_tree")
        sample_text = gs_default_currency().get_printname()
        sample_text2 = "%s%s" % (sample_text, sample_text)
        self.add_text_column(0, title="Security", pref_name="security", sizing_text=sample_text2, visible_default=True,
                             data_col=PriceModelColumn.COMMODITY, sort_func=self.sort_by_name)
        self.add_text_column(1, title="Currency", pref_name="currency", sizing_text=sample_text, visible_default=True,
                             data_col=PriceModelColumn.CURRENCY, visible_col=PriceModelColumn.VISIBILITY,
                             sort_func=self.sort_by_name)
        self.add_text_column(2, title="Date", pref_name="date", sizing_text="2005-05-20", visible_default=True,
                             data_col=PriceModelColumn.DATE,
                             visible_col=PriceModelColumn.VISIBILITY, sort_func=self.sort_by_name)
        self.add_text_column(3, title="Source", pref_name="source", sizing_text="Finance::Quote",
                             data_col=PriceModelColumn.SOURCE,
                             visible_col=PriceModelColumn.VISIBILITY,
                             sort_func=self.sort_by_source)
        self.add_text_column(4, title="Type", pref_name="type", sizing_text="last",
                             data_col=PriceModelColumn.TYPE,
                             visible_col=PriceModelColumn.VISIBILITY,
                             sort_func=self.sort_by_type)
        self.add_numeric_column(5, title="Price", pref_name="price", sizing_text="100.00000", visible_default=True,
                                data_col=PriceModelColumn.VALUE,
                                visible_col=PriceModelColumn.VISIBILITY,
                                sort_func=self.sort_by_value)
        self.configure_columns()
        if not s_model.get_sort_column_id():
            s_model.set_sort_column_id(PriceModelColumn.COMMODITY, Gtk.SortType.ASCENDING)
        self.set_model(s_model)
        self.set_show_expanders(True)
        self.show()

    @staticmethod
    def get_prices(f_model, f_iter_a, f_iter_b):
        model = f_model.get_model()
        iter_a = f_model.convert_iter_to_child_iter(f_iter_a)
        if not model.iter_is_price(iter_a): return False
        iter_b = f_model.convert_iter_to_child_iter(f_iter_b)
        price_a = model.get_price(iter_a)
        price_b = model.get_price(iter_b)
        return price_a, price_b

    @staticmethod
    def sort_ns_or_cm(f_model, f_iter_a, f_iter_b):
        model = f_model.get_model()
        iter_a = f_model.convert_iter_to_child_iter(f_iter_a)
        iter_b = f_model.convert_iter_to_child_iter(f_iter_b)
        if model.iter_is_namespace(iter_a):
            ns_a = model.get_namespace(iter_a)
            ns_b = model.get_namespace(iter_b)
            return GLib.utf8_collate(ns_a.get_gui_name(),
                                     ns_b.get_gui_name())
        comm_a = model.get_commodity(iter_a)
        comm_b = model.get_commodity(iter_b)
        return GLib.utf8_collate(comm_a.get_mnemonic(),
                                 comm_b.get_mnemonic())

    @staticmethod
    def default_sort(price_a, price_b):
        curr_a = price_a.get_currency()
        curr_b = price_b.get_currency()
        result = GLib.utf8_collate(curr_a.get_namespace(), curr_b.get_namespace())
        if result != 0: return result
        result = GLib.utf8_collate(curr_a.get_mnemonic(), curr_b.get_mnemonic())
        if result != 0: return result
        time_a = price_a.get_time64()
        time_b = price_b.get_time64()
        result = -1 if time_a < time_b else 1 if time_a > time_b else 0
        if result:
            return -result
        if price_a.get_value() < price_b.get_value():
            return -1
        if price_a.get_value() > price_b.get_value():
            return 1
        return 0

    @classmethod
    def sort_by_name(cls, f_model, f_iter_a, f_iter_b):
        if not cls.get_prices(f_model, f_iter_a, f_iter_b):
            return cls.sort_ns_or_cm(f_model, f_iter_a, f_iter_b)
        price_a, price_b = cls.get_prices(f_model, f_iter_a, f_iter_b)
        return cls.default_sort(price_a, price_b)

    @classmethod
    def sort_by_date(cls, f_model, f_iter_a, f_iter_b):
        if not cls.get_prices(f_model, f_iter_a, f_iter_b):
            return cls.sort_ns_or_cm(f_model, f_iter_a, f_iter_b)
        price_a, price_b = cls.get_prices(f_model, f_iter_a, f_iter_b)
        time_a = price_a.get_time64()
        time_b = price_b.get_time64()
        result = -1 if time_a < time_b else 1 if time_a > time_b else 0
        if result:
            return -result
        return cls.default_sort(price_a, price_b)

    @classmethod
    def sort_by_source(cls, f_model, f_iter_a, f_iter_b):
        if not cls.get_prices(f_model, f_iter_a, f_iter_b):
            return cls.sort_ns_or_cm(f_model, f_iter_a, f_iter_b)
        price_a, price_b = cls.get_prices(f_model, f_iter_a, f_iter_b)
        result = price_a.get_source() < price_b.get_source()
        if result != 0:
            return result
        return cls.default_sort(price_a, price_b)

    @classmethod
    def sort_by_type(cls, f_model, f_iter_a, f_iter_b):
        if not cls.get_prices(f_model, f_iter_a, f_iter_b):
            return cls.sort_ns_or_cm(f_model, f_iter_a, f_iter_b)
        price_a, price_b = cls.get_prices(f_model, f_iter_a, f_iter_b)

        result = GLib.utf8_collate(price_a.get_type_string(),
                                   price_b.get_type_string())
        if result != 0:
            return result
        return cls.default_sort(price_a, price_b)

    @classmethod
    def sort_by_value(cls, f_model, f_iter_a, f_iter_b):
        if not cls.get_prices(f_model, f_iter_a, f_iter_b):
            return cls.sort_ns_or_cm(f_model, f_iter_a, f_iter_b)
        price_a, price_b = cls.get_prices(f_model, f_iter_a, f_iter_b)
        comm_a = price_a.get_currency()
        comm_b = price_b.get_currency()
        if comm_a and comm_b:
            value = GLib.utf8_collate(comm_a.get_namespace(), comm_b.get_namespace())
            if value != 0:
                return value
            value = GLib.utf8_collate(comm_a.get_mnemonic(),
                                      comm_b.get_mnemonic())
            if value != 0:
                return value
        if price_a.get_value() < price_b.get_value():
            return -1
        if price_a.get_value() > price_b.get_value():
            return 1
        return cls.default_sort(price_a, price_b)

    def get_iter_from_price(self, price):
        if price is None: return False
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()
        t, _iter = model.get_iter_from_price(price)
        if not t: return False
        f_iter = f_model.convert_child_iter_to_iter(_iter)
        s_iter = s_model.convert_child_iter_to_iter(f_iter)
        return s_iter

    @staticmethod
    def filter_destroy(fd):
        if fd.user_destroy:
            fd.user_destroy(fd.user_data)

    @staticmethod
    def filter_helper(model, _iter, fd):
        if model is None: return False
        if _iter is None: return False

        if model.iter_is_namespace(_iter):
            if fd.user_ns_fn:
                name_space = model.get_namespace(_iter)
                return fd.user_ns_fn(name_space, fd.user_data)
            return True
        if model.iter_is_commodity(_iter):
            if fd.user_cm_fn:
                commodity = model.get_commodity(_iter)
                return fd.user_cm_fn(commodity, fd.user_data)
            return True
        if model.iter_is_price(_iter):
            if fd.user_pc_fn:
                price = model.get_price(_iter)
                return fd.user_pc_fn(price, fd.user_data)
            return True
        return False

    def set_filter(self, ns_func, cm_func, pc_func, data, destroy):
        if ns_func is None or cm_func is None: return
        fd = FilterUserData()
        fd.user_ns_fn = ns_func
        fd.user_cm_fn = cm_func
        fd.user_pc_fn = pc_func
        fd.user_data = data
        fd.user_destroy = destroy
        s_model = self.get_model()
        f_model = s_model.get_model()
        f_model.set_visible_func(self.filter_helper, fd)
        f_model.refilter()

    def get_selected_price(self):
        selection = self.get_selection()
        s_model, s_iter = selection.get_selected()
        if s_iter is None: return False
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        price = model.get_price(_iter)
        return price

    def set_selected_price(self, price):
        selection = self.get_selection()
        selection.unselect_all()
        if price is None:
            return
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()
        path = model.get_path_from_price(price)
        if path is None:
            return
        f_path = f_model.convert_child_path_to_path(path)
        if f_path is None: return
        s_path = s_model.convert_child_path_to_path(f_path)
        if s_path is None: return
        parent_path = s_path.copy()
        if parent_path.up():
            self.expand_to_path(parent_path)
        selection.select_path(s_path)
        self.scroll_to_cell(s_path, None, False, 0.0, 0.0)

    @staticmethod
    def get_selected_prices_helper(s_model, s_path, s_iter, data):
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        price = model.get_price(_iter)
        if price is not None:
            data.append(price)

    def get_selected_prices(self):
        return_list = []
        selection = self.get_selection()
        selection.selected_foreach(self.get_selected_prices_helper, return_list)
        return return_list


GObject.type_register(PriceView)
