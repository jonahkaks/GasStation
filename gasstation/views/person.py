from uuid import UUID

from gasstation.models.person import *
from gasstation.utilities.ui import gs_default_currency, gs_default_report_currency
from gasstation.views.tree import *
from libgasstation.core.hook import Hook, HOOK_CURRENCY_CHANGED

SAMPLE_PERSON_VALUE = "$1,000,000.00"


class PersonFilterDialog:
    def __init__(self):
        self.dialog = None
        self.view = None
        self.show_inactive = False
        self.original_show_inactive = False
        self.show_zero_total = False
        self.original_show_zero_total = False

    def filter_persons(self, person):
        report_commodity = gs_default_report_currency()
        if not self.show_inactive and not person.get_active():
            return False
        if not self.show_zero_total:
            total = person.get_balance_in_currency(report_commodity)
            if total.is_zero():
                return False
        return True

    def show_inactive_toggled_cb(self, button):
        self.show_inactive = button.get_active()
        self.view.refilter()

    def show_zero_toggled_cb(self, button):
        self.show_zero_total = button.get_active()
        self.view.refilter()

    def response_cb(self, _, response):
        if response != Gtk.ResponseType.OK:
            self.show_inactive = self.original_show_inactive
            self.show_zero_total = self.original_show_zero_total
            self.view.refilter()
        self.dialog.hide()
        return False

    def create(self, page):
        if self.dialog is not None:
            self.dialog.present()
            return
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/gs-tree-view-person.ui",
                                          ["filter_by_dialog"])
        dialog = builder.get_object("filter_by_dialog")
        self.dialog = dialog
        dialog.set_transient_for(page.window)
        title = "Filter %s by..." % page.get_name()
        dialog.set_title(title)
        self.original_show_inactive = self.show_inactive
        self.original_show_zero_total = self.show_zero_total
        button = builder.get_object("show_inactive")
        button.set_active(not self.show_inactive)
        button = builder.get_object("show_zero")
        button.set_active(self.show_zero_total)
        builder.connect_signals(self)
        dialog.show_all()


TREE_VIEW_PERSON_NAME = "PersonView"
PERSON_SELECTED_LABEL = "SelectedPerson"
SHOW_INACTIVE_LABEL = "ShowInactive"
SHOW_ZERO_LABEL = "ShowZeroTotal"


class Bar:
    def __init__(self, kf, gn):
        self.key_file = kf
        self.group_name = gn


class PersonView(TreeView):
    __gtype_name__ = "PersonView"

    def __init__(self, person_type, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id_column = None
        self.balance_report_column = None
        self.notes_column = None
        self._type = None
        self.filter_destroy = None
        self.filter_data = None
        self.filter_fn = None
        self.vi = None
        sel = self.get_selection()
        sel.set_mode(Gtk.SelectionMode.MULTIPLE)
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        self.set_fixed_height_mode(True)
        if person_type == "Customer":
            person_name = "COMPANY/CUSTOMER"
            person_id = "CUSTOMER NUMBER"
        elif person_type == "Jon=b":
            person_name = "JOB"
            person_id = "JOB NUMBER"

        elif person_type == "Supplier":
            person_name = "COMPANY/SUPPLIER"
            person_id = "SUPPLIER NUMBER"

        elif person_type == "Staff":
            person_name = "STAFF"
            person_id = "STAFF NUMBER"
        else:
            person_name = "NAME"
            person_id = "ID #"

        model = PersonModel.new(person_type)

        f_model = model.filter_new()
        s_model = Gtk.TreeModelSort(model=f_model)
        self.set_model(s_model)
        sample_currency = gs_default_currency().get_full_name()
        self.add_numeric_column(0, title="#", pref_name="num", sizing_text="100000", visible_always=True,
                                data_func=self.index_cdf)
        self.add_text_column(1, pixbuf_col=PersonModelColumn.AVATAR,
                             title=person_name, pref_name="name", sizing_text="Mr Kafeero Augustine Jonah",
                             data_col=PersonModelColumn.FULL_NAME, visible_always=True)
        self.add_text_column(2, title=person_id, pref_name="person-id", sizing_text="1-123-1234",
                             data_col=PersonModelColumn.ID)
        self.add_text_column(3, title="CURRENCY", pref_name="currency", sizing_text=sample_currency,
                             data_col=PersonModelColumn.CURRENCY)
        self.add_text_column(4, title="ADDRESS", pref_name="address", sizing_text="##########################",
                             data_col=PersonModelColumn.ADDRESS, visible_default=True)
        self.add_text_column(5, title="PHONE", pref_name="phone", sizing_text="+1-617-542-5942",
                             data_col=PersonModelColumn.PHONE, visible_default=True)
        self.add_text_column(6, title="FAX", pref_name="fax", sizing_text="+1-617-542-2652",
                             data_col=PersonModelColumn.FAX)
        self.add_text_column(7, title="EMAIL", pref_name="email", sizing_text="jjkafeeroaugusting@gmail.com",
                             data_col=PersonModelColumn.EMAIL, visible_default=True)
        self.add_numeric_column(8, title="BALANCE", pref_name="balance", sizing_text=SAMPLE_PERSON_VALUE,
                                data_col=PersonModelColumn.BALANCE, color_col=PersonModelColumn.COLOR_BALANCE,
                                visible_default=True)
        self.balance_report_column = self.add_numeric_column(9, title="BALANCE", pref_name="balance-report",
                                                             sizing_text=SAMPLE_PERSON_VALUE,
                                                             data_col=PersonModelColumn.BALANCE_REPORT,
                                                             color_col=PersonModelColumn.COLOR_BALANCE)

        self.notes_column = self.add_text_column(10, title="NOTES", pref_name="notes",
                                                 sizing_text="Sample person notes.",
                                                 data_col=PersonModelColumn.NOTES)
        self.add_toggle_column(11, title="ACTIVE", short_title="A", pref_name="active",
                               data_col=PersonModelColumn.ACTIVE,
                               edited=self.active_toggled)
        self.update_column_names()
        self.configure_columns()
        f_model.set_visible_func(self.filter_helper)
        self.set_search_equal_func(self.search_compare)
        Hook.add_dangler(HOOK_CURRENCY_CHANGED, self.currency_changed_cb)
        s_model.set_sort_column_id(PersonModelColumn.FULL_NAME, Gtk.SortType.ASCENDING)
        self.show()

    def index_cdf(self, col, cell, s_model, _iter, *user_data):
        path = s_model.get_path(_iter)
        cell.set_properties(text=str(path.get_indices()[0] + 1), editable=False)

    def active_toggled(self, cell, s_path_str):
        s_path = Gtk.TreePath.new_from_string(s_path_str)
        person = self.get_person_from_path(s_path)
        if person is not None:
            active = not cell.get_active()
            person.set_active(active)

    @staticmethod
    def update_column_name(column, fmt, mnemoonic):
        column.set_title(fmt % mnemoonic)

    def update_column_names(self):
        mnemonic = gs_default_report_currency().get_mnemonic()
        self.update_column_name(self.balance_report_column, "BALANCE (%s)", mnemonic)
        self.set_show_column_menu(False)
        self.set_show_column_menu(False)

    @staticmethod
    def currency_changed_cb():
        views = Tracking.get_list(TREE_VIEW_PERSON_NAME)
        for view in views:
            view.update_column_names()

    @staticmethod
    def get_person_from_iter(s_model, s_iter):
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        return model.get_person(_iter)

    def get_person_from_path(self, path):
        s_model = self.get_model()
        s_iter = s_model.get_iter(path)
        return self.get_person_from_iter(s_model, s_iter)

    def get_selected_persons(self):
        selection = self.get_selection()
        s_model, paths = selection.get_selected_rows()
        if s_model is None or paths is None:
            return []
        _iters = map(s_model.get_iter, paths)
        return list(map(lambda _iter: self.get_person_from_iter(s_model, _iter), _iters))

    def save_selected_row(self, key_file, group_name):
        persons = self.get_selected_persons()
        if persons is None or not isinstance(persons, list) or len(persons)==0:
            return
        key_file.set_string(group_name, PERSON_SELECTED_LABEL,
                            ",".join(map(lambda person: str(person.get_guid()), persons)))

    def save(self, fd, key_file, group_name):
        if key_file is None or group_name is None:
            return
        key_file.set_boolean(group_name, SHOW_INACTIVE_LABEL,fd.show_inactive)
        key_file.set_boolean(group_name, SHOW_ZERO_LABEL, fd.show_zero_total)
        self.save_selected_row(key_file, group_name)

    def restore_selected_row(self, person_type, person_guid_str):
        book = Session.get_current_book()
        coll = book.get_collection(person_type)
        person = coll.get(UUID(person_guid_str))
        if person is not None:
            self.set_selected_person(person)

    def restore(self, fd, key_file, group_name, person_type):
        try:
            show = key_file.get_boolean(group_name, SHOW_INACTIVE_LABEL)
        except GLib.Error:
            show = True
        fd.show_inactive = show
        try:
            show = key_file.get_boolean(group_name, SHOW_ZERO_LABEL)
        except GLib.Error:
            show = True
        fd.show_zero_total = show
        try:
            value = key_file.get_string(group_name, PERSON_SELECTED_LABEL)
            selection = self.get_selection()
            selection.unselect_all()
            for v in value.split(","):
                self.restore_selected_row(person_type, v)
        except GLib.Error:
            pass

        self.refilter()

    def set_selected_person(self, person):
        selection = self.get_selection()
        if person is None:
            return
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()

        path = model.get_path_from_person(person)
        if path is None:
            return
        f_path = f_model.convert_child_path_to_path(path)
        if f_path is None:
            return
        s_path = s_model.convert_child_path_to_path(f_path)
        if s_path is None:
            return
        selection.select_path(s_path)
        while Gtk.events_pending():
            Gtk.main_iteration()
        self.scroll_to_cell(s_path, None, False, 0.0, 0.0)

    def set_filter(self, func, data=None, destroy=None):
        if self.filter_destroy is not None:
            self.filter_destroy(self.filter_data)
        self.filter_destroy = destroy
        self.filter_data = data
        self.filter_fn = func
        self.refilter()

    def refilter(self):
        s_model = self.get_model()
        if s_model is None:
            return False
        f_model = s_model.get_model()
        f_model.clear_cache()
        f_model.refilter()

    def filter_helper(self, model, _iter, *args):
        if _iter is None:
            return False
        person = model.get_person(_iter)
        if self.filter_fn is not None:
            return self.filter_fn(person)
        else:
            return True

    def destroy(self):
        model = self.get_model_from_view()
        if model is not None:
            model.destroy()
        super().destroy()

    @staticmethod
    def search_compare(model, column, key, _iter):
        match = False
        case_normalized_key = None
        case_normalized_string = None
        normalized_key = GLib.utf8_normalize(key, -1, GLib.NormalizeMode.ALL)
        if normalized_key:
            case_normalized_key = GLib.utf8_casefold(normalized_key, -1)
        if case_normalized_key:
            for i in range(3):
                st = None
                if i == 0:
                    st = model.get_value(_iter, PersonModelColumn.FULL_NAME)
                elif i == 1:
                    st = model.get_value(_iter, PersonModelColumn.ID)
                elif i == 2:
                    st = model.get_value(_iter, PersonModelColumn.EMAIL)
                if st is None or len(st) == 0:
                    continue
                normalized_string = GLib.utf8_normalize(st, -1, GLib.NormalizeMode.ALL)
                if normalized_string:
                    case_normalized_string = GLib.utf8_casefold(normalized_string, -1)
                if case_normalized_string is not None and case_normalized_string.find(case_normalized_key) != -1:
                    match = True
                if match:
                    break
        return not match



GObject.type_register(PersonView)
