from gi.repository import Pango

# from gasstation.utilities.gtk import ensure_dir
from libgasstation.core.account import Placeholder
from libgasstation.core.euro import *
from libgasstation.core.uri import Uri
from .ledger import *

STATE_SECTION_REG_PREFIX = "Register"
STATE_SECTION_GEN_JOURNAL = "General Journal"
KEY_PAGE_SORT = "register_order"
KEY_PAGE_SORT_REV = "register_reversed"
KEY_PAGE_FILTER = "register_filter"


class BalanceType(GObject.GEnum):
    NORMAL = 0
    CLEARED = 1
    RECONCILED = 2


class RegisterWindow(Gtk.Box):
    __gsignals__ = {
        'help-changed': (GObject.SignalFlags.RUN_LAST, None, (int,))
    }

    def __init__(self, ledger, parent, number_of_lines, read_only, **kwargs):
        super().__init__(**kwargs)
        self.numRows = 10
        self.read_only = False
        self.scroll_bar = None
        self.balance_label = None
        self.future_label = None
        self.cleared_label = None
        self.reconciled_label = None
        self.projected_minimum_label = None
        self.shares_label = None
        self.value_label = None
        self.summary_bar = None
        self.scroll_adj = None
        self.set_orientation(Gtk.Orientation.VERTICAL)
        self.numRows = number_of_lines
        self.read_only = read_only
        self.ledger = weakref.ref(ledger)
        self.window = weakref.ref(parent)
        self.determine_read_only()
        self.determine_account_pr()
        self.setup_status_widgets()
        self.reference_ids = defaultdict(list)
        self.create_table()

    def scroll_sync_cb(self, model):
        trans_position = model.position_of_trans_in_full_tlist
        self.scroll_adj.set_value(trans_position)
        self.scroll_adj.set_upper(model.number_of_trans_in_full_tlist + 9)

    def scroll_value_changed_cb(self, adj):
        model = self.ledger().get_model()
        trans_position = int(adj.get_value())
        # text = model.get_tooltip(trans_position)
        # settings = self.scroll_bar.get_settings()
        # settings.set_property("gtk-tooltip-timeout", 2)
        # self.scroll_bar.set_tooltip_text(text)

    def scroll_button_event_cb(self, widget, event):
        model = self.ledger().get_model()
        trans_position = int(self.scroll_adj.get_value())
        model.set_current_trans_by_position(trans_position)
        model.emit("refresh_trans", 32)
        return False

    def create_table(self):
        state_file = State.get_current()
        account = self.ledger().get_leader()
        guid = account.guid if account is not None else ""
        self.ledger().set_user_data(self)
        self.ledger().set_handlers(self.ld_destroy, self.get_parent)
        model = self.ledger().get_model()
        view = RegisterView(model)
        ledger_type = self.ledger().get_type()
        if ledger_type == RegisterLedgerType.GL and model.type == RegisterModelType.GENERAL_JOURNAL:
            state_section = STATE_SECTION_GEN_JOURNAL
        elif ledger_type == RegisterLedgerType.SUB_ACCOUNT:
            state_section = " ".join([STATE_SECTION_REG_PREFIX, str(guid), "w/subaccounts"])
        else:
            state_section = " ".join([STATE_SECTION_REG_PREFIX, str(guid)])
        view.set_properties(state_section=state_section, show_column_menu=False)
        num_of_trans = model.number_of_trans_in_full_tlist - 1
        self.scroll_adj = Gtk.Adjustment.new(model.position_of_trans_in_full_tlist, 0.0,
                                             num_of_trans + 10, 2, 10.0, 15)
        scrolled_window = Gtk.ScrolledWindow.new(vadjustment=self.scroll_adj)
        scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scrolled_window.show()
        self.pack_start(scrolled_window, True, True, 0)
        self.ledger().set_view(view)
        try:
            model.sort_depth = state_file.get_integer(state_section, "sort_depth")
        except GLib.Error:
            pass
        s_model = view.get_model()
        if s_model is not None:
            sort_col, _type = s_model.get_sort_column_id()
            model.sort_col = sort_col
            model.sort_direction = _type
        view.configure_columns()
        if ledger_type == RegisterLedgerType.GL and model.type == RegisterModelType.GENERAL_JOURNAL:
            view.set_show_column_menu(True)
        else:
            view.set_show_column_menu(False)
        view.expand_columns("descnotes")
        view.set_headers_visible(True)
        scrolled_window.add(view)
        view.show()
        self.show()
        view.set_read_only(self.read_only)
        self.ledger().set_split_view_refresh(True)
        self.reference_ids[model].append(model.connect_after("refresh_status_bar", self.redraw_all_cb))
        self.reference_ids[model].append(model.connect("scroll_sync", self.scroll_sync_cb))
        self.reference_ids[view].append(view.connect("help_signal", self.emit_help_changed))
        self.scroll_value_changed_cb(self.scroll_adj)
        self.reference_ids[self.scroll_adj].append(
            self.scroll_adj.connect("value-changed", self.scroll_value_changed_cb))
        self.reference_ids[s_model].append(s_model.connect("sort-column-changed", self.sort_changed_cb))

    def emit_help_changed(self, view, p):
        self.emit("help-changed", 0)

    def setup_status_widgets(self):
        model = self.ledger().get_model()
        use_double_line = self.ledger().default_double_line()
        model.config(model.type, model.style, use_double_line)

    def jump_to_split(self, split):
        if split is None:
            return
        view = self.ledger().get_view()
        view.jump_to(None, split, False)

    def jump_to_split_amount(self, split):
        if split is None:
            return
        view = self.ledger().get_view()
        view.jump_to(None, split, True)

    @staticmethod
    def update_summary_label(label, getter, leader, print_info, commodity, euroFlag):
        if label is None:
            return
        amount = getter()
        st = sprintamount(amount, print_info)
        if euroFlag:
            st += " / "
            st += sprintamount(gs_convert_to_euro(commodity, amount),
                               PrintAmountInfo.commodity(gs_get_euro(), True))
        gs_set_label_color(label, amount)
        label.set_text(st)

    def redraw_all_cb(self, *args):
        leader = self.ledger().get_leader()
        if self.summary_bar is None or leader is None:
            return

        commodity = leader.get_commodity()
        print_info = PrintAmountInfo.account(leader, True)
        if commodity is not None:
            euro = gs_is_euro_currency(commodity) and commodity.get_mnemonic() == "EUR"
        else:
            euro = False
        self.update_summary_label(self.balance_label, leader.get_present_balance, leader, print_info, commodity,
                                  euro)
        self.update_summary_label(self.cleared_label, leader.get_cleared_balance, leader, print_info, commodity,
                                  euro)
        self.update_summary_label(self.reconciled_label, leader.get_reconciled_balance, leader, print_info, commodity,
                                  euro)
        self.update_summary_label(self.future_label, leader.get_balance, leader, print_info, commodity, False)
        self.update_summary_label(self.projected_minimum_label, leader.get_projected_minimum_balance, leader,
                                  print_info,
                                  commodity, euro)

        if self.shares_label is not None:
            print_info = PrintAmountInfo.account(leader, True)
            amount = leader.get_balance()
            # if reverse:
            #     amount = amount * -1
            string = sprintamount(amount, print_info)
            gs_set_label_color(self.shares_label, amount)
            self.shares_label.set_text(string)

        if self.value_label is not None:
            currency = gs_default_currency()
            amount = leader.get_balance_in_currency(currency, False)
            print_info = PrintAmountInfo.commodity(currency, True)
            string = sprintamount(amount, print_info)
            gs_set_label_color(self.value_label, amount)
            self.value_label.set_text(string)

    def ld_destroy(self):
        self.ledger().set_user_data(None)

    def sort_changed_cb(self, sortable):
        statef = State.get_current()
        sortcol, _type = sortable.get_sort_column_id()
        view = self.ledger().get_view()
        model = self.ledger().get_model()
        sort_depth = view.get_selected_row_depth()
        if sort_depth != 0:
            model.sort_depth = sort_depth
        model.sort_col = sortcol
        model.sort_direction = _type
        state_section = view.get_state_section()
        statef.set_integer(state_section, "sort_depth", model.sort_depth)
        if sortcol != -1:
            self.ledger().refresh()

    def change_style(self, style):
        model = self.ledger().get_model()
        if style == model.style:
            return
        model.config(model.type, style, model.use_double_line)
        self.ledger().get_view().set_format()

    def style_ledger_cb(self, w):
        if not w.get_active():
            return
        self.change_style(RegisterModelStyle.LEDGER)

    def style_auto_ledger_cb(self, w):
        if not w.get_active():
            return
        self.change_style(RegisterModelStyle.AUTO_LEDGER)

    def style_journal_cb(self, w):
        if not w.get_active():
            return
        self.change_style(RegisterModelStyle.JOURNAL)

    def double_line_cb(self, w):
        model = self.ledger().get_model()
        use_double_line = w.get_active()
        if use_double_line == model.use_double_line:
            return
        model.config(model.type, model.style, use_double_line)
        self.ledger().get_view().set_format()

    def create_summary_bar(self):
        if self.ledger().get_type() >= RegisterLedgerType.SUB_ACCOUNT:
            self.summary_bar = None
            return
        summary_bar = Gtk.Toolbar()
        gs_widget_set_style_context(summary_bar, "summary-tbar")
        summary_bar.set_icon_size(Gtk.IconSize.MENU)
        summary_bar.set_style(Gtk.ToolbarStyle.ICONS)
        summary_bar.get_style_context().add_class(Gtk.STYLE_CLASS_INLINE_TOOLBAR)
        gs_widget_set_style_context(summary_bar, "summary-tbar")
        if not self.ledger().get_leader().is_priced():
            self.balance_label = add_toolbar_label(summary_bar, "Present:")
            self.future_label = add_toolbar_label(summary_bar, "Future:")
            self.cleared_label = add_toolbar_label(summary_bar, "Cleared:")
            self.reconciled_label = add_toolbar_label(summary_bar, "Reconciled:")
            self.projected_minimum_label = add_toolbar_label(summary_bar, "Projected Minimum:")
        else:
            self.shares_label = add_toolbar_label(summary_bar, "Shares:")
            self.value_label = add_toolbar_label(summary_bar, "Current Value:")
        self.summary_bar = summary_bar
        self.redraw_all_cb()
        return self.summary_bar

    def get_placeholder(self):
        model = self.ledger().get_model()
        if model.type in [RegisterModelType.GENERAL_JOURNAL, RegisterModelType.INCOME_LEDGER,
                          RegisterModelType.PORTFOLIO_LEDGER,
                          RegisterModelType.SEARCH_LEDGER]:
            single_account = False
        else:
            single_account = True
        leader = self.ledger().get_leader()
        if leader is None:
            return Placeholder.NONE
        if single_account:
            if leader.get_placeholder():
                return Placeholder.THIS
            return Placeholder.NONE
        return leader.get_descendant_placeholder()

    def determine_account_pr_dialog(self):
        title = "Account Payable / Receivable Register"
        message = "The register displayed is for Account Payable or Account Receivable. " \
                  "Changing the entries may cause harm, please use the business " \
                  "options to change the entries."
        dialog = Gtk.MessageDialog(transient_for=self.window, message_format=title, modal=True,
                                   destroy_with_parent=True,
                                   message_type=Gtk.MessageType.WARNING,
                                   buttons=Gtk.ButtonsType.CLOSE)
        dialog.format_secondary_text(message)
        gs_dialog_run(dialog, PREF_WARN_REG_IS_ACCT_PAY_REC)
        dialog.destroy()
        return False

    def determine_account_pr(self):
        model = self.ledger().get_model()
        if model.type != RegisterModelType.PAYABLE_REGISTER and model.type != RegisterModelType.RECEIVABLE_REGISTER:
            return
        GLib.timeout_add(250, self.determine_account_pr_dialog)

    def callback_bug_workaround(self, string):
        read_only = "This account register is read-only."
        dialog = Gtk.MessageDialog(transient_for=self.window,
                                   modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.WARNING,
                                   buttons=Gtk.ButtonsType.CLOSE,
                                   message_format=read_only)
        dialog.format_secondary_text(string)
        gs_dialog_run(dialog, PREF_WARN_REG_IS_READ_ONLY)
        dialog.destroy()
        return False

    def determine_read_only(self):
        if Session.get_current_book().is_readonly():
            self.read_only = True
        if not self.read_only:
            ph = self.get_placeholder()
            if ph == Placeholder.NONE:
                return
            elif ph == Placeholder.THIS:
                string = "This account may not be edited. If you want to edit transactions in this register, please " \
                         "open the account options and turn off the placeholder checkbox."
            else:
                string = "One of the sub-accounts selected may not be edited. If you want to edit transactions in " \
                         "this register, please open the sub-account options and turn off the placeholder checkbox. " \
                         "You may also open an individual account instead of a set of accounts."
            self.read_only = True
            GLib.timeout_add(250, self.callback_bug_workaround, string)

    def get_parent(self):
        return self.window

    def get_register(self):
        return self.ledger().get_view()

    def get_summary_bar(self):
        return self.summary_bar

    def get_read_only(self):
        return self.read_only

    def set_moved_cb(self, cb, *cb_data):
        self.ledger().get_view().set_uiupdate_cb(cb, *cb_data)

    def raise_window(self):
        if self.window is not None:
            self.window.present()

    def associate_handler_file(self, trans, have_uri):
        path_head = gs_pref_get_string(PREFS_GROUP_GENERAL, "assoc-head")
        dialog = Gtk.FileChooserDialog(title="Associate File with Transaction",
                                       parent=self.window, action=Gtk.FileChooserAction.OPEN,
                                       buttons=("_Remove", Gtk.ResponseType.REJECT,
                                                "_Cancel", Gtk.ResponseType.CANCEL,
                                                "_OK", Gtk.ResponseType.ACCEPT))
        dialog.set_local_only(False)
        path_head_set = path_head is not None and len(path_head) != 0

        if have_uri:
            uri = Uri(trans.get_association_url() or "")
            scheme = uri.get_scheme()
            file_uri = None
            if scheme is None or len(scheme) == 0:
                if path_head_set:
                    file_path = os.path.abspath(Uri(path_head).get_path())
                else:
                    file_path = os.path.abspath(uri)
                file_uri = Uri(file_path)
            if scheme == "file":
                file_uri = uri

            if file_uri:
                file_uri_u = GLib.uri_unescape_string(file_uri.get_path(), None)
                filename = Uri(file_uri_u).get_path()

                if os.name == "windows":
                    filename = GLib.strdelimit(filename, "/", '\\')

                uri_label = "Existing Association is '" + filename
                label = Gtk.Label(uri_label)
                dialog.set_extra_widget(label)
                label.set_ellipsize(Pango.EllipsizeMode.START)
                gs_widget_set_style_context(label, "gs-class-highlight")
                dialog.set_uri(file_uri)

        response = dialog.run()
        if response == Gtk.ResponseType.REJECT:
            trans.set_association_url("")

        elif response == Gtk.ResponseType.ACCEPT:
            gfile = dialog.get_file()
            original = GLib.build_filenamev([GLib.get_user_data_dir(), "gasstation", "uploads", gfile.get_basename()])
            if not ensure_dir(original):
                return False

            new_gfile = Gio.File.new_for_path(original)
            gfile.copy(new_gfile, Gio.FileCopyFlags.OVERWRITE)
            trans.set_association_url(new_gfile.get_uri())
        dialog.destroy()

    @staticmethod
    def default_associate_handler_location_ok_cb(editable, ok_button):
        have_scheme = False
        text = editable.get_chars(0, -1)
        if text is not None and len(text) != 0:
            scheme = Uri(text).get_scheme()
            if scheme is not None and len(scheme) != 0:
                have_scheme = True
        ok_button.set_sensitive(have_scheme)

    def default_associate_handler_location(self, trans, have_uri):
        dialog = Gtk.Dialog(title="Associate Location with Transaction", transient_for=self.window,
                            destroy_with_parent=True, modal=True,
                            buttons=("_Remove", Gtk.ResponseType.REJECT, "_Cancel", Gtk.ResponseType.CANCEL))
        ok_button = dialog.add_button("_OK", Gtk.ResponseType.ACCEPT)
        ok_button.set_sensitive(False)
        content_area = dialog.get_content_area()

        entry = Gtk.Entry()
        entry.set_width_chars(80)
        entry.set_activates_default(True)
        entry.connect("changed", self.default_associate_handler_location_ok_cb, ok_button)
        if have_uri:
            label = Gtk.Label("Amend URL")
            entry.set_text(trans.get_association_url())
        else:
            label = Gtk.Label("Enter URL like https://www.gasstation.org")
        gs_label_set_alignment(label, 0.0, 0.5)
        content_area.add(label)
        content_area.add(entry)
        dialog.set_border_width(12)
        dialog.set_default_response(Gtk.ResponseType.ACCEPT)
        dialog.show_all()
        response = dialog.run()

        if response == Gtk.ResponseType.REJECT:
            trans.set_association_url("")
        elif response == Gtk.ResponseType.ACCEPT:
            dialog_uri = entry.get_text()
            trans.set_association_url(dialog_uri)
        dialog.destroy()

    @staticmethod
    def convert_associate_uri(trans):
        uri = trans.get_association_url()
        part = ""
        if uri is None or len(uri) == 0:
            return ""

        if uri.startswith("file:") and not uri.startswith("file://"):
            if uri.startswith("file:/") and not uri.startswith("file://"):
                part = uri + len("file:/")
            elif uri.startswith("file:") and not uri.startswith("file://"):
                part = uri + len("file:")

            if part:
                trans.set_association_url(part)
                return part
        return Uri(uri)

    def default_association_handler(self, uri_is_file):
        reg = self.ledger().get_view()
        split = reg.get_current_split()
        have_uri = False
        if split is None:
            return
        trans = split.get_transaction()
        uri = self.convert_associate_uri(trans)
        if uri is not None and len(uri) != 0:
            have_uri = True

        if uri_is_file:
            self.associate_handler_file(trans, have_uri)
        else:
            self.default_associate_handler_location(trans, have_uri)

    def default_execassociated_handler(self, data):
        reg = self.ledger().get_view()
        split = reg.get_current_split()
        if split is None:
            return
        trans = split.get_transaction()
        if trans is None:
            show_error(self.window, "This transaction is not associated with a URI.")
            return
        uri = self.convert_associate_uri(trans)
        if uri is None or len(uri) == 0:
            show_error(self.window, "This transaction is not associated with a URI.")
        else:
            run_uri = None
            scheme = Uri(uri).get_scheme()
            if scheme is None or len(scheme) == 0:
                path_head = gs_pref_get_string(PREFS_GROUP_GENERAL, "assoc-head")
                if path_head and len(path_head) != 0:
                    file_path = os.path.abspath(Uri(path_head).get_path())
                else:
                    file_path = os.path.abspath(uri)
                run_uri = Uri(file_path)

            if run_uri is None:
                run_uri = uri

            uri_scheme = run_uri.get_scheme()
            if uri_scheme is not None:
                self.open_file_association(self.window, str(run_uri))
            else:
                show_error(self.window, "This transaction is not associated with a valid URI.")

    @staticmethod
    def open_file_association(widget, url):
        Gtk.show_uri_on_window(widget, url, Gdk.CURRENT_TIME)

    def disconnect_all(self):
        for k, v in self.reference_ids.items():
            for v1 in v:
                k.disconnect(v1)
        self.reference_ids.clear()

    def __del__(self):
        self.reference_ids = None
