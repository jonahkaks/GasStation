import decimal
from typing import Tuple

from gasstation.utilities.warnings import *

from gasstation.models.register import *
from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.ui import *
from gasstation.views.dialogs.account.transfer import TransferDialog
from gasstation.views.tree import *
from libgasstation.core.helpers import *
from libgasstation.core.price_db import Price, PriceDB, PriceSource, PriceType
from libgasstation.core.scrub import Scrub

SPLIT_TRANS_STR = "-- Split Transaction --"
STOCK_SPLIT_STR = "-- Stock Split --"

PINKCELL = "#F8BEC6"
REDCELL = "#F34943"
BLUECELL = "#1D80DF"
BLACKCELL = "#CBCBD2"
YELLOWCELL = "#FFEF98"
ORANGECELL = "#F39536"

PREF_SHOW_EXTRA_DATES = "show-extra-dates"
PREF_SHOW_EXTRA_DATES_ON_SEL = "show-extra-dates-on-selection"
PREF_SHOW_CAL_BUTTONS = "show-calendar-buttons"
PREF_SEL_TO_BLANK_ON_EXPAND = "selection-to-blank-on-expand"
PREF_KEY_LENGTH = "key-length"
SHOW_SYMBOL = False


class ViewCol(GObject.GEnum):
    END_OF_LIST = -1
    CONTROL = 0
    DATE = 1
    DUE_DATE = 2
    NUM_ACTION = 3
    DESC_NOTES = 4
    TRANSFER_VOID = 5
    RECONCILE = 6
    TYPE = 7
    SHARES = 8
    VALUE = 9
    AMOUNT = 10
    RATE = 11
    PRICE = 12
    DEBIT = 13
    CREDIT = 14
    BALANCE = 15
    STATUS = 16
    COMM = 17


class RowDepth(GObject.GEnum):
    TOP = 0
    TRANS1 = 1
    TRANS2 = 2
    SPLIT3 = 3


class TransConfirm(GObject.GEnum):
    RESET = 0
    ACCEPT = 1
    DISCARD = 2
    CANCEL = 3


clipboard_trans = None
clipboard_acct = None


class RegisterView(TreeView):
    __gtype_name__ = "RegisterView"
    __gsignals__ = {
        'help_signal': (GObject.SignalFlags.RUN_LAST, None, (int,)),
        'update_signal': (GObject.SignalFlags.RUN_LAST, None, (int,))
    }

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_trans = None
        self.current_split = None
        self.current_depth = 0
        self.reg_closing = False
        self.current_ref = None
        self.change_allowed = False
        self.trans_confirm = TransConfirm.RESET
        self.set_has_tooltip(True)
        self.set_resize_mode(Gtk.ResizeMode.PARENT)
        self.set_hscroll_policy(Gtk.ScrollablePolicy.MINIMUM)
        self.dirty_trans = None
        self.temp_cr = None
        self.help_text = ""
        self.double_line = False
        self.expanded = False
        self.auto_complete = False
        self.private_window = None
        self.key_length = 0
        self.single_button_press = 0
        self.stop_cell_move = False
        self.uiupdate_cb = None
        self.uiupdate_cb_data = None
        self.transfer_string = ""
        self.editing_now = False
        self.handled_dc = False
        self.show_calendar_buttons = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_SHOW_CAL_BUTTONS)
        self.show_extra_dates = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_SHOW_EXTRA_DATES)
        self.show_extra_dates_on_selection = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                              PREF_SHOW_EXTRA_DATES_ON_SEL)
        self.selection_to_blank_on_expand = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                             PREF_SEL_TO_BLANK_ON_EXPAND)
        self.key_length = gs_pref_get_float(PREFS_GROUP_GENERAL_REGISTER, PREF_KEY_LENGTH)

        self.acct_short_names = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_SHOW_LEAF_ACCT_NAMES)
        self.negative_in_red = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED)
        self.use_horizontal_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_HOR_LINES)
        self.use_vertical_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_VERT_LINES)
        self.use_accounting_labels = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_ACCOUNTING_LABELS)
        gs_pref_register_cb(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_HOR_LINES, self.pref_changed)
        gs_pref_register_cb(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_VERT_LINES, self.pref_changed)
        self.freeze_child_notify()
        self.set_property("name", "split_reg_tree")
        self.anchor = model.get_anchor()
        self.reg_comm = self.anchor.get_commodity() if self.anchor is not None else None
        self.reg_currency, found = gs_account_or_default_currency(self.anchor)
        gs_widget_set_style_context(self, "register_grid_lines")
        if self.use_horizontal_lines:
            if self.use_vertical_lines:
                self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
            else:
                self.set_grid_lines(Gtk.TreeViewGridLines.HORIZONTAL)
        elif self.use_vertical_lines:
            self.set_grid_lines(Gtk.TreeViewGridLines.VERTICAL)
        else:
            self.set_grid_lines(Gtk.TreeViewGridLines.NONE)
        self.set_show_expanders(False)
        selection = self.get_selection()
        selection.unselect_all()
        s_model = Gtk.TreeModelSort(model)
        self.set_cols(model, self.get_column_list(model))
        s_model.set_sort_column_id(RegisterModelColumn.DATE, Gtk.SortType.ASCENDING)
        self.set_model(s_model)
        self.thaw_child_notify()

    @staticmethod
    def get_column_list(model):
        if model.type is None:
            return tuple(ViewCol.END_OF_LIST)
        if model.type in [RegisterModelType.BANK_REGISTER,
                          RegisterModelType.CASH_REGISTER,
                          RegisterModelType.ASSET_REGISTER,
                          RegisterModelType.CREDIT_REGISTER,
                          RegisterModelType.LIABILITY_REGISTER,
                          RegisterModelType.INCOME_REGISTER,
                          RegisterModelType.EXPENSE_REGISTER,
                          RegisterModelType.EQUITY_REGISTER,
                          RegisterModelType.TRADING_REGISTER,
                          RegisterModelType.INCOME_LEDGER]:
            return ViewCol.DATE, ViewCol.NUM_ACTION, ViewCol.DESC_NOTES, ViewCol.TRANSFER_VOID, ViewCol.RECONCILE, \
                   ViewCol.STATUS, ViewCol.DEBIT, ViewCol.CREDIT, ViewCol.BALANCE
        elif model.type == RegisterModelType.GENERAL_JOURNAL:
            return ViewCol.DATE, ViewCol.NUM_ACTION, ViewCol.DESC_NOTES, ViewCol.TRANSFER_VOID, ViewCol.RECONCILE, \
                   ViewCol.STATUS, ViewCol.COMM, ViewCol.VALUE, ViewCol.RATE, ViewCol.AMOUNT, \
                   ViewCol.DEBIT, ViewCol.CREDIT
        elif model.type in [RegisterModelType.STOCK_REGISTER, RegisterModelType.CURRENCY_REGISTER]:
            return ViewCol.DATE, ViewCol.NUM_ACTION, ViewCol.DESC_NOTES, ViewCol.TRANSFER_VOID, \
                   ViewCol.RECONCILE, ViewCol.STATUS, ViewCol.SHARES, ViewCol.PRICE, ViewCol.DEBIT, \
                   ViewCol.CREDIT, ViewCol.BALANCE

        elif model.type in [RegisterModelType.RECEIVABLE_REGISTER, RegisterModelType.PAYABLE_REGISTER]:
            return ViewCol.DATE, ViewCol.TYPE, ViewCol.DUE_DATE, ViewCol.NUM_ACTION, ViewCol.DESC_NOTES, \
                   ViewCol.TRANSFER_VOID, ViewCol.STATUS, ViewCol.DEBIT, ViewCol.CREDIT, ViewCol.BALANCE
        elif model.type == RegisterModelType.PORTFOLIO_LEDGER:
            return ViewCol.DATE, ViewCol.NUM_ACTION, ViewCol.DESC_NOTES, ViewCol.TRANSFER_VOID, ViewCol.RECONCILE, \
                   ViewCol.STATUS, ViewCol.AMOUNT, ViewCol.PRICE, ViewCol.DEBIT, ViewCol.CREDIT
        elif model.type == RegisterModelType.SEARCH_LEDGER:
            return ViewCol.DATE, ViewCol.NUM_ACTION, ViewCol.DESC_NOTES, ViewCol.TRANSFER_VOID, ViewCol.RECONCILE, \
                   ViewCol.STATUS, ViewCol.DEBIT, ViewCol.CREDIT
        else:
            return ViewCol.DATE, ViewCol.NUM_ACTION, ViewCol.DESC_NOTES, ViewCol.TRANSFER_VOID, ViewCol.RECONCILE, \
                   ViewCol.STATUS, ViewCol.VALUE, ViewCol.AMOUNT, ViewCol.RATE, ViewCol.PRICE, ViewCol.DEBIT, \
                   ViewCol.CREDIT, ViewCol.BALANCE

    def get_reg_commodity(self):
        return self.reg_comm

    def get_default_currency(self):
        return self.get_model_from_view().get_default_currency()

    def set_private_window(self, win):
        self.private_window = win

    def set_cols(self, model, col_list: Tuple[ViewCol]):
        reg_columns = {ViewCol.DATE: ColDef(ViewCol.DATE, RegisterModelColumn.DATE,
                                            "Date", "date", "00/00/0000",
                                            None, 1,
                                            self.edited_cb, self.editing_started, None),
                       ViewCol.DUE_DATE: ColDef(ViewCol.DUE_DATE, -1,
                                                "Due Date", "duedate", "00/00/0000",
                                                None, 1,
                                                self.edited_cb, self.editing_started,
                                                None),
                       ViewCol.NUM_ACTION: ColDef(ViewCol.NUM_ACTION, RegisterModelColumn.NUMACT,
                                                  "Num", "numact", "Buy",
                                                  None, 1,
                                                  self.edited_cb, self.editing_started, None),
                       ViewCol.DESC_NOTES: ColDef(ViewCol.DESC_NOTES,
                                                  RegisterModelColumn.DESCNOTES,
                                                  "Description / Notes / Memo", "descnotes",
                                                  "xxxxxxxxxxxxxxxxxxx",
                                                  None, 1,
                                                  self.edited_cb, self.editing_started,
                                                  None),
                       ViewCol.TRANSFER_VOID: ColDef(ViewCol.TRANSFER_VOID, -1,
                                                     "Transfer / Void", "transfervoid",
                                                     "Assets:Current Assets:MobileMoney",
                                                     None, 1,
                                                     self.edited_cb, self.editing_started,
                                                     None),

                       ViewCol.RECONCILE: ColDef(ViewCol.RECONCILE, RegisterModelColumn.RECN,
                                                 "R", "recn", "xx",
                                                 None, 1,
                                                 self.edited_cb, self.editing_started, None),

                       ViewCol.TYPE: ColDef(ViewCol.TYPE, -1,
                                            "Type", "type", "xx",
                                            None, 1,
                                            self.edited_cb, self.editing_started, None),

                       ViewCol.VALUE: ColDef(ViewCol.VALUE, -1,
                                             "Value", "value", "999,999.000",
                                             None, 0,
                                             self.edited_cb, self.editing_started, None),
                       ViewCol.SHARES: ColDef(ViewCol.SHARES, -1,
                                              "Shares", "shares", "999,999.000",
                                              None, 1,
                                              self.edited_cb, self.editing_started, None),

                       ViewCol.AMOUNT: ColDef(ViewCol.AMOUNT, -1,
                                              "Amount", "amount", "999,999.000",
                                              None, 0,
                                              self.edited_cb, self.editing_started, None),
                       ViewCol.RATE: ColDef(ViewCol.RATE, -1,
                                            "Rate", "rate", "999,999.000",
                                            None, 0,
                                            self.edited_cb, self.editing_started, None),

                       ViewCol.PRICE: ColDef(ViewCol.PRICE, -1,
                                             "Price", "price", "999,999.000",
                                             None, 1,
                                             self.edited_cb, self.editing_started, None),

                       ViewCol.DEBIT: ColDef(ViewCol.DEBIT, RegisterModelColumn.DEBIT,
                                             "Debit", "debit", "1,000,000.00",
                                             None, 1,
                                             self.edited_cb, self.editing_started, None),

                       ViewCol.CREDIT: ColDef(ViewCol.CREDIT, RegisterModelColumn.CREDIT,
                                              "Credit", "credit", "1,000,000.00",
                                              None, 1,
                                              self.edited_cb, self.editing_started, None),

                       ViewCol.BALANCE: ColDef(ViewCol.BALANCE, -1,
                                               "Balance", "balance", "999,999.000",
                                               None, 1,
                                               None, None, None),

                       ViewCol.STATUS: ColDef(ViewCol.STATUS, -1,
                                              " ", "status", "x",
                                              None, 1,
                                              None, None, None),

                       ViewCol.COMM: ColDef(ViewCol.COMM, -1,
                                            "Commodity", "commodity", "xxxxxxxx",
                                            None, 0,
                                            None, None, None)}
        for c in col_list:
            df = reg_columns[c]
            kwargs = {"xalign": 1.0}
            self.set_control_column_background(0, self.control_cdf0)
            if df.editing_started_cb is not None:
                kwargs["editing_started"] = df.editing_started_cb
            kwargs["editing_canceled"] = self.editing_canceled_cb
            kwargs["resizable"] = True
            kwargs["reorderable"] = True
            if c == ViewCol.STATUS:
                kwargs[REAL_TITLE] = "Status Bar"
                kwargs["resizable"] = False
                kwargs["fixed_width"] = 5
                kwargs["sizing"] = Gtk.TreeViewColumnSizing.FIXED
            if df.edited_cb:
                kwargs["editable"] = True
                kwargs["edited"] = df.edited_cb
            kwargs["data_func"] = self.cdf0

            if c == ViewCol.DATE:
                col = self.add_date_column(ViewCol.DATE, title=df.title, pref_name=df.pref_name,
                                           sizing_text=df.sizer, data_col=df.modelcol,
                                           visible_always=df.always_visible_col, visible_default=0,
                                           visible_col=df.visibility_model_col, **kwargs)
                col.set_sort_column_id(c)
            else:
                self.add_text_column(c, title=df.title, pref_name=df.pref_name,
                                     sizing_text=df.sizer, data_col=df.modelcol,
                                     visible_always=df.always_visible_col, visible_default=0,
                                     visible_col=df.visibility_model_col, **kwargs)

        selection = self.get_selection()
        selection.set_mode(Gtk.SelectionMode.SINGLE)
        self.reference_ids[selection].append(selection.connect("changed", self.motion_cb))
        self.set_user_data(self, "data-edited", False)
        self.reference_ids[model].append(model.connect("selection_move_delete", self.selection_move_delete_cb))
        self.reference_ids[model].append(model.connect("refresh_view", self.refresh_view_cb))
        self.reference_ids[self].append(self.connect("key-press-event", self.key_press_cb))
        self.reference_ids[self].append(self.connect("button_press_event", self.button_press_cb))

    def have_account(self, depth, expanded, is_template, trans, split):
        ha = False
        if depth == RowDepth.TRANS1 and not expanded and not self.is_multi(split):
            if split is None:
                split = trans.get_split(0)
            if split is not None and split.get_other_split() is not None:
                ha = True
        if depth == RowDepth.SPLIT3 and len(trans) == 0:
            ha = True
        if depth == RowDepth.SPLIT3:
            if not is_template and split is not None:
                acc = split.get_account()
                if acc is not None:
                    if acc.get_type() != AccountType.TRADING:
                        ha = True
                    else:
                        ha = False
            else:
                if self.template_get_transfer_entry(split) is not None:
                    ha = True
        return ha

    def control_cdf0(self, col, cell, s_model, s_iter, user_data):
        model = self.get_model_from_view()
        m_iter = s_model.convert_iter_to_child_iter(s_iter)
        is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
        mpath = model.get_path(m_iter)
        indices = mpath.get_indices()
        row_color = model.get_row_color(is_trow1, is_trow2, is_split, indices[0])
        if row_color is not None:
            cell.set_property("cell-background", row_color)
            cell.set_property("foreground", "black")
        else:
            cell.set_property("cell-background-set", 0)
            cell.set_property("foreground-set", 0)

    def cdf0(self, _2, cell, s_model, s_iter, _):
        editable = True
        open_edited = False
        show_extra_dates = False

        anchor = self.anchor
        model = self.get_model_from_view()
        m_iter = s_model.convert_iter_to_child_iter(s_iter)
        viewcol = self.get_user_data(cell, "view_column")
        is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
        spath = s_model.get_path(s_iter)
        if spath is None:
            return
        depth = spath.get_depth()
        indices = spath.get_indices()
        row_color = model.get_row_color(is_trow1, is_trow2, is_split, indices[0])
        if is_trow1 or is_trow2:
            if is_trow1:
                spath.down()
            expanded = self.row_expanded(spath)
        else:
            expanded = True
        if row_color is not None:
            cell.set_property("cell-background", row_color)
            cell.set_property("foreground", "black")
        else:
            cell.set_property("cell-background-set", 0)
            cell.set_property("foreground-set", 0)

        read_only = model.get_value(m_iter, RegisterModelColumn.RO)
        if trans.is_open() and self.dirty_trans != trans:
            read_only = True
            open_edited = True

        if trans is None:
            _type = TransactionType.NONE
        else:
            _type = trans.get_type()
        if model.type == RegisterModelType.RECEIVABLE_REGISTER or model.type == RegisterModelType.PAYABLE_REGISTER:
            if (
                    _type == TransactionType.INVOICE or _type == TransactionType.NONE) and self.dirty_trans != trans and not is_blank:
                read_only = True

        is_template = model.get_template()
        negative_in_red = self.negative_in_red

        if viewcol == ViewCol.DATE:
            datebuff = ""
            if is_split:
                cell.set_property("cell-background-set", 0)
            if self.show_extra_dates:
                show_extra_dates = True

            if self.current_trans == trans and self.show_extra_dates_on_selection and self.show_extra_dates:
                show_extra_dates = True

            if is_trow1 and trans is not None:
                t = trans.get_post_date()
                if t == 0:
                    t = datetime.date.today()
                datebuff = print_datetime(t)
                editable = True

            elif is_trow2 and trans is not None and show_extra_dates:
                t = trans.get_enter_date()
                cell.set_property("cell-background", YELLOWCELL)
                if t == 0:
                    t = datetime.date.today()
                datebuff = print_datetime(t)
                editable = False

            elif is_split and split is not None and show_extra_dates:
                if split.get_reconcile_state() == SplitState.RECONCILED:
                    t = split.get_reconcile_date()
                    if t == 0:
                        t = datetime.date.today()
                    datebuff = print_datetime(t)
                editable = False
            else:
                editable = False

            if is_template and is_trow1:
                datebuff = " Scheduled "
                editable = False

            elif is_template and is_trow2 and show_extra_dates:
                editable = False

            elif is_template and is_split and show_extra_dates:
                editable = False

            editable = False if read_only else editable
            cell.set_property("use_buttons", self.show_calendar_buttons)
            cell.set_property("text", datebuff)
            cell.set_property("editable", editable)
            return

        elif viewcol == ViewCol.DUE_DATE:
            datebuff = ""
            if is_split:
                cell.set_property("cell-background-set", 0)

            if is_trow1 and trans is not None:
                if _type == TransactionType.INVOICE:
                    t = trans.get_due_date()
                    datebuff = print_datetime(t)
                else:
                    editable = False
            if read_only:
                editable = False
            cell.set_properties(text=datebuff, editable=editable)
            return

        elif viewcol == ViewCol.NUM_ACTION:
            s = ""
            if is_trow1:
                s = gs_get_num_action(trans, self.get_this_split(trans))
            elif is_trow2:
                if expanded:
                    if Session.get_current_book().use_split_action_for_num_field():
                        s = gs_get_action_num(trans, self.get_this_split(trans))
                    # read_only = True
                else:
                    if self.get_this_split(trans):
                        s = gs_get_action_num(trans, self.get_this_split(trans))
            elif is_split:
                s = gs_get_num_action(None, split)
            cell.set_properties(text=s or "", editable=not read_only, xalign=0.0)
            return

        elif viewcol == ViewCol.DESC_NOTES:
            if is_trow1 and trans is not None:
                s = trans.get_description()

            elif is_trow2 and trans is not None:
                s = trans.get_notes()
            elif is_split and split is not None:
                s = split.get_memo()
            else:
                s = ""
            cell.set_properties(text=s or "", editable=not read_only, xalign=0.0)
            return

        elif viewcol == ViewCol.TRANSFER_VOID:
            string = ""
            if is_trow1:
                if expanded:
                    editable = False
                else:
                    string, ismulti = self.get_transfer_entry(self.get_this_split(trans))
                    editable = anchor is not None and not expanded and not ismulti
                holder = "Select Account"
            if is_trow2:
                string = trans.get_void_reason() or ""
                editable = False
                holder = "Void Reason"
            if is_split:
                if not is_template:
                    acct = split.get_account() if split is not None else None
                    if len(trans) == 0 and anchor is not None:
                        acct = anchor
                    if acct is not None:
                        if self.acct_short_names:
                            string = acct.get_name()
                        else:
                            string = acct.get_full_name()
                    else:
                        string = ""
                    if anchor == acct and model.type != RegisterModelType.GENERAL_JOURNAL \
                            and model.type != RegisterModelType.SEARCH_LEDGER:
                        editable = False
                    else:
                        editable = True
                else:
                    string = self.template_get_transfer_entry(split)
                    editable = True
                holder = "Split Account"
            if read_only:
                editable = False
            cell.set_properties(text=string, editable=editable)
            return

        elif viewcol == ViewCol.RECONCILE:
            s = ""
            if is_trow1 and not expanded:
                this_split = self.get_this_split(trans)

                if this_split is not None:
                    rec = this_split.get_reconcile_state()
                    if rec is not None:
                        s = rec.value
                    else:
                        s = "n"

            if is_split and split is not None:
                rec = split.get_reconcile_state()
                if rec is not None:
                    s = rec.value
                else:
                    s = SplitState.UNRECONCILED.value
            cell.set_properties(text=s, editable=False, xalign=0.5)
            return

        elif viewcol == ViewCol.TYPE:
            s = ""
            if is_split:
                cell.set_property("cell-background-set", 0)

            if is_trow1:
                s = "?"
                if _type is not None and _type != TransactionType.NONE:
                    s = str(_type)
                cell.set_property("text", s)
            cell.set_properties(text=s, editable=False, xalign=0.5)
            return

        elif viewcol == ViewCol.VALUE:
            num = Decimal(0)
            if is_split:
                num = split.get_value()
                s = sprintamount(num, PrintAmountInfo.commodity(trans.get_currency(), SHOW_SYMBOL))
                editable = False
                if self.get_imbalance(trans):
                    cell.set_property("cell-background", PINKCELL)
            else:
                s = ""
                editable = False
            editable = False if read_only else editable
            if num < 0 and negative_in_red:
                cell.set_property("foreground", "red")
            cell.set_properties(text=s, editable=editable, placeholder_text="Value" if is_blank else "")
            return

        elif viewcol == ViewCol.RATE:
            if is_trow1 or is_trow2:
                s = ""
                editable = False
            else:
                print_info = PrintAmountInfo.price(trans.get_currency())
                num = self.get_rate_for(trans, split, is_blank)
                s = sprintamount(num, print_info)
                editable = False
                if self.get_imbalance(trans):
                    cell.set_property("cell-background", PINKCELL)
            editable = False if read_only else editable
            cell.set_properties(text=s, editable=editable, placeholder_text="Rate" if is_blank else "")
            return

        elif viewcol == ViewCol.AMOUNT:
            s = ""
            num = Decimal(0)
            if is_split:
                if anchor is None:
                    num = split.get_amount() if split is not None else Decimal(0)
                    s = sprintamount(num, PrintAmountInfo.account(split.get_account(), SHOW_SYMBOL))
                    editable = False

                elif anchor is not None:
                    split_comm = split.get_account().get_commodity() if split.get_account() is not None else None
                    if (
                            split_comm is not None and not split_comm.is_currency()) or is_blank or model.type == RegisterModelType.GENERAL_JOURNAL:
                        num = split.get_amount()
                        s = sprintamount(num, PrintAmountInfo.account(split.get_account(), SHOW_SYMBOL))
                        editable = True
                if self.get_imbalance(trans):
                    cell.set_property("cell-background", PINKCELL)
            else:
                s = ""
                editable = False
            editable = False if read_only else editable
            if num < 0 and negative_in_red:
                cell.set_property("foreground", "red")
            cell.set_properties(text=s, editable=editable, placeholder_text="Amount" if is_blank else "")
            return

        elif viewcol == ViewCol.SHARES:
            s = ""
            num = Decimal(0)
            if is_trow2:
                s = ""
            elif is_trow1:
                if anchor is not None:
                    num = trans.get_account_amount(anchor)
                    if expanded:
                        s = ""
                    else:
                        s = sprintamount(num, PrintAmountInfo.commodity(trans.get_currency(), SHOW_SYMBOL))
                else:
                    s = ""
            if is_split:
                if anchor is None:
                    num = split.get_amount()
                    s = sprintamount(num, PrintAmountInfo.account(split.get_account(), SHOW_SYMBOL))
                elif anchor is not None:
                    split_comm = split.account.get_commodity() if split.account is not None else None
                    if (split_comm is not None and not split_comm.is_currency()) or is_blank:
                        num = split.get_amount()
                        s = sprintamount(num, PrintAmountInfo.account(split.get_account(), SHOW_SYMBOL))

                if self.get_imbalance(trans):
                    cell.set_property("cell-background", PINKCELL)
            editable = self.have_account(depth, expanded, is_template, trans, split)
            editable = False if read_only else editable
            if num < 0 and negative_in_red:
                cell.set_property("foreground", "red")
            cell.set_properties(text=s, editable=editable)
            return

        elif viewcol == ViewCol.PRICE:
            num = Decimal(0)
            s = ""
            if is_trow2:
                s = ""
            elif is_trow1:
                if expanded:
                    s = ""
                else:
                    if anchor is not None:
                        this_split = self.get_this_split(trans)
                        if this_split is not None:
                            if self.is_multi(this_split):
                                num = Decimal(0)
                            else:
                                num = this_split.get_share_price()
                            s = sprintamount(num, PrintAmountInfo.split(this_split, SHOW_SYMBOL))
                        else:
                            s = ""

            if is_split:
                split_comm = split.account.get_commodity() if split.account is not None else None
                if (split_comm is not None and not split_comm.is_currency()) or is_blank:
                    num = split.get_share_price()
                    s = sprintamount(num, PrintAmountInfo.split(split, SHOW_SYMBOL))
                else:
                    s = ""

                if self.get_imbalance(trans):
                    cell.set_property("cell-background", PINKCELL)
            editable = self.have_account(depth, expanded, is_template, trans, split)
            editable = False if read_only else editable
            if num < 0 and negative_in_red:
                cell.set_property("foreground", "red")
            cell.set_properties(text=s, editable=editable, placeholder_text="Price" if is_blank else "")
            return

        elif viewcol == ViewCol.DEBIT or viewcol == ViewCol.CREDIT:
            if not is_template:
                num = Decimal(0)
                if is_split:
                    try:
                        num, print_info = self.get_debcred_entry(trans, split, is_blank)
                    except ValueError:
                        num = Decimal(0)
                    if not trans.is_balanced():
                        cell.set_property("cell-background", PINKCELL)
                elif is_trow1:
                    if anchor is not None:
                        num = trans.get_account_amount(anchor)
                    else:
                        num = Decimal(0)
                elif is_trow2:
                    num = Decimal(0)
                if num.is_zero() or (
                        num < 0 and viewcol == ViewCol.DEBIT) \
                        or (num > 0 and viewcol == ViewCol.CREDIT):
                    s = ""
                else:
                    if (is_trow1 or is_trow2) and expanded:
                        s = ""
                    else:
                        s = gs_account_print_amount(abs(num), PrintAmountInfo.account(anchor, SHOW_SYMBOL))
            else:
                if is_trow1 or is_trow2:
                    s = ""
                elif is_split and viewcol == ViewCol.DEBIT:
                    s = self.template_get_fdebt_entry(split)
                else:
                    s = self.template_get_fcred_entry(split)
            editable = self.have_account(depth, expanded, is_template, trans, split)
            if read_only:
                editable = False
            cell.set_properties(text=s, editable=editable)
            return

        elif viewcol == ViewCol.BALANCE:
            if is_split:
                cell.set_property("cell-background-set", 0)
            if is_trow1 and anchor is not None:
                num = Decimal(trans.get_account_balance(anchor))
                s = gs_account_print_amount(num.__abs__(), PrintAmountInfo.account(anchor, False))
                if num < 0 and negative_in_red:
                    cell.set_property("foreground", "red")
            else:
                s = ""
            cell.set_properties(text=s, editable=False)
            return

        elif viewcol == ViewCol.STATUS:
            if read_only and not open_edited:
                cell.set_property("cell-background", REDCELL)
            elif read_only and open_edited:
                cell.set_property("cell-background", ORANGECELL)
            elif trans.in_future_by_post_date():
                cell.set_property("cell-background", BLUECELL)
            return

        elif viewcol == ViewCol.COMM:
            if is_split and split is not None and trans is not None:
                sa = split.get_account()
                split_com = sa.commodity if sa is not None else gs_default_currency()
                transaction_com = trans.get_currency()
                s = split_com.get_printname()
                if split_com == transaction_com:
                    s += "*"
                cell.set_properties(text=s, editable=False, placeholder_text="Currency" if is_blank else "")

    def get_credit_debit_string(self, credit):
        columns = self.get_columns()
        title = None
        for column in columns:
            renderers = column.get_cells()
            cr0 = renderers[0]
            viewcol = self.get_user_data(cr0, "view_column")
            if viewcol == ViewCol.CREDIT and credit:
                title = column.get_title()
            if viewcol == ViewCol.DEBIT and not credit:
                title = column.get_title()
        return title

    def cdf1(self, col, cell, s_model, s_iter, _):
        s = ""
        model = self.get_model_from_view()
        m_iter = s_model.convert_iter_to_child_iter(s_iter)
        viewcol = self.get_user_data(cell, "view_column")

        is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
        spath = s_model.get_path(s_iter)
        if spath is None:
            return
        # depth = spath.get_depth()
        indices = spath.get_indices()
        row_color = model.get_row_color(is_trow1, is_trow2, is_split, indices[0])

        if is_trow1 or is_trow2:
            if is_trow1:
                spath.down()
            expanded = self.row_expanded(spath)
        else:
            expanded = True

        cell.set_property("cell-background", row_color)
        read_only = model.get_value(m_iter, RegisterModelColumn.RO)
        if trans is None:
            _type = TransactionType.NONE
        else:
            _type = trans.get_type()
        if model.type in [RegisterModelType.PAYABLE_REGISTER, RegisterModelType.RECEIVABLE_REGISTER]:
            if (
                    _type == TransactionType.INVOICE or _type == TransactionType.NONE) and self.dirty_trans != trans and not is_blank:
                read_only = True

        if viewcol == ViewCol.NUM_ACTION:
            cell.set_property("xalign", 0.0)
            editable = True
            if is_trow1:
                s = gs_get_num_action(trans, self.get_this_split(trans))
            elif is_trow2 and expanded:
                if model.book.use_split_action_for_num_field():
                    s = gs_get_action_num(trans, self.get_this_split(trans))
                else:
                    s = ""
                editable = False

            elif is_trow2 and not expanded:
                if model.book.use_split_action_for_num_field():
                    if self.get_this_split(trans):
                        s = gs_get_action_num(trans, self.get_this_split(trans))
                    else:
                        s = ""

                else:
                    s = "XY"

            elif is_split:
                s = "XZ"

            if read_only:
                editable = False

            cell.set_property("text", s)
            cell.set_property("editable", editable)

    def motion_cb(self, sel):
        model = self.get_model_from_view()
        if model is None:
            return
        self.help_text = ""
        self.emit("help_signal", 0)
        t, m_iter = self.get_model_iter_from_selection(sel)
        if t:
            mpath = model.get_path(m_iter)
            spath = self.get_sort_path_from_model_path(mpath)
            self.set_current_path(mpath)
            depth = mpath.get_depth()
            is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
            if depth != self.current_depth:
                self.set_titles(depth)
            old_trans = self.current_trans
            model.set_blank_split_parent(trans, False)

            self.current_trans = trans
            self.current_split = split
            self.current_depth = depth
            model.current_trans = trans
            indices = spath.get_indices()
            model.current_row = indices[0]
            model.sync_scrollbar()
            if trans != old_trans and old_trans == self.dirty_trans:
                if self.transaction_changed():
                    return

            if self.trans_confirm == TransConfirm.CANCEL:
                return

            if old_trans != trans:
                if model.style != RegisterModelStyle.JOURNAL:
                    self.block_selection(True)
                    if self.trans_expanded(old_trans):
                        self.collapse_trans(old_trans)
                    self.block_selection(False)
                else:
                    self.expand_trans(None)

                if model.style == RegisterModelStyle.AUTO_LEDGER:
                    self.expand_row(spath, True)
                    self.expanded = True
                    if self.selection_to_blank_on_expand:
                        self.selection_to_blank()
            if self.trans_expanded(trans):
                self.expanded = True
            else:
                self.expanded = False

        else:
            self.set_titles(0)
            model.set_blank_split_parent(None, False)
            self.default_selection()
        self.call_uiupdate_cb()

    def default_selection(self):
        model = self.get_model_from_view()
        self.current_trans = model.current_trans
        if self.current_trans is None:
            btrans = self.get_blank_trans()
            mpath = model.get_path_to_split_and_trans(None, btrans)
            self.current_trans = btrans
        else:
            mpath = model.get_path_to_split_and_trans(self.current_split, self.current_trans)
        indices = mpath.get_indices()
        if self.current_depth == 2:
            new_mpath = Gtk.TreePath.new_from_indices([indices[0], indices[1]])
        else:
            new_mpath = Gtk.TreePath.new_from_indices([indices[0]])
        spath = self.get_sort_path_from_model_path(new_mpath)
        if spath is None:
            return
        if self.current_ref is not None:
            self.current_ref = None
        self.current_ref = Gtk.TreeRowReference.new(model, new_mpath)
        self.set_titles(self.current_depth)
        model.set_blank_split_parent(self.current_trans, False)
        self.set_format()
        self.view_scroll_to_cell()
        self.set_cursor(spath, None, False)

    def selection_move_delete_cb(self, _, trans):
        if trans == self.current_trans:
            self.goto_rel_trans_row(1)

    def refresh_view_cb(self, *args):
        self.set_format()

    def format_trans(self, trans):
        model = self.get_model_from_view()
        mpath = model.get_path_to_split_and_trans(None, trans)
        spath = self.get_sort_path_from_model_path(mpath)

        if model.style != RegisterModelStyle.JOURNAL:
            if model.use_double_line:
                self.expand_to_path(spath)
                spath.down()
                self.collapse_row(spath)
                spath.up()
            else:
                self.collapse_row(spath)
            self.expanded = False
        else:
            self.expand_row(spath, True)
            self.expanded = True
        self.call_uiupdate_cb()
        return False

    def collapse_trans(self, trans):
        model = self.get_model_from_view()
        self.finish_edit()
        if trans is None:
            mpath = self.current_ref.get_path()
        else:
            mpath = model.get_path_to_split_and_trans(None, trans)
        spath = self.get_sort_path_from_model_path(mpath)
        indices = spath.get_indices()
        depth = spath.get_depth()
        if model.use_double_line:
            temp_spath = Gtk.TreePath.new_from_indices([indices[0], 0])
        else:
            temp_spath = Gtk.TreePath.new_from_indices([indices[0]])

        if trans is None:
            self.block_selection(True)
            if model.use_double_line and depth == RowDepth.SPLIT3:
                self.get_selection().select_path(temp_spath)

            if not all([model.use_double_line, depth == RowDepth.TRANS1]):
                self.get_selection().select_path(temp_spath)
            self.collapse_row(temp_spath)
            t, miter = self.get_model_iter_from_selection(self.get_selection())
            if t:
                temp_mpath = model.get_path(miter)
                self.set_titles(temp_mpath.get_depth())
                self.set_current_path(temp_mpath)
            self.block_selection(False)
        else:
            self.collapse_row(temp_spath)
        self.expanded = False
        self.call_uiupdate_cb()

    def expand_trans(self, trans):
        model = self.get_model_from_view()
        self.finish_edit()
        if trans is None:
            mpath = self.current_ref.get_path()
        else:
            mpath = model.get_path_to_split_and_trans(None, trans)
        spath = self.get_sort_path_from_model_path(mpath)
        self.expand_row(spath, True)
        self.expanded = True
        if self.selection_to_blank_on_expand and model.style != RegisterModelStyle.JOURNAL:
            self.selection_to_blank()
        indices_spath = spath.get_indices()
        num_splits = len(self.current_trans)
        paths = self.get_visible_range()
        if paths is not None:
            indices_end = paths[1].get_indices()
            if model.use_double_line:
                lines = (indices_end[0] - indices_spath[0]) * 2
            else:
                lines = indices_end[0] - indices_spath[0]
            if (num_splits + 1) > lines:
                GLib.idle_add(self.scroll_to_bsplit)
        self.call_uiupdate_cb()

    def selection_to_blank(self):
        while Gtk.events_pending():
            Gtk.main_iteration()
        if not self.expanded:
            return False
        model = self.get_model_from_view()
        bsplit = model.get_blank_split()
        bpath = model.get_path_to_split_and_trans(bsplit, None)
        spath = self.get_sort_path_from_model_path(bpath)
        self.set_cursor(spath, None, False)
        return False

    def set_titles(self, depth):
        model = self.get_model_from_view()
        columns = self.get_columns()
        is_template = model.get_template()
        t = model.type
        for i, column in enumerate(columns):
            renderers = column.get_cells()
            cr0 = renderers[0]
            viewcol = self.get_user_data(cr0, "view_column")
            column.set_title("")
            if viewcol == ViewCol.DATE:
                if depth == RowDepth.TRANS1:
                    column.set_title("Date")
                elif depth == RowDepth.TRANS2:
                    column.set_title("Value Date")
                elif depth == RowDepth.SPLIT3:
                    column.set_title("Date Reconciled")
                else:
                    column.set_title("Date Posted / Entered / Reconciled")

            elif viewcol == ViewCol.DUE_DATE:
                if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                    column.set_title("Due Date")

            elif viewcol == ViewCol.NUM_ACTION:
                if t in (RegisterModelType.RECEIVABLE_REGISTER, RegisterModelType.PAYABLE_REGISTER):
                    if depth == RowDepth.TRANS1:
                        column.set_title("Ref")
                    elif depth == RowDepth.TRANS2:
                        column.set_title("Action")
                    elif depth == RowDepth.SPLIT3:
                        column.set_title("Action")
                    else:
                        column.set_title("Reference / Action")
                else:
                    if model.sort_depth == depth and model.sort_col == viewcol:
                        column.set_sort_indicator(True)
                    else:
                        column.set_sort_indicator(False)
                    if depth == RowDepth.TRANS1:
                        column.set_title("Num")
                    elif depth == RowDepth.TRANS2:
                        column.set_title("T-Number")
                    elif depth == RowDepth.SPLIT3:
                        column.set_title("Action")
                    else:
                        column.set_title("Number / Action")

            elif viewcol == ViewCol.DESC_NOTES:
                if t == RegisterModelType.RECEIVABLE_REGISTER:
                    if depth == RowDepth.TRANS1:
                        column.set_title("Customer")
                    elif depth == RowDepth.TRANS2:
                        column.set_title("Memo")
                    elif depth == RowDepth.SPLIT3:
                        column.set_title("Memo")
                    else:
                        column.set_title("Customer / Memo")

                elif t == RegisterModelType.PAYABLE_REGISTER:
                    if depth == RowDepth.TRANS1:
                        column.set_title("Supplier")
                    elif depth == RowDepth.TRANS2:
                        column.set_title("Memo")
                    elif depth == RowDepth.SPLIT3:
                        column.set_title("Memo")
                    else:
                        column.set_title("Supplier / Memo")

                else:
                    if model.sort_depth == depth and model.sort_col == viewcol:
                        column.set_sort_indicator(True)
                    else:
                        column.set_sort_indicator(False)
                    if depth == RowDepth.TRANS1:
                        column.set_title("Description")
                    elif depth == RowDepth.TRANS2:
                        column.set_title("Notes")
                    elif depth == RowDepth.SPLIT3:
                        column.set_title("Memo")
                    else:
                        column.set_title("Description / Notes / Memo")

            elif viewcol == ViewCol.TRANSFER_VOID:
                if t in [RegisterModelType.RECEIVABLE_REGISTER, RegisterModelType.PAYABLE_REGISTER]:
                    if depth == RowDepth.TRANS1:
                        column.set_title("Accounts")
                    elif depth == RowDepth.TRANS2:
                        column.set_title("Accounts")

                    elif depth == RowDepth.SPLIT3:
                        column.set_title("Accounts")
                    else:
                        column.set_title("Accounts")

                else:
                    if depth == RowDepth.TRANS1:
                        column.set_title("Transfer")
                    elif depth == RowDepth.TRANS2:
                        column.set_title("Void Reason")

                    elif depth == RowDepth.SPLIT3:
                        column.set_title("Accounts")
                    else:
                        column.set_title("Accounts / Void Reason")

            elif viewcol == ViewCol.RECONCILE:
                column.set_title("R")

            elif viewcol == ViewCol.TYPE:
                column.set_title("Type")

            elif viewcol == ViewCol.VALUE:
                column.set_title("Value")

            elif viewcol == ViewCol.AMOUNT:
                column.set_title("Amount")

            elif viewcol == ViewCol.SHARES:
                column.set_title("Shares")

            elif viewcol == ViewCol.RATE:
                column.set_title("Rate")

            elif viewcol == ViewCol.PRICE:
                column.set_title("Price")

            elif viewcol == ViewCol.COMM:
                column.set_title("Commodity")

            elif viewcol == ViewCol.CREDIT:
                if not self.use_accounting_labels:
                    if t == RegisterModelType.BANK_REGISTER:
                        column.set_title("Withdraw")

                    elif t == RegisterModelType.CASH_REGISTER:
                        column.set_title("Spend")

                    elif t == RegisterModelType.ASSET_REGISTER:
                        column.set_title("Decrease")

                    elif t in (RegisterModelType.LIABILITY_REGISTER,
                               RegisterModelType.EQUITY_REGISTER,
                               RegisterModelType.TRADING_REGISTER):
                        column.set_title("Increase")

                    elif t == RegisterModelType.CREDIT_REGISTER:
                        column.set_title("Charge")

                    elif t == RegisterModelType.INCOME_REGISTER:
                        column.set_title("Income")

                    elif t == RegisterModelType.EXPENSE_REGISTER:
                        column.set_title("Refund")

                    elif t in [RegisterModelType.STOCK_REGISTER, RegisterModelType.CURRENCY_REGISTER,
                               RegisterModelType.PORTFOLIO_LEDGER]:
                        column.set_title("Sell")

                    elif t == RegisterModelType.RECEIVABLE_REGISTER:
                        column.set_title("Payment")

                    elif t == RegisterModelType.PAYABLE_REGISTER:
                        column.set_title("Bill")

                    elif t == RegisterModelType.GENERAL_JOURNAL:
                        if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                            column.set_title("Funds Out")

                    elif t == RegisterModelType.SEARCH_LEDGER:
                        if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                            if not is_template:
                                column.set_title("Funds Out")
                            else:
                                column.set_title("Credit Formula")
                else:
                    column.set_title("Credit")

            elif viewcol == ViewCol.DEBIT:
                if not self.use_accounting_labels:
                    if t == RegisterModelType.BANK_REGISTER:
                        column.set_title("Deposit")

                    elif t == RegisterModelType.CASH_REGISTER:
                        column.set_title("Receive")

                    elif t == RegisterModelType.ASSET_REGISTER:
                        column.set_title("Increase")

                    elif t in (RegisterModelType.LIABILITY_REGISTER,
                               RegisterModelType.EQUITY_REGISTER,
                               RegisterModelType.TRADING_REGISTER):
                        column.set_title("Decrease")

                    elif t == RegisterModelType.INCOME_REGISTER:
                        column.set_title("Charge")

                    elif t == RegisterModelType.EXPENSE_REGISTER:
                        column.set_title("Expense")

                    elif t in [RegisterModelType.STOCK_REGISTER, RegisterModelType.CURRENCY_REGISTER,
                               RegisterModelType.PORTFOLIO_LEDGER]:
                        column.set_title("Buy")

                    elif t == RegisterModelType.RECEIVABLE_REGISTER:
                        column.set_title("Invoice")

                    elif t in (RegisterModelType.PAYABLE_REGISTER,
                               RegisterModelType.CREDIT_REGISTER):
                        column.set_title("Payment")
                    elif t == RegisterModelType.GENERAL_JOURNAL:
                        if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                            column.set_title("Funds In")

                    elif t == RegisterModelType.SEARCH_LEDGER:
                        if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                            if not is_template:
                                column.set_title("Funds In")
                            else:
                                column.set_title("Debit Formula")
                else:
                    column.set_title("Debit")
            elif viewcol == ViewCol.BALANCE:
                column.set_title("Balance")

    def match_selected_cb(self, widget: Gtk.EntryCompletion, search_text, _iter):
        model = widget.get_model()
        col = widget.get_text_column()
        v = model.get_value(_iter, col)
        return v.lower().__contains__(search_text.lower())

    def editing_started(self, cr, editable, path_string):
        completion = Gtk.EntryCompletion()
        model = self.get_model_from_view()
        description_list = model.get_description_list()
        notes_list = model.get_notes_list()
        memo_list = model.get_memo_list()

        spath = Gtk.TreePath.new_from_string(path_string)
        indices = spath.get_indices()
        depth = spath.get_depth()
        viewcol = self.get_user_data(cr, "view_column")
        self.set_user_data(cr, "cell-editable", editable)
        self.reference_ids[editable].append(editable.connect("key-press-event", self.ed_key_press_cb))

        if viewcol == ViewCol.DATE:
            entry = editable
            self.reference_ids[entry].append(entry.connect("key-press-event", self.ed_key_press_cb))
            self.set_user_data(cr, "current-string", entry.get_text())
            self.reference_ids[editable].append(editable.connect("remove-widget", self.remove_edit_date))

        elif viewcol == ViewCol.TRANSFER_VOID:
            entry = editable
            self.reference_ids[entry].append(entry.connect("key-press-event", self.ed_key_press_cb))
            if self.stop_cell_move:
                entry.insert_text(self.transfer_string, 0)
                entry.set_position(-1)
            model.update_account_list()
            entry.set_completion(completion)
            completion.set_match_func(self.match_selected_cb)
            account_list = model.get_acct_list()
            completion.set_model(account_list)
            completion.set_popup_completion(True)
            if self.acct_short_names:
                completion.set_text_column(0)
            else:
                completion.set_text_column(1)
            completion.set_inline_completion(True)
            completion.set_popup_single_match(False)
            completion.set_inline_selection(True)
            completion.set_popup_set_width(True)
            completion.set_minimum_key_length(0)
            if entry.get_text() == "":
                entry.emit("changed")
            self.set_user_data(cr, "current-string", entry.get_text())
            self.reference_ids[editable].append(editable.connect("remove-widget", self.remove_edit_entry))

        elif viewcol == ViewCol.NUM_ACTION:
            if depth == RowDepth.TRANS1 or (depth == RowDepth.TRANS2 and model.book.use_split_action_for_num_field()):
                entry = editable
                self.reference_ids[editable].append(entry.connect("insert_text", self.num_insert_cb))

            elif depth == RowDepth.SPLIT3 or (
                    depth == RowDepth.TRANS2 and not model.book.use_split_action_for_num_field()):
                model.update_action_list()
                entry = editable
                entry.set_completion(completion)
                numact = model.get_action_list()
                completion.set_model(numact)
                completion.set_match_func(self.match_selected_cb)
                completion.set_popup_completion(True)
                completion.set_inline_completion(True)
                completion.set_popup_single_match(False)
                completion.set_inline_selection(True)
                completion.set_text_column(0)
                completion.set_popup_set_width(True)
                completion.set_minimum_key_length(0)
                if entry.get_text() == "":
                    entry.emit("changed")

            self.set_user_data(cr, "current-string", entry.get_text())

            self.reference_ids[editable].append(editable.connect("remove-widget", self.remove_edit_entry))

        elif viewcol == ViewCol.DESC_NOTES:
            entry = editable
            model.update_completion_lists()
            if depth == RowDepth.TRANS1:
                entry.set_completion(completion)
                completion.set_model(description_list)
                completion.set_text_column(0)

            elif depth == RowDepth.TRANS2:
                entry.set_completion(completion)
                completion.set_model(notes_list)
                completion.set_text_column(0)

            elif depth == RowDepth.SPLIT3:
                entry.set_completion(completion)
                completion.set_model(memo_list)
                completion.set_text_column(0)
            completion.set_popup_completion(True)
            completion.set_inline_selection(True)
            completion.set_inline_completion(True)
            completion.set_minimum_key_length(1)
            completion.set_match_func(self.match_selected_cb)
            self.set_user_data(cr, "current-string", entry.get_text())
            self.reference_ids[editable].append(editable.connect("remove-widget", self.remove_edit_entry))

        elif viewcol == ViewCol.RECONCILE:
            entry = editable

            self.reference_ids[entry].append(entry.connect("insert_text", self.recn_insert_cb))
            self.set_user_data(cr, "current-string", entry.get_text())
            self.reference_ids[editable].append(editable.connect("remove-widget", self.remove_edit_entry))

        elif viewcol == ViewCol.TYPE:
            entry = editable
            self.reference_ids[entry].append(entry.connect("insert_text", self.type_insert_cb))
            self.set_user_data(cr, "current-string", entry.get_text())
            self.reference_ids[editable].append(editable.connect("remove-widget", self.remove_edit_entry))

        else:
            entry = editable
            self.set_user_data(cr, "current-string", entry.get_text())
            self.reference_ids[editable].append(editable.connect("remove-widget", self.remove_edit_entry))

        is_trow1 = False
        is_trow2 = False
        is_split = False
        if depth == RowDepth.TRANS1:
            is_trow1 = True
        if depth == RowDepth.TRANS2:
            is_trow2 = True
        if depth == RowDepth.SPLIT3:
            is_split = True

        row_color = model.get_row_color(is_trow1, is_trow2, is_split, indices[0])
        color = Gdk.RGBA()
        if row_color is not None and Gdk.RGBA.parse(color, row_color):
            if entry is not None:
                entry.set_name("cellrenderer")
                context = entry.get_style_context()
                css_provider = Gtk.CssProvider()
                col_str = Gdk.RGBA.to_string(color)
                css = "#cellrenderer{\nbackground-image: image(%s); color:black;\n}" % col_str
                css_provider.load_from_data(bytes(css.encode()))
                context.add_provider(css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        self.temp_cr = cr
        self.editing_now = True
        self.help(cr, viewcol, depth)
        self.set_user_data(cr, "editing-canceled", False)

    def num_insert_cb(self, editable, text, length, position):
        leave_string = ""
        is_num = False
        accel = False
        number = 0
        model = self.get_model_from_view()
        account = model.get_anchor()
        depth = self.get_selected_row_depth()

        if depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
            return
        entered_string = editable.get_chars(0, -1)

        try:
            number = int(entered_string)
            is_num = True
        except ValueError:
            pass

        if is_num and number < 0:
            is_num = False
        uc = GLib.utf8_get_char(text)
        if uc in ['+', '=']:
            number += 1
            accel = True
        elif uc in ['_', '-']:
            number -= 1
            accel = True
        elif uc in ['}', ']']:
            number += 10
            accel = True
        elif uc in ['{', '[']:
            number -= 10
            accel = True

        if number < 0:
            number = 0

        if accel and not is_num and entered_string != "":
            accel = False
        if accel and entered_string == "":
            if account is not None:
                try:
                    number = int(account.get_last_num()) + 1
                except ValueError:
                    number = 1
            else:
                number = 1
            is_num = True
        if not accel:
            leave_string = entered_string + text
        if accel and is_num:
            buff = "%ld" % number
            if buff == "":
                leave_string = ""
            else:
                leave_string = buff
        editable.handler_block_by_func(self.num_insert_cb)
        editable.delete_text(0, -1)
        editable.set_position(0)

        if leave_string is not None:
            editable.insert_text(leave_string, position)
        editable.handler_unblock_by_func(self.num_insert_cb)
        editable.stop_emission_by_name("insert_text")

    def type_insert_cb(self, editable, text, length, position):
        type_flags = ["I", "P"]
        flags = "IP"

        this_flag = flags.find(text)
        if this_flag != -1:
            index = this_flag
            result = type_flags[index]
        else:
            result = "N"
            index = 0
        self.set_user_data(self.temp_cr, "current-flag", index)
        editable.handler_block_by_func(self.type_insert_cb)
        editable.delete_text(0, -1)
        editable.insert_text(result, position)
        editable.handler_unblock_by_func(self.type_insert_cb)
        editable.stop_emission_by_name("insert_text")

    def edited_cb(self, cell, path_string, new_text):
        self.grab_focus()
        old_text = self.get_user_data(cell, "current-string")
        if old_text == new_text:
            if not self.stop_cell_move:
                return
        model = self.get_model_from_view()
        if not model.get_template():
            self.edited_normal_cb(cell, path_string, new_text)
        else:
            self.edited_template_cb(cell, path_string, new_text)

    def edited_normal_cb(self, cell, path_string, new_text):
        m_iter = self.get_model_iter_from_view_string(path_string)
        viewcol = self.get_user_data(cell, "view_column")
        model = self.get_model_from_view()
        is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
        anchor = self.anchor
        if viewcol == ViewCol.DATE:
            self.begin_edit(trans)
            if is_trow1 and trans is not None and new_text != "":
                if scan_date(new_text):
                    day, month, year = scan_date(new_text)
                    trans.set_post_date(datetime.date(year=year, month=month, day=day))
        elif viewcol == ViewCol.NUM_ACTION:
            self.begin_edit(trans)
            if is_trow1:
                gs_set_num_action(trans, self.get_this_split(trans), new_text, None)
                if not Session.get_current_book().use_split_action_for_num_field():
                    if new_text.isdigit() and anchor is not None:
                        anchor.set_last_num(int(new_text))

            elif is_trow2:
                gs_set_num_action(trans, self.get_this_split(trans), None, new_text)
                if not Session.get_current_book().use_split_action_for_num_field():
                    if new_text.isdigit() and anchor is not None:
                        anchor.set_last_num(int(new_text))

            elif is_split:
                gs_set_num_action(None, split, None, new_text)

        elif viewcol == ViewCol.DESC_NOTES:
            self.begin_edit(trans)
            if is_trow1:
                trans.set_description(new_text)
                if not self.auto_complete:
                    self.auto_complete_cb(trans, new_text)
                    self.auto_complete = True
            elif is_trow2 and trans is not None:
                trans.set_notes(new_text)
            elif is_split and split is not None:
                split.set_memo(new_text)

        elif viewcol == ViewCol.RECONCILE:
            self.begin_edit(trans)
            recn = {"n": SplitState.UNRECONCILED, "c": SplitState.CLEARED}.get(new_text, SplitState.UNRECONCILED)
            if is_trow1:
                split = self.get_this_split(trans)
                split.set_reconcile_state(recn)
            elif is_split:
                split.set_reconcile_state(recn)

        elif viewcol == ViewCol.TYPE:
            self.begin_edit(trans)
            logging.info("Type edited new text is %s" % new_text)
            _type = TransactionType.NONE
            if new_text is not None:
                _type = TransactionType(new_text)

            if is_trow1:
                trans.set_type(_type)

        elif viewcol in [ViewCol.TRANSFER_VOID,
                         ViewCol.SHARES,
                         ViewCol.AMOUNT,
                         ViewCol.PRICE,
                         ViewCol.DEBIT,
                         ViewCol.CREDIT]:
            self.begin_edit(trans)
            osplit = None
            force = False
            inp = Decimal(0)
            valid_input = False
            input_used = False
            if not is_split and anchor is not None:
                try:
                    split, osplit = self.get_split_pair(trans)
                except ValueError:
                    return False
            if viewcol == ViewCol.TRANSFER_VOID:
                self.stop_cell_move = False
                acct = self.get_account_by_name(new_text)
                self.transfer_string = new_text
                if acct is None:
                    if osplit is not None:
                        osplit.destroy()
                    self.stop_cell_move = True
                    GLib.idle_add(self.idle_transfer)
                    return

                if is_split:
                    old_acct = split.get_account()
                    split.set_account(acct)
                    if old_acct is not None and old_acct.commodity != acct.commodity:
                        force = True
                else:
                    old_acct = osplit.get_account()
                    osplit.set_account(acct)
                    if old_acct is not None and old_acct.commodity != acct.get_commodity():
                        force = True

            else:
                try:
                    inp = parse_exp_decimal(new_text)
                    valid_input = True
                except ValueError:
                    return
            acct = split.get_account()
            if acct is None:
                if anchor is not None:
                    split.set_account(anchor)
                    acct = split.get_account()
                else:
                    return
            if trans.currency is None:
                trans.set_currency(self.reg_currency)
                if anchor is None:
                    trans.set_currency(gs_account_or_default_currency(split.get_account())[0])

            if not valid_input:
                inp = self.get_value_for(trans, split, is_blank)

            if viewcol == ViewCol.CREDIT:
                inp = inp * -1
            split.set_transaction(trans)
            if viewcol == ViewCol.SHARES:
                self.set_number_for_input(trans, split, inp, ViewCol.SHARES)
                input_used = True

            elif viewcol == ViewCol.AMOUNT:
                self.set_number_for_input(trans, split, inp, ViewCol.AMOUNT)
                input_used = True

            elif viewcol == ViewCol.PRICE:
                self.set_number_for_input(trans, split, inp, ViewCol.PRICE)
                input_used = True

            elif viewcol == ViewCol.CREDIT or viewcol == ViewCol.DEBIT:
                if not acct.get_commodity().is_currency():
                    self.set_number_for_input(trans, split, inp, viewcol)
                    input_used = True

            if not input_used:
                if acct.get_commodity().is_currency():
                    self.set_value_for(trans, split, inp, force)
                else:
                    self.set_value_for_amount(split, inp)

            if is_blank:
                GLib.idle_add(model.commit_blank)
                GLib.idle_add(self.view_scroll_to_cell)

            if osplit is not None:
                osplit.set_transaction(trans)
                if acct.get_commodity().is_currency():
                    self.set_value_for(trans, osplit, inp * -1, force)
                else:
                    self.set_value_for_amount(osplit, split.get_value() * -1)

    def edited_template_cb(self, cell, path_string, new_text):
        m_iter = self.get_model_iter_from_view_string(path_string)
        viewcol = self.get_user_data(cell, "view_column")
        model = self.get_model_from_view()
        anchor = self.anchor
        is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
        if viewcol == ViewCol.NUM_ACTION:
            self.begin_edit(trans)
            if is_trow1:
                gs_set_num_action(trans, self.get_this_split(trans), new_text, None)
                if not Session.get_current_book().use_split_action_for_num_field():
                    if str(new_text).isnumeric() and anchor is not None:
                        anchor.set_last_num(new_text)
            if is_trow2:
                gs_set_num_action(None, split, None, new_text)

        elif viewcol == ViewCol.DESC_NOTES:
            self.begin_edit(trans)
            if is_trow1:
                trans.set_description(new_text)
                if not self.auto_complete:
                    self.auto_complete_cb(trans, new_text)
                    self.auto_complete = True
            if is_trow2:
                trans.set_notes(new_text)
            if is_split:
                split.set_memo(new_text)

        elif viewcol == ViewCol.RECONCILE:
            self.begin_edit(trans)
            recn = {"n": SplitState.UNRECONCILED, "c": SplitState.CLEARED}.get(new_text, SplitState.UNRECONCILED)
            if is_trow1:
                split = self.get_this_split(trans)
                split.set_reconcile_state(recn)
            elif is_split:
                split.set_reconcile_state(recn)

        elif viewcol == ViewCol.TRANSFER_VOID or viewcol == ViewCol.DEBIT or viewcol == ViewCol.CREDIT:
            self.begin_edit(trans)
            if viewcol == ViewCol.TRANSFER_VOID:
                self.stop_cell_move = False
                acct = self.get_account_by_name(new_text)
                if acct is None:
                    self.stop_cell_move = True
                    GLib.idle_add(self.idle_transfer)
                    return
                if new_text != "":
                    self.transfer_string = new_text
                split.scheduled_account = acct
                template_acc = model.get_template_account()
                template_acc.insert_split(split)
            if trans.get_currency() is None:
                trans.set_currency(gs_account_or_default_currency(split.get_account())[0])

            if viewcol == ViewCol.DEBIT:
                new_value = parse_exp_decimal(new_text)
                split.scheduled_debit_formula = new_text
                split.scheduled_debit_numeric = new_value
                split.scheduled_credit_formula = None
                split.scheduled_credit_numeric = None

            if viewcol == ViewCol.CREDIT:
                new_value = parse_exp_decimal(new_text)
                split.scheduled_debit_formula = None
                split.scheduled_debit_numeric = None
                split.scheduled_credit_formula = new_text
                split.scheduled_credit_numeric = new_value

            split.set_value(Decimal(0))
            split.set_transaction(trans)
            if is_blank:
                GLib.idle_add(model.commit_blank)
                GLib.idle_add(self.view_scroll_to_cell)

    def re_init_trans(self):
        self.finish_edit()
        trans = self.current_trans
        self.goto_rel_trans_row(0)
        depth = self.current_depth

        if trans is not None and depth != RowDepth.SPLIT3:
            if not trans.is_open():
                trans.begin_edit()
            self.set_dirty_trans(trans)
            for sp in trans.get_splits():
                try:
                    trans.get_rate_for_commodity(self.reg_comm, sp)
                    sp.destroy()
                except ConversionError:
                    continue



    def get_this_split(self, trans):
        model = self.get_model_from_view()
        anchor = model.get_anchor()
        if len(trans.splits) == 0:
            if model.is_blank_split_parent(trans):
                return model.get_blank_split()
        for split in trans.get_splits():
            if anchor == split.get_account():
                return split

    def get_current_trans(self):
        return self.current_trans

    def get_current_split(self):
        return self.current_split

    def get_selected_row_depth(self):
        return self.current_depth

    def get_dirty_trans(self):
        return self.dirty_trans

    def set_dirty_trans(self, trans):
        if trans is None:
            self.set_user_data(self, "data-edited", False)
            self.dirty_trans = None
        else:
            self.set_user_data(self, "data-edited", True)
            self.dirty_trans = trans

    def set_current_path(self, mpath):
        if mpath is None: return
        model = self.get_model_from_view()
        if self.current_ref:
            self.current_ref = None
        self.current_ref = Gtk.TreeRowReference.new(model, mpath)

    def get_current_path(self):
        if self.current_ref is None:
            return None
        return self.current_ref.get_path()

    def trans_expanded(self, trans):
        expanded = False
        if trans is None:
            expanded = self.expanded
        else:
            model = self.get_model_from_view()
            mpath = model.get_path_to_split_and_trans(None, trans)
            spath = self.get_sort_path_from_model_path(mpath)
            if spath:
                spath.down()
                expanded = self.row_expanded(spath)
        return expanded

    def cancel_edit(self, _):
        self.finish_edit()
        model = self.get_model_from_view()
        trans = self.dirty_trans
        if trans is not None and trans.is_open():
            self.goto_rel_trans_row(0)
            model.set_blank_split_parent(trans, True)
            self.set_user_data(self, "data-edited", False)
            self.dirty_trans.rollback_edit()
            model.set_blank_split_parent(trans, False)
            self.format_trans(self.dirty_trans)
            split = model.get_blank_split()
            split.reinit()
        self.dirty_trans = None
        self.change_allowed = False
        self.auto_complete = False
        self.call_uiupdate_cb()

    def delete_current_trans(self):
        self.finish_edit()
        trans = self.current_trans
        self.goto_rel_trans_row(1)
        if not trans.is_open():
            trans.begin_edit()
        self.set_dirty_trans(trans)
        trans.destroy()
        trans.commit_edit()
        self.set_dirty_trans(None)

    def delete_current_split(self):
        self.finish_edit()
        trans = self.current_trans
        split = self.current_split
        if not trans.is_open():
            trans.begin_edit()
        self.set_dirty_trans(trans)
        self.goto_rel_trans_row(0)
        split.destroy()
        trans.commit_edit()

    def enter(self):
        self.finish_edit()
        if self.transaction_changed():
            return False
        if self.trans_confirm == TransConfirm.DISCARD:
            return False
        return True

    def block_selection(self, block):
        sel = self.get_selection()
        if block:
            sel.handler_block_by_func(self.motion_cb)
        else:
            sel.handler_unblock_by_func(self.motion_cb)

    def get_parent(self):
        from gasstation.views.main_window import MainWindow
        return MainWindow.get_main_window(self)

    def set_format(self):
        model = self.get_model_from_view()
        total_num = model.iter_n_children(None)
        mpath = self.current_ref.get_path()
        spath = self.get_sort_path_from_model_path(mpath)
        self.expanded = False

        if model.style == RegisterModelStyle.JOURNAL:
            self.expand_all()
            self.expanded = True
            self.call_uiupdate_cb()
            return False
        if model.use_double_line:
            index = 0
            path = Gtk.TreePath.new_first()
            while index < total_num:
                self.expand_to_path(path)
                path.down()
                self.collapse_row(path)
                path.up()
                path.next()
                index += 1
        else:
            self.collapse_all()
            self.expanded = False

        if model.style == RegisterModelStyle.AUTO_LEDGER or model.style == RegisterModelStyle.JOURNAL:
            self.expand_row(spath, True)
            self.expanded = True
        self.call_uiupdate_cb()
        return False

    def set_read_only(self, read_only):
        model = self.get_model_from_view()
        model.read_only = read_only

    def recn_tests(self, column, spath):
        if column is None:
            return True
        renderers = column.get_cells()
        cr0 = renderers[0]
        viewcol = self.get_user_data(cr0, "view_column")
        if viewcol == ViewCol.RECONCILE:
            if not self.recn_change(spath):
                return True
        if viewcol != ViewCol.DESC_NOTES and viewcol != ViewCol.RECONCILE:
            if not self.recn_test(spath):
                return True
        return False

    @staticmethod
    def test_for_move(model, path):
        indices = path.get_indices()
        num_of_trans = model.iter_n_children(None)
        trans_pos = indices[0]
        if trans_pos < (num_of_trans * 1 / 3):
            model.move(RegisterModelUpdate.UP)
        if trans_pos > (num_of_trans * 2 / 3):
            model.move(RegisterModelUpdate.DOWN)

    def button_press_cb(self, _, evt):
        model = self.get_model_from_view()
        if evt.button == 1 and evt.type == Gdk.EventType.BUTTON_PRESS:
            window = self.get_bin_window()
            if evt.window != window:
                return False
            self.finish_edit()
            if self.stop_cell_move:
                return True
            x = self.get_path_at_pos(evt.x, evt.y)
            if x is not None:
                spath = x[0]
                col = x[1]
                mpath = self.get_model_path_from_sort_path(spath)
                if self.single_button_press > 0:
                    self.single_button_press -= 1
                    return True
                m_iter = model.get_iter(mpath)
                if m_iter is not None:
                    self.trans_confirm = TransConfirm.RESET
                    is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
                    if self.get_user_data(self, "data-edited") and self.transaction_changed_confirm(trans):
                        if self.trans_confirm == TransConfirm.CANCEL:
                            if len(self.dirty_trans) > 2 and self.dirty_trans is not None:
                                self.jump_to(None, self.dirty_trans.get_split(0), False)
                            else:
                                self.jump_to(self.dirty_trans, None, False)

                            return True
                        if self.trans_confirm == TransConfirm.DISCARD:
                            self.set_dirty_trans(None)

                    if self.current_trans != trans:
                        self.change_allowed = False

                    if self.recn_tests(col, spath):
                        return True
                    if is_split:
                        rotate_split = split
                    else:
                        rotate_split = self.get_this_split(trans)

                    if not self.rotate(col, trans, rotate_split):
                        self.set_cursor(spath, col, True)
                    else:
                        self.set_cursor(spath, col)

                    # self.test_for_move(model, spath)
                    return True

        if evt.button == 1 and evt.type == Gdk.EventType.DOUBLE_BUTTON_PRESS:
            window = self.get_bin_window()
            if self.current_trans.get_readonly() and ask_yes_no(self.get_parent(),
                                                                message="Transaction is not Editable.\n "
                                                                        "Wound you like to edit it?",
                                                                title="Edit Transaction?"):
                self.current_trans.clear_readonly()
            if evt.window != window:
                return False
            if model.style != RegisterModelStyle.JOURNAL:
                self.single_button_press = 1
                if self.expanded:
                    self.collapse_trans(None)
                else:
                    self.expand_trans(None)

                self.call_uiupdate_cb()
            return True
        return False

    def transaction_changed(self):
        model = self.get_model_from_view()
        spath, col = self.get_cursor()
        if spath is None:
            return False
        if self.recn_tests(col, spath):
            return False
        self.trans_confirm = TransConfirm.RESET
        if self.get_user_data(self, "data-edited") and self.transaction_changed_confirm(None):
            if self.trans_confirm == TransConfirm.CANCEL:
                if self.dirty_trans is not None and len(self.dirty_trans) > 2:
                    self.jump_to(None, self.dirty_trans.get_split(0), False)
                else:
                    self.jump_to(self.dirty_trans, None, False)
                return True

            if self.trans_confirm == TransConfirm.DISCARD:
                self.block_selection(True)
                if self.trans_expanded(self.dirty_trans):
                    self.collapse_trans(self.dirty_trans)
                self.block_selection(False)
                model.set_blank_split_parent(self.dirty_trans, True)
                model.set_blank_split_parent(self.dirty_trans, False)
                self.format_trans(self.dirty_trans)
                self.dirty_trans = None
        return False

    def transaction_changed_confirm(self, new_trans):
        title = "Save the changed transaction?"
        message = "The current transaction has changed. Would you like to " \
                  "record the changes, or discard the changes?"
        if self.dirty_trans is None or self.dirty_trans == new_trans:
            return False
        model = self.get_model_from_view()
        if self.dirty_trans.use_trading_accounts():
            default_account = model.get_anchor()
            if default_account is not None:
                Scrub.trans_imbalance(self.dirty_trans, default_account.get_root(), None)
            else:
                root = Session.get_current_book().get_root_account()
                Scrub.trans_imbalance(self.dirty_trans, root, None)
        if self.balance_trans(self.dirty_trans):
            self.trans_confirm = TransConfirm.CANCEL
            return True
        dialog = Gtk.MessageDialog(transient_for=self.private_window,
                                   modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE, title=title)
        dialog.format_secondary_text("%s" % message)
        dialog.add_buttons("_Discard Changes", Gtk.ResponseType.REJECT,
                           "_Cancel", Gtk.ResponseType.CANCEL,
                           "_Record Changes", Gtk.ResponseType.ACCEPT)
        response = gs_dialog_run(dialog, PREF_WARN_REG_TRANS_MOD)
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            self.set_user_data(self, "data-edited", False)
            self.dirty_trans.commit_edit()
            split = model.get_blank_split()
            split.reinit()
            self.dirty_trans = None
            self.change_allowed = False
            self.auto_complete = False
            self.trans_confirm = TransConfirm.ACCEPT
            return False

        elif response == Gtk.ResponseType.REJECT:
            if self.dirty_trans is not None and self.dirty_trans.is_open():
                self.goto_rel_trans_row(0)
                self.set_user_data(self, "data-edited", False)
                split = model.get_blank_split()
                split.reinit()
                self.change_allowed = False
                self.auto_complete = False
                self.trans_confirm = TransConfirm.DISCARD
            return True

        elif response == Gtk.ResponseType.CANCEL:
            self.trans_confirm = TransConfirm.CANCEL
            return True
        else:
            return False

    def set_uiupdate_cb(self, cb, *args):
        self.uiupdate_cb = weakref.WeakMethod(cb)
        self.uiupdate_cb_data = args

    def call_uiupdate_cb(self):
        if self.uiupdate_cb is not None:
            self.uiupdate_cb()(*self.uiupdate_cb_data)
        return False

    def key_press_cb(self, _, evt):
        next_trans = True
        editing = False
        step_off = False
        trans_changed = False
        spath, col = self.get_cursor()
        if evt.type != Gdk.EventType.KEY_PRESS:
            return False

        if evt.keyval in [Gdk.KEY_plus, Gdk.KEY_minus, Gdk.KEY_KP_Add, Gdk.KEY_KP_Subtract]:
            return True

        elif evt.keyval in [Gdk.KEY_Up, Gdk.KEY_Down]:
            model = self.get_model_from_view()
            if evt.keyval == Gdk.KEY_Up:
                model.move(RegisterModelUpdate.UP)
            else:
                model.move(RegisterModelUpdate.DOWN)
            return False

        elif evt.keyval in [Gdk.KEY_KP_Page_Up, Gdk.KEY_KP_Page_Down]:
            model = self.get_model_from_view()
            start_path, end_path = self.get_visible_range()
            if start_path and end_path:
                start_indices = start_path.get_indices()
                end_indices = end_path.get_indices()
                num_of_trans = end_indices[0] - start_indices[0]
                if evt.keyval == Gdk.KEY_KP_Page_Up:
                    new_start = start_indices[0] - num_of_trans + 2
                    if new_start < 0:
                        new_start = 0
                    new_start_path = Gtk.TreePath.new_from_indices([new_start, -1])
                    self.scroll_to_cell(new_start_path, None, True, 0.0, 0.0)
                    self.set_cursor(new_start_path, col, False)
                    model.move(RegisterModelUpdate.UP)
                else:
                    total_num = model.iter_n_children(None)
                    new_end = end_indices[0] + num_of_trans - 1
                    if new_end > (total_num - 1):
                        new_end = total_num - 1
                    new_end_path = Gtk.TreePath.new_from_indices([new_end, -1])
                    if model.use_double_line:
                        new_end_path.down()
                        self.scroll_to_cell(new_end_path, None, True, 1.0, 0.0)
                        new_end_path.up()
                    else:
                        self.scroll_to_cell(new_end_path, None, True, 1.0, 0.0)
                        self.set_cursor(new_end_path, col, False)
                    model.move(RegisterModelUpdate.DOWN)
            return True

        elif evt.keyval in [Gdk.KEY_Home, Gdk.KEY_KP_Home, Gdk.KEY_End, Gdk.KEY_KP_End]:
            model = self.get_model_from_view()
            if evt.keyval == Gdk.KEY_Home or evt.keyval == Gdk.KEY_KP_Home:
                hetrans = model.get_first_trans()
            else:
                hetrans = model.get_blank_trans()
            model.current_trans = hetrans
            if not model.trans_is_in_view(hetrans):
                model.emit("refresh_trans", 0)
            else:
                self.jump_to(hetrans, None, False)
            return True
        elif evt.keyval in [Gdk.KEY_Return, Gdk.KEY_space]:
            if spath is None:
                return True
            if not self.recn_tests(col, spath):
                self.set_cursor(spath, col, True)
            return True

        elif evt.keyval == Gdk.KEY_KP_Enter:
            if spath is None:
                return True
            goto_blank = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_ENTER_MOVES_TO_END)
            model = self.get_model_from_view()
            btrans = model.get_blank_trans()
            ctrans = self.get_current_trans()
            self.finish_edit()
            if btrans == ctrans:
                next_trans = False
            if self.enter():
                if goto_blank:
                    GLib.idle_add(self.jump_to_blank)
                elif next_trans:
                    self.goto_rel_trans_row(1)
            return True

        elif evt.keyval in [Gdk.KEY_Tab, Gdk.KEY_ISO_Left_Tab, Gdk.KEY_KP_Tab]:
            if spath is None:
                return True
            if evt.state & Gdk.ModifierType.CONTROL_MASK:
                self.auto_complete = True
            self.finish_edit()
            if self.stop_cell_move:
                return True
            while not editing and not step_off:
                start_spath = spath.copy()
                start_indices = start_spath.get_indices()
                ncol = self.keynav(col, spath, evt)
                next_indices = spath.get_indices()
                if start_indices[0] != next_indices[0]:
                    if self.dirty_trans is not None:
                        trans_changed = True
                    self.change_allowed = False
                if self.path_is_valid(spath):
                    if self.recn_tests(col, spath):
                        return True
                if spath is None or not self.path_is_valid(spath) or trans_changed:
                    if self.transaction_changed():
                        return True
                    step_off = True
                if self.trans_confirm != TransConfirm.DISCARD:
                    self.set_cursor(spath, ncol, True)
                editing = self.get_editing(ncol)
            return True
        else:
            return False

    def scroll_to_bsplit(self):
        model = self.get_model_from_view()
        if model is None:
            return False
        bsplit = model.get_blank_split()
        bsplit_mpath = model.get_path_to_split_and_trans(bsplit, None)
        bsplit_spath = self.get_sort_path_from_model_path(bsplit_mpath)
        if bsplit_spath is not None:
            selection = self.get_selection()
            selection.select_path(bsplit_spath)
            self.scroll_to_cell(bsplit_spath, None, False, 1.0, 0.0)
        return False

    def view_scroll_to_cell(self):
        model = self.get_model_from_view()
        mpath = self.get_current_path()
        spath = self.get_sort_path_from_model_path(mpath)
        if spath is None:
            return
        if model.sort_direction == Gtk.SortType.DESCENDING:
            self.scroll_to_cell(spath, None, True, 0.5, 0.0)
        else:
            if model.use_double_line:
                spath.down()
            self.scroll_to_cell(spath, None, True, 1.0, 0.0)
        return False

    def begin_edit(self, trans):
        if trans != self.dirty_trans:
            t = trans.get_post_date()
            if not trans.is_open():
                trans.begin_edit()
            self.dirty_trans = trans
            if t == 0:
                t = datetime.date.today()
                trans.set_post_date(t)

    def get_split_pair(self, trans):
        count = len(trans)
        anchor = self.anchor
        book = Session.get_current_book()
        split = None
        model = self.get_model_from_view()
        if count == 0:
            split = model.get_blank_split()
            split.set_account(anchor)
            split.set_transaction(trans)
            osplit = Split(book)
            osplit.set_transaction(trans)
            return split, osplit
        else:
            first_split = trans.get_split(0)
            if self.is_multi(first_split):
                raise ValueError("IS CORRUPTED")
            else:
                for s in trans.splits:
                    if s.account == anchor:
                        split = s
                        break
                if split is None:
                    raise ValueError("No account split achored to transaction")
                else:
                    osplit = split.get_other_split()
                    if osplit is None:
                        osplit = Split(book)
                        osplit.set_dirty()
                        osplit.set_transaction(trans)
            return split, osplit

    def idle_transfer(self):
        spath = self.get_current_path()
        columns = self.get_columns()
        for tvc in columns:
            renderers = tvc.get_cells()
            cr0 = renderers[0]
            viewcol = self.get_user_data(cr0, "view_column")
            if viewcol == ViewCol.TRANSFER_VOID:
                self.set_cursor(spath, tvc, True)
        return False

    def refresh_from_prefs(self):
        model = self.get_model_from_view()
        model.use_gs_color_theme = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_USE_GASSTATION_COLOR_THEME)
        model.alt_colors_by_transaction = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_ALT_COLOR_BY_TRANS)
        self.show_calendar_buttons = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_SHOW_CAL_BUTTONS)
        self.show_extra_dates = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_SHOW_EXTRA_DATES)
        self.show_extra_dates_on_selection = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                              PREF_SHOW_EXTRA_DATES_ON_SEL)
        self.selection_to_blank_on_expand = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                             PREF_SEL_TO_BLANK_ON_EXPAND)
        self.key_length = gs_pref_get_float(PREFS_GROUP_GENERAL_REGISTER, PREF_KEY_LENGTH)

        self.acct_short_names = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_SHOW_LEAF_ACCT_NAMES)
        self.negative_in_red = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED)
        self.use_horizontal_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_HOR_LINES)
        self.use_vertical_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_DRAW_VERT_LINES)
        self.use_accounting_labels = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_ACCOUNTING_LABELS)

    def pref_changed(self, _, pref, *args):
        if pref.endswith(PREF_DRAW_HOR_LINES) or pref.endswith(PREF_DRAW_VERT_LINES):
            self.use_horizontal_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                         PREF_DRAW_HOR_LINES)

            self.use_vertical_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                       PREF_DRAW_VERT_LINES)

            if self.use_horizontal_lines:
                if self.use_vertical_lines:
                    self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
                else:
                    self.set_grid_lines(Gtk.TreeViewGridLines.HORIZONTAL)

            elif self.use_vertical_lines:
                self.set_grid_lines(Gtk.TreeViewGridLines.VERTICAL)
            else:
                self.set_grid_lines(Gtk.TreeViewGridLines.NONE)

    def finish_edit(self):
        super().finish_edit()
        while Gtk.events_pending():
            Gtk.main_iteration()

    def ed_key_press_cb(self, widget, event):
        next_trans = True
        auto_popped = False
        spath, col = self.get_cursor()
        if event.type != Gdk.EventType.KEY_PRESS:
            return False

        if event.keyval == Gdk.KEY_Up or event.keyval == Gdk.KEY_Down:
            if spath is None:
                return True
            toplevel = widget.get_toplevel()
            if isinstance(toplevel, Gtk.Window):
                window_group = toplevel.get_group()
                win_list = window_group.list_windows()
                if len(win_list) == 1 and win_list[0].get_visible():
                    auto_popped = True
            if auto_popped:
                return False
            model = self.get_model_from_view()
            self.finish_edit()
            if self.stop_cell_move:
                return True
            depth = spath.get_depth()
            if event.keyval == Gdk.KEY_Up:
                if depth == 1:
                    if spath.prev():
                        if self.row_expanded(spath):
                            spath.down()
                            if self.row_expanded(spath) and model.type == RegisterModelType.GENERAL_JOURNAL:
                                spath.down()
                                while self.path_is_valid(spath):
                                    spath.next()
                                spath.prev()

                elif not spath.prev() and depth > 1:
                    spath.up()

            elif self.row_expanded(spath):
                spath.down()
            else:
                spath.next()
                if not self.path_is_valid(spath) and depth > 2:
                    spath.prev()
                    spath.up()
                    spath.next()

                if not self.path_is_valid(spath) and depth > 1:
                    spath.prev()
                    spath.up()
                    spath.next()
            self.set_cursor(spath, col, True)

            if event.keyval == Gdk.KEY_Up:
                model.move(RegisterModelUpdate.UP)
            else:
                model.move(RegisterModelUpdate.DOWN)
            return True

        elif event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return:
            if spath is None:
                return True
            if self.stop_cell_move:
                return True
            goto_blank = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_ENTER_MOVES_TO_END)
            model = self.get_model_from_view()
            btrans = model.get_blank_trans()
            ctrans = self.get_current_trans()
            if btrans == ctrans:
                next_trans = False
            if self.enter():
                if goto_blank:
                    GLib.idle_add(self.jump_to_blank)
                elif next_trans:
                    self.goto_rel_trans_row(1)
            return True
        else:
            return False

    def editing_canceled_cb(self, renderer, *args):
        if self.get_user_data(self, "data-edited") is None:
            self.dirty_trans = None
        if self.stop_cell_move:
            self.stop_cell_move = False
            GLib.idle_add(self.idle_transfer)
        self.help_text = " "
        self.emit("help_signal", 0)
        self.set_user_data(renderer, "edit-canceled", True)

    def help(self, _, viewcol, depth):
        model = self.get_model_from_view()
        help_text = " "
        if viewcol == ViewCol.DATE:
            if depth == RowDepth.TRANS1:
                current_string = self.get_user_data(self.temp_cr, "current-string")
                date = self.parse_date(current_string)
                help_text = self.get_date_help(date)
            else:
                help_text = " "

        elif viewcol == ViewCol.DUE_DATE:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "Enter Due Date"

        elif viewcol == ViewCol.NUM_ACTION:
            if model.type == RegisterModelType.RECEIVABLE_REGISTER or model.type == RegisterModelType.PAYABLE_REGISTER:
                if depth == RowDepth.TRANS1:
                    help_text = "Enter the transaction reference, such as the invoice or check number"
                elif depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                    help_text = "Enter the type of transaction, or choose one from the list"
            else:
                if depth == RowDepth.TRANS1:
                    help_text = "Enter the transaction number, such as the check number"
                elif depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                    help_text = "Enter the type of transaction, or choose one from the list"

        elif viewcol == ViewCol.DESC_NOTES:
            if model.type == RegisterModelType.RECEIVABLE_REGISTER:
                if depth == RowDepth.TRANS1:
                    help_text = "Enter the name of the Customer"
                elif depth == RowDepth.TRANS2:
                    help_text = "Enter notes for the transaction"
                elif depth == RowDepth.SPLIT3:
                    help_text = "Enter a description of the split"

            elif model.type == RegisterModelType.PAYABLE_REGISTER:
                if depth == RowDepth.TRANS1:
                    help_text = "Enter the name of the Supplier"
                elif depth == RowDepth.TRANS2:
                    help_text = "Enter notes for the transaction"
                elif depth == RowDepth.SPLIT3:
                    help_text = "Enter a description of the split"
            else:
                if depth == RowDepth.TRANS1:
                    help_text = "Enter a description of the transaction"
                elif depth == RowDepth.TRANS2:
                    help_text = "Enter notes for the transaction"
                elif depth == RowDepth.SPLIT3:
                    help_text = "Enter a description of the split"

        elif viewcol == ViewCol.TRANSFER_VOID:
            if depth == RowDepth.TRANS1:
                help_text = "Enter the account to transfer from, or choose one from the list"
            elif depth == RowDepth.TRANS2:
                help_text = "Reason the transaction was voided"
            elif depth == RowDepth.SPLIT3:
                help_text = "Enter the account to transfer from, or choose one from the list"

        elif viewcol == ViewCol.RECONCILE:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "Enter the reconcile type"

        elif viewcol == ViewCol.TYPE:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "Enter the type of transaction"

        elif viewcol == ViewCol.VALUE:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "Enter the value of shares bought or sold"

        elif viewcol == ViewCol.SHARES:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "Enter the number of shares bought or sold"

        elif viewcol == ViewCol.COMM:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "* Indicates the transaction Commodity."

        elif viewcol == ViewCol.RATE:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "Enter the rate"

        elif viewcol == ViewCol.PRICE:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "Enter the effective share price"

        elif viewcol == ViewCol.CREDIT:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "Enter credit formula for real transaction"

        elif viewcol == ViewCol.DEBIT:
            if depth == RowDepth.TRANS1 or depth == RowDepth.TRANS2 or depth == RowDepth.SPLIT3:
                help_text = "Enter debit formula for real transaction"
        else:
            help_text = " "
        self.help_text = help_text
        self.emit("help_signal", 0)

    def goto_rel_trans_row(self, relative):
        model = self.get_model_from_view()
        mpath = self.get_current_path()
        spath = self.get_sort_path_from_model_path(mpath)
        indices = spath.get_indices()
        if model.sort_direction == Gtk.SortType.DESCENDING:
            sort_direction = -1
        else:
            sort_direction = 1
        new_spath = Gtk.TreePath.new_from_indices([indices[0] + (relative * sort_direction)])
        self.block_selection(True)
        self.get_selection().unselect_path(spath)
        if relative != 0:
            self.block_selection(False)
        self.set_cursor(new_spath, None, False)

        if relative == 0:
            self.block_selection(False)
            new_mpath = self.get_model_path_from_sort_path(new_spath)
            self.set_current_path(new_mpath)

    def control_enter(self):
        next_trans = True
        model = self.get_model_from_view()
        goto_blank = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_ENTER_MOVES_TO_END)
        btrans = model.get_blank_trans()
        ctrans = self.get_current_trans()
        if btrans == ctrans:
            next_trans = False
        if self.enter():
            if goto_blank:
                GLib.idle_add(self.jump_to_blank)
            elif next_trans:
                self.goto_rel_trans_row(1)

    def jump_to_blank(self):
        model = self.get_model_from_view()
        btrans = model.get_blank_trans()
        model.current_trans = btrans
        if not model.trans_is_in_view(btrans):
            model.emit("refresh_trans")
        else:
            mpath = model.get_path_to_split_and_trans(None, btrans)
            spath = self.get_sort_path_from_model_path(mpath)
            if spath is not None:
                self.set_cursor(spath, None, False)
                GLib.idle_add(self.view_scroll_to_cell)
        return False

    def jump_to(self, trans, split, amount):
        model = self.get_model_from_view()
        if split is not None:
            trans = None
        mpath = model.get_path_to_split_and_trans(split, trans)
        spath = self.get_sort_path_from_model_path(mpath)
        if spath is None:
            return

        if split is not None:
            self.expand_trans(split.get_transaction())
        if amount:
            columns = self.get_columns()
            for tvc in columns:
                renderers = tvc.get_cells()
                cr0 = renderers[0]
                viewcol = self.get_user_data(cr0, "view_column")

                if viewcol == ViewCol.DEBIT and split.get_value() > 0:
                    self.set_cursor(spath, tvc, True)
                if viewcol == ViewCol.CREDIT and split.get_value() < 0:
                    self.set_cursor(spath, tvc, True)
        else:
            self.set_cursor(spath, None, False)
        self.scroll_to_cell(spath, None, True, 0.5, 0.0)

    def trans_open_and_warn(self, trans):
        title = "Save Transaction before proceeding?"
        message = "The current transaction has been changed.\n Would you like to record the changes before " \
                  "proceeding, or cancel? "
        window = self.get_parent()
        dirty_trans = self.get_dirty_trans()
        if trans == dirty_trans:
            dialog = Gtk.MessageDialog(window, destroy_with_parent=True, message_type=Gtk.MessageType.QUESTION,
                                       buttons=Gtk.ButtonsType.CANCEL, message_format=title)

            dialog.format_secondary_text(message)
            dialog.add_button("_Record", Gtk.ResponseType.ACCEPT)
            response = gs_dialog_run(dialog, PREF_WARN_REG_TRANS_MOD)
            dialog.destroy()
            if response != Gtk.ResponseType.ACCEPT:
                return True
            trans.commit_edit()
            self.set_dirty_trans(None)
            return False
        else:
            return False

    def recn_change(self, spath):
        title = "Mark split as unreconciled?"
        message = "You are about to mark a reconciled split as unreconciled." \
                  " Doing so might make future reconciliation difficult. Continue with this change?"
        model = self.get_model_from_view()
        anchor = model.get_anchor()
        mpath = self.get_model_path_from_sort_path(spath)
        m_iter = model.get_iter(mpath)
        if m_iter is None:
            return False
        is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
        if is_trow1 or is_trow2:
            split = trans.find_split_by_account(anchor)
        rec = split.get_reconcile_state() if split is not None else SplitState.UNRECONCILED
        if rec != SplitState.RECONCILED:
            return True
        window = self.private_window
        dialog = Gtk.MessageDialog(window, destroy_with_parent=True, message_type=Gtk.MessageType.WARNING,
                                   buttons=Gtk.ButtonsType.CANCEL, message_format=title)

        dialog.format_secondary_text("%s" % message)
        dialog.add_button("_Unreconcile", Gtk.ResponseType.YES)
        response = gs_dialog_run(dialog, PREF_WARN_REG_RECD_SPLIT_UNREC)
        dialog.destroy()
        if response == Gtk.ResponseType.YES:
            rec = SplitState.UNRECONCILED
            trans = split.get_transaction()
            self.set_dirty_trans(trans)
            if not trans.is_open():
                trans.begin_edit()
            split.set_reconcile_state(rec)
            return True
        return False

    def recn_test(self, spath):
        if self.change_allowed:
            return True
        model = self.get_model_from_view()
        anchor = model.get_anchor()
        m_iter = model.get_iter(spath)
        if m_iter is None:
            return True
        is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
        if is_trow1 or is_trow2:
            split = trans.find_split_by_account(anchor)
        if split is None:
            return True
        recn = split.get_reconcile_state()

        if recn == SplitState.RECONCILED or trans.has_reconciled_splits():
            if recn == SplitState.RECONCILED:
                title = "Change reconciled split?"
                message = "You are about to change a reconciled split. Doing so might make " \
                          "future reconciliation difficultnot  Continue with this change?"
            else:
                title = "Change split linked to a reconciled split? "
                message = "You are about to change a split that is linked to a reconciled split. " \
                          "Doing so might make future reconciliation difficult,  Continue with this change?"

            window = self.private_window
            dialog = Gtk.MessageDialog(window, destroy_with_parent=True, message_type=Gtk.MessageType.WARNING,
                                       buttons=Gtk.ButtonsType.CANCEL, message_format=title)
            dialog.format_secondary_text(message)
            dialog.add_button("_Change split", Gtk.ResponseType.YES)
            response = gs_dialog_run(dialog, PREF_WARN_REG_RECD_SPLIT_MOD)
            dialog.destroy()
            if response != Gtk.ResponseType.YES:
                return False
        self.change_allowed = True
        return True

    def get_blank_trans(self):
        model = self.get_model_from_view()
        return model.get_blank_trans()

    def save(self, reg_closing):
        self.finish_edit()
        if reg_closing:
            self.reg_closing = True
        dirty_trans = self.get_dirty_trans()
        blank_trans = self.get_blank_trans()
        trans = self.get_current_trans()
        if trans is None:
            return False
        if not trans.is_open():
            return False

        if trans == dirty_trans:
            if trans != blank_trans:
                trans.commit_edit()
                self.set_dirty_trans(None)
                return True
            else:
                if len(trans) == 0:
                    title = "Not enough information for Blank Transaction?"
                    message = "The blank transaction does not have enough information " \
                              "to save it. Would you like to return to the transaction" \
                              " to update, or cancel the save?"
                    window = self.private_window
                    dialog = Gtk.MessageDialog(window,
                                               destroy_with_parent=True,
                                               message_type=Gtk.MessageType.QUESTION,
                                               buttons=Gtk.ButtonsType.CANCEL, message_format=title)
                    dialog.format_secondary_text(message)
                    dialog.add_button("_Return", Gtk.ResponseType.ACCEPT)

                    dialog.get_widget_for_response(Gtk.ResponseType.ACCEPT).grab_focus()
                    response = dialog.run()
                    dialog.destroy()
                    if response != Gtk.ResponseType.ACCEPT:
                        return True
                    return False
                trans.commit_edit()
                self.set_dirty_trans(None)
                return True
        return True

    def balance_trans(self, trans):
        title = "Rebalance Transaction"
        message = "The current transaction is not balanced."
        if trans.is_balanced():
            return False
        window = self.get_parent()
        model = self.get_model_from_view()
        if trans.use_trading_accounts():
            imbal_list = trans.get_imbalance()
            if not any(imbal_list):
                multi_currency = True
            else:
                imbal_mon = imbal_list[0]
                if len(imbal_list) < 2 and Commodity.equiv(imbal_mon.commodity, trans.get_currency()):
                    multi_currency = False
                else:
                    multi_currency = True
        else:
            multi_currency = False
        split = trans.get_split(0)
        other_split = Split.get_other_split(split) if split is not None else None
        if other_split is None or multi_currency:
            two_accounts = False
            other_account = None
        else:
            two_accounts = True
            other_account = other_split.get_account()
        default_account = model.get_anchor()
        if default_account == other_account:
            other_account = split.get_account()
        if default_account == other_account:
            two_accounts = False
        radio_list = ["Balance it manually", "Let GasStation Adjust split"]
        if model.type < RegisterModelType.NUM_SINGLE_REGISTER_TYPES and not multi_currency:
            radio_list.append("Adjust current account _split total")
            default_value = 2
            if two_accounts:
                radio_list.append("Adjust _other account split total")
                default_value = 3
        else:
            default_value = 0
        choice = choose_radio_option_dialog(window, title, message, "Rebalance", default_value, radio_list)
        root = default_account.get_root()
        if choice == 1:
            Scrub.trans_imbalance(trans, root, None)
        elif choice == 2:
            Scrub.trans_imbalance(trans, root, default_account)
        elif choice == 3:
            Scrub.trans_imbalance(trans, root, other_account)
        else:
            return True
        return False

    def move_current_entry_updown(self, move_up):
        return self._move_current_entry_updown(move_up, True)

    def is_current_movable_updown(self, move_up):
        return self._move_current_entry_updown(move_up, False)

    def _move_current_entry_updown(self, move_up, really_do_it):
        model = self.get_model_from_view()
        resultvalue = False
        if model is None: return False
        if model.sort_col != RegisterModelColumn.DATE: return False
        mpath = self.get_current_path()
        if mpath is None:
            return resultvalue
        spath = self.get_sort_path_from_model_path(mpath)
        if spath is None: return False
        spath_target = spath.copy()
        if move_up:
            move_was_made = spath_target.prev()
            if not move_was_made: return resultvalue
        else:
            spath_target.next()
        if Gtk.TreePath.compare(spath, spath_target) == 0:
            return resultvalue
        mpath_target = self.get_model_path_from_sort_path(spath_target)
        if mpath_target is None: return resultvalue
        m_iter = model.get_iter(mpath)
        if m_iter is None: return resultvalue
        m_iter_target = model.get_iter(mpath_target)
        if m_iter_target is None: return resultvalue
        is_trow1, is_trow2, is_split, is_blank, current_trans, current_split = model.get_split_and_trans(m_iter)
        is_trow1, is_trow2, is_split, is_blank_target, target_trans, target_split = model.get_split_and_trans(
            m_iter_target)
        if is_blank or is_blank_target:
            return resultvalue
        if current_trans.equal(target_trans, True, False, False, False):
            return resultvalue
        if current_trans.is_closing() or target_trans.is_closing():
            return resultvalue
        if current_trans.post_date != target_trans.post_date: return resultvalue
        if current_trans.num != target_trans.num: return resultvalue
        if current_split is None or current_split.reconcile_state == SplitState.FROZEN or target_split.reconcile_state == SplitState.FROZEN:
            return resultvalue

        if really_do_it:
            if current_split.reconcile_state == SplitState.RECONCILED and not self.recn_test(spath): return resultvalue
            if target_split.reconcile_state == SplitState.RECONCILED and not self.recn_test(spath_target):
                return resultvalue
            ComponentManager.suspend()
            time_current = int(current_trans.enter_date)
            time_target = int(target_trans.enter_date)
            if time_current == time_target:
                if move_up:
                    time_current += 1
                else:
                    time_target += 1
            current_trans.set_enter_date(time_target)
            target_trans.set_enter_date(time_current)
            ComponentManager.resume()
        resultvalue = True
        return resultvalue

    def get_blank_split(self):
        model = self.get_model_from_view()
        return model.get_blank_split()

    def trans_test_for_edit(self, trans):
        from gasstation.views.main_window import MainWindow
        self.finish_edit()
        window = MainWindow.get_main_window(self)
        dirty_trans = self.get_dirty_trans()
        if trans.is_open() and dirty_trans != trans:
            show_error(window, "This transaction is being edited in a different register.")
            return True
        return False

    def reinit(self, _):
        title = "Remove the splits from this transaction?"
        recn_warn = "This transaction contains reconciled splits. " \
                    "Modifying it is not a good idea because that will " \
                    "cause your reconciled balance to be off."
        trans = self.get_current_trans()
        if trans is None:
            return
        if trans == self.get_blank_trans():
            return
        if self.trans_readonly_and_warn(trans):
            return

        if self.trans_test_for_edit(trans):
            return

        if self.trans_open_and_warn(trans):
            return

        window = self.get_parent()
        dialog = Gtk.MessageDialog(window, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.NONE,
                                   message_format=title)

        if trans.has_reconciled_splits():
            dialog.format_secondary_text(recn_warn)
            warning = PREF_WARN_REG_SPLIT_DEL_ALL_RECD
        else:
            warning = PREF_WARN_REG_SPLIT_DEL_ALL
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        gs_dialog_add_button(dialog, "_Remove Splits", "edit-delete", Gtk.ResponseType.ACCEPT)
        response = gs_dialog_run(dialog, warning)
        dialog.destroy()
        if response != Gtk.ResponseType.ACCEPT:
            return
        self.re_init_trans()

    def delete(self):
        split = self.get_current_split()
        if split is None:
            split = self.get_current_trans_split()
            if split is None:
                return
        model = self.get_model_from_view()
        anchor = model.get_anchor()
        trans = split.get_transaction()
        if trans is None:
            return
        if self.trans_readonly_and_warn(trans):
            return
        if self.trans_test_for_edit(trans):
            return
        if self.trans_open_and_warn(trans):
            return

        depth = self.get_selected_row_depth()
        blank_split = self.get_blank_split()
        if split == blank_split:
            return
        blank_trans = self.get_blank_trans()
        if trans == blank_trans:
            return
        window = self.get_parent()
        if depth == RowDepth.SPLIT3:
            fmt = "Delete the split '%s' from the transaction '%s'?"
            recn_warn = "You would be deleting a reconciled split! "
            "This is not a good idea as it will cause your "
            "reconciled balance to be off."
            anchor_error = "You cannot delete this split."
            anchor_split = "This is the split anchoring this transaction to the register.\n " \
                           "You may not delete it from this register window. You may delete the entire transaction" \
                           " from this window, or you may navigate to a register that shows " \
                           "another side of this same transaction and delete the split from that register."

            if split == self.get_current_trans_split() or split == trans.get_split_equal_to_ancestor(anchor):
                dialog = Gtk.MessageDialog(window, modal=True, destroy_with_parent=True,
                                           message_type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.OK,
                                           message_format=anchor_error)
                dialog.format_secondary_text(anchor_split)
                dialog.run()
                dialog.destroy()
                return
            memo = split.get_memo()
            memo = memo if memo else "(no memo)"
            desc = trans.get_description()
            desc = desc if desc else "(no description)"
            buf = fmt % (memo, desc)
            dialog = Gtk.MessageDialog(window, modal=True, destroy_with_parent=True,
                                       message_type=Gtk.MessageType.QUESTION, buttons=Gtk.ButtonsType.NONE,
                                       message_format=buf)

            recn = split.get_reconcile_state()
            if recn == SplitState.RECONCILED or recn == SplitState.FROZEN:
                dialog.format_secondary_text(recn_warn)
                warning = PREF_WARN_REG_SPLIT_DEL_RECD
            else:
                warning = PREF_WARN_REG_SPLIT_DEL
            dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
            gs_dialog_add_button(dialog, "_Delete Split", "edit-delete", Gtk.ResponseType.ACCEPT)
            response = gs_dialog_run(dialog, warning)
            dialog.destroy()
            if response != Gtk.ResponseType.ACCEPT:
                return
            self.delete_current_split()
            return
        if depth != RowDepth.TRANS1 and depth != RowDepth.TRANS2:
            return

        title = "Delete the current transaction?"
        recn_warn = "You would be deleting a transaction " \
                    "with reconciled splits! " \
                    "This is not a good idea as it will cause your " \
                    "reconciled balance to be off."
        dialog = Gtk.MessageDialog(transient_for=window, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.NONE,
                                   message_format=title)
        if trans.has_reconciled_splits():
            dialog.format_secondary_text(recn_warn)
            warning = PREF_WARN_REG_TRANS_DEL_RECD
        else:
            warning = PREF_WARN_REG_TRANS_DEL
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        gs_dialog_add_button(dialog, "_Delete Transaction", "edit-delete", Gtk.ResponseType.ACCEPT)
        response = gs_dialog_run(dialog, warning)
        dialog.destroy()
        if response != Gtk.ResponseType.ACCEPT:
            return
        self.delete_current_trans()
        return

    def get_account_by_name(self, name):
        from gasstation.views.main_window import MainWindow
        from gasstation.views.dialogs.account import AccountDialog
        placeholder = "The account %s does not allow transactions."
        missing = "The account %s does not exist.Would you like to create it?"
        if name is None or len(name) == 0:
            return
        if gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_SHOW_LEAF_ACCT_NAMES):
            account = Session.get_current_root()
            names = name.split(Account.get_separator())
            for n in names:
                account = account.lookup_by_name(n)

        else:
            account = Session.get_current_root().lookup_by_full_name(name)

        if account is None:
            account = Session.get_current_root().lookup_by_code(name)
        window = MainWindow.get_main_window(self)
        if account is None:
            if not ask_yes_no(window, True, missing % name):
                return None
            dialog = AccountDialog.new_from_name_window(window, name)
            if dialog.run() != Gtk.ResponseType.OK:
                return
            return self.get_account_by_name(name)
        if account.get_placeholder():
            show_error(window, placeholder % name)
        if account == self.anchor:
            return None
        return account

    def get_current_trans_split(self):
        model = self.get_model_from_view()
        mpath = self.get_current_path()
        m_iter = model.get_iter(mpath)
        is_trow1, is_trow2, is_split, is_blank, trans, split = model.get_split_and_trans(m_iter)
        anchor = model.get_anchor()
        split = trans.get_split_equal_to_ancestor(anchor)
        return split

    def auto_complete_cb(self, trans, new_text):
        model = self.get_model_from_view()
        btrans = model.get_blank_trans()
        if trans != btrans:
            return
        desc_list = model.get_description_list()
        _iter = desc_list.get_iter_first()
        while _iter is not None:
            text = desc_list.get_value(_iter, 0, )
            trans_from = desc_list.get_value(_iter, 1)
            if text == new_text:
                trans_from.copy_onto(trans)
                break
            _iter = desc_list.iter_next(_iter)

    def exchange_rate(self):
        from gasstation.views.main_window import MainWindow
        model = self.get_model_from_view()
        trans = self.get_current_trans()
        expanded = self.trans_expanded(None)
        depth = self.get_selected_row_depth()
        num_splits = len(trans)
        anchor = model.get_anchor()
        transaction_com = trans.get_currency()
        if trans is None:
            return
        if trans == self.get_blank_trans():
            return
        if self.trans_readonly_and_warn(trans):
            return

        if self.trans_test_for_edit(trans):
            return

        if self.trans_open_and_warn(trans):
            return
        if num_splits < 2:
            return

        window = MainWindow.get_main_window(self)
        if not self.has_rate():
            message = "This register does not support editing exchange rates."
            show_error(window, message)
            return
        if anchor and not anchor.get_commodity().is_currency():
            message = "This register does not support editing exchange rates."
            show_error(window, message)
            return

        if self.is_multi(trans.get_split(0)) and not expanded:
            message = "You need to expand the transaction in order to modify its exchange rates."
            show_error(window, message)
            return

        if not self.is_multi(trans.get_split(0)) and anchor is not None and not expanded:
            split = self.get_current_trans_split()
            if split.get_account().get_type() == AccountType.TRADING:
                return
            osplit = split.get_other_split()
            value = split.get_value()
            self.set_dirty_trans(trans)
            trans.begin_edit()
            if transaction_com == split.get_account().get_commodity():
                self.set_value_for(trans, osplit, value * -1, True)
            else:
                self.set_value_for(trans, split, value, True)
            trans.commit_edit()
            self.set_dirty_trans(None)

        if num_splits > 1 and expanded and depth == 3:
            split = self.get_current_split()
            if split.get_account().get_type() == AccountType.TRADING:
                return
            value = split.get_value()
            if transaction_com == split.get_account().get_commodity():
                message = "The two currencies involved equal each other."
                show_error(window, message)
                return
            else:
                self.set_dirty_trans(trans)
                trans.begin_edit()
                self.set_value_for(trans, split, value, True)
                trans.commit_edit()
                self.set_dirty_trans(None)

    def trans_readonly_and_warn(self, trans):
        title = "Cannot modify, delete or schedule this transaction."
        message_reason = "This transaction is marked read-only with the comment: '%s'"
        if trans is None: return False
        window = self.get_parent()
        model = self.get_model_from_view()

        if trans.is_readonly_by_post_date():
            dialog = Gtk.MessageDialog(window, destroy_with_parent=True, message_type=Gtk.MessageType.ERROR,
                                       buttons=Gtk.ButtonsType.OK, message_format=title)
            dialog.format_secondary_text(
                "The date of this transaction is older than the \"Read-Only Threshold\" set for this book. "
                "This setting can be changed in File -> Properties -> Accounts.")
            dialog.run()
            dialog.destroy()
            return True

        if trans.is_readonly():
            dialog = Gtk.MessageDialog(transient_for=window, message_type=Gtk.MessageType.ERROR,
                                       buttons=Gtk.ButtonsType.OK,
                                       message_format=title)
            dialog.format_secondary_text(message_reason % trans.get_readonly())
            dialog.run()
            dialog.destroy()
            return True

        if model.get_read_only(trans):
            dialog = Gtk.MessageDialog(window, destroy_with_parent=True, message_type=Gtk.MessageType.ERROR,
                                       buttons=Gtk.ButtonsType.OK, message_format=title)
            dialog.format_secondary_text(
                "You can not change this transaction, the Book or Register is set to Read Only.")
            dialog.run()
            dialog.destroy()
            return True
        return False

    def duplicate_current(self):
        from gasstation.views.main_window import MainWindow
        from gasstation.views.dialogs.dup_trans import DupTransDialog
        out_num = ""
        model = self.get_model_from_view()
        blank_split = self.get_blank_split()
        split = self.get_current_split()
        trans_split = self.get_current_trans_split()
        depth = self.get_selected_row_depth()
        use_split_action_for_num_field = Session.get_current_book().use_split_action_for_num_field()
        trans = self.get_current_trans()
        if trans is None:
            return False
        if trans == self.get_blank_trans():
            return False
        if split == blank_split:
            return False
        if self.trans_readonly_and_warn(trans):
            return False
        if self.trans_test_for_edit(trans):
            return False
        if self.trans_open_and_warn(trans):
            return False

        window = MainWindow.get_main_window(self)
        if depth == RowDepth.SPLIT3:
            new_act_num = False
            if split != trans_split:
                if use_split_action_for_num_field and str(gs_get_num_action(None, split)).isdigit():
                    account = split.get_account()
                    title = "New Split Information"
                    date = time.time()
                    if account:
                        in_num = account.get_last_num()
                    else:
                        in_num = gs_get_num_action(None, split)
                    a = DupTransDialog.trans_dialog(window, title, False, date, in_num, out_num, None, None, None,
                                                      None)
                    if not a[0]:
                        return False
                    else:
                        # date = a[1]
                        out_num = a[2]
                    new_act_num = True
                new_split = Split(Session.get_current_book())
                model.set_blank_split_parent(trans, True)
                if not trans.is_open():
                    trans.begin_edit()
                self.set_dirty_trans(trans)
                split.copy_onto(new_split)
                new_split.set_transaction(trans)
                model.set_blank_split_parent(trans, False)
                if new_act_num:
                    gs_set_num_action(None, new_split, out_num, None)
                if new_act_num and str(out_num).isdigit():
                    account = new_split.get_account()
                    if account == model.get_anchor():
                        account.set_last_num(out_num)
            else:
                show_error(window, "This is the split anchoring this transaction to the register. "
                                   " You can not duplicate it from this register window.")
                return False

        else:
            out_num = ""
            out_tnum = ""
            out_tassoc = ""
            in_num = ""
            use_auto_readonly = Session.get_current_book().uses_auto_readonly()
            date = datetime.date.today()
            if str(gs_get_num_action(trans, trans_split)).isdigit():
                account = model.get_anchor()
                if account:
                    in_num = account.get_last_num()
                else:
                    in_num = gs_get_num_action(trans, trans_split)
            in_tnum = gs_get_num_action(trans, None) if use_split_action_for_num_field else None

            a = DupTransDialog.trans_dialog(window, None, True, date, in_num, out_num, in_tnum, out_tnum,
                                              trans.get_association_url(), out_tassoc)
            if not a[0]:
                return False
            else:
                out_tassoc = a[4]
                date = a[1]
                out_num = a[2]
                out_tnum = a[3]
            if use_auto_readonly:
                readonly_threshold = Session.get_current_book().get_auto_readonly_date()
                if date < readonly_threshold:
                    dialog = Gtk.MessageDialog(transient_for=window,
                                               message_type=Gtk.MessageType.ERROR,
                                               buttons=Gtk.ButtonsType.OK,
                                               message_format="Cannot store a transaction at this date")
                    dialog.format_secondary_text("The entered date of the duplicated transaction is older than "
                                                 "the \"Read-Only Threshold\" set for this book. "
                                                 "This setting can be changed in File -> Properties -> Accounts.")
                    dialog.run()
                    dialog.destroy()
                    return False
            trans_split_index = trans.get_split_index(trans_split)
            new_trans = Transaction(Session.get_current_book())
            #TODO Add a location to this transaction
            new_trans.begin_edit()
            trans.copy_onto(new_trans)
            new_trans.set_post_date(date)
            new_trans.set_enter_date(datetime.datetime.now())
            if out_tassoc:
                new_trans.set_association_url("")
            gs_set_num_action(new_trans, None, out_num, out_tnum)
            if str(out_num).isdigit():
                account = model.get_anchor()
                if account:
                    account.set_last_num(out_num)

            if use_split_action_for_num_field:
                gs_set_num_action(None, new_trans.get_split(trans_split_index), out_num, None)
            new_trans.commit_edit()
            return True

    @staticmethod
    def get_account_for_trans_ancestor(trans, ancestor):
        if trans is None or ancestor is None: return
        for split in trans.splits:
            split_acc = split.get_account()
            if not trans.still_has_split(split):
                continue
            if ancestor == split_acc:
                return split_acc
            if ancestor and split_acc.has_ancestor(ancestor):
                return split_acc
        return None

    def copy_trans(self):
        global clipboard_acct, clipboard_trans
        model = self.get_model_from_view()
        from_trans = self.get_current_trans()
        if from_trans is None:
            return
        anchor = model.get_anchor()
        clipboard_acct = self.get_account_for_trans_ancestor(from_trans, anchor)
        if clipboard_trans is not None and not clipboard_trans.is_open():
            clipboard_trans.begin_edit()
        if clipboard_trans is not None:
            clipboard_trans.destroy()
        clipboard_trans = from_trans.copy_to_clipboard()

    def paste_trans(self):
        from gasstation.views.main_window import MainWindow
        global clipboard_acct, clipboard_trans
        model = self.get_model_from_view()
        anchor_acct = model.get_anchor()
        to_trans = self.get_current_trans()
        if to_trans is None or clipboard_trans is None:
            return
        if self.trans_test_for_edit(to_trans):
            return
        if self.trans_readonly_and_warn(to_trans):
            return

        if clipboard_acct is None and anchor_acct is not None:
            window = MainWindow.get_main_window(self)
            show_error(window, "You can not paste from the general journal to a register.")
            return
        self.set_dirty_trans(to_trans)
        if not to_trans.is_open():
            to_trans.begin_edit()
        model.set_blank_split_parent(to_trans, True)
        to_trans.copy_from_clipboard(clipboard_trans, clipboard_acct, anchor_acct, False)
        model.set_blank_split_parent(to_trans, False)
        model.emit("refresh-trans", None)

    def void_current_trans(self, reason):
        blank_split = self.get_blank_split()
        split = self.get_current_split()
        if split is None:
            return
        if split == blank_split:
            return
        if split.get_reconcile_state() == SplitState.VOIDED:
            return
        trans = split.get_transaction()
        if trans is None:
            return
        if trans == self.get_blank_trans():
            return
        if self.trans_readonly_and_warn(trans):
            return

        if self.trans_test_for_edit(trans):
            return

        if self.trans_open_and_warn(trans):
            return
        self.set_dirty_trans(trans)
        trans.void(reason)
        if trans.is_open():
            trans.commit_edit()
        self.set_dirty_trans(None)

    def un_void_current_trans(self):
        blank_split = self.get_blank_split()
        split = self.get_current_split()
        if split is None:
            return
        if split == blank_split:
            return
        if split.get_reconcile_state() != SplitState.VOIDED:
            return
        trans = split.get_transaction()
        if trans is None:
            return
        if trans == self.get_blank_trans():
            return
        self.set_dirty_trans(trans)
        trans.unvoid()
        self.set_dirty_trans(None)

    def reverse_current(self):
        from gasstation.views.main_window import MainWindow
        trans = self.get_current_trans()
        if trans is None: return
        if trans == self.get_blank_trans(): return
        if self.trans_readonly_and_warn(trans): return
        if self.trans_test_for_edit(trans): return
        window = MainWindow.get_main_window(self)
        if trans.get_reversed_by():
            show_error(window, "A reversing entry has already been created for this transaction.")
            return
        if self.trans_open_and_warn(trans): return
        new_trans = trans.reverse()
        new_trans.begin_edit()
        new_trans.set_post_date(datetime.datetime.now())
        new_trans.set_enter_date(datetime.datetime.now())
        new_trans.commit_edit()
        for sp in new_trans.splits:
            if new_trans.still_has_split(sp):
                Event.gen(sp.get_account(), EVENT_ITEM_ADDED, sp)
        while Gtk.events_pending():
            Gtk.main_iteration()
        self.jump_to(None, new_trans.get_split(0), False)

    def has_rate(self):
        model = self.get_model_from_view()
        if model.type in [RegisterModelType.BANK_REGISTER,
                          RegisterModelType.CASH_REGISTER,
                          RegisterModelType.ASSET_REGISTER,
                          RegisterModelType.CREDIT_REGISTER,
                          RegisterModelType.LIABILITY_REGISTER,
                          RegisterModelType.INCOME_REGISTER,
                          RegisterModelType.EXPENSE_REGISTER,
                          RegisterModelType.EQUITY_REGISTER,
                          RegisterModelType.TRADING_REGISTER,
                          RegisterModelType.GENERAL_JOURNAL,
                          RegisterModelType.INCOME_LEDGER,
                          RegisterModelType.SEARCH_LEDGER]:
            return True
        return False

    def needs_conv_rate(self, trans, acc):
        if not self.has_rate():
            return False
        acc_com = acc.get_commodity() if acc is not None else None
        trans_cur = trans.get_currency()
        if trans_cur is not None and acc_com is not None and trans_cur.equal(acc_com):
            return False
        return True

    def needs_amount(self, split):
        return self.needs_conv_rate(split.get_transaction(), split.get_account())

    def get_value_for(self, trans, split, is_blank):
        try:
            return self.get_debcred_entry(trans, split, is_blank)[0]
        except ValueError:
            return Decimal(0)

    def get_debcred_entry(self, trans, split, is_blank):
        model = self.get_model_from_view()
        amount = Decimal(0)
        print_info = PrintAmountInfo.default()
        currency = trans.get_currency()
        if currency is None:
            currency = gs_default_currency()
        if is_blank:
            imbalance = trans.get_imbalance_value()
            if imbalance.is_zero():
                raise ValueError("Imbalance is zero")
            if trans.use_trading_accounts():
                imbal_list = trans.get_imbalance()
                if imbal_list is None:
                    raise ValueError("No imbanaces")
                if not Commodity.equal(imbal_list.get_commodity(), currency):
                    raise ValueError("Currencies with imbalance not Same")
                if imbal_list.get_value() != imbalance:
                    raise ValueError("Imbalance Values differ")
            imbalance = imbalance * -1
            acc = model.get_anchor()

            if self.needs_conv_rate(trans, acc):
                imbalance *= trans.get_account_conv_rate(acc)
            return imbalance, PrintAmountInfo.account(acc, False)
        account = model.get_anchor()
        commodity = account.get_commodity() if account is not None else None
        split_commodity = split.get_account().get_commodity() if split.get_account() is not None else None
        if trans.use_trading_accounts():
            use_symbol = True
            is_current = False
            current_split = self.get_current_split()
            depth = self.get_selected_row_depth()

            if split == current_split and depth == RowDepth.SPLIT3:
                is_current = True

            if model.type == RegisterModelType.STOCK_REGISTER or model.type == RegisterModelType.CURRENCY_REGISTER or model.type == RegisterModelType.PORTFOLIO_LEDGER:
                if self.use_security():
                    amount = split.get_value()
                    amount_commodity = currency
                else:
                    amount = split.get_amount()
                    amount_commodity = split_commodity

                if is_current or Commodity.equiv(amount_commodity, gs_default_currency()):
                    use_symbol = False
                print_info = PrintAmountInfo.commodity(amount_commodity, use_symbol)

            else:
                amount = split.get_amount()
                if is_current or Commodity.equiv(split_commodity, commodity):
                    use_symbol = False
                else:
                    use_symbol = True
                print_info = PrintAmountInfo.commodity(split_commodity, use_symbol)
        elif split is not None:
            if model.type in [RegisterModelType.STOCK_REGISTER,
                              RegisterModelType.CURRENCY_REGISTER,
                              RegisterModelType.PORTFOLIO_LEDGER]:
                amount = split.get_value()
                print_info = PrintAmountInfo.commodity(currency, False)
            else:
                if commodity is not None and not Commodity.equal(commodity, currency):
                    amount = split.convert_amount(account)
                else:
                    amount = split.get_value()
                print_info = PrintAmountInfo.account(account, False)

        if amount.is_zero():
            raise ValueError("Amount is zero")
        return amount, print_info

    def rotate(self, col, trans, split):
        renderers = col.get_cells()
        cr0 = renderers[0]
        viewcol = self.get_user_data(cr0, "view_column")
        if viewcol == ViewCol.RECONCILE and split is not None:
            sstate = split.get_reconcile_state()
            if sstate == SplitState.UNRECONCILED:
                recn = SplitState.CLEARED
            else:
                recn = SplitState.UNRECONCILED
            self.set_dirty_trans(trans)
            if not trans.is_open():
                trans.begin_edit()
            split.set_reconcile_state(recn)
            return True
        elif viewcol == ViewCol.TYPE and trans is not None:
            _type = trans.get_type()
            if _type == TransactionType.NONE:
                _type = TransactionType.INVOICE
            elif _type == TransactionType.INVOICE:
                _type = TransactionType.PAYMENT
            elif _type == TransactionType.PAYMENT:
                _type = TransactionType.NONE
            self.set_dirty_trans(trans)
            if not trans.is_open():
                trans.begin_edit()
            trans.set_type(_type)
            return True
        return False

    def needs_exchange_rate(self, trans, split):
        transaction_curr = trans.get_currency()
        split_com = split.get_account().get_commodity()
        if split_com is not None and transaction_curr is not None and split_com.guid != transaction_curr.guid:
            return True

        reg_comm = self.get_reg_commodity()
        if split_com is not None and reg_comm is not None:
            if split_com.guid != reg_comm.guid:
                return True
        return False

    @staticmethod
    def parse_date(datestr):
        use_auto_readonly = Session.get_current_book().uses_auto_readonly()
        if datestr is None:
            return
        if not scan_date(datestr):
            today = datetime.date.today()
            day, month, year = today.day, today.month, today.year
        else:
            day, month, year = scan_date(datestr)
        if use_auto_readonly:
            d = datetime.date(day=day, month=month, year=year)
            readonly_threshold = Session.get_current_book().get_auto_readonly_date()
            if d < readonly_threshold:
                dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                           "Cannot store a transaction at this date")
                dialog.format_secondary_text("The entered date of the new transaction is older than the"
                                             " \"Read-Only Threshold\" set for this book. "
                                             "This setting can be changed in File -> Properties -> Accounts.")
                dialog.run()
                dialog.destroy()
                day = readonly_threshold.day
                month = readonly_threshold.month
                year = readonly_threshold.year
        return datetime.date(day=day, month=month, year=year)

    @staticmethod
    def get_date_help(date):
        return date.strftime("%A %d %B %Y")

    @staticmethod
    def get_rate_from_db(fro, to):
        book = Session.get_current_book()
        prc = PriceDB.lookup_latest(PriceDB.get_db(book), fro, to)
        if prc is None:
            return Decimal(1)
        if Commodity.equiv(fro, prc.get_currency()):
            return -prc.get_value()
        return prc.get_value()

    def handle_exchange_rate(self, amount, trans, split, force):
        xfer_comm = split.get_account().get_commodity()
        reg_comm = self.get_reg_commodity()
        trans_curr = trans.get_currency()
        have_rate = True
        value = Decimal(0)
        model = self.get_model_from_view()
        reg_acc = model.get_anchor()
        expanded = self.trans_expanded(trans)
        try:
            rate_split = trans.get_rate_for_commodity(xfer_comm, split)
            if trans_curr.equal(xfer_comm):
                split.set_amount(amount)
                split.set_value(amount)
                return True
            try:
                rate_reg = trans.get_rate_for_commodity(reg_comm, split)
                if not force:
                    value = amount / rate_reg
                    amount = value * rate_split
            except ConversionError:
                pass
        except ConversionError:
            rate_split = self.get_rate_from_db(reg_comm, xfer_comm)
            xfer = TransferDialog(self, None)
            xfer.is_exchange_dialog(rate_split)
            xfer.set_description(trans.get_description())
            xfer.set_memo(split.get_memo())
            xfer.set_num(gs_get_num_action(trans, split))
            xfer.set_date(trans.get_post_date())
            value = amount
            yes, rate_split = xfer.run_exchange_dialog(rate_split, value, reg_acc, trans, xfer_comm, expanded)

            if yes:
                rate_split = Decimal(1)
                have_rate = False
            else:
                have_rate = True
            amount = value * rate_split
        split.set_amount(amount)
        split.set_value(value)
        return have_rate

    def set_value_for(self, trans, split, inp, force):
        from gasstation.views.main_window import MainWindow
        if inp.is_zero():
            split.set_value(inp)
            split.set_amount(inp)
            return
        window = MainWindow.get_main_window(self)
        if self.needs_exchange_rate(trans, split):
            if self.handle_exchange_rate(inp, trans, split, force):
                pass
            else:
                show_error(window, "Exchange Rate Canceled, using existing "
                                   "rate or default 1 to 1 rate if this is a new transaction.")
            return
        self.save_amount_values(trans, split, inp)

    def save_amount_values(self, trans, split, inp):
        model = self.get_model_from_view()
        new_amount = inp
        acc = model.get_anchor()
        xfer_acc = split.get_account() if split is not None else None
        xfer_com = xfer_acc.get_commodity() if xfer_acc is not None else None
        reg_com = acc.get_commodity() if acc is not None else None

        try:
            amtconv = trans.get_rate_for_commodity(reg_com, None)
        except ConversionError:
            amtconv = Decimal(1)
        if self.needs_conv_rate(trans, acc):
            if Commodity.equal(reg_com, xfer_com):
                amtconv = trans.get_account_conv_rate(acc)

        if trans.use_trading_accounts():
            if (model.type == RegisterModelType.STOCK_REGISTER or
                    model.type == RegisterModelType.CURRENCY_REGISTER or
                    model.type == RegisterModelType.PORTFOLIO_LEDGER):
                if xfer_acc.is_priced() or not xfer_acc.get_commodity().is_iso():
                    is_amount = False
                else:
                    is_amount = True
            else:
                is_amount = True
            if is_amount:
                split.set_amount(new_amount)
                if self.needs_amount(split):
                    value = new_amount / amtconv
                    split.set_value(value)
                else:
                    split.set_value(new_amount)

            else:
                split.set_value(new_amount)
            return

        if self.needs_conv_rate(trans, acc):
            new_amount = new_amount / amtconv
            split.set_value(new_amount)
        else:
            split.set_value(new_amount)

        value = split.get_value()

        if self.needs_amount(split):
            value = value / amtconv
            split.set_amount(value)
        else:
            split.set_amount(value)

    @staticmethod
    def get_amount_denom(split):
        denom = split.get_account().get_commodity_scu()
        if denom == 0:
            commodity = gs_default_currency()
            denom = commodity.get_fraction()
            if denom == 0:
                denom = 100
        return denom

    @staticmethod
    def get_value_denom(split):
        currency = split.get_transaction().get_currency()
        denom = currency.get_fraction() if currency is not None else 0
        if denom == 0:
            commodity = gs_default_currency()
            denom = commodity.get_fraction()
            if denom == 0:
                denom = 100
        return denom

    def set_value_for_amount(self, split, inp):
        if Decimal(inp).is_zero():
            split.set_value(inp)
            split.set_amount(inp)
            return
        amount = split.get_amount()
        value = split.get_value()
        try:
            split_rate = value / amount
        except InvalidOperation:
            split_rate = Decimal(1)
        if split_rate.is_zero():
            split_rate = Decimal(1)
        new_value = inp * split_rate
        split.set_value(new_value)
        split.set_amount(inp)

    def set_number_for_input(self, trans, split, inp, viewcol):
        price_changed = False
        value_changed = False
        shares_changed = False

        recalc_shares = False
        recalc_price = False
        recalc_value = False
        account = None
        model = self.get_model_from_view()
        if not model.get_is_sub_ac():
            account = model.get_anchor()
        expanded = self.trans_expanded(trans)

        if account is None:
            account = split.get_account()
        if not account.is_priced():
            return

        if split.get_transaction().use_trading_accounts():
            acc_commodity = account.get_commodity()
            if not account.is_priced() or not acc_commodity.is_iso():
                return
        if inp.is_zero():
            split.set_value(inp)
            split.set_amount(inp)
            return

        value = split.get_value()
        amount = split.get_amount()

        if viewcol == ViewCol.SHARES or viewcol == ViewCol.AMOUNT:
            if expanded:
                value_changed = True
                if amount.is_zero():
                    split.set_value(inp)
                    split.set_amount(inp)
                    return
            else:
                shares_changed = True
                if value.is_zero():
                    split.set_value(inp)
                    split.set_amount(inp)
                    return

        elif viewcol == ViewCol.PRICE:
            price_changed = True
            if value.is_zero():
                amount = Decimal(1)
                split.set_value(inp)
                split.set_amount(amount)
                return

        elif viewcol == ViewCol.CREDIT or viewcol == ViewCol.DEBIT:
            if expanded:
                value_changed = True
            else:
                shares_changed = True
            if value.is_zero():
                split.set_value(inp)
                split.set_amount(inp)
                return

        radio_list = []
        title = "Recalculate Transaction"
        message = "The values entered for this transaction are inconsistent." \
                  " Which value would you like to have recalculated?"

        if shares_changed:
            radio_list.append("_Shares (Changed)")
        else:
            radio_list.append("_Shares")

        if price_changed:
            radio_list.append("_Price (Changed)")
        else:
            radio_list.append("_Price")

        if value_changed:
            radio_list.append("_Value (Changed)")
        else:
            radio_list.append("_Value")

        if expanded:
            if price_changed:
                default_value = 0
            else:
                default_value = 1
        else:
            if price_changed:
                default_value = 2
            else:
                default_value = 1

        choice = choose_radio_option_dialog(self.get_parent(), title, message, "_Recalculate", default_value,
                                            radio_list)

        if choice == 0:
            recalc_shares = True

        elif choice == 1:
            recalc_price = True

        elif choice == 2:
            recalc_value = True
        else:
            return False

        if recalc_shares:
            if shares_changed:
                return
            if price_changed:
                price = inp
            else:
                price = value / amount
            if value_changed:
                split.set_value(inp)
                amt = inp / price
                split.set_amount(amt)
            else:
                amt = value / price
                split.set_amount(amt)

        if recalc_price:
            if price_changed:
                return
            if shares_changed:
                split.set_amount(inp)
                split.set_value(value)

            if value_changed:
                split.set_value(inp)
                split.set_amount(amount)

        if recalc_value:
            if value_changed:
                return
            if price_changed:
                price = inp
            else:
                price = value / amount

            if shares_changed:
                split.set_amount(inp)
                value = inp * price
                split.set_value(value)

            else:
                value = amount * price
                split.set_value(value)

        if price_changed:
            if inp > 0:
                self.record_price(account, inp, PriceSource.SPLIT_REG)

        if not self.is_multi(split) and expanded:
            osplit = split.get_other_split()
            value = split.get_value()
            osplit_com = osplit.get_account().get_commodity()
            if osplit_com.is_currency():
                osplit.set_value(value * -1)
                osplit.set_amount(value * -1)

    def record_price(self, account, value:Decimal, source):
        trans = self.get_current_trans()
        book = account.get_book()
        pricedb = PriceDB.get_db(book)
        comm = account.get_commodity()
        curr = trans.get_currency()
        swap = False

        if self.has_rate():
            return
        t = trans.get_enter_date()
        price = pricedb.lookup_day_t64(comm, curr, t)
        if Commodity.equiv(comm, price.get_currency() if price is not None else None):
            swap = True

        if price is not None:
            price_value = price.get_value()
            if swap:
                num, den = value.as_integer_ratio()
                value = Decimal(den/num)

            if value == price_value:
                price.unref()
                return

            if price.get_source() < PriceSource.XFER_DLG_VAL:
                price.unref()
                return

            price.begin_edit()
            price.set_time64(t)
            price.set_source(source)
            price.set_type(PriceType.TRN)
            price.set_value(value)
            price.commit_edit()
            price.unref()
            return
        price = Price(book)
        price.begin_edit()
        price.set_commodity(comm)
        price.set_currency(curr)
        price.set_time64(t)
        price.set_source(source)
        price.set_type(PriceType.TRN)
        price.set_value(value)
        pricedb.add_price(price)
        price.commit_edit()

    @staticmethod
    def is_multi(split):
        multi = False
        if not split:
            return multi
        osplit = split.get_other_split()
        if osplit is not None:
            multi = False
        else:
            trans = split.get_transaction()
            osplit = trans.get_split(1) if trans is not None else None
            if osplit is not None:
                multi = True
            elif "stock-split" == split.get_type():
                multi = True
            else:
                multi = False
        return multi

    @staticmethod
    def get_transfer_entry(split):
        multi = False
        if split is None:
            return "", multi
        osplit = split.get_other_split()
        if osplit is not None:
            name = gs_get_account_name_for_register(osplit.get_account())
        else:
            osplit = split.get_transaction().get_split(1) if split.get_transaction() is not None else None
            if osplit:
                name = SPLIT_TRANS_STR
                multi = True
            elif "stock-split" == split.get_type():
                name = STOCK_SPLIT_STR
                multi = True
            else:
                name = ""
        return name, multi

    @staticmethod
    def get_imbalance(trans):
        prefix = "Imbalance"
        for split in trans.get_splits():
            acc_name = split.get_account().get_name() if split.get_account() is not None else ""
            if acc_name.startswith(prefix):
                return True
        return False

    def use_security(self):
        split = self.get_current_split()
        depth = self.get_selected_row_depth()
        account = None
        if split is None:
            return True

        if depth != RowDepth.SPLIT3:
            return True
        if account is None:
            account = split.get_account()

        if account is None:
            return True

        if split.get_transaction().use_trading_accounts():
            if not account.get_commodity().is_iso():
                return True
        return account.is_priced()

    def get_rate_for(self, trans, split, is_blank):
        num = self.get_value_for(trans, split, is_blank)
        try:
            if trans.use_trading_accounts():
                num /= num, split.get_value()
            else:
                num /= split.get_amount()
        except decimal.InvalidOperation:
            return num
        return num

    @staticmethod
    def template_get_transfer_entry(split):
        if split is None:
            return ""
        account = split.scheduled_account
        return gs_get_account_name_for_register(account)

    @staticmethod
    def template_get_fdebt_entry(split):
        if split is None:
            return ""
        return split.scheduled_debit_formula

    @staticmethod
    def template_get_fcred_entry(split):
        if split is None:
            return ""
        return split.scheduled_credit_formula

    def dispose_cb(self, widget):
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL_REGISTER,
                                  PREF_DRAW_HOR_LINES, self.pref_changed)
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL_REGISTER,
                                  PREF_DRAW_VERT_LINES, self.pref_changed)
        model = self.get_model_from_view()
        super().dispose_cb(widget)
        model.dispose_cb()
        self.anchor = None
        self.reg_currency = None
        self.uiupdate_cb = None
        self.uiupdate_cb_data = None
        self.stamp = 0
        return True


GObject.type_register(RegisterView)
