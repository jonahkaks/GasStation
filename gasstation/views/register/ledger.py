from libgasstation.core.transquery import *
from .view import *

REGISTER_SINGLE_CM_CLASS = "register-single"
REGISTER_SUBACCOUNT_CM_CLASS = "register-subaccount"
REGISTER_GL_CM_CLASS = "register-gl"
REGISTER_TEMPLATE_CM_CLASS = "register-template"

PREF_DOUBLE_LINE_MODE = "double-line-mode"

PREF_DEFAULT_STYLE_LEDGER = "default-style-ledger"
PREF_DEFAULT_STYLE_AUTOLEDGER = "default-style-autoledger"
PREF_DEFAULT_STYLE_JOURNAL = "default-style-journal"


class RegisterLedgerType(GObject.GEnum):
    SINGLE = 0
    SUB_ACCOUNT = 1
    GL = 2


class RegisterLedger:
    leader = None
    _query = None
    ld_type = None
    model = None
    view = None
    refresh_ok = False
    loading = False
    use_double_line_default = False
    user_data = None
    component_id = 0
    refresh_id = 0
    destroy = None
    get_parent = None

    def get_leader(self):
        return self.leader() if self.leader is not None else None

    def get_type(self):
        return self.ld_type

    def set_user_data(self, user_data):
        self.user_data = user_data

    def get_user_data(self):
        return self.user_data

    def set_handlers(self, destroy, get_parent):
        if destroy is not None:
            self.destroy = weakref.WeakMethod(destroy)
        if get_parent is not None:
            self.get_parent = weakref.WeakMethod(get_parent)

    def get_model(self):
        return self.model()

    def get_query(self):
        return self._query

    @staticmethod
    def get_default_register_style():
        new_style = RegisterModelStyle.LEDGER
        if gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_DEFAULT_STYLE_JOURNAL):
            new_style = RegisterModelStyle.JOURNAL
        elif gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER, PREF_DEFAULT_STYLE_AUTOLEDGER):
            new_style = RegisterModelStyle.AUTO_LEDGER
        return new_style

    @staticmethod
    def look_for_portfolio_cb(account):
        if account.is_priced():
            return RegisterModelType.PORTFOLIO_LEDGER
        return None

    @staticmethod
    def get_reg_type(leader, ld_type):
        if ld_type == RegisterLedgerType.GL:
            return RegisterModelType.GENERAL_JOURNAL

        acc_type = leader.get_type()

        if ld_type == RegisterLedgerType.SINGLE:
            if acc_type == AccountType.BANK:
                return RegisterModelType.BANK_REGISTER
            elif acc_type == AccountType.CASH:
                return RegisterModelType.CASH_REGISTER

            elif acc_type == AccountType.CURRENT_ASSET or acc_type == AccountType.FIXED_ASSET:
                return RegisterModelType.ASSET_REGISTER

            elif acc_type == AccountType.CREDIT:
                return RegisterModelType.CREDIT_REGISTER

            elif acc_type == AccountType.CURRENT_LIABILITY or acc_type == AccountType.LONG_TERM_LIABILITY :
                return RegisterModelType.LIABILITY_REGISTER

            elif acc_type == AccountType.PAYABLE:
                return RegisterModelType.PAYABLE_REGISTER

            elif acc_type == AccountType.RECEIVABLE:
                return RegisterModelType.RECEIVABLE_REGISTER

            elif acc_type == AccountType.STOCK or acc_type == AccountType.MUTUAL:
                return RegisterModelType.STOCK_REGISTER

            elif acc_type == AccountType.INCOME:
                return RegisterModelType.INCOME_REGISTER

            elif acc_type == AccountType.EXPENSE:
                return RegisterModelType.EXPENSE_REGISTER

            elif acc_type == AccountType.EQUITY:
                return RegisterModelType.EQUITY_REGISTER

            elif acc_type == AccountType.CURRENCY:
                return RegisterModelType.CURRENCY_REGISTER

            elif acc_type == AccountType.TRADING:
                return RegisterModelType.TRADING_REGISTER
            else:
                return RegisterModelType.BANK_REGISTER

        if ld_type != RegisterLedgerType.SUB_ACCOUNT:
            return RegisterModelType.BANK_REGISTER

        if acc_type in [AccountType.BANK, AccountType.CASH,
                        AccountType.CURRENT_ASSET,AccountType.FIXED_ASSET, AccountType.CREDIT,
                        AccountType.CURRENT_LIABILITY, AccountType.LONG_TERM_LIABILITY,
                        AccountType.RECEIVABLE,
                        AccountType.PAYABLE]:
            reg_type = RegisterModelType.GENERAL_JOURNAL

            ret = leader.foreach_descendant_until(RegisterLedger.look_for_portfolio_cb)
            if ret:
                reg_type = RegisterModelType.PORTFOLIO_LEDGER

        elif acc_type in [AccountType.STOCK,
                          AccountType.MUTUAL, AccountType.CURRENCY]:
            reg_type = RegisterModelType.PORTFOLIO_LEDGER

        elif acc_type == AccountType.INCOME or acc_type == AccountType.EXPENSE:
            reg_type = RegisterModelType.INCOME_LEDGER

        elif acc_type == AccountType.EQUITY or acc_type == AccountType.TRADING:
            reg_type = RegisterModelType.GENERAL_JOURNAL
        else:
            reg_type = RegisterModelType.GENERAL_JOURNAL
        return reg_type

    def default_double_line(self):
        return self.use_double_line_default or gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                                                PREF_DOUBLE_LINE_MODE)

    @classmethod
    def simple(cls, account):
        acc_type = account.get_type()
        use_double_line = False
        if acc_type.is_payable_receivable():
            use_double_line = True
        reg_type = cls.get_reg_type(account, RegisterLedgerType.SINGLE)
        ld = cls(account, None, RegisterLedgerType.SINGLE, reg_type,
                 RegisterLedger.get_default_register_style(),
                 use_double_line, False)
        return ld

    @classmethod
    def subaccounts(cls, account):
        reg_type = cls.get_reg_type(account, RegisterLedgerType.SUB_ACCOUNT)
        ld = cls(account, None, RegisterLedgerType.SUB_ACCOUNT,
                 reg_type, RegisterModelStyle.JOURNAL, False,
                 False)
        return ld

    @classmethod
    def gl(cls):
        query = Query.create_for(ID_SPLIT)
        query.set_book(Session.get_current_book())
        template_root = Session.get_current_book().get_template_root()
        al = template_root.get_descendants()
        if len(al) > 0:
            query.add_account_match(al, GuidMatch.NONE, QueryOp.AND)
        start = datetime.date.today() - relativedelta(months=1)
        query.add_date_match_tt(True, start, False, 0, QueryOp.AND)
        ld = cls(None, query, RegisterLedgerType.GL, RegisterModelType.GENERAL_JOURNAL,
                 RegisterModelStyle.JOURNAL, False, False)
        return ld

    @classmethod
    def query(cls, query, _type, style):
        return cls(None, query, RegisterLedgerType.GL, _type, style, False, False)

    @classmethod
    def template_gl(cls, _id: str):
        is_template_mode_true = True
        query = Query.create_for(ID_SPLIT)
        book = Session.get_current_book()
        query.set_book(book)
        acct = None
        if _id is not None:
            root = book.get_template_root()
            acct = root.lookup_by_name(_id)
            query.add_single_account_match(acct, QueryOp.AND)
        ld = cls(None, query, RegisterLedgerType.GL, RegisterModelType.SEARCH_LEDGER,
                 RegisterModelStyle.JOURNAL,
                 False, is_template_mode_true)
        model = ld.get_model()
        if acct is not None:
            model.set_template_account(acct)
        return ld

    def refresh_handler(self, changes):
        leader = None
        if self.loading:
            return
        has_leader = (self.ld_type == RegisterLedgerType.SINGLE or self.ld_type == RegisterLedgerType.SUB_ACCOUNT)
        if has_leader:
            leader = self.leader() if self.leader is not None else None
            if leader is None:
                ComponentManager.close(self.component_id)
                return
        if changes is not None and has_leader:
            info = ComponentManager.get_entity_events(changes, leader)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return
        self.view().default_selection()

    def close_handler(self, *_):
        ComponentManager.unregister(self.component_id)
        if self.refresh_id > 0:
            self.model().disconnect(self.refresh_id)
        if self.destroy is not None:
            self.destroy()()
        if self._query is not None:
            self._query.destroy()
        self._query = None
        self.leader = None
        self.view = None
        self.model = None

    def make_query(self, _type):
        accounts = []
        if self.ld_type not in [RegisterLedgerType.SINGLE, RegisterLedgerType.SUB_ACCOUNT]:
            return
        Query.destroy(self._query)
        self._query = Query.create_for(ID_SPLIT)
        self._query.set_book(Session.get_current_book())
        leader = self.leader()
        if self.ld_type == RegisterLedgerType.SUB_ACCOUNT:
            accounts[:] = leader.get_descendants()
        accounts.insert(0, leader)
        self._query.add_account_match(accounts, GuidMatch.ANY, QueryOp.AND)

    def ledger_parent(self):
        if self.get_parent is not None:
            return self.get_parent()()

    @staticmethod
    def find_by_leader(account, ld):
        if account is None or not ld: return False
        return account == ld.leader()

    @staticmethod
    def find_by_reg(model, ld):
        if model is None: return False
        return model == ld.model()

    def __new__(cls, lead_account, q, ld_type, reg_type, style, use_double_line, is_template):
        self = super(RegisterLedger, cls).__new__(cls)
        display_subaccounts = False
        is_gl = False
        if ld_type == RegisterLedgerType.SINGLE:
            klass = REGISTER_SINGLE_CM_CLASS
            if reg_type >= RegisterModelType.NUM_SINGLE_REGISTER_TYPES:
                return None
            if lead_account is None:
                return None
            if q:
                q = None
            ld = ComponentManager.find_first(klass, RegisterLedger.find_by_leader, lead_account)
            if ld:
                return ld

        elif ld_type == RegisterLedgerType.SUB_ACCOUNT:
            klass = REGISTER_SUBACCOUNT_CM_CLASS
            if lead_account is None:
                return None
            if q:
                q = None
            display_subaccounts = True
            ld = ComponentManager.find_first(klass, RegisterLedger.find_by_leader, lead_account)
            if ld:
                return ld
        elif ld_type == RegisterLedgerType.GL:
            klass = REGISTER_GL_CM_CLASS
            is_gl = True

        else:
            return None
        self.leader = weakref.ref(lead_account) if lead_account is not None else None
        self._query = None
        self.ld_type = ld_type
        self.loading = False
        self.refresh_ok = False
        self.destroy = None
        self.user_data = None

        if q is not None:
            self._query = q
        else:
            self.make_query(reg_type)
        self.component_id = ComponentManager.register(klass, refresh_handler=self.refresh_handler,
                                                      close_handler=self.close_handler)

        self.use_double_line_default = use_double_line
        model = RegisterModel(reg_type, style, use_double_line, is_template)
        self.model = weakref.ref(model)
        model.set_user_data(self, self.ledger_parent)
        model.set_display(display_subaccounts, is_gl)
        self.refresh_internal(None)
        self.refresh_id = model.connect("refresh_trans", lambda *args: GLib.idle_add(self.refresh))
        return self

    def set_view(self, view):
        self.view = weakref.ref(view)

    def get_view(self):
        return self.view()

    def set_query(self, q):
        self._query = q

    def refresh_internal(self, splits):
        if self.loading:
            return
        if self.view is not None:
            self.view().freeze_child_notify()
        if not self.refresh_ok:
            self.loading = True
            self.model().load(splits, self.leader() if self.leader is not None else None)
            self.loading = False
        else:
            self.loading = True
            s_model = self.view().get_model()
            self.view().block_selection(True)
            self.view().set_model(None)
            self.model().load(splits, self.leader() if self.leader is not None else None)
            self.view().set_model(s_model)
            self.view().block_selection(False)
            self.view().default_selection()
            self.loading = False
        if self.view is not None:
            self.view().thaw_child_notify()

    def refilter(self):
        self.view().default_selection()

    def refresh_sched(self, splits):
        if self.loading:
            return
        self.refresh_internal(splits)

    def refresh(self):
        if self.loading:
            return
        if self._query is None:
            return
        self.model().update_query(self._query)
        self.refresh_internal(self._query.run())

    def refresh_by_split_register(self, model):
        pass

    def set_split_view_refresh(self, ok):
        self.refresh_ok = ok

    def close(self):
        ComponentManager.close(self.component_id)

    def set_watches(self):
        pass
