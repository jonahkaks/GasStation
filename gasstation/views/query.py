

#include <config.h>

#include <gtk/gtk.h>

#include "dialog-utils.h"
#include "gnc-ui-util.h"
#include "qof.h"
#include "gnc-component-manager.h"
#include "gnc-query-view.h"
#include "search-param.h"
from libgasstation.core.component import ComponentManager

static QofLogModule log_module = GNC_MOD_GUI


enum
{
    COLUMN_TOGGLED,
    ROW_SELECTED,
    DOUBLE_CLICK_ENTRY,
    LAST_SIGNAL
}


def construct(self, GList *param_list, Query *query)
{
    GNCQueryViewPrivate *priv


g_return_if_fail (param_list)
g_return_if_fail (query)



        self.query = qof_query_copy (query)
self.column_params = param_list


                                                    priv = GNC_QUERY_VIEW_GET_PRIVATE(qview)
self.get_guid = qof_class_get_parameter (qof_query_get_search_for (query),
                                          PARAM_GUID)


                       gnc_query_view_init_view (qview)


                    gnc_query_view_set_query_sort (qview, True)
}

GtkWidget *
gnc_query_view_new (GList *param_list, Query *query)
{
    GNCQueryView  *qview
GtkListStore  *liststore
GList         *node
gint           columns, i
gsize          array_size
GType         *types

g_return_val_if_fail (param_list, None)
g_return_val_if_fail (query, None)


columns = g_list_length (param_list) + 1
qview = GNC_QUERY_VIEW(g_object_new (gnc_query_view_get_type (), None))

array_size = sizeof(GType) * columns
types = g_slice_alloc (array_size)

types[0] = G_TYPE_POINTER


for (i = 0, node = param_list node node = node->next, i++)
{
GNCSearchParamSimple *param = node->data
const char *type

g_assert (GNC_IS_SEARCH_PARAM_SIMPLE(param))

type = gnc_search_param_get_param_type ((GNCSearchParam *) param)

if (g_strcmp0 (type, TYPE_BOOLEAN) == 0)
types[i+1] = G_TYPE_BOOLEAN
else
types[i+1] = G_TYPE_STRING
}


liststore = gtk_list_store_newv (columns, types )
gtk_tree_view_set_model (GTK_TREE_VIEW(qview), GTK_TREE_MODEL(liststore))
g_object_unref (liststore)


g_slice_free1 (array_size, types)

gnc_query_view_construct (qview, param_list, query)

return GTK_WIDGET(qview)
}

void gnc_query_view_reset_query(self, Query *query)
{

g_return_if_fail (query)


self.query.destroy()
self.query = qof_query_copy (query)

gnc_query_view_set_query_sort (qview, True)
}

static def refresh_handler (GHashTable *changes, gpointer user_data)
{
GNCQueryView *qview = (GNCQueryView *)user_data



gnc_query_view_set_query_sort (qview, True)
}

G_DEFINE_TYPE_WITH_PRIVATE(GNCQueryView, gnc_query_view, GTK_TYPE_TREE_VIEW)

static def init(self):
{
GNCQueryViewPrivate *priv


gtk_widget_set_name (GTK_WIDGET(qview), "gnc-id-query-view-view")

self.query = None

self.num_columns = 0
self.column_params = None

self.use_scroll_to_selection = False

self.sort_column = 0
self.increasing = False

self.numeric_abs = False
self.numeric_inv_sort = False

priv = GNC_QUERY_VIEW_GET_PRIVATE(qview)
self.component_id = gnc_register_gui_component ("gnc-query-view-cm-class",
                                                 gnc_query_view_refresh_handler,
                                                 None, qview)
}

static gint
sort_iter_compare_func (GtkTreeModel *model,
GtkTreeIter  *a,
GtkTreeIter  *b,
gpointer      userdata)
{

return 0
}


void
gnc_query_sort_order(self, gint column, GtkSortType order)
{
GtkTreeSortable *sortable
gint sortcol

g_return_if_fail (qview != None)


sortable = GTK_TREE_SORTABLE(self.get_model())

if((column > self.num_columns) || (column == 0) )
    sortcol = 1
else
    sortcol = column

gtk_tree_sortable_set_sort_column_id (sortable, sortcol, order)
}

static void
gnc_query_sort_cb (GtkTreeSortable *sortable, gpointer user_data)
{
GNCQueryView *qview = GNC_QUERY_VIEW(user_data)
GtkSortType   type
gint          sortcol
     new_column = False

g_return_if_fail (qview != None)

g_return_if_fail (self.query != None)

gtk_tree_sortable_get_sort_column_id (sortable, &sortcol, &type)


sortcol = sortcol - 1

if(type == GTK_SORT_ASCENDING)
    self.increasing = True
else
    self.increasing = False


new_column = (self.sort_column != sortcol)


self.sort_column = sortcol

gnc_query_view_set_query_sort (qview, new_column)
}

static def init_view(self):
{
GtkTreeView         *view = GTK_TREE_VIEW(qview)
GtkTreeSortable     *sortable
GtkTreeSelection    *selection
GtkTreeViewColumn   *col
GtkCellRenderer     *renderer
GList               *node
gint                 i

sortable = GTK_TREE_SORTABLE(gtk_tree_view_get_model (GTK_TREE_VIEW(view)))


self.num_columns = g_list_length (self.column_params)


gtk_tree_view_set_grid_lines (GTK_TREE_VIEW(view), gnc_tree_view_get_grid_lines_pref ())

for (i = 0, node = self.column_params node node = node->next, i++)
    {
        const char *type
gfloat algn = 0
GNCSearchParamSimple *param = node->data

g_assert (GNC_IS_SEARCH_PARAM_SIMPLE(param))

col = gtk_tree_view_column_new ()


                  gtk_tree_view_column_set_title (col, (gchar *) ((GNCSearchParam *) param)->title)


                                   gtk_tree_view_append_column (view, col)


if (((GNCSearchParam *) param)->justify == GTK_JUSTIFY_CENTER)
algn = 0.5
else if (((GNCSearchParam *) param)->justify == GTK_JUSTIFY_RIGHT)
{

if (gtk_widget_get_direction (GTK_WIDGET(view)) != GTK_TEXT_DIR_RTL)
algn = 1.0
}


gtk_tree_view_column_set_alignment (col, algn)


if (((GNCSearchParam *) param)->non_resizeable)
{
gtk_tree_view_column_set_resizable (col, False)
gtk_tree_view_column_set_expand (col, False)
}
else
gtk_tree_view_column_set_resizable (col, True)


if (((GNCSearchParam *) param)->passive)
gtk_tree_view_column_set_clickable (col, False)
else
{
gtk_tree_view_column_set_clickable (col, True)

gtk_tree_view_column_set_sort_column_id (col, i+1)
gtk_tree_sortable_set_sort_func (sortable, i+1,
sort_iter_compare_func,
GINT_TO_POINTER(i+1), None)
}

type = gnc_search_param_get_param_type (((GNCSearchParam *) param))

if (g_strcmp0 (type, TYPE_BOOLEAN) == 0)
{
renderer = gtk_cell_renderer_toggle_new ()


gtk_tree_view_column_pack_start (col, renderer, True)
gtk_tree_view_column_add_attribute (col, renderer, "active", i+1)
g_object_set (renderer, "xalign", algn, None)
g_object_set_data (G_OBJECT(renderer), "column", GINT_TO_POINTER(i+1))
g_signal_connect (renderer, "toggled",
G_CALLBACK(gnc_query_view_toggled_cb), view)
}
else
{
renderer = gtk_cell_renderer_text_new ()


gtk_tree_view_column_pack_start (col, renderer, True)
gtk_tree_view_column_add_attribute (col, renderer, "text", i+1)
g_object_set (renderer, "xalign", algn, None)
g_object_set_data (G_OBJECT(renderer), "column", GINT_TO_POINTER(i+1))
}
}


gtk_tree_sortable_set_default_sort_func (sortable, None, None, None)
gtk_tree_sortable_set_sort_column_id (sortable, 1, GTK_SORT_DESCENDING)

g_signal_connect (sortable, "sort-column-changed",
                  G_CALLBACK(gnc_query_sort_cb),
                  view)

selection = gtk_tree_view_get_selection (GTK_TREE_VIEW(view))
g_signal_connect (selection, "changed",
                  G_CALLBACK(gnc_query_view_select_row_cb),
                  None)

g_signal_connect (view, "row-activated",
                  G_CALLBACK(gnc_query_view_double_click_cb),
                  None)
}

static def class_init (GNCQueryViewClass *klass)
{
GtkWidgetClass *widget_class = (GtkWidgetClass*) klass

parent_class = g_type_class_peek (GTK_TYPE_TREE_VIEW)

query_view_signals[COLUMN_TOGGLED] =
g_signal_new("column_toggled",
             G_TYPE_FROM_CLASS(widget_class),
             G_SIGNAL_RUN_FIRST,
             G_STRUCT_OFFSET(GNCQueryViewClass, column_toggled),
             None, None,
             g_cclosure_marshal_VOID__POINTER,
             G_TYPE_NONE,
             1,
             G_TYPE_POINTER)

query_view_signals[ROW_SELECTED] =
g_signal_new("row_selected",
             G_TYPE_FROM_CLASS(widget_class),
             G_SIGNAL_RUN_FIRST,
             G_STRUCT_OFFSET(GNCQueryViewClass, row_selected),
             None, None,
             g_cclosure_marshal_VOID__POINTER,
             G_TYPE_NONE,
             1,
             G_TYPE_POINTER)

query_view_signals[DOUBLE_CLICK_ENTRY] =
g_signal_new("double_click_entry",
             G_TYPE_FROM_CLASS(widget_class),
             G_SIGNAL_RUN_FIRST,
             G_STRUCT_OFFSET(GNCQueryViewClass, double_click_entry),
             None, None,
             g_cclosure_marshal_VOID__POINTER,
             G_TYPE_NONE,
             1,
             G_TYPE_POINTER)

widget_class->destroy = gnc_query_view_destroy

klass->column_toggled = None
klass->row_selected = None
klass->double_click_entry = None
}

    def select_row_cb (GtkTreeSelection *selection, gpointer user_data):
        {
        GNCQueryView *qview = GNC_QUERY_VIEW(selection.get_tree_view())
        gint number_of_rows = selection.count_selected_rows()

        g_signal_emit (qview, query_view_signals[ROW_SELECTED], 0,
                       GINT_TO_POINTER(number_of_rows))
        }

    def double_click_cb (GtkTreeView       *view,
        GtkTreePath       *path,
        GtkTreeViewColumn *column,
        gpointer           user_data)
        {
        GNCQueryView     *qview = GNC_QUERY_VIEW(view)
        GtkTreeModel     *model
        GtkTreeIter       iter
        gpointer          entry = None

        model = gtk_tree_view_get_model (GTK_TREE_VIEW(view))

        if (gtk_tree_model_get_iter (model, &iter, path))
        gtk_tree_model_get (model, &iter, 0, &entry, -1)

        g_signal_emit (qview, query_view_signals[DOUBLE_CLICK_ENTRY], 0, entry)
        }

    def toggled_cb (GtkCellRendererToggle *cell_renderer,
        gchar                 *path,
        gpointer               user_data)
        {
        GNCQueryView     *qview = GNC_QUERY_VIEW(user_data)
        GtkTreeModel     *model
        GtkTreeIter       iter
        GtkTreePath      *treepath
        gint             *indices
        gpointer          entry = None
                 toggled
        gint              column

        model = self.get_model()

        column = GPOINTER_TO_INT(g_object_get_data (G_OBJECT(cell_renderer), "column"))

        toggled = gtk_cell_renderer_toggle_get_active (cell_renderer)

        treepath = gtk_tree_path_new_from_string (path)

        if (gtk_tree_model_get_iter (model, &iter, treepath))
        {
            gtk_tree_model_get (model, &iter, 0, &entry, -1)
        indices = gtk_tree_path_get_indices (treepath)
        self.toggled_row = indices[0]
        self.toggled_column = column

        if(toggled)
        g_signal_emit (qview, query_view_signals[COLUMN_TOGGLED], 0, GINT_TO_POINTER(0))
        else
        g_signal_emit (qview, query_view_signals[COLUMN_TOGGLED], 0, GINT_TO_POINTER(1))
        }
        gtk_tree_path_free (treepath)
        }

    def destroy(self):
        if self.component_id > 0:
            ComponentManager.unregister(self.component_id)
            self.component_id = 0
        if self.query:
            self.query.destroy()
            self.query = None
        return True

   def get_num_entries(self):
        model = self.get_model()
        return model.iter_n_children(None)

  def get_selected_entry(self):
        {
        gpointer entry = None
        GList *entries = None
        gint num_entries = 0

        g_return_val_if_fail (qview != None, None)
        g_return_val_if_fail (GNC_IS_QUERY_VIEW(qview), None)

        entries = gnc_query_view_get_selected_entry_list (qview)
        if (entries)
            entry = entries->data

        num_entries = g_list_length (entries)
        if (num_entries > 1)
            PWARN ("Expected only one selected entry but found %i. "
                   "Discarding all but the first one.", num_entries)

        g_list_free (entries)

        return entry
        }

    def accumulate_entries (GtkTreeModel *model, GtkTreePath *path,
        GtkTreeIter *iter, gpointer data)
        {
        acc_data *acc_entries = (acc_data*)data
        gpointer entry = None
        GList *entries = acc_entries->entries

        gtk_tree_model_get (model, iter, 0, &entry, -1)
        entries = g_list_prepend (entries, entry)
        acc_entries->entries = entries
        }

    def get_selected_entry_list(self):
        {
        GtkTreeSelection *selection
        acc_data acc_entries
        GList *entries = None

        g_return_val_if_fail (qview != None, None)
        g_return_val_if_fail (GNC_IS_QUERY_VIEW(qview), None)

        acc_entries.entries = None
        selection = self.get_selection()
        selection.selected_foreach (selection, accumulate_entries,
        &acc_entries)
        acc_entries.entries = g_list_reverse (acc_entries.entries)
        return acc_entries.entries
        }

    def use_scroll_to_selection(self, scroll):
        self.use_scroll_to_selection = scroll

    def scroll_to_selection(self, override_scroll=False):
        if not self.use_scroll_to_selection and override_scroll:
            return
        selection = self.get_selection()
        path_list = selection.get_selected_rows()
        tree_path = path_list[-1]
        if tree_path is not None:
            self.scroll_to_cell(tree_path, None, False, 0.0, 0.0)

    def force_scroll_to_selection(self):
        self.scroll_to_selection(True)

    def refresh_selected(self, old_entry):
        model = self.get_model()
        selection = self.get_selection()
        if (g_list_length (old_entry) > 0)
            {

            for (node = old_entry node node = node->next)
            {
                gpointer pointer

            _iter = model.get_iter_first()

            while _iter is not None:
                {

                pointer = model.get_value(_iter, 0)

                if (pointer == node->data)
                {
                selection.select_iter (selection, &iter)
                break
                }
                _iter = model.iter_next(_iter)
                }
                }
                gnc_query_scroll_to_selection (qview)
                }

    def refresh(self):
        selected_entries = self.get_selected_entry_list()
        model = self.get_model()
        model.clear()
        self.fill()
        self.refresh_selected(selected_entries)

    def set_query_sort(self, new_column):
        sort_order = self.increasing
        node = g_list_nth (self.column_params, self.sort_column)
        param = node->data
        if (gnc_search_param_has_param_fcn (param))
        {
            self.refresh()
            return
        }

        if (self.numeric_inv_sort)
        {
        const char *type = gnc_search_param_get_param_type ((GNCSearchParam *) param)
        if (!g_strcmp0(type, TYPE_NUMERIC) ||
        !g_strcmp0(type, TYPE_DEBCRED))
        sort_order = !sort_order
        }


        if (new_column)
        {
        GSList *p1, *p2

        p1 = gnc_search_param_get_param_path (param)
        p2 = g_slist_prepend (None, QUERY_DEFAULT_SORT)
        qof_query_set_sort_order (self.query, p1, p2, None)
        }

        qof_query_set_sort_increasing (self.query,
        sort_order,
        sort_order,
        sort_order)
        self.refresh()

    def fill(self):
        ComponentManager.clear_watches (self.component_id)
        entries = qof_query_run (self.query)
        model = self.get_model()
        for (item = entries item item = item->next)
        {
        GList *node
        gint row = 0
        const QofParam *gup
        QofParam *qp = None


        gtk_list_store_append (GTK_LIST_STORE(model), &iter)

        gtk_list_store_set (GTK_LIST_STORE(model), &iter, 0, item->data, -1)

        for (i = 0, node = self.column_params node node = node->next)
            {
                result
        GNCSearchParamSimple *param = node->data
        GSList *converters = None
        const char *type = gnc_search_param_get_param_type ((GNCSearchParam *) param)
        gpointer res = item->data
        gchar *qofstring

        g_assert (GNC_IS_SEARCH_PARAM_SIMPLE(param))
        converters = gnc_search_param_get_converters (param)


        if (g_strcmp0 (type, TYPE_BOOLEAN) == 0)
        {
        result = (gboolean) GPOINTER_TO_INT(gnc_search_param_compute_value (param, res))
        gtk_list_store_set (GTK_LIST_STORE(model), &iter, i + 1, result, -1)
        i++
        continue
        }


        for ( converters converters = converters->next)
        {
            qp = converters->data
            if (converters->next)
                res = (qp->param_getfcn)(res, qp)
        }


        if (qp && (g_strcmp0 (type, TYPE_DEBCRED) == 0 ||
                   g_strcmp0 (type, TYPE_NUMERIC) == 0))
        {

        gnc_numeric (*nfcn)(gpointer, QofParam *) =
        (gnc_numeric(*)(gpointer, QofParam *))(qp->param_getfcn)
        gnc_numeric value = nfcn (res, qp)

        if (self.numeric_abs)
            value = gnc_numeric_abs (value)
        gtk_list_store_set (GTK_LIST_STORE(model), &iter, i + 1,
        xaccPrintAmount (value, gnc_default_print_info (False)), -1)
        }
        else
        {
        qofstring = qof_query_core_to_string (type, res, qp)
        gtk_list_store_set (GTK_LIST_STORE(model), &iter, i + 1, qofstring , -1)
        g_free (qofstring)
        }
        i++
        }
        row++

                                     gup = self.get_guid
        guid = (const GncGUID*)((gup->param_getfcn)(item->data, gup))
        ComponentManager.watch_entity (self.component_id, guid,
                                              EVENT_MODIFY | EVENT_DESTROY)
        }

    def unselect_all(self):
        selection = self.get_selection()
        selection.unselect_all()

    def item_in_view(self,  item):
        if item is None:
            return False
        model = self.get_model()
        _iter = model.get_iter_first()

        while _iter is not None:
            pointer = model.get_value(_iter, 0)
            if pointer == item:
                return True
            _iter = model.iter_next(_iter)
        return False

    def set_numerics(self, _abs, inv_sort):
        self.numeric_abs = _abs
        self.numeric_inv_sort = inv_sort
