# -*- coding: utf-8 -*-

from gasstation.models.account import *
from gasstation.models.account_types import *
from gasstation.utilities.icons import *
from libgasstation.core.session import *
from .tree import *

schema_id = "org.jonah.Gasstation.accounts"
path = "/org/gasstation/Gasstation/accounts"
ACCT_COUNT = "NumberOfOpenAccounts"
ACCT_OPEN = "OpenAccount%d"
ACCT_SELECTED = "SelectedAccount"
SHOW_HIDDEN = "ShowHidden"
SHOW_ZERO = "ShowZeroTotal"
SHOW_UNUSED = "ShowUnused"
ACCT_TYPES = "AccountTypes"

SHOW_HIDDEN_ACCOUNTS = "Show_Hidden"
SHOW_ZERO_TOTALS = "Show_ZeroTotal"
SHOW_UNUSED_ACCOUNTS = "Show_Unused"
ACCOUNT_TYPES = "Account_Types"


class BarT:
    def __init__(self):
        self.key_file = None
        self.group_name = None
        self.count = 0


SAMPLE_ACCOUNT_VALUE = "$1,000,000.00"
PREF_ACCOUNT_COLOR = "show-account-color"


class AccountViewInfo:
    def __init__(self):
        self.include_type = [True for i in range(AccountType.NUM_ACCOUNT_TYPES)]
        self.show_hidden = False


class AccountView(TreeView):
    __gtype_name__ = "AccountView"

    def __init__(self, root: Account = None, show_root: bool = False, *args, **kwargs):
        Hook.add_dangler(HOOK_CURRENCY_CHANGED, self.currency_changed_cb)
        super().__init__(*args, **kwargs)
        if root is None:
            root = Session.get_current_root()
        self.avi = weakref.ref(AccountViewInfo())
        self.filter_fn = None
        self.filter_data = None
        self.filter_destroy = None
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_ACCOUNT_COLOR, self.color_update)
        gs_widget_set_style_context(self, "AccountView")
        self.show_account_color = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_ACCOUNT_COLOR)

        self.set_headers_visible(False)
        self.name_column = self.add_text_column(0, title="Account Name", pref_name="name",
                                                icon_name=ICON_ACCOUNT, sizing_text="Expenses:Entertainment",
                                                data_col=AccountModelColumn.NAME, visible_always=True,
                                                sort_func=self.sort_by_string)

        self.add_text_column(1, title="Type", pref_name="type",
                             sizing_text=AccountType.CREDIT.to_string(), data_col=AccountModelColumn.TYPE,
                             sort_func=self.sort_by_string)
        self.add_text_column(2, title="Commodity", pref_name="commodity",
                             sizing_text=gs_default_currency().get_full_name(),
                             data_col=AccountModelColumn.COMMODITY, sort_func=self.sort_by_string)
        self.code_column = self.add_text_column(3, title="Account Code", pref_name="account-code",
                                                sizing_text="1-123-1234", data_col=AccountModelColumn.CODE,
                                                sort_func=self.sort_by_code)
        self.desc_column = self.add_text_column(4, title="Description", pref_name="description",
                                                sizing_text="Sample account description.",
                                                data_col=AccountModelColumn.DESCRIPTION,
                                                sort_func=self.sort_by_string)

        self.add_numeric_column(5, title="Last Num", pref_name="last_num", sizing_text="12345",
                                data_col=AccountModelColumn.LAST_NUM, sort_func=self.sort_by_string)

        self.add_numeric_column(6, title="Present", pref_name="present", sizing_text=SAMPLE_ACCOUNT_VALUE,
                                data_col=AccountModelColumn.PRESENT, color_col=AccountModelColumn.COLOR_PRESENT,
                                sort_func=self.sort_by_present_value)
        self.present_report_column = self.add_numeric_column(7, title="Present (Report)",
                                                             pref_name="present-report",
                                                             sizing_text=SAMPLE_ACCOUNT_VALUE,
                                                             data_col=AccountModelColumn.PRESENT_REPORT,
                                                             color_col=AccountModelColumn.COLOR_PRESENT,
                                                             sort_func=self.sort_by_present_value)

        self.add_numeric_column(8, title="Balance", pref_name="balance", sizing_text=SAMPLE_ACCOUNT_VALUE,
                                data_col=AccountModelColumn.BALANCE, color_col=AccountModelColumn.COLOR_BALANCE,
                                sort_func=self.sort_by_balance_value)
        self.balance_report_column = self.add_numeric_column(9, title="Balance (Report)",
                                                             pref_name="balance-report",
                                                             sizing_text=SAMPLE_ACCOUNT_VALUE,
                                                             data_col=AccountModelColumn.BALANCE_REPORT,
                                                             color_col=AccountModelColumn.COLOR_BALANCE,
                                                             sort_func=self.sort_by_balance_value)

        self.add_numeric_column(10, title="Balance (Period)", pref_name="balance-period",
                                sizing_text=SAMPLE_ACCOUNT_VALUE,
                                data_col=AccountModelColumn.BALANCE_PERIOD,
                                color_col=AccountModelColumn.COLOR_BALANCE_PERIOD,
                                sort_func=self.sort_by_balance_period_value)

        self.add_numeric_column(11, title="Cleared", pref_name="cleared", sizing_text=SAMPLE_ACCOUNT_VALUE,
                                data_col=AccountModelColumn.CLEARED, color_col=AccountModelColumn.COLOR_CLEARED,
                                sort_func=self.sort_by_cleared_value)
        self.cleared_report_column = self.add_numeric_column(12, title="Cleared (Report)",
                                                             pref_name="cleared-report",
                                                             sizing_text=SAMPLE_ACCOUNT_VALUE,
                                                             data_col=AccountModelColumn.CLEARED_REPORT,
                                                             color_col=AccountModelColumn.COLOR_CLEARED,
                                                             sort_func=self.sort_by_cleared_value)
        self.add_numeric_column(13, title="Reconciled",
                                pref_name="reconciled",
                                sizing_text=SAMPLE_ACCOUNT_VALUE,
                                data_col=AccountModelColumn.RECONCILED,
                                color_col=AccountModelColumn.COLOR_RECONCILED,
                                sort_func=self.sort_by_reconciled_value)
        self.reconciled_report_column = self.add_numeric_column(14, title="Reconciled (Report)",
                                                                pref_name="reconciled-report",
                                                                sizing_text=SAMPLE_ACCOUNT_VALUE,
                                                                data_col=AccountModelColumn.RECONCILED_REPORT,
                                                                color_col=AccountModelColumn.COLOR_RECONCILED,
                                                                sort_func=self.sort_by_reconciled_value)
        self.add_text_column(15, title="Last Reconcile Date", pref_name="last-recon-date",
                             sizing_text="Last Reconcile Date",
                             data_col=AccountModelColumn.RECONCILED_DATE,
                             sort_func=self.sort_by_last_reconcile_date)

        self.add_numeric_column(16, title="Future Minimum",
                                pref_name="future-min",
                                sizing_text=SAMPLE_ACCOUNT_VALUE,
                                data_col=AccountModelColumn.FUTURE_MIN,
                                color_col=AccountModelColumn.COLOR_FUTURE_MIN,
                                sort_func=self.sort_by_future_min_value)

        self.future_min_report_column = self.add_numeric_column(17, title="Future Minimum (Report)",
                                                                pref_name="future-min-report",
                                                                sizing_text=SAMPLE_ACCOUNT_VALUE,
                                                                data_col=AccountModelColumn.FUTURE_MIN_REPORT,
                                                                color_col=AccountModelColumn.COLOR_FUTURE_MIN,
                                                                sort_func=self.sort_by_future_min_value)

        self.total_column = self.add_numeric_column(18, title="Total", pref_name="total",
                                                    sizing_text=SAMPLE_ACCOUNT_VALUE,
                                                    data_col=AccountModelColumn.TOTAL,
                                                    color_col=AccountModelColumn.COLOR_TOTAL,
                                                    sort_func=self.sort_by_total_value)

        self.total_report_column = self.add_numeric_column(19, title="Total (Report)", pref_name="total-report",
                                                           sizing_text=SAMPLE_ACCOUNT_VALUE,
                                                           data_col=AccountModelColumn.TOTAL_REPORT,
                                                           color_col=AccountModelColumn.COLOR_TOTAL,
                                                           sort_func=self.sort_by_total_value)

        self.total_period_column = self.add_numeric_column(20, title="Total (Period)", pref_name="total-period",
                                                           sizing_text=SAMPLE_ACCOUNT_VALUE,
                                                           data_col=AccountModelColumn.TOTAL_PERIOD,
                                                           color_col=AccountModelColumn.COLOR_TOTAL_PERIOD,
                                                           sort_func=self.sort_by_total_period_value)

        acc_color_column = self.add_text_column(21, title="C", pref_name="account-color", sizing_text="xx")
        acc_color_column.get_button().set_tooltip_text("Account Color")
        self.set_user_data(acc_color_column, REAL_TITLE, "Account Color")
        self.notes_column = self.add_text_column(22, title="Notes", pref_name="notes",
                                                 sizing_text="Sample account notes.",
                                                 data_col=AccountModelColumn.NOTES, sort_func=self.sort_by_string)
        self.add_text_column(23, title="Tax Info", pref_name="tax-info",
                             sizing_text="Sample tax info.", data_col=AccountModelColumn.TAX_INFO,
                             sort_func=self.sort_by_string, data_func=self.tax_info_data_func)
        self.add_toggle_column(24, "Placeholder", short_title="P", pref_name="placeholder",
                               data_col=AccountModelColumn.PLACEHOLDER, sort_func=self.sort_by_placeholder,
                               edited=self.placeholder_toggled)
        self.add_toggle_column(25, title="Hidden", short_title="H", pref_name="hidden",
                               data_col=AccountModelColumn.HIDDEN,
                               sort_func=self.sort_by_hidden, edited=self.hidden_toggled)
        for col in self.get_columns():
            renderer = self.column_get_renderer(col)
            col.set_cell_data_func(renderer, self.acc_color_data_func)
        self.update_column_names()
        self.configure_columns()
        f_model = AccountModel.new(root).filter_new(Gtk.TreePath.new_first() if not show_root else None)
        s_model = Gtk.TreeModelSort.new_with_model(f_model)
        self.set_model(s_model)
        # f_model.set_visible_func(self.filter_helper)
        s_model.set_sort_column_id(AccountModelColumn.NAME, Gtk.SortType.ASCENDING)
        self.set_search_equal_func(self.search_compare)
        self.show()

    def set_selection_mode(self, mode, draggable=False):
        sel = self.get_selection()
        sel.set_mode(mode)

        if draggable:
            TARGETS = [
                Gtk.TargetEntry.new('MY_TREE_MODEL_ROW', Gtk.TargetFlags.SAME_WIDGET, 0)
            ]
            self.enable_model_drag_source(Gdk.ModifierType.BUTTON1_MASK, TARGETS,
                                          Gdk.DragAction.DEFAULT | Gdk.DragAction.MOVE)
            self.enable_model_drag_dest(TARGETS, Gdk.DragAction.DEFAULT | Gdk.DragAction.MOVE)
            self.connect("drag_data_get", self.drag_data_get)
            self.connect("drag_data_received", self.drag_data_received)

    def drag_data_get(self, treeview, context, selection, info, timestamp):
        treeselection = self.get_selection()
        model, paths = treeselection.get_selected_rows()
        iters = [model.get_iter(path) for path in paths]
        iter_str = ','.join([model.get_string_from_iter(iter) for iter in iters])
        selection.set(selection.get_target(), Account, bytes(iter_str, "utf-8"))

    def drag_data_received(self, treeview, context, x, y, selection, info, etime):
        model = treeview.get_model()
        drop_info = treeview.get_dest_row_at_pos(x, y)
        if drop_info:
            path, position = drop_info
            destination_iter = model.get_iter(path)
            destination_account = self.get_account_from_iter(model, destination_iter)
            data = selection.get_data().decode("utf-8")
            if data == '' or data is None:
                iters = []
            else:
                iters = data.split(',')
            for _iter in iters:
                if info == Account:
                    dragged_iter = model.get_iter_from_string(_iter)
                    if dragged_iter is not None and model.iter_is_valid(dragged_iter):
                        dragged_account = self.get_account_from_iter(model, dragged_iter)
                        print(dragged_account)
            treeview.stop_emission('drag_data_received')

    def currency_changed_cb(self):
        views = Tracking.get_list(self.__class__.__name__)
        for view in views:
            view.update_column_names()

    def set_column_editor(self, column, edited_cb):
        if column is None or edited_cb is None:
            return
        renderers = column.get_cells()
        for renderer in renderers:
            if isinstance(renderer, Gtk.CellRendererText):
                renderer.set_property("editable", True)
                self.setup_column_renderer_edited_cb(column, renderer, edited_cb)

    def setup_column_renderer_edited_cb(self, column, renderer, edited_cb):
        if edited_cb is None:
            renderer.set_property("editable", False)
            self.set_user_data(renderer, "column_edited_callback", edited_cb)
            s_model = self.get_model()
            renderer.disconnect_by_func(edited_cb, s_model)
            self.set_user_data(renderer, "column_view", column)

        else:
            renderer.set_property("editable", True)
            self.set_user_data(renderer, "column_edited_callback", edited_cb)
            s_model = self.get_model()
            self.reference_ids[renderer].append(renderer.connect("edited", self.col_edited_helper, s_model))
            self.set_user_data(renderer, "column_view", column)

    def col_edited_helper(self, cell, path_string, new_text, s_model):
        col_edited_cb = self.get_user_data(cell, "column_edited_callback")
        col = self.get_user_data(cell, "column_view")
        s_iter = s_model.get_iter_from_string(path_string)
        if s_iter is None:
            return
        account = self.get_account_from_iter(s_model, s_iter)
        col_edited_cb(account, col, new_text.strip())

    def acc_color_data_func(self, col, renderer, s_model, _iter, user_data):

        acc_cond_color = None
        acc_color = None
        try:
            item = s_model.get_value(_iter, AccountModelColumn.COLOR_ACCOUNT)
        except TypeError:
            return False
        if item is not None and len(item):
            acc_color = item.strip()
            color = Gdk.RGBA()
            if acc_color is not None and not Gdk.RGBA.parse(color, acc_color):
                acc_color = None
        if self.show_account_color:
            acc_cond_color = acc_color
        column_name = self.get_user_data(col, PREF_NAME)
        renderers = col.get_cells()
        if column_name != "account-color":
            acc_color = acc_cond_color
        d = {"present": AccountModelColumn.COLOR_PRESENT,
             "present_report": AccountModelColumn.COLOR_PRESENT,
             "balance": AccountModelColumn.COLOR_BALANCE,
             "balance_report": AccountModelColumn.COLOR_BALANCE,
             "balance-period": AccountModelColumn.COLOR_BALANCE_PERIOD,
             "cleared": AccountModelColumn.COLOR_CLEARED,
             "cleared_report": AccountModelColumn.COLOR_CLEARED,
             "reconciled": AccountModelColumn.COLOR_RECONCILED,
             "reconciled_report": AccountModelColumn.COLOR_RECONCILED,
             "future_min": AccountModelColumn.COLOR_FUTURE_MIN,
             "future_min_report": AccountModelColumn.COLOR_FUTURE_MIN,
             "total": AccountModelColumn.COLOR_TOTAL,
             "total_report": AccountModelColumn.COLOR_TOTAL,
             "total-period": AccountModelColumn.COLOR_TOTAL_PERIOD}
        vcol = d.get(column_name)
        if vcol is not None:
            value = s_model.get_value(_iter, vcol)
        else:
            value = None
        for cell in renderers:
            if value is not None and value == "black":
                cell.set_property("foreground-set", False)
            if acc_color is not None and len(acc_color):
                cell.set_property("cell-background", acc_color)
            else:
                cell.set_property("cell-background-set", False)

    def color_update(self, gsettings, key):
        if key == PREF_ACCOUNT_COLOR:
            self.show_account_color = gs_pref_get_bool(PREFS_GROUP_GENERAL, key)
        self.refilter()

    def clear_model_cache(self):
        model = self.get_model_from_view()
        model.clear_cache()

    def column_add_color(self, col):
        renderer = TreeView.column_get_renderer(col)
        col.set_cell_data_func(col, renderer, self.acc_color_data_func)

    def tax_info_data_func(self, col, renderer, model, _iter):
        tax_info = model.get_value(_iter, AccountModelColumn.TAX_INFO)
        path = model.get_path(_iter)
        if self.row_expanded(path):
            renderer.set_property("text", tax_info)
        else:
            tax_info_sub_acct = model.get_value(_iter, AccountModelColumn.TAX_INFO_SUB_ACCT)
            if tax_info_sub_acct == "":
                renderer.set_property("text", tax_info)
            else:
                if tax_info == "":
                    renderer.set_property("text", tax_info_sub_acct)
                else:
                    combined_tax_info = "%s; %s" % tax_info, tax_info_sub_acct
                    renderer.set_property("text", combined_tax_info)

    def placeholder_toggled(self, cell, s_path_str):
        s_path = Gtk.TreePath.new_from_string(s_path_str)
        account = self.get_account_from_path(s_path)
        if account is not None:
            placeholder = not cell.get_active()
            account.set_placeholder(placeholder)

    def hidden_toggled(self, cell, s_path_str):
        s_path = Gtk.TreePath.new_from_string(s_path_str)
        account = self.get_account_from_path(s_path)
        if account is not None:
            hidden = not cell.get_active()
            account.set_hidden(hidden)

    def set_filter(self, func, data=None, destroy=None):
        if self.filter_destroy is not None:
            self.filter_destroy(self.filter_data)
        self.filter_destroy = destroy
        self.filter_data = data
        self.filter_fn = func
        self.refilter()

    @staticmethod
    def search_compare(model, column, key, _iter):
        match = False
        case_normalized_key = None
        case_normalized_string = None
        normalized_key = GLib.utf8_normalize(key, -1, GLib.NormalizeMode.ALL)
        if normalized_key:
            case_normalized_key = GLib.utf8_casefold(normalized_key, -1)
        if case_normalized_key:
            for i in range(3):
                st = None
                if i == 0:
                    st = model.get_value(_iter, AccountModelColumn.NAME)
                elif i == 1:
                    st = model.get_value(_iter, AccountModelColumn.CODE)
                elif i == 2:
                    st = model.get_value(_iter, AccountModelColumn.DESCRIPTION)
                if st is None or len(st) == 0:
                    continue
                normalized_string = GLib.utf8_normalize(st, -1, GLib.NormalizeMode.ALL)
                if normalized_string:
                    case_normalized_string = GLib.utf8_casefold(normalized_string, -1)
                if case_normalized_string is not None and case_normalized_string.find(case_normalized_key) != -1:
                    match = True
                if match:
                    break
        return not match

    def update_column_names(self):
        mnemonic = gs_default_report_currency().get_mnemonic()
        self.present_report_column.set_title("Present ({})".format(mnemonic))
        self.balance_report_column.set_title("Balance ({})".format(mnemonic))
        self.cleared_report_column.set_title("Cleared ({})".format(mnemonic))
        self.reconciled_report_column.set_title("Reconciled ({})".format(mnemonic))
        self.future_min_report_column.set_title("Future Minimum ({})".format(mnemonic))
        self.total_report_column.set_title("Total ({})".format(mnemonic))
        self.set_show_column_menu(False)
        self.set_show_column_menu(True)

    def get_path_from_account(self, account):
        if account is None:
            return None
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()
        path = model.get_path_from_account(account)
        if path is None:
            return None
        f_path = f_model.convert_child_path_to_path(path)
        if f_path is None:
            return None
        s_path = s_model.convert_child_path_to_path(f_path)
        return s_path

    def get_iter_from_account(self, account, s_iter):
        if account is None:
            return False
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()
        _iter = model.get_iter_from_account(account)
        if _iter is None:
            return False
        f_iter = f_model.convert_child_iter_to_iter(_iter)
        if not f_iter[0]:
            return False
        s = s_model.convert_child_iter_to_iter(f_iter[1])
        if not s[0]:
            return False
        s = s[1]
        s_iter.stamp = s.stamp
        s_iter.user_data = s.user_data
        s_iter.user_data2 = s.user_data2
        s_iter.user_data3 = s.user_data3
        return True

    def count_children(self, account):
        if account is None:
            return 0
        s_iter = Gtk.TreeIter()
        if not self.get_iter_from_account(account, s_iter):
            return 0
        s_model = self.get_model()
        num_children = s_model.iter_n_children(s_iter)
        return num_children

    def get_account_from_path(self, s_path):
        s_model = self.get_model()
        f_path = s_model.convert_path_to_child_path(s_path)
        if f_path is None:
            return None
        f_model = s_model.get_model()
        path = f_model.convert_path_to_child_path(f_path)
        if f_path is None:
            return None
        model = f_model.get_model()
        _iter = model.get_iter(path)
        if _iter is None:
            return None
        account = model.get_account(_iter)
        return account

    @staticmethod
    def get_account_from_iter(s_model, s_iter):
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        account = model.get_account(_iter)
        return account

    def get_selected_account(self):
        selection = self.get_selection()
        if selection is None:
            return
        mode = selection.get_mode()
        if mode != Gtk.SelectionMode.SINGLE and mode != Gtk.SelectionMode.BROWSE:
            accts = self.get_selected_accounts()
            return accts[-1] if accts is not None and len(accts) else None
        s_model, s_iter = selection.get_selected()
        if s_model is None or s_iter is None:
            return None
        return self.get_account_from_iter(s_model, s_iter)

    def restore_expanded_row(self, account_name):
        root = Session.get_current_root()
        account = root.lookup_by_full_name(account_name)
        if account is not None:
            self.expand_to_account(account)

    def save(self, fd, key_file, group_name):
        if key_file is None or group_name is None:
            return
        key_file.set_integer(group_name, ACCT_TYPES, fd.visible_types)
        key_file.set_boolean(group_name, SHOW_HIDDEN, fd.show_hidden)
        key_file.set_boolean(group_name, SHOW_ZERO, fd.show_zero_total)
        key_file.set_boolean(group_name, SHOW_UNUSED, fd.show_unused)
        bar = BarT()
        bar.key_file = key_file
        bar.group_name = group_name
        bar.count = 0
        self.save_selected_row(bar)
        self.map_expanded_rows(self.save_expanded_row, bar)
        key_file.set_integer(group_name, ACCT_COUNT, bar.count)

    def save_expanded_row(self, tree, path, bar):
        account = self.get_account_from_path(path)
        if account is None:
            return
        account_name = account.get_full_name()
        if account_name is None:
            return
        bar.count += 1
        key = ACCT_OPEN % bar.count
        bar.key_file.set_string(bar.group_name, key, account_name)

    def restore_selected_row(self, account_name):
        root = Session.get_current_root()
        account = root.lookup_by_full_name(account_name)
        if account is not None:
            self.set_selected_account(account)

    def save_selected_row(self, bar):
        account = self.get_selected_account()
        if account is None:
            return
        account_name = account.get_full_name()
        if account_name is None:
            return
        bar.key_file.set_string(bar.group_name, ACCT_SELECTED, account_name)

    def restore(self, fd, key_file, group_name):
        try:
            show = key_file.get_boolean(group_name, SHOW_HIDDEN)
        except GLib.Error:
            show = True
        fd.show_hidden = show
        try:
            show = key_file.get_boolean(group_name, SHOW_ZERO)
        except GLib.Error:
            show = True
        fd.show_zero_total = show
        try:
            show = key_file.get_boolean(group_name, SHOW_UNUSED)
        except GLib.Error:
            show = True
        fd.show_unused = show
        try:
            i = key_file.get_integer(group_name, ACCT_TYPES)
        except GLib.Error:
            i = -1
        fd.visible_types = i

        try:
            count = key_file.get_integer(group_name, ACCT_COUNT)
            for i in range(1, count + 1, 1):
                key = ACCT_OPEN % i
                try:
                    value = key_file.get_string(group_name, key)
                    self.restore_expanded_row(value)
                except KeyError:
                    continue
        except GLib.Error:
            pass

        try:
            value = key_file.get_string(group_name, ACCT_SELECTED)
            self.restore_selected_row(value)
        except GLib.Error:
            pass
        self.refilter()

    def set_selected_account(self, account):
        selection = self.get_selection()
        if account is None or selection is None:
            return
        selection.unselect_all()
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()
        path = model.get_path_from_account(account)
        if path is None:
            return
        f_path = f_model.convert_child_path_to_path(path)
        if f_path is None:
            return
        s_path = s_model.convert_child_path_to_path(f_path)
        if s_path is None:
            return
        parent_path = s_path.copy()
        if parent_path.up():
            self.expand_to_path(parent_path)
        selection.select_path(s_path)
        while Gtk.events_pending():
            Gtk.main_iteration()
        self.scroll_to_cell(s_path, None, False, 0.0, 0.0)

    def expand_to_account(self, account):
        path = self.get_path_from_account(account)
        if path is not None:
            self.expand_to_path(path)

    def get_cursor_account(self):
        s_path, col = self.get_cursor()
        if s_path is None:
            return None
        account = self.get_account_from_path(s_path)
        return account

    @staticmethod
    def name_edited_cb(account, col, new_name):
        if new_name and len(new_name) > 0 and new_name != account.get_name():
            parent = account.get_parent()
            if parent.lookup_by_name(new_name) is not None:
                return
            account.set_name(new_name)

    @staticmethod
    def code_edited_cb(account, col, new_code):
        if account.get_code() != new_code:
            account.set_code(new_code)

    @staticmethod
    def description_edited_cb(account, col, new_desc):
        if account.get_description() != new_desc:
            account.set_description(new_desc)

    @staticmethod
    def notes_edited_cb(account, col, new_code):
        if account.get_notes() != new_code:
            account.set_notes(new_code)

    def set_name_edited(self, edited_cb):
        self.set_column_editor(self.name_column, edited_cb)

    def set_code_edited(self, edited_cb):
        self.set_column_editor(self.code_column, edited_cb)

    def set_desc_edited(self, edited_cb):
        self.set_column_editor(self.desc_column, edited_cb)

    def set_notes_edited(self, edited_cb):
        self.set_column_editor(self.notes_column, edited_cb)

    @staticmethod
    def sort_cb_setup_w_iters(f_model, f_iter_a, f_iter_b):
        model = f_model.get_model()
        iter_a = f_model.convert_iter_to_child_iter(f_iter_a)
        iter_b = f_model.convert_iter_to_child_iter(f_iter_b)
        account_a = model.get_account(iter_a)
        account_b = model.get_account(iter_b)
        return model, iter_a, iter_b, account_a, account_b

    @staticmethod
    def sort_cb_setup(f_model, f_iter_a, f_iter_b):
        c = AccountView.sort_cb_setup_w_iters(f_model, f_iter_a, f_iter_b)
        return c[3], c[4]

    @staticmethod
    def sort_by_last_reconcile_date(f_model, f_iter1, f_iter2):
        account1, account2 = AccountView.sort_cb_setup(f_model, f_iter1, f_iter2)
        account1_date = account1.get_reconcile_last_date()
        account2_date = account2.get_reconcile_last_date()
        if account1_date < account2_date:
            return -1
        elif account1_date > account2_date:
            return 1
        else:
            return account1.order(account2)

    @staticmethod
    def sort_by_string(f_model, f_iter1, f_iter2, column):
        model, iter1, iter2, account1, account2 = AccountView.sort_cb_setup_w_iters(f_model, f_iter1, f_iter2)
        if account1 is None or account2 is None:
            return -1
        st1 = model.get_value(iter1, column)
        st2 = model.get_value(iter2, column)
        result = GLib.utf8_collate(st1, st2)
        if result != 0:
            return result
        return account1.order(account2)

    @staticmethod
    def sort_by_code(f_model, f_iter1, f_iter2, column):
        account1, account2 = AccountView.sort_cb_setup(f_model, f_iter1, f_iter2)
        return account1.order(account2)

    @staticmethod
    def sort_by_xxx_value(fn, recurse, f_model, f_iter_a, f_iter_b):
        account_a, account_b = AccountView.sort_cb_setup(f_model, f_iter_a, f_iter_b)
        balance_a = gs_ui_account_get_balance_full(fn, account_a, recurse, None)[0]
        balance_b = gs_ui_account_get_balance_full(fn, account_b, recurse, None)[0]
        if balance_a != balance_b:
            return balance_a - balance_b
        return account_a.order(account_b)

    @staticmethod
    def sort_by_present_value(f_model, f_iter_a, f_iter_b, column):
        return AccountView.sort_by_xxx_value(Account.get_present_balance_in_currency, True,
                                             f_model, f_iter_a, f_iter_b)

    @staticmethod
    def sort_by_balance_value(f_model, f_iter_a, f_iter_b, column):
        return AccountView.sort_by_xxx_value(Account.get_balance_in_currency, True,
                                             f_model, f_iter_a, f_iter_b)

    @staticmethod
    def sort_by_cleared_value(f_model, f_iter_a, f_iter_b, column):
        return AccountView.sort_by_xxx_value(Account.get_cleared_balance_in_currency, True,
                                             f_model, f_iter_a, f_iter_b)

    @staticmethod
    def sort_by_reconciled_value(f_model, f_iter_a, f_iter_b):
        return AccountView.sort_by_xxx_value(Account.get_reconciled_balance_in_currency, True,
                                             f_model, f_iter_a, f_iter_b)

    @staticmethod
    def sort_by_future_min_value(f_model, f_iter_a, f_iter_b, column):
        return AccountView.sort_by_xxx_value(Account.get_projected_minimum, True,
                                             f_model, f_iter_a, f_iter_b)

    @staticmethod
    def sort_by_total_value(f_model, f_iter_a, f_iter_b, column):
        return AccountView.sort_by_xxx_value(Account.get_balance_in_currency, True,
                                             f_model, f_iter_a, f_iter_b)

    @staticmethod
    def sort_by_placeholder(f_model, f_iter_a, f_iter_b, column):
        account_a, account_b = AccountView.sort_cb_setup(f_model, f_iter_a, f_iter_b)
        flag_a = account_a.get_placeholder()
        flag_b = account_b.get_placeholder()

        if flag_a > flag_b:
            return -1
        elif flag_a < flag_b:
            return 1
        return account_a.order(account_b)

    @staticmethod
    def sort_by_hidden(f_model, f_iter_a, f_iter_b, column):
        account_a, account_b = AccountView.sort_cb_setup(f_model, f_iter_a, f_iter_b)
        flag_a = account_a.get_hidden()
        flag_b = account_b.get_hidden()
        if flag_a > flag_b:
            return -1
        elif flag_a < flag_b:
            return 1
        return account_a.order(account_b)

    @staticmethod
    def sort_by_xxx_period_value(f_model, f_iter_a, f_iter_b, recurse):
        acct1, acct2 = AccountView.sort_cb_setup(f_model, f_iter_a, f_iter_b)
        t1 = AccountingPeriod.fiscal_start()
        t2 = AccountingPeriod.fiscal_end()
        b1 = acct1.get_balance_change_for_period(t1, t2, recurse)
        b2 = acct2.get_balance_change_for_period(t1, t2, recurse)
        if b1 != b2:
            return b1 - b2
        return acct1.order(acct2)

    @staticmethod
    def sort_by_balance_period_value(f_model, f_iter_a, f_iter_b, column):
        return AccountView.sort_by_xxx_period_value(f_model, f_iter_a, f_iter_b, False)

    @staticmethod
    def sort_by_total_period_value(f_model, f_iter_a, f_iter_b, column):
        return AccountView.sort_by_xxx_period_value(f_model, f_iter_a, f_iter_b, True)

    @staticmethod
    def restore_filter(fd, key_file, group_name):
        if key_file is None or group_name is None: return
        try:
            show = key_file.get_boolean(group_name, SHOW_HIDDEN_ACCOUNTS)
        except GLib.Error:
            show = True

        fd.show_hidden = show
        try:
            show = key_file.get_boolean(group_name, SHOW_ZERO_TOTALS)
        except GLib.Error:
            show = True
        fd.show_zero_total = show
        try:
            show = key_file.get_boolean(group_name, SHOW_UNUSED_ACCOUNTS)
        except GLib.Error:
            show = True
        fd.show_unused = show
        try:
            fd.visible_types = key_file.get_integer(group_name, ACCOUNT_TYPES)
        except GLib.Error:
            pass

    @staticmethod
    def save_filter(fd, key_file, group_name):
        if key_file is None or group_name is None: return
        key_file.set_integer(group_name, ACCOUNT_TYPES, fd.visible_types)
        key_file.set_boolean(group_name, SHOW_HIDDEN_ACCOUNTS,
                             fd.show_hidden)
        key_file.set_boolean(group_name, SHOW_ZERO_TOTALS,
                             fd.show_zero_total)
        key_file.set_boolean(group_name, SHOW_UNUSED_ACCOUNTS,
                             fd.show_unused)
        key_file.set_comment(group_name, ACCOUNT_TYPES, "Account Filter Section below, four lines")

    def get_view_info(self):
        return self.avi()

    def set_view_info(self, avi: AccountViewInfo):
        sel_bits = 0
        if avi is None: return

        for i in range(AccountType.NUM_ACCOUNT_TYPES):
            sel_bits |= (1 << i) if avi.include_type[i] else 0

        self.set_filter(self.filter_by_view_info, avi, None)
        self.avi = weakref.ref(avi)

    def filter_helper(self, model, _iter, *args):
        if _iter is None:
            return True
        account = model.get_account(_iter)
        if self.filter_fn is not None and account is not None:
            if self.filter_data is not None:
                return self.filter_fn(account, self.filter_data)
            else:
                return self.filter_fn(account)
        else:
            return True

    @staticmethod
    def filter_by_view_info(acct, avi):
        if acct is None or not isinstance(acct, Account): return False
        acct_type = acct.get_type()
        if not avi.include_type[acct_type]:
            return False
        if not avi.show_hidden and acct.get_hidden():
            return False
        return True

    def refilter(self):
        s_model = self.get_model()
        if s_model is None:
            return False
        f_model = s_model.get_model()
        f_model.clear_cache()
        f_model.refilter()

    def get_selected_accounts_helper(self, s_model, s_path, s_iter, return_list):
        account = self.get_account_from_iter(s_model, s_iter)
        return_list.append(account)

    def get_selected_accounts(self):
        return_list = []
        selection = self.get_selection()
        selection.selected_foreach(self.get_selected_accounts_helper, return_list)
        return return_list

    def set_selected_accounts(self, account_list, show_last):
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()
        selection = self.get_selection()
        selection.unselect_all()
        self.collapse_all()
        total = len(account_list)
        for i, account in enumerate(account_list):
            if account is None:
                continue
            path = model.get_path_from_account(account)
            if path is None:
                continue
            f_path = f_model.convert_child_path_to_path(path)
            if f_path is None:
                continue
            s_path = s_model.convert_child_path_to_path(f_path)
            if s_path is None:
                continue
            parent_path = s_path.copy()
            if parent_path.up():
                self.expand_to_path(parent_path)
            selection.select_path(s_path)
            if show_last and i == total - 1:
                self.scroll_to_cell(s_path, None, False, 0.0, 0.0)

    def select_sub_accounts(self, account):
        have_end = False
        if account is None:
            return
        si_account = Gtk.TreeIter()
        if not self.get_iter_from_account(account, si_account):
            return
        s_model = self.get_model()
        num_children = s_model.iter_n_children(si_account)
        if num_children == 0:
            return
        sp_account = s_model.get_path(si_account)
        self.expand_row(sp_account, True)
        si_start = s_model.iter_nth_child(si_account, 0)
        have_start = si_start is not None
        si_end = si_account
        while num_children:
            tmp_iter = si_end.copy()
            si_end = s_model.iter_nth_child(tmp_iter, num_children - 1)
            have_end = si_end is not None
            if have_end:
                num_children = s_model.iter_n_children(si_end)
            else:
                num_children = 0
        if have_start and have_end:
            sp_start = s_model.get_path(si_start)
            sp_end = s_model.get_path(si_end)
            selection = self.get_selection()
            selection.select_range(sp_start, sp_end)

    def dispose_cb(self, widget):
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL, PREF_ACCOUNT_COLOR, self.color_update)
        if self.filter_destroy is not None:
            self.filter_destroy(*self.filter_data)
            self.filter_destroy = None
        self.filter_fn = None
        self.avi = None
        self.name_column = None
        self.desc_column = None
        self.present_report_column = None
        self.reconciled_report_column = None
        self.future_min_report_column = None
        Hook.remove_dangler(HOOK_CURRENCY_CHANGED, self.currency_changed_cb)
        model = self.get_model_from_view()
        views = Tracking.get_list(self.__class__.__name__)
        users = -1
        for view in views:
            if view.get_model_from_view() == model:
                users += 1

        super().dispose_cb(widget)
        for col in self.get_columns():
            renderer = self.column_get_renderer(col)
            col.set_cell_data_func(renderer, None)
        if users == 0:
            model.dispose_cb()
        return True


GObject.type_register(AccountView)
