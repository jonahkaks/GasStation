DIALOG_PREFERENCES_CM_CLASS = "dialog-newpreferences"
PREFS_GROUP = "dialogs.preferences"
PREF_PREFIX_LEN = len("pref/")
PREFS_WIDGET_HASH = "prefs_widget_hash"
NOTEBOOK = "notebook"
from functools import cmp_to_key

from gasstation.utilities.custom_dialogs import show_warning
from gasstation.utilities.ui import *
from gasstation.views.selections.currency import CurrencySelection
from gasstation.views.selections.period_select import *
from libgasstation.core.component import *


class AddOns:
    def __init__(self, filename=None, widget_name=None, tab_name=None, full_page=False, cl=None):
        self.filename = filename
        self.widget_name = widget_name
        self.tab_name = tab_name
        self.full_page = full_page
        self.caller_class = cl


class CopyData:
    def __init__(self, grid_from=None, grid_to=None, cols=0, row=0):
        self.grid_from = grid_from
        self.grid_to = grid_to
        self.cols = cols
        self.rows = row


@Gtk.Template.from_resource("/org/jonah/Gasstation/gtkbuilder/preferences.ui")
class PreferencesDialog(Gtk.Dialog):
    __gtype_name__ = "PreferencesDialog"
    addins = []
    sample_account = Gtk.Template.Child()
    separator_error = Gtk.Template.Child()
    save_on_close_wait_time = Gtk.Template.Child("pref/general/save-on-close-wait-time")
    notebook: Gtk.Notebook = Gtk.Template.Child("notebook1")
    start_period_box: Gtk.Box = Gtk.Template.Child("pref/" + PREFS_GROUP_ACCT_SUMMARY + "/" + PREF_START_PERIOD)
    end_period_box: Gtk.Box = Gtk.Template.Child("pref/" + PREFS_GROUP_ACCT_SUMMARY + "/" + PREF_END_PERIOD)
    start_date_box: Gtk.Box = Gtk.Template.Child("pref/" + PREFS_GROUP_ACCT_SUMMARY + "/" + PREF_START_DATE)
    end_date_box: Gtk.Box = Gtk.Template.Child("pref/" + PREFS_GROUP_ACCT_SUMMARY + "/" + PREF_END_DATE)
    default_currency_box: Gtk.Box = Gtk.Template.Child("pref/" + PREFS_GROUP_GENERAL + "/" + PREF_CURRENCY_OTHER)
    report_currency_box: Gtk.Box = Gtk.Template.Child(
        "pref/" + PREFS_GROUP_GENERAL_REPORT + "/" + PREF_CURRENCY_OTHER)
    assoc_head:Gtk.Box  = Gtk.Template.Child("pref/" + PREFS_GROUP_GENERAL + "/assoc-head")
    path_head_error = Gtk.Template.Child()
    date_formats = Gtk.Template.Child()
    locale_currency = Gtk.Template.Child()
    locale_currency2 = Gtk.Template.Child()
    save_on_close_expires = Gtk.Template.Child("pref/general/save-on-close-expires")

    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        date_is_valid = False
        gs_widget_set_style_context(self, "PreferenceDialog")
        self.set_transient_for(parent)
        self.prefs_table = {}
        book = Session.get_current_book()
        fy_end = book.get_fiscal_year_end()
        period = PeriodSelect(True)
        period.show()
        self.start_period_box.pack_start(period, True, True, 0)
        if date_is_valid:
            period.set_fy_end(fy_end)
        period = PeriodSelect(False)
        period.show()
        self.end_period_box.pack_start(period, True, True, 0)
        if date_is_valid:
            period.set_fy_end(fy_end)

        date = DateSelection()
        date.show()
        self.start_date_box.pack_start(date, True, True, 0)
        date = DateSelection()
        date.show()
        self.end_date_box.pack_start(date, True, True, 0)

        currency = CurrencySelection()
        currency.set_currency(gs_default_currency())
        currency.show()
        self.default_currency_box.pack_start(currency, True, True, 0)

        currency = CurrencySelection()
        currency.set_currency(gs_default_currency())
        currency.show()
        self.report_currency_box.pack_start(currency, True, True, 0)
        self.report_currency_box.show()

        fcb = Gtk.FileChooserButton.new("Select a folder", Gtk.FileChooserAction.SELECT_FOLDER)
        self.assoc_head.pack_start(fcb, True, True, 0)
        button = Gtk.Button.new_with_label("Clear")
        self.assoc_head.pack_start(button, True, True, 0)
        button.show()
        self.separator_error.hide()
        self.path_head_error.hide()
        self.file_chooser_clear = button
        self.build_widget_table()
        for add_in in self.__class__.addins:
            self.build_page(add_in)
        self.sort_pages(self.notebook)
        self.notebook.set_current_page(0)
        gs_pref_block_all()
        for k, v in self.prefs_table.items():
            self.connect_one(k, v)
        gs_pref_unblock_all()
        buf = datetime.date(day=31, month=7, year=2013).strftime("%x")
        path = Gtk.TreePath.new_from_indices([DateFormat.LOCALE, -1])
        _iter = self.date_formats.get_iter(path)
        if _iter:
            self.date_formats.set_value(_iter, 1, buf)
        locale_currency = gs_locale_default_currency()
        currency_name = locale_currency.get_printname()
        self.locale_currency.set_label(currency_name)
        self.locale_currency2.set_label(currency_name)
        self.save_on_close_expires_cb(self.save_on_close_expires)
        gs_restore_window_size(PREFS_GROUP, self, parent)
        self.component_id = ComponentManager.register(DIALOG_PREFERENCES_CM_CLASS, None, self.close_handler)
        self.connect("response", self.response_cb)
        self.show()

    @staticmethod
    def account_separator_is_valid(separator):
        message = None
        book = Session.get_current_book()
        normalized_separator = gs_normalize_account_separator(separator)
        conflict_accts = Account.list_name_violations(book, normalized_separator)
        if any(conflict_accts):
            message = Account.name_violations_errmsg(normalized_separator, conflict_accts)
        return message, normalized_separator

    @Gtk.Template.Callback()
    def account_separator_pref_changed_cb(self, entry):
        conflict_msg, separator = self.account_separator_is_valid(entry.get_text())
        sample = "Income%sSalary%sTaxable" % (separator, separator)
        self.sample_account.set_text(sample)
        if conflict_msg is not None:
            self.separator_error.set_tooltip_text(conflict_msg)
            self.separator_error.show()
        else:
            self.separator_error.hide()

    def account_separator_validate_cb(cls, entry, _, dialog):
        conflict_msg, separator = cls.account_separator_is_valid(entry.get_text())
        if conflict_msg:
            show_warning(dialog, conflict_msg)
        return False

    @Gtk.Template.Callback()
    def save_on_close_expires_cb(self, button):
        self.save_on_close_wait_time.set_sensitive(button.get_active())

    @staticmethod
    def compare_addins(a, b):
        return GLib.utf8_collate(a.tab_name, b.tab_name)

    @classmethod
    def add_page_internal(cls, filename, widget_name, tab_name, full_page, cl=None):
        add_in = AddOns(filename, widget_name, tab_name, full_page, cl)
        error = False
        if not add_in.filename or not add_in.widget_name or not add_in.tab_name:
            return
        preexisting = None
        ls = list(filter(lambda x: cls.compare_addins(x, add_in), cls.addins))
        if any(ls):
            preexisting = ls[0]
        if preexisting:
            if preexisting.full_page:
                error = True
            elif add_in.full_page:
                error = True
        if error:
            return
        else:
            cls.addins.append(add_in)

    @classmethod
    def add_page(cls, filename, widget_name, tab_name, cl=None):
        cls.add_page_internal(filename, widget_name, tab_name, True, cl)

    @classmethod
    def add_to_page(cls, filename, widget_name, tab_name, cl=None):
        cls.add_page_internal(filename, widget_name, tab_name, False, cl)

    @classmethod
    def get_all_objects(cls, widget, interesting):
        for child in widget.get_children():
            interesting.append(child)
            if isinstance(child, Gtk.Container):
                cls.get_all_objects(child, interesting)

    def build_widget_table(self, builder=None):
        if builder is not None:
            interesting = builder.get_objects()
        else:
            interesting = []
            self.get_all_objects(self, interesting)
        for widget in interesting:
            try:
                name = Gtk.Buildable.get_name(widget)
                if name.startswith("pref"):
                    self.prefs_table[name] = widget
            except (AttributeError, TypeError):
                pass

    @staticmethod
    def find_page(notebook, name):
        if notebook is None or name is None:
            return
        n_pages = notebook.get_n_pages()
        for i in range(n_pages):
            child = notebook.get_nth_page(i)
            child_name = notebook.get_tab_label_text(child)
            if GLib.utf8_collate(name, child_name) == 0:
                return child
        return None

    @staticmethod
    def get_grid_size(child, copydata):
        left, top, height, width = copydata.grid_to.child_get(child, "left-attach", "top-attach", "height", "width")
        if left + width >= copydata.cols:
            copydata.cols = left + width
        if top + height >= copydata.rows:
            copydata.rows = top + height

    @staticmethod
    def move_grid_entry(child, copydata):
        left, top, height, width = copydata.grid_from.child_get(child, "left-attach", "top-attach", "height", "width")
        hexpand = child.get_hexpand()
        vexpand = child.get_vexpand()
        halign = child.get_halign()
        valign = child.get_valign()
        topm = child.get_property("margin-top")
        bottomm = child.get_property("margin-bottom")
        leftm = child.get_property("margin-left")
        rightm = child.get_property("margin-right")
        copydata.grid_from.remove(child)
        copydata.grid_to.attach(child, left, copydata.rows + top, width, height)
        child.set_hexpand(hexpand)
        child.set_vexpand(vexpand)
        child.set_halign(halign)
        child.set_valign(valign)
        child.set_property("margin-left", leftm)
        child.set_property("margin-right", rightm)
        child.set_property("margin-top", topm)
        child.set_property("margin-bottom", bottomm)

    def build_page(self, add_in: AddOns):
        copydata = CopyData()
        builder = Gtk.Builder()
        widget_name = add_in.widget_name.split(",")
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/" + add_in.filename, widget_name)
        new_content = builder.get_object(widget_name[-1])
        self.build_widget_table(builder)
        # if add_in.caller_class is not None:
        #     builder.connect_signals(add_in.caller_class)
        if new_content is None:
            return
        notebook = self.notebook
        if add_in.full_page:
            label = Gtk.Label.new(add_in.tab_name)
            gs_label_set_alignment(label, 0.0, 0.5)
            notebook.append_page(new_content, label)
            return
        if not isinstance(new_content, Gtk.Grid):
            return
        existing_content = self.find_page(notebook, add_in.tab_name)
        if not existing_content:
            existing_content = Gtk.Grid()
            existing_content.set_border_width(6)
            label = Gtk.Label(add_in.tab_name)
            gs_label_set_alignment(label, 0.0, 0.5)
            notebook.append_page(existing_content, label)
            existing_content.show()

        else:
            copydata.grid_to = existing_content
            existing_content.foreach(self.get_grid_size, copydata)
        if copydata.rows > 0:
            label = Gtk.Label("")
            label.show()
            existing_content.attach(label, 0, copydata.rows, 1, 1)
            copydata.rows += 1
        copydata.grid_from = new_content
        copydata.grid_to = existing_content
        new_content.foreach(self.move_grid_entry, copydata)

    @staticmethod
    def tab_cmp(page_a, page_b, notebook):
        return GLib.utf8_collate(notebook.get_tab_label_text(page_a), notebook.get_tab_label_text(page_b))

    @classmethod
    def sort_pages(cls, notebook):
        tabs = []
        n_pages = notebook.get_n_pages()
        for i in range(n_pages - 1, 0, -1):
            tabs.insert(0, notebook.get_nth_page(i))
        for i, tab in enumerate(sorted(tabs, key=cmp_to_key(lambda a, b: cls.tab_cmp(a, b, notebook)))):
            notebook.reorder_child(tab, i)

    @staticmethod
    def split_widget_name(name):
        return name.split("/")[1:]

    def connect_font_button(self, fb):
        group, pref = self.split_widget_name(Gtk.Buildable.get_name(fb))
        gs_pref_bind(group, pref, fb, "font-name")
        fb.show()

    def file_chooser_selected_cb(self, fc, group, pref):
        folder = fc.get_uri()
        if not folder.endswith("/"):
            folder_with_slash = folder + "/"
            folder = folder_with_slash
        self.path_head_error.hide()
        gs_pref_set_string(group, pref, folder)

    def connect_file_chooser_button(self, fcb, box_name):
        folder_set = True
        if fcb is None: return
        if box_name is None:
            group, pref = self.split_widget_name(Gtk.Buildable.get_name(fcb))
        else:
            group, pref = self.split_widget_name(box_name)
        uri = gs_pref_get_string(group, pref)
        if uri and len(uri) > 0:
            path_head, hostname = GLib.filename_from_uri(uri)
            if GLib.file_test(path_head, GLib.FileTest.IS_DIR):
                fcb.set_current_folder_uri(uri)
            else:
                folder_set = False
        if folder_set:
            self.path_head_error.hide()
        else:
            uri_u = GLib.uri_unescape_string(uri)
            path_head, hostname = GLib.filename_from_uri(uri_u)
            ttip = "Path does not exist, " + path_head
            self.path_head_error.set_tooltip_text(ttip)
            self.path_head_error.show()
        fcb.connect("selection-changed", self.file_chooser_selected_cb, group, pref)
        self.file_chooser_clear.connect("clicked", self.file_chooser_clear_cb, fcb, group, pref)
        fcb.show()

    def file_chooser_clear_cb(self, button, fcb, group, pref):
        gs_pref_set_string(group, pref, "")
        fcb.unselect_all()

    def connect_radio_button(self, button):
        group, pref = self.split_widget_name(Gtk.Buildable.get_name(button))
        gs_pref_bind(group, pref, button, "active")

    def connect_check_button(self, button):
        group, pref = self.split_widget_name(Gtk.Buildable.get_name(button))
        gs_pref_bind(group, pref, button, "active")

    def connect_spin_button(self, spin):
        group, pref = self.split_widget_name(Gtk.Buildable.get_name(spin))
        gs_pref_bind(group, pref, spin, "value")

    def connect_combo_box(self, box):
        group, pref = self.split_widget_name(Gtk.Buildable.get_name(box))
        gs_pref_bind(group, pref, box, "active")
        box.show()

    def connect_currency_edit(self, gce, boxname):
        group, pref = self.split_widget_name(boxname)
        gs_pref_bind(group, pref, gce, "mnemonic")
        gce.show()

    def connect_entry(self, entry):
        group, pref = self.split_widget_name(Gtk.Buildable.get_name(entry))
        gs_pref_bind(group, pref, entry, "text")

    def connect_period_select(self, period, boxname):
        group, pref = self.split_widget_name(boxname)
        gs_pref_bind(group, pref, period, "active")

    def connect_date_edit(self, gde, boxname):
        group, pref = self.split_widget_name(boxname)
        gs_pref_bind(group, pref, gde, "time")

    @Gtk.Template.Callback()
    def delete_event_cb(self, *args):
        return True

    def response_cb(self, dialog, response):
        if response == Gtk.ResponseType.HELP:
            pass
        else:
            gs_save_window_size(PREFS_GROUP, dialog)
            ComponentManager.unregister(self.component_id)
            dialog.destroy()

    def connect_one(self, name, widget):
        if isinstance(widget, Gtk.FontButton):
            self.connect_font_button(widget)
        elif isinstance(widget, Gtk.FileChooserButton):
            self.connect_file_chooser_button(widget, None)
        elif isinstance(widget, Gtk.RadioButton):
            self.connect_radio_button(widget)
        elif isinstance(widget, Gtk.CheckButton):
            self.connect_check_button(widget)
        elif isinstance(widget, Gtk.SpinButton):
            self.connect_spin_button(widget)
        elif isinstance(widget, Gtk.ComboBox):
            self.connect_combo_box(widget)
        elif isinstance(widget, Gtk.Entry):
            self.connect_entry(widget)
        elif isinstance(widget, Gtk.Box):
            child = widget.get_children()
            widget_child = child[0]
            if isinstance(widget_child, CurrencySelection):
                self.connect_currency_edit(widget_child, name)
            elif isinstance(widget_child, PeriodSelect):
                self.connect_period_select(widget_child, name)
            elif isinstance(widget_child, DateSelection):
                self.connect_date_edit(widget_child, name)
            elif isinstance(widget_child, Gtk.FileChooserButton):
                self.connect_file_chooser_button(widget_child, name)

    @staticmethod
    def show_handler(self, class_name, component_id):
        self.present()
        return True

    def close_handler(self):
        ComponentManager.unregister(self.component_id)
        self.destroy()


GObject.type_register(PreferencesDialog)
