from gasstation.utilities.cursors import gs_set_busy_cursor, gs_unset_busy_cursor
from gasstation.utilities.dialog import gs_widget_remove_style_context
from gasstation.views.selections.account import AccountSelection
from gasstation.views.selections.amount_edit import *
from libgasstation.core.component import *
from libgasstation.core.tax import *

DIALOG_TAX_TABLE_CM_CLASS = "tax-table-dialog"
PREFS_GROUP = "dialogs.business.tax-tables"


class TaxCol(GObject.GEnum):
    NAME = 0
    POINTER = 1
    NUM = 2


class TaxEntryCol(GObject.GEnum):
    NAME = 0
    POINTER = 1
    NUM = 2


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/tax/new.ui")
class NewTaxDialog(Gtk.Dialog):
    __gtype_name__ = "NewTaxDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()
    type_combo: Gtk.ComboBoxText = Gtk.Template.Child()
    tax_grid: Gtk.Grid = Gtk.Template.Child()
    rate_label: Gtk.Label = Gtk.Template.Child()
    upper_scale_label: Gtk.Label = Gtk.Template.Child()
    lower_scale_label: Gtk.Label = Gtk.Template.Child()
    account_label: Gtk.Label = Gtk.Template.Child()
    ok_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, parent, book, table=None, entry: TaxEntry = None, name=None):
        super().__init__()
        self.book = book
        self.set_transient_for(parent)
        self.created_table = table
        self.created_entry = entry
        if entry is not None and table is None:
            self.created_table = entry.get_table()
        self.type = TaxAmountType.PERCENT
        gs_widget_set_style_context(self, "NewTaxDialog")
        if name is not None:
            self.name_entry.set_text(name)

        self.rate_entry = AmountEntry()
        self.rate_entry.connect("amount_changed", self.validate)
        self.rate_entry.set_evaluate_on_enter(True)
        self.tax_grid.attach(self.rate_entry, 1, 4, 1, 1)
        self.rate_entry.show()

        self.lower_scale_entry = AmountEntry()
        self.lower_scale_entry.set_evaluate_on_enter(True)
        self.lower_scale_entry.connect("amount_changed", self.validate)
        self.tax_grid.attach(self.lower_scale_entry, 1, 5, 1, 1)
        self.lower_scale_entry.show()

        self.upper_scale_entry = AmountEntry()
        self.upper_scale_entry.connect("amount_changed", self.validate)
        self.upper_scale_entry.set_evaluate_on_enter(True)
        self.tax_grid.attach(self.upper_scale_entry, 1, 6, 1, 1)
        self.upper_scale_entry.show()

        self.account_selection = AccountSelection()
        self.account_selection.connect("selection_changed", self.validate)
        self.account_selection.set_new_account_ability(True)
        self.tax_grid.attach(self.account_selection, 1, 7, 1, 1)
        self.account_selection.show()

        self.rate_label.set_mnemonic_widget(self.rate_entry)
        self.lower_scale_label.set_mnemonic_widget(self.lower_scale_entry)
        self.upper_scale_label.set_mnemonic_widget(self.upper_scale_entry)
        self.account_label.set_mnemonic_widget(self.account_selection)

        if table is not None:
            self.tax_grid.remove_row(0)
            self.tax_grid.remove_row(0)
            self.rate_entry.grab_focus()

        if entry is not None:
            self.type = entry.get_type()
            self.type_combo.set_active(int(self.type))
            self.rate_entry.set_amount(entry.get_rate())
            self.upper_scale_entry.set_amount(entry.get_upper_scale())
            self.upper_scale_entry.set_amount(entry.get_lower_scale())
            self.account_selection.set_account(entry.get_account())

    @Gtk.Template.Callback()
    def response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            ComponentManager.suspend()
            gs_set_busy_cursor(None, True)
            name = self.name_entry.get_text()
            acc = self.account_selection.get_account()
            rate = self.rate_entry.get_amount()
            lower_scale = self.lower_scale_entry.get_amount()
            upper_scale = self.upper_scale_entry.get_amount()
            if self.created_table is None:
                self.created_table = Tax(self.book)
                self.created_table.begin_edit()
                self.created_table.set_name(name or "")
            else:
                self.created_table.begin_edit()

            if self.created_entry is None:
                entry = TaxEntry()
                self.created_table.add_entry(entry)
                self.created_entry = entry
            self.created_entry.set_account(acc)
            self.created_entry.set_type(self.type)
            self.created_entry.set_rate(rate)
            self.created_entry.set_lower_scale(lower_scale)
            self.created_entry.set_upper_scale(upper_scale)
            self.created_table.changed()
            self.created_table.commit_edit()
            gs_unset_busy_cursor(None)
            ComponentManager.resume()
            dialog.destroy()
            return True
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            dialog.destroy()
            return True

        return False

    @Gtk.Template.Callback()
    def validate(self, *_):
        sensitive = True
        gs_widget_remove_style_context(self.name_entry, "error")
        gs_widget_remove_style_context(self.rate_entry, "error")
        self.name_entry.set_tooltip_text("")
        self.rate_entry.set_tooltip_text("")
        self.account_selection.set_tooltip_text("")
        name = self.name_entry.get_text()
        if self.created_table is None:
            message = ""
            if name == "" or name is None or len(name.strip()) == 0:
                message = "You must provide a name for this Tax."
                sensitive = False

            if Tax.lookup_by_name(self.book, name) is not None:
                message = "You must provide a unique name for this Tax. " \
                          "Your choice \"%s\" is already in use." % name
                sensitive = False
            if not sensitive:
                self.name_entry.set_tooltip_text(message)
                gs_widget_set_style_context(self.name_entry, "error")
                self.name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning")
            else:
                self.name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "emblem-default")

        rate = self.rate_entry.get_amount()
        if self.type == TaxAmountType.PERCENT and abs(rate) > 100:
            message = "Percentage amount must be between -100 and 100."
            self.rate_entry.set_tooltip_text(message)
            gs_widget_set_style_context(self.rate_entry, "error")
            sensitive = False

        acc = self.account_selection.get_account()
        if acc is None:
            message = "You must choose a Tax Account."
            self.account_selection.set_tooltip_text(message)
            gs_widget_set_style_context(self.account_label, "error")
            sensitive = False

        if sensitive:
            self.ok_button.set_image(Gtk.Image.new_from_icon_name("emblem-default", Gtk.IconSize.BUTTON))
            self.ok_button.set_tooltip_text("Press Okay to save customer")
        else:
            self.ok_button.set_image(Gtk.Image.new_from_icon_name("dialog-warning", Gtk.IconSize.BUTTON))
            self.ok_button.set_tooltip_text("Please fix the errors to continue")
        self.ok_button.set_sensitive(sensitive)

    @Gtk.Template.Callback()
    def type_combo_changed(self, widget):
        index = widget.get_active()
        self.type = TaxAmountType(index)


GObject.type_register(NewTaxDialog)
