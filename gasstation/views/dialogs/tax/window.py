from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import *
from .new import *


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/tax/list.ui")
class TaxDialog(Gtk.Dialog):
    __gtype_name__ = "TaxDialog"
    names_view: Gtk.TreeView = Gtk.Template.Child("taxes_view")
    entries_view: Gtk.TreeView = Gtk.Template.Child("tax_entries")

    def __init__(self, parent, book):
        super().__init__()
        self.book = book
        self.current_table = None
        self.current_entry = None
        self.set_transient_for(parent)
        gs_widget_set_style_context(self, "TaxDialog")
        view = self.names_view
        store = Gtk.ListStore(str, object)
        view.set_model(store)

        store.set_sort_column_id(TaxCol.NAME, Gtk.SortType.ASCENDING)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("", renderer, text=TaxCol.NAME)
        column.set_property("reorderable", True)
        view.append_column(column)
        column.set_sort_column_id(TaxCol.NAME)

        selection = view.get_selection()
        selection.connect("changed", self.selection_changed)
        view = self.entries_view
        store = Gtk.ListStore(str, object, str)
        view.set_model(store)
        store.set_sort_column_id(TaxEntryCol.NAME, Gtk.SortType.ASCENDING)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("", renderer, text=TaxEntryCol.NAME)
        column.set_property("reorderable", True)
        view.append_column(column)
        column.set_sort_column_id(TaxEntryCol.NAME)

        selection = view.get_selection()
        selection.connect("changed", self.entry_selection_changed)
        view.connect("row-activated", self.entry_row_activated)
        self.component_id = ComponentManager.register(DIALOG_TAX_TABLE_CM_CLASS, self.refresh_handler,
                                                      self.close_handler, book)
        self.refresh()
        gs_restore_window_size(PREFS_GROUP, self, parent)

    def entries_refresh(self):
        reference = None
        view = self.entries_view
        store = view.get_model()
        selected_entry = self.current_entry
        store.clear()
        if self.current_table is None:
            return
        lis = self.current_table.get_entries()
        for entry in lis:
            row_text = []
            acc = entry.get_account()
            amount = entry.get_rate()
            row_text.append(acc.get_full_name())
            row_text.append(entry)
            t = entry.get_type()
            if t == TaxAmountType.PERCENT:
                row_text.append("%s%%" % sprintamount(amount, PrintAmountInfo.default()))
            elif t == TaxAmountType.VALUE:
                row_text.append("%s" % sprintamount(amount, PrintAmountInfo.default()))
            else:
                row_text.append("")
            _iter = store.append(row_text)

            if entry == selected_entry:
                path = store.get_path(_iter)
                reference = Gtk.TreeRowReference.new(store, path)

        if reference is not None:
            path = reference.get_path()
            if path is not None:
                selection = view.get_selection()
                selection.select_path(path)
                view.scroll_to_cell(path, None, True, 0.5, 0.0)

    def refresh(self):
        reference = None
        saved_current_table = self.current_table
        view = self.names_view
        store = view.get_model()
        store.clear()
        ComponentManager.clear_watches(self.component_id)
        lis = Tax.get_list(self.book)

        for table in lis:
            ComponentManager.watch_entity(self.component_id, table.get_guid(), EVENT_MODIFY)
            _iter = store.append([table.get_name(), table])

            if table == saved_current_table:
                path = store.get_path(_iter)
                reference = Gtk.TreeRowReference.new(store, path)
        ComponentManager.watch_entity_type(self.component_id, ID_TAXTABLE, EVENT_CREATE | EVENT_DESTROY)

        if reference is not None:
            path = reference.get_path()
            if path is not None:
                selection = view.get_selection()
                selection.select_path(path)
                view.scroll_to_cell(path, None, True, 0.5, 0.0)

        self.entries_refresh()

    def selection_changed(self, selection):
        model, _iter = selection.get_selected()
        if _iter is None:
            return
        table = model.get_value(_iter, TaxCol.POINTER)
        if table != self.current_table:
            self.current_table = table
            self.current_entry = None
        self.entries_refresh()

    def entry_selection_changed(self, selection):
        model, _iter = selection.get_selected()
        if _iter is None:
            self.current_entry = None
            return
        self.current_entry = model.get_value(_iter, TaxEntryCol.POINTER)

    def entry_row_activated(self, tree_view, path, column):
        NewTaxDialog(self, self.book,table=self.current_table, entry=self.current_entry).run()

    @Gtk.Template.Callback()
    def new_table_cb(self, _):
        NewTaxDialog(self, self.book).run()

    @classmethod
    def rename_tax_dialog(cls, parent, title, msg, button_name, text):
        main_vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 3)
        main_vbox.set_homogeneous(False)
        main_vbox.set_border_width(6)
        main_vbox.show()

        label = Gtk.Label(msg)
        label.set_justify(Gtk.Justification.LEFT)
        main_vbox.pack_start(label, False, False, 0)
        label.show()

        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 3)
        vbox.set_homogeneous(True)
        vbox.set_border_width(6)
        main_vbox.add(vbox)
        vbox.show()

        textbox = Gtk.Entry()
        textbox.show()
        textbox.set_text(text)
        vbox.pack_start(textbox, False, False, 0)

        dialog = Gtk.Dialog(title=title, transient_for=parent, destroy_with_parent=True,
                            buttons=("_Cancel", Gtk.ResponseType.CANCEL, button_name, Gtk.ResponseType.OK))
        dialog.set_default_response(Gtk.ResponseType.OK)
        dvbox = dialog.get_content_area()
        dvbox.pack_start(main_vbox, True, True, 0)

        if dialog.run() != Gtk.ResponseType.OK:
            dialog.destroy()
            return
        text = textbox.get_text()
        dialog.destroy()
        return text

    @Gtk.Template.Callback()
    def rename_table_cb(self, button):
        if self.current_table is None:
            return
        oldname = self.current_table.get_name()
        newname = self.rename_tax_dialog(self, "Rename", "Please enter new name", "_Rename", oldname)

        if newname is not None and len(newname) and oldname != newname:
            if Tax.lookup_by_name(self.book, newname) is not None:
                message = "Tax table name \"%s\" already exists." % newname
                show_error(self, message)
            else:
                self.current_table.set_name(newname)

    @Gtk.Template.Callback()
    def delete_table_cb(self, _):
        if self.current_table is None:
            return
        if self.current_table.get_refcount() > 0:
            message = "Tax table \"%s\" is in use. You cannot delete it." % self.current_table.get_name()
            show_error(self, message)
            return
        if ask_ok_cancel(self, False, "Are you sure you want to delete \"%s\"?" % self.current_table.get_name()):
            ComponentManager.suspend()
            self.current_table.begin_edit()
            self.current_table.destroy()
            self.current_table = None
            self.current_entry = None
            ComponentManager.resume()

    @Gtk.Template.Callback()
    def new_entry_cb(self, _):
        if self.current_table is None:
            return
        dialog = NewTaxDialog(self, book=self.book, table=self.current_table)
        dialog.run()
        self.current_entry = dialog.created_entry

    @Gtk.Template.Callback()
    def edit_entry_cb(self, _):
        if self.current_entry is None:
            return
        dialog = NewTaxDialog(self, book=self.book, table=self.current_table,
                              entry=self.current_entry)
        dialog.run()

    @Gtk.Template.Callback()
    def delete_entry_cb(self, _):
        if self.current_table is None or self.current_entry is None:
            return

        if len(self.current_table.get_entries()) <= 1:
            message = "You cannot remove the last entry from the tax table. " \
                      "Try deleting the tax table if you want to do that."
            show_error(self, message)
            return

        if ask_ok_cancel(self, False, "Are you sure you want to delete this entry?"):
            ComponentManager.suspend()
            self.current_table.begin_edit()
            self.current_table.remove_entry(self.current_entry)
            self.current_table.changed()
            self.current_table.commit_edit()
            self.current_entry.destroy()
            self.current_entry = None
            ComponentManager.resume()

    def refresh_handler(self, *args):
        self.refresh()

    def close_handler(self, *args):
        gs_save_window_size(PREFS_GROUP, self)
        self.destroy()

    @Gtk.Template.Callback()
    def close_cb(self, widget):
        ComponentManager.close(self.component_id)

    @Gtk.Template.Callback()
    def destroy_cb(self, widget):
        ComponentManager.unregister(self.component_id)
        self.destroy()

    @staticmethod
    def find_handler(data, book):
        return data is not None and data[0] == book


GObject.type_register(TaxDialog)
