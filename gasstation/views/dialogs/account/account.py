# -*- coding: utf-8 -*-
import math

from gasstation.views.account import *
from gasstation.views.commodity_edit import *
from gasstation.views.dialogs.commodity.new import CommodityMode
from gasstation.views.selections.amount_edit import *
from gasstation.views.selections.date_edit import *
from gasstation.views.selections.general import GeneralSelect, GeneralSelectType
from libgasstation import Split
from libgasstation.core.account import Account
from libgasstation.core.scrub import Scrub
from .color import *

DIALOG_NEW_ACCOUNT_CM_CLASS = "dialog-new-account"
DIALOG_EDIT_ACCOUNT_CM_CLASS = "dialog-edit-account"
PREFS_GROUP = "dialogs.account"

ac_destroy_cb_list = []
last_used_account_type = AccountType.BANK


class AccountCol(GObject.GEnum):
    FULLNAME = 0
    FIELDNAME = 1
    OLD_VALUE = 2
    NEW_VALUE = 3
    NUM_ACCOUNT_COLS = 4


class AccountDialogType(GObject.GEnum):
    NEW_ACCOUNT = 0
    EDIT_ACCOUNT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/account/account.ui')
class AccountDialog(Gtk.Dialog):
    __gtype_name__ = "AccountDialog"
    security_label: Gtk.Label = Gtk.Template.Child()
    type_label: Gtk.Label = Gtk.Template.Child()
    balance_label: Gtk.Label = Gtk.Template.Child()
    parent_label: Gtk.Label = Gtk.Template.Child()
    name_entry: Gtk.Entry = Gtk.Template.Child()
    code_entry: Gtk.Entry = Gtk.Template.Child()
    description_entry: Gtk.Entry = Gtk.Template.Child()
    commodity_hbox: Gtk.Box = Gtk.Template.Child()
    account_scu: Gtk.ComboBox = Gtk.Template.Child()
    color_entry_button: Gtk.ColorButton = Gtk.Template.Child()
    color_default_button: Gtk.Button = Gtk.Template.Child()
    notes_text: Gtk.TextView = Gtk.Template.Child()
    tax_related_button: Gtk.CheckButton = Gtk.Template.Child()
    hidden_button: Gtk.CheckButton = Gtk.Template.Child()
    placeholder_button: Gtk.CheckButton = Gtk.Template.Child()
    auto_interest_button: Gtk.CheckButton = Gtk.Template.Child()
    opening_balance_button: Gtk.CheckButton = Gtk.Template.Child()
    type_view: Gtk.TreeView = Gtk.Template.Child()
    parent_scroll: Gtk.ScrolledWindow = Gtk.Template.Child()
    opening_balance_box: Gtk.Box = Gtk.Template.Child()
    opening_balance_date_box: Gtk.Box = Gtk.Template.Child()
    opening_equity_radio: Gtk.RadioButton = Gtk.Template.Child()
    transfer_account_scroll: Gtk.ScrolledWindow = Gtk.Template.Child()
    notebook: Gtk.Notebook = Gtk.Template.Child()
    ok_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, parent_window, book, account=None, base_account=None, dialog_type=AccountDialogType.NEW_ACCOUNT,
                 sub_account_names=None, modal=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if parent_window is not None:
            self.set_transient_for(parent_window)
        self.book = book
        if base_account is None:
            base_account = book.get_root_account()
        self.dialog_type = dialog_type
        self.account = account
        self.created_account = None
        self.sub_account_names = sub_account_names
        self.next_name = 1 if sub_account_names is not None else 0
        self.type = account.get_type()
        self.commodity_mode = [CommodityMode.CURRENCY]
        self.valid_types = 0
        tt = "This Account contains Transactions.\nChanging this option is not possible."
        compat_types = AccountType.valid()
        self.get_style_context().add_class("AccountDialog")
        self.color_default_button.connect("clicked", lambda b: color_default_cb(self.color_entry_button))
        self.notes_text_buffer = self.notes_text.get_buffer()
        self.commodity_edit = GeneralSelect(GeneralSelectType.SELECT, commodity_edit_get_string,
                                            commodity_edit_new_select, self.commodity_mode)
        if self.account.count_splits(False) > 0:
            sec_name = self.account.get_commodity().get_printname()
            label = Gtk.Label(sec_name)
            label.set_tooltip_text(tt)
            self.commodity_hbox.pack_start(label, False, False, 0)
        else:
            self.commodity_hbox.pack_start(self.commodity_edit, True, True, 0)
            self.commodity_edit.show()
        self.commodity_hbox.show_all()

        self.commodity_edit.make_mnemonic_target(self.security_label)
        self.commodity_edit.connect("changed", self.commodity_changed_cb)

        self.parent_tree = AccountView(show_root=True)
        self.parent_tree.expand_columns("name")
        self.parent_tree.set_grid_lines(Gtk.TreeViewGridLines.NONE)
        self.parent_scroll.add(self.parent_tree)
        self.parent_tree.show()
        selection = self.parent_tree.get_selection()
        selection.set_mode(Gtk.SelectionMode.SINGLE)
        selection.connect("changed", self.parent_changed_cb)
        self.set_auto_interest_box()

        amount = AmountEntry()
        self.opening_balance_edit = amount
        self.opening_balance_box.pack_start(amount, True, True, 0)
        amount.set_evaluate_on_enter(True)
        amount.show()

        self.balance_label.set_mnemonic_widget(amount)

        date_edit = DateSelection()
        self.opening_balance_date_edit = date_edit
        self.opening_balance_date_box.pack_start(date_edit, True, True, 0)
        date_edit.show()

        self.opening_balance_page = self.notebook.get_nth_page(1)

        self.transfer_tree = AccountView(show_root=False)
        selection = self.transfer_tree.get_selection()
        selection.set_mode(Gtk.SelectionMode.SINGLE)
        selection.set_select_function(self.commodity_filter)
        self.transfer_account_scroll.add(self.transfer_tree)
        self.transfer_tree.show()
        self.parent_label.set_mnemonic_widget(self.parent_tree)

        if self.account.count_splits(False) > 0:
            atype = self.account.get_type()
            compat_types = atype.types_compatible_with()
        if not compat_types:
            compat_types = AccountType.valid()
        self.type_view_create(compat_types)
        self.parent_tree.collapse_all()
        self.parent_tree.set_selected_account(base_account)
        gs_window_adjust_for_screen(self)
        self.set_display_title()

        self.component_id = ComponentManager.register(
            DIALOG_EDIT_ACCOUNT_CM_CLASS if dialog_type == AccountDialogType.EDIT_ACCOUNT else DIALOG_NEW_ACCOUNT_CM_CLASS,
            self.refresh_handler, None if modal else self.close_handler)
        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id, ID_ACCOUNT, EVENT_MODIFY | EVENT_DESTROY)

        gs_restore_window_size(PREFS_GROUP, self, parent_window)
        self.name_entry.grab_focus()
        self.show()
        if self.account is not None:
            self.to_ui()

    def set_auto_interest_box(self):
        type_ok = self.type.has_auto_interest_xfer()
        self.auto_interest_button.set_active(type_ok and self.account.get_auto_interest())
        self.auto_interest_button.set_sensitive(type_ok)

    @staticmethod
    def call_destroy_callbacks(acc):
        for cb in ac_destroy_cb_list:
            cb(acc)

    def commodity_from_type(self, update):
        if self.type == AccountType.TRADING:
            new_mode = CommodityMode.ALL
        elif self.type == AccountType.STOCK or self.type == AccountType.MUTUAL:
            new_mode = CommodityMode.NON_CURRENCY_SELECT
        else:
            new_mode = CommodityMode.CURRENCY
        if update and (len(self.commodity_mode) and new_mode != self.commodity_mode[0]):
            self.commodity_edit.set_selected(None)
        self.commodity_mode.clear()
        self.commodity_mode.insert(0, new_mode)

    def to_ui(self):
        account = self.account
        if account is None:
            return
        self.name_entry.set_text(account.get_name() or "")
        self.description_entry.set_text(account.get_description() or "")
        string = account.get_color()
        if string == '' or string is None:
            string = DEFAULT_COLOR
        color = Gdk.RGBA()
        Gdk.RGBA.parse(color, string)
        if color:
            self.color_entry_button.set_rgba(color)
        commodity = account.get_commodity()
        self.commodity_edit.set_selected(commodity)
        self.commodity_from_type(False)
        nonstd_scu = account.get_non_std_scu()
        if nonstd_scu:
            index = account.get_commodity_scu()
            index = math.log10(index) + 1
        else:
            index = 0
        self.account_scu.set_active(index)
        self.code_entry.set_text(account.get_code() or "")
        self.notes_text_buffer.set_text(account.get_notes() or "")
        flag = account.get_tax_related()
        self.tax_related_button.set_active(flag)

        flag = account.get_placeholder()
        self.placeholder_button.set_active(flag)

        flag = account.get_hidden()
        self.hidden_button.set_active(flag)
        self.set_auto_interest_box()

    @staticmethod
    def create_transfer_balance(book, account, transfer, balance, date):
        if balance.is_zero() or account is None or transfer is None:
            return False
        account.begin_edit()
        transfer.begin_edit()
        trans = Transaction(book)
        trans.begin_edit()
        trans.set_currency(account.get_commodity() or book.default_currency)
        trans.set_post_date(date)
        trans.set_description("Opening Balance")
        split = Split(book)
        trans.append_split(split)
        split.set_account(account)
        split.set_amount(balance)
        split.set_value(balance)
        balance = balance * -1
        split = Split(book)
        trans.append_split(split)
        split.set_account(transfer)
        split.set_amount(balance)
        split.set_value(balance)
        trans.commit_edit()
        account.commit_edit()
        transfer.commit_edit()
        return True

    def to_account(self):
        global last_used_account_type
        old_scu = 0
        account = self.account
        if account is None:
            return False
        if self.dialog_type == AccountDialogType.EDIT_ACCOUNT and self.type != account.get_type():
            self.call_destroy_callbacks(account)
        if account.get_edit_level() < 1:
            account.begin_edit()
        if self.type != account.get_type():
            account.set_type(self.type)
        last_used_account_type = self.type

        string = self.name_entry.get_text()
        account.set_name(string)
        string = self.description_entry.get_text()
        account.set_description(string)
        rgba = self.color_entry_button.get_rgba()
        string = Gdk.RGBA.to_string(rgba)
        if string == DEFAULT_COLOR:
            string = ""
        old_string = account.get_color()
        if not string and old_string:
            account.set_color("")
        else:
            if string != old_string:
                account.set_color(string)
        commodity = self.commodity_edit.get_selected()
        if commodity is not None and commodity != account.get_commodity():
            account.set_commodity(commodity)
        else:
            old_scu = account.get_commodity_scu()
        index = self.account_scu.get_active()
        nonstd = (index != 0)
        if nonstd != account.get_non_std_scu():
            account.set_non_std_scu(nonstd)
        new_scu = pow(10, index - 1) if nonstd else commodity.get_fraction()
        if old_scu != new_scu:
            account.set_commodity_scu(new_scu)
        string = self.code_entry.get_text()
        account.set_code(string)
        start = self.notes_text_buffer.get_start_iter()
        end = self.notes_text_buffer.get_end_iter()
        string = self.notes_text_buffer.get_text(start, end, False)
        account.set_notes(string)
        flag = self.tax_related_button.get_active()
        account.set_tax_related(flag)
        flag = self.placeholder_button.get_active()
        account.set_placeholder(flag)
        flag = self.hidden_button.get_active()
        account.set_hidden(flag)
        parent_account = self.parent_tree.get_selected_account()
        if parent_account is None:
            parent_account = self.book.root_account
        if parent_account != account.get_parent():
            parent_account.append_child(account)
        account.commit_edit()
        balance = self.opening_balance_edit.get_amount()
        if balance.is_zero():
            return False

        date = self.opening_balance_date_edit.get_date()
        use_equity = self.opening_equity_radio.get_active()

        if use_equity:
            try:
                Scrub.create_opening_balance(account, balance, date, self.book)
            except ValueError:
                message = "Could not create opening balance."
                print(message)
        else:
            transfer = self.transfer_tree.get_selected_account()
            if transfer is None:
                return
            self.create_transfer_balance(self.book, account, transfer, balance, date)

    def set_children_types(self, account, _type):
        for ac in account.children:
            if _type == ac.get_type():
                continue
            else:
                ac.begin_edit()
                ac.set_type(_type)
                ac.commit_edit()
                self.set_children_types(account, _type)

    def make_children_compatible(self):
        account = self.account
        if self.dialog_type == AccountDialogType.NEW_ACCOUNT or account is None:
            return
        if AccountType.types_compatible(account.get_type(), self.type):
            return
        self.set_children_types(account, self.type)

    @staticmethod
    def add_children_to_expander(expander, param_spec, data):
        if expander.get_expanded() and not expander.get_child():
            view = AccountView(show_root=True)
            scrolled_window = Gtk.ScrolledWindow(None, None)
            scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
            scrolled_window.set_shadow_type(Gtk.ShadowType.IN)
            scrolled_window.add(view)
            expander.add(scrolled_window)
            scrolled_window.set_vexpand(True)
            scrolled_window.show_all()

    def verify_children_compatible(self):
        account = self.account
        if account is None:
            return False
        if AccountType.types_compatible(account.get_type(), self.type):
            return True
        if account.get_n_children() == 0:
            return True
        dialog = Gtk.Dialog(title="", parent=self,
                            destroy_with_parent=True, modal=True,
                            buttons=("_Cancel", Gtk.ResponseType.CANCEL,
                                     "_OK", Gtk.ResponseType.OK))
        dialog.set_skip_taskbar_hint(True)
        hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 12)
        hbox.set_homogeneous(False)
        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 12)
        vbox.set_homogeneous(False)
        hbox.pack_start(Gtk.Image.new_from_icon_name("dialog-information", Gtk.IconSize.DIALOG),
                        False, False, 0)
        label = Gtk.Label("Give the children the same type?")
        label.set_line_wrap(True)
        label.set_selectable(True)
        label.get_style_context().add_class("emphasize-label")
        vbox.pack_start(label, True, True, 0)
        string = "The children of the edited account have to be " \
                 "changed to type %s to make them compatible." % self.type.to_string()
        label = Gtk.Label(string)
        label.set_line_wrap(True)
        label.set_selectable(True)
        vbox.pack_start(label, True, True, 0)

        expander = Gtk.Expander.new_with_mnemonic("_Show children accounts")
        expander.set_spacing(6)
        expander.connect("notify::expanded", self.add_children_to_expander)
        vbox.pack_start(expander, True, True, 0)
        hbox.pack_start(vbox, True, True, 0)
        dialog.get_content_area().pack_start(hbox, True, True, 0)
        dialog.get_content_area().set_spacing(14)
        hbox.show_all()
        dialog.set_default_response(Gtk.ResponseType.OK)
        result = dialog.run() == Gtk.ResponseType.OK
        dialog.destroy()
        return result

    def filter_parent_accounts(self, account):
        aw_account = self.account
        if account is None or aw_account is None:
            return False
        if account.is_root():
            return True

        if account == aw_account:
            return False
        if account.has_ancestor(aw_account):
            return False
        return True

    @Gtk.Template.Callback()
    def validate(self, *args):
        root = self.book.get_root_account()
        separator = Account.get_separator()
        name = self.name_entry.get_text()
        sensitive = True
        gs_widget_remove_style_context(self.name_entry, "error")
        self.name_entry.set_tooltip_text("")
        if name == "":
            message = "The account must be given a name."
            gs_widget_set_style_context(self.name_entry, "error")
            self.name_entry.set_tooltip_text(message)
            self.name_entry.trigger_tooltip_query()
            sensitive = False
        parent = self.parent_tree.get_selected_account()
        if parent is None:
            account = root.lookup_by_full_name(name)
        else:
            fullname_parent = parent.get_full_name()
            fullname = "%s%s%s" % (fullname_parent, separator, name)
            account = root.lookup_by_full_name(fullname)
        if account is not None and self.account != account:
            message = "There is already an account with that name."
            gs_widget_set_style_context(self.name_entry, "error")
            self.name_entry.set_tooltip_text(message)
            sensitive = False
        gs_widget_remove_style_context(self.parent_label, "error")
        self.parent_label.set_tooltip_text("")
        if not self.filter_parent_accounts(parent):
            message = "You must choose a valid parent account."
            gs_widget_set_style_context(self.parent_label, "error")
            self.parent_label.set_tooltip_text(message)
            sensitive = False

        gs_widget_remove_style_context(self.type_label, "error")
        self.type_label.set_tooltip_text("")
        if self.type == AccountType.INVALID:
            message = "You must select an account type."
            gs_widget_set_style_context(self.type_label, "error")
            self.parent_label.set_tooltip_text(message)
            sensitive = False

        if not AccountType.types_compatible(self.type, parent.get_type()):
            message = "The selected account type is incompatible with the one of the selected parent."
            gs_widget_set_style_context(self.type_label, "error")
            self.type_label.set_tooltip_text(message)
            sensitive = False

        commodity = self.commodity_edit.get_selected()
        if commodity is None:
            message = "You must choose a commodity."
            gs_widget_set_style_context(self.security_label, "error")
            self.security_label.set_tooltip_text(message)
            sensitive = False

        balance = self.opening_balance_edit.get_amount()
        if not balance.is_zero():
            use_equity = self.opening_equity_radio.get_active()
            if not use_equity:
                transfer = self.transfer_tree.get_selected_account()
                if transfer is None:
                    message = "You must select a transfer account or choose " \
                              "the opening balances equity account."
                    sensitive = False

        if sensitive:
            self.ok_button.set_image(Gtk.Image.new_from_icon_name("emblem-default", Gtk.IconSize.BUTTON))
            self.ok_button.set_tooltip_text("Press Okay to save customer")
        else:
            self.ok_button.set_image(Gtk.Image.new_from_icon_name("dialog-warning", Gtk.IconSize.BUTTON))
            self.ok_button.set_tooltip_text("Please fix the errors to continue")

        self.ok_button.set_sensitive(sensitive)

    @Gtk.Template.Callback()
    def response_cb(self, _, response):
        if response == Gtk.ResponseType.OK:
            ComponentManager.suspend()
            self.make_children_compatible()
            self.to_account()
            ComponentManager.resume()
            if self.dialog_type == AccountDialogType.NEW_ACCOUNT:
                try:
                    name = self.sub_account_names[self.next_name]
                    parent = self.account
                    ComponentManager.suspend()
                    account = Account(self.book)
                    account.begin_edit()
                    self.account = account
                    self.type = parent.get_type()
                    account.set_name(name)
                    account.set_description(name)
                    self.next_name += 1
                    self.to_ui()
                    self.set_display_title()
                    commodity = parent.get_commodity()
                    self.commodity_edit.set_selected(commodity)
                    self.commodity_from_type(False)
                    self.parent_tree.set_selected_account(parent)
                    ComponentManager.resume()
                except (IndexError, TypeError):
                    pass
            self.created_account = self.account
            self.account = None

        ComponentManager.close(self.component_id)
        self.destroy()
        return True

    def parent_changed_cb(self, selection):
        scroll_to = False
        parent_account = self.parent_tree.get_selected_account()
        if parent_account is None:
            return
        if parent_account.is_root():
            types = self.valid_types
        else:
            types = self.valid_types & parent_account.get_type().parent_types_compatible_with()
        type_model = self.type_view.get_model()
        if type_model is None:
            return
        if self.type != self.preferred_account_type and (types & (1 << self.preferred_account_type)) != 0:
            self.type = self.preferred_account_type
            scroll_to = True
        elif (types & (1 << self.type)) == 0:
            self.type = AccountType.INVALID
        else:
            old_types = AccountTypesModel.get_mask(type_model)
            if old_types != types:
                scroll_to = True
        AccountTypesModel.set_mask(type_model, types)
        if scroll_to:
            type_selection = self.type_view.get_selection()
            AccountTypesModel.set_selection(type_selection, 1 << self.type)
        self.set_display_title()

    def type_changed_cb(self, selection):
        sensitive = False
        type_id = AccountTypesModel.get_selection_single(selection)
        if type_id == AccountType.NONE:
            self.type = AccountType.INVALID
        else:
            self.type = type_id
            self.preferred_account_type = type_id
            self.commodity_from_type(True)
            sensitive = (self.type != AccountType.EQUITY and
                         self.type != AccountType.CURRENCY and
                         self.type != AccountType.STOCK and
                         self.type != AccountType.MUTUAL and
                         self.type != AccountType.TRADING)
        self.opening_balance_page.set_sensitive(sensitive)
        if not sensitive:
            self.opening_balance_edit.set_amount(Decimal(0))
        self.set_auto_interest_box()

    def type_view_create(self, compat_types):
        global last_used_account_type
        self.valid_types &= compat_types
        if self.valid_types == 0:
            try:
                self.valid_types = compat_types | (1 << self.type)
            except ValueError:
                self.type = last_used_account_type
                for i in range(32):
                    if self.valid_types & (1 << i) != 0:
                        self.type = AccountType(i)
                        break
            self.preferred_account_type = self.type
        elif (self.valid_types & (1 << self.type)) != 0:
            self.preferred_account_type = self.type
        elif (self.valid_types & (1 << last_used_account_type)) != 0:
            self.type = last_used_account_type
            self.preferred_account_type = last_used_account_type
        else:
            self.preferred_account_type = self.type
            self.type = AccountType.INVALID
            for i in range(32):
                if self.valid_types & (1 << i) != 0:
                    self.type = AccountType(i)
                    break
        model = AccountTypesModel.filter_using_mask(compat_types)
        view = self.type_view
        view.set_model(model)
        renderer = Gtk.CellRendererText.new()
        view.insert_column_with_attributes(-1, "", renderer, text=AccountTypesModelColumn.NAME)
        view.set_search_column(AccountTypesModelColumn.NAME)
        selection = view.get_selection()
        selection.connect("changed", self.type_changed_cb)
        AccountTypesModel.set_selection(selection, 1 << self.type)

    def name_changed_cb(self, _):
        self.set_display_title()

    def commodity_changed_cb(self, gsl, *_):
        currency = gsl.get_selected()
        if currency is None:
            return
        self.opening_balance_edit.set_print_info(PrintAmountInfo.commodity(currency, False))
        selection = self.transfer_tree.get_selection()
        selection.unselect_all()

    def commodity_filter(self, selection, unused_model, s_path, path_currently_selected):
        if path_currently_selected:
            return True
        account = self.transfer_tree.get_account_from_path(s_path)
        if account is None:
            return False
        commodity = self.commodity_edit.get_selected()
        return account.get_commodity() == commodity

    @Gtk.Template.Callback()
    def opening_equity_cb(self, w):
        use_equity = w.get_active()
        self.transfer_account_scroll.set_sensitive(not use_equity)

    def set_display_title(self):
        def get_fullname():
            name = self.name_entry.get_text()
            if name is None or name == "":
                name = "<No name>"
            parent_account = self.parent_tree.get_selected_account()
            if parent_account is not None and not parent_account.is_root():
                parent_name = parent_account.get_full_name()
                separator = Account.get_separator()
                return parent_name + separator + name
            else:
                return name

        fullname = get_fullname()
        if self.dialog_type == AccountDialogType.EDIT_ACCOUNT:
            title = "Edit Account - " + fullname
        else:
            try:
                next_name = self.sub_account_names[self.next_name]
                form = "(%s) New Accounts"
                prefix = form % next_name
                title = prefix + " - " + fullname + " ..."
            except (IndexError, TypeError):
                title = "New Account - " + fullname
        self.set_title(title)

    def close_handler(self, *args):
        gs_save_window_size(PREFS_GROUP, self)
        self.destroy()

    def refresh_handler(self, changes):
        account = self.account
        if account is None:
            ComponentManager.close(self.component_id)
            return

        if changes is not None:
            info = ComponentManager.get_entity_events(changes, account.guid)
            if info is not None and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return
        self.set_display_title()

    @classmethod
    def __internal(cls, parent, book, base_account, sub_account_names=None, valid_types=None,
                   default_commodity=None, modal=False):
        global last_used_account_type
        if book is None:
            return
        if valid_types is None:
            valid_types = []
        valid_types_in = 0
        for t in valid_types:
            valid_types_in |= (1 << t)
        account = Account(book)
        account.begin_edit()
        if base_account is not None:
            _type = base_account.get_type()
            parent_commodity = base_account.get_commodity()
        else:
            if any(valid_types):
                accs = list(
                    filter(lambda a: a.get_type() in valid_types, book.get_root_account().get_descendants_sorted()))
                if any(accs):
                    base_account = accs[0]
                    _type = base_account.get_type()
                    parent_commodity = base_account.get_commodity()
                else:
                    _type = last_used_account_type
                    parent_commodity = gs_default_currency()
            else:
                _type = last_used_account_type
                parent_commodity = gs_default_currency()
        ComponentManager.suspend()
        if sub_account_names is not None and any(sub_account_names):
            name = sub_account_names.pop(0)
            account.set_name(name)
            account.set_description(name)
        account.set_type(_type)
        self = cls(parent, book, account, base_account=base_account, sub_account_names=sub_account_names, modal=modal)
        ComponentManager.resume()
        if default_commodity is not None:
            commodity = default_commodity
            if (self.type == AccountType.STOCK) or (self.type == AccountType.MUTUAL):
                self.name_entry.set_text(commodity.get_mnemonic())
                self.description_entry.set_text(commodity.get_fullname())

        elif (self.type != AccountType.STOCK) and (self.type != AccountType.MUTUAL):
            commodity = parent_commodity
        else:
            commodity = None

        self.commodity_edit.set_selected(commodity)
        self.commodity_from_type(False)
        return self

    @classmethod
    def new_from_name_window(cls, parent, name):
        return cls.new_from_name_with_defaults(parent, name, None, None, None)

    @classmethod
    def new_from_name_window_with_types(cls, parent, name, valid_types):
        logging.info("Account window new from types")
        return cls.new_from_name_with_defaults(parent, name, valid_types, None, None)

    @classmethod
    def new_from_name_with_defaults(cls, parent, name, valid_types, default_commodity, parent_acct):
        def split_account_name(book, in_name):
            account = book.get_root_account()
            names = in_name.split(Account.get_separator())
            base_account = None
            for i, name in enumerate(names.copy()):
                account = account.lookup_by_name(name)
                if account is not None:
                    base_account = account
                    names.pop(0)
                else:
                    break
            return names, base_account

        logging.info("Account Window new with defaults %s" % name)
        book = Session.get_current_book()
        if name is None or name == "":
            sub_account_names = None
            base_account = None
        else:
            sub_account_names, base_account = split_account_name(book, name)
        if parent_acct is not None:
            base_account = parent_acct
        self = cls.__internal(parent, book, base_account,
                              sub_account_names,
                              valid_types, default_commodity,
                              True)
        return self

    @classmethod
    def new_edit(cls, parent, account):
        if account is None:
            return
        book = Session.get_current_book()
        ComponentManager.suspend()
        self = cls(parent, book, account, account.get_parent(), AccountDialogType.EDIT_ACCOUNT)
        ComponentManager.resume()
        self.opening_balance_page.hide()
        return self

    @classmethod
    def new_(cls, parent, book, parent_acct):
        return cls.__internal(parent, book, parent_acct)

    @classmethod
    def new_with_types(cls, parent, book, valid_types):
        return cls.__internal(parent, book, None, valid_types=valid_types)

    def destroy_cb(self, _):
        account = self.account
        ComponentManager.suspend()
        if self.dialog_type == AccountDialogType.NEW_ACCOUNT:
            if account is not None:
                account.destroy()
                self.account = None
        elif self.dialog_type == AccountDialogType.EDIT_ACCOUNT:
            pass
        else:
            ComponentManager.resume()
            return
        self.grab_remove()
        ComponentManager.unregister(self.component_id)
        ComponentManager.resume()

    def __repr__(self):
        return '<AccountDialog>'


GObject.type_register(AccountDialog)
