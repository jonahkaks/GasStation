from gi.repository import Gdk

DEFAULT_COLOR = "rgb(237,236,235)"


def color_default_cb(cbutton):
    color = Gdk.RGBA()
    Gdk.RGBA.parse(color, DEFAULT_COLOR)
    cbutton.set_rgba(color)
