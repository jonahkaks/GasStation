from gasstation.models.account_types import *

FILTER_TREE_VIEW = "types_tree_view"


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/account/filter.ui")
class FilterAccountDialog(Gtk.Dialog):
    __gtype_name__ = "FilterAccountDialog"
    show_hidden_check: Gtk.CheckButton = Gtk.Template.Child()
    show_zero_check: Gtk.CheckButton = Gtk.Template.Child()
    show_unused_check: Gtk.CheckButton = Gtk.Template.Child()
    view: Gtk.TreeView = Gtk.Template.Child(FILTER_TREE_VIEW)

    def __init__(self, page, **kwargs):
        super().__init__(**kwargs)
        self.filter_override = {}
        self.visible_types = 0
        self.original_visible_types = 0
        self.show_hidden = False
        self.original_show_hidden = False
        self.show_zero_total = False
        self.original_show_zero_total = False
        self.show_unused = False
        self.original_show_unused = False
        self.set_transient_for(page.window)
        title = "Filter %s by..." % page.get_name()
        self.set_title(title)
        self.original_visible_types = self.visible_types
        self.original_show_hidden = self.show_hidden
        self.original_show_zero_total = self.show_zero_total
        self.original_show_unused = self.show_unused

        self.model = AccountTypesModel.filter_using_mask(~(1 << AccountType.ROOT))
        self.view.set_model(self.model)
        renderer = Gtk.CellRendererToggle()
        renderer.connect("toggled", self.visible_toggled_cb)
        self.view.insert_column_with_data_func(-1, "", renderer, self.visible_set_func)
        self.view.insert_column_with_attributes(-1, "Account Types", Gtk.CellRendererText(),
                                                text=AccountTypesModelColumn.NAME)

    def filter_accounts(self, account, *_):
        if account is None:
            return True
        if len(self.filter_override) > 0:
            test_acc = self.filter_override.get(account)
            if test_acc is not None:
                return True
        if not self.show_hidden and account.get_hidden():
            return False
        if not self.show_zero_total:
            total = account.get_balance_in_currency(None, True)
            if total.is_zero():
                return False
        if not self.show_unused:
            if account.count_splits(True) == 0:
                return False
        try:
            return bool(self.visible_types & (1 << account.get_type()))
        except ValueError:
            return True

    @Gtk.Template.Callback()
    def show_hidden_toggled_cb(self, button):
        if not isinstance(button, Gtk.ToggleButton): return
        self.show_hidden = button.get_active()
        self.tree_view.refilter()

    @Gtk.Template.Callback()
    def show_zero_toggled_cb(self, button):
        if not isinstance(button, Gtk.ToggleButton): return
        self.show_zero_total = button.get_active()
        self.tree_view.refilter()

    @Gtk.Template.Callback()
    def show_unused_toggled_cb(self, button):
        if not isinstance(button, Gtk.ToggleButton): return
        self.show_unused = button.get_active()
        self.tree_view.refilter()

    @Gtk.Template.Callback()
    def clear_all_cb(self, button):
        if not isinstance(button, Gtk.Button): return
        self.visible_types = 0
        self.model.refilter()
        self.tree_view.refilter()

    @Gtk.Template.Callback()
    def select_all_cb(self, button):
        if not isinstance(button, Gtk.Button): return
        self.visible_types = -1
        self.model.refilter()
        self.tree_view.refilter()

    @Gtk.Template.Callback()
    def select_default_cb(self, button):
        self.select_all_cb(button)

    def visible_set_func(self, column, renderer, model, _iter):
        _type = model.get_value(_iter, AccountTypesModelColumn.TYPE)
        active = True if self.visible_types & (1 << _type) else False
        renderer.set_property("active", active)

    def visible_toggled_cb(self, renderer, path_str):
        model = self.model
        path = Gtk.TreePath.new_from_string(path_str)
        _iter = model.get_iter(path)
        if _iter is not None:
            _type = model.get_value(_iter, AccountTypesModelColumn.TYPE)
            self.visible_types ^= (1 << _type)
            self.tree_view.refilter()

    @Gtk.Template.Callback()
    def response_cb(self, dialog, response):
        if not isinstance(dialog, Gtk.Dialog): return
        if response != Gtk.ResponseType.OK:
            self.visible_types = self.original_visible_types
            self.show_hidden = self.original_show_hidden
            self.show_zero_total = self.original_show_zero_total
            self.show_unused = self.original_show_unused
            self.tree_view.refilter()
        dialog.hide()
        return True

GObject.type_register(FilterAccountDialog)
