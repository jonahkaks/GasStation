from gi.repository import GObject

from gasstation.utilities.dialog import *
from libgasstation.core.component import ComponentManager
from libgasstation.core.session import Session

DIALOG_FIND_ACCOUNT_CM_CLASS = "dialog-find-account"
PREFS_GROUP = "dialogs.find-account"


class FindAccountColumn(GObject.GEnum):
    ACC_FULL_NAME = 0
    ACCOUNT = 1
    PLACE_HOLDER = 2
    HIDDEN = 3
    NOT_USED = 4
    BAL_ZERO = 5
    TAX = 6


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/account/find.ui')
class FindAccountDialog(Gtk.Window):
    __gtype_name__ = "FindAccountDialog"
    radio_root: Gtk.RadioButton = Gtk.Template.Child("radio-root")
    radio_sub_root: Gtk.RadioButton = Gtk.Template.Child("radio-subroot")
    filter_text_entry: Gtk.Entry = Gtk.Template.Child("filter-text-entry")
    radio_frame: Gtk.Frame = Gtk.Template.Child()
    view: Gtk.TreeView = Gtk.Template.Child("treeview")

    def __init__(self, parent, account):
        super().__init__()
        self.account = account
        self.jump_close = True
        self.parent = parent
        gs_widget_set_style_context(self, "FindAccountDialog")
        self.session = Session.get_current_session()
        self.set_title("Find Account")

        store = Gtk.ListStore(str, object, str, str, str, str, str)
        self.view.set_model(store)
        self.view.connect("row-activated", self.row_double_clicked)
        self.view.set_grid_lines(gs_tree_view_get_grid_lines_pref())
        selection = self.view.get_selection()
        selection.set_mode(Gtk.SelectionMode.SINGLE)
        tree_column = Gtk.TreeViewColumn.new()
        tree_column.set_title("Place Holder")
        self.view.append_column(tree_column)
        tree_column.set_alignment(0.5)
        tree_column.set_expand(True)
        cr = Gtk.CellRendererPixbuf.new()
        tree_column.pack_start(cr, True)
        tree_column.add_attribute(cr, "icon-name", FindAccountColumn.PLACE_HOLDER)
        cr.set_alignment(0.5, 0.5)
        tree_column = Gtk.TreeViewColumn.new()
        tree_column.set_title("Hidden")
        self.view.append_column(tree_column)
        tree_column.set_alignment(0.5)
        tree_column.set_expand(True)
        cr = Gtk.CellRendererPixbuf.new()
        tree_column.pack_start(cr, True)
        tree_column.add_attribute(cr, "icon-name", FindAccountColumn.HIDDEN)
        cr.set_alignment(0.5, 0.5)

        tree_column = Gtk.TreeViewColumn.new()
        tree_column.set_title("Not Used")
        self.view.append_column(tree_column)
        tree_column.set_alignment(0.5)
        tree_column.set_expand(True)
        cr = Gtk.CellRendererPixbuf.new()
        tree_column.pack_start(cr, True)

        tree_column.add_attribute(cr, "icon-name", FindAccountColumn.NOT_USED)
        cr.set_alignment(0.5, 0.5)

        tree_column = Gtk.TreeViewColumn.new()
        tree_column.set_title("Balance Zero")
        self.view.append_column(tree_column)
        tree_column.set_alignment(0.5)
        tree_column.set_expand(True)
        cr = Gtk.CellRendererPixbuf.new()
        tree_column.pack_start(cr, True)
        tree_column.add_attribute(cr, "icon-name", FindAccountColumn.BAL_ZERO)
        cr.set_alignment(0.5, 0.5)

        tree_column = Gtk.TreeViewColumn.new()
        tree_column.set_title("Tax related")
        self.view.append_column(tree_column)
        tree_column.set_alignment(0.5)
        tree_column.set_expand(True)
        cr = Gtk.CellRendererPixbuf.new()
        tree_column.pack_start(cr, True)
        tree_column.add_attribute(cr, "icon-name", FindAccountColumn.TAX)
        cr.set_alignment(0.5, 0.5)

        gs_restore_window_size(PREFS_GROUP, self, parent)
        self.show_all()
        if self.account is not None:
            sub_label_start = "Search from "
            sub_full_name = self.account.get_full_name()
            sub_label = sub_label_start + sub_full_name
            self.radio_sub_root.set_label(sub_label)
            self.radio_sub_root.set_active(True)
        else:
            self.radio_frame.hide()
        self.filter_text_entry.set_text("")
        self.get_account_info()
        self.component_id = ComponentManager.register(DIALOG_FIND_ACCOUNT_CM_CLASS, self.refresh_handler,
                                                      self.close_handler,
                                                      self)
        ComponentManager.set_session(self.component_id, self.session)

    @Gtk.Template.Callback()
    def destroy_cb(self, _):
        ComponentManager.close(self.component_id)
        return True

    @Gtk.Template.Callback()
    def delete_event_cb(self, *args):
        gs_save_window_size(PREFS_GROUP, self.parent)
        return False

    @Gtk.Template.Callback()
    def key_press_cb(self, widget, event):
        if event.keyval == Gdk.KEY_Escape:
            self.close_handler()
            return True
        else:
            return False

    def jump_to_account(self, jump_account):
        from gasstation.plugins.account import AccountTreePluginPage
        if jump_account is not None:
            AccountTreePluginPage.open(jump_account, self.parent)
        if self.jump_close:
            ComponentManager.close_by_data(DIALOG_FIND_ACCOUNT_CM_CLASS, self)

    def jump_set(self):
        if self.jump_close:
            self.jump_close = False
        else:
            self.jump_close = True

    def jump_to(self):
        selection = self.view.get_selection()
        model, _iter = selection.get_selected()
        if _iter is not None:
            jump_account = model.get_value(_iter, FindAccountColumn.ACCOUNT)
            self.jump_to_account(jump_account)

    def row_double_clicked(self, treeview, path, col):
        model = treeview.get_model()
        _iter = model.get_iter(path)
        if _iter is not None:
            jump_account = model.get_value(_iter, FindAccountColumn.ACCOUNT)
            self.jump_to_account(jump_account)

    @Gtk.Template.Callback()
    def jump_button_cb(self, _):
        self.jump_to()

    @Gtk.Template.Callback()
    def check_button_cb(self, _):
        self.jump_set()

    @Gtk.Template.Callback()
    def close_button_cb(self, _):
        ComponentManager.close_by_data(DIALOG_FIND_ACCOUNT_CM_CLASS, self)

    @staticmethod
    def fill_model(model, account):
        fullname = account.get_full_name()
        splits = account.count_splits(True)
        total = account.get_balance()
        _iter = model.append()
        model.set_value(_iter, FindAccountColumn.ACC_FULL_NAME, fullname)
        model.set_value(_iter, FindAccountColumn.ACCOUNT, account)
        model.set_value(_iter, FindAccountColumn.PLACE_HOLDER, "emblem-default" if account.get_placeholder() else "")
        model.set_value(_iter, FindAccountColumn.HIDDEN, "emblem-default" if account.get_hidden() else "")
        model.set_value(_iter, FindAccountColumn.NOT_USED, "emblem-default" if splits == 0 else "")
        model.set_value(_iter, FindAccountColumn.BAL_ZERO, "emblem-default" if total else "")
        model.set_value(_iter, FindAccountColumn.TAX, "emblem-default" if account.get_tax_related() else "")

    def get_account_info(self):
        radio_root = self.radio_root.get_active()
        if self.account is None or radio_root == True:
            root = Session.get_current_root()
        else:
            root = self.account
        accts = root.get_descendants()
        filter_text = self.filter_text_entry.get_text().lower()
        model = self.view.get_model()
        self.view.set_model(None)
        model.clear()
        for acc in accts:
            full_name = acc.get_full_name()
            match_string = full_name.lower()
            if filter_text == "" or re.search(filter_text, match_string):
                self.fill_model(model, acc)
        self.view.set_model(model)
        self.view.columns_autosize()

    @Gtk.Template.Callback()
    def filter_button_cb(self, _):
        self.get_account_info()
        self.filter_text_entry.set_text("")

    def close_handler(self, *args):
        gs_save_window_size(PREFS_GROUP, self.parent)
        self.destroy()

    def refresh_handler(self, changes, *a):
        pass

    @staticmethod
    def show_handler(klass, component_id, self):
        self.present()
        return True

    def __repr__(self):
        return '<FindAccountDialog>'


GObject.type_register(FindAccountDialog)
