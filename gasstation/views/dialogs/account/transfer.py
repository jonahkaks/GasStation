from gi.repository import GObject

from gasstation.utilities.cursors import *
from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import *
from gasstation.utilities.ui import *
from gasstation.views.account import AccountView
from gasstation.views.dialogs.location.location import LocationDialog
from gasstation.views.selections import ReferenceSelection
from gasstation.views.selections.amount_edit import AmountEntry
from gasstation.views.selections.date_edit import DateSelection
from libgasstation import Location, Split, Transaction
from libgasstation.core.component import *
from libgasstation.core.euro import *
from libgasstation.core.helpers import gs_set_num_action
from libgasstation.core.price_db import *
from libgasstation.core.price_quotes import PriceQuotes

DIALOG_TRANSFER_CM_CLASS = "dialog-transfer"
PREFS_GROUP = "dialogs.transfer"


class TransferDirection(GObject.GEnum):
    FROM = 0
    TO = 1


class AccountTreeFilterInfo:
    def __init__(self):
        self.show_inc_exp = False
        self.show_hidden = False


class PriceDate(GObject.GEnum):
    SAME_DAY = 0
    NEAREST = 1
    LATEST = 2


class PriceReq:
    def __init__(self):
        self.price = None
        self.pricedb = None
        self.fro = None
        self.to = None
        self.time = 0
        self.reverse = False


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/transfer.ui')
class TransferDialog(Gtk.Dialog):
    __gtype_name__ = "TransferDialog"
    num_entry: Gtk.Entry = Gtk.Template.Child()
    description_entry: Gtk.Entry = Gtk.Template.Child()
    memo_entry: Gtk.Entry = Gtk.Template.Child()
    left_trans_window: Gtk.ScrolledWindow = Gtk.Template.Child()
    right_trans_window: Gtk.ScrolledWindow = Gtk.Template.Child()
    left_show_button: Gtk.CheckButton = Gtk.Template.Child()
    right_show_button: Gtk.CheckButton = Gtk.Template.Child()
    transfer_info_label: Gtk.Label = Gtk.Template.Child()
    fetch_button: Gtk.Button = Gtk.Template.Child()
    price_radio: Gtk.RadioButton = Gtk.Template.Child()
    amount_radio: Gtk.RadioButton = Gtk.Template.Child()
    conv_forward: Gtk.Label = Gtk.Template.Child()
    conv_reverse: Gtk.Label = Gtk.Template.Child()
    right_trans_label: Gtk.Label = Gtk.Template.Child()
    left_trans_label: Gtk.Label = Gtk.Template.Child()
    right_currency_label: Gtk.Label = Gtk.Template.Child()
    left_currency_label: Gtk.Label = Gtk.Template.Child()
    currency_transfer_grid: Gtk.Grid = Gtk.Template.Child()
    details_grid: Gtk.Grid = Gtk.Template.Child()

    def __init__(self, parent, initial):
        super().__init__()
        if parent is not None:
            self.set_transient_for(parent.get_toplevel())
        if initial is not None:
            book = initial.get_book()
        else:
            book = Session.get_current_book()
        self.from_window = None
        self.from_tree_view = None
        self.from_commodity = None
        self.to_window = None
        self.to_tree_view = None
        self.to_commodity = None
        self.qf = None
        self.desc_start_selection = 0
        self.desc_end_selection = 0
        self.desc_selection_source_id = 0
        self.exch_rate = None
        self.price_source = PriceSource.USER_PRICE
        self.price_type = PriceType.TRN
        self.transaction_cb = None
        self.transaction_user_data = None
        use_accounting_labels = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_ACCOUNTING_LABELS)
        self.quick_fill = TransferDirection.FROM
        self.set_fetch_sensitive(self.fetch_button)

        self.amount_entry = AmountEntry()
        self.details_grid.attach(self.amount_entry, 1, 0, 1, 1)
        self.amount_entry.set_evaluate_on_enter(True)
        self.amount_entry.set_activates_default(True)
        self.amount_entry.connect("focus-out-event", self.amount_update_cb)
        self.amount_entry.show()

        self.date_selection = DateSelection()
        self.details_grid.attach(self.date_selection, 1, 1, 1, 1)
        self.date_selection.connect("date_changed", self.date_changed_cb)
        self.date_selection.show()

        self.location_selection = ReferenceSelection(Location, LocationDialog)
        self.location_selection.set_new_ability(True)
        self.details_grid.attach(self.location_selection, 1, 2, 1, 1)
        self.location_selection.show()

        self.price_entry = AmountEntry()
        self.price_entry.set_print_info(PrintAmountInfo.default(False))
        self.currency_transfer_grid.attach(self.price_entry, 1, 1, 1, 1)
        self.price_entry.connect("focus-out-event", self.price_update_cb)
        self.price_entry.set_activates_default(True)
        self.price_entry.show()

        self.to_amount_entry = AmountEntry()
        self.currency_transfer_grid.attach(self.to_amount_entry, 1, 2, 1, 1)
        self.to_amount_entry.connect("focus-out-event", self.to_amount_update_cb)
        self.to_amount_entry.set_activates_default(True)
        self.to_amount_entry.show()

        self.to_info = AccountTreeFilterInfo()
        self.from_info = AccountTreeFilterInfo()

        self.fill_tree_view(TransferDirection.TO)
        self.fill_tree_view(TransferDirection.FROM)

        if use_accounting_labels:
            self.from_transfer_label = self.right_trans_label
            self.to_transfer_label = self.left_trans_label
            text = "<b>Credit Account</b>"
            self.from_transfer_label.set_markup(text)
            text = "<b>Debit Account</b>"
            self.to_transfer_label.set_markup(text)
            self.from_currency_label = self.right_currency_label
            label = self.left_currency_label
            self.to_currency_label = label
        else:
            self.from_transfer_label = self.left_trans_label
            self.to_transfer_label = self.right_trans_label
            text = "<b>Transfer From</b>"
            self.from_transfer_label.set_markup(text)
            text = "<b>Transfer To</b>"
            self.to_transfer_label.set_markup(text)
            self.from_currency_label = self.left_currency_label
            self.to_currency_label = self.right_currency_label
        if use_accounting_labels:
            self.amount_radio.get_child().set_text("Debit Amount:")
        else:
            self.amount_radio.get_child().set_text("To Amount:")
        gs_widget_set_style_context(self, "TransferDialog")
        gs_restore_window_size(PREFS_GROUP, self, parent.get_toplevel())
        self.book = book
        self.pricedb = PriceDB.get_db(book)
        self.component_id = ComponentManager.register(DIALOG_TRANSFER_CM_CLASS, None, self.close_handler)
        self.amount_entry.grab_focus()
        self.select_from_account(initial)
        self.select_to_account(initial)
        self.curr_acct_activate()
        gs_window_adjust_for_screen(self)
        self.show()

    def compute_price_value(self):
        from_amt = self.amount_entry.get_amount()
        to_amt = self.to_amount_entry.get_amount()
        return to_amt / from_amt

    def price_request(self):
        pr = PriceReq()
        pr.price = None
        pr.pricedb = self.pricedb
        pr.fro = self.from_commodity
        pr.to = self.to_commodity
        pr.time = self.date_selection.get_date()
        pr.reverse = False
        return pr

    @staticmethod
    def lookup_price(pr, pd):
        if pr is None or pr.pricedb is None or pr.fro is None or pr.to is None:
            return False
        pr.reverse = False
        if pd == PriceDate.NEAREST:
            prc = pr.pricedb.lookup_nearest_in_time64(pr.fro, pr.to, pr.time)
        elif pd == PriceDate.LATEST:
            prc = pr.pricedb.lookup_latest(pr.fro, pr.to)
        else:
            prc = pr.pricedb.lookup_day_t64(pr.fro, pr.to, pr.time)
        if prc is None:
            pr.price = None
            return False
        if Commodity.equiv(prc.get_currency(), pr.fro):
            pr.reverse = True
        pr.price = prc
        return True

    def update_price(self):
        if not isinstance(self.from_commodity, Commodity) or not isinstance(self.to_commodity, Commodity):
            return

        if Commodity.equal(self.from_commodity, self.to_commodity):
            return
        if self.pricedb is None:
            return
        pr = self.price_request()
        if not self.lookup_price(pr, PriceDate.SAME_DAY):
            if not self.lookup_price(pr, PriceDate.NEAREST):
                return

        price_value = pr.price.get_value()
        if pr.reverse:
            price_value *= -1
        pr.price.unref()
        self.set_price_edit(price_value)
        self.update_to_amount()

    @staticmethod
    def toggle_cb(button, treeview):
        info = treeview.get_user_data(treeview, "filter-info")
        if info:
            info.show_inc_exp = button.get_active()
            info.show_hidden = False
        treeview.refilter()

    def key_press_cb(self, widget, event):
        if event.keyval == Gdk.KEY_Return or event.keyval == Gdk.KEY_KP_Enter:
            toplevel = widget.get_toplevel()
            if toplevel is not None and toplevel.is_toplevel() and isinstance(toplevel, Gtk.Window):
                toplevel.activate_default()
                return True
        return False

    def set_price_auto(self, currency_active, from_currency, to_currency):
        if not currency_active:
            self.set_price_edit(Decimal(0))
            self.price_entry.set_text("")
            self.update_to_amount()
            return

        if not gs_is_euro_currency(from_currency) or not gs_is_euro_currency(to_currency):
            self.update_price()
            return
        from_rate = gs_euro_currency_get_rate(from_currency)
        to_rate = gs_euro_currency_get_rate(to_currency)
        if from_rate.is_zero() or to_rate.is_zero():
            self.update_price()
        price_value = to_rate / from_rate
        self.price_entry.set_amount(price_value)
        self.update_to_amount()

    def curr_acct_activate(self):
        from_account = self.get_selected_account(TransferDirection.FROM)
        to_account = self.get_selected_account(TransferDirection.TO)
        curr_active = self.exch_rate or \
                      (from_account is not None and to_account is not None) \
                      and not Commodity.equiv(self.from_commodity, self.to_commodity)
        self.currency_transfer_grid.set_sensitive(curr_active)
        self.price_entry.set_sensitive(curr_active and self.price_radio.get_active())
        self.to_amount_entry.set_sensitive(curr_active and self.amount_radio.get_active())
        self.price_radio.set_sensitive(curr_active)
        self.amount_radio.set_sensitive(curr_active)
        self.set_price_auto(curr_active, self.from_commodity, self.to_commodity)
        self.update_conv_info()
        if not curr_active:
            self.to_amount_entry.set_amount(Decimal(0))
            self.to_amount_entry.set_text("")

    @Gtk.Template.Callback()
    def price_amount_radio_toggled_cb(self, _):
        self.price_entry.set_sensitive(self.price_radio.get_active())
        self.to_amount_entry.set_sensitive(self.amount_radio.get_active())

    def reload_quickfill(self):
        account = self.get_selected_account(self.quick_fill)
        # gs_quickfill_destroy(self.qf)
        # self.qf = gs_quickfill_new()
        splitlist = account.get_splits()
        # for (node = splitlist node node = node.next) {
        # split = node.data
        # trans = xaccSplitGetParent(split)
        # gs_quickfill_insert(self.qf,
        #                                xaccTransGetDescription(trans), QUICKFILL_LIFO)
        # }
        # }

    def from_tree_selection_changed_cb(self, _):
        account = self.get_selected_account(TransferDirection.FROM)
        if account is None:
            return
        commodity = gs_account_or_default_currency(account)[0]
        self.from_currency_label.set_text(commodity.get_printname())
        self.from_commodity = commodity
        print_info = PrintAmountInfo.account(account, False)
        self.amount_entry.set_print_info(print_info)
        self.curr_acct_activate()
        if self.quick_fill == TransferDirection.FROM:
            self.reload_quickfill()

    def to_tree_selection_changed_cb(self, selection):
        account = self.get_selected_account(TransferDirection.TO)
        if account is None:
            return
        commodity = account.get_commodity()
        self.to_currency_label.set_text(commodity.get_printname())
        self.to_commodity = commodity

        print_info = PrintAmountInfo.account(account, False)
        self.to_amount_entry.set_print_info(print_info)
        self.curr_acct_activate()
        if self.quick_fill == TransferDirection.TO:
            self.reload_quickfill()

    @staticmethod
    def inc_exp_filter_func(account, info):
        if account is None or info is None: return
        if info.show_hidden and account.get_hidden():
            return False
        if info.show_inc_exp:
            return True
        _type = account.get_type()
        return _type != AccountType.INCOME and _type != AccountType.EXPENSE

    def fill_tree_view(self, direction):
        show_inc_exp_message = "Show the income and expense accounts"
        use_accounting_labels = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_ACCOUNTING_LABELS)
        if use_accounting_labels:
            button = self.left_show_button if direction == TransferDirection.TO else self.right_show_button
            scroll_win = self.left_trans_window if direction == TransferDirection.TO else self.right_trans_window
        else:
            button = self.right_show_button if direction == TransferDirection.TO else self.left_show_button
            scroll_win = self.right_trans_window if direction == TransferDirection.TO else self.left_trans_window
        if direction == TransferDirection.TO:
            info = self.to_info
        else:
            info = self.from_info

        tree_view = AccountView()
        scroll_win.add(tree_view)
        info.show_inc_exp = button.get_active()
        info.show_hidden = False
        tree_view.set_filter(self.inc_exp_filter_func, info, None)
        tree_view.set_user_data(tree_view, "filter-info", info)
        tree_view.show()
        tree_view.connect("key-press-event", self.key_press_cb)
        selection = tree_view.get_selection()
        selection.set_mode(Gtk.SelectionMode.BROWSE)
        button.set_active(False)
        button.set_tooltip_text(show_inc_exp_message)

        if direction == TransferDirection.TO:
            self.to_tree_view = tree_view
            self.to_window = scroll_win
            self.to_show_button = button
            selection.connect("changed", self.to_tree_selection_changed_cb)
        else:
            self.from_tree_view = tree_view
            self.from_window = scroll_win
            self.from_show_button = button
            selection.connect("changed", self.from_tree_selection_changed_cb)
        button.connect("toggled", self.toggle_cb, tree_view)

    def parse_error_dialog(self, error_string):
        show_error(self, error_string)

    def quickfill(self):
        changed = False
        match_account = self.get_selected_account(self.quick_fill)
        desc = self.description_entry.get_text()
        if desc == "": return False
        split = match_account.find_split_by_desc(desc)
        if split is None:
            return False
        if self.amount_entry.get_amount().is_zero():
            amt = split.get_value()
            if amt < 0:
                amt *= -1
            self.amount_entry.set_amount(amt)
            changed = True

        if self.memo_entry.get_text() != "":
            self.memo_entry.set_text(split.get_memo())
            changed = True
        other = split.get_other_split()
        other_acct = Split.get_account(other)
        if other is not None and other_acct is not None:
            if self.quick_fill == TransferDirection.FROM:
                other_button = self.to_show_button
                other_direction = TransferDirection.TO
            else:
                other_button = self.from_show_button
                other_direction = TransferDirection.FROM
            other_type = other_acct.get_type()
            if other_type == AccountType.EXPENSE or other_type == AccountType.INCOME:
                other_button.set_active(True)
            self.set_selected_account(other_acct, other_direction)
            changed = True
        return changed

    def idle_select_region(self):
        self.description_entry.select_region(self.desc_start_selection, self.desc_end_selection)
        self.desc_selection_source_id = 0
        return False

    def description_insert_cb(self, editable, insert_text, insert_text_len, start_pos):
        if insert_text_len <= 0:
            return

    def description_key_press_cb(self, entry, event):
        done_with_input = False
        keyval = event.keyval
        if keyval == Gdk.KEY_Return or keyval == Gdk.KEY_KP_Enter:
            self.quickfill()
            return done_with_input
        elif keyval == Gdk.KEY_Tab or keyval == Gdk.KEY_ISO_Left_Tab:
            if not event.state & Gdk.SHIFT_MASK:
                self.quickfill()
                self.description_entry.select_region(0, 0)
        return done_with_input

    def update_conv_info(self):
        from_mnemonic = self.from_commodity.get_mnemonic() if self.from_commodity else None
        to_mnemonic = self.to_commodity.get_mnemonic() if self.to_commodity else None
        if from_mnemonic == "" or from_mnemonic is None or to_mnemonic == "" or to_mnemonic is None:
            return
        rate: Decimal = self.price_entry.get_amount()
        if rate.is_zero():
            string = "1 %s = x %s" % (from_mnemonic, to_mnemonic)
            self.conv_forward.set_text(string)
            string = "1 %s = x %s" % (to_mnemonic, from_mnemonic)
            self.conv_reverse.set_text(string)
        else:
            string = "1 %s = %f %s" % (from_mnemonic, float(rate), to_mnemonic)
            self.conv_forward.set_text(string)
            num, denom = rate.as_integer_ratio()
            rate = Decimal(denom / num)
            string = "1 %s = %f %s" % (to_mnemonic, float(rate), from_mnemonic)
            self.conv_reverse.set_text(string)

    def amount_update_cb(self, widget, event):
        self.amount_entry.evaluate()
        self.update_to_amount()
        return False

    def update_to_amount(self):
        self.price_source = PriceSource.USER_PRICE
        amount_edit = self.amount_entry
        price_edit = self.price_entry
        to_amount_entry = self.to_amount_entry

        price_value = price_edit.get_amount()
        if not price_edit.evaluate() or price_value.is_zero():
            to_amount = Decimal(0)
        else:
            to_amount = amount_edit.get_amount() * price_value
        to_amount_entry.set_amount(to_amount)
        if to_amount.is_zero():
            to_amount_entry.set_text("")
        self.update_conv_info()

    def price_update_cb(self, widget, event):
        self.update_to_amount()
        self.price_type = PriceType.TRN
        return False

    def date_changed_cb(self, widget, d):
        self.update_price()
        return False

    def to_amount_update_cb(self, widget, event):
        self.to_amount_entry.evaluate()
        price_value = self.compute_price_value()
        self.price_entry.set_amount(price_value)
        self.price_source = PriceSource.XFER_DLG_VAL
        self.price_type = PriceType.TRN
        self.update_conv_info()
        return False

    def select_from_account(self, account):
        self.set_selected_account(account, TransferDirection.FROM)

    def select_to_account(self, account):
        self.set_selected_account(account, TransferDirection.TO)

    def select_from_currency(self, cur):
        if cur is None: return
        self.from_currency_label.set_text(cur.get_printname())
        self.amount_entry.set_print_info(PrintAmountInfo.commodity(cur, False))
        self.from_commodity = cur
        self.curr_acct_activate()

    def select_to_currency(self, cur):
        if cur is None or not isinstance(cur, Commodity): return
        self.to_currency_label.set_text(cur.get_printname())
        self.to_amount_entry.set_print_info(PrintAmountInfo.commodity(cur, False))
        self.to_commodity = cur
        self.curr_acct_activate()

    def lock_account_tree(self, direction, hide):
        if direction == TransferDirection.FROM:
            tree_view = self.from_tree_view
            scroll_win = self.from_window
            show_button = self.from_show_button
        elif direction == TransferDirection.TO:
            tree_view = self.to_tree_view
            scroll_win = self.to_window
            show_button = self.to_show_button
        else:
            return
        tree_view.set_sensitive(False)
        show_button.set_sensitive(False)

        if hide:
            scroll_win.hide()
            show_button.hide()

    def lock_from_account_tree(self):
        self.lock_account_tree(TransferDirection.FROM, False)

    def lock_to_account_tree(self):
        self.lock_account_tree(TransferDirection.TO, False)

    def hide_from_account_tree(self):
        self.lock_account_tree(TransferDirection.FROM, True)

    def hide_to_account_tree(self):
        self.lock_account_tree(TransferDirection.TO, True)

    def is_exchange_dialog(self, exch_rate):
        self.amount_entry.set_sensitive(False)
        self.date_selection.set_sensitive(False)
        self.num_entry.set_sensitive(False)
        self.description_entry.set_sensitive(False)
        self.memo_entry.set_sensitive(False)
        self.price_entry.grab_focus()
        self.exch_rate = exch_rate

    def set_amount(self, amount):
        account = self.get_selected_account(TransferDirection.FROM)
        if account is None:
            self.get_selected_account(TransferDirection.TO)
        self.amount_entry.set_amount(amount)

    def set_amount_sensitive(self, is_sensitive):
        self.amount_entry.set_sensitive(is_sensitive)

    @staticmethod
    def set_fetch_sensitive(fetch):
        if QuoteSource.fq_installed():
            fetch.set_sensitive(True)
            fetch.set_tooltip_text("Retrieve the current online quote. "
                                   "This will fail if there is a manually-created price for today.")
            return
        fetch.set_sensitive(False)
        fetch.set_tooltip_text("Finance::Quote must be installed to enable this button.")
        return

    def set_description(self, description):
        if description is None: return
        self.description_entry.set_text(str(description or ""))

    def set_memo(self, memo):
        if memo is None: return
        self.memo_entry.set_text(memo)

    def set_num(self, num):
        if num is None: return
        self.num_entry.set_text(num)

    def set_date(self, set_date):
        self.date_selection.set_time(set_date)

    def set_date_sensitive(self, is_sensitive):
        self.date_selection.set_sensitive(is_sensitive)

    def set_price_edit(self, price_value):
        if price_value.is_zero():
            return
        self.price_entry.set_amount(price_value)
        self.update_to_amount()

    def check_accounts(self, from_account, to_account):
        if from_account is None or to_account is None:
            message = "You must specify an account to transfer from, " \
                      "or to, or both, for this transaction. " \
                      "Otherwise, it will not be recorded."
            show_error(self, message)
            return False
        if from_account == to_account:
            message = "You can't transfer from and to the same account!"
            show_error(self, message)
            return False

        if from_account.get_placeholder() or to_account.get_placeholder():
            placeholder_format = "The account %s does not allow transactions."
            if from_account.get_placeholder():
                name = from_account.get_full_name()
            else:
                name = to_account.get_full_name()
            show_error(self, placeholder_format % name)
            return False

        if not Commodity.is_iso(self.from_commodity):
            message = "You can't transfer from a non-currency account. " \
                      "Try reversing the \"from\" and \"to\" accounts " \
                      "and making the \"amount\" negative."
            show_error(self, message)
            return False
        return True

    def check_edit(self):
        if not self.price_entry.evaluate():
            if self.price_radio.get_active():
                self.parse_error_dialog("You must enter a valid price.")
                return False
        if not self.to_amount_entry.evaluate():
            if self.amount_radio.get_active():
                self.parse_error_dialog("You must enter a valid `to' amount.")
                return False
        return True

    def create_transaction(self, t, from_account, to_account, amount, to_amount):
        trans = Transaction(self.book)
        trans.begin_edit()
        trans.set_currency(self.from_commodity)
        trans.set_post_date(t)
        trans.set_location(self.location_selection.get_ref())
        trans.set_description(self.description_entry.get_text())
        from_split = Split(self.book)
        trans.append_split(from_split)
        to_split = Split(self.book)
        trans.append_split(to_split)
        from_account.begin_edit()
        from_split.set_account(from_account)
        to_account.begin_edit()
        to_split.set_account(to_account)
        from_split.set_base_value(amount * -1, self.from_commodity)
        to_split.set_base_value(amount, self.from_commodity)
        to_split.set_base_value(to_amount, self.to_commodity)
        gs_set_num_action(trans, from_split, self.num_entry.get_text(), None)
        string = self.memo_entry.get_text()
        from_split.set_memo(string)
        to_split.set_memo(string)
        trans.commit_edit()
        from_account.commit_edit()
        to_account.commit_edit()
        if self.transaction_cb:
            self.transaction_cb(trans, self.transaction_user_data)

    @staticmethod
    def swap_commodities(fro, to, value):
        to, fro = fro, to
        num, denom = value.as_integer_ratio()
        return fro, to, Decimal(denom / num)

    def _update_price(self, pr):
        fro = self.from_commodity
        to = self.to_commodity
        value = self.price_entry.get_amount()
        price_value = pr.price.get_value()
        rounded_pr_value = round(price_value, 2)
        if pr.price.get_source() < self.price_source:
            pr.price.unref()
            return
        if pr.reverse:
            fro, to, value = self.swap_commodities(fro, to, value)
        rounded_value = round(value, 2)
        if rounded_value == rounded_pr_value:
            pr.price.unref()
            return
        pr.price.begin_edit()
        pr.price.set_time64(pr.time)
        pr.price.set_type(self.price_type)
        pr.price.set_value(value)
        pr.price.commit_edit()
        pr.price.unref()

    def new_price(self, t):
        fro = self.from_commodity
        to = self.to_commodity
        value = self.price_entry.get_amount()
        value = abs(value)
        if Commodity.is_currency(fro) and Commodity.is_currency(to):
            if value < 0:
                value = self.swap_commodities(fro, to, value)
        elif Commodity.is_currency(fro):
            value = self.swap_commodities(fro, to, value)
        price = Price(self.book)
        price.begin_edit()
        price.set_commodity(fro)
        price.set_currency(to)
        price.set_time64(t)
        price.set_source(self.price_source)
        price.set_type(self.price_type)
        price.set_value(value)
        self.pricedb.add_price(price)
        price.commit_edit()
        price.unref()

    def create_price(self, t):
        if gs_is_euro_currency(self.from_commodity) and gs_is_euro_currency(self.to_commodity):
            return
        pr = self.price_request()
        if self.lookup_price(pr, PriceDate.SAME_DAY):
            self._update_price(pr)
            return
        self.new_price(t)

    @Gtk.Template.Callback()
    def response_cb(self, _, response):
        if response == Gtk.ResponseType.APPLY:
            return
        try:
            self.date_selection.disconnect_by_func(self.date_changed_cb)
        except TypeError:
            pass
        if response != Gtk.ResponseType.OK:
            ComponentManager.close(self.component_id)
            return
        from_account = self.get_selected_account(TransferDirection.FROM)
        to_account = self.get_selected_account(TransferDirection.TO)
        if self.exch_rate is None and not self.check_accounts(from_account, to_account):
            return
        if not self.amount_entry.evaluate():
            self.parse_error_dialog("You must enter a valid amount.")
            return
        amount = self.amount_entry.get_amount()
        if amount.is_zero():
            message = "You must enter an amount to transfer."
            show_error(self, message)
            return
        date = self.date_selection.get_date()
        if not Commodity.equiv(self.from_commodity, self.to_commodity):
            if not self.check_edit():
                return
            to_amount = self.to_amount_entry.get_amount()
        else:
            to_amount = amount
        ComponentManager.suspend()
        if self.exch_rate is not None:
            if self.price_radio.get_active():
                self.update_to_amount()
            price_value = self.compute_price_value()
            self.price_entry.set_amount(price_value)
            self.exch_rate = abs(price_value)
        else:
            self.create_transaction(date, from_account, to_account, amount, to_amount)

        if self.pricedb and not Commodity.equal(self.from_commodity, self.to_commodity):
            self.create_price(datetime.datetime.now().replace(day=date.day, month=date.month, year=date.year))
        ComponentManager.resume()
        ComponentManager.close(self.component_id)

    def close_cb(self, _):
        if self.transaction_cb is not None:
            self.transaction_cb(None, self.transaction_user_data)
        ComponentManager.unregister(self.component_id)

    def fetch(self, _):
        gs_set_busy_cursor(None, True)
        PriceQuotes.add_quotes(lambda e: show_info(self, e), lambda e: show_error(self, e),
                               lambda e: show_warning(self, e), self.book)
        gs_unset_busy_cursor(None)
        pr = self.price_request()
        if self.lookup_price(pr, PriceDate.LATEST):
            price_value = pr.price.get_value()
            if pr.reverse:
                num, denom = price_value.as_integer_ratio()
                price_value = Decimal(denom / num)
            self.set_price_edit(price_value)
            pr.price.unref()

    def close_handler(self, *_):
        dialog = self
        gs_save_window_size(PREFS_GROUP, dialog)
        dialog.hide()
        self.close_cb(dialog)
        dialog.destroy()

    def close(self):
        self.response(Gtk.ResponseType.NONE)

    def set_title(self, title):
        if title:
            self.set_title(title)

    def set_information_label(self, text):
        if text is not None:
            markup_text = "<b>%s</b>" % text
            self.transfer_info_label.set_markup(markup_text)

    def set_account_label(self, text, direction):
        if text is not None:
            markup_text = "<b>%s</b>" % text
            l = self.from_transfer_label if direction == TransferDirection.FROM else self.to_transfer_label
            l.set_markup(markup_text)

    def set_from_account_label(self, label):
        self.set_account_label(label, TransferDirection.FROM)

    def set_to_account_label(self, label):
        self.set_account_label(label, TransferDirection.TO)

    def set_from_show_button_active(self, set_value):
        if self.from_show_button is not None:
            self.from_show_button.set_active(set_value)

    def set_to_show_button_active(self, set_value):
        if self.to_show_button is not None:
            self.to_show_button.set_active(set_value)

    def add_user_specified_button(self, label, callback, *user_data):
        if label is not None and callback is not None:
            box = self.get_content_area()
            button = Gtk.Button.new_with_label(label)
            box.pack_end(button, False, False, 0)
            button.connect("clicked", callback, user_data)
            button.show()

    def toggle_currency_table(self, show_table):
        if self.currency_transfer_grid:
            if show_table:
                self.currency_transfer_grid.show()
            else:
                self.currency_transfer_grid.hide()

    @staticmethod
    def find(find_data, user_data):
        return find_data == user_data

    def run_until_done(self):
        self.disconnect_by_func(self.response_cb)
        while True:
            response = self.run()
            self.response_cb(self, response)
            if response != Gtk.ResponseType.OK and response != Gtk.ResponseType.APPLY:
                return False
            if not ComponentManager.find_first(DIALOG_TRANSFER_CM_CLASS, self.find, self):
                return True

    def quickfill_to_account(self, qf_to_account):
        old = self.quick_fill
        self.quick_fill = TransferDirection.TO if qf_to_account else TransferDirection.FROM
        if old != self.quick_fill:
            self.reload_quickfill()

    def get_selected_account(self, direction):
        if direction == TransferDirection.FROM:
            tree_view = self.from_tree_view
        elif direction == TransferDirection.TO:
            tree_view = self.to_tree_view
        else:
            return None
        account = tree_view.get_selected_account()
        return account

    def set_selected_account(self, account, direction):
        if account is None:
            return
        if direction == TransferDirection.FROM:
            tree_view = self.from_tree_view
            show_button = self.from_show_button
        elif direction == TransferDirection.TO:
            tree_view = self.to_tree_view
            show_button = self.to_show_button
        else:
            return
        t = account.get_type()
        show_button.set_active(t == AccountType.EXPENSE or t == AccountType.INCOME)
        tree_view.set_selected_account(account)

    def set_transaction_cb(self, cb, handler, user_data):
        self.transaction_cb = cb
        self.transaction_user_data = user_data

    def run_exchange_dialog(self, exch_rate, amount, reg_acc, transaction, xfer_com, expanded):
        swap_amounts = False
        transaction_cur = transaction.get_currency()
        reg_com = reg_acc.get_commodity()
        if transaction_cur is None or not isinstance(transaction_cur, Commodity): return True
        if xfer_com is None or not isinstance(xfer_com, Commodity): return True

        if transaction.use_trading_accounts():
            if Commodity.equal(xfer_com, transaction_cur):
                return False, Decimal(1)
            swap_amounts = expanded
        elif Commodity.equal(reg_com, transaction_cur):
            swap_amounts = False
        elif Commodity.equal(reg_com, xfer_com):
            swap_amounts = True
        else:
            rate = transaction.get_conv_rate(reg_acc)
            amount /= rate
        if swap_amounts:
            self.select_to_currency(transaction_cur)
            self.select_from_currency(xfer_com)
            if not exch_rate.is_zero():
                num, denom = exch_rate.as_integer_ratio()
                exch_rate = Decimal(denom / num)
            amount *= -1
        else:
            self.select_to_currency(xfer_com)
            self.select_from_currency(transaction_cur)
            if transaction.use_trading_accounts():
                amount *= -1

        self.hide_to_account_tree()
        self.hide_from_account_tree()
        self.set_amount(amount)
        self.update_to_amount()
        self.set_price_edit(exch_rate)
        if not self.run_until_done():
            return True, exch_rate
        if swap_amounts:
            num, denom = exch_rate.as_integer_ratio()
            exch_rate = Decimal(denom / num)
        return False, exch_rate


GObject.type_register(TransferDialog)
