import math

from gi.repository import Gtk, GObject

from gasstation.utilities.cursors import gs_set_busy_cursor, gs_unset_busy_cursor


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/account/renumber.ui')
class RenumberAccountDialog(Gtk.Dialog):
    __gtype_name__ = "RenumberAccountDialog"
    prefix: Gtk.Entry = Gtk.Template.Child("prefix_entry")
    interval: Gtk.SpinButton = Gtk.Template.Child("interval_spin")
    start: Gtk.SpinButton = Gtk.Template.Child("start_spin")
    example1: Gtk.Label = Gtk.Template.Child("example1_label")
    example2: Gtk.Label = Gtk.Template.Child("example2_label")
    interval_adjustment: Gtk.Label = Gtk.Template.Child()

    def __init__(self, window, account, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if window is not None:
            self.set_transient_for(window)
        self.num_children = account.get_n_children()
        self.account = account
        self.prefix.set_text(account.get_code() or "")
        self.update_examples()
        self.show_all()

    def update_examples(self):
        if self.num_children < 0:
            return
        prefix = self.prefix.get_text()
        interval = self.interval.get_value_as_int()
        start = self.start.get_value_as_int()
        if interval <= 0:
            interval = 10
        num_digits = int(math.log10(float(self.num_children * interval) + 1))
        if len(prefix):
            strg = "%s-%0*d" % (prefix, num_digits, interval)
        else:
            strg = "%0*d" % (num_digits, interval)
        self.example1.set_text(strg)
        if len(prefix):
            strg = "%s-%0*d" % (prefix, num_digits,
                                start + (interval * self.num_children))
        else:
            strg = "%0*d" % (num_digits, start + (interval * self.num_children))

        self.example2.set_text(strg)

    @Gtk.Template.Callback()
    def prefix_changed_cb(self, _):
        self.update_examples()

    @Gtk.Template.Callback()
    def interval_changed_cb(self, _):
        self.update_examples()

    @Gtk.Template.Callback()
    def response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            children = sorted(self.account.get_children())
            if not len(children):
                return
            start = self.start.get_value_as_int()
            prefix = self.prefix.get_chars(0, -1)
            interval = self.interval.get_value_as_int()

            if interval <= 0:
                interval = 10

            num_digits = int(math.log10(float(self.num_children * interval) + 1))

            be = self.account.book.get_backend()
            if be is not None:
                with be.add_lock():
                    gs_set_busy_cursor(None, True)
                    for tmp in children:
                        start += interval
                        if len(prefix):
                            strg = "%s-%0*d" % (prefix, num_digits, start)
                        else:
                            strg = "%0*d" % (num_digits, start)
                        tmp.set_code(strg)
                    gs_unset_busy_cursor(None)
        dialog.destroy()

    def __repr__(self):
        return '<RenumberAccountDialog>'


GObject.type_register(RenumberAccountDialog)
