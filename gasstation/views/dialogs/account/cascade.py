from gi.repository import Gtk, GObject

from gasstation.utilities.cursors import gs_set_busy_cursor, gs_unset_busy_cursor
from .color import *


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/account/cascade.ui')
class CascadeAccountPropertiesDialog(Gtk.Dialog):
    __gtype_name__ = "CascadeAccountPropertiesDialog"
    color_label: Gtk.Label = Gtk.Template.Child()
    placeholder_label: Gtk.Label = Gtk.Template.Child()
    hidden_label: Gtk.Label = Gtk.Template.Child()
    over_write: Gtk.CheckButton = Gtk.Template.Child()
    color_button: Gtk.ColorButton = Gtk.Template.Child()
    color_button_default: Gtk.Button = Gtk.Template.Child()
    enable_color: Gtk.CheckButton = Gtk.Template.Child()
    enable_placeholder: Gtk.CheckButton = Gtk.Template.Child()
    enable_hidden: Gtk.CheckButton = Gtk.Template.Child()
    placeholder_button: Gtk.CheckButton = Gtk.Template.Child()
    hidden_button: Gtk.CheckButton = Gtk.Template.Child()
    color_box: Gtk.Box = Gtk.Template.Child()
    placeholder_box: Gtk.Box = Gtk.Template.Child()
    hidden_box: Gtk.Box = Gtk.Template.Child()

    def __init__(self, window, account, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if window is not None:
            self.set_transient_for(window)
        self.old_color_string = ""
        self.color_button.set_use_alpha(False)
        self.color_button_default.connect("clicked", lambda b: color_default_cb(self.color_button))
        fullname = account.get_full_name()
        self.color_label.set_text("Set the account color for account '%s' "
                                  "including all sub-accounts to the selected color:" % fullname)
        self.placeholder_label.set_text("Set the account placeholder value for account '%s' "
                                        "including all sub-accounts" % fullname)
        self.hidden_label.set_text("Set the account hidden value for account '%s' "
                                   "including all sub-accounts" % fullname)
        color_string = account.get_color()
        if color_string is None or color_string == "":
            color_string = DEFAULT_COLOR
        else:
            self.old_color_string = color_string
        color = Gdk.RGBA()
        if not Gdk.RGBA.parse(color, color_string):
            Gdk.RGBA.parse(color, DEFAULT_COLOR)
        self.color_button.set_rgba(color)
        self.account = account
        self.enable_color.connect("toggled", self.enable_box_cb, self.color_box)
        self.enable_placeholder.connect("toggled", self.enable_box_cb, self.placeholder_box)
        self.enable_hidden.connect("toggled", self.enable_box_cb, self.hidden_box)
        self.show()

    def enable_box_cb(self, widget, box):
        box.set_sensitive(widget.get_active())

    @staticmethod
    def update_account_color(acc, old_color, new_color, replace):
        if new_color != "" and new_color is not None:
            if old_color is None or old_color == "" or replace:
                if new_color != old_color:
                    acc.set_color(new_color)
        else:
            acc.set_color(DEFAULT_COLOR)

    @Gtk.Template.Callback()
    def response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            accounts = self.account.get_descendants()
            color_active = self.enable_color.get_active()
            placeholder_active = self.enable_placeholder.get_active()
            hidden_active = self.enable_hidden.get_active()
            replace = self.over_write.get_active()
            placeholder = self.placeholder_button.get_active()
            hidden = self.hidden_button.get_active()

            be = self.account.book.get_backend()
            if be is not None:
                with be.add_lock():
                    gs_set_busy_cursor(None, True)
                    if color_active:
                        new_color = self.color_button.get_rgba()
                        new_color_string = new_color.to_string()
                        if new_color_string == DEFAULT_COLOR:
                            new_color_string = None
                        self.update_account_color(self.account, self.old_color_string, new_color_string, replace)
                    if placeholder_active:
                        self.account.set_placeholder(placeholder)
                    if hidden_active:
                        self.account.set_hidden(hidden)
                    if any(accounts):
                        for acc in accounts:
                            if color_active:
                                self.update_account_color(acc, acc.get_color(), new_color_string, replace)
                            if placeholder_active:
                                acc.set_placeholder(placeholder)
                            if hidden_active:
                                acc.set_hidden(hidden)
                    gs_unset_busy_cursor(None)
        dialog.destroy()

    def __repr__(self):
        return '<CascadeAccountPropertiesDialog>'


GObject.type_register(CascadeAccountPropertiesDialog)
