from gasstation.utilities.dialog import *
from gasstation.utilities.ui import *
from gasstation.views.tree import tree_view_column_set_default_width
from libgasstation.core.component import *
from libgasstation.core.helpers import gs_get_num_action
from libgasstation.core.invoice import Invoice
from libgasstation.core.lot import *

LOT_VIEWER_CM_CLASS = "dialog-lot-viewer"


class LotCol(GObject.GEnum):
    TYPE = 0
    OPEN = 1
    CLOSE = 2
    TITLE = 3
    BALN = 4
    BALN_DOUBLE = 5
    GAINS = 6
    GAINS_DOUBLE = 7
    PNTR = 8
    NUM_COLS = 9


class SplitCol(GObject.GEnum):
    DATE = 0
    NUM = 1
    DESCRIPTION = 2
    AMOUNT = 3
    AMOUNT_DOUBLE = 4
    VALUE = 5
    VALUE_DOUBLE = 6
    GAIN_LOSS = 7
    GAIN_LOSS_DOUBLE = 8
    BALANCE = 9
    BALANCE_DOUBLE = 10
    PNTR = 11
    NUM_COLS = 12


RESPONSE_VIEW = 1
RESPONSE_DELETE = 2
RESPONSE_SCRUB_LOT = 3
RESPONSE_SCRUB_ACCOUNT = 4
RESPONSE_NEW_LOT = 5

PREFS_GROUP = "dialogs.lot-viewer"
PREF_HPOS = "hpane-position"
PREF_VPOS = "vpane-position"


class LotViewer:
    def __init__(self, parent, account):
        self.delete_button = None
        self.scrub_lot_button = None
        self.new_lot_button = None
        self.lot_view = None
        self.lot_store = None
        self.lot_notes: Gtk.TextView = None
        self.title_entry = None
        self.split_in_lot_view = None
        self.split_in_lot_store = None
        self.split_free_view = None
        self.split_free_store = None
        self.add_split_to_lot_button = None
        self.remove_split_from_lot_button = None
        self.only_show_open_lots_checkbutton = None
        self.account = None
        self.selected_lot = None
        self.component_id = 0
        self.account = account
        self.create(parent)
        self.fill()
        self.show_splits_free()
        self.component_id = ComponentManager.register(LOT_VIEWER_CM_CLASS,
                                                      self.refresh_handler,
                                                      self.close_handler)

        ComponentManager.watch_entity_type(self.component_id,
                                           ID_LOT,
                                           EVENT_CREATE | EVENT_ADD | EVENT_REMOVE | EVENT_MODIFY | EVENT_DESTROY)

        self.window.show_all()
        gs_window_adjust_for_screen(self.window)

    @staticmethod
    def find_first_currency(lot):
        split_list = lot.get_split_list()
        for s in split_list:
            if not s.get_amount().is_zero():
                continue
            trans = s.get_transaction()
            return trans.get_currency()
        return None

    @staticmethod
    def get_realized_gains(lot, currency):
        zero = Decimal(0)
        gains = zero
        if currency is None:
            return zero
        split_list = lot.get_split_list()
        for s in split_list:
            if not s.get_amount().is_zero():
                continue
            trans = s.get_transaction()
            if trans.get_currency() != currency:
                continue
            gains += s.get_value()
        return gains

    def show_splits_in_lot(self):
        lot = self.selected_lot
        if lot is None:
            return
        split_list = lot.get_split_list()
        self.split_viewer_fill(self.split_in_lot_store, split_list)

    def clear_splits_in_lot(self):
        self.split_in_lot_store.clear()

    def show_splits_free(self):
        self.split_free_store.clear()
        split_list = self.account.get_splits()
        filtered_list = list(filter(lambda sp: sp.get_lot() is None, split_list))
        self.split_viewer_fill(self.split_free_store, filtered_list)

    def save_current_lot(self):
        lot = self.selected_lot
        if lot is not None:
            lot.begin_edit()
            lot.set_title(self.title_entry.get_text())
            buffer = self.lot_notes.get_buffer()
            notes = buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter(), True)
            lot.set_notes(notes)
            lot.commit_edit()

    def unset_lot(self):
        self.selected_lot = None
        self.title_entry.set_text("")
        self.title_entry.set_editable(False)
        self.lot_notes.get_buffer().set_text("")
        self.lot_notes.set_editable(False)
        self.clear_splits_in_lot()
        self.delete_button.set_sensitive(False)
        self.scrub_lot_button.set_sensitive(False)

    def select_row(self, lot):
        self.save_current_lot()
        self.title_entry.set_text(lot.get_title() or "")
        self.title_entry.set_editable(True)
        self.lot_notes.get_buffer().set_text(lot.get_notes() or "")
        self.lot_notes.set_editable(True)
        self.selected_lot = lot
        self.show_splits_in_lot()
        self.delete_button.set_sensitive(True)
        self.scrub_lot_button.set_sensitive(True)

    def unselect_row(self):
        self.save_current_lot()
        self.unset_lot()

    def fill(self):
        selected_lot = None
        found = False
        lot_list = self.account.get_lots()
        selection = self.lot_view.get_selection()
        model, _iter = selection.get_selected()
        if _iter is not None:
            selected_lot = model.get_value(_iter, LotCol.PNTR)
        self.lot_store.clear()
        store = self.lot_store
        for lot in lot_list:
            esplit = lot.get_earliest_split()
            etrans = esplit.get_transaction() if esplit is not None else None
            open_date = etrans.get_enter_date() if etrans is not None else None
            amt_baln = lot.get_balance()
            currency = self.find_first_currency(lot)
            gains_baln = self.get_realized_gains(lot, currency)

            if self.only_show_open_lots_checkbutton.get_active() and lot.is_closed():
                continue
            _iter = store.append()
            type_buff = ""
            if Invoice.from_lot(lot) is not None:
                type_buff = "I"
            store.set_value(_iter, LotCol.TYPE, type_buff)
            store.set_value(_iter, LotCol.OPEN, open_date)

            if lot.is_closed():
                fsplit = lot.get_latest_split()
                ftrans = fsplit.get_transaction()
                if ftrans is not None:
                    close_date = ftrans.get_enter_date()
                    store.set_value(_iter, LotCol.CLOSE, close_date)
            else:
                store.set_value(_iter, LotCol.CLOSE, None)
            store.set_value(_iter, LotCol.TITLE, lot.get_title())
            store.set_value(_iter, LotCol.BALN, sprintamount(amt_baln, PrintAmountInfo.account(self.account, True)))
            store.set_value(_iter, LotCol.BALN_DOUBLE, float(amt_baln))
            store.set_value(_iter, LotCol.GAINS, sprintamount(gains_baln, PrintAmountInfo.commodity(currency, True)))
            store.set_value(_iter, LotCol.GAINS_DOUBLE, float(gains_baln))
            store.set_value(_iter, LotCol.PNTR, lot)

        if selected_lot:
            model = self.lot_store
            _iter = model.get_iter_first()
            while _iter is not None:
                this_lot = model.get_value(_iter, LotCol.PNTR)
                if this_lot == selected_lot:
                    selection.select_iter(_iter)
                    found = True
                _iter = model.iter_next(_iter)
        if not found:
            selection.unselect_all()

    def get_selected_split(self, view):
        split = None
        selection = view.get_selection()
        model, _iter = selection.get_selected()
        if _iter is not None:
            split = model.get_value(_iter, SplitCol.PNTR)
        return split

    @staticmethod
    def can_remove_split_from_lot(split, lot):
        lot_invoice = Invoice.from_lot(lot)
        transaction = split.get_transaction()
        transaction_invoice = Invoice.from_transaction(transaction)
        if lot_invoice is not None and lot_invoice == transaction_invoice:
            return False
        return True

    def split_viewer_fill(self, store, split_list):
        is_business_lot = False
        baln = Decimal(0)
        if self.selected_lot is not None and self.selected_lot.get_account() is not None:
            is_business_lot = self.selected_lot.get_account().get_type().is_payable_receivable()
        self.split_in_lot_store.clear()
        for split in split_list:
            trans = split.get_transaction()
            if trans is None:
                continue
            date = trans.get_enter_date()
            if not is_business_lot and split.get_amount().is_zero():
                continue
            _iter = store.append()
            store.set_value(_iter, SplitCol.DATE, date)
            store.set_value(_iter, SplitCol.NUM, gs_get_num_action(trans, split))
            store.set_value(_iter, SplitCol.DESCRIPTION, trans.get_description())
            amnt = split.get_amount()
            amtbuff = sprintamount(amnt, PrintAmountInfo.account(self.account, True))
            store.set_value(_iter, SplitCol.AMOUNT, amtbuff)
            store.set_value(_iter, SplitCol.AMOUNT_DOUBLE, float(amnt))
            currency = trans.get_currency()
            value = split.get_value()
            if self.selected_lot is not None and not is_business_lot:
                value = value * -1
            valbuff = sprintamount(value, PrintAmountInfo.commodity(currency, True))
            store.set_value(_iter, SplitCol.VALUE, valbuff)
            store.set_value(_iter, SplitCol.VALUE_DOUBLE, float(value))
            # gains = split.get_cap_gains()
            # gainbuff = sprintamount(gains, PrintAmountInfo.commodity(currency, True))
            # store.set_value(_iter, SplitCol.GAIN_LOSS, gainbuff)
            # store.set_value(_iter, SplitCol.GAIN_LOSS_DOUBLE, gains.to_double())
            baln += amnt
            balnbuff = sprintamount(baln, PrintAmountInfo.account(self.account, True))
            store.set_value(_iter, SplitCol.BALANCE, balnbuff)
            store.set_value(_iter, SplitCol.BALANCE_DOUBLE, float(baln))
            store.set_value(_iter, SplitCol.PNTR, split)

    def update_split_buttons(self):
        self.add_split_to_lot_button.set_sensitive(False)
        self.remove_split_from_lot_button.set_sensitive(False)
        if self.selected_lot is not None:
            if self.get_selected_split(self.split_free_view) is not None:
                self.add_split_to_lot_button.set_sensitive(True)
            split = self.get_selected_split(self.split_in_lot_view)
            if split is not None and self.can_remove_split_from_lot(split, self.selected_lot):
                self.remove_split_from_lot_button.set_sensitive(True)

    def refresh(self):
        self.fill()
        self.show_splits_free()
        self.show_splits_in_lot()

    def refresh_handler(self, changes):
        self.refresh()

    def close_handler(self):
        self.save_current_lot()
        gs_save_window_size(PREFS_GROUP, self.window)
        self.window.destroy()

    def title_entry_changed_cb(self, _):
        title = self.title_entry.get_text()

        selection = self.lot_view.get_selection()
        model, _iter = selection.get_selected()
        if _iter is not None:
            model.set_value(_iter, LotCol.TITLE, title)

    def selection_changed_cb(self, selection):
        model, _iter = selection.get_selected()
        if _iter is not None:
            lot = model.get_value(_iter, LotCol.PNTR)
            self.select_row(lot)
        else:
            self.unselect_row()
        self.update_split_buttons()

    def window_destroy_cb(self, *args):
        ComponentManager.unregister(self.component_id)

    def split_selection_changed_cb(self, _):
        self.update_split_buttons()

    def add_split_to_lot_cb(self, _):
        if self.selected_lot is None:
            return
        split = self.get_selected_split(self.split_free_view)
        if split is None:
            return
        self.account.begin_edit()
        self.selected_lot.add_split(split)
        self.account.commit_edit()
        self.refresh()

    def remove_split_from_lot_cb(self, widget):
        if self.selected_lot is None:
            return
        split = self.get_selected_split(self.split_in_lot_view)
        if split is None:
            return
        if not self.can_remove_split_from_lot(split, self.selected_lot):
            return
        self.account.begin_edit()
        self.selected_lot.remove_split(split)
        self.account.commit_edit()
        self.refresh()

    def only_show_open_lots_changed_cb(self, widget):
        self.refresh()

    def response_cb(self, dialog, response):
        lot = self.selected_lot
        if response == Gtk.ResponseType.CLOSE:
            ComponentManager.close(self.component_id)
            return
        elif response == RESPONSE_VIEW:
            if lot is None:
                return

        elif response == RESPONSE_DELETE:
            if lot is None:
                return
            if Invoice.from_lot(lot) is not None:
                return
            lot.get_account().remove_lot(lot)
            lot.destroy()
            self.unset_lot()
            self.fill()

        elif response == RESPONSE_SCRUB_LOT:
            if lot is None:
                return
            if self.account.get_type().is_payable_receivable():
                # gsScrubBusinessLot (lot)
                pass
            else:
                # xaccScrubLot (lot)
                pass
            self.fill()
            self.show_splits_in_lot()

        elif response == RESPONSE_SCRUB_ACCOUNT:
            ComponentManager.suspend()
            if self.account.get_type().is_payable_receivable():
                # gsScrubBusinessAccountLots (self.account, gs_window_show_progress)
                pass
            else:
                # xaccAccountScrubLots (self.account)
                pass
            ComponentManager.resume()
            self.fill()
            self.show_splits_free()
            self.show_splits_in_lot()

        elif response == RESPONSE_NEW_LOT:
            self.save_current_lot()
            lot = Lot.make_default(self.account)
            self.account.insert_lot(lot)

    def print_date(self, tree_column, cell, tree_model, _iter, col):
        doc_date_str = "Open"
        doc_date_time = tree_model.get_value(_iter, col)
        if doc_date_time is not None:
            doc_date_str = print_datetime(doc_date_time)
        cell.set_property("text", doc_date_str)

    @staticmethod
    def configure_number_columns(column, renderer, sort_column):
        column.set_sort_column_id(sort_column)
        renderer.set_alignment(1.0, 0.5)
        column.set_alignment(1.0)
        renderer.set_padding(5, 0)

    def init_lot_view(self):
        if self.lot_view is None:
            return
        view = self.lot_view
        store = Gtk.ListStore(str, object, object, str, str, float, str, float, object)
        view.set_model(store)
        self.lot_store = store

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Type", renderer, text=LotCol.TYPE)
        column.set_sort_column_id(LotCol.TYPE)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Opened", renderer,
                                    text=LotCol.OPEN)
        column.set_sort_column_id(LotCol.OPEN)
        tree_view_column_set_default_width(view, column, "31-12-2013")
        column.set_cell_data_func(renderer, self.print_date, LotCol.OPEN)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Closed", renderer, text=LotCol.CLOSE)
        column.set_sort_column_id(LotCol.CLOSE)
        tree_view_column_set_default_width(view, column, "31-12-2013")
        column.set_cell_data_func(renderer, self.print_date, LotCol.CLOSE)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Title", renderer,
                                    text=LotCol.TITLE)
        column.set_sort_column_id(LotCol.TITLE)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Balance", renderer,
                                    text=LotCol.BALN)
        self.configure_number_columns(column, renderer, LotCol.BALN_DOUBLE)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Gains", renderer,
                                    text=LotCol.GAINS)
        self.configure_number_columns(column, renderer, LotCol.GAINS_DOUBLE)
        view.append_column(column)

        selection = view.get_selection()
        selection.connect("changed", self.selection_changed_cb)
        self.only_show_open_lots_checkbutton.connect("toggled", self.only_show_open_lots_changed_cb)

    def init_split_view(self, view):
        store = Gtk.ListStore(object, str, str, str, float, str, float, str, float, str, float, object)
        view.set_model(store)
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Date", renderer,
                                    text=SplitCol.DATE)
        column.set_sort_column_id(SplitCol.DATE)
        tree_view_column_set_default_width(view, column, "31-12-2013")
        column.set_cell_data_func(renderer, self.print_date, SplitCol.DATE)
        view.append_column(column)
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Num", renderer,
                                    text=SplitCol.NUM)
        column.set_sort_column_id(SplitCol.NUM)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Description", renderer,
                                    text=SplitCol.DESCRIPTION)
        column.set_sort_column_id(SplitCol.DESCRIPTION)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Amount", renderer,
                                    text=SplitCol.AMOUNT)
        self.configure_number_columns(column, renderer, SplitCol.AMOUNT_DOUBLE)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Value", renderer,
                                    text=SplitCol.VALUE)
        self.configure_number_columns(column, renderer, SplitCol.VALUE_DOUBLE)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Gain/Loss", renderer,
                                    text=SplitCol.GAIN_LOSS)
        self.configure_number_columns(column, renderer, SplitCol.GAIN_LOSS_DOUBLE)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Balance", renderer,
                                    text=SplitCol.BALANCE)
        self.configure_number_columns(column, renderer, SplitCol.BALANCE_DOUBLE)
        view.append_column(column)

        selection = view.get_selection()
        selection.connect("changed", self.split_selection_changed_cb)
        return store

    def init_split_views(self):
        self.split_free_store = self.init_split_view(self.split_free_view)
        self.split_in_lot_store = self.init_split_view(self.split_in_lot_view)

    def init_split_buttons(self):
        self.add_split_to_lot_button.connect("clicked", self.add_split_to_lot_cb)
        self.remove_split_from_lot_button.connect("clicked", self.remove_split_from_lot_cb)

    def create(self, parent):
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/lot-viewer.ui",
                                          ["lot_viewer_dialog"])
        self.window = builder.get_object("lot_viewer_dialog")
        self.window.set_transient_for(parent)
        gs_widget_set_style_context(self.window, "LotViewerDialog")
        win_title = "Lots in Account %s" % self.account.get_name()
        self.window.set_title(win_title)
        self.delete_button = builder.get_object("delete_button")
        self.scrub_lot_button = builder.get_object("scrub_lot_button")
        self.new_lot_button = builder.get_object("new_lot_button")
        self.lot_view = builder.get_object("lot_view")
        self.only_show_open_lots_checkbutton = builder.get_object("only_show_open_lots_checkbutton")
        self.init_lot_view()
        self.lot_notes = builder.get_object("lot_notes_text")
        self.title_entry = builder.get_object("lot_title_entry")
        self.split_in_lot_view = builder.get_object("split_in_lot_view")
        self.split_free_view = builder.get_object("split_free_view")
        self.init_split_views()
        self.add_split_to_lot_button = builder.get_object("add_split_to_lot_button")
        self.remove_split_from_lot_button = builder.get_object("remove_split_from_lot_button")
        self.init_split_buttons()
        self.lot_view.set_grid_lines(gs_tree_view_get_grid_lines_pref())
        self.split_in_lot_view.set_grid_lines(gs_tree_view_get_grid_lines_pref())
        self.split_free_view.set_grid_lines(gs_tree_view_get_grid_lines_pref())
        if gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_SAVE_GEOMETRY):
            obj = builder.get_object("lot_vpaned")
            gs_pref_bind(PREFS_GROUP, PREF_VPOS, obj, "position")
            obj = builder.get_object("lot_hpaned")
            gs_pref_bind(PREFS_GROUP, PREF_HPOS, obj, "position")
        self.selected_lot = None
        builder.connect_signals(self)
        self.update_split_buttons()
        gs_restore_window_size(PREFS_GROUP, self.window, parent)
