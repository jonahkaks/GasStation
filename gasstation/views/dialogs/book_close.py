from gasstation.utilities.ui import gs_account_or_default_currency
from gasstation.views.selections.account import *
from gasstation.views.selections.date_edit import *
from libgasstation.core.account import Account, AccountType
from libgasstation.core.commodity import Commodity
from libgasstation.core.component import *
from libgasstation.core.numeric import *
from libgasstation.core.transaction import Split, Transaction

DIALOG_BOOK_CLOSE_CM_CLASS = "dialog-book-close"


class CloseAccountsCB:
    cbw = None
    base_acct = None
    acct_type = None
    transactions = None
    hash_size = 0


class CACBTransactionList:
    cmdty = None
    transaction = {}
    total = None


class CloseBookWindow(GObject.Object):
    def __init__(self):
        super().__init__()
        self.book = None
        self.dialog = None
        self.close_date_widget = None
        self.income_acct_widget = None
        self.expense_acct_widget = None
        self.desc_widget = None
        self.close_date = None
        self.desc = None
        self.component_manager_id = 0

    @staticmethod
    def find_or_create_transaction(cacb, cmdty):
        if cacb is None or not isinstance(cacb, CloseAccountsCB) or cmdty is None or not isinstance(cmdty, Commodity):
            return None
        transaction = cacb.transactions.get(cmdty)
        if transaction is None:
            transaction = CACBTransactionList()
            transaction.cmdty = cmdty
            transaction.total = Decimal(0)
            transaction.transaction = Transaction(cacb.cbw.book)
            #TODO ADD LOCATION TO BOOK CLOSE, BUT SEEMS NOT NEEDED HERE
            transaction.transaction.begin_edit()
            transaction.transaction.set_post_date(cacb.cbw.close_date)
            transaction.transaction.set_description(cacb.cbw.desc)
            transaction.transaction.set_currency(cmdty)
            transaction.transaction.set_closing()
            cacb.transactions[cmdty] = transaction
        return transaction

    @staticmethod
    def close_accounts_cb(a, cacb):
        if a is None or cacb is None or cacb.cbw is None or cacb.transactions is None: return False
        if cacb.acct_type != a.get_type():
            return
        end = cacb.cbw.close_date
        bal = a.get_balance_as_of_date(end)
        if bal.is_zero():
            return
        acct_commodity = gs_account_or_default_currency(a)[0]
        if acct_commodity is None: return False
        transaction = CloseBookWindow.find_or_create_transaction(cacb, acct_commodity)
        split = Split(cacb.cbw.book)
        split.set_transaction(transaction.transaction)
        a.begin_edit()
        split.set_account(a)
        split.set_base_value(bal*-1, acct_commodity)
        a.commit_edit()
        transaction.total = transaction.total.add_fixed(bal)

    @staticmethod
    def finish_transaction_cb(cmdty, transaction, cacb):
        if cmdty is None or transaction is None or cacb is None:
            return False
        if cacb.hash_size == 1 and cmdty == cacb.base_acct.get_commodity():
            acc = cacb.base_acct
        else:
            acc = cacb.base_acct.lookup_by_name(cmdty.get_mnemonic())
        if acc is None:
            acc = Account(cacb.cbw.book)
            acc.begin_edit()
            acc.set_type(AccountType.EQUITY)
            acc.set_name(cmdty.get_mnemonic())
            acc.set_description(cmdty.get_mnemonic())
            acc.set_commodity(cmdty)
            cacb.base_acct.append_child(acc)
            acc.commit_edit()
        split = Split(cacb.cbw.book)
        split.set_transaction(transaction.transaction)
        acc.begin_edit()
        split.set_account(acc)
        split.set_base_value(transaction.total, cmdty)
        acc.commit_edit()
        transaction.transaction.commit_edit()

    def close_accounts_of_type(self, acct, acct_type):
        cacb = CloseAccountsCB()
        if acct is None: return
        cacb.cbw = self
        cacb.base_acct = acct
        cacb.acct_type = acct_type
        cacb.transactions = {}
        root_acct = self.book.get_root_account()
        root_acct.foreach_descendant(self.close_accounts_cb, cacb)
        cacb.hash_size = len(cacb.transactions)
        if cacb.hash_size:
            for k, v in cacb.transactions.items():
                self.finish_transaction_cb(k, v, cacb)
            cacb.transactions = None

    @staticmethod
    def close_handler(dialog):
        dialog.destroy()

    def destroy_cb(self, dialog):
        if self.component_manager_id:
            ComponentManager.unregister(self.component_manager_id)
            self.component_manager_id = 0
        return True

    def response_cb(self, dialog, response, *unused):
        if dialog is None: return
        if response == Gtk.ResponseType.HELP:
            pass

        elif response == Gtk.ResponseType.OK:
            self.close_date = self.close_date_widget.get_date()
            self.desc = self.desc_widget.get_text()
            income_acct = self.income_acct_widget.get_account()
            expense_acct = self.expense_acct_widget.get_account()

            if income_acct is None:
                show_error(self.dialog, "Please select an Equity account to hold the total Period Income.")
                return
            if expense_acct is None:
                show_error(self.dialog, "Please select an Equity account to hold the total Period Expense.")
                return
            ComponentManager.suspend()
            self.close_accounts_of_type(income_acct, AccountType.INCOME)
            self.close_accounts_of_type(expense_acct, AccountType.EXPENSE)
            ComponentManager.resume()
            dialog.destroy()
        else:
            dialog.destroy()
        return True

    @classmethod
    def new(cls, book, parent):
        if book is None: return
        self = GObject.new(cls)
        self.book = book
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/book-close.ui",
                                          ["close_book_dialog"])
        self.dialog = builder.get_object("close_book_dialog")
        gs_widget_set_style_context(self.dialog, "BookCloseDialog")
        if parent is not None:
            self.dialog.set_transient_for(parent)
        self.dialog.set_border_width(10)
        box = builder.get_object("date_box")
        self.close_date_widget = DateSelection()
        box.pack_start(self.close_date_widget, True, True, 0)
        equity_list = [AccountType.EQUITY]
        box = builder.get_object("income_acct_box")
        self.income_acct_widget = AccountSelection()
        self.income_acct_widget.set_account_filters(equity_list)
        self.income_acct_widget.set_new_account_ability(True)
        box.pack_start(self.income_acct_widget, True, True, 0)
        box = builder.get_object("expense_acct_box")
        self.expense_acct_widget = AccountSelection()
        self.expense_acct_widget.set_account_filters(equity_list)
        self.expense_acct_widget.set_new_account_ability(True)
        box.pack_start(self.expense_acct_widget, True, True, 0)
        self.desc_widget = builder.get_object("desc_entry")
        builder.connect_signals(self)
        self.component_manager_id = ComponentManager.register(DIALOG_BOOK_CLOSE_CM_CLASS, None, self.close_handler,
                                                              self.dialog)
        ComponentManager.set_session(self.component_manager_id, Session.get_current_session())
        self.dialog.connect("destroy", self.destroy_cb)
        self.dialog.show_all()
