from gasstation.views.selections import ReferenceSelection
from gasstation.views.selections.date_format import DateFormatSelection

PREF_CLOCK_24H = "clock-24h"

from gasstation.utilities.combott import ComboTT
from gasstation.utilities.dialog import *
from gasstation.utilities.options import *
from gasstation.views.account import AccountView
from gasstation.views.commodity_edit import *
from gasstation.views.selections.account import AccountSelection
from gasstation.views.selections.currency import CurrencySelection
from gasstation.views.selections.date_edit import DateSelection
from gasstation.views.selections.general import *
from libgasstation.core._declbase import ID_ACCOUNT
from libgasstation.core.component import ComponentManager

DIALOG_OPTIONS_CM_CLASS = "dialog-options"
DIALOG_BOOK_OPTIONS_CM_CLASS = "dialog-book-options"
PREFS_GROUP = "dialogs.options"
MAX_TAB_COUNT = 6
LAST_SELECTION = "last-selection"

gain_loss_accounts_in_filter = 0
from gi.repository import GObject, Gtk, Gdk, GdkPixbuf


class RdPositions(GObject.GEnum):
    AB_BUTTON_POS = 0
    AB_WIDGET_POS = 1
    REL_BUTTON_POS = 2
    REL_WIDGET_POS = 3


class CurrencyAccountingData:
    def __init__(self):
        self.currency_radiobutton_0 = None
        self.currency_radiobutton_1 = None
        self.currency_radiobutton_2 = None
        self.book_currency_widget = None
        self.default_cost_policy_widget = None
        self.default_gain_loss_account_widget = None
        self.book_currency_table = None
        self.book_currency_vbox = None
        self.gain_loss_account_del_button = None
        self.gain_loss_account_table = None
        self.default_gain_loss_account_text = None
        self.option = None
        self.retrieved_book_currency = None
        self.prior_gain_loss_account = None


class OptionDialog:
    optionTable = {}
    user_data = defaultdict(dict)

    def __init__(self):
        self.window = None
        self.notebook = None
        self.page_list_view = None
        self.page_list = None
        self.toplevel = False
        self.apply_cb = None
        self.apply_cb_data = None
        self.help_cb = None
        self.help_cb_data = None
        self.close_cb = None
        self.close_cb_data = None
        self.option_db = None
        self.component_class = None
        self.destroyed = False
        self.component_id = 0

    @staticmethod
    def changed_internal(widget, sensitive):
        while widget is not None and not isinstance(widget, Gtk.Window):
            widget = widget.get_parent()
        if widget is None:
            return
        if isinstance(widget, Gtk.Container):
            for achild in filter(lambda a: isinstance(a, Gtk.Box), widget.get_children()):
                for bchild in filter(lambda a: isinstance(a, Gtk.ButtonBox), achild.get_children()):
                    for cchild in bchild.get_children():
                        name = cchild.get_name()
                        if name == "ok_button" or name == "apply_button":
                            cchild.set_sensitive(sensitive)

    def changed(self):
        self.changed_internal(self.window, True)

    @staticmethod
    def changed_widget_cb(widget, option: Option):
        option.set_changed(True)
        option.widget_changed()
        OptionDialog.changed_internal(widget, True)

    @classmethod
    def changed_option_cb(cls, _, option: Option):
        widget = option.get_widget()
        cls.changed_widget_cb(widget, option)

    @classmethod
    def font_changed_cb(cls, font_button, option):
        cls.changed_widget_cb(font_button, option)

    @classmethod
    def color_changed_cb(cls, font_button, option):
        cls.changed_widget_cb(font_button, option)

    @classmethod
    def set_ui_widget(cls, option: Option, page_box):
        packed = False
        enclosing = None
        value = None
        type = option.get_type()
        if type is None or type == "internal":
            return
        raw_name = option.get_name()
        if raw_name and len(raw_name):
            name = raw_name
        else:
            name = None
        raw_documentation = option.get_documentation()
        if raw_documentation and len(raw_documentation):
            documentation = raw_documentation
        else:
            documentation = None
        option_def = cls.optionTable.get(type)
        if option_def is not None and option_def.set_widget is not None:
            value, enclosing, packed = option_def.set_widget(option, page_box, name, documentation)
        if not packed and enclosing is not None:
            eventbox = Gtk.EventBox.new()
            eventbox.add(enclosing)
            if type == "text":
                page_box.pack_start(eventbox, True, True, 0)
            else:
                page_box.pack_start(eventbox, False, False, 0)
            eventbox.set_tooltip_text(documentation)
        if value is not None:
            value.set_tooltip_text(documentation)

    @classmethod
    def add_option(cls, page, option):
        cls.set_ui_widget(option, page)

    def append_page(self, name):
        if name is None:
            return -1
        if name[:2] == "__":
            return -1
        advanced = name[:2] == "_+"
        name_offset = 2 if advanced else 0
        page_label = Gtk.Label.new(name[name_offset:])
        page_label.show()
        page_content_box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 2)
        page_content_box.set_homogeneous(False)
        page_content_box.set_border_width(12)
        options_scrolled_win = Gtk.ScrolledWindow.new()
        page_content_box.pack_start(options_scrolled_win, True, True, 0)
        options_box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 5)
        options_box.set_homogeneous(False)
        options_box.set_border_width(0)
        options_scrolled_win.add(options_box)
        options_scrolled_win.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        for option in self.option_db.section_options(name):
            self.add_option(options_box, option)
        buttonbox = Gtk.ButtonBox.new(Gtk.Orientation.HORIZONTAL)
        buttonbox.set_layout(Gtk.ButtonBoxStyle.EDGE)
        buttonbox.set_border_width(5)
        page_content_box.pack_end(buttonbox, False, False, 0)
        reset_button = Gtk.Button.new_with_label("Reset defaults")
        reset_button.set_tooltip_text("Reset all values to their defaults.")
        reset_button.connect("clicked", self.reset_cb, name)
        buttonbox.pack_end(reset_button, False, False, 0)
        page_content_box.show_all()
        self.notebook.append_page(page_content_box, page_label)
        page_count = self.notebook.page_num(page_content_box)
        if self.page_list_view:
            view = self.page_list_view
            lis = view.get_model()
            lis.append([page_count, name])

            if page_count > MAX_TAB_COUNT - 1:
                self.page_list.show()
                self.notebook.set_show_tabs(False)
                self.notebook.set_show_border(False)
            else:
                self.page_list.hide()
            if advanced:
                notebook_page = self.notebook.get_nth_page(page_count)
                self.user_data[notebook_page]["advanced"] = advanced
        return page_count

    def build_contents(self, odb):
        self.build_contents_full(odb, True)

    def build_contents_full(self, odb: OptionDB, show_dialog):
        default_page = -1
        if odb is None:
            return
        odb.set_ui_callbacks(self.get_ui_value_internal, self.set_ui_value_internal, self.set_selectable_internal)
        self.option_db = odb
        default_section_name = odb.get_default_section()
        for section in sorted(odb.sections()):
            page = self.append_page(section)
            if section == default_section_name:
                default_page = page

        for section in odb.sections():
            for option in odb.section_options(section):
                option.widget_changed()
        self.notebook.popup_enable()
        if default_page >= 0:
            selection = self.page_list_view.get_selection()
            model = self.page_list_view.get_model()
            _iter = model.iter_nth_child(None, default_page)
            selection.select_iter(_iter)
            self.notebook.set_current_page(default_page)
        self.changed_internal(self.window, False)
        if show_dialog:
            self.window.show()

    @classmethod
    def set_ui_value_internal(cls, option: Option, use_default):
        widget = option.get_widget()
        if widget is None:
            return
        _type = option.get_type()
        if use_default:
            value = option.get_default()
        else:
            value = option.get_value()
        option_def = cls.optionTable.get(_type)
        if option_def is not None and option_def.set_value is not None:
            option_def.set_value(option, use_default, widget, value)

    @classmethod
    def get_ui_value_internal(cls, option: Option):
        widget = option.get_widget()
        if widget is None:
            logging.debug("Option has no widget")
            return
        _type = option.get_type()
        option_def = cls.optionTable.get(_type)
        if option_def is not None and option_def.get_value is not None:
            return option_def.get_value(option, widget)
        logging.debug("Option not found in table %s" % _type)

    @staticmethod
    def set_selectable_internal(option: Option, selectable):
        widget = option.get_widget()
        if widget is None:
            return
        widget.set_sensitive(selectable)

    @classmethod
    def default_cb(cls, widget, option: Option):
        option.set_ui_value(True)
        option.set_changed(True)
        cls.changed_internal(widget, True)

    @classmethod
    def show_hidden_toggled_cb(cls, widget, option):
        tree_view = option.get_widget()
        avi = tree_view.get_view_info()
        avi.show_hidden = widget.get_active()
        tree_view.set_view_info(avi)
        cls.changed_widget_cb(widget, option)

    @classmethod
    def multichoice_cb(cls, widget, option, *_):
        cls.changed_widget_cb(widget, option)

    @classmethod
    def radiobutton_cb(cls, w, option):
        widget = option.get_widget()
        current = cls.user_data[widget].get("radiobutton_index")
        new_value = cls.user_data[w].get("radiobutton_index")
        if current == new_value:
            return
        cls.user_data[widget]["radiobutton_index"] = new_value
        cls.changed_widget_cb(widget, option)

    @classmethod
    def get_ui_value_boolean(cls, _, widget):
        active = widget.get_active()
        return active

    @classmethod
    def get_ui_value_string(cls, _, widget):
        return widget.get_text()

    @classmethod
    def get_ui_value_text(cls, _, widget):
        buff = widget.get_buffer()
        start = buff.get_start_iter()
        end = buff.get_end_iter()
        return buff.get_text(start, end, True)

    @classmethod
    def get_ui_value_currency(cls, _, widget):
        return widget.get_currency()

    @classmethod
    def get_ui_value_commodity(cls, _, widget):
        return widget.get_selected()

    @classmethod
    def get_ui_value_multichoice(cls, option, widget):
        index = widget.get_active()
        return option.index_get_value(index)

    @classmethod
    def get_ui_value_date(cls, option: DateOption, widget):
        result = None
        subtype = option.get_subtype()
        if subtype == "relative":
            index = widget.get_active()
            val = option.index_get_value(index)
            result = ["relative", val]
        elif subtype == "absolute":
            t = widget.get_date()
            result = ["absolute", t]
        elif subtype == "both":
            widget_list = widget.get_children()
            ab_button = widget_list[RdPositions.AB_BUTTON_POS]
            ab_widget = widget_list[RdPositions.AB_WIDGET_POS]
            rel_widget = widget_list[RdPositions.REL_WIDGET_POS]
            if ab_button.get_active():
                t = ab_widget.get_date()
                result = ["absolute", t]
            else:
                index = rel_widget.get_active()
                val = option.index_get_value(index)
                result = ["relative", val]
        return result

    @classmethod
    def get_ui_value_account_list(cls, _, widget):
        return widget.get_selected_accounts()

    @classmethod
    def get_ui_value_account_sel(cls, _, widget):
        return widget.get_account()

    @classmethod
    def get_ui_value_reference_sel(cls, _, widget):
        return widget.get_ref()

    @classmethod
    def get_ui_value_list(cls, option: Option, widget):
        selection = widget.get_selection()
        num_rows = option.num_values()
        result = []
        for row in range(num_rows):
            path = widget.get_selection()
            selected = selection.path_is_selected(path)
            if selected:
                result.append(option.index_get_value(row))
        return result

    @classmethod
    def get_ui_value_number_range(cls, _, widget):
        return widget.get_value()

    @classmethod
    def get_ui_value_color(cls, option: ColorOption, widget: Gtk.ColorButton):
        color = widget.get_rgba()
        scale = option.range()
        return [color.red * scale, color.green * scale, color.blue * scale, color.alpha * scale]

    @classmethod
    def get_ui_value_font(cls, _, widget: Gtk.FontButton):
        return widget.get_font_name() or ""

    @classmethod
    def get_ui_value_pixmap(cls, option, widget: Gtk.FileChooserDialog):
        return widget.get_filename() or ""

    @classmethod
    def get_ui_value_radiobutton(cls, option: Option, _):
        index = 0
        return option.index_get_value(index)

    @classmethod
    def get_ui_value_dateformat(cls, option: DateFormatOption, widget):
        fo = date_dateformat_to_string(widget.get_format())
        months = date_monthformat_to_string(widget.get_months())
        years = widget.get_years()
        custom = widget.get_custom()
        return [fo, months, bool(years), custom if custom else False]

    @classmethod
    def get_ui_value_plot_size(cls, option: Option, widget):
        widget_list = widget.get_children()
        px_button = widget_list[0]
        px_widget = widget_list[1]
        p_widget = widget_list[3]
        if px_button.get_active():
            return ["pixels", px_widget.get_value()]
        else:
            return ["percent", p_widget.get_value()]

    @classmethod
    def get_ui_value_currency_accounting(cls, option: Option, widget):
        pass

    @classmethod
    def set_ui_value_boolean(cls, option, use_default, widget, value):
        if isinstance(value, bool):
            widget.set_active(value)
            return False
        return True

    @classmethod
    def set_ui_value_string(cls, option, use_default, widget, value):
        if isinstance(value, str):
            widget.set_text(value)
            return False
        return True

    @classmethod
    def set_ui_value_text(cls, option, use_default, widget, value):
        if isinstance(widget, Gtk.TextBuffer):
            buffer = widget
        else:
            buffer = widget.get_buffer()
        if isinstance(value, str):
            buffer.set_text(value)
            return False
        return True

    @classmethod
    def set_ui_value_currency(cls, option, use_default, widget, value):
        if value:
            widget.set_currency(value)
            return False
        return True

    @classmethod
    def set_ui_value_commodity(cls, option, use_default, widget, value):
        if value:
            widget.set_selected(value)
            return False
        return True

    @classmethod
    def set_ui_value_multichoice(cls, option: Option, use_default, widget, value):
        index = option.value_get_index(value)
        if index < 0:
            return True
        else:
            widget.set_active(index)
            return False

    @classmethod
    def date_option_set_select_method(cls, option, use_absolute, set_buttons):
        widget = option.get_widget()
        widget_list = widget.get_children()
        ab_button = widget_list[RdPositions.AB_BUTTON_POS]
        ab_widget = widget_list[RdPositions.AB_WIDGET_POS]
        rel_button = widget_list[RdPositions.REL_BUTTON_POS]
        rel_widget = widget_list[RdPositions.REL_WIDGET_POS]
        if use_absolute:
            ab_widget.set_sensitive(True)
            rel_widget.set_sensitive(False)
            if set_buttons:
                ab_button.set_active(True)
        else:
            ab_widget.set_sensitive(False)
            rel_widget.set_sensitive(True)
            if set_buttons:
                rel_button.set_active(True)

    @classmethod
    def rd_option_ab_set_cb(cls, widget, option):
        cls.date_option_set_select_method(option, True, False)
        cls.changed_widget_cb(option.get_widget(), option)

    @classmethod
    def rd_option_rel_set_cb(cls, widget, option):
        cls.date_option_set_select_method(option, False, False)
        cls.changed_widget_cb(option.get_widget(), option)

    @classmethod
    def image_option_update_preview_cb(cls, chooser, option):
        if chooser is None:
            return
        filename = chooser.get_preview_filename()
        if filename is None:
            filename = cls.user_data[chooser].get(LAST_SELECTION)
            if filename is None:
                return
        image = chooser.get_preview_widget()
        try:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(filename, 128, 128)
        except GLib.GError:
            pixbuf = None
        have_preview = pixbuf is not None
        image.set_from_pixbuf(pixbuf)
        chooser.set_preview_widget_active(have_preview)

    @classmethod
    def image_option_selection_changed_cb(cls, chooser, option):
        filename = chooser.get_preview_filename()
        if filename is None:
            return
        cls.user_data[chooser][LAST_SELECTION] = filename

    @classmethod
    def set_ui_value_date(cls, option: DateOption, use_default, widget, value):
        subtype = option.get_subtype()
        bad_value = False
        if isinstance(value, (list, tuple)) and len(value) == 2:
            vtype = DateOption.value_get_type(value)
            if vtype == "relative":
                relative = DateOption.relative_time(value)
                index = option.value_get_index(relative)
                if subtype == "relative":
                    widget.set_active(index)
                elif subtype == "both":
                    widget_list = widget.get_children()
                    rel_date_widget = widget_list[RdPositions.REL_WIDGET_POS]
                    cls.date_option_set_select_method(option, False, True)
                    rel_date_widget.set_active(index)
                else:
                    bad_value = True
            elif vtype == "absolute":
                t = DateOption.absolute_time(value)
                if subtype == "absolute":
                    widget.set_time(t)
                elif subtype == "both":
                    widget_list = widget.get_children()
                    ab_widget = widget_list[RdPositions.AB_WIDGET_POS]
                    cls.date_option_set_select_method(option, True, True)
                    ab_widget.set_time(t)
                else:
                    bad_value = True
            else:
                bad_value = True
        else:
            bad_value = False
        return bad_value

    @classmethod
    def set_ui_value_account_list(cls, option, use_default, widget, value):
        widget.set_selected_accounts(value, True)
        return False

    @classmethod
    def set_ui_value_account_sel(cls, option, use_default, widget, value):
        widget.set_account(value, False)
        return False

    @classmethod
    def set_ui_value_reference_sel(cls, option, use_default, widget: ReferenceSelection, value):
        widget.set_ref(value, False)
        return False

    @classmethod
    def set_ui_value_list(cls, option, use_default, widget, value):
        selection = widget.get_selection()
        selection.unselect_all()
        if isinstance(value, list):
            item, value = value
            row = option.value_get_index(item)
            if row < 0:
                return True
            path = Gtk.TreePath.new_from_indices([row, -1])
            selection.select_path(path)
        else:
            return True
        return False

    @classmethod
    def set_ui_value_number_range(cls, option, use_default, widget, value):
        if isinstance(value, (int, float)):
            widget.set_value(value)
            return False
        return True

    @classmethod
    def set_ui_value_color(cls, option, use_default, widget, value):
        color = Gdk.RGBA()
        if OptionUtils.get_color_info(option, use_default, color):
            widget.set_rgba(color)
            return False
        return True

    @classmethod
    def set_ui_value_font(cls, option, use_default, widget, value):
        if isinstance(value, str):
            if len(value) > 0:
                widget.set_font_name(value)
                return False
        return True

    @classmethod
    def set_ui_value_pixmap(cls, option, use_default, widget, value):
        if isinstance(value, str):
            if len(value) > 0:
                widget.select_filename(value)
                cls.user_data[widget][LAST_SELECTION] = value
            return False
        return True

    @classmethod
    def set_ui_value_radiobutton(cls, option: Option, use_default, widget, value):
        index = option.value_get_index(value)
        if index < 0:
            return True
        else:
            lis = widget.get_children()
            box = lis[0]
            lis = box.get_children()
            button = lis[index]
            val = cls.user_data[button]["radiobutton_index"]
            if val != value:
                return True
            button.set_active(True)
        return False

    @classmethod
    def set_ui_value_dateformat(cls, option, use_default, widget, value):
        parsed = OptionUtils.dateformat_parse(value)
        if isinstance(parsed, bool) and parsed:
            return True
        fmt, months, years, custom = parsed
        widget.set_format(fmt)
        widget.set_months(months)
        widget.set_years(years)
        widget.set_custom(custom)
        widget.refresh()
        return False

    @classmethod
    def set_ui_value_plot_size(cls, option, use_default, widget, value):
        widget_list = widget.get_children()
        px_button = widget_list[0]
        px_widget = widget_list[1]
        p_button = widget_list[2]
        p_widget = widget_list[3]
        if isinstance(value, (list, tuple)):
            try:
                symbol_str = value[0]
                d_value = value[1]
                if symbol_str == "pixels":
                    px_widget.set_value(d_value)
                    px_button.set_active(True)
                else:
                    p_widget.set_value(d_value)
                    p_button.set_active(True)
                return False
            except IndexError:
                return True

    @classmethod
    def set_ui_value_currency_accounting(cls, option, use_default, widget, value):
        pass

    @classmethod
    def set_ui_widget_boolean(cls, option, page_box, name, documentation):
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        value = Gtk.CheckButton.new_with_label(name)
        option.set_widget(value)
        option.set_ui_value(False)
        value.connect("toggled", cls.changed_option_cb, option)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_string(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        value = Gtk.Entry()
        option.set_widget(value)
        option.set_ui_value(False)
        value.connect("changed", cls.changed_option_cb, option)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, True, True, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_text(cls, option, page_box, name, documentation):
        frame = Gtk.Frame.new(name)
        scroll = Gtk.ScrolledWindow.new(None, None)
        scroll.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        scroll.set_border_width(2)
        frame.add(scroll)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 10)
        enclosing.set_homogeneous(False)
        value = Gtk.TextView.new()
        value.set_wrap_mode(Gtk.WrapMode.WORD)
        value.set_editable(True)
        value.set_accepts_tab(False)
        scroll.add(value)
        option.set_widget(value)
        option.set_ui_value(False)
        text_buffer = value.get_buffer()
        text_buffer.connect("changed", cls.changed_option_cb, option)
        enclosing.pack_start(frame, True, True, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_currency(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        value = CurrencySelection()
        option.set_widget(value)
        option.set_ui_value(False)
        value.connect("changed", cls.changed_option_cb, option)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_commodity(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        value = GeneralSelect(GeneralSelectType.SELECT, commodity_edit_get_string, commodity_edit_new_select)
        option.set_widget(value)
        option.set_ui_value(False)
        if documentation is not None:
            value.entry.set_tooltip_text(documentation)
        value.entry.connect("changed", cls.changed_option_cb, option)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_multichoice(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        value = cls.create_multichoice_widget(option)
        option.set_widget(value)
        option.set_ui_value(False)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_date(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        value = cls.create_date_widget(option)
        option.set_widget(value)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, False, False, 0)
        eventbox = Gtk.EventBox.new()
        eventbox.add(enclosing)
        page_box.pack_start(eventbox, False, False, 5)
        eventbox.set_tooltip_text(documentation)
        option.set_ui_value(False)
        enclosing.show_all()
        return value, enclosing, True

    @classmethod
    def set_ui_widget_account_list(cls, option, page_box, name, documentation):
        enclosing = cls.create_account_widget(option, name)
        value = option.get_widget()
        enclosing.set_tooltip_text(documentation)
        page_box.pack_start(enclosing, True, True, 5)
        option.set_ui_value(False)
        selection = value.get_selection()
        selection.connect("changed", cls.account_cb, option)
        enclosing.show_all()
        return value, enclosing, True

    @classmethod
    def set_ui_widget_account_sel(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        acct_type_list = option.get_account_type_list()
        value = AccountSelection()
        value.set_account_filters(acct_type_list, None)
        value.connect("selection_changed", lambda acc, widget: cls.changed_option_cb(widget, option))
        option.set_widget(value)
        option.set_ui_value(False)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_reference_sel(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        e_type = option.get_entity_type()
        new_cb = option.get_new_cb()
        depends = option.get_depends_on()
        depends_callback = option.get_depends_cb()
        value = ReferenceSelection(e_type, new_cb, depends, depends_callback)
        value.connect("selection_changed", lambda acc, widget: cls.changed_option_cb(widget, option))
        option.set_widget(value)
        option.set_ui_value(False)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_list(cls, option, page_box, name, documentation):
        enclosing = cls.create_list_widget(option, name)
        value = option.get_widget()
        eventbox = Gtk.EventBox.new()
        eventbox.add(enclosing)
        page_box.pack_start(eventbox, False, False, 5)
        eventbox.set_tooltip_text(documentation)
        option.set_ui_value(False)
        enclosing.show()
        return value, enclosing, True

    @classmethod
    def set_ui_widget_number_range(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        lower_bound, upper_bound, num_decimals, step_size = option.get_range_info()
        adj = Gtk.Adjustment.new(lower_bound, lower_bound, upper_bound, step_size, step_size * 5.0, 0)
        value = Gtk.SpinButton.new(adj, step_size, num_decimals)
        value.set_numeric(True)
        biggest = abs(lower_bound)
        biggest = max(biggest, abs(upper_bound))
        num_digits = 0
        while biggest >= 1:
            num_digits += 1
            biggest = biggest / 10
        if num_digits == 0:
            num_digits = 1
        num_digits += num_decimals
        value.set_width_chars(num_digits)
        option.set_widget(value)
        option.set_ui_value(False)
        value.connect("changed", cls.changed_option_cb, option)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_color(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        use_alpha = option.use_alpha()
        value = Gtk.ColorButton()
        value.set_title(name)
        value.set_use_alpha(use_alpha)
        option.set_widget(value)
        option.set_ui_value(False)
        value.connect("color-set", cls.color_changed_cb, option)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_font(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        value = Gtk.FontButton()
        value.set_properties(use_font=True, show_style=True, show_size=True)
        option.set_widget(value)
        option.set_ui_value(False)
        value.connect("font-set", cls.font_changed_cb, option)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_pixmap(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        button = Gtk.Button.new_with_label("Clear")
        button.set_tooltip_text("Clear any selected image file.")
        value = Gtk.FileChooserButton.new("Select image", Gtk.FileChooserAction.OPEN)
        value.set_tooltip_text("Select an image file.")
        value.set_properties(width_chars=30, preview_widget=Gtk.Image.new())
        value.connect("selection-changed", cls.changed_widget_cb, option)
        value.connect("selection-changed", cls.image_option_selection_changed_cb, option)
        value.connect("update-preview", cls.image_option_update_preview_cb, option)
        button.connect("clicked", lambda b: value.unselect_all())
        option.set_widget(value)
        option.set_ui_value(False)
        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_end(button, False, False, 0)
        enclosing.pack_end(value, False, False, 0)
        value.show()
        label.show()
        enclosing.show()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_radiobutton(cls, option, page_box, name, documentation):
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        value = cls.create_radiobutton_widget(name, option)
        option.set_widget(value)
        option.set_ui_value(False)
        enclosing.pack_start(value, False, False, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def set_ui_widget_dateformat(cls, option, page_box, name, documentation):
        enclosing = DateFormatSelection.new_with_label(name)
        option.set_widget(enclosing)
        option.set_ui_value(False)
        enclosing.connect("format_changed", cls.changed_option_cb, option)
        enclosing.show_all()
        return enclosing, enclosing, False

    @classmethod
    def plot_size_option_set_select_method(cls, option, set_buttons):
        widget = option.get_widget()
        widget_list = widget.get_children()
        px_widget = widget_list[1]
        p_widget = widget_list[3]
        if set_buttons:
            px_widget.set_sensitive(True)
            p_widget.set_sensitive(False)
        else:
            p_widget.set_sensitive(True)
            px_widget.set_sensitive(False)

    @classmethod
    def rd_option_px_set_cb(cls, widget, option):
        cls.plot_size_option_set_select_method(option, True)
        cls.changed_widget_cb(widget, option)

    @classmethod
    def rd_option_p_set_cb(cls, widget, option):
        cls.plot_size_option_set_select_method(option, False)
        cls.changed_widget_cb(widget, option)
        return

    @classmethod
    def set_ui_widget_plot_size(cls, option, page_box, name, documentation):
        label = Gtk.Label(name)
        gs_label_set_alignment(label, 1.0, 0.5)
        hbox = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        hbox.set_homogeneous(False)

        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)

        enclosing.pack_start(label, False, False, 0)
        enclosing.pack_start(hbox, False, False, 0)

        lower_bound, upper_bound, num_decimals, step_size = option.get_range_info()
        adj_px = Gtk.Adjustment.new(lower_bound, lower_bound, upper_bound, step_size, step_size * 5.0, 0)
        value_px = Gtk.SpinButton.new(adj_px, step_size, num_decimals)
        value_px.set_numeric(True)
        biggest = abs(lower_bound)
        biggest = max(biggest, abs(upper_bound))
        num_digits = 0
        while biggest >= 1:
            num_digits += 1
            biggest = biggest / 10
        if num_digits == 0:
            num_digits = 1
        num_digits += num_decimals
        value_px.set_width_chars(num_digits)
        value_px.set_value(upper_bound / 2)
        value_px.connect("changed", cls.changed_option_cb, option)

        adj_percent = Gtk.Adjustment.new(1, 10, 100, 1, 5.0, 0)
        value_percent = Gtk.SpinButton.new(adj_percent, 1, 0)
        value_percent.set_numeric(True)
        value_percent.set_value(100)
        value_percent.set_width_chars(3)
        value_percent.set_sensitive(False)
        value_percent.connect("changed", cls.changed_option_cb, option)
        px_butt = Gtk.RadioButton.new_with_label(None, "Pixels")
        px_butt.set_active(True)
        px_butt.connect("toggled", cls.rd_option_px_set_cb, option)
        p_butt = Gtk.RadioButton.new_with_label_from_widget(px_butt, "Percent")
        p_butt.connect("toggled", cls.rd_option_p_set_cb, option)
        hbox.pack_start(px_butt, False, False, 0)
        hbox.pack_start(value_px, False, False, 0)
        hbox.pack_start(p_butt, False, False, 0)
        hbox.pack_start(value_percent, False, False, 0)
        option.set_widget(hbox)
        option.set_ui_value(False)
        enclosing.show_all()
        return hbox, enclosing, False

    @classmethod
    def set_ui_widget_currency_accounting(cls, option, page_box, name, documentation):
        enclosing = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        enclosing.set_homogeneous(False)
        value = cls.create_currency_accounting_widget(name, option)
        option.set_widget(value)
        option.set_ui_value(False)
        enclosing.pack_start(value, True, True, 0)
        enclosing.show_all()
        return value, enclosing, False

    @classmethod
    def create_date_widget(cls, option: DateOption):
        ab_widget = None
        rel_widget = None
        type = option.get_subtype()
        show_time = option.get_show_time()
        use24 = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_CLOCK_24H)

        if type != "relative":
            ab_widget = DateSelection()
            entry = ab_widget.date_entry
            entry.connect("changed", cls.changed_option_cb, option)

        if type != "absolute":
            num_values = option.num_values()
            if num_values <= 0:
                return None
            store = Gtk.ListStore(str, str)
            for i in range(num_values):
                itemstring = option.index_get_name(i)
                description = option.index_get_description(i)
                store.append([itemstring, description])

            rel_widget = ComboTT()
            rel_widget.set_model(store)
            rel_widget.connect("changed", cls.multichoice_cb, option)
        if type == "absolute":
            option.set_widget(ab_widget)
            return ab_widget
        elif type == "relative":
            option.set_widget(rel_widget)
            return rel_widget
        elif type == "both":
            box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
            box.set_homogeneous(False)
            ab_button = Gtk.RadioButton()
            ab_button.connect("toggled", cls.rd_option_ab_set_cb, option)
            rel_button = Gtk.RadioButton.new_from_widget(ab_button)
            rel_button.connect("toggled", cls.rd_option_rel_set_cb, option)
            box.pack_start(ab_button, False, False, 0)
            box.pack_start(ab_widget, False, False, 0)
            box.pack_start(rel_button, False, False, 0)
            box.pack_start(rel_widget, False, False, 0)
            option.set_widget(box)
            return box
        else:
            return None

    @classmethod
    def create_multichoice_widget(cls, option: MultiChoiceOption):
        num_values = option.num_values()
        store = Gtk.ListStore(str, str)
        for i in range(num_values):
            itemstring = option.index_get_name(i)
            description = option.index_get_description(i)
            store.append([itemstring, description])
        widget = ComboTT()
        widget.set_model(store)
        widget.connect("changed", cls.multichoice_cb, option)
        return widget

    @classmethod
    def create_radiobutton_widget(cls, name, option: Option):
        num_values = option.num_values()
        widget = None
        frame = Gtk.Frame.new(name)
        box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        box.set_homogeneous(False)
        frame.add(box)
        for i in range(num_values):
            label = option.index_get_name(i)
            tip = option.index_get_description(i)
            widget = Gtk.RadioButton.new_with_label_from_widget(widget, label)
            cls.user_data[widget]["radiobutton_index"] = i
            widget.set_tooltip_text(tip)
            widget.connect("toggled", cls.radiobutton_cb, option)
            box.pack_start(widget, False, False, 0)
        return frame

    @classmethod
    def initialise(cls):
        class OptionDef:
            def __init__(self, type, set_w, set_v, get_v):
                self.set_widget = set_w
                self.type = type
                self.set_value = set_v
                self.get_value = get_v

        options = [
            OptionDef(
                "boolean", cls.set_ui_widget_boolean,
                cls.set_ui_value_boolean, cls.get_ui_value_boolean
            ),
            OptionDef(
                "string", cls.set_ui_widget_string,
                cls.set_ui_value_string, cls.get_ui_value_string
            ),
            OptionDef(
                "text", cls.set_ui_widget_text,
                cls.set_ui_value_text,
                cls.get_ui_value_text
            ),
            OptionDef(
                "currency", cls.set_ui_widget_currency,
                cls.set_ui_value_currency, cls.get_ui_value_currency
            ),
            OptionDef(
                "commodity", cls.set_ui_widget_commodity,
                cls.set_ui_value_commodity, cls.get_ui_value_commodity
            ),
            OptionDef(
                "multichoice", cls.set_ui_widget_multichoice,
                cls.set_ui_value_multichoice, cls.get_ui_value_multichoice
            ),
            OptionDef(
                "date", cls.set_ui_widget_date,
                cls.set_ui_value_date, cls.get_ui_value_date
            ),
            OptionDef(
                "account-list", cls.set_ui_widget_account_list,
                cls.set_ui_value_account_list, cls.get_ui_value_account_list
            ),
            OptionDef(
                "account-sel", cls.set_ui_widget_account_sel,
                cls.set_ui_value_account_sel, cls.get_ui_value_account_sel
            ),
            OptionDef(
                "reference-sel", cls.set_ui_widget_reference_sel,
                cls.set_ui_value_reference_sel, cls.get_ui_value_reference_sel
            ),
            OptionDef(
                "list", cls.set_ui_widget_list,
                cls.set_ui_value_list, cls.get_ui_value_list
            ),
            OptionDef(
                "number-range", cls.set_ui_widget_number_range,
                cls.set_ui_value_number_range, cls.get_ui_value_number_range
            ),
            OptionDef(
                "color", cls.set_ui_widget_color,
                cls.set_ui_value_color, cls.get_ui_value_color
            ),
            OptionDef(
                "font", cls.set_ui_widget_font,
                cls.set_ui_value_font, cls.get_ui_value_font
            ),
            OptionDef(
                "pixmap", cls.set_ui_widget_pixmap,
                cls.set_ui_value_pixmap, cls.get_ui_value_pixmap
            ),
            OptionDef(
                "radiobutton", cls.set_ui_widget_radiobutton,
                cls.set_ui_value_radiobutton, cls.get_ui_value_radiobutton
            ),
            OptionDef(
                "dateformat", cls.set_ui_widget_dateformat,
                cls.set_ui_value_dateformat, cls.get_ui_value_dateformat
            ),
            OptionDef(
                "plot-size", cls.set_ui_widget_plot_size,
                cls.set_ui_value_plot_size, cls.get_ui_value_plot_size
            ),
            OptionDef(
                "currency-accounting",
                cls.set_ui_widget_currency_accounting,
                cls.set_ui_value_currency_accounting,
                cls.get_ui_value_currency_accounting
            )]
        for op in options:
            cls.optionTable[op.type] = op

    def close_handler(self):
        gs_save_window_size(PREFS_GROUP, self.window)
        self.cancel_button_cb(None)

    def refresh_handler(self, changes):
        pass

    @classmethod
    def new(cls, title, parent):
        return cls.new_modal(False, title, None, parent)

    @classmethod
    def new_modal(cls, modal, title, component_class, parent):
        retval = cls()
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/options.ui", ["options_window"])
        retval.window = builder.get_object("options_window")
        retval.page_list = builder.get_object("page_list_scroll")
        gs_widget_set_style_context(retval.window, "OptionsDialog")
        retval.page_list_view = builder.get_object("page_list_treeview")
        view = retval.page_list_view
        store = Gtk.ListStore(int, str)
        view.set_model(store)
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Page", renderer, text=1)
        view.append_column(column)
        column.set_alignment(0.5)
        selection = view.get_selection()
        selection.set_mode(Gtk.SelectionMode.BROWSE)
        selection.connect("changed", retval.list_select_cb)
        button = builder.get_object("helpbutton")
        button.connect("clicked", retval.help_button_cb)
        button = builder.get_object("cancelbutton")
        button.connect("clicked", retval.cancel_button_cb)
        button = builder.get_object("applybutton")
        button.connect("clicked", retval.apply_button_cb)
        button = builder.get_object("okbutton")
        button.connect("clicked", retval.ok_button_cb)
        builder.connect_signals(retval)
        gs_restore_window_size(PREFS_GROUP, retval.window, parent)
        if title:
            retval.window.set_title(title)
        if modal:
            apply_button = builder.get_object("applybutton")
            apply_button.hide()

        hbox = builder.get_object("notebook_placeholder")
        retval.notebook = Gtk.Notebook()
        retval.notebook.set_vexpand(True)
        retval.notebook.show()
        hbox.pack_start(retval.notebook, True, True, 5)
        retval.component_class = component_class if component_class is not None else DIALOG_OPTIONS_CM_CLASS
        retval.component_id = ComponentManager.register(retval.component_class, retval.refresh_handler,
                                                        retval.close_handler)
        ComponentManager.set_session(retval.component_id, Session.get_current_session())
        if retval.component_class == DIALOG_BOOK_OPTIONS_CM_CLASS:
            ComponentManager.watch_entity_type(retval.component_id,
                                               ID_ACCOUNT,
                                               EVENT_MODIFY | EVENT_DESTROY)
        retval.window.connect("destroy", retval.destroy_cb)
        retval.window.connect("key_press_event", retval.window_key_press_cb)
        retval.destroyed = False
        return retval

    @classmethod
    def new_w_dialog(cls, title, window):
        retval = cls()
        cls.window = window
        return retval

    def set_apply_cb(self, cb, *data):
        self.apply_cb = cb
        self.apply_cb_data = data

    def set_help_cb(self, cb, *data):
        self.help_cb = cb
        self.help_cb_data = data

    def set_close_cb(self, cb, *data):
        self.close_cb = cb
        self.close_cb_data = data

    def destroy(self):
        ComponentManager.unregister(self.component_id)
        self.destroyed = True
        self.window.destroy()

        self.window = None
        self.notebook = None
        self.apply_cb = None
        self.help_cb = None
        self.component_class = None

    def help_button_cb(self, widget):
        if self.help_cb is not None:
            self.help_cb(self, *self.help_cb_data)

    def cancel_button_cb(self, widget):
        gs_save_window_size(PREFS_GROUP, self.window)
        if self.close_cb is not None:
            self.close_cb(self, *self.close_cb_data)
        else:
            self.window.hide()

    def apply_button_cb(self, _):
        close_cb = self.close_cb
        self.close_cb = None
        if self.apply_cb:
            self.apply_cb(self, *self.apply_cb_data)
        self.close_cb = close_cb
        gs_save_window_size(PREFS_GROUP, self.window)
        self.changed_internal(self.window, False)

    def ok_button_cb(self, _):
        close_cb = self.close_cb
        self.close_cb = None
        if self.apply_cb:
            self.apply_cb(self, *self.apply_cb_data)
        self.close_cb = close_cb
        gs_save_window_size(PREFS_GROUP, self.window)
        if self.close_cb is not None:
            self.close_cb(self, *self.close_cb_data)
        else:
            self.window.hide()

    def destroy_cb(self, obj):
        if not self.destroyed:
            if self.close_cb:
                self.close_cb(self, *self.close_cb_data)

    def window_key_press_cb(self, widget, event):
        if event.keyval == Gdk.KEY_Escape:
            self.component_close_handler()
            return True
        else:
            return False

    def reset_cb(self, w, section):
        self.option_db.reset_section_widgets(section)
        self.changed_internal(self.window, True)

    def list_select_cb(self, selection):
        lis, _iter = selection.get_selected()
        if lis is None or _iter is None:
            return
        index = lis.get_value(_iter, 0)
        self.notebook.set_current_page(index)

    def component_close_handler(self):
        gs_save_window_size(PREFS_GROUP, self.window)
        self.cancel_button_cb(None)

    @classmethod
    def account_cb(cls, selection, option):
        tree_view = selection.get_tree_view()
        cls.changed_widget_cb(tree_view, option)

    @classmethod
    def account_select_all_cb(cls, widget, option):
        tree_view = option.get_widget()
        tree_view.expand_all()
        selection = tree_view.get_selection()
        selection.select_all()
        cls.changed_widget_cb(widget, option)

    @classmethod
    def account_clear_all_cb(cls, widget, option):
        tree_view = option.get_widget()
        selection = tree_view.get_selection()
        selection.unselect_all()
        cls.changed_widget_cb(widget, option)

    @classmethod
    def account_select_children_cb(cls, widget, option):
        tree_view = option.get_widget()
        acct_list = tree_view.get_selected_accounts()
        for acc in acct_list:
            tree_view.select_sub_accounts(acc)

    @classmethod
    def create_account_widget(cls, option: AccountSelection, name):
        multiple_selection = option.multiple_selection()
        acct_type_list = option.get_account_type_list()
        frame = Gtk.Frame.new(name)
        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        vbox.set_homogeneous(False)
        frame.add(vbox)
        tree = AccountView()
        tree.set_headers_visible(False)
        selection = tree.get_selection()
        if multiple_selection:
            selection.set_mode(Gtk.SelectionMode.MULTIPLE)
        else:
            selection.set_mode(Gtk.SelectionMode.BROWSE)
        avi = tree.get_view_info()
        if avi is not None:
            if any(acct_type_list):
                avi = tree.get_view_info()
                for i in range(AccountType.NUM_ACCOUNT_TYPES):
                    avi.include_type[i] = False
                    avi.show_hidden = False
                for _type in acct_type_list:
                    avi.include_type[_type] = True
            else:
                for i in range(AccountType.NUM_ACCOUNT_TYPES):
                    avi.include_type[i] = True
                    avi.show_hidden = False
            tree.set_view_info(avi)
        scroll_win = Gtk.ScrolledWindow.new()
        scroll_win.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

        vbox.pack_start(scroll_win, True, True, 0)
        scroll_win.set_border_width(5)
        scroll_win.add(tree)

        bbox = Gtk.ButtonBox.new(Gtk.Orientation.HORIZONTAL)
        bbox.set_layout(Gtk.ButtonBoxStyle.SPREAD)
        vbox.pack_start(bbox, False, False, 10)

        if multiple_selection:
            button = Gtk.Button.new_with_label("Select All")
            bbox.pack_start(button, False, False, 0)
            button.set_tooltip_text("Select all accounts.")

            button.connect("clicked", cls.account_select_all_cb, option)

            button = Gtk.Button.new_with_label("Clear All")
            bbox.pack_start(button, False, False, 0)
            button.set_tooltip_text("Clear the selection and unselect all accounts.")

            button.connect("clicked", cls.account_clear_all_cb, option)

            button = Gtk.Button.new_with_label("Select Children")
            bbox.pack_start(button, False, False, 0)
            button.set_tooltip_text("Select all descendents of selected account.")

            button.connect("clicked", cls.account_select_children_cb, option)
        button = Gtk.Button.new_with_label("Select Default")
        bbox.pack_start(button, False, False, 0)
        button.set_tooltip_text("Select the default account selection.")
        button.connect("clicked", cls.default_cb, option)
        if multiple_selection:
            bbox = Gtk.ButtonBox.new(Gtk.Orientation.HORIZONTAL)
            bbox.set_layout(Gtk.ButtonBoxStyle.START)
            vbox.pack_start(bbox, False, False, 0)
        button = Gtk.CheckButton.new_with_label("Show Hidden Accounts")
        bbox.pack_start(button, False, False, 0)
        button.set_tooltip_text("Show accounts that have been marked hidden.")
        button.set_active(False)
        button.connect("toggled", cls.show_hidden_toggled_cb, option)
        option.set_widget(tree)
        return frame

    @classmethod
    def list_changed_cb(cls, selection, option):
        view = selection.get_tree_view()
        cls.changed_widget_cb(view, option)

    @classmethod
    def list_select_all_cb(cls, widget, option):
        view = option.get_widget()
        selection = view.get_selection()
        selection.select_all()
        cls.changed_widget_cb(view, option)

    @classmethod
    def list_clear_all_cb(cls, widget, option):
        view = option.get_widget()
        selection = view.get_selection()
        selection.unselect_all()
        cls.changed_widget_cb(view, option)

    @classmethod
    def create_list_widget(cls, option: Option, name):
        frame = Gtk.Frame.new(name)
        hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        hbox.set_homogeneous(False)
        frame.add(hbox)
        store = Gtk.ListStore(str)
        view = Gtk.TreeView.new_with_model(store)
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("", renderer, text=0)
        view.append_column(column)
        view.set_headers_visible(False)
        num_values = option.num_values()
        for i in range(num_values):
            store.append([option.index_get_name(i)])
        hbox.pack_start(view, False, False, 0)

        selection = view.get_selection()
        selection.set_mode(Gtk.SelectionMode.MULTIPLE)
        selection.connect("changed", cls.list_changed_cb, option)

        bbox = Gtk.ButtonBox.new(Gtk.Orientation.VERTICAL)
        bbox.set_layout(Gtk.ButtonBoxStyle.SPREAD)
        hbox.pack_start(bbox, False, False, 10)

        button = Gtk.Button.new_with_label("Select All")
        bbox.pack_start(button, False, False, 0)
        button.set_tooltip_text("Select all entries.")

        button.connect("clicked", cls.list_select_all_cb, option)

        button = Gtk.Button.new_with_label("Clear All")
        bbox.pack_start(button, False, False, 0)
        button.set_tooltip_text("Clear the selection and unselect all entries.")

        button.connect("clicked", cls.list_clear_all_cb, option)

        button = Gtk.Button.new_with_label("Select Default")
        bbox.pack_start(button, False, False, 0)
        button.set_tooltip_text("Select the default selection.")
        button.connect("clicked", cls.default_cb, option)
        option.set_widget(view)
        return frame


OptionDialog.initialise()
