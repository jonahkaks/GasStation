from gasstation.utilities.dialog import *
from gasstation.views.selections.account import AccountSelection
from libgasstation import AccountType
from libgasstation.core._declbase import ID_PAYMENT_METHOD
from libgasstation.core.component import *
from libgasstation.core.payment import PaymentMethod
from libgasstation.core.session import Session
DIALOG_NEW_PAYMENT_METHOD_CM_CLASS = "dialog-new-payment-method"
DIALOG_EDIT_PAYMENT_METHOD_CM_CLASS = "dialog-edit-payment-method"
from gi.repository import GObject


class PaymentMethodDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/payment/method.ui')
class PaymentMethodDialog(Gtk.Dialog):
    __gtype_name__ = "PaymentMethodDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()
    account_box: Gtk.Box = Gtk.Template.Child()
    account_label: Gtk.Label = Gtk.Template.Child()
    is_credit_card_check: Gtk.CheckButton = Gtk.Template.Child()
    ok_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, payment_method=None, name=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if parent is not None:
            self.set_transient_for(parent)
        self.account_selection = AccountSelection()
        self.account_selection.set_account_filters([AccountType.BANK, AccountType.CASH])
        self.account_selection.set_new_account_ability(True)
        self.account_selection.connect("selection-changed", self.validate)
        self.account_box.add(self.account_selection)
        self.account_selection.show()

        self.dialog_type = PaymentMethodDialogType.NEW if payment_method is None else PaymentMethodDialogType.EDIT
        self.payment_method: PaymentMethod = payment_method
        self.created_payment_method: PaymentMethod = None
        self.set_title("Add/Edit PaymentMethod")
        self.get_style_context().add_class("PaymentMethodDialog")
        self.book = book
        if name is not None:
            self.name_entry.set_text(name)
        gs_window_adjust_for_screen(self)

        self.component_id = ComponentManager.register(DIALOG_NEW_PAYMENT_METHOD_CM_CLASS,
                                                      self.refresh_handler,
                                                      None if modal else self.close_handler)

        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id,
                                           ID_PAYMENT_METHOD,
                                           EVENT_MODIFY | EVENT_DESTROY)
        self.show()
        if payment_method is not None:
            self.to_ui()

    def to_ui(self):
        self.name_entry.set_text(self.payment_method.get_name())
        self.account_selection.set_account(self.payment_method.get_account())
        self.is_credit_card_check.set_active(self.payment_method.get_is_credit_card())

    def to_payment_method(self):
        if self.payment_method is None:
            self.payment_method = PaymentMethod(self.book)
        self.payment_method.begin_edit()
        self.payment_method.set_name(self.name_entry.get_text())
        self.payment_method.set_account(self.account_selection.get_account())
        self.payment_method.set_is_credit_card(self.is_credit_card_check.get_active())

    def finish_ok(self):
        self.to_payment_method()
        self.payment_method.commit_edit()
        self.created_payment_method = self.payment_method
        self.payment_method = None
        self.close()
        ComponentManager.close(self.component_id)

    @Gtk.Template.Callback()
    def validate(self, *_):
        sensitive = True
        name = self.name_entry.get_text()
        gs_widget_remove_style_context(self.name_entry, "error")
        if PaymentMethod.lookup_name(Session.get_current_book(), name) is not None:
            message = "The payment method %s already exists" % name
            self.name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning")
            gs_widget_set_style_context(self.name_entry, "error")
            self.name_entry.set_tooltip_text(message)
            sensitive = False
        else:
            self.name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "emblem-default")
        gs_widget_remove_style_context(self.account_label, "error")
        if self.account_selection.get_account() is None:
            self.account_selection.set_tooltip_text("Please select or add a payment account")
            gs_widget_set_style_context(self.account_label, "error")
            sensitive = False
        if sensitive:
            self.ok_button.set_image(Gtk.Image.new_from_icon_name("emblem-default", Gtk.IconSize.BUTTON))
            self.ok_button.set_tooltip_text("Press Okay to save customer")
        else:
            self.ok_button.set_image(Gtk.Image.new_from_icon_name("dialog-warning", Gtk.IconSize.BUTTON))
            self.ok_button.set_tooltip_text("Please fix the errors to continue")
        self.ok_button.set_sensitive(sensitive)
        return True

    @Gtk.Template.Callback()
    def response_cb(self, _, response):
        if response == Gtk.ResponseType.OK:
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            self.close()

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        inv = self.payment_method
        if inv is None:
            ComponentManager.close(self.component_id)
            return

        if changes:
            info = ComponentManager.get_entity_events(changes, inv.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(PaymentMethodDialog)
