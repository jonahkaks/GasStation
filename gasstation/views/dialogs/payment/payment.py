import datetime
from decimal import Decimal

from gi.repository import Gtk, GObject

from gasstation.utilities.custom_dialogs import show_info, show_error
from gasstation.utilities.date import print_datetime
from gasstation.utilities.dialog import gs_tree_view_get_grid_lines_pref
from gasstation.utilities.preferences import *
from gasstation.utilities.ui import sprintamount, PrintAmountInfo
from gasstation.views import TransferDialog, DateSelection, tree_view_column_set_default_width
from gasstation.views.dialogs.payment.payment_method import PaymentMethodDialog
from gasstation.views.selections import AmountEntry, ReferenceSelection
from libgasstation import Customer, Supplier, Transaction, ID_ACCOUNT, EVENT_CREATE, \
    EVENT_MODIFY, EVENT_DESTROY, TransactionType, Book, Invoice, Bill
from libgasstation.core.component import ComponentManager
from libgasstation.core.helpers import gs_get_action_num, gs_get_num_action
from libgasstation.core.payment import PaymentMethod

DIALOG_PAYMENT_CM_CLASS = "payment-dialog"


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/payment/payment.ui')
class PaymentDialog(Gtk.Dialog):
    __gtype_name__ = "PaymentDialog"
    person_label: Gtk.Label = Gtk.Template.Child()
    person_frame: Gtk.Frame = Gtk.Template.Child()
    email_entry: Gtk.Entry = Gtk.Template.Child()
    date_frame: Gtk.Frame = Gtk.Template.Child()
    total_label: Gtk.Label = Gtk.Template.Child()
    total_amount_label: Gtk.Label = Gtk.Template.Child()
    payment_method_frame: Gtk.Frame = Gtk.Template.Child()
    reference_entry: Gtk.Entry = Gtk.Template.Child()
    amount_frame: Gtk.Frame = Gtk.Template.Child()
    docs_list_tree_view: Gtk.TreeView = Gtk.Template.Child()
    notes_entry: Gtk.TextView = Gtk.Template.Child()

    def __init__(self, parent, book: Book, person=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.book = book
        self.transaction = None
        self.person = person
        self.set_transient_for(parent)
        self.set_name("PaymentDialog")
        self.notes_buffer: Gtk.TextBuffer = self.notes_entry.get_buffer()
        person_label = ""
        person_type = None
        if isinstance(person, Customer):
            person_type = Customer
            person_label = "Customer"
        elif isinstance(person, Supplier):
            person_label = "Supplier"
            person_type = Supplier

        self.amount = AmountEntry()
        self.amount.set_evaluate_on_enter(True)
        self.total_amount = Decimal(0)
        self.amount.connect("changed", self.amount_changed_cb)
        self.amount_frame.add(self.amount)
        self.amount.show()

        self.date_edit: DateSelection = DateSelection()
        self.date_frame.add(self.date_edit)
        self.date_edit.show()

        self.payment_method = ReferenceSelection(PaymentMethod, PaymentMethodDialog)
        self.payment_method.set_new_ability(True)
        self.payment_method_frame.add(self.payment_method)
        self.payment_method.show()

        self.person_label.set_text(person_label)
        self.person_selection = ReferenceSelection(person_type)
        self.person_selection.connect("selection_changed", self.person_changed_cb)
        self.person_selection.set_ref(self.person)
        self.person_frame.add(self.person_selection)
        self.person_selection.show()

        model = Gtk.ListStore(object, str, str, str, str, object)
        self.docs_list_tree_view.set_model(model)
        selection = self.docs_list_tree_view.get_selection()
        selection.set_mode(Gtk.SelectionMode.MULTIPLE)
        self.docs_list_tree_view.set_grid_lines(gs_tree_view_get_grid_lines_pref())

        renderer = Gtk.CellRendererText()
        column = self.docs_list_tree_view.get_column(0)
        column.pack_start(renderer, True)
        tree_view_column_set_default_width(self.docs_list_tree_view, column, "31-12-2013")
        column.set_cell_data_func(renderer, self.print_date)

        column = self.docs_list_tree_view.get_column(1)
        tree_view_column_set_default_width(self.docs_list_tree_view, column, "Pre-Payment")
        column = self.docs_list_tree_view.get_column(2)
        tree_view_column_set_default_width(self.docs_list_tree_view, column, "Credit Note")
        column = self.docs_list_tree_view.get_column(3)
        tree_view_column_set_default_width(self.docs_list_tree_view, column, "9,999,999.00")
        column = self.docs_list_tree_view.get_column(4)
        tree_view_column_set_default_width(self.docs_list_tree_view, column, "9,999,999.00")

        model.set_default_sort_func(self.doc_sort_func)
        model.set_sort_column_id(Gtk.TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID, Gtk.SortType.ASCENDING)
        self.component_id = ComponentManager.register(DIALOG_PAYMENT_CM_CLASS, self.refresh_handler, self.close_handler)
        ComponentManager.watch_entity_type(self.component_id, ID_ACCOUNT,
                                           EVENT_CREATE | EVENT_MODIFY | EVENT_DESTROY)

        self.check_payment()
        self.set_amount(Decimal(0))
        contact = self.person.get_contact()
        self.email_entry.set_text(",".join(map(lambda a: a.get_address(), contact.emails)))

        if isinstance(self.person, Customer):
            self.payment_method.set_ref(self.person.get_payment_method())
        self.fill_docs_list()

    def set_num(self, num):
        self.reference_entry.set_text(num)

    def set_date(self, date: datetime.date):
        self.date_edit.set_date(date)

    def set_amount(self, amount):
        if (amount < 0 and isinstance(self.person, Customer)) or (
                amount > 0 and isinstance(self.person, Supplier)):
            self.total_label.set_markup("<b>Refund</b>")
            self.total_label.set_text("AMOUNT REFUNDED")
            amount = -amount
        else:
            self.total_label.set_text("AMOUNT RECEIVED")
            self.total_label.set_markup("<b>Payment</b>")
        self.amount.set_amount(amount)
        self.total_amount = amount
        self.total_amount_label.set_text(
            sprintamount(abs(amount), PrintAmountInfo.commodity(self.person.get_currency(), True)))

    def set_commodity(self, account):
        comm = account.get_commodity()
        self.commodity_label.set_text("(%s)" % comm.get_nice_symbol())

    def has_pre_existing_transaction(self):
        return self.transaction is not None

    def refresh_handler(self, changes):
        self.fill_docs_list()

    def check_payment(self):
        selection = self.docs_list_tree_view.get_selection()
        if selection.count_selected_rows() == 0:
            conflict_msg = "No documents were selected to assign this payment to. This may create an unattached payment."
            allow_payment = True

    def close_handler(self):
        ComponentManager.unregister(self.component_id)
        self.docs_list_tree_view.destroy()
        del self

    @staticmethod
    def calculate_selected_total_helper(model, _, _iter, subtotal):
        lot = model.get_value(_iter, 5)
        if isinstance(lot, (Invoice, Bill)):
            cur_val = lot.get_balance()
        else:
            cur_val = -lot.amount
        subtotal.val += cur_val

    def calculate_selected_total(self):
        class Balance:
            def __init__(self):
                self.val = Decimal(0)

        val = Balance()
        if not self.docs_list_tree_view or not isinstance(self.docs_list_tree_view, Gtk.TreeView):
            return Decimal(0)
        selection = self.docs_list_tree_view.get_selection()
        selection.selected_foreach(self.calculate_selected_total_helper, val)
        return val.val

    def document_selection_changed(self):
        if self.has_pre_existing_transaction():
            return
        val = self.calculate_selected_total()
        self.set_amount(val)

    def fill_docs_list(self):
        if self.docs_list_tree_view is None or not isinstance(self.docs_list_tree_view, Gtk.TreeView):
            return
        if self.person is None:
            return
        selection = self.docs_list_tree_view.get_selection()
        selection.unselect_all()
        store = self.docs_list_tree_view.get_model()
        store.clear()

        for document in self.person.invoices if isinstance(self.person, Customer) else self.person.bills \
                if isinstance(self.person, Supplier) else []:
            debit = Decimal(0)
            credit = Decimal(0)
            value = document.get_balance()
            if value.is_zero():
                continue
            elif value > 0:
                debit = value
            else:
                credit = -value
            doc_deb_str = ""
            doc_cred_str = ""
            doc_date_time = document.get_date_posted()
            doc_type_str = document.__class__.__name__
            doc_id_str = document.get_id()
            if not debit.is_zero():
                doc_deb_str = sprintamount(debit, PrintAmountInfo.default(False))
            if not credit.is_zero():
                doc_cred_str = sprintamount(credit, PrintAmountInfo.default(False))
            store.append([doc_date_time, doc_id_str, doc_type_str, doc_deb_str, doc_cred_str, document])
        for payment in self.person.get_upapplied_payments():
            store.append(
                [payment.date, "", "Prepayment", sprintamount(payment.amount, PrintAmountInfo.default(False)), "",
                 payment])

        selection.select_all()

    def person_changed_cb(self, _, person):
        if person != self.person:
            self.person = person
            contact = person.get_contact()
            self.email_entry.set_text(",".join(map(lambda a: a.get_address(), contact.emails)))
            self.payment_method.set_ref(person.get_payment_method())
            self.person_changed()
        self.check_payment()
        return False

    def person_changed(self):
        self.fill_docs_list()

    @Gtk.Template.Callback()
    def document_selection_changed_cb(self, widget):
        self.document_selection_changed()
        self.check_payment()

    @staticmethod
    def get_selected_documents(model, path, _iter, data):
        doc = model.get_value(_iter, 5)
        if doc is not None:
            data.append(doc)

    def save_payment(self):
        ComponentManager.clear_watches(self.component_id)
        ComponentManager.suspend()
        exch = Decimal(1)
        selected_documents = []
        num = self.reference_entry.get_text()
        date = self.date_edit.get_date()
        if isinstance(self.person, Customer):
            account = self.person.get_receivable_account()
        elif isinstance(self.person, Supplier):
            account = self.person.get_payable_account()
        else:
            show_error("UNKNOWN PERSON")
            return True

        selection = self.docs_list_tree_view.get_selection()
        selection.selected_foreach(self.get_selected_documents, selected_documents)
        payment_method = self.payment_method.get_ref()
        if payment_method is None:
            show_error(self, "Payment method is required!!")
            return False
        if not self.total_amount.is_zero() and payment_method is not None and \
                payment_method.get_account().get_commodity() != account.get_commodity():
            text = "The transfer and post accounts are associated with different currencies." \
                   " Please specify the conversion rate."

            xfer: TransferDialog = TransferDialog(self, account)
            show_info(self, text)

            xfer.select_to_account(self.deposit_account_selection.get_account())
            xfer.set_amount(self.total_amount)
            xfer.set_date(date)
            xfer.set_from_show_button_active(False)
            xfer.set_to_show_button_active(False)
            xfer.hide_from_account_tree()
            xfer.hide_to_account_tree()
            xfer.is_exchange_dialog(exch)
            p, exch = xfer.run_until_done()
            if not p:
                return False

        start = self.notes_buffer.get_start_iter()
        end = self.notes_buffer.get_end_iter()
        memo = self.notes_buffer.get_text(start, end, False)

        self.transaction = self.person.auto_apply_payment(selected_documents, account, payment_method,
                                                          self.total_amount, exch, date, memo, num)
        ComponentManager.resume()
        return True

    @Gtk.Template.Callback()
    def response_cb(self, _, response):
        if response == Gtk.ResponseType.OK or response == Gtk.ResponseType.APPLY:
            self.save_payment()
        if response == Gtk.ResponseType.APPLY:
            return self.send_mail_receipt()
        if response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
        self.destroy()
        return True

    def send_mail_receipt(self):
        pass

    def amount_changed_cb(self, *_):
        amount = self.amount.get_amount()
        if amount != self.total_amount:
            self.set_amount(amount)
        self.check_payment()

    @staticmethod
    def find_handler(find_data, *_):
        return find_data is not None

    def print_date(self, _, cell, tree_model, _iter, *args):
        doc_date_time = tree_model.get_value(_iter, 0)
        doc_date_str = print_datetime(doc_date_time)
        cell.set_property("text", doc_date_str)

    def doc_sort_func(self, model, a, b, *_):
        a_date = model.get_value(a, 0)
        b_date = model.get_value(b, 0)
        a_id = model.get_value(a, 1)
        b_id = model.get_value(b, 1)
        if a_date < b_date:
            ret = -1
        elif a_date > b_date:
            ret = 1
        else:
            ret = GLib.strcmp0(a_id, b_id)
        return ret

    @Gtk.Template.Callback()
    def close_cb(self, *_):
        ComponentManager.close(self.component_id)
        return True

    @staticmethod
    def gen_split_desc(transaction, split):
        value = split.get_value()
        deposit_account_selection = split.get_account()
        acct_name = deposit_account_selection.get_full_name()
        action = gs_get_action_num(transaction, split)
        memo = split.get_memo()
        print_amt = sprintamount(value, PrintAmountInfo.account(deposit_account_selection, True))

        if action is not None and len(action) and memo is not None and len(memo):
            split_str = "%s: %s (%s, %s)" % (acct_name, print_amt,
                                             action, memo)
        elif (action is not None and len(action)) or (memo is not None and len(memo)):
            split_str = "%s: %s (%s)" % (acct_name, print_amt,
                                         action if action is not None else memo)
        else:
            split_str = "%s: %s" % (acct_name, print_amt)
        return split_str

    @classmethod
    def select_payment_split(cls, parent, transaction: Transaction):
        selected_split = None
        payment_splits = transaction.get_payment_account_splits()
        if len(payment_splits) == 0:
            if transaction.get_type() == TransactionType.LINK:
                return
            show_info(parent, "The selected transaction doesn't have splits that can be assigned as a payment")
            return

        if len(payment_splits) > 1:
            message = "While this transaction has multiple splits that can be considered\nas 'the payment split'," \
                      " gasstation only knows how to handle one.\nPlease select one, the others will be ignored.\n\n"
            dialog = Gtk.Dialog(title="Warning", parent=parent,
                                destroy_with_parent=True, modal=True,
                                buttons=("_Cancel", Gtk.ResponseType.CANCEL,
                                         "_Continue", Gtk.ResponseType.OK))
            content = dialog.get_content_area()
            label = Gtk.Label(message)
            content.pack_start(label, False, True, 0)
            first_rb = None
            for i, split in enumerate(payment_splits):
                split_str = cls.gen_split_desc(transaction, split)
                if i == 0:
                    first_rb = Gtk.RadioButton.new_with_label(None, split_str)
                    rbutton = first_rb
                else:
                    rbutton = Gtk.RadioButton.new_with_label_from_widget(first_rb, split_str)

                content.pack_start(rbutton, False, False, 0)

            dialog.set_default_response(Gtk.ResponseType.CANCEL)
            dialog.show_all()
            answer = dialog.run()

            if answer == Gtk.ButtonsType.OK:
                rbgroup = first_rb.get_group()
                for i, rbutton in enumerate(rbgroup):
                    if rbgroup.get_active():
                        selected_split = payment_splits[i]
                        break
            dialog.destroy()
            return selected_split

        else:
            return payment_splits[0]

    @classmethod
    def new_with_transaction(cls, parent, person, transaction):
        if transaction is None:
            return
        if len(transaction) == 0:
            return
        payment_split = cls.select_payment_split(parent, transaction)
        if payment_split is None and transaction.get_type() != TransactionType.LINK:
            return
        self = cls(parent, transaction.get_book(), person)
        self.set_num(gs_get_num_action(transaction, payment_split))
        self.set_memo(transaction.get_description())
        transaction_date = transaction.get_post_date()
        self.set_date(transaction_date)

        self.set_amount(payment_split.get_value())
        if payment_split is not None:
            self.set_transfer_account(payment_split.get_account())
        return self


GObject.type_register(PaymentDialog)
