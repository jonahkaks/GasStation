from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.ui import *
from gasstation.views.account import AccountView
from gasstation.views.selections.amount_edit import *
from gasstation.views.selections.currency import CurrencySelection
from gasstation.views.selections.date_edit import *
from libgasstation.core.component import ComponentManager
from libgasstation.core.helpers import *
from libgasstation.core.price_db import *

ASSISTANT_STOCK_SPLIT_CM_CLASS = "self-stock-split"


class StockSplitCol(GObject.GEnum):
    ACCOUNT = 0
    FULLNAME = 1
    MNEMONIC = 2
    SHARES = 3
    NUM_COLS = 4


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/assistant-stock-split.ui")
class StockSplitAssistant(Gtk.Assistant):
    __gtype_name__ = "StockSplitAssistant"
    account_view: Gtk.TreeView = Gtk.Template.Child()
    stock_details_table: Gtk.Grid = Gtk.Template.Child()
    description_entry: Gtk.Entry = Gtk.Template.Child()
    cash_label: Gtk.Label = Gtk.Template.Child()
    date_label: Gtk.Label = Gtk.Template.Child()
    distribution_label: Gtk.Label = Gtk.Template.Child()
    price_label: Gtk.Label = Gtk.Template.Child()
    cash_box: Gtk.Box = Gtk.Template.Child()
    memo_entry: Gtk.Entry = Gtk.Template.Child()
    income_label: Gtk.Label = Gtk.Template.Child()
    income_scroll:Gtk.ScrolledWindow = Gtk.Template.Child()
    asset_label: Gtk.Label = Gtk.Template.Child()
    asset_scroll:Gtk.ScrolledWindow = Gtk.Template.Child()

    def __init__(self, parent, initial):
        super().__init__()
        gs_widget_set_style_context(self, "StockSplitAssistant")
        if parent is not None:
            self.set_transient_for(parent)

        self.account = None
        view = self.account_view
        view.set_grid_lines(gs_tree_view_get_grid_lines_pref())

        store = Gtk.ListStore(object, str, str, str)
        view.set_model(store)
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Account", renderer, text=StockSplitCol.FULLNAME)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Symbol", renderer, text=StockSplitCol.MNEMONIC)
        view.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Shares", renderer, text=StockSplitCol.SHARES)
        view.append_column(column)

        selection = view.get_selection()
        selection.set_mode(Gtk.SelectionMode.BROWSE)
        selection.connect("changed", self.selection_changed_cb)

        date = DateSelection()
        self.stock_details_table.attach(date, 1, 0, 1, 1)
        date.show()
        self.date_edit = date

        date.make_mnemonic_target(self.date_label)
        amount = AmountEntry()
        amount.connect("changed", self.details_valid_cb)
        amount.set_evaluate_on_enter(True)
        self.stock_details_table.attach(amount, 1, 1, 1, 1)
        amount.show()
        self.distribution_edit = amount
        self.distribution_label.set_mnemonic_widget(amount)

        amount = AmountEntry()
        amount.set_print_info(PrintAmountInfo.price(gs_default_currency()))
        amount.connect("changed", self.details_valid_cb)
        amount.set_evaluate_on_enter(True)
        self.stock_details_table.attach(amount, 1, 5, 1, 1)
        amount.show()
        self.price_edit = amount
        self.price_label.set_mnemonic_widget(amount)
        self.price_currency_edit = CurrencySelection()
        self.price_currency_edit.set_currency(gs_default_currency())
        self.price_currency_edit.show()
        self.stock_details_table.attach(self.price_currency_edit, 1, 6, 1, 1)

        amount = AmountEntry()
        amount.connect("changed", self.cash_valid_cb)
        amount.set_evaluate_on_enter(True)
        self.cash_box.pack_start(amount, True, True, 0)
        self.cash_edit = amount
        self.cash_label.set_mnemonic_widget(amount)

        tree = AccountView()
        self.income_tree = tree
        tree.set_filter(self.view_filter_income)
        tree.show()
        tree.expand_all()
        selection = tree.get_selection()
        selection.unselect_all()
        selection.connect("changed", self.cash_valid_cb)
        self.income_label.set_mnemonic_widget(tree)
        self.income_scroll.add(tree)

        tree = AccountView()
        self.asset_tree = tree
        tree.set_filter(self.view_filter_asset)
        tree.show()
        self.asset_label.set_mnemonic_widget(tree)
        self.asset_scroll.add(tree)
        tree.expand_all()
        selection = tree.get_selection()
        selection.unselect_all()
        selection.connect("changed", self.cash_valid_cb)
        self.connect("destroy", self.window_destroy_cb)
        self.component_id = ComponentManager.register(ASSISTANT_STOCK_SPLIT_CM_CLASS,
                                                      self.refresh_handler, self.close_handler)
        ComponentManager.watch_entity_type(self.component_id, ID_ACCOUNT, EVENT_MODIFY | EVENT_DESTROY)

        if self.fill_account_list(initial) == 0:
            show_warning(parent, "You don't have any stock accounts with balances!")
            ComponentManager.close(self.component_id)
            return
        self.show_all()
        gs_window_adjust_for_screen(self)

    def window_destroy_cb(self, dialog):
        ComponentManager.unregister(self.component_id)

    def fill_account_list(self, selected_account):
        rows = 0
        reference = None
        view = self.account_view
        lis = view.get_model()
        lis.clear()
        accounts = Session.get_current_root().get_descendants_sorted()
        for account in filter(lambda a: a.is_priced() and not a.get_placeholder(), accounts):
            balance = account.get_balance()
            if balance.is_zero():
                continue
            commodity = account.get_commodity()
            full_name = account.get_full_name()
            print_info = PrintAmountInfo.account(account, False)
            _iter = lis.append([account, full_name, commodity.get_mnemonic(), sprintamount(balance, print_info)])

            if account == selected_account:
                path = lis.get_path(_iter)
                reference = Gtk.TreeRowReference.new(lis, path)
            rows += 1

        if reference is not None:
            selection = view.get_selection()
            path = reference.get_path()
            if path is not None:
                selection.select_path(path)
                view.scroll_to_cell(path, None, True, 0.5, 0.0)
        return rows

    def selection_changed_cb(self, selection):
        lis, _iter = selection.get_selected()
        if _iter is None:
            return
        self.account = lis.get_value(_iter, StockSplitCol.ACCOUNT)

    def refresh_details_page(self):
        account = self.account
        if account is None:
            return

        print_info = PrintAmountInfo.account(account, False)
        self.distribution_edit.set_print_info(print_info)
        commodity = account.get_commodity()
        book = account.get_book()
        db = PriceDB.get_db(book)
        prices = db.lookup_latest_any_currency(commodity)
        if any(prices):
            price = prices[0]
            if Commodity.equiv(commodity, price.get_currency()):
                currency = price.get_commodity()
            else:
                currency = price.get_currency()
        else:
            currency = gs_default_currency()
        prices.destroy()
        self.price_currency_edit.set_currency(currency)

    @Gtk.Template.Callback()
    def prepare(self, *_):
        current_page = self.get_current_page()
        if current_page == 2:
            self.details_prepare()

    def details_prepare(self):
        self.refresh_details_page()

    def details_complete(self):
        try:
            amount = self.distribution_edit.expr_is_valid(True)
        except ValueError:
            return False
        if amount.is_zero():
            return False
        try:
            amount = self.price_edit.expr_is_valid(True)
        except ValueError:
            return False
        if amount.is_zero():
            return False
        return True

    def cash_complete(self):
        try:
            self.cash_edit.expr_is_valid(True)
        except ValueError:
            return False
        account = self.income_tree.get_selected_account()
        if account is None:
            return False

        account = self.asset_tree.get_selected_account()
        if account is None:
            return False
        return True

    @Gtk.Template.Callback()
    def finish(self, _):
        account = self.account
        if account is None:
            return
        amount = self.distribution_edit.get_amount()
        if amount.is_zero():
            return
        ComponentManager.suspend()
        trans = Transaction(Session.get_current_book())
        # TODO Add location to stock split
        trans.begin_edit()
        trans.set_currency(gs_default_currency())
        date = self.date_edit.get_date()
        trans.set_post_date(date)
        description = self.description_entry.get_text()
        trans.set_description(description)
        split = Split(Session.get_current_book())
        account.begin_edit()
        account_commits = [account]
        trans.append_split(split)
        account.insert_split(split)
        split.set_amount(amount)
        split.make_stock()
        gs_set_num_action(None, split, None, "Split")
        amount = self.price_edit.get_amount()
        if amount > 0:
            ce = self.price_currency_edit
            price = Price(Session.get_current_book())
            price.begin_edit()
            price.set_commodity(account.get_commodity())
            price.set_currency(ce.get_currency())
            price.set_time64(date)
            price.set_source(PriceSource.STOCK_SPLIT)
            price.set_type(PriceType.UNK)
            price.set_value(amount)
            price.commit_edit()
            book = Session.get_current_book()
            pdb = PriceDB.get_db(book)
            if not pdb.add_price(price):
                show_error(self, "Error adding price.")
        amount = self.cash_edit.get_amount()
        if amount > 0:
            memo = self.memo_entry.get_text()
            account = self.asset_tree.get_selected_account()
            split = Split(Session.get_current_book())
            account.begin_edit()
            account_commits.insert(0, account)
            account.insert_split(split)
            trans.append_split(split)
            split.set_amount(amount)
            split.set_value(amount)
            split.set_memo(memo)
            account = self.income_tree.get_selected_account()
            split = Split(Session.get_current_book())
            account.begin_edit()
            account_commits.insert(0, account)
            account.insert_split(split)
            trans.append_split(split)
            split.set_amount(amount * -1)
            split.set_value(amount * -1)
            split.set_memo(memo)
        trans.commit_edit()
        for acc in account_commits:
            acc.commit_edit()
        ComponentManager.resume()
        ComponentManager.close(self.component_id)

    @Gtk.Template.Callback()
    def cancel(self, _):
        ComponentManager.close(self.component_id)

    @staticmethod
    def view_filter_income(account):
        t = account.get_type()
        return t == AccountType.INCOME

    @staticmethod
    def view_filter_asset(account):
        t = account.get_type()
        return t == AccountType.BANK or t == AccountType.CASH or t == AccountType.CURRENT_ASSET

    def details_valid_cb(self, widget):
        num = self.get_current_page()
        page = self.get_nth_page(num)
        self.set_page_complete(page, self.details_complete())

    def cash_valid_cb(self, widget):
        num = self.get_current_page()
        page = self.get_nth_page(num)
        self.set_page_complete(page, self.cash_complete())

    def refresh_handler(self, changes):
        if self.fill_account_list(self.account) == 0:
            ComponentManager.close(self.component_id)
            return

    def close_handler(self):
        self.destroy()
