from gi import require_version

from gasstation.utilities.custom_dialogs import show_error
from gasstation.utilities.dialog import gs_window_adjust_for_screen, gs_widget_set_style_context
from libgasstation import Location, Session, EVENT_MODIFY, EVENT_DESTROY, ID_LOCATION, Contact, Address
from libgasstation.core.component import ComponentManager
from libgasstation.core.email import Email, EmailForType
from libgasstation.core.phone import PhoneType, Phone

require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject

DIALOG_NEW_LOCATION_CM_CLASS = "dialog-new-location"
DIALOG_EDIT_LOCATION_CM_CLASS = "dialog-edit-location"


class LocationDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/location.ui')
class LocationDialog(Gtk.Dialog):
    __gtype_name__ = 'LocationDialog'
    name_entry: Gtk.Entry = Gtk.Template.Child()
    phone_entry: Gtk.Entry = Gtk.Template.Child()
    email_entry: Gtk.Entry = Gtk.Template.Child()
    address_frame = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, location=None, name=None, **kwargs):
        super().__init__(**kwargs)
        self.set_transient_for(parent)
        self.book = book
        self.type = LocationDialogType.NEW if location is None else LocationDialogType.EDIT
        self.location = location
        if name is not None:
            self.name_entry.set_text(name)
        gs_window_adjust_for_screen(self)

        self.component_id = ComponentManager.register(DIALOG_NEW_LOCATION_CM_CLASS,
                                                      self.refresh_handler,
                                                      None if modal else self.close_handler)

        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id,
                                           ID_LOCATION,
                                           EVENT_MODIFY | EVENT_DESTROY)
        if location is not None:
            self.to_ui()

    def to_ui(self):
        loc = self.location
        self.name_entry.set_text(loc.get_name() or "")
        if self.type == LocationDialogType.EDIT:
            contact = loc.get_contact()
            if any(contact.emails):
                self.email_entry.set_text(contact.emails[0].get_address())
            if any(contact.phones):
                self.phone_entry.set_text(str(contact.phones[0].get_number()))
            if any(contact.addresses):
                self.address_frame.set_address(contact.addresses[0])

    def to_location(self):
        loc: Location = self.location
        if loc is None:
            ComponentManager.suspend()
            loc = Location(self.book)
            self.location = loc
        loc.begin_edit()
        contact = loc.get_contact()
        if contact is None:
            contact = Contact(self.book)
            loc.set_contact(contact)
        name = self.name_entry.get_text()
        loc.set_name(name)
        contact.set_full_name(name)
        contact.set_display_name(name)
        ea = None
        if any(contact.emails):
            ea = contact.emails[0]
        email_text = self.email_entry.get_text()
        if email_text == "" and ea is not None:
            ea.destroy()
            ea.commit_edit()
        elif email_text != "":
            if ea is None:
                ea = Email(self.book)
            ea.begin_edit()
            ea.set_type(EmailForType.WORK)
            ea.set_address(email_text)
            ea.set_contact(contact)
            ea.commit_edit()
        phone_text =self.phone_entry.get_text()
        ph =None
        if any(contact.phones):
            ph = contact.phones[0]
        if phone_text == "" and ph is not None:
            ph.destroy()
            ph.commit_edit()
        elif phone_text != "":
            if ph is None:
                ph = Phone(self.book)
            ph.begin_edit()
            ph.set_type(PhoneType.WORK)
            ph.set_number(phone_text)
            ph.set_contact(contact)
            ph.commit_edit()

        if any(contact.addresses):
            ba = contact.addresses[0]
        else:
            ba = Address(self.book)
        ba.begin_edit()
        self.address_frame.copy_to_address(ba)
        ba.set_contact(contact)
        ba.commit_edit()
        contact.commit_edit()

    def finish_ok(self):
        be = self.book.get_backend()
        if be is not None:
            with be.add_lock():
                self.to_location()
                self.location.commit_edit()
        self.close()
        ComponentManager.close(self.component_id)

    def common_ok(self):
        name = self.name_entry.get_text()
        if name == "":
            message = "Please add a name."
            show_error(self, message)
            self.name_entry.grab_focus()
            return False
        if self.type == LocationDialogType.NEW:
            book = Session.get_current_book()
            if Location.lookup_name(book, name) is not None:
                show_error(self, "The location {} already exists".format(name))
                return False
        if not self.address_frame.validate():
            show_error(self, "Please fill in the address details")
            gs_widget_set_style_context(self.address_frame, "error")
            return False
        return True

    @Gtk.Template.Callback()
    def response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            if not self.common_ok():
                return False
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            dialog.close()

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        inv = self.location
        if inv is None:
            ComponentManager.close(self.component_id)
            return
        if changes:
            info = ComponentManager.get_entity_events(changes, inv.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(LocationDialog)
