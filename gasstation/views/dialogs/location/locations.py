from gasstation.utilities.cursors import *
from gasstation.utilities.custom_dialogs import *
from gasstation.views.dialogs.location.location import LocationDialog
from gasstation.views.location import *

DIALOG_LOCATIONS_CM_CLASS = "dialog-locations"
STATE_SECTION = "dialogs/edit_locations"
from libgasstation.core.session import Session


class LocationsDialog(GObject.Object):
    def __init__(self):
        GObject.Object.__init__(self)
        self.dialog = None
        self.session = None
        self.book = None
        self.location_tree = None
        self.edit_button = None
        self.remove_button = None
        self.component_id = 0

    def window_destroy_cb(self, widget):
        ComponentManager.unregister(self.component_id)

    def edit_clicked(self):
        locations = self.location_tree.get_selected_locations()
        if not any(locations):
            return
        for location in locations:
            dialog = LocationDialog(self.dialog,book=Session.get_current_book(), location=location)
            dialog.run()
        ComponentManager.refresh_all()

    def row_activated_cb(self, view, path, column):
        self.edit_clicked()

    def remove_clicked(self):
        from gasstation.views.dialogs.object_references import ObjectReferencesDialog
        locations = self.location_tree.get_selected_locations()
        if locations is None or not any(locations):
            return
        dialog = Gtk.MessageDialog(transient_for=self.dialog, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE,
                                   message_format="The following locations will be deleted %s" % ",".join([location.get_name() for location in locations]))
        dialog.add_buttons("Cancel", Gtk.ResponseType.CANCEL, "Delete", Gtk.ResponseType.ACCEPT)
        dialog.set_default_response(Gtk.ResponseType.CANCEL)
        response = dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            gs_set_busy_cursor(None, True)
            ComponentManager.suspend()
            for location in locations:
                lis = location.get_referrers()
                if len(lis):
                    exp = "The list below shows objects which make use of the location which you want to delete.\n" \
                          "Before you can delete it, you must either delete those objects or else modify them so they " \
                          "make use of another location"
                    ObjectReferencesDialog(exp, lis)
                    continue
                location.begin_edit()
                location.destroy()
            ComponentManager.resume()
            gs_unset_busy_cursor(None)

    def add_clicked(self):
        dialog = LocationDialog(self.dialog, Session.get_current_book())
        dialog.run()

    def dialog_response(self, dialog, response):
        if response == RESPONSE_NEW:
            self.add_clicked()
            return

        elif response == RESPONSE_DELETE:
            self.remove_clicked()
            return

        elif response == RESPONSE_EDIT:
            self.edit_clicked()
            return

        else:
            ComponentManager.close(self.component_id)

    def selection_changed(self, selection):
        y = any(self.location_tree.get_selected_locations())
        self.edit_button.set_sensitive(y)
        self.remove_button.set_sensitive(y)

    def create(self, parent):
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/common.ui", ["common_dialog"])
        self.dialog = builder.get_object("common_dialog")
        self.dialog.set_title("Locations Dialog")
        self.session = Session.get_current_session()
        self.book = self.session.get_book()
        gs_widget_set_style_context(self.dialog, "LocationsDialog")
        builder.connect_signals(self)
        if parent is not None:
            self.dialog.set_transient_for(parent)
        self.remove_button = builder.get_object("remove_button")
        self.edit_button = builder.get_object("edit_button")
        scrolled_window = builder.get_object("common_list_window")
        view = LocationView(state_section=STATE_SECTION)
        self.location_tree = view
        scrolled_window.add(view)
        scrolled_window.show()
        view.set_headers_visible(True)
        selection = view.get_selection()
        selection.connect("changed", self.selection_changed)
        self.location_tree.connect("row-activated", self.row_activated_cb)

    def close_handler(self):
        self.location_tree.destroy()
        self.dialog.destroy()

    def show_handler(self, klass, component_id):
        self.dialog.present()
        return True

    @classmethod
    def new(cls, parent):
        if ComponentManager.forall(DIALOG_LOCATIONS_CM_CLASS, cls.show_handler):
            return
        iv = GObject.new(cls)
        iv.create(parent)
        iv.component_id = ComponentManager.register(DIALOG_LOCATIONS_CM_CLASS, None, iv.close_handler)
        ComponentManager.set_session(iv.component_id, iv.session)
        iv.location_tree.grab_focus()
        iv.dialog.show()
