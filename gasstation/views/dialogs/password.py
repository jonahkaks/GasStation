from gi import require_version

require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/login.ui')
class PassWordDialog(Gtk.Dialog):
    __gtype_name__ = 'PassWordDialog'
    username_entry: Gtk.Entry = Gtk.Template.Child()
    password_entry: Gtk.Entry = Gtk.Template.Child()

    def __init__(self, parent, title=None, username=None, **kwargs):
        super().__init__(**kwargs)
        if parent is not None:
            self.set_transient_for(parent)
        if title is not None:
            self.set_title(title)
        if username is not None:
            self.username_entry.set_text(username)
        self.show()

    def get_password(self):
        return self.password_entry.get_text()


GObject.type_register(PassWordDialog)
