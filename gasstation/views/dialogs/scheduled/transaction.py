from libgasstation.core.transaction_template import *
from .editor import *

SXFTD_ERRNO_UNBALANCED_XACTION = 3
SXFTD_ERRNO_OPEN_XACTION = -3

SXFTD_EXCAL_NUM_MONTHS = 4
SXFTD_EXCAL_MONTHS_PER_COL = 4

SXFTD_RESPONSE_ADVANCED = 100


class GetEndTuple:
    def __init__(self):
        self.type = 0
        self.end_date = datetime.datetime.now()
        self.n_occurrences = 0


class ScheduledFromTransactionFrequencyType(GObject.GEnum):
    DAILY = 0
    WEEKLY = 1
    BIWEEKLY = 2
    MONTHLY = 3
    QUARTERLY = 4
    ANNUALLY = 5


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/scheduled/from_transaction.ui')
class ScheduledFromTransactionDialog(Gtk.Dialog):
    __gtype_name__ = "ScheduledFromTransactionDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()
    end_date_box: Gtk.Box = Gtk.Template.Child()
    never_end_button: Gtk.RadioButton = Gtk.Template.Child()
    end_on_date_button: Gtk.RadioButton = Gtk.Template.Child()
    end_on_number_of_occurrences_button: Gtk.RadioButton = Gtk.Template.Child()
    number_of_occurrences_entry: Gtk.Entry = Gtk.Template.Child()
    dense_calendar_frame: Gtk.Frame = Gtk.Template.Child()
    frequency_combo: Gtk.ComboBox = Gtk.Template.Child()
    param_table: Gtk.Grid = Gtk.Template.Child()

    def __init__(self, parent, transaction):
        super().__init__()
        self.set_transient_for(parent)
        self.scheduled = ScheduledTransaction(Session.get_current_book())
        self.scheduled.begin_edit()
        self.transaction = transaction
        trans_name = transaction.get_description()
        self.scheduled.set_name(trans_name)
        if trans_name is not None:
            self.name_entry.set_text(trans_name)
        num_marks = SXFTD_EXCAL_NUM_MONTHS * 31

        self.dense_calendar_model = DenseCalendarStore.new(num_marks)
        self.dense_calendar = DenseCalendar.new_with_model(self.dense_calendar_model)
        self.dense_calendar.set_num_months(SXFTD_EXCAL_NUM_MONTHS)
        self.dense_calendar.set_months_per_col(SXFTD_EXCAL_MONTHS_PER_COL)
        self.dense_calendar_frame.add(self.dense_calendar)

        self.start_date = DateSelection()
        self.param_table.attach(self.start_date, 1, 2, 1, 1)
        self.start_date.set_halign(Gtk.Align.FILL)
        self.start_date.set_valign(Gtk.Align.FILL)
        self.start_date.set_hexpand(True)
        self.start_date.set_vexpand(False)
        self.start_date.set_property("margin", 0)
        self.start_date.connect("date-changed", self.update_dense_calendar_adapter)
        self.start_date.show()

        self.end_date = DateSelection()
        self.end_date_box.pack_start(self.end_date, True, True, 0)
        self.end_date.connect("date-changed", self.update_dense_calendar_adapter)
        self.end_date.show()

        date = self.transaction.get_post_date()

        schedule = self.update_schedule(date)
        next_date = Recurrence.list_next_instance(schedule, date)
        self.start_date.set_time(next_date)
        self.update_dense_calendar()
        gs_widget_set_style_context(self, "ScheduledFromTransactionDialog")
        self.show()

    def get_end_info(self):
        retval = GetEndTuple()
        retval.type = DenseCalendarEndType.BAD_END
        retval.n_occurrences = 0

        if self.never_end_button.get_active():
            retval.type = DenseCalendarEndType.NEVER_END
            return retval
        if self.end_on_date_button.get_active():
            retval.type = DenseCalendarEndType.END_ON_DATE
            retval.end_date = self.end_date.get_date()
            return retval

        if self.end_on_number_of_occurrences_button.get_active():
            text = self.number_of_occurrences_entry.get_chars(0, -1)
            if text is None or len(text) == 0:
                n_occs = 0
            else:
                try:
                    n_occs = int(text)
                except ValueError:
                    n_occs = -1
            retval.type = DenseCalendarEndType.END_AFTER_N_OCCS
            retval.n_occurrences = n_occs
            return retval
        return retval

    def add_template_transaction(self):
        tr = self.transaction
        tt_list = []
        template_splits = []
        tti = TransactionTemplate()
        running_balance = Decimal(0)
        tti.set_description(tr.get_description())
        tti.set_location(tr.get_location())
        tti.set_num(gs_get_num_action(tr, None))
        tti.set_notes(tr.get_notes())
        tti.set_currency(tr.get_currency())

        for sp in tr.splits:
            ttsi = SplitTemplate()
            ttsi.set_action(gs_get_num_action(None, sp))
            split_value = sp.get_value()
            ttsi.set_memo(sp.get_memo())
            running_balance += split_value

            if split_value > 0:
                tmpStr = sprintamount(split_value, PrintAmountInfo.default(False, False))
                ttsi.set_debit_formula(tmpStr)
            else:
                tmpStr = sprintamount(split_value * -1, PrintAmountInfo.default(False, False))
                ttsi.set_credit_formula(tmpStr)
            ttsi.set_account(sp.get_account())
            template_splits.append(ttsi)

        if not running_balance.is_zero() and not ask_yes_no(self, False, "The Scheduled Transaction Editor "
                                                                         "cannot automatically balance this "
                                                                         "transaction. "
                                                                         "Should it still be entered?"):
            return SXFTD_ERRNO_UNBALANCED_XACTION

        tti.set_template_splits(template_splits)
        tt_list.append(tti)
        ComponentManager.suspend()
        self.scheduled.set_template_transaction(tt_list, Session.get_current_book())
        ComponentManager.resume()
        return 0

    def update_schedule(self, date):
        recurrences = []
        index = self.frequency_combo.get_active()
        if index == ScheduledFromTransactionFrequencyType.DAILY:
            r = Recurrence()
            r.set(1, RecurrencePeriodType.DAY, date, RecurrenceWeekendAdjust.NONE)
            recurrences.append(r)
        elif index == ScheduledFromTransactionFrequencyType.WEEKLY or \
                index == ScheduledFromTransactionFrequencyType.BIWEEKLY:
            r = Recurrence()
            mult = 2 if index == ScheduledFromTransactionFrequencyType.BIWEEKLY else 1
            r.set(mult, RecurrencePeriodType.WEEK, date, RecurrenceWeekendAdjust.NONE)
            recurrences.append(r)
        elif index == ScheduledFromTransactionFrequencyType.MONTHLY or \
                index == ScheduledFromTransactionFrequencyType.QUARTERLY or \
                index == ScheduledFromTransactionFrequencyType.ANNUALLY:
            r = Recurrence()
            mult = 1 if index == ScheduledFromTransactionFrequencyType.MONTHLY else 3 \
                if index == ScheduledFromTransactionFrequencyType.QUARTERLY else 12
            r.set(mult, RecurrencePeriodType.MONTH, date, r.get_weekend_adjustment())
            recurrences.append(r)
        return recurrences

    def compute_scheduled(self):
        error_number = 0
        scheduled = self.scheduled
        name = self.name_entry.get_chars(0, -1)
        scheduled.set_name(name)
        date = self.start_date.get_date()
        schedule = self.update_schedule(date)
        if error_number == 0:
            scheduled.set_schedule(schedule)
            scheduled.set_start_date(date)
        end_info = self.get_end_info()

        if end_info.type == DenseCalendarEndType.NEVER_END:
            pass
        elif end_info.type == DenseCalendarEndType.END_ON_DATE:
            scheduled.set_end_date(end_info.end_date)
        elif end_info.type == DenseCalendarEndType.END_AFTER_N_OCCS:
            scheduled.set_num_occur(end_info.n_occurrences)
        else:
            error_number = 2
        scheduled.set_instance_count(1)
        auto_create_state = gs_pref_get_bool(PREFS_GROUP_SXED, PREF_CREATE_AUTO)
        notify_state = gs_pref_get_bool(PREFS_GROUP_SXED, PREF_NOTIFY)
        scheduled.set_auto_create(auto_create_state, (auto_create_state & notify_state))

        days_in_advance = gs_pref_get_float(PREFS_GROUP_SXED, PREF_CREATE_DAYS)
        scheduled.set_advance_creation(days_in_advance)
        days_in_advance = gs_pref_get_float(PREFS_GROUP_SXED, PREF_REMIND_DAYS)
        scheduled.set_advance_reminder(days_in_advance)
        if self.add_template_transaction() != 0:
            error_number = SXFTD_ERRNO_UNBALANCED_XACTION
        return error_number

    def on_close(self, delete_scheduled):
        if self.scheduled and delete_scheduled:
            self.scheduled.destroy()
        self.scheduled = None
        self.destroy()

    def ok_clicked(self):
        scheduled_error = self.compute_scheduled()
        if scheduled_error != 0 and scheduled_error != SXFTD_ERRNO_UNBALANCED_XACTION:
            pass
        else:
            if scheduled_error == SXFTD_ERRNO_UNBALANCED_XACTION:
                show_error(self, "The Scheduled Transaction is unbalanced. "
                                 "You are strongly encouraged to correct this situation.")
            book = Session.get_current_book()
            schedules = ScheduledTransactionList(book)
            with book.get_backend().add_lock():
                self.scheduled.commit_edit()
                schedules.add(self.scheduled)
        self.on_close(False)
        return

    @Gtk.Template.Callback()
    def frequency_changed_cb(self, _):
        date = self.transaction.get_post_date()
        schedule = self.update_schedule(date)
        next_date = Recurrence.list_next_instance(schedule, date)
        if next_date is not None:
            self.start_date.set_time(next_date)
        self.update_dense_calendar()

    def advanced_clicked(self):
        from gasstation.views.main_window import MainWindow
        scheduled_error = self.compute_scheduled()
        if scheduled_error != 0 and scheduled_error != SXFTD_ERRNO_UNBALANCED_XACTION:
            return
        self.hide()
        dialog = ScheduledTransactionEditorDialog(MainWindow.get_main_window(self), self.scheduled, True)
        dialog.run()
        self.on_close(False)

    def destroy_cb(self, w):
        if self.scheduled:
            self.scheduled.destroy()

    @Gtk.Template.Callback()
    def response_cb(self, _, response):
        if response == Gtk.ResponseType.OK:
            self.ok_clicked()
        elif response == SXFTD_RESPONSE_ADVANCED:
            self.advanced_clicked()
        else:
            self.on_close(True)

    def update_dense_calendar(self):
        get = self.get_end_info()
        date = self.start_date.get_date()
        schedule = self.update_schedule(date)
        start_date = date
        date = date - datetime.timedelta(days=1)
        Recurrence.list_next_instance(schedule, date)
        name = self.name_entry.get_chars(0, -1)
        self.dense_calendar_model.update_name(name)
        schedule_desc = Recurrence.list_to_compact_string(schedule)
        self.dense_calendar_model.update_info(schedule_desc)
        self.end_date.set_sensitive(get.type == DenseCalendarEndType.END_ON_DATE)
        self.number_of_occurrences_entry.set_sensitive(get.type == DenseCalendarEndType.END_AFTER_N_OCCS)
        if get.type == DenseCalendarEndType.NEVER_END:
            self.dense_calendar_model.update_recurrences_no_end(date, schedule)
        elif get.type == DenseCalendarEndType.END_ON_DATE:
            self.dense_calendar_model.update_recurrences_date_end(date, schedule, get.end_date)
        elif get.type == DenseCalendarEndType.END_AFTER_N_OCCS:
            self.dense_calendar_model.update_recurrences_count_end(date, schedule, get.n_occurrences)
        self.dense_calendar.set_month(start_date.month)
        self.dense_calendar.set_year(start_date.year)

    @Gtk.Template.Callback()
    def update_dense_calendar_adapter(self, *args):
        self.update_dense_calendar()

    def __new__(cls, parent, transaction, *args, **kwargs):
        errno = 0
        if transaction is None:
            errno = -2
        if transaction.is_open():
            errno = SXFTD_ERRNO_OPEN_XACTION
        if errno < 0:
            if errno == SXFTD_ERRNO_OPEN_XACTION:
                show_error(None, "Cannot create a Scheduled Transaction "
                                 "from a Transaction currently "
                                 "being edited. Please Enter the "
                                 "Transaction before Scheduling.")
                return
        return super().__new__(cls, transaction, parent=parent)


GObject.type_register(ScheduledFromTransactionDialog)
