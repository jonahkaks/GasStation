from gasstation.models.dense_calendar import *
from gasstation.views.dense_calendar import *
from gasstation.views.dialogs.preferences import *
from gasstation.views.embedded_window import EmbeddedWindow
from gasstation.views.register.ledger import *
from gasstation.views.selections.frequency import *
from libgasstation.core.backend import BackEnd
from libgasstation.core.scheduled_transaction import ScheduledTransaction
from libgasstation.core.scheduled_transaction_list import *
from .since_last_run import ScheduledSinceLastRunDialog

DIALOG_SCHEDXACTION_CM_CLASS = "dialog-scheduledtransactions"
DIALOG_SCHEDXACTION_EDITOR_CM_CLASS = "dialog-scheduledtransaction-editor"

PREFS_GROUP_SXED = "dialogs.schedules.transaction-editor"
PREF_CREATE_DAYS = "create-days"
PREF_REMIND_DAYS = "remind-days"
PREF_CREATE_AUTO = "create-auto"
PREF_NOTIFY = "notify"

END_NEVER_OPTION = 0
END_DATE_OPTION = 1
NUM_OCCUR_OPTION = 2

NUM_LEDGER_LINES_DEFAULT = 6
EX_CAL_NUM_MONTHS = 6
EX_CAL_MO_PER_COL = 3
D_WIDTH = 25
D_BUF_WIDTH = 26


class EndType(IntEnum):
    NEVER = 0
    DATE = 1
    OCCUR = 2


menu_entries = [
    ("EditAction", None, "_Edit", None, None, None),
    ("TransactionAction", None, "_Transaction", None, None, None),
    ("ViewAction", None, "_View", None, None, None),
    ("ActionsAction", None, "_Actions", None, None, None)]


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/scheduled/account_deletion.ui')
class ScheduledAccountDeletionDialog(Gtk.Dialog):
    __gtype_name__ = "ScheduledAccountDeletionDialog"
    scheduled_list: Gtk.TreeView = Gtk.Template.Child()

    def __init__(self, model, **kwargs):
        super().__init__(**kwargs)
        renderer = Gtk.CellRendererText()
        name_column = Gtk.TreeViewColumn("Name", renderer, text=0)
        self.scheduled_list.append_column(name_column)
        self.scheduled_list.set_model(model)
        self.scheduled_list.set_grid_lines(gs_tree_view_get_grid_lines_pref())


GObject.type_register(ScheduledAccountDeletionDialog)


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/scheduled/editor.ui')
class ScheduledTransactionEditorDialog(Gtk.Dialog):
    __gtype_name__ = "ScheduledTransactionEditorDialog"

    notebook: Gtk.Notebook = Gtk.Template.Child()
    name_entry: Gtk.Entry = Gtk.Template.Child()
    enabled_opt: Gtk.ToggleButton = Gtk.Template.Child()
    auto_create_opt: Gtk.ToggleButton = Gtk.Template.Child()
    notify_opt: Gtk.ToggleButton = Gtk.Template.Child()
    advance_opt: Gtk.ToggleButton = Gtk.Template.Child()
    advance_days: Gtk.SpinButton = Gtk.Template.Child()
    remind_opt: Gtk.ToggleButton = Gtk.Template.Child()
    remind_days: Gtk.SpinButton = Gtk.Template.Child()
    last_occur_label: Gtk.Label = Gtk.Template.Child()
    never_end_opt: Gtk.RadioButton = Gtk.Template.Child()
    end_on_date_opt: Gtk.RadioButton = Gtk.Template.Child()
    end_after_opt: Gtk.RadioButton = Gtk.Template.Child()
    end_spin: Gtk.SpinButton = Gtk.Template.Child()
    remain_spin: Gtk.SpinButton = Gtk.Template.Child()

    frequency_box: Gtk.Box = Gtk.Template.Child()
    dense_calendar_box: Gtk.Box = Gtk.Template.Child()
    register_box: Gtk.Box = Gtk.Template.Child()
    end_date_box: Gtk.Box = Gtk.Template.Child()

    def __init__(self, parent, scheduled: ScheduledTransaction=None):
        super().__init__()
        from gasstation.plugins.pages import RegisterPluginPage

        if scheduled is None:
            scheduled = ScheduledTransaction(Session.get_current_book())
            r = Recurrence()
            now = datetime.date.today()
            r.set(1, RecurrencePeriodType.MONTH, now, RecurrenceWeekendAdjust.NONE)
            scheduled.add_recurrence(r)
        self.scheduled = scheduled
        gs_widget_set_style_context(self, "ScheduledTransactionEditor")
        self.set_transient_for(parent)
        self.end_date_entry: DateSelection = DateSelection()
        self.end_date_entry.connect("date-changed", self.occurrences_update_cb)
        self.end_date_entry.show()
        self.end_date_box.pack_start(self.end_date_entry, True, True, 0)

        self.component_id = ComponentManager.register(DIALOG_SCHEDXACTION_EDITOR_CM_CLASS, None, self.close_handler,
                                                      self)

        self.notify_opt.set_sensitive(False)
        self.advance_days.set_sensitive(False)
        self.remind_days.set_sensitive(False)
        self.end_spin.set_sensitive(False)
        self.remain_spin.set_sensitive(False)
        self.advance_days.set_editable(True)
        self.remind_days.set_editable(True)
        self.set_resizable(True)
        gs_restore_window_size(PREFS_GROUP_SXED, self, parent)

        start_date = self.scheduled.get_start_date()
        self.frequency = FrequencySelection(scheduled.get_schedule() if scheduled is not None else [],start_date)
        self.frequency.connect("changed", self.frequency_changed_cb)
        self.frequency_box.add(self.frequency)
        self.frequency.show()

        self.dense_calendar_model = DenseCalendarStore.new(EX_CAL_NUM_MONTHS * 31)
        self.dense_calendar = DenseCalendar.new_with_model(self.dense_calendar_model)
        self.dense_calendar.set_num_months(EX_CAL_NUM_MONTHS)
        self.dense_calendar.set_months_per_col(EX_CAL_MO_PER_COL)
        self.dense_calendar_box.pack_start(self.dense_calendar, True, True, 0)
        self.dense_calendar.show()

        scheduled_guid = str(scheduled.get_guid()) if scheduled is not None else ""
        self.ledger = RegisterLedger.template_gl(scheduled_guid)
        model = self.ledger.get_model()

        self.embed_window = EmbeddedWindow("SXWindowActions", menu_entries, "scheduled-window-ui.xml", self,
                                           False, self)
        self.register_box.pack_start(self.embed_window, True, True, 0)
        self.embed_window.show()
        label = Gtk.Label("Note: If you have already accepted changes to the Template, Cancel will not revoke them.")
        self.register_box.pack_end(label, False, True, 0)
        label.show()

        self.plugin_page = RegisterPluginPage.new_ledger(self.ledger)
        self.plugin_page.set_ui_description("scheduled-window-ui-full.xml")
        self.plugin_page.set_options(NUM_LEDGER_LINES_DEFAULT, False)
        self.embed_window.open_page(self.plugin_page)
        model.config(model.type, model.style, True)

        self.populate()
        self.show()
        self.notebook.set_current_page(0)
        self.dense_calendar.queue_resize()
        self.ledger.refresh()
        self.name_entry.grab_focus()

    def close_handler(self, *args):
        self.reg_check_close()
        gs_save_window_size(PREFS_GROUP_SXED, self)
        self.destroy()

    def confirmed_cancel(self):
        view = self.ledger.get_view()
        if self.check_changed():
            scheduled_changed_msg = "This Scheduled Transaction has changed are you sure you want to cancel?"
            if not ask_yes_no(self, False, scheduled_changed_msg):
                return False
        view.cancel_edit(True)
        return True

    @Gtk.Template.Callback()
    def cancel_button_clicked_cb(self, b, *args):
        if not self.confirmed_cancel():
            return True
        ComponentManager.close(self.component_id)
        return True

    @Gtk.Template.Callback()
    def help_button_clicked_cb(self, b):
        # gs_gnome_help(HF_HELP, HL_SXEDITOR)
        pass

    def check_consistent(self):
        ttVarCount = 0
        splitCount = 0
        NUM_ITERS_WITH_VARS = 5
        NUM_ITERS_NO_VARS = 1
        unbalanceable = False
        vars = {}
        transactions = {}

    @Gtk.Template.Callback()
    def ok_button_clicked_cb(self, *_):
        b = self.scheduled.get_book()
        be: BackEnd = b.get_backend()
        with be.add_lock():
            self.save_scheduled()
            if self.new_scheduled:
                book = Session.get_current_book()
                schedules = ScheduledTransactionList(book)
                schedules.add(self.scheduled)
                self.new_scheduled = False
            ComponentManager.close(self.component_id)

    def check_changed(self):
        if self.new_scheduled:
            return True
        name = self.name_entry.get_chars(0, -1)
        if len(name) == 0:
            return True
        scheduled_name = self.scheduled.get_name()
        if scheduled_name is None or scheduled_name != name:
            return True

        if self.never_end_opt.get_active():
            if self.scheduled.has_end_date() or self.scheduled.has_occur_def():
                return True
        if self.end_on_date_opt.get_active():
            if not self.scheduled.has_end_date():
                return True
            scheduled_end_date = self.scheduled.get_end_date()
            dlg_end_date = self.end_date_entry.get_date()

            if scheduled_end_date != dlg_end_date:
                return True

        if self.end_after_opt.get_active():
            if not self.scheduled.get_num_occur():
                return True
            dlg_num_occur = self.end_spin.get_value_as_int()
            dlg_num_rem = self.remain_spin.get_value_as_int()
            scheduled_num_occur = self.scheduled.get_num_occur()
            scheduled_num_rem = self.scheduled.get_rem_occur()
            if (dlg_num_occur != scheduled_num_occur) or (dlg_num_rem != scheduled_num_rem):
                return True

        dlg_enabled = self.enabled_opt.get_active()
        dlg_auto_create = self.auto_create_opt.get_active()
        dlg_notify = self.notify_opt.get_active()
        scheduled_enabled = self.scheduled.get_enabled()
        if not (dlg_enabled == scheduled_enabled):
            return True

        scheduled_auto_create, scheduled_notify = self.scheduled.get_auto_create()
        if not ((dlg_auto_create == scheduled_auto_create) and (dlg_notify == scheduled_notify)):
            return True
        dlg_advance = 0
        if self.advance_opt.get_active():
            dlg_advance = self.advance_days.get_value_as_int()
        scheduled_advance = self.scheduled.get_advance_creation()
        if dlg_advance != scheduled_advance:
            return True
        dlg_remind = 0
        if self.remind_opt.get_active():
            dlg_remind = self.remind_days.get_value_as_int()
        scheduled_remind = self.scheduled.get_advance_reminder()
        if dlg_remind != scheduled_remind:
            return True

        dialog_schedule = []
        dialog_start_date = self.frequency.save_to_recurrence(dialog_schedule)
        dialog_schedule_str = Recurrence.list_to_string(dialog_schedule)

        scheduled_start_date = self.scheduled.get_start_date()
        scheduled_schedule_str = Recurrence.list_to_string(self.scheduled.get_schedule())
        schedules_are_the_same = dialog_schedule_str == scheduled_schedule_str

        start_dates_are_the_same = dialog_start_date == scheduled_start_date
        if not schedules_are_the_same or not start_dates_are_the_same:
            return True
        view = self.ledger.get_view()
        view.finish_edit()
        if view.get_dirty_trans() is not None:
            return True
        return False

    def save_scheduled(self):
        if self.scheduled.get_edit_level() == 0:
            self.scheduled.begin_edit()
        name = self.name_entry.get_text()
        self.scheduled.set_name(name)
        if self.end_on_date_opt.get_active():
            dt = self.end_date_entry.get_date()
            self.scheduled.set_end_date(dt)
            self.scheduled.set_num_occur(0)
        elif self.end_after_opt.get_active():
            num = self.end_spin.get_value_as_int()
            self.scheduled.set_num_occur(num)
            num = self.remain_spin.get_value_as_int()
            self.scheduled.set_rem_occur(num)
            self.scheduled.set_end_date(None)

        elif self.never_end_opt.get_active():
            self.scheduled.set_num_occur(0)
            self.scheduled.set_end_date(None)

        enabled_state = self.enabled_opt.get_active()
        self.scheduled.set_enabled(enabled_state)
        auto_create_state = self.auto_create_opt.get_active()
        notify_state = self.notify_opt.get_active()
        self.scheduled.set_auto_create(auto_create_state, (auto_create_state & notify_state))
        days_in_advance = 0
        if self.advance_opt.get_active():
            days_in_advance = self.advance_days.get_value_as_int()
        self.scheduled.set_advance_creation(days_in_advance)
        days_in_advance = 0
        if self.remind_opt.get_active():
            days_in_advance = self.remind_days.get_value_as_int()
        self.scheduled.set_advance_reminder(days_in_advance)
        schedule = []
        date = self.frequency.save_to_recurrence(schedule)
        self.scheduled.set_schedule(schedule)
        self.scheduled.set_start_date(date)

        self.scheduled.commit_edit()

    @Gtk.Template.Callback()
    def enabled_toggled_cb(self, *args):
        return

    @Gtk.Template.Callback()
    def auto_create_toggled_cb(self, o, *args):
        if not o.get_active():
            self.notify_opt.set_active(False)
        self.notify_opt.set_sensitive(o.get_active())

    @Gtk.Template.Callback()
    def advance_toggled_cb(self, *args):
        self.advance_days.set_sensitive(self.advance_opt.get_active())
        self.advance_days.set_editable(True)

    @Gtk.Template.Callback()
    def remind_toggled_cb(self, *args):
        self.remind_days.set_sensitive(self.remind_opt.get_active())
        self.remind_days.set_editable(True)

    @Gtk.Template.Callback()
    def dialog_destroy(self, *args):
        ComponentManager.unregister(self.component_id)
        self.embed_window.close_page(self.plugin_page)
        self.embed_window.destroy()
        self.embed_window = None
        self.plugin_page = None
        self.ledger = None
        if self.new_scheduled:
            self.scheduled.destroy()
        self.scheduled = None

    @Gtk.Template.Callback()
    def delete_cb(self, *_):
        if self.scheduled is None:
            return False
        if not self.confirmed_cancel():
            return True
        return False

    def __new__(cls, parent, scheduled):
        dlg_exists = ComponentManager.find_gui_components(DIALOG_SCHEDXACTION_EDITOR_CM_CLASS,
                                                          cls.component_scheduled_equality,
                                                          scheduled)
        if any(dlg_exists):
            self = dlg_exists[0][0]

            self.present()
            return self

        return super().__new__(cls, parent, scheduled)

    def populate(self):
        name = self.scheduled.get_name()
        if name is not None and len(name):
            self.name_entry.set_text(name)
        last_occur = self.scheduled.get_last_occur_date()
        if last_occur is not None:
            date_buf = print_datetime(last_occur)
            self.last_occur_label.set_text(date_buf)
        else:
            self.last_occur_label.set_text("(never)")
        end_date = self.scheduled.get_end_date()

        if end_date is not None:
            self.end_on_date_opt.set_active(True)
            self.end_date_entry.set_date(end_date)
            self.set_end_group_toggle_states(EndType.DATE)
        elif self.scheduled.has_occur_def():
            num_occur = self.scheduled.get_num_occur()
            num_remain = self.scheduled.get_rem_occur()
            self.end_after_opt.set_active(True)
            self.end_spin.set_value(num_occur)
            self.remain_spin.set_value(num_remain)
            self.set_end_group_toggle_states(EndType.OCCUR)
        else:
            self.never_end_opt.set_active(True)
            self.set_end_group_toggle_states(EndType.NEVER)
        enabled_state = self.scheduled.get_enabled()
        self.enabled_opt.set_active(enabled_state)
        if self.new_scheduled:
            auto_create_state = gs_pref_get_bool(PREFS_GROUP_SXED, PREF_CREATE_AUTO)
            notify_state = gs_pref_get_bool(PREFS_GROUP_SXED, PREF_NOTIFY)
        else:
            auto_create_state, notify_state = self.scheduled.get_auto_create()
        self.auto_create_opt.set_active(auto_create_state)
        if not auto_create_state:
            notify_state = False
        self.notify_opt.set_active(notify_state)

        if self.new_scheduled:
            days_in_advance = gs_pref_get_float(PREFS_GROUP_SXED, PREF_CREATE_DAYS)
        else:
            days_in_advance = self.scheduled.get_advance_creation()

        if days_in_advance != 0:
            self.advance_opt.set_active(True)
            self.advance_days.set_value(days_in_advance)
        if self.new_scheduled:
            days_in_advance = gs_pref_get_float(PREFS_GROUP_SXED, PREF_REMIND_DAYS)
        else:
            days_in_advance = self.scheduled.get_advance_reminder()

        if days_in_advance != 0:
            self.remind_opt.set_active(True)
            self.remind_days.set_value(days_in_advance)
        if self.new_scheduled:
            self.scheduled.set_instance_count(1)
        split_list = self.scheduled.get_splits()
        if any(split_list):
            model = self.ledger.get_model()
            model.load(split_list, None)
        self.update_dense_calendar()

    def set_end_group_toggle_states(self, _type):
        self.end_date_entry.set_sensitive(_type == EndType.DATE)
        self.end_spin.set_sensitive(_type == EndType.OCCUR)
        self.remain_spin.set_sensitive(_type == EndType.OCCUR)

    @Gtk.Template.Callback()
    def end_group_toggled_cb(self, widget):
        if widget == self.never_end_opt:
            self.set_end_group_toggle_states(EndType.NEVER)
        elif widget == self.end_on_date_opt:
            self.set_end_group_toggle_states(EndType.DATE)
        elif widget == self.end_after_opt:
            self.set_end_group_toggle_states(EndType.OCCUR)
        self.update_dense_calendar()

    def reg_check_close(self):
        message = "The current template transaction has been changed. " \
                  "Would you like to record the changes?"
        view = self.ledger.get_view()

        dirty_trans = view.get_dirty_trans()
        if dirty_trans is None:
            return

        if ask_yes_no(self, True, message):
            dirty_trans.commit_edit()
            view.set_dirty_trans(None)
            return
        else:
            view.cancel_edit(True)

    @classmethod
    def component_scheduled_equality(cls, find_data, user_data):
        return find_data[0] == user_data[0].scheduled

    def update_dense_calendar(self):
        recurrences = []
        start_date = self.frequency.save_to_recurrence(recurrences)
        start_date -= relativedelta(days=1)
        first_date = Recurrence.list_next_instance(recurrences, start_date)
        last_occur = self.scheduled.get_last_occur_date()
        if last_occur is not None and first_date is not None and last_occur != first_date:
            start_date = last_occur
            first_date = Recurrence.list_next_instance(recurrences, start_date)
        else:
            start_date -= relativedelta(days=1)
        if first_date is None:
            self.dense_calendar_model.clear()
            return
        self.dense_calendar_model.update_name(self.scheduled.get_name())
        schedule_desc = Recurrence.list_to_compact_string(recurrences)
        self.dense_calendar_model.update_info(schedule_desc)

        if self.end_on_date_opt.get_active():
            end_date = self.end_date_entry.get_date()
            self.dense_calendar_model.update_recurrences_date_end(start_date, recurrences, end_date)
        elif self.never_end_opt.get_active():
            self.dense_calendar_model.update_recurrences_no_end(start_date, recurrences)
        elif self.end_after_opt.get_active():
            num_remain = self.remain_spin.get_value_as_int()
            self.dense_calendar_model.update_recurrences_count_end(start_date, recurrences, num_remain)

    def frequency_changed_cb(self, *args):
        self.update_dense_calendar()

    def occurrences_update_cb(self, *args):
        self.update_dense_calendar()

    @staticmethod
    def on_scheduled_check_toggled_cb(togglebutton, self):
        table = self.pref_hash
        widget_notify = table.get("pref/" + PREFS_GROUP_SXED + "/" + PREF_NOTIFY)
        if togglebutton.get_active():
            widget_notify.set_sensitive(True)
        else:
            widget_notify.set_sensitive(False)

    @classmethod
    def event_handler(cls, ent, event_type, *evt_data):
        from gasstation.views.main_window import MainWindow
        if not event_type & EVENT_DESTROY:
            return
        if not isinstance(ent, Account):
            return
        acct = ent
        book = acct.get_book()
        affected_schedules = ScheduledTransactionList.get_all_referencing_account(book, acct)
        if len(affected_schedules) == 0:
            return
        name_list = Gtk.ListStore(str)
        for scheduled in affected_schedules:
            name_list.append([scheduled.get_name()])

        parent = MainWindow.get_main_window(None)
        dialog = ScheduledAccountDeletionDialog(name_list)
        dialog.set_transient_for(parent)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            dialog.hide()
            for scheduled in affected_schedules:
                cls.create(parent, scheduled, False)
            dialog.destroy()

    @classmethod
    def initialise(cls):
        Event.register_handler(cls.event_handler)
        Hook.add_dangler(HOOK_BOOK_OPENED, ScheduledSinceLastRunDialog.book_opened)
        PreferencesDialog.add_page("scheduled/preferences.ui", "create_days_adj,remind_days_adj,ScheduledPreferencesGrid",
                                   "Scheduled Transactions", cls)


GObject.type_register(ScheduledTransactionEditorDialog)
