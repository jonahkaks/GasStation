from gasstation.models.scheduled_instance_model import *
from gasstation.views.register.register import *

DIALOG_SX_SINCE_LAST_RUN_CM_CLASS = "dialog-scheduled-since-last-run"
PREFS_GROUP_STARTUP = "dialogs.schedules.since-last-run"
PREF_RUN_AT_FOPEN = "show-at-file-open"
PREF_SHOW_AT_FOPEN = "show-notify-window-at-file-open"
instance_state_names = [
    "Ignored",
    "Postponed",
    "To-Create",
    "Reminder",
    "Created"]


class ScheduledSinceLastRunColumn(GObject.GEnum):
    NAME = 0
    INSTANCE_STATE = 1
    VARAIBLE_VALUE = 2
    INSTANCE_VISIBILITY = 3
    VARIABLE_VISIBILITY = 4
    INSTANCE_STATE_SENSITIVITY = 5


class ScheduledSinceLastRunTreeModelAdapter(GObject.Object):
    state_model = None
    __gtype_name__ = "ScheduledSinceLastRunTreeModelAdapter"

    def __init__(self, instances):
        super().__init__()
        self.updated_cb_id = None
        self.disposed = False
        self.instances = None
        self.real = Gtk.TreeStore.new([str, str, str, bool, bool, bool])
        self.instances = instances
        self.populate_tree_store()
        self.instances.connect("added", self.added_cb)
        self.updated_cb_id = self.instances.connect("updated", self.updated_cb)
        self.instances.connect("removing", self.removing_cb)

    @classmethod
    def get_state_model(cls):
        if cls.state_model is None:
            cls.state_model = Gtk.ListStore(str)
            for i, n in enumerate(instance_state_names):
                if i == ScheduledInstanceState.CREATED:
                    continue
                cls.state_model.append([n])
        return cls.state_model

    @staticmethod
    def _consume_excess_rows(model, last_index, parent_iter, maybe_invalid_iter):
        if last_index == -1:
            maybe_invalid_iter = model.iter_children(parent_iter)
            if maybe_invalid_iter is None:
                return
        else:
            if model.iter_next(maybe_invalid_iter) is None:
                return
        while model.remove(maybe_invalid_iter):
            pass

    def populate_tree_store(self):
        instances_index = -1
        scheduled_tree_iter = None
        for instances_index, instances in enumerate(self.instances.instance_list):
            if not any(instances.instance_list):
                continue
            scheduled_tree_iter = self.real.iter_nth_child(None, instances_index)
            if scheduled_tree_iter is None:
                scheduled_tree_iter = self.real.append(None)
            self.real.set(scheduled_tree_iter,
                          [ScheduledSinceLastRunColumn.NAME, ScheduledSinceLastRunColumn.INSTANCE_STATE,
                           ScheduledSinceLastRunColumn.VARAIBLE_VALUE,
                           ScheduledSinceLastRunColumn.INSTANCE_VISIBILITY,
                           ScheduledSinceLastRunColumn.VARIABLE_VISIBILITY,
                           ScheduledSinceLastRunColumn.INSTANCE_STATE_SENSITIVITY],
                          [instances.scheduled.get_name(), None, None, False, False, False])
            inst_tree_iter = None
            instance_index = -1
            for instance_index, inst in enumerate(instances.instance_list):
                instance_date_buf = print_datetime(inst.date)
                inst_tree_iter = self.real.iter_nth_child(scheduled_tree_iter, instance_index)
                if inst_tree_iter is None:
                    inst_tree_iter = self.real.append(scheduled_tree_iter)
                self.real.set(inst_tree_iter,
                              [ScheduledSinceLastRunColumn.NAME, ScheduledSinceLastRunColumn.INSTANCE_STATE,
                               ScheduledSinceLastRunColumn.VARAIBLE_VALUE,
                               ScheduledSinceLastRunColumn.INSTANCE_VISIBILITY,
                               ScheduledSinceLastRunColumn.VARIABLE_VISIBILITY,
                               ScheduledSinceLastRunColumn.INSTANCE_STATE_SENSITIVITY],
                              [instance_date_buf, instance_state_names[inst.state], None, True, False,
                               inst.state != ScheduledInstanceState.CREATED])

                # self._consume_excess_rows(self.real, instances_index, None, scheduled_tree_iter)
            self._consume_excess_rows(self.real, instance_index, scheduled_tree_iter, inst_tree_iter)
        self._consume_excess_rows(self.real, instances_index, None, scheduled_tree_iter)

    def get_instance_model(self):
        return self.instances

    def get_scheduled_instances(self, _iter):
        return self._get_scheduled_instances(_iter, True)

    def _get_scheduled_instances(self, _iter, check_depth):
        path = self.get_path(_iter)
        if check_depth and path.get_depth() != 1:
            return None
        indices = path.get_indices()
        index = indices[0]
        return self.instances.instance_list[index]

    def get_instance(self, iter):
        return self._get_instance(iter, True)

    def _get_instance(self, _iter, check_depth):
        path = self.get_path(_iter)
        if check_depth and path.get_depth() != 2:
            return None
        indices = path.get_indices()
        instances_index = indices[0]
        instance_index = indices[1]
        instances = self.instances.instance_list[instances_index]
        if instance_index < 0 or instance_index >= len(instances.instance_list):
            return None

        return instances.instance_list[instance_index]

    def get_instance_and_variable(self, _iter):
        var = None
        instance = self._get_instance(_iter, False)
        if instance is None:
            return False
        variables = instance.get_variables()
        path = self.get_path(_iter)
        if path.get_depth(path) != 3:
            return False
        indices = path.get_indices()
        variable_index = indices[2]
        if variable_index < 0 or variable_index >= len(variables):
            return False

        for var in variables:
            if not var.editable:
                continue
            if variable_index - 1 == 0:
                break
        return instance, var

    @staticmethod
    def _variable_list_index(variables, variable):
        index = 0
        for var in variables:
            if not var.editable:
                continue
            if variable == var:
                return index
            index += 1
        return -1

    def _get_path_for_variable(self, instance, variable):
        indices = []
        try:
            indices.append(self.instances.instance_list.index(instance.parent))
            indices.append(instance.parent.instance_list.index(instance))
            variables = instance.get_variables()
            indices.append(self._variable_list_index(variables, variable))
        except ValueError:
            return None
        path = Gtk.TreePath.new_from_indices(indices)
        return path

    def added_cb(self, instances, added_scheduled):
        self.populate_tree_store()

    def updated_cb(self, instances, updated_scheduled):
        instances.update_scheduled_instances(updated_scheduled)
        self.populate_tree_store()

    def removing_cb(self, instances, to_remove_scheduled):
        ins = None
        index = 0
        for index, ins in enumerate(instances.instance_list):
            if ins.scheduled == to_remove_scheduled:
                break
        if ins is None:
            return
        tree_iter = self.real.iter_nth_child(None, index)
        if tree_iter is None:
            return
        self.real.remove(tree_iter)
        instances.remove_scheduled_instances(to_remove_scheduled)


GObject.type_register(ScheduledSinceLastRunTreeModelAdapter)


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/scheduled/since_last_run.ui')
class ScheduledSinceLastRunDialog(Gtk.Dialog):
    __gtype_name__ = "ScheduledSinceLastRunDialog"
    instance_view: Gtk.TreeView = Gtk.Template.Child()
    review_created_transactions_toggle: Gtk.ToggleButton = Gtk.Template.Child()

    def __init__(self, parent, scheduled_instances, auto_created_transaction_guids):
        super().__init__()
        self.set_transient_for(parent)
        gs_widget_set_style_context(self, "ScheduledSinceLastRunDialog")

        self.editing_model = ScheduledSinceLastRunTreeModelAdapter(scheduled_instances)
        self.created_transactions = auto_created_transaction_guids
        self.instance_view.set_model(self.editing_model.real)

        renderer = Gtk.CellRendererText()
        col = Gtk.TreeViewColumn("Transaction", renderer, text=ScheduledSinceLastRunColumn.NAME)
        self.instance_view.append_column(col)

        renderer = Gtk.CellRendererCombo()
        renderer.set_properties(model=ScheduledSinceLastRunTreeModelAdapter.get_state_model(),
                                text_column=0, has_entry=False, editable=True)

        renderer.connect("edited", self.instance_state_changed_cb)
        col = Gtk.TreeViewColumn("Status", renderer, text=ScheduledSinceLastRunColumn.INSTANCE_STATE,
                                 visible=ScheduledSinceLastRunColumn.INSTANCE_VISIBILITY,
                                 editable=ScheduledSinceLastRunColumn.INSTANCE_STATE_SENSITIVITY,
                                 sensitive=ScheduledSinceLastRunColumn.INSTANCE_STATE_SENSITIVITY)
        self.instance_view.append_column(col)
        col.set_resizable(True)

        renderer = Gtk.CellRendererText()
        renderer.set_property("editable", True)
        renderer.connect("edited", self.variable_value_changed_cb)
        col = Gtk.TreeViewColumn("Value", renderer, text=ScheduledSinceLastRunColumn.VARAIBLE_VALUE,
                                 visible=ScheduledSinceLastRunColumn.VARIABLE_VISIBILITY)
        self.instance_view.append_column(col)
        self.instance_view.expand_all()

        self.instance_view.set_grid_lines(gs_tree_view_get_grid_lines_pref())
        self.connect("response", self.response_cb)
        self.connect("destroy", self.destroy_cb)
        gs_restore_window_size(PREFS_GROUP_STARTUP, self, parent)
        self.component_id = ComponentManager.register(DIALOG_SX_SINCE_LAST_RUN_CM_CLASS, None, self.close_handler)
        ComponentManager.set_session(self.component_id, Session.get_current_session())

    @classmethod
    def creation_error_dialog(cls, creation_errors):
        if creation_errors is None or len(creation_errors) == 0:
            return
        message = "\n".join(creation_errors)
        dialog = Gtk.MessageDialog(transient_for=None, modal=True,
                                   destroy_with_parent=True,
                                   message_type=Gtk.MessageType.ERROR,
                                   buttons=Gtk.ButtonsType.CLOSE,
                                   message_format="Invalid Transactions")
        dialog.format_secondary_text(message)
        dialog.connect("response", dialog.destroy)
        dialog.run()

    @classmethod
    def book_opened(cls, session):
        from gasstation.views.main_window import MainWindow
        auto_created_transactions = []
        creation_errors = []
        summary = Summary()
        if not gs_pref_get_bool(PREFS_GROUP_STARTUP, PREF_RUN_AT_FOPEN):
            return
        if session.get_current_book().is_readonly():
            return
        inst_model = ScheduledInstanceModel.get_current_instances()
        inst_model.summarize(summary)
        inst_model.effect_change(True, auto_created_transactions, creation_errors)

        if summary.need_dialog:
            dialog = cls(MainWindow.get_main_window(None), inst_model, auto_created_transactions)
            dialog.run()
        else:
            if summary.num_auto_create_no_notify_instances != 0:
                if not gs_pref_get_bool(PREFS_GROUP_STARTUP, PREF_SHOW_AT_FOPEN):
                    return
                show_info(MainWindow.get_main_window(None),
                          "There are no Scheduled Transactions to be entered at this"
                          " time. (%d transactions automatically created)" %
                          summary.num_auto_create_no_notify_instances)
        if any(creation_errors):
            cls.creation_error_dialog(creation_errors)

    def instance_state_changed_cb(self, cell, path, value):
        i = 0
        for i in range(ScheduledInstanceState.CREATED):
            if value == instance_state_names[i]:
                break
        if i == ScheduledInstanceState.CREATED:
            return
        new_state = i

        tree_iter = self.editing_model.get_iter_from_string(path)
        if tree_iter is None:
            return
        inst = self.editing_model.get_instance(tree_iter)
        if inst is None:
            return
        self.editing_model.instances.change_instance_state(inst, new_state)

    def variable_value_changed_cb(self, cell, path, value):
        # if (!gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(self.editing_model), &tree_iter, path)) {
        # g_warning("invalid path [%s]", path)
        # return
        # }
        #
        # if (!self.editing_model.get_instance_and_variable(self.editing_model, &tree_iter, &inst, &var)) {
        # g_critical("path [%s] doesn't correspond to a valid variable", path)
        # return
        # }
        #
        # if (!xaccParseAmount(value, True, &parsed_num, &endStr)
        #     || gnc_numeric_check(parsed_num) != ERROR_OK) {
        #     gchar *value_copy = g_strdup(value)
        #     g_debug("value=[%s] endStr[%s]", value, endStr)
        #     if (strlen(g_strstrip(value_copy)) == 0) {
        #     gnc_numeric invalid_num = gnc_numeric_error(ERROR_ARG)
        #     gnc_scheduled_instance_model_set_variable(self.editing_model->instances, inst, var, &invalid_num)
        #     } else {
        #     g_warning("error parsing value [%s]", value)
        #     }
        #     g_free(value_copy)
        #     return
        # }
        # gnc_scheduled_instance_model_set_variable(self.editing_model->instances, inst, var, &parsed_num)
        # }
        pass

    def _show_created_transactions(self, created_transaction_guids):
        from gasstation.views.main_window import MainWindow
        from gasstation.plugins.pages import RegisterPluginPage
        book_query = Query.create_for(ID_SPLIT)
        guid_query = Query.create_for(ID_SPLIT)
        book_query.set_book(Session.get_current_book())
        for tr in created_transaction_guids:
            guid_query.add_guid_match(tr, ID_TRANS, QueryOp.OR)
        query = book_query.merge(guid_query, QueryOp.AND)
        ledger = RegisterLedger.query(query, RegisterModelType.SEARCH_LEDGER, RegisterModelStyle.JOURNAL)
        ledger.refresh()
        page = RegisterPluginPage.new_ledger(ledger)
        page.set_name("Created Transactions")
        MainWindow.get_main_window(None).open_page(page)
        query.destroy()
        book_query.destroy()
        guid_query.destroy()

    def close_handler(self):
        gs_save_window_size(PREFS_GROUP_STARTUP, self)
        self.destroy()

    def destroy_cb(self, *args):
        ComponentManager.unregister(self.component_id)
        self.editing_model = None

    def response_cb(self, dialog, response_id):
        self.created_transactions = []
        creation_errors = []
        if response_id == Gtk.ResponseType.OK:
            unbound_variables = self.editing_model.instances.check_variables()
            if len(unbound_variables) > 0:
                variable_view_column = 2
                start_editing = True
                first_unbound = unbound_variables[0]
                variable_path = self.editing_model._get_path_for_variable(first_unbound.instance,
                                                                          first_unbound.variable)
                variable_col = self.instance_view.get_column(self.instance_view, variable_view_column)

                self.instance_view.set_cursor(self.instance_view, variable_path, variable_col, start_editing)
                return
            ComponentManager.suspend()
            self.effect_change(self.editing_model, False, self.created_transactions, creation_errors)
            ComponentManager.resume()
            ComponentManager.refresh_all()
            if any(creation_errors):
                self.creation_error_dialog(creation_errors)

            if self.review_created_transactions_toggle.get_active() and self.created_transactions is not None and len(
                    self.created_transactions) > 0:
                self._show_created_transactions(self.created_transactions)
            ComponentManager.close(self.component_id)

        elif response_id == Gtk.ResponseType.CANCEL or response_id == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)

    @staticmethod
    def effect_change(model, auto_create_only, created_transaction_guids, creation_errors):
        if Session.get_current_book().is_readonly():
            return
        model.instances.handler_block(model.updated_cb_id)
        model.instances.effect_change(auto_create_only, created_transaction_guids, creation_errors)
        model.instances.handler_unblock(model.updated_cb_id)


GObject.type_register(ScheduledSinceLastRunDialog)
