from gi.repository import GObject

from gasstation.utilities.cursors import *
from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import *
from gasstation.utilities.file_history import FileHistory
from gasstation.utilities.keyring import *
from gasstation.utilities.print_settings import PrintOperation
from gasstation.utilities.state import *
from gasstation.utilities.ui import *
from gasstation.views.dialogs.password import PassWordDialog
from gasstation.views.window import *
from libgasstation.core.component import ComponentManager
from libgasstation.core.dafault_data import default_data
from libgasstation.core.product import *
from libgasstation.core.session import *

RESPONSE_NEW = 1
RESPONSE_OPEN = 2
RESPONSE_QUIT = 3
RESPONSE_READONLY = 4


class FileDialogType(GObject.GEnum):
    OPEN = 0
    IMPORT = 1
    SAVE = 2
    EXPORT = 3


class File:
    save_in_progres = 0
    been_here_before = False
    shutdown_cb = None

    @staticmethod
    def dialog(parent, title, filters, starting_dir, _type):
        ok_icon = None
        file_name = None

        if _type == FileDialogType.OPEN:
            action = Gtk.FileChooserAction.OPEN
            okbutton = "_Open"
            if title is None:
                title = "Open"

        elif _type == FileDialogType.IMPORT:
            action = Gtk.FileChooserAction.OPEN
            okbutton = "_Import"
            if title is None:
                title = "Import"

        elif _type == FileDialogType.SAVE:
            action = Gtk.FileChooserAction.SAVE
            okbutton = "_Save"
            if title is None:
                title = "Save"

        elif _type == FileDialogType.EXPORT:
            action = Gtk.FileChooserAction.SAVE
            okbutton = "_Export"
            ok_icon = "go-next"
            if title is None:
                title = "Export"
        else:
            return
        file_box = Gtk.FileChooserDialog(title, parent, action, ("_Cancel",
                                                                 Gtk.ResponseType.CANCEL))
        if ok_icon is not None:
            gs_dialog_add_button(file_box, okbutton, ok_icon, Gtk.ResponseType.ACCEPT)
        else:
            file_box.add_button(okbutton, Gtk.ResponseType.ACCEPT)

        if starting_dir:
            file_box.set_current_folder(starting_dir)
        file_box.set_modal(True)

        if filters is not None:
            all_filter = Gtk.FileFilter()
            for f in filters:
                file_box.add_filter(f)
            all_filter.set_name("All files")
            all_filter.add_pattern("*")
            file_box.add_filter(all_filter)
            file_box.set_filter(filters)
        response = file_box.run()
        gs_widget_set_style_context(file_box, "FileDialog")

        if response == Gtk.ResponseType.ACCEPT:
            internal_name = file_box.get_uri()
            if internal_name is not None:
                if internal_name.find("file://") == 0:
                    internal_name = file_box.get_filename()
            file_name = internal_name
        file_box.destroy()
        return file_name

    @staticmethod
    def show_session_error(parent, io_error, newfile, _type):
        uh_oh = True
        if newfile is None or not len(newfile):
            displayname = "none"
        else:
            newfile = Uri(newfile)
            displayname = newfile.get_path() if newfile.is_file() else str(newfile)

        if io_error == BackEndError.NO_ERR:
            return False

        elif io_error == BackEndError.NO_HANDLER:
            fmt = "No suitable backend was found for %s."
            show_error(parent, fmt % displayname)

        elif io_error == BackEndError.NO_BACKEND:
            fmt = "The URL %s is not supported by this version of GasStation."
            show_error(parent, fmt % displayname)

        elif io_error == BackEndError.BAD_URL and displayname is not None:
            fmt = "Can't parse the URL %s."
            show_error(parent, fmt % displayname)

        elif io_error == BackEndError.CANT_CONNECT:
            fmt = "Can't connect to %s. The host, username or password were incorrect."
            show_error(parent, fmt % displayname)

        elif io_error == BackEndError.CONN_LOST:
            fmt = "Can't connect to %s. "
            "Connection was lost, unable to send data."
            show_error(parent, fmt % displayname)

        elif io_error == BackEndError.TOO_NEW:
            fmt = "This file URL appears to be from a newer version "
            "of GasStation. You must upgrade your version of GasStation "
            "to work with this data."
            show_error(parent, fmt)

        elif io_error == BackEndError.NO_SUCH_DB:
            fmt = "The database %s doesn't seem to exist. "
            "Do you want to create it?"
            if ask_yes_no(parent, True, fmt % displayname):
                uh_oh = False

        elif io_error == BackEndError.LOCKED:

            if _type == FileDialogType.IMPORT:
                label = "Import"
                fmt = "GasStation could not obtain the lock for %s. "
                "That database may be in use by another user, "
                "in which case you should not import the database. "
                "Do you want to proceed with importing the database?"

            elif _type == FileDialogType.SAVE:
                label = "Save"
                fmt = "GasStation could not obtain the lock for %s. "
                "That database may be in use by another user, "
                "in which case you should not save the database. "
                "Do you want to proceed with saving the database?"

            elif _type == FileDialogType.EXPORT:
                label = "Export"
                fmt = "GasStation could not obtain the lock for %s. "
                "That database may be in use by another user, "
                "in which case you should not export the database. "
                "Do you want to proceed with exporting the database?"

            else:
                label = "Open"
                fmt = "GasStation could not obtain the lock for %s. "
                "That database may be in use by another user, "
                "in which case you should not open the database. "
                "Do you want to proceed with opening the database?"
            dialog = Gtk.MessageDialog(transient_for=parent, destroy_with_parent=True,
                                       message_type=Gtk.MessageType.QUESTION,
                                       buttons=Gtk.ButtonsType.NONE,
                                       text=fmt % displayname)
            dialog.add_buttons("_Cancel", Gtk.ResponseType.CANCEL, label, Gtk.ResponseType.YES)
            if parent is None:
                dialog.set_skip_taskbar_hint(False)
            response = dialog.run()
            dialog.destroy()
            uh_oh = (response != Gtk.ResponseType.YES)

        elif io_error == BackEndError.READONLY:
            fmt = "GasStation could not write to %s. "
            "That database may be on a read-only file system, "
            "you may not have write permission for the directory "
            "or your anti-virus software is preventing this action."
            show_error(parent, fmt % displayname)

        elif io_error == BackEndError.DATA_CORRUPT:
            fmt = "The file/URL %s "
            "does not contain GasStation data or the data is corrupt."
            show_error(parent, fmt % displayname)

        elif io_error == BackEndError.SERVER_ERR:
            fmt = "The server at URL %s " \
                  "experienced an error or encountered bad or corrupt data."
            show_error(parent, fmt % displayname)

        elif io_error == BackEndError.PERM:
            fmt = "You do not have permission to access %s."
            show_error(parent, fmt % displayname)

        elif io_error == BackEndError.MISC:
            fmt = "An error occurred while processing %s."
            show_error(parent, fmt % displayname)

        elif io_error == FileError.FILE_BAD_READ:
            fmt = "There was an error reading the file. "
            "Do you want to continue?"
            if ask_yes_no(parent, True, "%s", fmt):
                uh_oh = False

        elif io_error == FileError.PARSE_ERROR:
            fmt = "There was an error parsing the file %s."
            show_error(parent, fmt % displayname)

        elif io_error == FileError.FILE_EMPTY:
            fmt = "The file %s is empty."
            show_error(parent, fmt % displayname)

        elif io_error == FileError.FILE_NOT_FOUND:
            if _type == FileDialogType.SAVE:
                uh_oh = False
            else:
                if FileHistory.test_for_file(displayname):
                    fmt = "The file URI %s could not be found.\n\nThe file is in the history list, do you want to " \
                          "remove it? "
                    if ask_yes_no(parent, False, fmt % displayname):
                        FileHistory.remove_file(displayname)
                else:
                    fmt = "The file URI %s could not be found."
                    show_error(parent, fmt % displayname)

        elif io_error == FileError.FILE_TOO_OLD:
            fmt = "This file is from an older version of GasStation. "
            "Do you want to continue?"
            if ask_yes_no(parent, True, "%s" % fmt):
                uh_oh = False

        elif io_error == FileError.UNKNOWN_FILE_TYPE:
            fmt = "The file type of file %s is unknown."
            show_error(parent, fmt % displayname)

        elif io_error == FileError.BACKUP_ERROR:
            fmt = "Could not make a backup of the file %s"
            show_error(parent, fmt % displayname)

        elif io_error == FileError.WRITE_ERROR:
            fmt = "Could not write to file %s. Check that you have "
            "permission to write to this file and that "
            "there is sufficient space to create it."
            show_error(parent, fmt % displayname)

        elif io_error == FileError.FILE_EACCES:
            fmt = "No read permission to read from file %s."
            show_error(parent, fmt % displayname)

        elif io_error == FileError.RESERVED_WRITE:
            fmt = "You attempted to save in\n%s\nor a subdirectory thereof. "
            "This is not allowed as %s reserves that directory for internal use.\n\n"
            "Please try again in a different directory."
            # show_error (parent, fmt, gs_userdata_dir(), PACKAGE_NAME)

        elif io_error == SqlError.DB_TOO_OLD:
            fmt = "This database is from an older version of GasStation. "
            "Select OK to upgrade it to the current version, Cancel "
            "to mark it read-only."

            response = ask_ok_cancel(parent, Gtk.ResponseType.CANCEL, fmt)
            uh_oh = (response == Gtk.ResponseType.CANCEL)

        elif io_error == SqlError.DB_TOO_NEW:
            fmt = "This database is from a newer version of GasStation. "
            "This version can read it, but cannot safely save to it. "
            "It will be marked read-only until you do File>Save As, "
            "but data may be lost in writing to the old version."
            show_warning(parent, fmt)
            uh_oh = True

        elif io_error == SqlError.DB_BUSY:
            fmt = "The SQL database is in use by other users, "
            "and the upgrade cannot be performed until they logoff. "
            "If there are currently no other users, consult the  "
            "documentation to learn how to clear out dangling login "
            "sessions."
            show_error(parent, fmt)

        elif io_error == SqlError.BAD_DBI:
            fmt = "The library \"libdbi\" installed on your system doesn't correctly "
            "store large numbers. This means GasStation cannot use SQL databases "
            "correctly. Gnucash will not open or save to SQL databases until this is "
            "fixed by installing a different version of \"libdbi\". Please see "
            "https://bugs.gnucash.org/show_bug.cgi?id=611936 for more "
            "information."

            show_error(parent, fmt)

        elif io_error == SqlError.DBI_UNTESTABLE:

            fmt = "GasStation could not complete a critical test for the presence of "
            "a bug in the \"libdbi\" library. This may be caused by a "
            "permissions misconfiguration of your SQL database. Please see "
            "https://bugs.gnucash.org/show_bug.cgi?id=645216 for more "
            "information."
            show_error(parent, fmt)

        elif io_error == FileError.FILE_UPGRADE:
            fmt = "This file is from an older version of GasStation and will be "
            "upgraded when saved by this version. You will not be able "
            "to read the saved file from the older version of Gnucash "
            "(it will report an \"error parsing the file\"). If you wish "
            "to preserve the old version, exit without saving."
            show_warning(parent, fmt)
            uh_oh = False

        else:
            fmt = "An unknown I/O error (%d) occurred."
            show_error(parent, fmt % io_error)

        return uh_oh

    @staticmethod
    def add_history(session):
        if session is None:
            return
        url = session.get_url()
        if url is None:
            return
        FileHistory.add_file(url.get_path() if url.is_file() else str(url))

    @staticmethod
    def new(parent):
        if not File.query_save(parent, True):
            return
        if Session.current_session_exist():
            session = Session.get_current_session()
            Event.suspend()
            Hook.run(HOOK_BOOK_CLOSED, session)
            ComponentManager.close_by_session(session)
            State.save(session)
            PrintOperation.save(session)
            Session.clear_current_session()
            Event.resume()
        Session.get_current_session()
        Hook.run(HOOK_NEW_BOOK, parent)
        ComponentManager.refresh_all()
        File.book_opened()

    @staticmethod
    def book_opened():
        Hook.run(HOOK_BOOK_OPENED, Session.get_current_session())

    @staticmethod
    def open(parent):
        last = FileHistory.get_last()
        if not File.query_save(parent, True):
            return False
        default_dir = gs_get_default_directory(PREFS_GROUP_OPEN_SAVE)
        if last is not None:
            filepath = Uri(last).get_path()
            default_dir = GLib.path_get_dirname(filepath)
        newfile = File.dialog(parent, "Open", None, default_dir, FileDialogType.OPEN)
        return File.post_open(parent, newfile, False)

    @staticmethod
    def open_file(parent, newfile, open_readonly):
        if newfile is None:
            return False
        if not File.query_save(parent, True):
            return False
        return File.post_open(parent, newfile, open_readonly)

    @staticmethod
    def query_save(parent, can_cancel):
        from gasstation.utilities.auto_save import AutoSave
        import datetime
        if not Session.current_session_exist():
            return True
        current_book = Session.get_current_book()
        AutoSave.remove_timer(current_book)

        while current_book is not None and current_book.session_not_saved():
            title = "Save changes to the file?"
            dialog = Gtk.MessageDialog(parent, destroy_with_parent=True,
                                       message_type=Gtk.MessageType.QUESTION,
                                       buttons=Gtk.ButtonsType.NONE, title=title)
            oldest_change = current_book.get_session_dirty_time()

            minutes = (datetime.datetime.now().second - oldest_change.second) / 60 + 1
            dialog.format_secondary_text(
                "If you don't save, changes from the past %d minute will be discarded." % minutes)
            dialog.add_button("Continue _Without Saving", Gtk.ResponseType.OK)
            if can_cancel:
                dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
            dialog.add_button("_Save", Gtk.ResponseType.YES)

            dialog.set_default_response(Gtk.ResponseType.YES)

            response = dialog.run()
            dialog.destroy()

            if response == Gtk.ResponseType.YES:
                File.save(parent)
                pass
            elif response == Gtk.ResponseType.OK:
                return True
            else:
                if can_cancel:
                    return False
        return True

    @staticmethod
    def post_open(parent, filename, is_readonly):
        if filename is None or not len(filename):
            File.show_session_error(parent, FileError.FILE_NOT_FOUND, filename, FileDialogType.OPEN)
            return False
        newfile = Uri(filename)
        if not newfile.is_file():
            key_ring = KeyRing(newfile.scheme, newfile.hostname,
                               newfile.port, newfile.username)
        if not newfile.is_file() and newfile.get_password() is None:
            try:
                password = key_ring.get_password()
            except GLib.GError:
                password = None
            if password is None:
                dialog = PassWordDialog(parent, username=newfile.username)
                response = dialog.run()
                if response == Gtk.ResponseType.ACCEPT:
                    password = dialog.get_password()
                dialog.destroy()
                if password is None:
                    return False
            newfile.set_password(password)
        if newfile.is_file():
            default_dir = GLib.path_get_dirname(newfile.get_path())
            gs_set_default_directory(PREFS_GROUP_OPEN_SAVE, default_dir)
        Event.suspend()
        gs_set_busy_cursor(None, True)
        if Session.current_session_exist():
            current_session = Session.get_current_session()
            Hook.run(HOOK_BOOK_CLOSED, current_session)
            ComponentManager.close_by_session(current_session)
            State.save(current_session)
            PrintOperation.save(current_session)
            Session.clear_current_session()
        new_session = Session(None)
        new_session.begin(newfile, SessionOpenMode.READ_ONLY if is_readonly else SessionOpenMode.NORMAL_OPEN)

        io_err = new_session.get_error()

        if BackEndError.BAD_URL == io_err:
            File.show_session_error(parent, io_err, str(newfile), FileDialogType.OPEN)
            if GLib.file_test(filename, GLib.FileTest.IS_DIR):
                directory = filename
            else:
                directory = gs_get_default_directory(PREFS_GROUP_OPEN_SAVE)

            filename = File.dialog(parent, None, None, directory, FileDialogType.OPEN)
            new_session.destroy()
            new_session = None
            File.post_open(parent, filename, is_readonly)

        elif BackEndError.LOCKED == io_err or BackEndError.READONLY == io_err:
            fmt1 = "GasStation could not obtain the lock for %s."
            fmt2 = ("That database may be in use by another user,in which "
                    "case you should not open the database. What would you like to do?" if BackEndError.LOCKED == io_err else
                    "That database may be on a read-only file system, "
                    "you may not have write permission for the directory, "
                    "or your anti-virus software is preventing this action. "
                    "If you proceed you may not be able to save any changes. "
                    "What would you like to do?")

            displayname = newfile.get_path()
            dialog = Gtk.MessageDialog(parent, 0, Gtk.MessageType.WARNING, Gtk.ButtonsType.NONE, fmt1 % displayname)
            dialog.format_secondary_text(fmt2)
            dialog.set_skip_taskbar_hint(False)

            gs_dialog_add_button(dialog, "_Open Read-Only", "document-revert", RESPONSE_READONLY)
            gs_dialog_add_button(dialog, "_Create New File", "document-new", RESPONSE_NEW)
            gs_dialog_add_button(dialog, "Open _Anyway", "document-open", RESPONSE_OPEN)
            if File.shutdown_cb is not None:
                dialog.add_button("_Quit", RESPONSE_QUIT)
            rc = dialog.run()
            dialog.destroy()
            if rc == Gtk.ResponseType.DELETE_EVENT:
                rc = RESPONSE_QUIT if File.shutdown_cb is not None else RESPONSE_NEW
                if rc == RESPONSE_QUIT:
                    if File.shutdown_cb:
                        File.shutdown_cb(0)
                elif rc == RESPONSE_READONLY:
                    new_session.begin(newfile, SessionOpenMode.READ_ONLY)

                elif rc == RESPONSE_OPEN:
                    new_session.begin(newfile, SessionOpenMode.BREAK_LOCK)
                else:
                    File.new(parent)

        elif BackEndError.NO_SUCH_DB == io_err:
            if not File.show_session_error(parent, io_err, str(newfile), FileDialogType.OPEN):
                new_session.begin(newfile, SessionOpenMode.NEW_STORE)
        if new_session is None:
            return True
        io_err = new_session.get_error()
        if BackEndError.LOCKED == io_err or BackEndError.READONLY == io_err or BackEndError.NO_SUCH_DB == io_err:
            uh_oh = True
        else:
            uh_oh = File.show_session_error(parent, io_err, str(newfile), FileDialogType.OPEN)
        if not uh_oh:
            if not newfile.is_file():
                key_ring.set_username(newfile.username)
                key_ring.set_port(newfile.port)
                key_ring.set_scheme(newfile.scheme)
                key_ring.set_password(newfile.password)
            Session.set_current_session(new_session)
            Window.show_progress("Loading data...", 0.0)
            new_session.load(Window.show_progress)
            Window.show_progress(None, -1)
            if is_readonly:
                new_session.get_book().mark_read_only()
            io_err = new_session.pop_error()
            uh_oh = File.show_session_error(parent, io_err, str(newfile), FileDialogType.OPEN)
            if not uh_oh and io_err == SqlError.DB_TOO_OLD:
                Window.show_progress("Re-saving user data...", 0.0)
                new_session.safe_save(Window.show_progress)
                io_err = new_session.get_error()
                uh_oh = File.show_session_error(parent, io_err, str(newfile), FileDialogType.SAVE)

            if uh_oh and (io_err == SqlError.DB_TOO_OLD or io_err == SqlError.DB_TOO_NEW):
                new_session.get_book().mark_read_only()
                uh_oh = False
            new_root = new_session.get_book().get_root_account()
            if uh_oh:
                new_root = None
            if not uh_oh and new_root is None:
                uh_oh = File.show_session_error(parent, BackEndError.MISC, str(newfile), FileDialogType.OPEN)

        gs_unset_busy_cursor(None)
        if uh_oh:
            new_session.close()
            Session.get_current_session()
            Event.resume()
            return False
        Session.set_current_session(new_session)
        File.add_history(new_session)
        Event.resume()
        ComponentManager.refresh_all()

        File.book_opened()
        new_book = Session.get_current_book()
        invalid_account_names = Account.list_name_violations(new_book, Account.get_separator())
        if any(invalid_account_names):
            message = Account.name_violations_errmsg(Account.get_separator(), invalid_account_names)
            show_warning(parent, message)
        return True

    @staticmethod
    def export(parent):
        last = FileHistory.get_last()
        default_dir = gs_get_default_directory(PREFS_GROUP_EXPORT)
        if last is not None:
            last = Uri(last)
            if last.is_file():
                filepath = last.get_path()
                default_dir = GLib.path_get_dirname(filepath)
        filename = File.dialog(parent, "Save", None, default_dir, FileDialogType.SAVE)
        if not filename:
            return
        File.do_export(parent, filename)

    @staticmethod
    def check_file_path(path):
        d = GLib.path_get_dirname(path)
        ud = GLib.get_user_data_dir()
        if d == ud:
            return True
        return False

    @staticmethod
    def do_export(parent, filename):
        if filename is None:
            File.show_session_error(parent, FileError.FILE_NOT_FOUND, filename, FileDialogType.EXPORT)
            return
        newfile = Uri(filename)
        newfile.add_extension(".gas")
        if newfile.is_file():
            if File.check_file_path(newfile.get_path()):
                File.show_session_error(parent, FileError.RESERVED_WRITE, str(newfile), FileDialogType.SAVE)
                return
            gs_set_default_directory(PREFS_GROUP_OPEN_SAVE, GLib.path_get_dirname(newfile.get_path()))
        if Session.current_session_exist():
            current_session = Session.get_current_session()
            oldfile = current_session.get_url()
        else:

            return
        if len(oldfile) and oldfile == newfile:
            File.show_session_error(parent, FileError.WRITE_ERROR, filename, FileDialogType.EXPORT)
            return
        new_session = Session(None)
        new_session.begin(newfile, SessionOpenMode.NEW_STORE)
        io_err = new_session.get_error()

        if BackEndError.STORE_EXISTS == io_err:
            format = "The file %s already exists.Are you sure you want to overwrite it?"
            if newfile.is_file():
                name = newfile.get_path()
            else:
                name = str(newfile)

            if not ask_yes_no(parent, False, format % name, ""):
                return
            new_session.begin(newfile, SessionOpenMode.NEW_OVERITE)

        if BackEndError.LOCKED == io_err or BackEndError.READONLY == io_err:
            if not File.show_session_error(parent, io_err, str(newfile), FileDialogType.EXPORT):
                pass
                new_session.begin(newfile, SessionOpenMode.BREAK_LOCK)

        gs_set_busy_cursor(None, True)
        Window.show_progress("Exporting file...", 0.0)
        try:
            new_session.export(current_session, Window.show_progress)
        except Exception as e:
            show_error(parent, "There was an error saving the file.\n%s" % e.args[1])
        Window.show_progress(None, -1.0)
        gs_unset_busy_cursor(None)
        new_session.destroy()
        Event.resume()

    @staticmethod
    def save(parent):
        session = Session.get_current_session()
        url = session.get_url()
        if url is None or len(url) == 0:
            File.save_as(parent)
            return
        if session.get_current_book().is_readonly():
            response = ask_ok_cancel(parent, Gtk.ResponseType.CANCEL, "The database was opened read-only. "
                                                                      "Do you want to save it to a different place?")
            if response == Gtk.ResponseType.OK:
                File.save_as(parent)
            return
        File.save_in_progres += 1
        gs_set_busy_cursor(None, True)
        Window.show_progress("Writing file...", 0.0)
        session.save(Window.show_progress)
        Window.show_progress(None, -1.0)
        gs_unset_busy_cursor(None)
        File.save_in_progres -= 1
        io_err = session.get_error()
        if BackEndError.NO_ERR != io_err:
            newfile = session.get_url()
            File.show_session_error(parent, io_err, newfile, FileDialogType.SAVE)
            if File.been_here_before:
                return
            File.been_here_before = True
            File.save_as(parent)
            File.been_here_before = False
            return
        File.add_history(session)
        Hook.run(HOOK_BOOK_SAVED, session)

    @staticmethod
    def save_as(parent):
        last = FileHistory.get_last()
        default_dir = gs_get_default_directory(PREFS_GROUP_OPEN_SAVE)
        if last is not None:
            last = Uri(last)
            if last.is_file():
                filepath = last.get_path()
                default_dir = GLib.path_get_dirname(filepath)
        filename = File.dialog(parent, "Save", None, default_dir, FileDialogType.SAVE)
        if not filename or filename is None:
            return
        File.do_save_as(parent, filename)

    @staticmethod
    def do_save_as(parent, filename):
        if filename is None:
            File.show_session_error(parent, FileError.FILE_NOT_FOUND, filename,
                                    FileDialogType.SAVE)
            return False
        if isinstance(filename, Uri):
            newfile = filename
        else:
            newfile = Uri(filename)
        if newfile.is_file():
            newfile.add_extension(".gas")
            path = newfile.get_path()
            if File.check_file_path(path):
                File.show_session_error(parent, FileError.RESERVED_WRITE, newfile,
                                        FileDialogType.SAVE)
                return False
            gs_set_default_directory(PREFS_GROUP_OPEN_SAVE, GLib.path_get_dirname(path))
        session = Session.get_current_session()
        oldfile = session.get_url()
        if oldfile is not None and len(oldfile) and oldfile == newfile:
            if File.save(parent):
                return True
            return False
        session.ensure_all_data_loaded()
        File.save_in_progres += 1
        new_session = Session(None)
        new_session.begin(newfile, SessionOpenMode.NEW_STORE)
        io_err = new_session.get_error()
        if BackEndError.STORE_EXISTS == io_err:
            format = "The file %s already exists.\nAre you sure you want to overwrite it?"
            name = newfile.get_path() if newfile.is_file() else str(newfile)
            if not ask_yes_no(parent, False, format % name):
                new_session.close()
                File.save_in_progres -= 1
                return False
            new_session.begin(newfile, SessionOpenMode.NEW_OVERITE)

        elif BackEndError.LOCKED == io_err or BackEndError.READONLY == io_err:
            if not File.show_session_error(parent, io_err, newfile, FileDialogType.SAVE):
                new_session.begin(newfile, SessionOpenMode.BREAK_LOCK)

        elif FileError.FILE_NOT_FOUND == io_err or BackEndError.NO_SUCH_DB == io_err or SqlError.DB_TOO_OLD == io_err:
            if not File.show_session_error(parent, io_err, newfile, FileDialogType.SAVE):
                new_session.begin(newfile, SessionOpenMode.NEW_STORE)
        io_err = new_session.get_error()
        if BackEndError.NO_ERR != io_err:
            File.show_session_error(parent, io_err, newfile, FileDialogType.SAVE)
            new_session.close()
            File.save_in_progres -= 1
            return False
        key = KeyRing(scheme=newfile.scheme, hostname=newfile.hostname, port=newfile.port, username=newfile.username)
        if not newfile.is_file():
            key.set_password(newfile.password)
        new_session.swap_data(session)
        new_session.get_book().mark_session_dirty()

        gs_set_busy_cursor(None, True)
        Window.show_progress("Writing file...", 0.0)
        new_session.save(Window.show_progress)
        Window.show_progress(None, -1.0)
        gs_unset_busy_cursor(None)

        io_err = new_session.get_error()
        Event.suspend()
        if BackEndError.NO_ERR != io_err:
            File.show_session_error(parent, io_err, newfile, FileDialogType.SAVE)
            new_session.swap_data(session)
            new_session.close()
            new_session = None
        else:
            Session.clear_current_session()
            Session.set_current_session(new_session)
        Event.resume()
        File.write_default_data(new_session.get_current_book())
        File.add_history(new_session)
        Hook.run(HOOK_BOOK_SAVED, new_session)
        ComponentManager.refresh_all()
        File.save_in_progres -= 1
        return True

    @staticmethod
    def write_default_data(book):
        be = book.get_backend()
        if be is not None:
            with be.add_lock():
                for group_name, units in default_data["uom"].items():
                    group = UnitOfMeasureGroup(book)
                    group.begin_edit()
                    group.set_name(group_name)
                    primary_unit_data = units.pop(0)
                    primary_unit = UnitOfMeasure(book)
                    primary_unit.begin_edit()
                    primary_unit.set_name(primary_unit_data['name'])
                    primary_unit.set_quantity(Decimal(primary_unit_data['quantity']))
                    primary_unit.set_group(group)
                    primary_unit.commit_edit()
                    for u in units:
                        unit = UnitOfMeasure(book)
                        unit.begin_edit()
                        unit.set_name(u['name'])
                        unit.set_base(primary_unit)
                        unit.set_quantity(Decimal(u['quantity']))
                        unit.set_group(group)
                        unit.commit_edit()
                    group.commit_edit()

    @staticmethod
    def save_in_progress():
        if Session.current_session_exist():
            session = Session.get_current_session()
            return session.save_in_progress() or File.save_in_progres > 0
        return False

    @staticmethod
    def revert(parent):
        from gasstation.views.main_window import MainWindow
        title = "Reverting will discard all unsaved changes to %s. Are you sure you want to proceed ?"
        if not MainWindow.all_finish_pending():
            return
        session = Session.get_current_session()
        fileurl = session.get_url()
        if not ask_yes_no(parent, False, title % fileurl):
            return
        session.get_book().mark_session_saved()
        File.open_file(parent, fileurl, Session.get_current_book().is_readonly())

    @staticmethod
    def quit():
        if not Session.current_session_exist():
            return
        gs_set_busy_cursor(None, True)
        session = Session.get_current_session()
        Event.suspend()
        Hook.run(HOOK_BOOK_CLOSED, session)
        ComponentManager.close_by_session(session)
        State.save(session)
        PrintOperation.save(session)
        Session.clear_current_session()
        Event.resume()
        gs_unset_busy_cursor(None)

    @staticmethod
    def set_shutdown_callback(cb):
        File.shutdown_cb = cb
