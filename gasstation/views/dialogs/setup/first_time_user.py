from gi.repository import Gtk, GObject

from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.utilities.preferences import gs_pref_set_bool
from libgasstation import Session
from libgasstation.core.hook import Hook, HOOK_NEW_BOOK, HOOK_BOOK_OPENED

PREFS_GROUP_NEW_USER = "dialogs.new-user"
PREF_FIRST_STARTUP = "first-startup"


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/first-time-user-cancel.ui")
class FirstTimeUserCancelDialog(Gtk.Dialog):
    __gtype_name__ = "FirstTimeUserCancelDialog"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


GObject.type_register(FirstTimeUserCancelDialog)


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/first-time-user.ui")
class FirstTimeUserDialog(Gtk.Dialog):
    __gtype_name__ = "FirstTimeUserDialog"
    new_accounts_button: Gtk.RadioButton = Gtk.Template.Child()
    import_qif_button: Gtk.RadioButton = Gtk.Template.Child()
    tutorial_button: Gtk.RadioButton = Gtk.Template.Child()

    def __init__(self, import_assistant_fnc=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ok_pressed = False
        self.import_assistant_fnc = import_assistant_fnc
        self.set_keep_above(True)
        self.set_title("First Time Setup Assistant")
        gs_widget_set_style_context(self, "NewUserDialog")
        self.import_qif_button.set_sensitive(self.import_assistant_fnc is not None)
        self.show()
        self.connect("response", self.response_cb)
        self.connect("destroy", self.destroy_cb)

    def destroy_cb(self, widget, *args):
        if not self.ok_pressed:
            dialog = FirstTimeUserCancelDialog()
            dialog.set_transient_for(widget)
            result = dialog.run()
            self.set_first_startup(result == Gtk.ResponseType.YES)
            self.finish()
            dialog.destroy()

    def response_cb(self, _, response):
        if response == Gtk.ResponseType.OK:
            self.ok_pressed = True
            if self.new_accounts_button.get_active():
                Hook.run(HOOK_NEW_BOOK, self)
                self.finish()
                self.set_first_startup(False)
            elif self.import_assistant_fnc is not None and self.import_qif_button.get_active():
                self.import_assistant_fnc()
                self.finish()
            elif self.tutorial_button.get_active():
                self.finish()
        self.destroy()

    @staticmethod
    def finish():
        Hook.run(HOOK_BOOK_OPENED, Session.get_current_session())

    def register_qif_assistant(self, cb_fcn):
        self.import_assistant_fnc = cb_fcn

    @staticmethod
    def set_first_startup(first_startup):
        gs_pref_set_bool(PREFS_GROUP_NEW_USER, PREF_FIRST_STARTUP, first_startup)


GObject.type_register(FirstTimeUserDialog)
