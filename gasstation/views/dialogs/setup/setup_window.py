import stat

from gasstation.views.account import *
from gasstation.views.dialogs.options import OptionDialog
from gasstation.views.selections.currency import CurrencySelection
from libgasstation import OptionDB
from libgasstation.core.account_merge import *
from libgasstation.core.example_account import ExampleAccount
from libgasstation.core.helpers import *
from libgasstation.core.scrub import Scrub

PREFS_GROUP = "dialogs.new-hierarchy"

PREF_SHOW_ON_NEW_FILE = "show-on-new-file"
DIALOG_BOOK_OPTIONS_CM_CLASS = "dialog-book-options"

data_dir = os.environ['DATA_DIR']


class LanguageRegion(GObject.GEnum):
    STRING = 0
    REGION = 1
    LANG_REG_STRING = 2
    FILTER = 3


def get_ea_locale_dir(top_dir):
    default_locale = "C"
    l = locale.setlocale(locale.LC_MESSAGES, None)
    ret = GLib.build_filenamev([top_dir, l])
    try:
        statbuf = os.stat(ret)
        if not stat.S_ISDIR(statbuf.st_mode):
            ret = GLib.build_filenamev([top_dir, default_locale])
    except FileNotFoundError:
        ret = GLib.build_filenamev([top_dir, default_locale])
    return ret


class ColumnNames(GObject.GEnum):
    CHECKED = 0
    TITLE = 1
    SHORT_DESCRIPTION = 2
    LONG_DESCRIPTION = 3
    ACCOUNT = 4
    NUM_COLUMNS = 5


class AddGroupData:
    def __init__(self, to=None, p=None, comm=None):
        self.to = to
        self.parent = p
        self.com = comm


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/assistant-hierarchy.ui')
class SetupWindow(Gtk.Assistant):
    __gtype_name__ = "SetupWindow"
    currency_chooser_hbox: Gtk.Box = Gtk.Template.Child()
    choose_currency_label: Gtk.Label = Gtk.Template.Child()
    account_categories_tree_view: Gtk.TreeView = Gtk.Template.Child()
    language_combo: Gtk.ComboBox = Gtk.Template.Child()
    region_combo: Gtk.ComboBox = Gtk.Template.Child()
    region_label: Gtk.Label = Gtk.Template.Child()
    accounts_in_category_label: Gtk.Label = Gtk.Template.Child()
    accounts_in_category = Gtk.Template.Child()
    account_types_description = Gtk.Template.Child()
    final_account_tree_box = Gtk.Template.Child()

    def __init__(self, parent=None, use_defaults=True, when_completed=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if parent is not None:
            self.set_transient_for(parent)
        self.next_ok = False
        self.get_child().get_children()[0].destroy()
        self.initial_category = None
        self.category_accounts_tree = None
        self.category_set_changed = False
        self.final_account_tree = None
        self.balance_hash = {}
        self.our_account_tree = None
        self.options_dialog = None
        self.accounts_dir = GLib.build_filenamev([data_dir, "accounts"])
        self.new_book = Session.is_new_book()
        gs_widget_set_style_context(self, "AssistAccountHierarchy")
        self.currency_selector = CurrencySelection()
        self.currency_selector.set_currency(gs_default_currency())
        self.currency_selector.show()
        self.currency_chooser_hbox.pack_start(self.currency_selector, True, True, 0)
        self.currency_chooser_hbox.show()
        sel = self.account_categories_tree_view.get_selection()
        sel.set_mode(Gtk.SelectionMode.SINGLE)
        sel.connect("changed", self.account_categories_tree_view_selection_changed)
        self.account_list_added = False
        if self.new_book:
            self.insert_book_options_page()
        gs_restore_window_size(PREFS_GROUP, self, parent)
        self.connect("destroy", self.destroy_cb)
        self.when_completed = when_completed
        self.use_defaults = use_defaults
        self.show()

    def delete_dialog(self):
        gs_save_window_size(PREFS_GROUP, self)
        self.destroy()
        del self

    def destroy_cb(self, _):
        self.final_account_tree = None
        self.balance_hash = None
        del self
        return True

    def region_combo_changed_cb(self, widget):
        filter_model = self.region_combo.get_model()
        region_model = filter_model.get_model()
        filter_iter = widget.get_active_iter()
        if filter_iter is not None:
            cat_list = self.account_categories_tree_view.get_model()
            selection = self.account_categories_tree_view.get_selection()
            region_iter = filter_model.convert_iter_to_child_iter(filter_iter)
            lang_reg = region_model.get_value(region_iter, LanguageRegion.LANG_REG_STRING)
            ComponentManager.suspend()
            if self.category_accounts_tree is not None:
                self.category_accounts_tree.destroy()
            self.category_accounts_tree = None
            if cat_list is not None:
                cat_list.clear()
            account_path = GLib.build_filenamev([self.accounts_dir, lang_reg])
            Event.suspend()
            lis = ExampleAccount.load_from_directory(account_path)
            Event.resume()
            if self.initial_category is not None:
                self.initial_category = None
            for a in lis:
                self.add_one_category(a)

            if self.initial_category is not None:
                path = self.initial_category.get_path()
                self.account_categories_tree_view.scroll_to_cell(path, None, True, 0.5, 0.5)
            else:
                path = Gtk.TreePath.new_first()
            selection.select_path(path)
            self.account_categories_tree_view_selection_changed(selection)
            ComponentManager.resume()

    def region_combo_change_filter_cb(self, _):
        filter_model = self.region_combo.get_model()
        region_model = filter_model.get_model()
        have_one_region = False
        sorted_iter = self.language_combo.get_active_iter()
        count = 0
        _iter = None
        if sorted_iter is not None:
            sort_model = self.language_combo.get_model()
            language_model = sort_model.get_model()
            language_iter = sort_model.convert_iter_to_child_iter(sorted_iter)
            language = language_model.get_value(language_iter, LanguageRegion.STRING)
            region_iter = region_model.get_iter_first()
            while region_iter is not None:
                region_test = region_model.get_value(region_iter, LanguageRegion.STRING)
                if language == region_test:
                    region_model.set_value(region_iter, LanguageRegion.FILTER, True)
                    if count == 0:
                        _iter = region_iter.copy()
                    count += 1
                else:
                    region_model.set_value(region_iter, LanguageRegion.FILTER, False)
                region_iter = region_model.iter_next(region_iter)
            if count == 1:
                filter_iter = filter_model.convert_child_iter_to_iter(_iter)
                self.region_combo.set_active_iter(filter_iter[1])
                have_one_region = True
                region_label = region_model.get_value(_iter, LanguageRegion.REGION)
                self.region_label.set_text(region_label)
            else:
                if self.region_combo.get_active() == -1:
                    filter_iter = filter_model.convert_child_iter_to_iter(_iter)
                    self.region_combo.set_active_iter(filter_iter[1])
            self.region_label.set_visible(have_one_region)
            self.region_combo.set_visible(not have_one_region)

    def update_language_region_combos(self, locale_dir):
        language_store = Gtk.ListStore(str)
        region_store = Gtk.ListStore(str, str, str, bool)
        filter_model = region_store.filter_new()
        start_region = ""
        sort_model = Gtk.TreeModelSort(model=language_store)
        sort_model.set_sort_column_id(LanguageRegion.STRING, Gtk.SortType.ASCENDING)
        self.language_combo.set_model(sort_model)
        self.region_combo.set_model(filter_model)
        filter_model.set_visible_column(LanguageRegion.FILTER)
        self.language_combo.connect("changed", self.region_combo_change_filter_cb)
        if GLib.file_test(self.accounts_dir, GLib.FileTest.IS_DIR):
            testhash = {}
            for name in os.listdir(self.accounts_dir):
                parts = name.split("_")
                region_iter = region_store.append()
                region_store.set(region_iter,
                                 [LanguageRegion.LANG_REG_STRING, LanguageRegion.STRING, LanguageRegion.FILTER],
                                 [name, parts[0], True])

                if locale_dir.endswith(name):
                    filter_iter = filter_model.convert_child_iter_to_iter(region_iter)
                    self.region_combo.set_active_iter(filter_iter[1])
                    start_region = parts[0]
                if len(parts) > 1:
                    region_store.set_value(region_iter, LanguageRegion.REGION, parts[1])
                else:
                    region_store.set_value(region_iter, LanguageRegion.REGION, "--")

                if name == "C":
                    region_store.set(region_iter, [LanguageRegion.STRING, LanguageRegion.REGION], ["en", "US"])
                    lang_name = "en"
                    if locale_dir.endswith(name):
                        start_region = lang_name
                else:
                    lang_name = parts[0]
                if testhash.get(lang_name) is None:
                    language_iter = language_store.append()
                    language_store.set_value(language_iter, LanguageRegion.STRING, lang_name)
                    testhash[lang_name] = "test"

        language_iter = language_store.get_iter_first()
        while language_iter is not None:
            language_test = language_store.get_value(language_iter, LanguageRegion.STRING)
            if language_test == start_region:
                sort_iter = sort_model.convert_child_iter_to_iter(language_iter)
                self.language_combo.set_active_iter(sort_iter[1])
            language_iter = language_store.iter_next(language_iter)
        self.region_combo.connect("changed", self.region_combo_changed_cb)

    @staticmethod
    def get_final_balance(bhash, account):
        if bhash is None or len(bhash) == 0 or account is None:
            return Decimal(0)
        return bhash.get(account, Decimal(0))

    @staticmethod
    def set_final_balance(bhash, account, in_balance):
        if bhash is None or account is None or in_balance is None:
            return
        bhash[account] = in_balance

    def account_set_checked_helper(self, store, _, _iter):
        c = store.get_value(_iter, ColumnNames.CHECKED)
        if c:
            self.next_ok = True
            return True
        return False

    def categories_page_enable_next(self):
        self.next_ok = False
        self.account_categories_tree_view.get_model().foreach(self.account_set_checked_helper)
        currentpagenum = self.get_current_page()
        currentpage = self.get_nth_page(currentpagenum)
        self.set_page_complete(currentpage, self.next_ok)

    def categories_selection_changed(self, *_):
        self.category_set_changed = True
        self.categories_page_enable_next()

    def add_one_category(self, acc):
        view = self.account_categories_tree_view
        store = view.get_model()
        use_defaults = self.use_defaults and acc.start_selected
        _iter = store.append([use_defaults, acc.title, acc.short_description,
                              acc.long_description,
                              acc])
        if use_defaults:
            self.category_set_changed = True
            path = store.get_path(_iter)
            self.initial_category = Gtk.TreeRowReference.new(store, path)

    @staticmethod
    def category_checkbox_toggled(toggle, path, store):
        _iter = store.get_iter_from_string(path)
        if _iter is None:
            return
        active = toggle.get_active()
        store.set_value(_iter, ColumnNames.CHECKED, not active)

    def account_categories_tree_view_prepare(self):
        tree_view = self.account_categories_tree_view
        tree_view.set_model(None)
        model = Gtk.ListStore(bool, str, str, str, object)
        tree_view.set_model(model)
        locale_dir = get_ea_locale_dir(self.accounts_dir)
        Event.suspend()
        for acc in ExampleAccount.load_from_directory(locale_dir):
            self.add_one_category(acc)
        Event.resume()
        self.update_language_region_combos(locale_dir)
        model.connect("row_changed", self.categories_selection_changed)
        renderer = Gtk.CellRendererToggle()
        renderer.set_property("activatable", True)
        column = Gtk.TreeViewColumn("Selected")
        column.pack_start(renderer, True)
        column.add_attribute(renderer, "active", ColumnNames.CHECKED)
        tree_view.append_column(column)
        column.set_sort_column_id(ColumnNames.CHECKED)
        renderer.connect("toggled", self.category_checkbox_toggled, model)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Account Types")
        column.pack_start(renderer, True)
        column.add_attribute(renderer, "text", ColumnNames.TITLE)
        tree_view.append_column(column)
        column.set_sort_column_id(ColumnNames.TITLE)
        tree_view.set_headers_clickable(True)
        model.set_sort_column_id(ColumnNames.TITLE, Gtk.SortType.ASCENDING)

        if self.initial_category:
            path = Gtk.TreeRowReference.get_path(self.initial_category)
            selection = tree_view.get_selection()
            tree_view.scroll_to_cell(path, None, True, 0.5, 0.5)
            selection.select_path(path)

    @Gtk.Template.Callback()
    def on_prepare(self, *args):
        currency_page = 2 if self.new_book else 1
        selection_page = 3 if self.new_book else 2
        final_page = 4 if self.new_book else 3
        current_page = self.get_current_page()
        if current_page == currency_page:
            self.on_select_currency_prepare()
        elif current_page == selection_page:
            self.on_choose_account_categories_prepare()
        elif current_page == final_page:
            self.on_final_account_prepare()

    def on_choose_account_categories_prepare(self):
        if not self.account_list_added:
            if self.category_accounts_tree is not None:
                self.category_accounts_tree.destroy()
                self.category_accounts_tree = None
            buffer = self.account_types_description.get_buffer()
            buffer.set_text("", -1)
            self.account_list_added = True
            ComponentManager.suspend()
            self.account_categories_tree_view_prepare()
            ComponentManager.resume()
        self.categories_page_enable_next()

    def account_categories_tree_view_selection_changed(self, selection):
        if self.category_accounts_tree is not None:
            self.category_accounts_tree.destroy()
            self.category_accounts_tree = None
        model, _iter = selection.get_selected()
        if _iter is not None:
            gea = model.get_value(_iter, ColumnNames.ACCOUNT)
            text2 = "Accounts in '%s'" % gea.title
            text = "<b>%s</b>" % text2
            self.accounts_in_category_label.set_markup(text)
            buffer = self.account_types_description.get_buffer()
            buffer.set_text(gea.long_description if gea.long_description else "No description provided.")
            tree_view = AccountView(root=gea.root, show_root=False)
            column = tree_view.get_column(0)
            column.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
            self.category_accounts_tree = tree_view
            tree_view.expand_all()
            self.accounts_in_category.add(tree_view)
            tree_view.show()
        else:
            text = "<b>Accounts in Category</b>"
            self.accounts_in_category_label.set_markup(text)
            buffer = self.account_types_description.get_buffer()
            buffer.set_text("", -1)

    @staticmethod
    def select_helper(store, _, _iter, data):
        gea = store.get_value(_iter, ColumnNames.ACCOUNT)
        if gea is not None and not gea.exclude_from_select_all:
            store.set_value(_iter, ColumnNames.CHECKED, int(data))
        return False

    @Gtk.Template.Callback()
    def select_all_clicked(self, _):
        self.account_categories_tree_view.get_model().foreach(self.select_helper, True)

    @Gtk.Template.Callback()
    def clear_all_clicked(self, _):
        self.account_categories_tree_view.get_model().foreach(self.select_helper, False)

    def delete_our_account_tree(self):
        if self.our_account_tree is not None:
            self.our_account_tree.destroy()
            self.our_account_tree.dispose()

    def add_groups_for_each(self, toadd, data):
        foundact = data.to.lookup_by_name(toadd.get_name())
        if foundact is None:
            foundact = toadd.clone(Session.get_current_book())
            foundact.set_commodity(data.com)
            if data.to is not None:
                data.to.append_child(foundact)
            elif data.parent is not None:
                data.parent.append_child(foundact)
            else:
                logging.warning("add_groups_for_each: no valid parent")
        if len(toadd.children) > 0:
            fn = self.add_groups_for_each
            udata = AddGroupData(foundact, foundact, data.com)
            for child in toadd.children:
                fn(child, udata)

    def merge_accounts(self, dalist, com):
        ret = Account(Session.get_current_book())
        for xea in dalist:
            xea.root.foreach_child(self.add_groups_for_each, AddGroupData(ret, None, com))
        return ret

    @staticmethod
    def accumulate_accounts(store, path, _iter, l):
        active = store.get_value(_iter, ColumnNames.CHECKED)
        gea = store.get_value(_iter, ColumnNames.ACCOUNT)
        if active and gea:
            l.insert(0, gea)
        return False

    @classmethod
    def get_selected_account_list(cls, tree_view):
        actlist = []
        model = tree_view.get_model()
        model.foreach(cls.accumulate_accounts, actlist)
        return actlist

    def balance_cell_data_func(self, _, cell, model, _iter, *args):
        account = AccountView.get_account_from_iter(model, _iter)
        balance = self.get_final_balance(self.balance_hash, account)
        if balance.is_zero():
            string = ""
        else:
            print_info = PrintAmountInfo.account(account, False)
            string = gs_account_print_amount(balance, print_info)

        if account.get_type() in [AccountType.EQUITY or AccountType.TRADING]:
            allow_value = False
            string = "zero"
        else:
            disp = AccountMerge.determine_merge_disposition(Session.get_current_root(), account)
            if disp == AccountMergeDisposition.CREATE_NEW:
                allow_value = not account.get_placeholder()
            else:
                allow_value = False
                string = "existing account"
        cell.set_property("text", str(string))
        cell.set_property("editable", allow_value)
        cell.set_property("sensitive", allow_value)

    @staticmethod
    def use_existing_account_data_func(tree_column, cell, tree_model, _iter, *args):
        to_user = "error unknown condition"
        new_acct = AccountView.get_account_from_iter(tree_model, _iter)
        if new_acct is None:
            cell.set_property("text", "(null account)")
            return
        real_root = Session.get_current_root()
        disposition = AccountMerge.determine_merge_disposition(real_root, new_acct)
        if disposition == AccountMergeDisposition.USE_EXISTING:
            to_user = "Yes"
        elif AccountMergeDisposition.CREATE_NEW:
            to_user = "No"
        cell.set_property("text", to_user)

    def on_final_account_prepare(self):
        if not self.category_set_changed:
            return
        self.category_set_changed = False
        ComponentManager.suspend()
        if self.final_account_tree is not None:
            self.final_account_tree.destroy()
            self.final_account_tree = None
        self.delete_our_account_tree()
        actlist = self.get_selected_account_list(self.account_categories_tree_view)
        com = self.currency_selector.get_currency()
        self.our_account_tree = self.merge_accounts(actlist, com)
        self.final_account_tree = AccountView(root=self.our_account_tree, show_root=False)
        tree_view = self.final_account_tree
        self.final_account_tree.set_name_edited(self.final_account_tree.name_edited_cb)
        self.final_account_tree.set_code_edited(self.final_account_tree.code_edited_cb)
        self.final_account_tree.set_desc_edited(self.final_account_tree.description_edited_cb)
        self.final_account_tree.set_notes_edited(self.final_account_tree.notes_edited_cb)
        tree_view.set_headers_visible(True)
        column = self.final_account_tree.find_column_by_name("type")
        self.final_account_tree.set_user_data(column, DEFAULT_VISIBLE, 1)
        self.final_account_tree.configure_columns()
        self.final_account_tree.set_show_column_menu(False)

        selection = tree_view.get_selection()
        selection.set_mode(Gtk.SelectionMode.BROWSE)

        renderer = Gtk.CellRendererToggle()
        renderer.set_property("activatable", True)
        renderer.set_property("sensitive", True)
        renderer.connect("toggled", self.placeholder_cell_toggled)
        column = Gtk.TreeViewColumn("Placeholder")
        column.pack_start(renderer, True)
        column.set_cell_data_func(renderer, self.placeholder_cell_data_func)
        tree_view.append_column(column)
        renderer = Gtk.CellRendererText()
        renderer.set_property("xalign", 1.0)
        renderer.connect("edited", self.balance_cell_edited)
        column = Gtk.TreeViewColumn("Opening Balance")
        column.pack_start(renderer, True)
        column.set_cell_data_func(renderer, self.balance_cell_data_func)
        tree_view.append_column(column)
        if len(Session.get_current_root()) > 0:
            column = tree_view.add_text_column("Use Existing", None, None, "yes", TREE_VIEW_COLUMN_DATA_NONE,
                                               TREE_VIEW_COLUMN_VISIBLE_ALWAYS)
            renderers = column.get_cells()[0]
            renderers.set_property("xalign", 1.0)
            column.set_cell_data_func(renderers, self.use_existing_account_data_func)
        self.final_account_tree_box.add(self.final_account_tree)
        tree_view.expand_all()
        self.final_account_tree.show()
        ComponentManager.resume()

    @staticmethod
    def placeholder_cell_data_func(tree_column, cell, model, _iter, *args):
        willbe_placeholder = False
        if model is None:
            return
        account = AccountView.get_account_from_iter(model, _iter)
        root = Session.get_current_root()
        disp = AccountMerge.determine_merge_disposition(root, account)
        if disp == AccountMergeDisposition.USE_EXISTING:
            full_name = account.get_full_name()
            existing_acct = root.lookup_by_full_name(full_name)
            willbe_placeholder = existing_acct.get_placeholder()
        elif disp == AccountMergeDisposition.CREATE_NEW:
            willbe_placeholder = account.get_placeholder()
        cell.set_active(willbe_placeholder)

    def placeholder_cell_toggled(self, cell_renderer, path):
        treepath = Gtk.TreePath.new_from_string(path)
        account = self.final_account_tree.get_account_from_path(treepath)
        state = cell_renderer.get_active()
        if account is not None:
            account.set_placeholder(not state)
        if not state:
            self.set_final_balance(self.balance_hash, account, 0)

    def balance_cell_edited(self, cell, _, new_text):
        account = self.final_account_tree.get_selected_account()
        if account is None:
            return
        amount = parse_exp_decimal(new_text)
        if amount.is_zero():
            cell.set_property("text", "")
        account_cmdty_fraction = account.get_commodity().get_fraction()
        amount = amount.convert(account_cmdty_fraction, NumericRound.HALF_UP)
        self.set_final_balance(self.balance_hash, account, amount)
        Event.gen(account, EVENT_MODIFY)

    @Gtk.Template.Callback()
    def on_cancel(self, _):
        ComponentManager.suspend()
        if self.new_book:
            self.options_dialog.destroy()
        self.delete_dialog()
        self.delete_our_account_tree()
        ComponentManager.resume()

    def starting_balance_helper(self, account):
        balance = self.get_final_balance(self.balance_hash, account)
        if not balance.is_zero():
            Scrub.create_opening_balance(account, balance, datetime.datetime.now(), Session.get_current_book())

    @Gtk.Template.Callback()
    def on_finish(self, _):
        com = self.currency_selector.get_currency()
        self.account_categories_tree_view.destroy()
        if self.our_account_tree is not None:
            self.our_account_tree.foreach_descendant(self.starting_balance_helper)
        if self.initial_category:
            self.initial_category = None
        self.delete_dialog()
        ComponentManager.suspend()
        if self.new_book:
            self.options_dialog.destroy()
        if self.final_account_tree is not None:
            self.final_account_tree.destroy()
            self.final_account_tree = None
        AccountMerge.account_trees_merge(Session.get_current_root(), self.our_account_tree)
        self.delete_our_account_tree()
        root = Session.get_current_root()
        root.set_commodity(com)
        ComponentManager.resume()
        if self.when_completed is not None:
            self.when_completed()

    def on_select_currency_prepare(self):
        from gasstation.views.main_window import MainWindow
        if self.new_book:
            MainWindow.book_options_dialog_apply_helper(self.options)
            if Session.get_current_book().use_currency():
                self.currency_selector.set_currency(Session.get_current_book().get_currency())
                self.choose_currency_label.set_text("You selected a book currency and it will be used for\n"
                                                    "new accounts. Accounts in other currencies must be\n"
                                                    "added manually.")

                self.currency_selector.set_sensitive(False)
            else:
                self.currency_selector.set_currency(gs_default_currency())
                self.choose_currency_label.set_text("Please choose the currency to use for new accounts.")
                self.currency_selector.set_sensitive(True)

    @staticmethod
    def book_options_dialog_close_cb(optionwin, options):
        optionwin.destroy()
        options.destroy()

    def insert_book_options_page(self):
        from libgasstation import ID_BOOK
        vbox = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        vbox.set_homogeneous(False)
        book = Session.get_current_book()
        self.options = OptionDB.new_for_type(ID_BOOK, "Business")
        self.options.load(book)
        self.options.clean()
        self.options_dialog = OptionDialog.new_modal(True, "New Book Options", DIALOG_BOOK_OPTIONS_CM_CLASS, None)
        self.options_dialog.build_contents(self.options)
        self.options_dialog.set_close_cb(self.book_options_dialog_close_cb, self.options)
        options = self.options_dialog.notebook
        parent = options.get_parent()
        parent.remove(options)
        vbox.add(options)
        vbox.show_all()
        self.options_dialog.window.hide()
        self.insert_page(vbox, 1)
        self.set_page_title(vbox, "New Book Options")
        self.set_page_complete(vbox, True)

    def __repr__(self):
        return '<SetUpWindow>'


GObject.type_register(SetupWindow)
