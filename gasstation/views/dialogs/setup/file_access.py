from enum import IntEnum

import gi

from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.utilities.file_history import FileHistory
from gasstation.utilities.ui import PREFS_GROUP_OPEN_SAVE, PREFS_GROUP_EXPORT, gs_get_default_directory
from libgasstation.core.uri import Uri

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GLib

DEFAULT_HOST = "localhost"
DEFAULT_DATABASE = "gasstation"


class FileAccessType(IntEnum):
    OPEN = 0
    SAVE_AS = 1
    EXPORT = 2


def get_default_database():
    default_db = GLib.getenv("DEFAULT_DATABASE")
    if default_db is None:
        default_db = DEFAULT_DATABASE
    return default_db


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/file-access.ui')
class FileAccessDialog(Gtk.Dialog):
    __gtype_name__ = 'FileAccessDialog'
    frame_file: Gtk.Frame = Gtk.Template.Child()
    frame_database: Gtk.Frame = Gtk.Template.Child()
    readonly_checkbutton: Gtk.CheckButton = Gtk.Template.Child()
    tf_host: Gtk.Entry = Gtk.Template.Child()
    tf_database: Gtk.Entry = Gtk.Template.Child()
    tf_port: Gtk.SpinButton = Gtk.Template.Child()
    file_chooser: Gtk.FileChooserWidget = Gtk.Template.Child()
    tf_username: Gtk.Entry = Gtk.Template.Child()
    tf_password: Gtk.Entry = Gtk.Template.Child()
    pb_op: Gtk.Button = Gtk.Template.Child()
    data_format_grid: Gtk.Grid = Gtk.Template.Child()

    def __init__(self, parent, _type, starting_directory=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.starting_dir = starting_directory
        self.default_db = get_default_database()
        self.data_access_method = None
        active_access_method_index = -1
        self.tf_host.set_text(DEFAULT_HOST)
        self.tf_database.set_text(self.default_db)
        file_chooser_action = Gtk.FileChooserAction.OPEN
        need_access_method_file = False
        need_access_method_mysql = False
        need_access_method_postgresql = False
        need_access_method_sqlite = False
        need_access_method_xml = False
        if not (_type == FileAccessType.OPEN or _type == FileAccessType.SAVE_AS or _type == FileAccessType.EXPORT):
            raise TypeError("Invalid FIle access type")
        self._type = _type
        self.set_transient_for(parent)
        gs_widget_set_style_context(self, "FileAccessDialog")
        button_label = None
        settings_section = None
        if _type == FileAccessType.OPEN:
            self.set_title("Open...")
            button_label = "_Open"
            file_chooser_action = Gtk.FileChooserAction.OPEN
            settings_section = PREFS_GROUP_OPEN_SAVE
        elif _type == FileAccessType.SAVE_AS:
            self.set_title("Save As...")
            button_label = "_Save As"
            file_chooser_action = Gtk.FileChooserAction.SAVE
            settings_section = PREFS_GROUP_OPEN_SAVE
            self.readonly_checkbutton.destroy()
        elif _type == FileAccessType.EXPORT:
            self.set_title("Export")
            button_label = "_Save As"
            file_chooser_action = Gtk.FileChooserAction.SAVE
            settings_section = PREFS_GROUP_EXPORT
            self.readonly_checkbutton.destroy()
        op = self.pb_op
        if op is not None:
            op.set_label(button_label)
        self.file_chooser.set_action(file_chooser_action)

        if _type == FileAccessType.OPEN or _type == FileAccessType.SAVE_AS:
            last = FileHistory.get_last()
            if last is not None:
                last = Uri(last)
                if last.get_path() is not None:
                    self.starting_dir = GLib.path_get_dirname(last.get_path())
        if self.starting_dir is None:
            self.starting_dir = gs_get_default_directory(settings_section)
        self.file_chooser.set_current_folder(self.starting_dir)

        self.data_access_method = Gtk.ComboBoxText()
        self.data_format_grid.attach(self.data_access_method, 1, 0, 1, 1)
        self.data_access_method.show()
        self.data_access_method.connect("changed", self.data_access_method_changed_cb)

        for access_method in ["mysql", "postgresql", "sqlite"]:
            if access_method == "mysql":
                need_access_method_mysql = True
            elif access_method == "postgresql":
                need_access_method_postgresql = True
            elif access_method == "xml":
                if _type == FileAccessType.OPEN:
                    need_access_method_file = True
                else:
                    need_access_method_xml = True
            elif access_method == "sqlite":
                if _type == FileAccessType.OPEN:
                    need_access_method_file = True
                else:
                    need_access_method_sqlite = True
        access_method_index = -1
        if need_access_method_file:
            self.data_access_method.append_text("file")
            active_access_method_index = access_method_index + 1

        if need_access_method_mysql:
            self.data_access_method.append_text("mysql")
            access_method_index += 1

        if need_access_method_postgresql:
            self.data_access_method.append_text("postgresql")
            access_method_index += 1

        if need_access_method_sqlite:
            self.data_access_method.append_text("sqlite")
            active_access_method_index = access_method_index + 1

        if need_access_method_xml:
            self.data_access_method.append_text("xml")
            access_method_index += 1
            active_access_method_index = access_method_index
        self.data_access_method.set_active(active_access_method_index)
        self.set_widget_sensitivity_for_uri_type(self.data_access_method.get_active_text())
        self.show()

    def __repr__(self):
        return '<FileAccessDialog>'

    def geturl(self):
        scheme = self.data_access_method.get_active_text()
        if scheme in ["xml", "sqlite", "file"]:
            path = self.file_chooser.get_filename()
            if not path:
                return None
            return Uri(scheme=scheme, path=path)
        else:
            host = self.tf_host.get_text()
            path = self.tf_database.get_text()
            port = self.tf_port.get_value_as_int()
            username = self.tf_username.get_text()
            password = self.tf_password.get_text()
            return Uri(scheme=scheme, hostname=host, port=port, username=username, password=password, path=path)

    def set_widget_sensitivity(self, is_file_based_uri):
        if is_file_based_uri:
            self.frame_file.show()
            self.frame_database.hide()
            self.file_chooser.set_current_folder(self.starting_dir)
        else:
            self.frame_database.show()
            self.frame_file.hide()

    def set_widget_sensitivity_for_uri_type(self, uri_type):
        if uri_type == "file" or uri_type == "xml" or uri_type == "sqlite":
            self.set_widget_sensitivity(True)
        elif uri_type == "mysql" or uri_type == "postgresql":
            self.set_widget_sensitivity(False)
            self.tf_port.set_value(3306 if uri_type == "mysql" else 5432)
        else:
            raise TypeError("False url type")

    def data_access_method_changed_cb(self, cb):
        _type = cb.get_active_text()
        self.set_widget_sensitivity_for_uri_type(_type)


GObject.type_register(FileAccessDialog)
