from gasstation.plugins.pages import RegisterPluginPage
from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.ui import *
from gasstation.views.dialogs.account import AccountDialog
from gasstation.views.dialogs.account.transfer import TransferDialog
from gasstation.views.main_window import Window
from gasstation.views.reconcile import *
from gasstation.views.selections.amount_edit import *
from gasstation.views.selections.date_edit import *
from libgasstation.core.component import ComponentManager
from libgasstation.core.scrub import *
from libgasstation.core.transaction import SplitState

WINDOW_RECONCILE_CM_CLASS = "window-reconcile"
PREF_AUTO_CC_PAYMENT = "auto-cc-payment"
PREF_ALWAYS_REC_TO_TODAY = "always-reconcile-to-today"


class StartRecnWindowData:
    def __init__(self):
        self.account = None
        self.account_type = AccountType.NONE
        self.startRecnWindow = None
        self.xfer_button = None
        self.date_value = None
        self.end_value = None
        self.original_value = None
        self.user_set_value = False
        self.xferData = None
        self.include_children = False
        self.date = 0

    def update_cb(self, widget, event):
        self.end_value.evaluate()
        value = self.end_value.get_amount()
        self.user_set_value = not value.equal(self.original_value)
        return False

    def date_changed(self, widget):
        if self.user_set_value:
            return
        new_date = widget.get_date_end()
        new_balance = gs_ui_account_get_balance_as_of_date(self.account, new_date, self.include_children)
        self.end_value.set_amount(new_balance)

    def children_changed(self, widget):
        self.include_children = widget.get_active()
        self.date_changed(self.date_value)

    @staticmethod
    def make_interest_window_name(account, text):
        fullname = account.get_full_name()
        return " ".join([fullname, "-", text if text else ""])

    def interest_xfer_no_auto_clicked_cb(self, button):
        self.xferData.close()
        if self.xfer_button is not None:
            self.xfer_button.set_sensitive(True)


# static void
# recnInterestXferWindow (self)
# {
# gchar *title
#
# if (!data.account_type.has_auto_interest_xfer (data.account_type)) return
#
#
# data.xferData = TransferDialog.new()(GTK_WIDGET(data.startRecnWindow),
#                                  data.account)
#
#
#
#
# if ( data.account_type.has_auto_interest_payment (data.account_type))
#     title = self.make_interest_window_name (data.account,
#                                                 "Interest Payment"))
# else
# title = self.make_interest_window_name (data.account,
#                                             "Interest Charge"))
#
# gs_xfer_dialog_set_title (data.xferData, title)
#
#
#
#
# gs_xfer_dialog_set_information_label (data.xferData,
#                                        "Payment Information"))
#
#
# if (data.account_type.has_auto_interest_payment (data.account_type))
#     {
#         gs_xfer_dialog_set_from_account_label (data.xferData,
#                                                 "Payment From"))
# gs_xfer_dialog_set_from_show_button_active( data.xferData, True)
#
#
#
# gs_xfer_dialog_set_to_account_label (data.xferData,
#                                       "Reconcile Account"))
# gs_xfer_dialog_select_to_account (data.xferData, data.account)
# gs_xfer_dialog_lock_to_account_tree (data.xferData )
#
#
# gs_xfer_dialog_quickfill_to_account (data.xferData, True)
# }
# else
# {
#     gs_xfer_dialog_set_from_account_label (data.xferData,
#                                             "Reconcile Account"))
# gs_xfer_dialog_select_from_account (data.xferData, data.account)
# gs_xfer_dialog_lock_from_account_tree (data.xferData)
#
# gs_xfer_dialog_set_to_account_label (data.xferData,
#                                       "Payment To"))
# gs_xfer_dialog_set_to_show_button_active (data.xferData, True)
#
#
#
#
# gs_xfer_dialog_quickfill_to_account (data.xferData, False)
# }
#
#
#
# gs_xfer_dialog_add_user_specified_button (data.xferData,
#                                            ( data.account_type.has_auto_interest_payment (data.account_type) ?
#                                            "No Auto Interest Payments for this Account")
# : "No Auto Interest Charges for this Account")),
# G_CALLBACK (self.interest_xfer_no_auto_clicked_cb),
# (gpointer) data )
#
#
# gs_xfer_dialog_toggle_currency_table (data.xferData, False)
#
#
# gs_xfer_dialog_set_date (data.xferData, data.date)
#
#
# if (!gs_xfer_dialog_run_until_done (data.xferData))
# if (data.xfer_button)
#     gtk_widget_set_sensitive (data.xfer_button), True)
#
#
# data.xferData = None
# }
#
#
#
# static void
# gs_reconcile_interest_xfer_run (self)
# {
# GtkWidget *entry = gs_amount_edit_gtk_entry (
#     gs_AMOUNT_EDIT(data.end_value))
# gs_numeric before = gs_amount_edit_get_amount (
#     gs_AMOUNT_EDIT(data.end_value))
# gs_numeric after
#
# recnInterestXferWindow (data)
#
#
# after = xaccAccountGetBalanceAsOfDate (data.account, data.date)
#
#
# if ( gs_numeric_compare (before, after))
#     {
#     if (gs_reverse_balance (data.account))
#     after = gs_numeric_neg (after)
#
#     gs_amount_edit_set_amount (gs_AMOUNT_EDIT (data.end_value), after)
#     gtk_widget_grab_focus (entry))
#     gtk_editable_select_region (GTK_EDITABLE (entry), 0, -1)
#     data.original_value = after
#     data.user_set_value = False
#     }
#     }
#
#
#     void
#     gs_start_recn2_interest_clicked_cb (GtkButton *button, self)
#     {
#
#     if (data.xfer_button)
#         gtk_widget_set_sensitive (data.xfer_button), False)
#
#
#         gs_reconcile_interest_xfer_run (data)
#     }


class RecnWindow:
    last_statement_date = None

    def __init__(self):
        actions = [("ReconcileMenuAction", None, "_Reconcile", None, None, None,),
                   ("AccountMenuAction", None, "_Account", None, None, None,),
                   ("TransactionMenuAction", None, "_Transaction", None, None, None,),
                   ("HelpMenuAction", None, "_Help", None, None, None,),

                   (
                       "RecnChangeInfoAction", None, "_Reconcile Information...", None,
                       "Change the reconcile information "
                       "including statement date and ending balance.",
                       self.change_cb
                   ),
                   (
                       "RecnFinishAction", "system-run", "_Finish", "<primary>w",
                       "Finish the reconciliation of this account",
                       self.finish_cb
                   ),
                   (
                       "RecnPostponeAction", "go-previous", "_Postpone", "<primary>p",
                       "Postpone the reconciliation of this account",
                       self.postpone_cb
                   ),
                   (
                       "RecnCancelAction", "process-stop", "_Cancel", None,
                       "Cancel the reconciliation of this account",
                       self.cancel_cb
                   ),

                   (
                       "AccountOpenAccountAction", "go-jump", "_Open Account", None,
                       "Open the account",
                       self.open_cb
                   ),
                   (
                       "AccountEditAccountAction", None, "_Edit Account", None,
                       "Edit the main account for this register",
                       self.edit_account_cb
                   ),
                   (
                       "AccountTransferAction", None, "_Transfer...", None,
                       "Transfer funds from one account to another",
                       self.xfer_cb
                   ),
                   (
                       "AccountCheckRepairAction", None, "_Check & Repair", None,
                       "Check for and repair unbalanced transactions and orphan splits "
                       "in this account",
                       self.scrub_cb
                   ),

                   (
                       "TransBalanceAction", "document-new", "_Balance", "<primary>b",
                       "Add a new balancing entry to the account",
                       self.balance_cb
                   ),
                   (
                       "TransEditAction", "document-properties", "_Edit", "<primary>e",
                       "Edit the current transaction",
                       self.edit_cb
                   ),
                   (
                       "TransDeleteAction", "edit-delete", "_Delete", "<primary>d",
                       "Delete the selected transaction",
                       self.delete_cb
                   ),
                   (
                       "TransRecAction", "emblem-default", "_Reconcile Selection", "<primary>r",
                       "Reconcile the selected transactions",
                       self.rec_cb
                   ),
                   (
                       "TransUnRecAction", "edit-clear", "_Unreconcile Selection", "<primary>u",
                       "Unreconcile the selected transactions",
                       self.unrec_cb
                   ),

                   (
                       "HelpHelpAction", None, "_Help", None,
                       "Open the GnuCash help window",
                       self.help_cb
                   )
                   ]
        self.account = None
        self.new_ending = Decimal(0)
        self.statement_date = 0
        self.component_id = 0
        self.window = None
        self.ui_merge = None
        self.action_group = None
        self.starting = None
        self.ending = None
        self.recn_date = None
        self.reconciled = None
        self.difference = None

        self.total_debit = None
        self.total_credit = None

        self.debit = None
        self.credit = None

        self.debit_frame = None
        self.credit_frame = None
        self.delete_refresh = False
        self.component_id = 0

    def refresh(self):
        self.debit.refresh()
        self.credit.refresh()
        self.set_sensitivity()
        self.set_window_name()
        self.recalculate_balance()
        self.window.queue_resize()

    def get_account(self):
        return Account.lookup(self.account, Session.get_current_book())

    def recalculate_balance(self):
        account = self.get_account()
        if account is None:
            return Decimal(0)
        include_children = account.get_reconcile_children_status()
        starting = gs_ui_account_get_reconciled_balance(account, include_children)
        print_info = PrintAmountInfo.account(account, True)
        amount = sprintamount(starting, print_info)
        gs_set_label_color(self.starting, starting)
        self.starting.set_text(amount)
        amount = print_datetime(self.statement_date)
        self.recn_date.set_text(amount)
        ending = self.new_ending
        amount = sprintamount(ending, print_info)
        gs_set_label_color(self.ending, ending)
        self.ending.set_text(amount)
        debit = self.debit.reconciled_balance()
        credit = self.credit.reconciled_balance()
        amount = sprintamount(debit, print_info)
        self.total_debit.set_text(amount)
        amount = sprintamount(credit, print_info)
        self.total_credit.set_text(amount)
        reconciled = starting.add_fixed(debit.sub_fixed(credit))
        amount = sprintamount(reconciled, print_info)
        gs_set_label_color(self.reconciled, reconciled)
        self.reconciled.set_text(amount)
        diff = ending-reconciled
        amount = sprintamount(diff, print_info)
        gs_set_label_color(self.difference, diff)
        self.difference.set_text(amount)
        action = self.action_group.get_action("RecnFinishAction")
        action.set_sensitive(diff.is_zero())
        action = self.action_group.get_action("TransBalanceAction")
        action.set_sensitive(not diff.is_zero())
        return diff

    @staticmethod
    def save_reconcile_interval(account, statement_date):
        months = 0
        prev_statement_date = account.get_last_reconcile_date()
        if not prev_statement_date:
            return
        seconds = statement_date - prev_statement_date
        days = int(seconds / 60 / 60 / 24)
        if days == 28:
            prev_months, prev_days = account.get_reconcile_last_interval()
            if prev_months == 1:
                months = 1
                days = 0
        elif days > 28:
            current = time.localtime(statement_date)
            prev = time.localtime(prev_statement_date)
            months = ((12 * current.tm_year + current.tm_mon) -
                      (12 * prev.tm_year + prev.tm_mon))
            days = 0
        if months >= 0 and days >= 0:
            account.set_reconcile_last_interval(months, days)

    @classmethod
    def start(cls, parent, account, new_ending, statement_date, enable_subaccount):
        data = StartRecnWindowData()
        data.account = account
        data.account_type = account.get_type()
        data.date = statement_date
        auto_interest_xfer_option = account.get_auto_interest()
        data.include_children = account.get_reconcile_child_status()
        ending = gs_ui_account_get_reconciled_balance(account, data.include_children)
        print_info = PrintAmountInfo.account(account, True)

        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/window-reconcile.ui",
                                          ["reconcile_start_dialog"])
        dialog = builder.get_object("reconcile_start_dialog")
        dialog.set_name("gs-id-reconcile2-start")
        title = cls.make_window_name(account)
        dialog.set_title(title)
        data.startRecnWindow = dialog
        if parent is not None:
            dialog.set_transient_for(parent)
        start_value = builder.get_object("start_value")
        start_value.set_text(sprintamount(ending, print_info))
        include_children_button = builder.get_object("subaccount_check")
        include_children_button.set_active(data.include_children)
        include_children_button.set_sensitive(enable_subaccount)

        date_value = DateSelection()
        date_value.set_date(statement_date)
        data.date_value = date_value
        box = builder.get_object("date_value_box")
        box.pack_start(date_value, True, True, 0)
        label = builder.get_object("date_label")
        date_value.make_mnemonic_target(label)

        end_value = AmountEntry()
        data.end_value = end_value
        data.original_value = new_ending
        data.user_set_value = False
        box = builder.get_object("ending_value_box")
        box.pack_start(end_value, True, True, 0)
        label = builder.get_object("end_label")
        label.set_mnemonic_widget(end_value)

        builder.connect_signals(data)
        date_value.activates_default(True)
        date_value.connect("date_changed", data.date_changed)
        print_info.use_symbol = 0
        end_value.set_print_info(print_info)
        end_value.set_amount(new_ending)

        end_value.select_region(0, -1)
        end_value.connect("focus-out-event", data.update_cb)
        end_value.set_activates_default(True)

        interest = builder.get_object("interest_button")
        if data.account_type.has_auto_interest_payment():
            interest.set_label("Enter _Interest Payment...")
        elif data.account_type.has_auto_interest_charge():
            interest.set_label("Enter _Interest Charge...")
        else:
            interest.destroy()
            interest = None

        if interest is not None:
            data.xfer_button = interest
            if auto_interest_xfer_option:
                interest.set_sensitive(False)
        dialog.show_all()
        end_value.grab_focus()
        if data.account_type.has_auto_interest_xfer() and auto_interest_xfer_option:
            data.interest_xfer_run()

        result = dialog.run()
        if result == Gtk.ResponseType.OK:
            new_ending = end_value.get_amount()
            statement_date = date_value.get_date_end()
            if gs_reverse_balance(account):
                new_ending = new_ending*-1
            account.set_reconcile_child_status(data.include_children)
            cls.save_reconcile_interval(account, statement_date)
        dialog.destroy()
        return result == Gtk.ResponseType.OK, new_ending, statement_date

    def set_sensitivity(self):
        sensitive = False
        if self.debit.num_selected() == 1:
            sensitive = True
        if self.credit.num_selected() == 1:
            sensitive = True
        action = self.action_group.get_action("TransEditAction")
        action.set_sensitive(sensitive)
        action = self.action_group.get_action("TransDeleteAction")
        action.set_sensitive(sensitive)
        sensitive = False
        if self.debit.num_selected() > 0:
            sensitive = True
        if self.credit.num_selected() > 0:
            sensitive = True
        action = self.action_group.get_action("TransRecAction")
        action.set_sensitive(sensitive)
        action = self.action_group.get_action("TransUnRecAction")
        action.set_sensitive(sensitive)

    def toggled_cb(self, view, split):
        self.set_sensitivity()
        self.recalculate_balance()

    def row_cb(self, view, item):
        self.set_sensitivity()

    def do_popup_menu(self, event):
        menu = self.ui_merge.get_widget("/MainPopup")
        if menu is None:
            return
        menu.popup_at_pointer(event)

    def popup_menu_cb(self, widget):
        self.do_popup_menu(None)
        return True

    def button_press_cb(self, widget, event):
        qview = widget
        if event.button == 3 and event.type == Gdk.EventButton.BUTTON_PRESS:
            path = qview.wget_path_at_pos(event.x, event.y, None, None, None)
            selection = qview.get_selection()
            selection.select_path(path)
            self.do_popup_menu(event)
            return True
        return False

    def open_register(self):
        account = self.account
        if account is None:
            return
        include_children = account.get_reconcile_children_status()
        page = RegisterPluginPage.new(account, include_children)
        self.window.open_page(page)
        gsr = page.get_gsr()
        gsr._raise()
        return gsr

    def double_click_cb(self, view, split):
        if split is None:
            return
        gsr = self.open_register()
        if gsr is None:
            return
        gsr.jump_to_split(split)

    def focus_cb(self, widget, event):
        debit = self.debit
        credit = self.credit
        other_view = credit if widget == debit else debit
        other_view.unselect_all()

    def key_press_cb(self, widget, event):
        if event.keyval != Gdk.KEY_Tab or event.keyval != Gdk.KEY_ISO_Left_Tab:
            return False
        widget.stop_emission_by_name("key_press_event")
        this_view = widget
        debit = self.debit
        credit = self.credit
        other_view = credit if this_view == debit else debit
        other_view.grab_focus()
        return True

    def set_titles(self):
        title = Account.get_debit_string(AccountType.NONE)
        self.debit_frame.set_label(title)

        title = Account.get_credit_string(AccountType.NONE)
        self.credit_frame.set_label(title)

    def create_view_box(self, account, type):
        frame = Gtk.Frame.new()
        if type == ReconcileViewType.DEBIT:
            self.debit_frame = frame
        else:
            self.credit_frame = frame

        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 5)
        vbox.set_homogeneous(False)

        view = ReconcileView(account, type, self.statement_date)
        view.connect("toggle_reconciled", self.toggled_cb)
        view.connect("line_selected", self.row_cb)
        view.connect("button_press_event", self.button_press_cb)
        view.connect("double_click_split", self.double_click_cb)
        view.connect("focus_in_event", self.focus_cb)
        view.connect("key_press_event", self._key_press_cb)

        scrollWin = Gtk.ScrolledWindow.new(None, None)
        scrollWin.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scrollWin.set_border_width(5)
        frame.add(scrollWin)
        scrollWin.add(view)
        vbox.pack_start(frame, True, True, 0)
        hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
        hbox.set_homogeneous(False)
        vbox.pack_start(hbox, False, False, 0)

        label = Gtk.Label("Total")
        gs_label_set_alignment(label, 1.0, 0.5)
        hbox.pack_start(label, True, True, 0)
        label = Gtk.Label.new("")
        hbox.pack_start(label, False, False, 0)
        label.set_margin_end(10)
        return vbox, view, label

    def get_current_split(self):
        view = self.debit
        split = view.get_current_split()
        if split is not None:
            return split
        view = self.credit
        split = view.get_current_split()
        return split

    def help_cb(self, widget):
        pass

    def change_cb(self, action):
        account = self.account
        new_ending = self.new_ending
        statement_date = self.statement_date
        if self.start(self.window, account, new_ending, statement_date, False):
            self.new_ending = new_ending
            self.statement_date = statement_date
            self.recalculate_balance()

    def balance_cb(self, button):
        gsr = self.open_register()
        if gsr is None:
            return

        account = self.account
        if account is None:
            return

        balancing_amount = self.recalculate_balance()
        if balancing_amount.is_zero():
            return
        statement_date = self.statement_date
        if statement_date == 0:
            statement_date = datetime.datetime.now()
        gsr.balancing_entry(account, statement_date, balancing_amount)

    def rec_cb(self, button):
        self.debit.set_list(True)
        self.credit.set_list(True)

    def unrec_cb(self, button):
        self.debit.set_list(False)
        self.credit.set_list(False)

    def delete_cb(self, button):
        split = self.get_current_split()
        if split is None:
            return
        message = "Are you sure you want to delete the selected transaction?"
        result = ask_yes_no(self.window, False, message)
        if result:
            return
        ComponentManager.suspend()
        trans = split.get_transaction()
        trans.destroy()
        ComponentManager.resume()

    def edit_cb(self, button):
        split = self.get_current_split()
        if split is None:
            return
        gsr = self.open_register()
        if gsr is None:
            return
        gsr.jump_to_split_amount(split)

    @staticmethod
    def make_window_name(account):
        fullname = account.get_full_name()
        title = " - ".join([fullname, "Reconcile"])
        return title

    def set_window_name(self):
        title = self.make_window_name(self.account)
        self.window.set_title(title)

    def edit_account_cb(self, action):
        if self.account is None:
            return
        dialog = AccountDialog.new_edit(self.window, self.account)
        dialog.run()

    def xfer_cb(self, action):
        if self.account is None:
            return
        TransferDialog(self.window, self.account)

    def scrub_cb(self, account):
        if account is None:
            return
        ComponentManager.suspend()
        Scrub.account_tree_orphans(account, Window.show_progress)
        Scrub.account_tree_imbalance(account, Window.show_progress)

        # if (g_getenv("gs_AUTO_SCRUB_LOTS") is not None)
        #     xaccAccountTreeScrubLots(account)

        ComponentManager.resume()

    def open_cb(self, action):
        self.open_register()

    @staticmethod
    def get_reconcile_info(account):
        always_today = gs_pref_get_bool(PREFS_GROUP_RECONCILE, PREF_ALWAYS_REC_TO_TODAY)
        statement_date = account.get_last_reconcile_date()
        if not always_today and statement_date:
            months, days = account.get_last_reconcile_interval()
            if months:
                was_last_day_of_month = date.is_last_of_month()
                statement_date += relativedelta(months=months)
                if was_last_day_of_month:
                    date.set_day(GLib.Date.get_days_in_month(date.get_month(), date.get_year()))
            else:
                statement_date += relativedelta(days=days)
            today = datetime.datetime.now()
            if statement_date > today:
                statement_date = today
        statement_date = account.get_reconcile_postpone_date()
        f, new_ending = account.get_reconcile_postpone_date()
        if f:
            if gs_reverse_balance(account):
                new_ending = new_ending*-1
        else:
            new_ending = gs_ui_account_get_balance_as_of_date(account, statement_date,
                                                              account.get_reconcile_child_status())
        return new_ending, statement_date

    @staticmethod
    def find_by_account(account, ld):
        if account is None or not ld: return False
        return account == ld.account

    def set_watches_one_account(self, account):
        for split in account.get_splits():
            recn = split.get_reconcile_state()
            if recn == SplitState.UNRECONCILED or recn == SplitState.CLEARED:
                trans = split.get_transaction()
                ComponentManager.watch_entity(self.component_id, trans.get_guid(),
                                              EVENT_MODIFY
                                              | EVENT_DESTROY
                                              | EVENT_ITEM_CHANGED)

    def set_watches(self):
        accounts = []
        ComponentManager.clear_watches(self.component_id)
        ComponentManager.watch_entity(self.component_id, self.account.guid, EVENT_MODIFY | EVENT_DESTROY)
        account = self.account
        include_children = account.get_reconcile_children_status()
        if include_children:
            accounts = account.get_descendants()
        accounts.insert(0, account)
        for acc in accounts:
            self.set_watches_one_account(acc)

    def refresh_handler(self, changes):
        account = self.account
        if account is None:
            ComponentManager.close(self.component_id)
            return
        if changes:
            info = ComponentManager.get_entity_events(changes, self.account)
            if info and (info.event_mask & EVENT_DESTROY):
                ComponentManager.close(self.component_id)
                return

    # def set_titles(self):
    #     self.set_watches()
    #     self.refresh()

    def close_handler(self):
        gs_save_window_size(PREFS_GROUP_RECONCILE, self.window)
        self.window.destroy()

    @classmethod
    def new(cls, parent, account):
        if account is None:
            return
        if not cls.last_statement_date:
            statement_date = datetime.datetime.now()
        else:
            statement_date = cls.last_statement_date
        new_ending, statement_date = cls.get_reconcile_info(account)
        f, new_ending, statement_date = cls.start(parent, account, new_ending, statement_date, True)
        if not f:
            return None
        return cls.new_with_balance(parent, account, new_ending, statement_date)

    @staticmethod
    def add_widget(merge, widget, dock):
        dock.pack_start(widget, False, False, 0)
        widget.show()

    @classmethod
    def new_with_balance(cls, parent, account, new_ending, statement_date):
        if account is None:
            return None
        self = ComponentManager.find_first(WINDOW_RECONCILE_CM_CLASS, cls.find_by_account, account)
        if self is not None:
            return self
        self = cls()
        self.account = account
        self.component_id = ComponentManager.register(WINDOW_RECONCILE_CM_CLASS,
                                                      cls.refresh_handler, cls.close_handler)
        self.set_watches()
        cls.last_statement_date = statement_date
        self.new_ending = new_ending
        self.statement_date = statement_date
        self.window = Gtk.Window.new(Gtk.WindowType.TOP_LEVEL)
        self.delete_refresh = False
        self.set_window_name()

        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        vbox.set_homogeneous(False)
        self.window.add(vbox)
        self.window.set_name("gs-id-reconcile2")

        dock = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        dock.set_homogeneous(False)
        dock.show()
        vbox.pack_start(dock, False, True, 0)

        self.ui_merge = Gtk.UIManager.new()
        self.ui_merge.connect("add_widget", cls.add_widget, dock)

        action_group = Gtk.ActionGroup.new("ReconcileWindowActions")
        self.action_group = action_group
        # gtk_action_group_set_translation_domain (action_group, PROJECT_NAME)
        action_group.add_actions(self.actions)
        action = action_group.get_action("AccountOpenAccountAction")
        action.set_property("short_label", "Open")

        self.ui_merge.insert_action_group(action_group, 0)
        merge_id = self.ui_merge.add_ui_from_resource('/org/jonah/Gasstation/ui/reconcile-window-ui.xml')
        if merge_id:
            self.window.add_accel_group(self.ui_merge.get_accel_group())
            self.ui_merge.ensure_update()
        else:
            assert (merge_id is not 0)
        self.window.connect("popup-menu", self.popup_menu_cb)

        statusbar = Gtk.Statusbar.new()
        vbox.pack_end(statusbar, False, False, 0)
        self.window.connect("destroy", self.destroy_cb)
        self.window.connect("delete_event", self._delete_cb)
        self.window.connect("key_press_event", self.key_press_cb)

        frame = Gtk.Frame()
        main_area = Gtk.Box.new(Gtk.Orientation.VERTICAL, 10)
        debcred_area = Gtk.Grid()
        main_area.set_homogeneous(False)
        vbox.pack_start(frame, True, True, 10)
        self.window.set_default_size(800, 600)
        gs_restore_window_size(PREFS_GROUP_RECONCILE, self.window, parent)
        frame.add(main_area)
        main_area.set_border_width(10)

        debits_box, self.debit, self.total_debit = self.create_view_box(account, ReconcileViewType.DEBIT)
        gs_widget_set_style_context(debits_box, "gs-class-debits")
        credits_box, self.credit, self.total_credit = self.create_view_box(account, ReconcileViewType.CREDIT)
        gs_widget_set_style_context(credits_box, "gs-class-credits")
        self.debit.sibling = self.credit
        self.credit.sibling = self.debit
        main_area.pack_start(debcred_area, True, True, 0)
        debcred_area.set_column_homogeneous(True)
        debcred_area.set_column_spacing(15)
        debcred_area.attach(debits_box, 0, 0, 1, 1)
        debits_box.set_hexpand(True)
        debits_box.set_vexpand(True)
        debits_box.set_halign(Gtk.Align.FILL)
        debits_box.set_valign(Gtk.Align.FILL)
        debcred_area.attach(credits_box, 1, 0, 1, 1)
        credits_box.set_hexpand(True)
        credits_box.set_vexpand(True)
        credits_box.set_halign(Gtk.Align.FILL)
        credits_box.set_valign(Gtk.Align.FILL)

        hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
        hbox.set_homogeneous(False)
        main_area.pack_start(hbox, False, False, 0)

        frame = Gtk.Frame()
        hbox.pack_end(frame, False, False, 0)
        frame.set_name("gs-id-reconcile-totals")

        totals_hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 3)
        totals_hbox.set_homogeneous(False)
        frame.add(totals_hbox)
        totals_hbox.set_border_width(5)

        title_vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 3)
        title_vbox.set_homogeneous(False)
        totals_hbox.pack_start(title_vbox, False, False, 0)

        value_vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 3)
        value_vbox.set_homogeneous(False)
        totals_hbox.pack_start(value_vbox, True, True, 0)

        title = Gtk.Label("Statement Date")
        gs_label_set_alignment(title, 1.0, 0.5)
        title_vbox.pack_start(title, False, False, 0)

        value = Gtk.Label("")
        self.recn_date = value
        gs_label_set_alignment(value, 1.0, 0.5)
        value_vbox.pack_start(value, False, False, 0)

        title = Gtk.Label("Starting Balance")
        gs_label_set_alignment(title, 1.0, 0.5)
        title_vbox.pack_start(title, False, False, 3)

        value = Gtk.Label("")
        self.starting = value
        gs_label_set_alignment(value, 1.0, 0.5)
        value_vbox.pack_start(value, False, False, 3)

        title = Gtk.Label("Ending Balance")
        gs_label_set_alignment(title, 1.0, 0.5)
        title_vbox.pack_start(title, False, False, 0)

        value = Gtk.Label("")
        self.ending = value
        gs_label_set_alignment(value, 1.0, 0.5)
        value_vbox.pack_start(value, False, False, 0)

        title = Gtk.Label("Reconciled Balance")
        gs_label_set_alignment(title, 1.0, 0.5)
        title_vbox.pack_start(title, False, False, 0)

        value = Gtk.Label("")
        self.reconciled = value
        gs_label_set_alignment(value, 1.0, 0.5)
        value_vbox.pack_start(value, False, False, 0)

        title = Gtk.Label("Difference")
        gs_label_set_alignment(title, 1.0, 0.5)
        title_vbox.pack_start(title, False, False, 0)

        value = Gtk.Label("")
        self.difference = value
        gs_label_set_alignment(value, 1.0, 0.5)
        value_vbox.pack_start(value, False, False, 0)
        self.refresh()
        self.window.set_resizable(True)
        self.window.show_all()
        self.set_titles()
        self.recalculate_balance()
        gs_window_adjust_for_screen(self.window)
        self.debit.sort_order(ReconcileViewColumn.DATE, Gtk.SortType.ASCENDING)
        self.credit.sort_order(ReconcileViewColumn.DATE, Gtk.SortType.ASCENDING)
        self.debit.grab_focus()
        return self

    def _raise(self):
        if self.window is None:
            return
        self.window.present()

    def destroy_cb(self, w):
        ComponentManager.close(self.component_id)
        if self.delete_refresh:
            ComponentManager.resume()

    def cancel(self):
        changed = False
        if self.credit.changed():
            changed = True
        if self.debit.changed():
            changed = True
        if changed:
            message = "You have made changes to this reconcile window. Are you sure you want to cancel?"
            if not ask_yes_no(self.window, False, message):
                return
        ComponentManager.close(self.component_id)

    def _delete_cb(self, widget, event):
        self.cancel()
        return True

    def _key_press_cb(self, widget, event):
        if event.keyval == Gdk.KEY_Escape:
            self.cancel()
            return True
        else:
            return False

    @staticmethod
    def find_payment_account(account):
        if account is None:
            return None
        lis = account.get_splits()
        for split in lis[::-1]:
            if not split.get_amount()> 0:
                continue
            trans = split.get_transaction()
            if trans is None:
                continue

            for sp in trans.get_splits():
                if sp == split:
                    continue
                a = sp.get_account()
                if a is None or a == account:
                    continue
                _type = a.get_type()
                if _type == AccountType.BANK or _type == AccountType.CASH or \
                        _type == AccountType.CURRENT_ASSET or _type == AccountType.FIXED_ASSET:
                    return a
        return None

    def finish_cb(self, action):
        if not self.recalculate_balance().is_zero():
            message = "The account is not balanced. " \
                      "Are you sure you want to finish?"
            if not ask_yes_no(self.window, False, message):
                return
        date = self.statement_date
        ComponentManager.suspend()
        self.delete_refresh = True
        self.credit.commit(date)
        self.debit.commit(date)
        auto_payment = gs_pref_get_bool(PREFS_GROUP_RECONCILE, PREF_AUTO_CC_PAYMENT)
        account = self.account
        account.clear_reconcile_postpone()
        account.set_last_reconcile_date(date)
        if auto_payment and account.get_type() == AccountType.CREDIT and self.new_ending < 0:
            xfer = TransferDialog(self.window, account)
            xfer.set_amount(self.new_ending*-1)
            payment_account = self.find_payment_account(account)
            if payment_account is not None:
                xfer.select_from_account(payment_account)
        ComponentManager.close(self.component_id)

    def postpone_cb(self, action):
        message = "Do you want to postpone this reconciliation and finish it later?"
        if not ask_yes_no(self.window, False, message):
            return
        ComponentManager.suspend()
        self.delete_refresh = True
        self.credit.postpone()
        self.debit.postpone()
        account = self.account
        account.set_reconcile_postpone_date(self.statement_date)
        account.set_reconcile_postpone_balance(self.new_ending)
        ComponentManager.close(self.component_id)

    def cancel_cb(self, action):
        self.cancel()
