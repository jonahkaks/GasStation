from collections import defaultdict

from gasstation.utilities.warnings import warnings
from gi.repository import GObject

from gasstation.utilities.dialog import *
from libgasstation.core.component import ComponentManager

PREFS_GROUP = "dialogs.reset-warnings"
DIALOG_RESET_WARNINCM_CLASS = "reset-warnings"
TIPS_STRING = "tips"


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/reset-warnings.ui')
class ResetWarningsDialog(Gtk.Dialog):
    __gtype_name__ = "ResetWarningsDialog"
    perm_vbox_and_label = Gtk.Template.Child()
    perm_vbox = Gtk.Template.Child()
    temp_vbox_and_label = Gtk.Template.Child()
    hbuttonbox = Gtk.Template.Child()
    temp_vbox = Gtk.Template.Child()
    no_warnings = Gtk.Template.Child()
    applybutton = Gtk.Template.Child()

    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if ComponentManager.forall(DIALOG_RESET_WARNINCM_CLASS, self.show_handler, None):
            return
        self.user_data = defaultdict(dict)
        gs_widget_set_style_context(self, "GncResetWarningsDialog")
        self.set_transient_for(parent)
        self.add_section(PREFS_GROUP_WARNINPERM, self.perm_vbox)
        self.add_section(PREFS_GROUP_WARNINTEMP, self.temp_vbox)
        self.update_widgets()
        gs_restore_window_size(PREFS_GROUP, self, parent)
        self.component_id = ComponentManager.register(DIALOG_RESET_WARNINCM_CLASS, None, self.close_handler)
        self.show()

    def update_widgets(self, *args):
        checked = False
        a = False
        lis = self.perm_vbox.get_children()
        if any(lis):
            self.perm_vbox_and_label.show()
            for p in lis:
                if p.get_active():
                    checked = True
                    break
            a = True
        else:
            self.perm_vbox_and_label.hide()

        lis = self.temp_vbox.get_children()
        if any(lis):
            self.temp_vbox_and_label.show()
            for p in lis:
                if p.get_active():
                    checked = True
                    break
            a = True

        else:
            self.temp_vbox_and_label.hide()

        if a:
            self.hbuttonbox.show()
            self.no_warnings.hide()
            self.applybutton.set_sensitive(checked)
        else:
            self.hbuttonbox.hide()
            self.no_warnings.show()
            self.applybutton.set_sensitive(False)

    def apply_one(self, widget):
        if not widget.get_active():
            return
        pref = widget.get_name()
        prefs_group = self.user_data[widget]["prefs-group"]
        if prefs_group is not None:
            gs_pref_reset(prefs_group, pref)
        widget.destroy()

    def apply_changes(self):
        self.perm_vbox.foreach(self.apply_one)
        self.temp_vbox.foreach(self.apply_one)
        self.update_widgets()

    @Gtk.Template.Callback()
    def response_cb(self, dialog, response):
        if response == Gtk.ResponseType.APPLY:
            self.apply_changes()
        elif response == Gtk.ResponseType.OK:
            self.apply_changes()
            gs_save_window_size(PREFS_GROUP, self)
            ComponentManager.unregister(self.component_id)
            self.destroy()
        else:
            ComponentManager.unregister(self.component_id)
            self.destroy()

    def select_common(self, selected):
        self.perm_vbox.foreach(Gtk.ToggleButton.set_active, selected)
        self.temp_vbox.foreach(Gtk.ToggleButton.set_active, selected)
        self.update_widgets()

    @Gtk.Template.Callback()
    def select_all_cb(self, button):
        self.select_common(True)

    @Gtk.Template.Callback()
    def unselect_all_cb(self, button):
        self.select_common(False)

    def add_one(self, prefs_group, warning, box):
        checkbox = Gtk.CheckButton.new_with_label(warning.desc if warning.desc is not None else warning.name)
        if warning.long_desc is not None:
            checkbox.set_tooltip_text(warning.long_desc)
        checkbox.set_name(warning.name)
        self.user_data[checkbox]["prefs-group"] = prefs_group
        checkbox.connect("toggled", self.update_widgets)
        box.pack_start(checkbox, True, True, 0)
        checkbox.show()

    def add_section(self, prefs_group, box):
        for warn in warnings:
            if gs_pref_get_int(prefs_group, warn.name) != 0:
                self.add_one(prefs_group, warn, box)

    def show_handler(self, class_name, component_id):
        self.present()
        return True

    def close_handler(self):
        ComponentManager.unregister(self.component_id)
        self.destroy()


GObject.type_register(ResetWarningsDialog)
