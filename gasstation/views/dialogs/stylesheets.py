from gasstation.utilities.custom_dialogs import RESPONSE_NEW, RESPONSE_EDIT, RESPONSE_DELETE
from gasstation.views.reports.report import Report
from libgasstation.html.stylesheets import *
from .options import *


class SsInfo:
    def __init__(self):
        self.odialog = None
        self.odb = None
        self.stylesheet = None
        self.row_ref = None


class StyleSheetsDialog:
    ss_dialog = None

    def __init__(self):
        self.toplevel = None
        self.list_view = None
        self.list_store = None
        self.options_frame = None

    @classmethod
    def dirty_same_stylesheet(cls, report, dirty_ss):
        rep_ss = report.get_stylesheet()
        if rep_ss == dirty_ss:
            report.set_dirty(True)

    @classmethod
    def options_apply_cb(cls, propertybox, ssi):
        reports = Report.get()
        if any(reports):
            for k in reports:
                cls.dirty_same_stylesheet(k, ssi.stylesheet)
        results = ssi.odb.commit()
        for mess in results:
            dialog = Gtk.MessageDialog(transient_for=None, modal=True,
                                       destroy_with_parent=True,
                                       message_type=Gtk.MessageType.ERROR,
                                       buttons=Gtk.ButtonsType.OK,
                                       message_format=mess)
            dialog.run()
            dialog.destroy()

    @classmethod
    def options_close_cb(cls, propertybox, ssi):
        if Gtk.TreeRowReference.valid(ssi.row_ref):
            ss = cls.ss_dialog
            path = ssi.row_ref.get_path()
            _iter = ss.list_store.get_iter(path)
            if _iter is not None:
                ss.list_store.set_value(_iter, 2, None)
            ssi.odialog.destroy()
            ssi.odb.destroy()

    def _create(self, name, sheet_info, row_ref):
        options = sheet_info.get_options()
        ssinfo = SsInfo()
        parent = self.list_view.get_toplevel()
        title = "HTML Style Sheet Properties: %s" % name
        ssinfo.odialog = OptionDialog.new(title, parent)
        ssinfo.odb = OptionDB(options)
        ssinfo.stylesheet = sheet_info
        ssinfo.row_ref = row_ref
        ssinfo.odialog.build_contents(ssinfo.odb)
        ssinfo.odialog.set_apply_cb(self.options_apply_cb, ssinfo)
        ssinfo.odialog.set_close_cb(self.options_close_cb, ssinfo)
        window = ssinfo.odialog.window
        window.set_transient_for(self.toplevel)
        window.set_destroy_with_parent(True)
        window.present()
        return ssinfo

    def new(self):
        new_ss = None
        templates = HtmlStyleSheetTemplate.get()
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/report.ui",
                                          ["template_liststore", "new_style_sheet_dialog"])
        dlg = builder.get_object("new_style_sheet_dialog")
        template_combo = builder.get_object("template_combobox")
        name_entry = builder.get_object("name_entry")
        gs_widget_set_style_context(dlg, "StyleSheetDialog")
        template_model = template_combo.get_model()
        template_model.clear()
        for template in templates:
            template_model.append([template.get_name()])
        template_combo.set_active(0)
        dlg.set_transient_for(self.toplevel)
        dialog_retval = dlg.run()
        if dialog_retval == Gtk.ResponseType.OK:
            choice = template_combo.get_active()
            template = templates[choice]
            name_str = name_entry.get_text()
            if len(name_str) == 0:
                show_error(self.toplevel, "You must provide a name for the new style sheet.")
                name_str = None
            new_ss = HtmlStyleSheet(name_str, template)
        dlg.destroy()
        return new_ss

    def add_one(self, sheet_info, select):
        cname = sheet_info.get_name()
        if cname is None:
            return
        _iter = self.list_store.append([cname, sheet_info, None])
        if select:
            selection = self.list_view.get_selection()
            selection.select_iter(_iter)

    def fill(self):
        stylesheets = HtmlStyleSheet.get_sheets()
        for sheet_info in stylesheets:
            self.add_one(sheet_info, False)

    def response_cb(self, unused, response):
        if response == RESPONSE_NEW:
            sheet_info = self.new()
            if sheet_info is None:
                return
            self.add_one(sheet_info, True)

        elif response == RESPONSE_EDIT:
            selection = self.list_view.get_selection()
            model, _iter = selection.get_selected()
            if _iter is not None:
                name = model.get_value(_iter, 0)
                sheet_info = model.get_value(_iter, 1)
                path = self.list_store.get_path(_iter)
                row_ref = Gtk.TreeRowReference(self.list_store, path)
                ssinfo = self._create(name, sheet_info, row_ref)
                self.list_store.set_value(_iter, 2, ssinfo)

        elif response == RESPONSE_DELETE:
            selection = self.list_view.get_selection()
            model, _iter = selection.get_selected()
            if _iter is not None:
                sheet_info = model.get_value(_iter, 1)
                ssinfo = model.get_value(_iter, 2)
                self.list_store.remove(_iter)
                if ssinfo is not None:
                    self.options_close_cb(None, ssinfo)
                HtmlStyleSheet.remove(sheet_info)

        else:
            self.toplevel.destroy()
            StyleSheetsDialog.ss_dialog = None

    def event_cb(self, widget, event):
        if event is None:
            return
        if event.type != Gdk.EventType._2BUTTON_PRESS:
            return
        self.response_cb(None, RESPONSE_EDIT)

    @classmethod
    def create(cls, parent):
        ss = cls()
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/report.ui",
                                          ["select_style_sheet_dialog"])
        ss.toplevel = builder.get_object("select_style_sheet_dialog")
        ss.toplevel.set_transient_for(parent)
        gs_widget_set_style_context(ss.toplevel, "StyleSheetDialog")
        ss.list_view = builder.get_object("style_sheet_list_view")
        ss.list_store = Gtk.ListStore(str, object, object)
        ss.list_view.set_model(ss.list_store)
        renderer = Gtk.CellRendererText()
        ss.list_view.append_column(Gtk.TreeViewColumn("Style Sheet Name", renderer, text=0))
        selection = ss.list_view.get_selection()
        selection.set_mode(Gtk.SelectionMode.BROWSE)
        ss.toplevel.connect("response", ss.response_cb)
        ss.list_view.connect("event-after", ss.event_cb)
        ss.fill()
        ss.toplevel.show_all()
        return ss

    @classmethod
    def open(cls, parent):
        if cls.ss_dialog is not None:
            cls.ss_dialog.toplevel.present()
        else:
            cls.ss_dialog = cls.create(parent)
