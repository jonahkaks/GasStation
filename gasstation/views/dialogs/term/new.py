from gi.repository import GObject

from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import *
from libgasstation.core.component import *
from libgasstation.core.numeric import *
from libgasstation.core.term import TermType, Term, DiscountType


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/term/new.ui")
class NewPaymentTermDialog(Gtk.Dialog):
    __gtype_name__ = "NewPaymentTermDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()
    description_entry: Gtk.Entry = Gtk.Template.Child()
    type_combo: Gtk.ComboBoxText = Gtk.Template.Child()
    notebook: Gtk.Notebook = Gtk.Template.Child()
    term_grid: Gtk.Grid = Gtk.Template.Child()

    days_due_days: Gtk.SpinButton = Gtk.Template.Child()
    days_discount_days: Gtk.SpinButton = Gtk.Template.Child()
    days_discount_type: Gtk.ComboBox = Gtk.Template.Child()
    days_discount: Gtk.SpinButton = Gtk.Template.Child()

    prox_due_day: Gtk.SpinButton = Gtk.Template.Child()
    prox_discount_day: Gtk.SpinButton = Gtk.Template.Child()
    prox_discount: Gtk.SpinButton = Gtk.Template.Child()
    prox_discount_type: Gtk.ComboBox = Gtk.Template.Child()
    prox_cutoff_day: Gtk.SpinButton = Gtk.Template.Child()

    def __init__(self, parent, book, term=None, name=None):
        super().__init__()
        self.book = book
        self.set_transient_for(parent)
        self.term = term
        gs_widget_set_style_context(self, "NewTaxDialog")
        if name is not None:
            self.name_entry.set_text(name)
        if term is not None:
            self.term_grid.remove_row(0)
            self.term_grid.remove_row(0)
            self.description_entry.grab_focus()
            self.to_ui()
        else:
            self.name_entry.grab_focus()

    def to_term(self):
        if self.term is None:
            self.term = Term(self.book)
        self.term.begin_edit()
        name = self.name_entry.get_text()
        self.term.set_name(name.strip())
        description = self.description_entry.get_text()
        self.term.set_description(description.strip())
        _type = TermType(self.type_combo.get_active())
        self.term.set_type(_type)
        if _type == TermType.DAYS:
            self.term.set_due_days(self.days_due_days.get_value_as_int())
            self.term.set_discount_days(self.days_discount_days.get_value_as_int())
            self.term.set_discount_type(DiscountType(self.days_discount_type.get_active()))
            self.term.set_discount(Decimal(self.days_discount.get_value()))
        elif _type == TermType.PROXIMO:
            self.term.set_due_days(self.prox_due_day.get_value_as_int())
            self.term.set_discount_days(self.prox_discount_day.get_value_as_int())
            self.term.set_discount_type(DiscountType(self.prox_discount_type.get_active()))
            self.term.set_discount(Decimal(self.prox_discount.get_value()))
            self.term.set_cutoff(self.prox_cutoff_day.get_value_as_int())

    def to_ui(self):
        if self.term is not None:
            self.name_entry.set_text(self.term.get_name())
            self.description_entry.set_text(self.term.get_description())
            _type = self.term.get_type()
            self.type_combo.set_active(_type)
            if _type == TermType.DAYS:
                self.days_due_days.set_value(self.term.get_due_days())
                self.days_discount_days.set_value(self.term.get_discount_days())
                self.days_discount_type.set_active(self.term.get_discount_type())
                self.days_discount.set_value(float(self.term.get_discount()))

            elif _type == TermType.PROXIMO:
                self.prox_due_day.set_value(self.term.get_due_days())
                self.prox_discount_day.set_value(self.term.get_discount_days())
                self.prox_discount_type.set_active(self.term.get_discount_type())
                self.prox_discount.set_value(float(self.term.get_discount()))
                self.prox_cutoff_day.set_value(self.term.get_cutoff())

    @Gtk.Template.Callback()
    def response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            if self.ok_cb():
                dialog.destroy()
                return True
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            dialog.destroy()
            return True

        return False

    @Gtk.Template.Callback()
    def type_combo_changed(self, widget):
        index = widget.get_active()
        self.notebook.set_current_page(index)

    def verify_term_ok(self):
        message = "Discount days cannot be more than due days."
        days_due_days = self.days_due_days.get_value_as_int()
        days_disc_days = self.days_discount_days.get_value_as_int()
        prox_due_days = self.prox_due_day.get_value_as_int()
        prox_disc_days = self.prox_discount_day.get_value_as_int()

        _type = TermType(self.type_combo.get_active())
        if _type == TermType.DAYS:
            if days_due_days < days_disc_days:
                show_error(self, message)
                return False
        elif _type == TermType.PROXIMO:
            if prox_due_days < prox_disc_days:
                show_error(self, message)
                return False
        return True

    def ok_cb(self):
        name = self.name_entry.get_text()
        if name is None or name == "":
            message = "You must provide a name for this Term."
            show_error(self, message)
            return False
        if Term.lookup_by_name(self.book, name):
            message = "You must provide a unique name for this Term." \
                      "Your choice \"%s\" is already in use." % name
            show_error(self, message)
            return False

        if not self.verify_term_ok():
            return False
        ComponentManager.suspend()
        self.to_term()
        self.term.commit_edit()
        ComponentManager.resume()
        return True


GObject.type_register(NewPaymentTermDialog)
