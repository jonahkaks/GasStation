from enum import IntEnum

from gi.repository import GObject

from gasstation.utilities.cursors import *
from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import gs_widget_set_style_context
from libgasstation import Term, EVENT_MODIFY, ID_TERM, EVENT_DESTROY, EVENT_CREATE
from libgasstation.core.component import ComponentManager
from libgasstation.core.session import Session
from .new import NewPaymentTermDialog

DIALOG_TERMS_CM_CLASS = "terms-dialog"


class TermCol(IntEnum):
    NAME = 0
    TERM = 1
    NUM_COLS = 2

DIALOG_termS_CM_CLASS = "dialog-terms"
STATE_SECTION = "dialogs/edit_terms"


class PaymentTermsDialog(GObject.Object):
    def __init__(self):
        GObject.Object.__init__(self)
        self.dialog = None
        self.session = None
        self.book = None
        self.terms_tree = None
        self.edit_button = None
        self.remove_button = None
        self.component_id = 0
        self.current_term = None

    def window_destroy_cb(self, widget):
        ComponentManager.unregister(self.component_id)

    def edit_clicked(self):
        book = Session.get_current_book()
        NewPaymentTermDialog(self.dialog, book, term=self.current_term).run()
        ComponentManager.refresh_all()

    def row_activated_cb(self, view, path, column):
        self.edit_clicked()

    def remove_clicked(self):
        from gasstation.views.dialogs.object_references import ObjectReferencesDialog

        dialog = Gtk.MessageDialog(transient_for=self.dialog, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE,
                                   message_format="The following terms will be deleted %s" % self.current_term.get_name())
        dialog.add_buttons("Cancel", Gtk.ResponseType.CANCEL, "Delete", Gtk.ResponseType.ACCEPT)
        dialog.set_default_response(Gtk.ResponseType.CANCEL)
        response = dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            gs_set_busy_cursor(None, True)
            ComponentManager.suspend()
            term = self.current_term
            lis = term.get_referrers()
            if len(lis):
                exp = "The list below shows objects which make use of the term which you want to delete.\n" \
                      "Before you can delete it, you must either delete those objects or else modify them so they " \
                      "make use of another term"
                ObjectReferencesDialog(exp, lis)
                ComponentManager.resume()
                gs_unset_busy_cursor(None)
                return
            term.begin_edit()
            term.destroy()
            ComponentManager.resume()
            gs_unset_busy_cursor(None)

    def add_clicked(self):
        dialog = NewPaymentTermDialog(self.dialog, Session.get_current_book())
        dialog.run()

    def dialog_response(self, dialog, response):
        if response == RESPONSE_NEW:
            self.add_clicked()
            return

        elif response == RESPONSE_DELETE:
            self.remove_clicked()
            return

        elif response == RESPONSE_EDIT:
            self.edit_clicked()
            return
        else:
            ComponentManager.close(self.component_id)

    def create(self, parent):
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/common.ui", ["common_dialog"])
        self.dialog = builder.get_object("common_dialog")
        self.dialog.set_title("Payment Terms Dialog")
        self.session = Session.get_current_session()
        self.book = self.session.get_book()
        gs_widget_set_style_context(self.dialog, "PaymentTermsDialog")
        builder.connect_signals(self)
        if parent is not None:
            self.dialog.set_transient_for(parent)
        self.remove_button = builder.get_object("remove_button")
        self.edit_button = builder.get_object("edit_button")
        scrolled_window = builder.get_object("common_list_window")
        view = Gtk.TreeView()
        self.terms_tree = view
        scrolled_window.add(view)
        scrolled_window.show()
        view.set_headers_visible(True)
        selection = view.get_selection()
        selection.connect("changed", self.selection_changed)
        self.terms_tree.connect("row-activated", self.row_activated_cb)
        self.dialog.set_transient_for(parent)

        store = Gtk.ListStore(str, object)
        view.set_model(store)

        renderer = Gtk.CellRendererText.new()
        column = Gtk.TreeViewColumn("Name")
        column.pack_start(renderer, True)
        column.add_attribute(renderer, "text", TermCol.NAME)
        view.append_column(column)

        view.connect("row-activated", self.selection_activated)
        selection = view.get_selection()
        selection.connect("changed", self.selection_changed)

        builder.connect_signals(self)
        self.component_id = ComponentManager.register(DIALOG_TERMS_CM_CLASS, self.refresh_handler,
                                                      self.close_handler)

        self.refresh()
        self.dialog.show_all()

    def show_handler(self, klass):
        self.dialog.present()
        return True

    def refresh(self):
        reference = None
        view = self.terms_tree
        store = view.get_model()
        selection = view.get_selection()
        store.clear()
        ComponentManager.clear_watches(self.component_id)
        lis = Term.get_terms(self.book)
        lis = reversed(lis.copy())
        for term in lis:
            if term.get_parent() is None:
                ComponentManager.watch_entity(self.component_id, term.get_guid(), EVENT_MODIFY)
                _iter = store.prepend([term.get_name(), term])
                if term == self.current_term:
                    path = store.get_path(_iter)
                    reference = Gtk.TreeRowReference.new(store, path)

        ComponentManager.watch_entity_type(self.component_id, ID_TERM, EVENT_CREATE | EVENT_DESTROY)

        if reference is not None:
            path = reference.get_path()
            if path is not None:
                selection.select_path(path)
                view.scroll_to_cell(path, None, True, 0.5, 0.0)
        else:
            _iter = store.get_iter_first()
            if _iter is not None:
                selection.select_iter(_iter)

    def selection_changed(self, selection):
        model, _iter = selection.get_selected()
        term = None
        if _iter is not None:
            term = model.get_value(_iter, TermCol.TERM)
        if term != self.current_term:
            self.current_term = term
        self.edit_button.set_sensitive(term is not None)
        self.remove_button.set_sensitive(term is not None)

    def selection_activated(self, tree_view, path, column):
        dialog = NewPaymentTermDialog(self.dialog, self.book, self.current_term)
        dialog.run()

    def refresh_handler(self, changes):
        self.refresh()

    def close_handler(self):
        self.dialog.destroy()

    def close(self, widget):
        self.window_destroy()

    def destroy_cb(self, widget):
        ComponentManager.unregister(self.component_id)

    @staticmethod
    def find_handler(data, book):
        return data is not None and data[0] == book

    @classmethod
    def new(cls, parent, book):
        if book is None:
            return
        self = ComponentManager.find_first(DIALOG_TERMS_CM_CLASS, cls.find_handler, book)
        if self is not None:
            self.dialog.present()
            return self
        self = cls()
        self.book = book
        self.create(parent)
        return self.dialog

    def window_destroy(self):
        ComponentManager.close(self.component_id)
