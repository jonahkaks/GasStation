from gi.repository import GObject

from gasstation.utilities.commodity import *
from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.utilities.gtk import *
from libgasstation.core.session import *
from .new import CommodityDialog


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/commodity/selector.ui")
class CommoditySelectorDialog(Gtk.Dialog):
    __gtype_name__ = "CommoditySelectorDialog"
    namespace_combo: Gtk.ComboBox = Gtk.Template.Child()
    commodity_combo: Gtk.ComboBox = Gtk.Template.Child()
    item_label: Gtk.Label = Gtk.Template.Child()
    select_user_prompt: Gtk.Label = Gtk.Template.Child()
    ok_button: Gtk.Button = Gtk.Template.Child()
    new_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, parent, orig_sel, mode, user_message=None, cusip=None, full_name=None, mnemonic=None):
        super().__init__()
        self.selection = None
        if parent is not None:
            self.set_transient_for(parent)
        gs_widget_set_style_context(self, "CommoditySelectorDialog")
        Combo.require_list_item(self.namespace_combo)
        Combo.require_list_item(self.commodity_combo)
        self.namespace_combo.connect("changed", self.namespace_changed_cb)
        self.commodity_combo.connect("changed", self.changed_cb)
        if mode == CommodityMode.ALL:
            title = "Select security/currency"
            text = "_Security/currency:"
        elif mode == CommodityMode.NON_CURRENCY or mode == CommodityMode.NON_CURRENCY_SELECT:
            title = "Select security"
            text = "_Security:"
        else:
            title = "Select currency"
            text = "Cu_rrency:"
            self.new_button.destroy()

        self.set_title(title)
        self.item_label.set_text_with_mnemonic(text)
        update_namespace_picker(self.namespace_combo, orig_sel.get_namespace() if orig_sel else None, mode)
        name_space = namespace_picker_ns(self.namespace_combo)
        update_commodity_picker(self.commodity_combo, name_space,
                                orig_sel.get_printname() if orig_sel is not None else None)
        self.default_cusip = cusip
        self.default_full_name = full_name
        self.default_mnemonic = mnemonic
        self.default_user_symbol = ""
        self.default_fraction = 1000

        if user_message is not None:
            initial = user_message
        elif (cusip is not None) or (full_name is not None) or (mnemonic is not None):
            initial = "\nPlease select a commodity to match:"
        else:
            initial = ""
        user_prompt_text = "%s%s%s%s%s%s%s" % (initial, "\nCommodity: " if full_name is not None else "",
                                               full_name if full_name is not None else "",
                                               "\nExchange code (ISIN, CUSIP or similar): " if cusip is not None else "",
                                               cusip if cusip is not None else "",
                                               "\nMnemonic (Ticker symbol or similar): " if mnemonic is not None else "",
                                               mnemonic if mnemonic is not None else "")
        self.select_user_prompt.set_text(user_prompt_text)

    def new_cb(self):
        name_space = namespace_picker_ns(self.namespace_combo)
        new_commodity = CommodityDialog.common_commodity_modal(None, self, name_space, self.default_cusip,
                                                               self.default_full_name, self.default_mnemonic,
                                                               self.default_user_symbol, self.default_fraction)
        if new_commodity:
            update_namespace_picker(self.namespace_combo,
                                    new_commodity.get_namespace(),
                                    CommodityMode.ALL)
            update_commodity_picker(self.commodity_combo,
                                    new_commodity.get_namespace(),
                                    new_commodity.get_printname())

    def changed_cb(self, cbwe):
        name_space = namespace_picker_ns(self.namespace_combo)
        full_name = self.commodity_combo.get_child().get_text()
        self.selection = Session.get_current_commodity_table().find_full(name_space, full_name)
        ok = (self.selection is not None)
        self.ok_button.set_sensitive(ok)
        self.set_default_response(0 if ok else 2)

    def namespace_changed_cb(self, cbwe):
        name_space = namespace_picker_ns(self.namespace_combo)
        update_commodity_picker(self.commodity_combo, name_space, None)

    @classmethod
    def modal(cls, parent,orig_sel, mode, user_message=None, cusip=None, full_name=None, mnemonic=None):
        self = cls(parent, orig_sel, mode,  user_message, cusip, full_name, mnemonic)
        while 1:
            value = self.run()
            if value == Gtk.ResponseType.OK:
                retval = self.selection
                break
            elif value == 1:
                self.new_cb()
            else:
                retval = None
                break
        self.destroy()
        return retval



GObject.type_register(CommoditySelectorDialog)
