from gasstation.utilities.warnings import *

from gasstation.utilities.dialog import *
from gasstation.views.commodity import CommodityView
from libgasstation.core.component import *
from libgasstation.core.price_db import PriceDB
from .new import *

DIALOG_COMMODITIES_CM_CLASS = "dialog-commodities"
STATE_SECTION = "dialogs/edit_commodities"
PREFS_GROUP = "dialogs.commodities"
PREF_INCL_ISO = "include-iso"


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/commodity/window.ui")
class CommodityListWindow(Gtk.Window):
    __gtype_name__ = "CommodityListWindow"
    remove_button: Gtk.Button = Gtk.Template.Child()
    edit_button: Gtk.Button = Gtk.Template.Child()
    scrolled_window: Gtk.ScrolledWindow = Gtk.Template.Child()
    show_currencies_button: Gtk.CheckButton = Gtk.Template.Child()
    close_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, parent):
        super().__init__()
        if parent is not None:
            self.set_transient_for(parent)
        self.session = Session.get_current_session()
        self.book = self.session.get_book()
        self.show_currencies = gs_pref_get_bool(PREFS_GROUP, PREF_INCL_ISO)
        gs_widget_set_style_context(self, "CommodityWindow")
        view = CommodityView(self.book)
        view.set_properties(state_section=STATE_SECTION, show_column_menu=True)
        self.commodity_tree = view
        self.scrolled_window.add(view)
        self.commodity_tree.set_headers_visible(True)
        self.commodity_tree.set_filter(self.filter_ns_func, self.filter_cm_func, None, None)
        selection = view.get_selection()
        selection.connect("changed", self.selection_changed)
        self.commodity_tree.connect("row-activated", self.row_activated_cb)
        self.show_currencies_button.set_active(self.show_currencies)
        gs_restore_window_size(PREFS_GROUP, self, parent)
        self.component_id = ComponentManager.register(DIALOG_COMMODITIES_CM_CLASS, self.refresh_handler,
                                                      self.close_handler)
        ComponentManager.set_session(self.component_id, self.session)
        self.commodity_tree.grab_focus()

    @Gtk.Template.Callback()
    def destroy_cb(self, *args):
        ComponentManager.unregister(self.component_id)
        self.destroy()

    @Gtk.Template.Callback()
    def close_clicked(self, _):
        ComponentManager.close(self.component_id)

    @Gtk.Template.Callback()
    def edit_clicked(self, widget):
        commodity = self.commodity_tree.get_selected_commodity()
        if commodity is None:
            return
        if CommodityDialog.edit_commodity_modal(commodity, self):
            self.commodity_tree.set_selected_commodity(commodity)
            ComponentManager.refresh_all()

    def row_activated_cb(self, view, path, column):
        if view is None: return
        model = view.get_model()
        _iter = model.get_iter(path)
        if _iter is not None:
            if model.iter_has_child(_iter):
                if view.row_expanded(path):
                    view.collapse_row(path)
                else:
                    view.expand_row(path, False)
            else:
                self.edit_clicked(view)

    @Gtk.Template.Callback()
    def remove_clicked(self, widget, *args):
        commodity = self.commodity_tree.get_selected_commodity()
        if commodity is None:
            return
        accounts = Session.get_current_root().get_descendants()
        can_delete = True
        for account in accounts:
            if commodity == account.get_commodity():
                can_delete = False
                break
        if not can_delete:
            message = "That commodity is currently used by " \
                      "at least one of your accounts. You may " \
                      "not delete it."
            show_warning(self, message)
            return
        pdb = PriceDB.get_db(self.book)
        prices = pdb.get_prices(commodity, None)
        if any(prices):
            message = "This commodity has price quotes. " \
                      "Are you sure you want to delete the selected " \
                      "commodity and its price quotes?"
            warning = PREF_WARN_PRICE_COMM_DEL_QUOTES
        else:
            message = "Are you sure you want to delete the " \
                      "selected commodity?"
            warning = PREF_WARN_PRICE_COMM_DEL
        dialog = Gtk.MessageDialog(transient_for=self, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE,
                                   message_format="Delete commodity?")

        dialog.format_secondary_text(message)
        dialog.add_buttons("_Cancel", Gtk.ResponseType.CANCEL, "_Delete", Gtk.ResponseType.OK)
        response = gs_dialog_run(dialog, warning)
        dialog.destroy()

        if response == Gtk.ResponseType.OK:
            ct = CommodityTable.get_table(self.book)
            for price in prices:
                pdb.remove_price(price)
            ct.remove(commodity)
            commodity.destroy()
            self.commodity_tree.get_selection().unselect_all()
        ComponentManager.refresh_all()

    @Gtk.Template.Callback()
    def add_clicked(self, widget):
        commodity = self.commodity_tree.get_selected_commodity()
        if commodity:
            name_space = commodity.get_namespace()
        else:
            name_space = None

        ret_commodity = CommodityDialog.new_commodity_modal(name_space, self)
        self.commodity_tree.set_selected_commodity(ret_commodity)

    def selection_changed(self, selection):
        commodity = self.commodity_tree.get_selected_commodity()
        remove_ok = commodity and not commodity.is_iso()
        self.edit_button.set_sensitive(commodity is not None)
        self.remove_button.set_sensitive(remove_ok)

    @Gtk.Template.Callback()
    def show_currencies_toggled(self, toggle):
        self.show_currencies = toggle.get_active()
        self.commodity_tree.refilter()

    def filter_ns_func(self, name_space, *args):
        name = name_space.get_name()
        if name == COMMODITY_NAMESPACE_NAME_TEMPLATE:
            return False
        if not self.show_currencies and CommodityNameSpace.is_iso(name):
            return False
        lis = name_space.get_commodity_list()
        return any(lis)

    def filter_cm_func(self, commodity, *args):
        if self.show_currencies:
            return True
        return not commodity.is_iso()

    def close_handler(self):
        gs_save_window_size(PREFS_GROUP, self)
        gs_pref_set_bool(PREFS_GROUP, PREF_INCL_ISO, self.show_currencies)
        self.commodity_tree.destroy()
        self.commodity_tree = None
        self.destroy()

    def refresh_handler(self, changes):
        self.commodity_tree.refilter()

    def show_handler(self, klass, component_id, price):
        self.present()
        return True

    def __new__(cls, parent, *args, **kwargs):
        if ComponentManager.forall(DIALOG_COMMODITIES_CM_CLASS, cls.show_handler):
            return
        return super().__new__(cls, parent)

GObject.type_register(CommodityListWindow)
