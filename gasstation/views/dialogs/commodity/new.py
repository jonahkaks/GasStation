from gi.repository import GObject

from gasstation.utilities.commodity import *
from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.gtk import *
from libgasstation.core.commodity import *
from libgasstation.core.session import *


class SourceCol(GObject.GEnum):
    NAME = 0
    FQ_SUPPORTED = 1
    NUM_SOURCE_COLS = 2


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/commodity/new.ui")
class CommodityDialog(Gtk.Dialog):
    __gtype_name__ = "CommodityDialog"
    full_name_entry: Gtk.Entry = Gtk.Template.Child()
    mnemonic_entry: Gtk.Entry = Gtk.Template.Child()
    user_symbol_entry: Gtk.Entry = Gtk.Template.Child()
    code_entry: Gtk.Entry = Gtk.Template.Child()
    namespace_combo: Gtk.ComboBox = Gtk.Template.Child()
    fraction_spin_button: Gtk.SpinButton = Gtk.Template.Child()
    finance_quote_warning: Gtk.Box = Gtk.Template.Child()
    bottom_end: Gtk.Box = Gtk.Template.Child()
    get_quote_check: Gtk.CheckButton = Gtk.Template.Child()
    single_source_button: Gtk.RadioButton = Gtk.Template.Child()
    multi_source_button: Gtk.RadioButton = Gtk.Template.Child()
    unknown_source_button: Gtk.RadioButton = Gtk.Template.Child()
    quote_tz_label: Gtk.Label = Gtk.Template.Child()
    ok_button: Gtk.Button = Gtk.Template.Child()
    security_label: Gtk.Label = Gtk.Template.Child()
    quote_label: Gtk.Label = Gtk.Template.Child()
    source_label: Gtk.Label = Gtk.Template.Child()
    table: Gtk.Grid = Gtk.Template.Child()
    source_menu = []

    def __init__(self, selected_namespace, parent, full_name, mnemonic, user_symbol, cusip, fraction, edit):
        super().__init__()
        self.created_commodity = None
        self.is_currency = False
        self.source_button = [self.single_source_button, self.multi_source_button, self.unknown_source_button]
        if parent is not None:
            self.set_transient_for(parent)
        self.comm_section_top = self.table.child_get(self.security_label, "top-attach")
        self.comm_section_bottom = self.table.child_get(self.quote_label, "top-attach")
        self.comm_symbol_line = self.table.child_get(self.user_symbol_entry, "top-attach")
        self.edit_commodity = None
        if CommodityNameSpace.is_iso(selected_namespace):
            menu = self.source_menu_create(QuoteSourceType.CURRENCY)
        else:
            menu = self.source_menu_create(QuoteSourceType.SINGLE)
        self.source_menu.insert(QuoteSourceType.SINGLE, menu)
        self.table.attach(menu, 1, 12, 1, 1)
        menu = self.source_menu_create(QuoteSourceType.MULTI)
        self.source_menu.insert(QuoteSourceType.MULTI, menu)
        self.table.attach(menu, 1, 13, 1, 1)

        self.quote_tz_menu = self.quote_tz_menu_create()
        self.table.attach(self.quote_tz_menu, 1, 15, 1, 1)

        if QuoteSource.num_entries(QuoteSourceType.UNKNOWN):
            self.source_button.insert(QuoteSourceType.UNKNOWN, self.unknown_source_button)
            menu = self.source_menu_create(QuoteSourceType.UNKNOWN)
            self.source_menu.insert(QuoteSourceType.UNKNOWN, menu)
            self.table.attach(menu, 1, 14, 1, 1)
        else:
            self.table.remove_row(14)
            self.table.set_row_spacing(0)
        if CommodityNameSpace.is_iso(selected_namespace):
            self.is_currency = True
            self.update_commodity_info()
            include_iso = True
            title = "Edit currency"
            text = "<b>Currency Information</b>"
        else:
            include_iso = False
            title = "Edit security" if edit else "New security"
            text = "<b>Security Information</b>"
        self.set_title(title)
        self.security_label.set_markup(text)
        if QuoteSource.fq_installed():
            self.finance_quote_warning.destroy()
        else:
            self.fq_section_top = self.table.child_get(self.finance_quote_warning, "top-attach")
            self.fq_section_bottom = self.table.child_get(self.bottom_end, "top-attach")
            self.update_fq_info()
        # self.connect("close", self.commodity_close)
        self.full_name_entry.set_text(full_name or "")
        self.mnemonic_entry.set_text(mnemonic or "")
        self.user_symbol_entry.set_text(user_symbol or "")
        Combo.add_completion(self.namespace_combo)
        update_namespace_picker(self.namespace_combo, selected_namespace,
                                CommodityMode.ALL if include_iso else CommodityMode.NON_CURRENCY)
        self.code_entry.set_text(cusip or "")
        if fraction > 0:
            self.fraction_spin_button.set_value(fraction)

    @staticmethod
    def collate(a, b):
        if not a:
            return -1
        if not b:
            return 1
        return GLib.utf8_collate(a, b)

    def set_commodity_section_sensitivity(self, widget):
        offset = self.table.child_get(widget, "top-attach")
        if offset < self.comm_section_top or offset >= self.comm_section_bottom:
            return
        if self.is_currency:
            widget.set_sensitive(offset == self.comm_symbol_line)

    def update_commodity_info(self):
        self.table.foreach(self.set_commodity_section_sensitivity)

    def set_fq_sensitivity(self, widget):
        offset = self.table.child_get(widget, "top-attach")
        if offset < self.fq_section_top or offset >= self.fq_section_bottom:
            return
        widget.set_property("sensitive", False)

    def update_fq_info(self):
        self.table.foreach(self.set_fq_sensitivity)

    @Gtk.Template.Callback()
    def quote_info_cb(self, w):
        get_quote = w.get_active()
        text = self.namespace_combo.get_child().get_text()
        allow_src = not CommodityNameSpace.is_iso(text)
        self.source_label.set_sensitive(get_quote and allow_src)
        for i in range(QuoteSourceType.MAX):
            try:
                if not self.source_button[i]:
                    continue
            except IndexError:
                continue
            active = self.source_button[i].get_active()
            self.source_button[i].set_sensitive(get_quote and allow_src)
            self.source_menu[i].set_sensitive(get_quote and allow_src and active)
        self.quote_tz_label.set_sensitive(get_quote)
        self.quote_tz_menu.set_sensitive(get_quote)

    @Gtk.Template.Callback()
    def namespace_changed_cb(self, dummy):
        if not self.is_currency:
            name_space = namespace_picker_ns(self.namespace_combo)
            full_name = self.full_name_entry.get_text()
            mnemonic = self.mnemonic_entry.get_text()
            ok = full_name and name_space and mnemonic
        else:
            ok = True
        self.ok_button.set_sensitive(ok)
        self.set_default_response(0 if ok else 1)

    @staticmethod
    def source_menu_create(_type):
        store = Gtk.ListStore(str, bool)
        if _type == QuoteSourceType.CURRENCY:
            store.append(["Currency", True])
        else:
            ma = QuoteSource.num_entries(_type)
            for i in range(ma):
                source = QuoteSource.lookup_by_ti(_type, i)
                if source is None:
                    break
                name = source.get_user_name()
                supported = source.get_supported()
                store.append([name, supported])
        combo = Gtk.ComboBox.new_with_model(store)
        renderer = Gtk.CellRendererText()
        combo.pack_start(renderer, True)
        combo.add_attribute(renderer, "text", SourceCol.NAME)
        combo.add_attribute(renderer, "sensitive", SourceCol.FQ_SUPPORTED)
        combo.set_active(0)
        combo.show()
        return combo

    @staticmethod
    def find_timezone_menu_position(timezone):
        return known_timezones.index(timezone) + 1 if timezone in known_timezones else 0

    @staticmethod
    def timezone_menu_position_to_string(pos):
        if pos == 0:
            return None
        return known_timezones[pos - 1]

    @staticmethod
    def quote_tz_menu_create():
        combo = Gtk.ComboBoxText.new()
        combo.append_text("Use local time")
        for tz in known_timezones:
            combo.append_text(tz)
        combo.show()
        return combo

    def update_quote_info(self, commodity):
        pos = 0
        has_quote_src = False
        source = None
        quote_tz = None
        if commodity is not None:
            has_quote_src = commodity.get_quote_flag()
            source = commodity.get_quote_source()
            if source is None:
                source = commodity.get_default_quote_source()
            quote_tz = commodity.get_quote_tz()
        self.get_quote_check.set_active(has_quote_src)
        if commodity is not None and not commodity.is_iso():
            _type = source.get_type()
            self.source_button[_type].set_active(True)
            self.source_menu[_type].set_active(source.get_index())
        if quote_tz is not None:
            pos = self.find_timezone_menu_position(quote_tz)
        self.quote_tz_menu.set_active(pos)

    @classmethod
    def common_commodity_modal(cls, commodity, parent, name_space, cusip, full_name, mnemonic, user_symbol, fraction):
        if commodity is not None:
            name_space = commodity.get_namespace()
            full_name = commodity.get_full_name()
            mnemonic = commodity.get_mnemonic()
            user_symbol = commodity.get_nice_symbol()
            cusip = commodity.get_cusip()
            fraction = commodity.get_fraction()
        else:
            if CommodityNameSpace.is_iso(name_space):
                name_space = None
        self = cls(name_space, parent, full_name, mnemonic, user_symbol, cusip, fraction, (commodity is not None))

        self.update_quote_info(commodity)
        self.edit_commodity = commodity

        self.quote_info_cb(self.get_quote_check)
        while 1:
            value = self.run()
            if value == Gtk.ResponseType.OK:
                self.to_commodity()
                self.created_commodity = self.edit_commodity
                break
            elif value == Gtk.ResponseType.HELP:
                continue
            else:
                break
        self.destroy()
        return self.created_commodity

    @staticmethod
    def new_commodity_modal(default_namespace, parent):
        result = CommodityDialog.common_commodity_modal(None, parent, default_namespace, None,
                                                        None, None, None, 0)
        return result

    @staticmethod
    def edit_commodity_modal(commodity, parent):
        result = CommodityDialog.common_commodity_modal(commodity, parent, None, None,
                                                        None, None, None, 0)
        return result is not None

    def to_commodity(self):
        full_name = self.full_name_entry.get_text()
        name_space = namespace_picker_ns(self.namespace_combo)
        mnemonic = self.mnemonic_entry.get_text()
        user_symbol = self.user_symbol_entry.get_text()
        code = self.code_entry.get_text()
        book = Session.get_current_book()
        fraction = self.fraction_spin_button.get_value_as_int()
        if CommodityNameSpace.is_iso(name_space):
            if self.edit_commodity:
                quote_set = self.get_quote_check.get_active()
                c = self.edit_commodity
                c.begin_edit()
                c.user_set_quote_flag(quote_set)
                if quote_set:
                    selection = self.quote_tz_menu.get_active()
                    string = self.timezone_menu_position_to_string(selection)
                    c.set_quote_tz(string)
                else:
                    c.set_quote_tz(None)
                c.set_user_symbol(user_symbol)
                c.commit_edit()
                return True
            show_warning(self, "You may not create a new national currency.")
            return False

        if GLib.utf8_collate(name_space, COMMODITY_NAMESPACE_NAME_TEMPLATE) == 0:
            show_warning(self,
                         "%s is a reserved commodity type. Please use something else." % COMMODITY_NAMESPACE_NAME_TEMPLATE)
            return False
        if full_name is not None and name_space is not None and mnemonic is not None:
            c = Session.get_current_commodity_table().lookup(name_space, mnemonic)
            if (not self.edit_commodity and c is not None) or (
                    self.edit_commodity and c is not None and c != self.edit_commodity):
                show_warning(self, "That commodity already exists.")
                return False
            if self.edit_commodity is None:
                c = Commodity(book, full_name, name_space, mnemonic, code, fraction)
                self.edit_commodity = c
                c.begin_edit()
            else:
                c = self.edit_commodity
                c.begin_edit()
                Session.get_current_commodity_table().remove(c)
                c.set_full_name(full_name)
                c.set_mnemonic(mnemonic)
                c.set_namespace(name_space)
                c.set_cusip(code)
                c.set_fraction(fraction)
                c.set_user_symbol(user_symbol)
            c.user_set_quote_flag(self.get_quote_check.get_active())
            i = QuoteSourceType.SINGLE
            for x in range(QuoteSourceType.MAX):
                i = x
                if self.source_button[x].get_active():
                    break

            selection = self.source_menu[i].get_active()
            source = QuoteSource.lookup_by_ti(i, selection)
            c.set_quote_source(source)
            selection = self.quote_tz_menu.get_active()
            string = self.timezone_menu_position_to_string(selection)
            c.set_quote_tz(string)
            c.commit_edit()
            Session.get_current_commodity_table().insert(c)
        else:
            show_warning(self,
                         "You must enter a non-empty \"Full name\", "
                         "\"Symbol/abbreviation\", "
                         "and \"Type\" for the commodity.")
            return False

        return True


GObject.type_register(CommodityDialog)
