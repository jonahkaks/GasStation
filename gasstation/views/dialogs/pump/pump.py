from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import *
from gasstation.views.dialogs.location.location import LocationDialog
from gasstation.views.selections import ReferenceSelection
from gasstation.views.selections.date_edit import DateSelection
from libgasstation import Location
from libgasstation.core import Pump
from libgasstation.core._declbase import ID_PRODUCT
from libgasstation.core.component import *
from libgasstation.core.session import Session

DIALOG_NEW_INVENTORY_CM_CLASS = "dialog-new-pump"
DIALOG_EDIT_INVENTORY_CM_CLASS = "dialog-edit-pump"
from gi.repository import GObject


class PumpDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/fuel/pump.ui")
class PumpDialog(Gtk.Dialog):
    __gtype_name__ = "PumpDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()
    serial_entry: Gtk.Entry = Gtk.Template.Child()
    pump_grid: Gtk.Grid = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, pump=None, name=None, **kwargs):
        super().__init__(**kwargs)
        self.book = book
        self.pump: Pump = pump
        self.dialog_type = PumpDialogType.NEW if pump is None else PumpDialogType.EDIT
        self.set_title("Add/Edit Pump")
        if parent is not None:
            self.set_transient_for(parent)
        gs_widget_set_style_context(self, "PumpDialog")
        if not modal:
            self.connect("response", self.window_response_cb)
        else:
            self.set_modal(True)

        self.manufactured_date_selection = DateSelection()
        self.pump_grid.attach(self.manufactured_date_selection, 1, 2, 1, 1)
        self.manufactured_date_selection.show()

        self.location_selection = ReferenceSelection(Location, LocationDialog)
        self.location_selection.set_new_ability(True)
        self.pump_grid.attach(self.location_selection, 1, 3, 1, 1)
        self.location_selection.show()
        self.set_default_response(Gtk.ResponseType.OK)
        if name is not None:
            self.name_entry.set_text(name)
        ComponentManager.resume()
        gs_window_adjust_for_screen(self)

        self.component_id = ComponentManager.register(DIALOG_NEW_INVENTORY_CM_CLASS,
                                                      self.refresh_handler,
                                                      None if modal else self.close_handler)

        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id,
                                           ID_PRODUCT,
                                           EVENT_MODIFY | EVENT_DESTROY)
        if pump is not None:
            self.to_ui()

    def to_ui(self):
        p = self.pump
        self.name_entry.set_text(p.get_name())
        self.serial_entry.set_text(p.get_serial_number())
        self.location_selection.set_ref(p.get_location())
        self.manufactured_date_selection.set_date(p.get_manufactured_date())

    def to_pump(self):
        p = self.pump
        if p is None:
            self.pump = Pump(self.book)
            p = self.pump
        p.begin_edit()
        p.set_name(self.name_entry.get_text())
        p.set_serial_number(self.serial_entry.get_text())
        p.set_manufactured_date(self.manufactured_date_selection.get_date())
        p.set_location(self.location_selection.get_ref())

    def finish_ok(self):
        self.to_pump()
        self.pump.commit_edit()
        self.pump = None
        self.close()
        ComponentManager.close(self.component_id)

    def common_ok(self):
        name = self.name_entry.get_text()
        if name == "":
            message = "The Pump must be given a name."
            show_error(self, message)
            return False
        loc = self.location_selection.get_ref()
        if loc is None:
            show_error(self, "Location must be set")
            return False
        if Pump.lookup_name_with_location(Session.get_current_book(),
                                          name, loc) is not None and self.dialog_type == PumpDialogType.NEW:
            show_error(self, "The pump %s already exists for location %s" % (name, loc.get_name()))
            return False
        return True

    def window_response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            if not self.common_ok():
                return False
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            dialog.close()

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        if self.pump is None:
            ComponentManager.close(self.component_id)
            return
        if changes:
            info = ComponentManager.get_entity_events(changes, self.pump.get_guid())
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(PumpDialog)
