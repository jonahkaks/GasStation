from gasstation.utilities.warnings import *

from gasstation.utilities.cursors import *
from gasstation.utilities.custom_dialogs import show_info
from gasstation.views.price import *
from .price import *

DIALOG_PRICE_DB_CM_CLASS = "dialog-price-edit-db"
STATE_SECTION = "dialogs/edit_prices"
PREFS_GROUP = "dialogs.pricedb-editor"


class PriceColumn:
    FULL_NAME = 0
    COMM = 1
    DATE = 2
    COUNT = 3


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/price/db.ui")
class PriceDbDialog(Gtk.Dialog):
    __gtype_name__ = "PriceDbDialog"
    price_list_window: Gtk.ScrolledWindow = Gtk.Template.Child()
    get_quotes_button: Gtk.Button = Gtk.Template.Child()
    edit_button: Gtk.Button = Gtk.Template.Child()
    add_button: Gtk.Button = Gtk.Template.Child()
    remove_button: Gtk.Button = Gtk.Template.Child()
    remove_old_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, parent, book, **kwargs):
        super().__init__(**kwargs)
        gs_widget_set_style_context(self, "PriceDbDialog")
        self.book = book
        self.price_db = PriceDB.get_db(self.book)
        if parent is not None:
            self.set_transient_for(parent)
        self.set_default_response(Gtk.ResponseType.CLOSE)
        view = PriceView(self.book)
        view.set_properties(state_section=STATE_SECTION, show_column_menu=True)
        self.price_tree = view
        self.price_list_window.add(view)
        self.price_tree.set_filter(self.filter_ns_func, self.filter_cm_func, None, None, None)
        selection = view.get_selection()
        selection.set_mode(Gtk.SelectionMode.MULTIPLE)
        selection.connect("changed", self.selection_changed)
        view.connect("row-activated", self.row_activated)

        if not QuoteSource.fq_installed():
            self.get_quotes_button.set_sensitive(False)
        gs_restore_window_size(PREFS_GROUP, self, parent)
        self.component_id = ComponentManager.register(DIALOG_PRICE_DB_CM_CLASS,
                                                      self.refresh_handler, self.close_handler)
        ComponentManager.set_session(self.component_id, Session.get_current_session())
        self.price_tree.grab_focus()
        self.show()

    def window_destroy_cb(self, *args):
        ComponentManager.unregister(self.component_id)
        self.destroy()

    def close_cb(self, dialog):
        ComponentManager.close(self.component_id)

    def response_cb(self, dialog, response_id):
        ComponentManager.close(self.component_id)

    @Gtk.Template.Callback()
    def edit_clicked(self, _):
        price_list = self.price_tree.get_selected_prices()
        if price_list is None or len(price_list) == 0:
            return
        for price in price_list:
            dialog = PriceDialog(parent=self, book=self.book, price=price)
            dialog.run()

    @staticmethod
    def remove_helper(price, pdb):
        pdb.remove_price(price)

    @Gtk.Template.Callback()
    def remove_clicked(self, _):
        price_list = self.price_tree.get_selected_prices()
        if price_list is None or len(price_list) == 0: return
        length = len(price_list)
        if length > 0:
            message = "Are you sure you want to delete the %d selected prices?" % length
            dialog = Gtk.MessageDialog(self, destroy_with_parent=True,
                                       message_type=Gtk.MessageType.QUESTION,
                                       buttons=Gtk.ButtonsType.NONE, message_format="Delete prices?")
            dialog.format_secondary_text(message)
            dialog.add_buttons("_Cancel", Gtk.ResponseType.CANCEL,
                               "_Delete", Gtk.ResponseType.YES)
            dialog.set_default_response(Gtk.ResponseType.YES)
            response = gs_dialog_run(dialog, PREF_WARN_PRICE_QUOTES_DEL)
            dialog.destroy()
        else:
            response = Gtk.ResponseType.YES

        if response == Gtk.ResponseType.YES:
            for p in price_list:
                self.remove_helper(p, self.price_db)

    @staticmethod
    def load_view(view, pdb):
        model = view.get_model()
        commodity_table = Session.get_current_commodity_table()
        namespace_list = commodity_table.get_namespaces_list()
        oldest = datetime.datetime.now()
        for tmp_namespace in namespace_list:
            commodity_list = tmp_namespace.get_commodity_list()
            for tmp_commodity in commodity_list:
                num = pdb.get_num_prices(tmp_commodity)
                if num > 0:
                    lis = pdb.get_prices(tmp_commodity, None)
                    price = lis[-1]
                    price_time = price.get_time64()
                    name_str = tmp_commodity.get_printname()
                    if oldest > price_time:
                        oldest = price_time
                    date_str = print_datetime(price_time)
                    num_str = "%d" % num
                    _iter = model.append()
                    model.set_value(_iter, PriceColumn.FULL_NAME, name_str)
                    model.set_value(_iter, PriceColumn.COMM, tmp_commodity)
                    model.set_value(_iter, PriceColumn.DATE, date_str)
                    model.set_value(_iter, PriceColumn.COUNT, num_str)
                    price.unref()
        return oldest

    @staticmethod
    def get_commodities(view):
        comm_list = []
        selection = view.get_selection()
        model, list = selection.get_selected_rows()
        for row in list:
            _iter = model.get_iter(row)
            if _iter is not None:
                comm = model.get_value(_iter, PriceColumn.COMM)
                comm_list.append(comm)
        return comm_list

    def change_source_flag(self, source, sett):
        w = self.remove_dialog.get_widget_for_response(Gtk.ResponseType.OK)
        if sett:
            self.remove_source = self.remove_source | source
        else:
            self.remove_source = self.remove_source & (~source)
        enable_button = True if self.remove_source > 8 else False
        w.set_sensitive(enable_button)

    def check_event_fq_cb(self, widget):
        active = widget.get_active()
        self.change_source_flag(PriceRemoveSourceFlags.FQ, active)

    def check_event_user_cb(self, widget):
        active = widget.get_active()
        self.change_source_flag(PriceRemoveSourceFlags.USER, active)

    def check_event_app_cb(self, widget):
        active = widget.get_active()
        self.change_source_flag(PriceRemoveSourceFlags.APP, active)

    def selection_changed_cb(self, selection):
        model, rows = selection.get_selected_rows()
        have_rows = True if len(rows) > 0 else False
        self.change_source_flag(PriceRemoveSourceFlags.COMM, have_rows)

    @staticmethod
    def get_fiscal_end_date():
        end = AccountingPeriod.fiscal_end()
        return datetime.date.fromtimestamp(end)

    @Gtk.Template.Callback()
    def remove_old_clicked(self, _):
        builder = Gtk.Builder()
        # builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/price.ui", ["liststore4",
        #                                                                                 "deletion_date_dialog"])
        # self.remove_dialog = builder.get_object("deletion_date_dialog")
        # box = builder.get_object("date_hbox")
        # date = DateSelection()
        # box.pack_start(date, False, False, 0)
        # date.show()
        # date.date_entry.set_activates_default(True)
        # label = builder.get_object("date_label")
        # date.make_mnemonic_target(label)
        # self.remove_view = builder.get_object("commodty_treeview")
        # self.remove_view.set_model(Gtk.ListStore(str, object, str, str))
        # selection = self.remove_view.get_selection()
        # selection.set_mode(Gtk.SelectionMode.MULTIPLE)
        # tree_column = Gtk.TreeViewColumn()
        # tree_column.set_title("Entries")
        # self.remove_view.append_column(tree_column)
        # tree_column.set_alignment(0.5)
        # tree_column.set_expand(True)
        # cr = Gtk.CellRendererText.new()
        # tree_column.pack_start(cr, True)
        # tree_column.add_attribute(cr, "text", PriceColumn.COUNT)
        # cr.set_alignment(0.5, 0.5)
        # self.load_view(self.remove_view, self.price_db)
        # selection.select_all()
        # selection.connect("changed", self.selection_changed_cb)
        # builder.connect_signals(self)
        # self.remove_dialog.set_transient_for(self)
        # self.remove_source = 9
        # button = builder.get_object("checkbutton_fq")
        # button.connect("toggled", self.check_event_fq_cb)
        # button = builder.get_object("checkbutton_user")
        # button.connect("toggled", self.check_event_user_cb)
        # button = builder.get_object("checkbutton_app")
        # button.connect("toggled", self.check_event_app_cb)
        # result = self.remove_dialog.run()
        # if result == Gtk.ResponseType.OK:
        #     fmt = "Are you sure you want to delete these prices ?"
        #     comm_list = PricesDialog.get_commodities(self.remove_view)
        #     if len(comm_list) != 0 and ask_yes_no(self.remove_dialog, False, fmt, None):
        #         fiscal_end_date = self.get_fiscal_end_date()
        #         source = PriceRemoveSourceFlags.FQ
        #         keep = PriceRemoveKeepOptions.NONE
        #         model = self.price_tree.get_model()
        #         self.price_tree.set_model(None)
        #         last = date.get_date()
        #         button = builder.get_object("radiobutton_last_week")
        #         if button.get_active():
        #             keep = PriceRemoveKeepOptions.LAST_WEEKLY
        #         button = builder.get_object("radiobutton_last_month")
        #         if button.get_active():
        #             keep = PriceRemoveKeepOptions.LAST_MONTHLY
        #         button = builder.get_object("radiobutton_last_quarter")
        #         if button.get_active():
        #             keep = PriceRemoveKeepOptions.LAST_QUARTERLY
        #         button = builder.get_object("radiobutton_last_period")
        #         if button.get_active():
        #             keep = PriceRemoveKeepOptions.LAST_PERIOD
        #         button = builder.get_object("radiobutton_scaled")
        #         if button.get_active():
        #             keep = PriceRemoveKeepOptions.SCALED
        #
        #         if keep != PriceRemoveKeepOptions.SCALED:
        #             self.price_db.remove_old_prices(comm_list, fiscal_end_date, last, self.remove_source, keep)
        #         else:
        #             tmp_date = GLib.Date()
        #             tmp_date = gdate_set_time64(tmp_date, last)
        #             tmp_date.subtract_months(6)
        #             tmp = gdate_get_time64(tmp_date)
        #             self.price_db.remove_old_prices(comm_list, fiscal_end_date, tmp, self.remove_source,
        #                                             PriceRemoveKeepOptions.LAST_WEEKLY)
        #             tmp_date.subtract_months(6)
        #             tmp = gdate_get_time64(tmp_date)
        #             self.price_db.remove_old_prices(comm_list, fiscal_end_date, tmp, self.remove_source,
        #                                             PriceRemoveKeepOptions.LAST_MONTHLY)
        #         self.price_tree.set_model(model)
        #     self.remove_dialog.destroy()

    @Gtk.Template.Callback()
    def add_clicked(self, _):
        price = None
        price_list = self.price_tree.get_selected_prices()
        if price_list is not None and len(price_list) > 0:
            price = price_list[0]
        dialog = PriceDialog(parent=self, book=self.book, parent_price=price)
        dialog.run()

    @Gtk.Template.Callback()
    def get_quotes_clicked(self, _):
        from libgasstation.core.price_quotes import PriceQuotes
        gs_set_busy_cursor(None, True)
        PriceQuotes.add_quotes(lambda e: show_info(self, e), lambda e: show_error(self, e),
                               lambda e: show_warning(self, e), self.book)
        gs_unset_busy_cursor(None)
        ComponentManager.refresh_all()

    def selection_changed(self, treeselection):
        price_list = self.price_tree.get_selected_prices()
        length = len(price_list)
        self.edit_button.set_sensitive(length == 1)
        self.remove_button.set_sensitive(length >= 1)

    def filter_ns_func(self, name_space, *args):
        name = name_space.get_name()
        if name == COMMODITY_NAMESPACE_NAME_TEMPLATE:
            return False
        cm_list = name_space.get_commodity_list()
        for item in cm_list:
            if self.price_db.has_prices(item, None):
                return True
        return False

    def filter_cm_func(self, commodity, *args):
        return self.price_db.has_prices(commodity, None)

    def row_activated(self, view, path, column):
        if view is None:
            return
        model = view.get_model()
        _iter = model.get_iter(path)
        if _iter is not None:
            if model.iter_has_child(_iter):
                if view.row_expanded(path):
                    view.collapse_row(path)
                else:
                    view.expand_row(path, False)
            else:
                self.edit_clicked(view)

    def close_handler(self):
        if self is not None:
            gs_save_window_size(PREFS_GROUP, self)
            self.destroy()

    def refresh_handler(self, changes):
        pass

    def show_handler(self, klass, component_id, price):
        self.present()
        return True

    def __new__(cls, parent, book, *args, **kwargs):
        if ComponentManager.forall(DIALOG_PRICE_DB_CM_CLASS, cls.show_handler): return
        return super().__new__(cls, parent, book)

GObject.type_register(PriceDbDialog)
