from gasstation.utilities.commodity import *
from gasstation.utilities.custom_dialogs import show_warning
from gasstation.utilities.ui import *
from gasstation.views.selections.amount_edit import *
from gasstation.views.selections.currency import *
from gasstation.views.selections.date_edit import *
from libgasstation.core.component import *
from libgasstation.core.price_db import *

DIALOG_PRICE_EDIT_CM_CLASS = "dialog-price-edit"
PREFS_GROUP = "dialogs.price-editor"


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/price/price.ui")
class PriceDialog(Gtk.Dialog):
    __gtype_name__ = "PriceDialog"
    price_label: Gtk.Label = Gtk.Template.Child()
    currency_label: Gtk.Label = Gtk.Template.Child()
    date_label: Gtk.Label = Gtk.Template.Child()
    namespace_combo: Gtk.ComboBox = Gtk.Template.Child()
    commodity_combo: Gtk.ComboBox = Gtk.Template.Child()
    type_combo: Gtk.ComboBox = Gtk.Template.Child()
    price_grid: Gtk.Grid = Gtk.Template.Child()
    source_entry: Gtk.Entry = Gtk.Template.Child()
    apply_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, parent, book, parent_price=None, price=None, **kwargs):
        super().__init__(**kwargs)
        self.changed = False
        self.book = book
        self.price_db = PriceDB.get_db(self.book)
        if parent is not None:
            self.set_transient_for(parent)

        self.currency_selection = CurrencySelection()
        self.currency_selection.set_currency(gs_default_currency())
        self.price_grid.attach(self.currency_selection, 1, 2, 1, 1)
        self.currency_selection.show()
        self.currency_selection.connect("changed", self.data_changed_cb)
        self.currency_label.set_mnemonic_widget(self.currency_selection)

        self.date_selection = DateSelection()
        self.price_grid.attach(self.date_selection, 1, 3, 1, 1)
        self.date_selection.show()
        self.date_selection.connect("date_changed", self.data_changed_cb)
        self.date_selection.make_mnemonic_target(self.date_label)

        self.price_entry = AmountEntry()
        self.price_grid.attach(self.price_entry, 1, 6, 1, 1)
        self.price_entry.set_evaluate_on_enter(True)
        print_info = PrintAmountInfo.price(self.currency_selection.get_currency())
        self.price_entry.set_print_info(print_info)
        self.price_entry.set_activates_default(True)
        self.price_entry.show()

        self.price_label.set_mnemonic_widget(self.price_entry)
        self.price_entry.connect("changed", self.data_changed_cb)
        self.set_changed(False)

        gs_restore_window_size(PREFS_GROUP, self, parent)
        self.is_new = True

        if parent_price is not None and price is None:
            price = parent_price.clone(self.book)
            price.set_source(PriceSource.EDIT_DLG)
            self.is_new = True
        elif parent_price is  None and price is not None:
            price.ref()
            self.is_new = False
        self.price = price
        self.to_ui()
        self.set_changed(False)
        component_id = ComponentManager.register(DIALOG_PRICE_EDIT_CM_CLASS, self.refresh_handler, self.close_handler, self)
        ComponentManager.set_session(component_id, Session.get_current_session())
        update_namespace_picker(self.namespace_combo, None, CommodityMode.ALL)
        Combo.require_list_item(self.namespace_combo)

        Combo.require_list_item(self.commodity_combo)
        name_space = namespace_picker_ns(self.namespace_combo)
        update_commodity_picker(self.commodity_combo, name_space, None)
        self.commodity_combo.grab_focus()
        self.namespace_combo.set_active(1)
        self.show()

    def set_changed(self, changed):
        self.changed = changed
        self.apply_button.set_sensitive(changed)

    @staticmethod
    def type_string_to_index(_type):
        if _type == "bid":
            return 0
        if _type == "ask":
            return 1
        if _type == "last":
            return 2
        if _type == "nav":
            return 3
        return 4

    @staticmethod
    def type_index_to_string(index):
        if index == 0:
            return "bid"
        elif index == 1:
            return "ask"
        elif index == 2:
            return "last"
        elif index == 3:
            return "nav"
        else:
            return "unknown"

    def to_ui(self):
        commodity = None
        if self.price is not None:
            commodity = self.price.get_commodity()
        if commodity is not None:
            name_space = commodity.get_namespace()
            fullname = commodity.get_printname()
            update_namespace_picker(self.namespace_combo, name_space, CommodityMode.ALL)
            update_commodity_picker(self.commodity_combo, name_space, fullname)

            currency = self.price.get_currency()
            date = self.price.get_time64()
            source = self.price.get_source_string()
            type = self.price.get_type_string()
            value = self.price.get_value()
        else:
            currency = gs_default_currency()
            date = datetime.datetime.now()
            source = "user:price-editor"
            type = ""
            value = Decimal(0)
        if currency is not None:
            self.currency_selection.set_currency(currency)
        self.date_selection.set_date(date)
        if source is not None:
            self.source_entry.set_text(source)
        self.type_combo.set_active(self.type_string_to_index(type))
        self.price_entry.set_amount(value)

    def to_price(self):
        name_space = namespace_picker_ns(self.namespace_combo)
        fullname = self.commodity_combo.get_child().get_text()
        commodity = Session.get_current_commodity_table().find_full(name_space, fullname)
        if commodity is None:
            return "You must select a Security."
        currency = self.currency_selection.get_currency()
        if currency is None:
            return "You must select a Currency."
        date = self.date_selection.get_date()
        source = self.source_entry.get_text()
        price_type = self.type_index_to_string(self.type_combo.get_active())

        if not self.price_entry.evaluate():
            return "You must enter a valid amount."
        value = self.price_entry.get_amount()
        if self.price is None:
            self.price = Price(self.book)
        self.price.begin_edit()
        self.price.set_commodity(commodity)
        self.price.set_currency(currency)
        self.price.set_time64(date)
        self.price.set_source(PriceSource.__getitem__(source))
        self.price.set_type(PriceType.__getitem__(price_type))
        self.price.set_value(value)
        self.price.commit_edit()
        return None

    def destroy_cb(self, widget):
        ComponentManager.unregister_by_data(DIALOG_PRICE_EDIT_CM_CLASS, self)
        if self.price:
            self.price.unref()
            self.price = None
            self.is_new = False

    @Gtk.Template.Callback()
    def response_cb(self, _, response):
        if response == Gtk.ResponseType.OK or response == Gtk.ResponseType.APPLY:
            error_str = self.to_price()
            if error_str is not None:
                show_warning(self, error_str)
                return
            self.set_changed(False)
            if self.is_new:
                self.price_db.add_price(self.price)
            ComponentManager.refresh_all()

        if response == Gtk.ResponseType.APPLY:
            new_price = self.price.clone(self.book)
            self.is_new = True
            self.price.unref()
            self.price = new_price
        else:
            gs_save_window_size(PREFS_GROUP, self)
            self.destroy()
            self.destroy_cb(None)

    @Gtk.Template.Callback()
    def commodity_namespace_changed_cb(self, _):
        self.set_changed(True)
        name_space = namespace_picker_ns(self.namespace_combo)
        if name_space != "":
            update_commodity_picker(self.commodity_combo, name_space, None)

    @Gtk.Template.Callback()
    def commodity_changed_cb(self, _):
        self.set_changed(True)
        name_space = namespace_picker_ns(self.namespace_combo)
        fullname = self.commodity_combo.get_child().get_text()
        commodity = Session.get_current_commodity_table().find_full(name_space, fullname)
        if commodity:
            price_list = self.price_db.lookup_latest_any_currency(commodity)
            if price_list and len(price_list) > 0:
                price = price_list[0]
                if Commodity.equiv(commodity, price.get_currency()):
                    currency = price.get_commodity()
                else:
                    currency = price.get_currency()
                if currency:
                    self.currency_selection.set_currency(currency)
                price_list.destroy()
            else:
                self.currency_selection.set_currency(gs_default_currency())

    def data_changed_cb(self, w, *args):
        self.set_changed(True)

    def close_handler(self):
        self.response(Gtk.ResponseType.CANCEL)

    def refresh_handler(self, changes):
        # self.load_prices()
        pass

    @staticmethod
    def show_handler(klass, component_id,self, price):
        if self is None:
            return
        if self.price != price:
            return False
        self.present()
        return True

    def __new__(cls, parent, book, parent_price=None, price=None, **kwargs):
        if ComponentManager.forall(DIALOG_PRICE_EDIT_CM_CLASS, cls.show_handler, price):
            return
        return super().__new__(cls, parent, book, parent_price=parent_price, price=price, **kwargs)


GObject.type_register(PriceDialog)
