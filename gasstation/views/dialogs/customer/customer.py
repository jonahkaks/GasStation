from nameparser import HumanName

from gasstation.utilities.cursors import gs_set_busy_cursor, gs_unset_busy_cursor
from gasstation.utilities.dialog import *
from gasstation.utilities.ui import PrintAmountInfo, gs_default_currency
from gasstation.views.components.address import AddressFrame
from gasstation.views.components.avatar import Avatar
from gasstation.views.dialogs.location.location import LocationDialog
from gasstation.views.dialogs.payment.payment_method import PaymentMethodDialog
from gasstation.views.dialogs.tax import NewTaxDialog
from gasstation.views.dialogs.term.new import NewPaymentTermDialog
from gasstation.views.selections.amount_edit import AmountEntry
from gasstation.views.selections.currency import CurrencySelection
from gasstation.views.selections.date_edit import DateSelection
from gasstation.views.selections.reference import ReferenceSelection
from gasstation.views.selections.telephone_entry import TelNumberEntry
from libgasstation import Customer, ID_CUSTOMER, DeliveryMethod, Location
from libgasstation import Tax
from libgasstation.core.address import Address
from libgasstation.core.component import *
from libgasstation.core.contact import Contact
from libgasstation.core.email import Email, EmailForType
from libgasstation.core.payment import PaymentMethod
from libgasstation.core.phone import PhoneType, Phone
from libgasstation.core.session import Session
from libgasstation.core.term import Term

DIALOG_NEW_CUSTOMER_CM_CLASS = "dialog-new-customer"
DIALOG_EDIT_CUSTOMER_CM_CLASS = "dialog-edit-customer"
from gi.repository import GObject


class CustomerDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/customer/customer.ui')
class CustomerDialog(Gtk.Dialog):
    __gtype_name__ = "CustomerDialog"
    avatar: Avatar = Gtk.Template.Child()
    full_name_entry: Gtk.Entry = Gtk.Template.Child()
    display_name_combo: Gtk.ComboBox = Gtk.Template.Child()

    email_entry: Gtk.Entry = Gtk.Template.Child()
    phone_entry: TelNumberEntry = Gtk.Template.Child()
    mobile_entry: TelNumberEntry = Gtk.Template.Child()
    tin_entry: Gtk.Entry = Gtk.Template.Child()
    website_entry: Gtk.Entry = Gtk.Template.Child()
    notes_entry: Gtk.TextView = Gtk.Template.Child()
    is_sub_customer: Gtk.CheckButton = Gtk.Template.Child()
    tax_check: Gtk.CheckButton = Gtk.Template.Child()

    bill_parent_combo: Gtk.ComboBox = Gtk.Template.Child()
    sub_customer_grid: Gtk.Grid = Gtk.Template.Child()

    preferred_delivery_combo: Gtk.ComboBoxText = Gtk.Template.Child()
    preferred_payment_frame: Gtk.Frame = Gtk.Template.Child()
    currency_frame: Gtk.Frame = Gtk.Template.Child()
    terms_frame: Gtk.Frame = Gtk.Template.Child()
    credit_limit_frame: Gtk.Frame = Gtk.Template.Child()
    tax_frame: Gtk.Frame = Gtk.Template.Child()

    opening_balance_date_frame: Gtk.Frame = Gtk.Template.Child()
    opening_balance_amount_frame: Gtk.Frame = Gtk.Template.Child()
    location_frame: Gtk.Frame = Gtk.Template.Child()

    billing_address_frame: AddressFrame = Gtk.Template.Child()
    shipping_address_frame: AddressFrame = Gtk.Template.Child()
    ok_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, customer=None, name=None, **kwargs):
        super().__init__(**kwargs)
        self.book = book
        self.billing_address_frame.street_entry.connect("focus-out-event", self.validate)
        self.dialog_type = CustomerDialogType.NEW if customer is None else CustomerDialogType.EDIT
        self.customer = customer
        self.created_customer = None
        self.notes_buffer = self.notes_entry.get_buffer()
        if parent is not None:
            self.set_transient_for(parent)

        self.get_style_context().add_class("CustomerDialog")
        if not modal:
            self.connect("response", self.window_response_cb)
        else:
            self.set_modal(True)
        self.set_default_response(Gtk.ResponseType.OK)
        self.display_name_combo.set_model(Gtk.ListStore(str))

        cust = self.customer
        if cust:
            currency = cust.get_currency()
        else:
            currency = gs_default_currency()

        self.display_name_combo.set_model(Gtk.ListStore(str))
        self.billing_address_frame.set_label("Billing Address")
        self.address_same_checkbox = Gtk.CheckButton("Same as billing address")

        grid = Gtk.Grid()
        self.shipping_address_frame.set_label_widget(grid)
        self.address_same_checkbox.show()
        grid.attach(Gtk.Label("Shipping Address"), 0, 0, 1, 1)
        grid.attach(self.address_same_checkbox, 1, 0, 1, 1)
        grid.show_all()

        self.location_selection = ReferenceSelection(Location, LocationDialog)
        self.location_selection.connect("selection_changed", self.validate)
        self.location_selection.set_new_ability(True)
        self.location_frame.add(self.location_selection)
        self.location_selection.show()

        self.address_same_checkbox.connect("toggled", self.address_same_checkbox_toggled, self.shipping_address_frame)
        self.address_same_checkbox.set_active(True)

        self.is_sub_customer.connect("toggled", lambda
            a: self.sub_customer_grid.show() if a.get_active() else self.sub_customer_grid.hide())

        self.terms = ReferenceSelection(Term, NewPaymentTermDialog)
        self.terms.set_new_ability(True)
        self.terms.connect("selection_changed", self.validate)
        self.terms_frame.add(self.terms)
        self.terms.show()

        self.tax_selection = ReferenceSelection(Tax, NewTaxDialog)
        self.tax_selection.set_new_ability(True)
        self.tax_frame.add(self.tax_selection)
        self.tax_selection.show()

        self.currency_selection = CurrencySelection()
        self.currency_selection.set_currency(currency)
        self.currency_frame.add(self.currency_selection)
        self.currency_selection.show()

        self.credit_limit_entry = AmountEntry()
        print_info = PrintAmountInfo.commodity(currency, False)
        self.credit_limit_entry.set_evaluate_on_enter(True)
        self.credit_limit_entry.set_print_info(print_info)
        self.credit_limit_frame.add(self.credit_limit_entry)
        self.credit_limit_entry.show()

        self.preferred_payment_method = ReferenceSelection(PaymentMethod, PaymentMethodDialog)
        self.preferred_payment_method.set_new_ability(True)
        self.preferred_payment_method.connect("selection_changed", self.validate)
        self.preferred_payment_frame.add(self.preferred_payment_method)
        self.preferred_payment_method.show()

        self.parent = ReferenceSelection(Customer, CustomerDialog)
        if self.dialog_type == CustomerDialogType.EDIT:
            self.parent.purge_ref(self.customer, False)
        self.sub_customer_grid.attach(self.parent, 2, 0, 1, 1)
        self.parent.show()

        edit = AmountEntry()
        edit.set_evaluate_on_enter(True)
        print_info = PrintAmountInfo.integral(2)
        print_info.max_decimal_places = 5
        edit.set_print_info(print_info)
        self.opening_balance = edit
        self.opening_balance_amount_frame.add(edit)
        edit.show()

        self.opening_date = DateSelection()
        self.opening_balance_date_frame.add(self.opening_date)
        self.opening_date.show()


        if name is not None:
            self.full_name_entry.set_text(name)
        gs_window_adjust_for_screen(self)
        self.component_id = ComponentManager.register(DIALOG_NEW_CUSTOMER_CM_CLASS,
                                                      self.refresh_handler,
                                                      None if modal else self.close_handler)

        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id,
                                           ID_CUSTOMER,
                                           EVENT_MODIFY | EVENT_DESTROY)

        self.is_sub_customer.set_active(False)
        self.avatar.set_icon_name("avatar-default")
        self.avatar.set_valign(Gtk.Align.START)
        self.show()
        if customer is not None:
            self.to_ui()
            self.validate()

    def to_ui(self):
        cust = self.customer
        contact = cust.get_contact()
        if contact is not None:
            self.full_name_entry.set_text(contact.get_full_name())
            if self.dialog_type == CustomerDialogType.EDIT:
                model = self.display_name_combo.get_model()
                self.display_name_changed()
                d = contact.get_display_name()
                _iter = model.get_iter_first()
                while _iter is not None:
                    tree_string = model.get_value(_iter, 0)
                    if GLib.utf8_collate(d, tree_string) == 0:
                        self.display_name_combo.set_active_iter(_iter)
                        break
                    _iter = model.iter_next(_iter)
            if any(contact.emails):
                self.email_entry.set_text(",".join(set(ea.get_address() for ea in contact.emails)))
            if any(contact.phones):
                works = list(filter(lambda a: a.type == PhoneType.WORK, contact.phones))
                if any(works):
                    self.phone_entry.set_number(works[0].get_number())
                mobiles = list(filter(lambda a: a.type == PhoneType.MOBILE, contact.phones))
                if any(mobiles):
                    self.mobile_entry.set_number(mobiles[0].get_number())
            self.website_entry.set_text(contact.get_website() or "")
            same = False
            if any(contact.addresses):
                ba = contact.addresses[0]
                if len(contact.addresses) == 1:
                    sa = ba
                    same = True
                else:
                    sa = contact.addresses[1]
                self.billing_address_frame.set_address(ba)
                self.shipping_address_frame.set_address(sa)
            self.address_same_checkbox.set_active(same)

        url = contact.get_image_url()
        self.avatar.set_fullname(contact.get_full_name())
        self.avatar.set_url(url)
        self.notes_buffer.set_text(cust.get_notes() or "")
        self.tin_entry.set_text(cust.get_tin() or "")
        self.parent.set_ref(cust.get_parent())
        self.credit_limit_entry.set_amount(cust.get_credit_limit())
        self.currency_selection.set_currency(cust.get_currency())
        self.preferred_delivery_combo.set_active(int(cust.get_delivery_method()))
        self.bill_parent_combo.set_active(int(cust.get_billable()))
        self.is_sub_customer.set_active(cust.get_parent() is not None)
        self.preferred_payment_method.set_ref(cust.get_payment_method())
        self.terms.set_ref(cust.get_term())
        self.location_selection.set_ref(cust.get_location())
        self.tax_selection.set_ref(cust.get_tax())
        self.tax_check.set_active(cust.get_tax_included())
        txn = cust.get_opening_balance_transaction()
        if txn is not None:
            self.opening_balance.set_amount(cust.get_opening_balance())
            self.opening_date.set_date(txn.get_post_date())

    def to_customer(self):
        cust = self.customer
        ComponentManager.suspend()
        if cust is None:
            cust = Customer(self.book)
            self.customer = cust
        contact = cust.get_contact()
        if contact is None:
            contact = Contact(self.book)
            cust.set_contact(contact)
        contact.begin_edit()
        contact.set_image_url(self.avatar.get_url())
        contact.set_full_name(self.full_name_entry.get_text())
        contact.set_website(self.website_entry.get_text())
        model = self.display_name_combo.get_model()
        display_name = model.get_value(self.display_name_combo.get_active_iter(), 0)
        contact.set_display_name(display_name)
        contact_emails = dict(map(lambda a: (a.get_address(), a), contact.emails))
        for i, e in enumerate(set(self.email_entry.get_text().split(","))):
            if e in contact_emails.keys():
                contact_emails.pop(e)
                continue
            if e == "":
                continue
            ea = Email(self.book)
            ea.begin_edit()
            if i != 0:
                ea.set_type(EmailForType.OTHER)
            ea.set_address(e)
            ea.set_contact(contact)
            ea.commit_edit()
        for remaining in contact_emails.values():
            remaining.begin_edit()
            remaining.set_dirty()
            remaining.set_destroying()
            remaining.commit_edit()

        work_number = self.phone_entry.get_number()
        if work_number is not None:
            works = list(filter(lambda a: a.type == PhoneType.WORK, contact.phones))
            if any(works):
                work_phone = works[0]
                work_phone.set_number(work_number)
            else:
                work_phone = Phone(self.book)
                work_phone.begin_edit()
                work_phone.set_type(PhoneType.WORK)
                work_phone.set_number(work_number)
                work_phone.set_contact(contact)
                work_phone.commit_edit()

        mobile_number = self.mobile_entry.get_number()
        if mobile_number is not None:
            mobiles = list(filter(lambda a: a.type == PhoneType.MOBILE, contact.phones))
            if any(mobiles):
                mobile_phone = list(mobiles)[0]
                mobile_phone.set_number(mobile_number)
            else:
                mobile_phone = Phone(self.book)
                mobile_phone.begin_edit()
                mobile_phone.set_type(PhoneType.MOBILE)
                mobile_phone.set_number(mobile_number)
                mobile_phone.set_contact(contact)
                mobile_phone.commit_edit()

        if any(contact.addresses):
            billing_address = contact.addresses[0]
        else:
            billing_address = Address(self.book)
        billing_address.begin_edit()
        self.billing_address_frame.copy_to_address(billing_address)
        billing_address.set_contact(contact)
        billing_address.commit_edit()
        if not self.address_same_checkbox.get_active():
            if any(contact.addresses) and len(contact.addresses) > 1:
                shipping_address = contact.addresses[1]
            else:
                shipping_address = Address(self.book)
                shipping_address.set_contact(contact)
            shipping_address.begin_edit()
            self.shipping_address_frame.copy_to_address(billing_address)
            shipping_address.set_contact(contact)
            shipping_address.commit_edit()
        else:
            if len(contact.addresses) > 1:
                contact.addresses.pop(1)

        if self.is_sub_customer.get_active():
            cust.set_parent(self.parent.get_ref())
            cust.set_billable(not self.bill_parent_combo.get_active())
        cust.set_payment_method(self.preferred_payment_method.get_ref())
        cust.set_credit_limit(self.credit_limit_entry.get_amount())
        curr = self.currency_selection.get_currency()
        cust.set_currency(curr)

        cust.set_delivery_method(DeliveryMethod.from_int(self.preferred_delivery_combo.get_active()))
        start = self.notes_buffer.get_start_iter()
        end = self.notes_buffer.get_end_iter()
        notes = self.notes_buffer.get_text(start, end, False)
        cust.set_notes(notes)
        cust.set_opening_balance(self.opening_balance.get_amount(), self.opening_date.get_date())
        cust.set_tax(self.tax_selection.get_ref())
        cust.set_tax_included(self.tax_check.get_active())
        cust.set_tin(self.tin_entry.get_text())
        cust.set_term(self.terms.get_ref())
        cust.set_location(self.location_selection.get_ref())
        cust.commit_edit()
        ComponentManager.resume()
        self.customer = cust

    def finish_ok(self):
        gs_set_busy_cursor(None, True)
        be = self.book.get_backend()
        if be is not None:
            with be.add_lock():
                self.to_customer()
        self.created_customer = self.customer
        self.customer = None
        self.close()
        gs_unset_busy_cursor(None)
        ComponentManager.close(self.component_id)

    @Gtk.Template.Callback()
    def validate(self, *_):
        model = self.display_name_combo.get_model()
        _iter = self.display_name_combo.get_active_iter()
        sensitive = True
        display_name = None
        if _iter is not None:
            display_name = model.get_value(_iter, 0)
        gs_widget_remove_style_context(self.full_name_entry, "error")
        self.full_name_entry.set_tooltip_text("")
        self.display_name_combo.set_tooltip_text("")
        gs_widget_remove_style_context(self.display_name_combo, "error")
        if display_name == "" or display_name is None:
            message = "Display name cannot be null."
            gs_widget_set_style_context(self.display_name_combo, "error")
            self.display_name_combo.set_tooltip_text(message)
            sensitive = False
        elif self.dialog_type == CustomerDialogType.NEW:
            if Customer.lookup_display_name(Session.get_current_book(),display_name) is not None:
                message = "The customer with display name %s already exists" % display_name
                gs_widget_set_style_context(self.display_name_combo, "error")
                self.display_name_combo.set_tooltip_text(message)
                sensitive = False

            full_name = self.full_name_entry.get_text()
            if Customer.lookup_name(Session.get_current_book(), full_name) is not None:
                message = "The customer %s already exists" % full_name
                self.full_name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning")
                gs_widget_set_style_context(self.full_name_entry, "error")
                self.full_name_entry.set_tooltip_text(message)
                sensitive = False
            else:
                self.full_name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "emblem-default")
        gs_widget_remove_style_context(self.billing_address_frame.get_label_widget(), "error")
        self.billing_address_frame.street_entry.set_tooltip_text("")
        gs_widget_remove_style_context(self.billing_address_frame.street_entry, "error")
        if not self.billing_address_frame.validate():
            self.billing_address_frame.street_entry.set_tooltip_text("Please fill in the address details")
            gs_widget_set_style_context(self.billing_address_frame.get_label_widget(), "error")
            sensitive = False
        self.location_selection.set_tooltip_text("")
        gs_widget_remove_style_context(self.location_frame.get_label_widget(), "error")
        if self.location_selection.get_ref() is None:
            self.location_selection.set_tooltip_text(
                "Please select or add a location Under Payment and Billing>Location")
            gs_widget_set_style_context(self.location_frame.get_label_widget(), "error")
            sensitive = False
        gs_widget_remove_style_context(self.terms_frame.get_label_widget(), "error")
        self.terms.set_tooltip_text("")
        if self.terms.get_ref() is None:
            self.terms.set_tooltip_text("Please select or add a Term Agreement Under Payment and Billing>Payment Terms")
            gs_widget_set_style_context(self.terms_frame.get_label_widget(), "error")
            sensitive = False
        gs_widget_remove_style_context(self.preferred_payment_frame.get_label_widget(), "error")
        if self.preferred_payment_method.get_ref() is None:
            self.preferred_payment_method.set_tooltip_text(
                "Please select or add a payment method Under Payment and Billing>Payment Method")
            gs_widget_set_style_context(self.preferred_payment_frame.get_label_widget(), "error")
            sensitive = False
        if sensitive:
            self.ok_button.set_tooltip_text("Press Okay to save customer")
        else:
            self.ok_button.set_tooltip_text("Please fix the errors to continue")
        self.ok_button.set_sensitive(sensitive)

    def window_response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            dialog.close()

    def address_same_checkbox_toggled(self, widget, sa_grid):
        active = widget.get_active()
        self.shipping_address_frame.set_grid_sensitive(not active)
        self.shipping_address_frame.share_buffer(self.billing_address_frame if active else None)

    @Gtk.Template.Callback()
    def display_name_changed(self, *args):
        model = self.display_name_combo.get_model()
        _iter = self.display_name_combo.get_active_iter()
        model.clear()
        human_name = HumanName(full_name=self.full_name_entry.get_text())

        for disp_name in sorted(set(
                map(str.strip,
                    ["{} {} {} {} {}".format(human_name.suffix, human_name.first,
                                             human_name.middle, human_name.last, human_name.suffix).replace("  ", " "),
                     "{} {}".format(human_name.first, human_name.last).replace("  ", " "),
                     "{} {}".format(human_name.last, human_name.first).replace("  ", " "),
                     "{} {} {}".format(human_name.middle, human_name.last, human_name.first).replace("  ", " ")
                     ]))):
            if len(disp_name) == 0:
                continue
            model.append([disp_name])
        self.display_name_combo.set_active(0)

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        inv = self.customer
        if inv is None:
            ComponentManager.close(self.component_id)
            return
        if changes:
            info = ComponentManager.get_entity_events(changes, inv.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(CustomerDialog)
