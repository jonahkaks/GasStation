import datetime

from gi.repository import Gtk, GObject

from gasstation.utilities.custom_dialogs import show_error, ask_ok_cancel
from gasstation.views.invoice_entry.ledger import InvoiceLedger
from gasstation.views.invoice_entry.register import InvoiceRegister
from gasstation.views.selections import ReferenceSelection, DateSelection
from libgasstation import Location, Term, Customer
from libgasstation.core.component import ComponentManager


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/customer/invoice.ui')
class InvoiceDialog(Gtk.Dialog):
    __gtype_name__ = "InvoiceDialog"
    customer_frame: Gtk.Frame = Gtk.Template.Child()
    terms_frame: Gtk.Frame = Gtk.Template.Child()
    email_entry: Gtk.Entry = Gtk.Template.Child()
    tin_entry: Gtk.Entry = Gtk.Template.Child()
    memo_entry: Gtk.TextView = Gtk.Template.Child()
    billing_entry: Gtk.TextView = Gtk.Template.Child()
    shipping_entry: Gtk.TextView = Gtk.Template.Child()
    shipping_via_entry: Gtk.Entry = Gtk.Template.Child()
    ledger_frame: Gtk.Frame = Gtk.Template.Child()
    location_frame: Gtk.Frame = Gtk.Template.Child()
    id_label: Gtk.Label = Gtk.Template.Child()
    date_selection: DateSelection = Gtk.Template.Child()
    shipping_date_selection: DateSelection = Gtk.Template.Child()
    due_date_selection: DateSelection = Gtk.Template.Child()

    def __init__(self, parent, invoice, is_edit=False):
        super().__init__()
        if parent is not None:
            self.set_transient_for(parent)
        self.set_title("Edit Invoice" if is_edit else "New Invoice")
        self.maximize()
        self.is_edit = is_edit
        self.tree_view = None
        self.info_label = None
        self.width = 0
        self.terms = None
        self.is_credit_note = False
        self.book = None
        self.created_invoice = None
        self.invoice = invoice
        self.reset_taxes = False
        self.component_id = 0

        self.location = ReferenceSelection(Location, use_first=False)
        self.location_frame.add(self.location)
        self.location_frame.show()

        self.customer_selection = ReferenceSelection(Customer)
        self.customer_selection.connect("selection_changed", self.customer_changed_cb)
        self.customer_frame.add(self.customer_selection)
        self.customer_selection.show()

        self.terms = ReferenceSelection(Term, None)
        self.terms_frame.add(self.terms)
        self.terms_frame.show()
        self.terms.connect("selection_changed", self.terms_changed_cb)

        self.memo_buffer: Gtk.TextBuffer = self.memo_entry.get_buffer()
        self.billing_buffer = self.billing_entry.get_buffer()

        self.shipping_buffer = self.shipping_entry.get_buffer()
        self.shipping_via_buffer = self.shipping_via_entry.get_buffer()

        book = invoice.get_book()
        ld = InvoiceLedger(book)
        self.id_label.set_text(self.invoice.get_id())
        self.customer_selection.set_ref(invoice.get_customer())
        ld.set_default_invoice(self.invoice)
        self.ledger = ld
        reg = InvoiceRegister(ld, self.get_window())
        self.ledger.view.set_location(self.location.get_ref())
        self.location.connect("selection_changed", self.ledger.view.location_changed_cb)
        self.location.connect("selection_changed", self.location_changed_cb)
        reg.show()
        self.label = Gtk.Label()
        self.label.set_use_markup(True)
        self.label.set_markup("<b>Please Select a location where this sale is made</b>")
        self.ledger_frame.add(self.label)
        self.ledger_frame.show()
        self.reg = reg
        self.connect("delete-event", self.delete_event_cb)
        self.location.grab_focus()

        self.show_all()

    @classmethod
    def new_edit(cls, parent, invoice):
        self = cls(parent, invoice, True)
        loc = invoice.get_location()
        term = invoice.get_terms()
        self.terms.set_ref(term)
        self.location.set_ref(loc)
        self.date_selection.set_date(invoice.get_date_posted())
        self.due_date_selection.set_date(invoice.get_date_due())

    def location_changed_cb(self, widget, location):
        if location is not None:
            self.ledger_frame.remove(self.label)
            self.ledger_frame.add(self.reg)
        else:
            self.ledger_frame.remove(self.reg)
            self.ledger_frame.add(self.label)

    def terms_changed_cb(self, *args):
        term = self.terms.get_ref()
        days = term.get_due_days()
        date = self.date_selection.get_date()
        self.due_date_selection.set_date(date + datetime.timedelta(days=days))

    def customer_changed_cb(self, *args):
        customer = self.customer_selection.get_ref()
        if customer is None:
            return
        contact = customer.get_contact()
        self.set_title("{} New Invoice".format(contact.get_display_name()))
        if not any(contact.addresses):
            return
        ba = contact.addresses[0]
        if len(contact.addresses) == 1:
            sa = ba
        else:
            sa = contact.addresses[1]
        print(ba, sa)
        self.email_entry.set_text(",".join(map(lambda a: a.get_address(), contact.emails)))
        self.tin_entry.set_text(customer.get_tin())
        self.billing_buffer.set_text(str(ba))
        self.shipping_buffer.set_text(str(sa))
        self.terms.set_ref(customer.get_term())
        self.invoice.set_customer(customer)

    @Gtk.Template.Callback()
    def leave_memo_cb(self, widget, event):
        if self.invoice is None:
            return False
        start = self.memo_buffer.get_start_iter()
        end = self.memo_buffer.get_end_iter()
        memo = self.memo_buffer.get_text(start, end, False)
        self.invoice.set_notes(memo)
        return False

    @Gtk.Template.Callback()
    def save_invoice(self, _):
        if self.invoice is None:
            return
        ComponentManager.suspend()
        book = self.invoice.get_book()
        loc = self.location.get_ref()
        if loc is None:
            show_error(self, "Please select a location for the invoice")
            return False
        customer = self.customer_selection.get_ref()
        book = self.invoice.get_book()
        be = book.get_backend()
        with be.add_lock():
            self.invoice.set_customer(customer)
            self.invoice.set_currency(customer.get_currency())
            self.invoice.set_location(loc)
            self.invoice.set_terms(self.terms.get_ref())
            self.invoice.set_date_posted(self.date_selection.get_date())
            self.invoice.set_date_due(self.due_date_selection.get_date())
            self.invoice.set_id(book.increment_and_format_counter("Invoice"))
            self.invoice.post_to_account(receivable_account=customer.get_receivable_account(),
                                         post_date=self.date_selection.get_date())
            self.invoice.commit_edit()
        self.ledger.finish()
        self.destroy()
        ComponentManager.resume()

    def delete_event_cb(self, *args):
        if self.invoice is not None and self.invoice.get_dirty() and not self.is_edit and ask_ok_cancel(self,
                                                                                                        message="Cancel and Delete Invoice?",
                                                                                                        title="Cancel Invoice") == Gtk.ResponseType.OK:
            self.invoice.destroy()
            self.invoice = None
            del self.ledger
            return False
        return True


GObject.type_register(InvoiceDialog)
