from gi.repository import Gtk

from gasstation.utilities.dialog import gs_widget_set_style_context


class ObjectReferencesDialog:
    def __init__(self, explanation_text, objlist):
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/object-references.ui",
                                          ["object_references_dialog"])
        dialog = builder.get_object("object_references_dialog")
        gs_widget_set_style_context(dialog, "ObjectRefDialog")
        explanation = builder.get_object("lbl_explanation")
        explanation.set_text(explanation_text)
        store = Gtk.ListStore(str)
        for inst in objlist:
            store.append([str(inst)])
        listview = Gtk.TreeView.new_with_model(model=store)
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Object", renderer, text=0)
        listview.append_column(column)
        box = builder.get_object("hbox_list")
        sw = Gtk.ScrolledWindow()
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        sw.add(listview)
        box.pack_start(sw, True, True, 0)
        builder.connect_signals(dialog)
        dialog.show_all()
        dialog.run()
        dialog.destroy()
