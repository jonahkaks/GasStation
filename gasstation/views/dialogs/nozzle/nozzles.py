from gasstation.utilities.cursors import *
from gasstation.utilities.custom_dialogs import *
from gasstation.views.nozzle import *
from libgasstation import Location
from .nozzle import NozzleDialog

DIALOG_NOZZLES_CM_CLASS = "dialog-nozzles"
STATE_SECTION = "dialogs/edit_nozzles"


class NozzlesDialog(GObject.Object):
    def __init__(self):
        GObject.Object.__init__(self)
        self.dialog = None
        self.session = None
        self.book = None
        self.nozzle_tree = None
        self.edit_button = None
        self.remove_button = None
        self.component_id = 0

    def window_destroy_cb(self, widget):
        ComponentManager.unregister(self.component_id)

    def edit_clicked(self):
        nozzles = self.nozzle_tree.get_selected_nozzles()
        book = Session.get_current_book()
        if any(nozzles):
            for nozzle in nozzles:
                NozzleDialog(self.dialog, book, nozzle=nozzle).run()
        ComponentManager.refresh_all()

    def row_activated_cb(self, view, path, column):
        self.edit_clicked()

    def remove_clicked(self):
        from gasstation.views.dialogs.object_references import ObjectReferencesDialog
        nozzles = self.nozzle_tree.get_selected_nozzles()
        if nozzles is None or not any(nozzles):
            return
        dialog = Gtk.MessageDialog(transient_for=self.dialog, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE,
                                   message_format="The following nozzles will be deleted %s" % ",".join(
                                       [nozzle.get_name() for nozzle in nozzles]))
        dialog.add_buttons("Cancel", Gtk.ResponseType.CANCEL, "Delete", Gtk.ResponseType.ACCEPT)
        dialog.set_default_response(Gtk.ResponseType.CANCEL)
        response = dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            gs_set_busy_cursor(None, True)
            ComponentManager.suspend()
            for nozzle in nozzles:
                lis = nozzle.get_referrers()
                if len(lis):
                    exp = "The list below shows objects which make use of the nozzle which you want to delete.\n" \
                          "Before you can delete it, you must either delete those objects or else modify them so they " \
                          "make use of another nozzle"
                    ObjectReferencesDialog(exp, lis)
                    continue
                nozzle.begin_edit()
                nozzle.destroy()
            ComponentManager.resume()
            gs_unset_busy_cursor(None)

    def add_clicked(self):
        nozzle = NozzleDialog(self.dialog, Session.get_current_book())
        nozzle.run()

    def dialog_response(self, dialog, response):
        if response == RESPONSE_NEW:
            self.add_clicked()
            return

        elif response == RESPONSE_DELETE:
            self.remove_clicked()
            return

        elif response == RESPONSE_EDIT:
            self.edit_clicked()
            return
        else:
            ComponentManager.close(self.component_id)

    def selection_changed(self, selection):
        inv = any(self.nozzle_tree.get_selected_nozzles())
        self.edit_button.set_sensitive(inv)
        self.remove_button.set_sensitive(inv)

    def create(self, parent):
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/common.ui", ["common_dialog"])
        self.dialog = builder.get_object("common_dialog")
        self.dialog.set_title("Nozzles Dialog")
        self.session = Session.get_current_session()
        self.book = self.session.get_book()
        gs_widget_set_style_context(self.dialog, "NozzlesDialog")
        builder.connect_signals(self)
        if parent is not None:
            self.dialog.set_transient_for(parent)
        self.remove_button = builder.get_object("remove_button")
        self.edit_button = builder.get_object("edit_button")
        scrolled_window = builder.get_object("common_list_window")
        view = NozzleView(state_section=STATE_SECTION)
        self.nozzle_tree = view
        scrolled_window.add(view)
        scrolled_window.show()
        view.set_headers_visible(True)
        selection = view.get_selection()
        selection.connect("changed", self.selection_changed)
        self.nozzle_tree.connect("row-activated", self.row_activated_cb)

    def close_handler(self):
        self.dialog.destroy()
        self.nozzle_tree.destroy()

    def show_handler(self, klass):
        self.dialog.present()
        return True

    @classmethod
    def new(cls, parent):
        if ComponentManager.forall(DIALOG_NOZZLES_CM_CLASS, cls.show_handler):
            return
        if not any(Location.get_list(Session.get_current_book())):
            show_error(parent, "To add a nozzle you need to create some locations under Business>Location")
            return
        iv = cls()
        iv.create(parent)
        iv.component_id = ComponentManager.register(DIALOG_NOZZLES_CM_CLASS, None, iv.close_handler)
        ComponentManager.set_session(iv.component_id, iv.session)
        iv.nozzle_tree.grab_focus()
        iv.dialog.show()
