from decimal import Decimal

from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import *
from gasstation.views.dialogs.location.location import LocationDialog
from gasstation.views.dialogs.pump.pump import PumpDialog
from gasstation.views.dialogs.tank.tank import TankDialog
from gasstation.views.selections import ReferenceSelection
from libgasstation import Location
from libgasstation.core import Pump, Tank, Nozzle, ID_NOZZLE
from libgasstation.core.component import *
from libgasstation.core.session import Session

DIALOG_NEW_NOZZLE_CM_CLASS = "dialog-new-nozzle"
DIALOG_EDIT_NOZZLE_CM_CLASS = "dialog-edit-nozzle"
from gi.repository import GObject


class NozzleDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/fuel/nozzle.ui")
class NozzleDialog(Gtk.Dialog):
    __gtype_name__ = "NozzleDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()
    opening_meter_entry: Gtk.SpinButton = Gtk.Template.Child()
    nozzle_grid: Gtk.Grid = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, nozzle=None, name="", **kwargs):
        super().__init__(**kwargs)
        self.book = book
        self.nozzle = nozzle
        self.dialog_type = NozzleDialogType.NEW if nozzle is None else NozzleDialogType.EDIT
        self.set_title("Add/Edit Nozzle")
        if parent is not None:
            self.set_transient_for(parent)
        self.get_style_context().add_class("NozzleDialog")
        self.set_default_response(Gtk.ResponseType.OK)
        if not modal:
            self.connect("response", self.window_response_cb)
        else:
            self.set_modal(True)

        self.location_selection = ReferenceSelection(Location, LocationDialog)
        self.location_selection.set_new_ability(True)
        self.nozzle_grid.attach(self.location_selection, 1, 1, 1, 1)
        self.location_selection.show()

        self.tank_selection = ReferenceSelection(Tank, TankDialog, depend_on=self.location_selection, depends_cb=Tank.get_location)
        self.tank_selection.set_new_ability(True)
        self.nozzle_grid.attach(self.tank_selection, 1, 2, 1, 1)
        self.tank_selection.show()

        self.pump_selection = ReferenceSelection(Pump, PumpDialog, depend_on=self.location_selection, depends_cb=Pump.get_location)
        self.pump_selection.set_new_ability(True)
        self.nozzle_grid.attach(self.pump_selection, 1, 3, 1, 1)
        self.pump_selection.show()
        gs_window_adjust_for_screen(self)

        self.component_id = ComponentManager.register(DIALOG_NEW_NOZZLE_CM_CLASS,
                                                      self.refresh_handler,
                                                      None if modal else self.close_handler)

        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id,
                                           ID_NOZZLE,
                                           EVENT_MODIFY | EVENT_DESTROY)
        if name is not None:
            self.name_entry.set_text(name)
        self.show()
        if nozzle is not None:
            self.to_ui()

    def to_ui(self):
        nozzle: Nozzle = self.nozzle
        self.name_entry.set_text(nozzle.get_name())
        self.pump_selection.set_ref(nozzle.get_pump())
        self.tank_selection.set_ref(nozzle.get_tank())
        self.location_selection.set_ref(nozzle.get_location())
        if self.dialog_type != NozzleDialogType.NEW:
            self.opening_meter_entry.set_sensitive(False)
            self.opening_meter_entry.set_value(nozzle.get_opening_meter())

    def to_nozzle(self):
        nozzle = self.nozzle
        if nozzle is None:
            nozzle = Nozzle(self.book)
            self.nozzle = nozzle
        name = self.name_entry.get_text()
        nozzle.begin_edit()
        nozzle.set_name(name)
        nozzle.set_pump(self.pump_selection.get_ref())
        nozzle.set_tank(self.tank_selection.get_ref())
        nozzle.set_location(self.location_selection.get_ref())
        if self.dialog_type == NozzleDialogType.NEW:
            nozzle.set_opening_meter(Decimal(self.opening_meter_entry.get_value()))

    def finish_ok(self):
        self.to_nozzle()
        self.nozzle.commit_edit()
        self.close()
        ComponentManager.close(self.component_id)

    def common_ok(self):
        name = self.name_entry.get_text()
        if name == "":
            message = "The Nozzle must be given a name."
            show_error(self, message)
            return False
        loc = self.location_selection.get_ref()
        if Nozzle.lookup_name_with_location(Session.get_current_book(),
                                            name, loc) is not None and self.dialog_type == NozzleDialogType.NEW:
            show_error(self, "The nozzle %s already exists for the location" % name)
            return False
        if self.pump_selection.get_ref() is None:
            show_error(self, "No pump selected")
            return False
        if self.tank_selection.get_ref() is None:
            show_error(self, "No tank selected")
            return False
        if loc is None:
            show_error(self, "No location selected")
            return False

        return True

    def window_response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            if not self.common_ok():
                return False
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            dialog.close()

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        nozzle = self.nozzle
        if nozzle is None:
            ComponentManager.close(self.component_id)
            return

        if changes:
            info = ComponentManager.get_entity_events(changes, nozzle.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(NozzleDialog)
