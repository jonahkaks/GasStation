from gasstation.views.dialogs.account.transfer import *
from .about import *
from .account import *
from .commodity import *
from .customer import *
from .product import *
from .scheduled import *
from .setup import *
from .splash_screen import *
from .supplier import *
from .tank import *

