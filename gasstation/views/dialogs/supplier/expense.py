from gi.repository import Gtk, GObject

from gasstation.utilities.custom_dialogs import ask_ok_cancel, show_error
from gasstation.views.bill_entry.ledger import *
from gasstation.views.bill_entry.register import BillEntryRegister, BillAccountEntryRegister
from gasstation.views.dialogs.location.location import LocationDialog
from gasstation.views.dialogs.payment.payment_method import PaymentMethodDialog
from gasstation.views.selections import ReferenceSelection, DateSelection
from gasstation.views.selections.account import AccountSelection
from libgasstation import Location, Product, ProductLocation, Supplier, Session, AccountType
from libgasstation.core.payment import PaymentMethod


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/supplier/expense.ui')
class ExpenseDialog(Gtk.Window):
    __gtype_name__ = "ExpenseDialog"
    payee_frame: Gtk.Frame = Gtk.Template.Child()
    payment_account_frame: Gtk.Frame = Gtk.Template.Child()
    payment_date_frame: Gtk.Frame = Gtk.Template.Child()
    payment_method_frame: Gtk.Frame = Gtk.Template.Child()
    payment_ref: Gtk.Entry = Gtk.Template.Child()
    memo_entry: Gtk.TextView = Gtk.Template.Child()
    categories_expander: Gtk.Expander = Gtk.Template.Child()
    items_expander: Gtk.Expander = Gtk.Template.Child()
    location_frame: Gtk.Frame = Gtk.Template.Child()
    id_label: Gtk.Label = Gtk.Template.Child()

    def __init__(self, parent, expense):
        super().__init__()
        if parent is not None:
            self.set_transient_for(parent)
        self.set_title("New Expense")
        self.maximize()
        self.show_all()
        self.tree_view = None
        self.info_label = None
        self.width = 0
        self.terms = None
        self.is_credit_note = False
        self.book = None
        self.created_expense = None
        self.reset_taxes = False
        self.component_id = 0

        self.location = ReferenceSelection(Location, LocationDialog)
        self.location_frame.add(self.location)
        self.location_frame.show()
        self.location.set_new_ability(True)

        self.payee_choice = ReferenceSelection(Supplier)
        self.payee_choice.connect("selection_changed", self.payee_changed_cb)
        self.payee_frame.add(self.payee_choice)
        self.payee_choice.show()
        self.payee_frame.show_all()

        self.payment_method = ReferenceSelection(PaymentMethod, PaymentMethodDialog)
        self.payment_method.set_new_ability(True)
        self.payment_method_frame.add(self.payment_method)

        self.payment_account = AccountSelection()
        self.payment_account.set_account_filters([AccountType.BANK, AccountType.CASH])
        self.payment_account_frame.add(self.payment_account)

        self.memo_buffer: Gtk.TextBuffer = self.memo_entry.get_buffer()

        self.payment_date = DateSelection()
        self.payment_date_frame.add(self.payment_date)
        self.payment_date.show()

        book = Session.get_current_book()
        ld = BillLedger(book)
        self.expense = expense
        self.id_label.set_text(self.expense.get_book().format_counter("Expense"))

        ld.set_default_bill(self.expense)
        self.ledger = ld
        self.items_expander.add(BillEntryRegister(ld, self.get_window()))
        self.items_expander.show_all()
        self.categories_expander.add(BillAccountEntryRegister(ld, self.get_window()))
        self.categories_expander.show_all()
        self.location.connect("selection_changed", self.ledger.view.location_changed_cb)
        self.location.connect("selection_changed", self.ledger.account_view.location_changed_cb)
        self.ledger.view.set_location(self.location.get_ref())
        self.ledger.account_view.set_location(self.location.get_ref())
        self.connect("delete-event", self.delete_event_cb)
        self.show_all()
        self.payee_choice.set_ref(expense.get_supplier())
        self.location.grab_focus()


    def payee_changed_cb(self, *args):
        supplier = self.payee_choice.get_ref()
        contact = supplier.get_contact()
        self.set_title("{} New Expense".format(contact.get_display_name()))
        self.expense.set_supplier(supplier)

    @Gtk.Template.Callback()
    def leave_memo_cb(self, widget, event):
        if self.expense is None:
            return False
        start = self.memo_buffer.get_start_iter()
        end = self.memo_buffer.get_end_iter()
        memo = self.memo_buffer.get_text(start, end, False)
        self.expense.set_notes(memo)
        return False

    @Gtk.Template.Callback()
    def save_expense(self, _):
        ComponentManager.suspend()
        book = self.expense.get_book()
        be = book.get_backend()
        loc = self.location.get_ref()
        if loc is None:
            show_error(self, "Please select a location for the expense")
            return False
        if be is not None:
            with be.add_lock():
                supplier = self.payee_choice.get_ref()
                self.expense.set_supplier(supplier)
                self.expense.set_currency(supplier.get_currency())
                self.expense.set_location(loc)
                self.expense.set_id(book.increment_and_format_counter("Expense"))
                transaction = self.expense.post_to_account(payable_account=self.payment_account.get_account(),
                                          post_date=self.payment_date.get_date(), auto_pay=True)
                transaction.set_num(self.payment_ref.get_text() or "")
                for entry in self.expense.entries:
                    prod: Product = entry.get_product()
                    for pq in filter(lambda pq: pq.get_location() == self.expense.get_location(), prod.quantities):
                        if self.expense.get_is_debit_note():
                            pq.set_current_stock(pq.get_current_stock() - entry.get_quantity())
                        else:
                            pq.set_current_stock(pq.get_current_stock() + entry.get_quantity())
                    else:
                        pq = ProductLocation(self.expense.get_book())
                        prod.begin_edit()
                        pq.set_location(self.expense.get_location())
                        pq.set_current_stock(entry.get_quantity())
                        prod.quantities.append(pq)
                        prod.commit_edit()
                self.expense.commit_edit()
        self.ledger.finish()
        self.ledger = None

        self.destroy()
        ComponentManager.resume()

    def delete_event_cb(self, *args):
        if ask_ok_cancel(self, message="Cancel and Delete Expense?", title="Cancel Expense") == Gtk.ResponseType.OK:
            ComponentManager.suspend()
            if self.expense is not None:
                self.expense.destroy()
                self.expense = None
            del self.ledger
            return False
        return True


GObject.type_register(ExpenseDialog)
