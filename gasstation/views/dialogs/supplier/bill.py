import datetime

from gi.repository import Gtk, GObject

from gasstation.utilities.custom_dialogs import ask_ok_cancel, show_error
from gasstation.views.bill_entry.ledger import *
from gasstation.views.bill_entry.register import BillEntryRegister, BillAccountEntryRegister
from gasstation.views.dialogs.location.location import LocationDialog
from gasstation.views.dialogs.term.new import NewPaymentTermDialog
from gasstation.views.selections import DateSelection
from gasstation.views.selections import ReferenceSelection
from libgasstation import Location, Term, Supplier, Session


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/supplier/bill.ui')
class BillDialog(Gtk.Dialog):
    __gtype_name__ = "BillDialog"
    supplier_frame: Gtk.Frame = Gtk.Template.Child()
    terms_frame: Gtk.Frame = Gtk.Template.Child()
    email_entry: Gtk.Entry = Gtk.Template.Child()
    tin_entry: Gtk.Entry = Gtk.Template.Child()
    memo_entry: Gtk.TextView = Gtk.Template.Child()
    due_date_frame: Gtk.Frame = Gtk.Template.Child()
    date_frame: Gtk.Frame = Gtk.Template.Child()
    mailing_address_entry: Gtk.TextView = Gtk.Template.Child()
    categories_expander: Gtk.Expander = Gtk.Template.Child()
    items_expander: Gtk.Expander = Gtk.Template.Child()
    location_frame: Gtk.Frame = Gtk.Template.Child()
    id_label: Gtk.Label = Gtk.Template.Child()

    def __init__(self, parent, bill):
        super().__init__()

        if parent is not None:
            self.set_transient_for(parent)
        self.set_title("New Bill")
        self.maximize()
        self.show_all()
        self.tree_view = None
        self.info_label = None
        self.width = 0
        self.terms = None
        self.is_credit_note = False
        self.book = None
        self.created_bill = None
        self.reset_taxes = False
        self.component_id = 0

        self.location = ReferenceSelection(Location, LocationDialog)
        self.location_frame.add(self.location)
        self.location_frame.show()
        self.location.set_new_ability(True)

        self.supplier_choice = ReferenceSelection(Supplier)
        self.supplier_choice.connect("selection_changed", self.supplier_changed_cb)
        self.supplier_frame.add(self.supplier_choice)
        self.supplier_choice.show()
        self.supplier_frame.show_all()

        self.terms = ReferenceSelection(Term, NewPaymentTermDialog, depend_on=self.supplier_choice,
                                        references_cb=Supplier.get_term)
        self.terms.set_new_ability(True)
        self.terms_frame.add(self.terms)
        self.terms_frame.show()
        self.terms.connect("selection_changed", self.terms_changed_cb)

        self.memo_buffer: Gtk.TextBuffer = self.memo_entry.get_buffer()
        self.mailing_buffer: Gtk.TextBuffer = self.mailing_address_entry.get_buffer()

        self.date = DateSelection()
        self.date_frame.add(self.date)
        self.date.show()
        self.date.set_sensitive(False)

        self.due_date = DateSelection()
        self.due_date_frame.add(self.due_date)
        self.due_date.show()

        book = Session.get_current_book()
        ld = BillLedger(book)
        self.bill = bill
        self.id_label.set_text(self.bill.get_book().format_counter("Bill"))

        ld.set_default_bill(self.bill)
        self.ledger = ld
        self.items_expander.add(BillEntryRegister(ld, self.get_window()))
        self.items_expander.show_all()
        self.categories_expander.add(BillAccountEntryRegister(ld, self.get_window()))
        self.categories_expander.show_all()
        self.location.connect("selection_changed", self.ledger.view.location_changed_cb)
        self.location.connect("selection_changed", self.ledger.account_view.location_changed_cb)
        self.ledger.view.set_location(self.location.get_ref())
        self.ledger.account_view.set_location(self.location.get_ref())
        self.connect("delete-event", self.delete_event_cb)
        self.show_all()
        self.supplier_choice.set_ref(bill.get_supplier())
        self.location.grab_focus()

    def terms_changed_cb(self, *args):
        term = self.terms.get_ref()
        if term is None:
            return
        days = term.get_due_days()
        date = self.date.get_date()
        self.due_date.set_date(date + datetime.timedelta(days=days))

    def supplier_changed_cb(self, *args):
        supplier = self.supplier_choice.get_ref()
        contact = supplier.get_contact()
        self.set_title("{} New Bill".format(contact.get_display_name()))
        ba = contact.addresses[0]
        self.mailing_buffer.set_text(str(ba))
        self.email_entry.set_text(",".join(map(lambda a: a.get_address(), contact.emails)))
        tin = supplier.get_tin()
        if tin is not None:
            self.tin_entry.set_text(tin)
        term = supplier.get_term()
        if term is not None:
            self.terms.set_ref(term)
        self.bill.set_supplier(supplier)

    @Gtk.Template.Callback()
    def leave_memo_cb(self, widget, event):
        if self.bill is None:
            return False
        start = self.memo_buffer.get_start_iter()
        end = self.memo_buffer.get_end_iter()
        memo = self.memo_buffer.get_text(start, end, False)
        self.bill.set_notes(memo)
        return False

    @Gtk.Template.Callback()
    def save_bill(self, _):
        ComponentManager.suspend()
        book = self.bill.get_book()
        loc = self.location.get_ref()

        terms = self.terms.get_ref()
        if terms is None:
            show_error(self, "Please select the supplier terms")
            return False
        if loc is None:
            show_error(self, "Please select a location for the bill")
            return False
        supplier = self.supplier_choice.get_ref()
        self.bill.set_supplier(supplier)
        self.bill.set_currency(supplier.get_currency())
        self.bill.set_location(loc)
        self.bill.set_terms(terms)
        self.bill.set_id(book.increment_and_format_counter("Bill"))
        self.bill.set_date_due(self.due_date.get_date())
        self.bill.post_to_account(payable_account=supplier.get_payable_account(),
                                  post_date=self.date.get_date())
        self.bill.commit_edit()
        self.ledger.finish()
        self.ledger = None

        self.destroy()
        ComponentManager.resume()

    def delete_event_cb(self, *args):
        if ask_ok_cancel(self, message="Cancel and Delete Bill?", title="Cancel Bill") == Gtk.ResponseType.OK:
            ComponentManager.suspend()
            if self.bill is not None:
                self.bill.destroy()
                self.bill = None
            return False
        return True


GObject.type_register(BillDialog)
