from gi.repository import GObject
from nameparser import HumanName

from gasstation.utilities.cursors import gs_set_busy_cursor, gs_unset_busy_cursor
from gasstation.utilities.dialog import *
from gasstation.utilities.ui import PrintAmountInfo, gs_default_currency
from gasstation.views.components.address import AddressFrame
from gasstation.views.components.avatar import Avatar
from gasstation.views.dialogs.payment.payment_method import PaymentMethodDialog
from gasstation.views.dialogs.tax import NewTaxDialog
from gasstation.views.dialogs.term.new import NewPaymentTermDialog
from gasstation.views.selections.amount_edit import AmountEntry
from gasstation.views.selections.currency import CurrencySelection
from gasstation.views.selections.date_edit import DateSelection
from gasstation.views.selections.reference import ReferenceSelection
from gasstation.views.selections.telephone_entry import TelNumberEntry
from libgasstation import Contact, Tax, PaymentMethod
from libgasstation.core.address import Address
from libgasstation.core.component import *
from libgasstation.core.email import Email, EmailForType
from libgasstation.core.phone import Phone, PhoneType
from libgasstation.core.session import Session
from libgasstation.core.supplier import Supplier, ID_SUPPLIER
from libgasstation.core.term import Term

DIALOG_NEW_SUPPLIER_CM_CLASS = "dialog-new-supplier"
DIALOG_EDIT_SUPPLIER_CM_CLASS = "dialog-edit-supplier"


class SupplierDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/supplier/supplier.ui')
class SupplierDialog(Gtk.Dialog):
    __gtype_name__ = "SupplierDialog"
    avatar: Avatar = Gtk.Template.Child()
    full_name_entry: Gtk.Entry = Gtk.Template.Child()
    display_name_combo: Gtk.ComboBox = Gtk.Template.Child()

    email_entry: Gtk.Entry = Gtk.Template.Child()
    phone_entry: TelNumberEntry = Gtk.Template.Child()
    mobile_entry: TelNumberEntry = Gtk.Template.Child()

    website_entry: Gtk.Entry = Gtk.Template.Child()

    address_frame:AddressFrame = Gtk.Template.Child()
    tax_check: Gtk.CheckButton = Gtk.Template.Child()
    tax_frame: Gtk.Frame = Gtk.Template.Child()
    notes_entry: Gtk.TextView = Gtk.Template.Child()
    tin_entry: Gtk.Entry = Gtk.Template.Child()
    terms_frame: Gtk.Frame = Gtk.Template.Child()
    currency_frame: Gtk.Frame = Gtk.Template.Child()
    opening_balance_date_frame: Gtk.Frame = Gtk.Template.Child()
    opening_balance_frame: Gtk.Frame = Gtk.Template.Child()
    details_grid: Gtk.Grid = Gtk.Template.Child()
    payment_method_frame: Gtk.Frame = Gtk.Template.Child()
    billing_rate_frame: Gtk.Frame = Gtk.Template.Child()
    ok_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, supplier=None, name=None, **kwargs):
        super().__init__(**kwargs)
        self.book = book
        self.type = SupplierDialogType.NEW if supplier is None else SupplierDialogType.EDIT
        self.supplier = supplier
        self.created_supplier = None
        self.notes_buffer = self.notes_entry.get_buffer()
        self.address_frame.street_entry.connect("focus-out-event", self.validate)
        if parent is not None:
            self.set_transient_for(parent)

        self.get_style_context().add_class("SupplierDialog")
        if not modal:
            self.connect("response", self.window_response_cb)
        else:
            self.set_modal(True)
        self.set_default_response(Gtk.ResponseType.OK)
        sup = self.supplier
        if sup is not None:
            currency = sup.get_currency()
        else:
            currency = gs_default_currency()
        self.display_name_combo.set_model(Gtk.ListStore(str))
        self.terms = ReferenceSelection(Term, NewPaymentTermDialog)
        self.terms.set_new_ability(True)
        self.terms.connect("focus-out-event", self.validate)
        self.terms.connect("selection_changed", self.validate)
        self.terms_frame.add(self.terms)
        self.terms.show()

        self.payment_method = ReferenceSelection(PaymentMethod, PaymentMethodDialog)
        self.payment_method.set_new_ability(True)
        self.payment_method.connect("focus-out-event", self.validate)
        self.payment_method.connect("selection_changed", self.validate)
        self.payment_method_frame.add(self.payment_method)
        self.payment_method.show()

        self.tax_selection = ReferenceSelection(Tax, NewTaxDialog)
        self.tax_selection.set_new_ability(True)
        self.tax_frame.add(self.tax_selection)
        self.tax_selection.show()

        edit = CurrencySelection()
        edit.set_currency(currency)
        self.currency_edit = edit
        self.currency_frame.add(edit)
        edit.show()

        edit = AmountEntry()
        edit.set_evaluate_on_enter(True)
        print_info = PrintAmountInfo.integral(2)
        print_info.max_decimal_places = 5
        edit.set_print_info(print_info)
        self.opening_balance = edit
        self.opening_balance.connect("amount_changed", self.validate)
        self.opening_balance_frame.add(edit)
        edit.show()

        edit = AmountEntry()
        print_info = PrintAmountInfo.commodity(currency, False)
        edit.set_evaluate_on_enter(True)
        edit.set_print_info(print_info)
        self.billing_entry = edit
        self.billing_entry.connect("amount_changed", self.validate)
        self.billing_rate_frame.add(self.billing_entry)
        edit.show()
        self.opening_date = DateSelection()
        self.opening_balance_date_frame.add(self.opening_date)
        self.opening_date.show()
        if name is not None:
            self.full_name_entry.set_text(name)
        gs_window_adjust_for_screen(self)

        self.component_id = ComponentManager.register(DIALOG_NEW_SUPPLIER_CM_CLASS,
                                                      self.refresh_handler,
                                                      None if modal else self.close_handler)

        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id,
                                           ID_SUPPLIER,
                                           EVENT_MODIFY | EVENT_DESTROY)
        self.avatar.set_icon_name("avatar-default")
        self.avatar.set_valign(Gtk.Align.START)
        if supplier is not None:
            self.to_ui()
            self.validate()

    def to_ui(self):
        sup: Supplier = self.supplier
        contact = sup.get_contact()
        if contact is not None:
            self.full_name_entry.set_text(contact.get_full_name())
            if self.type == SupplierDialogType.EDIT:
                model = self.display_name_combo.get_model()
                self.display_name_changed()
                d = contact.get_display_name()
                _iter = model.get_iter_first()
                while _iter is not None:
                    tree_string = model.get_value(_iter, 0)
                    if GLib.utf8_collate(d, tree_string) == 0:
                        self.display_name_combo.set_active_iter(_iter)
                        break
                    _iter = model.iter_next(_iter)
            if any(contact.emails):
                self.email_entry.set_text(",".join(set(ea.get_address() for ea in contact.emails)))
            if any(contact.phones):
                works = list(filter(lambda a: a.type == PhoneType.WORK, contact.phones))
                if any(works):
                    self.phone_entry.set_number(works[0].get_number())
                mobiles = list(filter(lambda a: a.type == PhoneType.MOBILE, contact.phones))
                if any(mobiles):
                    self.mobile_entry.set_number(mobiles[0].get_number())

            self.website_entry.set_text(contact.get_website() or "")
            if any(contact.addresses):
                self.address_frame.set_address(contact.addresses[0])
        url = contact.get_image_url()
        self.avatar.set_fullname(contact.get_full_name())
        self.avatar.set_url(url)
        self.tin_entry.set_text(sup.get_tin() or "")
        self.notes_buffer.set_text(sup.get_notes() or "")
        self.terms.set_ref(sup.get_term())
        self.billing_entry.set_amount(sup.get_billing_rate())
        self.currency_edit.set_currency(sup.get_currency())
        self.tax_selection.set_ref(sup.get_tax())
        self.payment_method.set_ref(sup.get_payment_method())
        self.tax_check.set_active(sup.get_tax_included())
        txn = sup.get_opening_balance_transaction()
        if txn is not None:
            self.opening_date.set_date(txn.get_post_date())
            self.opening_balance.set_amount(sup.get_opening_balance())

    def to_supplier(self):
        sup: Supplier = self.supplier
        ComponentManager.suspend()
        if sup is None:
            sup = Supplier(self.book)
            self.supplier = sup
        sup.begin_edit()
        contact = sup.get_contact()
        if contact is None:
            contact = Contact(self.book)
            sup.set_contact(contact)
        contact.begin_edit()
        contact.set_image_url(self.avatar.get_url())
        contact.set_full_name(self.full_name_entry.get_text())
        contact.set_website(self.website_entry.get_text())
        model = self.display_name_combo.get_model()
        display_name = model.get_value(self.display_name_combo.get_active_iter(), 0)
        contact.set_display_name(display_name)
        contact_emails = dict(map(lambda a: (a.get_address(), a), contact.emails))
        for i, e in enumerate(set(self.email_entry.get_text().split(","))):
            if e in contact_emails.keys():
                contact_emails.pop(e)
                continue
            if e == "":
                continue
            ea = Email(self.book)
            ea.begin_edit()
            if i != 0:
                ea.set_type(EmailForType.OTHER)
            ea.set_address(e)
            ea.set_contact(contact)
            ea.commit_edit()
        for remaining in contact_emails.values():
            remaining.begin_edit()
            remaining.set_dirty()
            remaining.set_destroying(True)
            remaining.commit_edit()

        work_number = self.phone_entry.get_number()
        if work_number is not None:
            works = list(filter(lambda a: a.type == PhoneType.WORK, contact.phones))
            if any(works):
                work_phone = works[0]
                work_phone.set_number(work_number)
            else:
                work_phone = Phone(self.book)
                work_phone.begin_edit()
                work_phone.set_type(PhoneType.WORK)
                work_phone.set_number(work_number)
                work_phone.set_contact(contact)
                work_phone.commit_edit()

        mobile_number = self.mobile_entry.get_number()
        if mobile_number is not None:
            mobiles = list(filter(lambda a: a.type == PhoneType.MOBILE, contact.phones))
            if any(mobiles):
                mobile_phone = list(mobiles)[0]
                mobile_phone.set_number(mobile_number)
            else:
                mobile_phone = Phone(self.book)
                mobile_phone.begin_edit()
                mobile_phone.set_type(PhoneType.MOBILE)
                mobile_phone.set_number(mobile_number)
                mobile_phone.set_contact(contact)
                mobile_phone.commit_edit()

        if any(contact.addresses):
            address = contact.addresses[0]
        else:
            address = Address(Session.get_current_book())
        if address is not None:
            address.begin_edit()
            self.address_frame.copy_to_address(address)
            address.set_contact(contact)
            address.commit_edit()
        contact.commit_edit()
        sup.set_tin(self.tin_entry.get_text())
        sup.set_term(self.terms.get_ref())
        sup.set_payment_method(self.payment_method.get_ref())
        sup.set_billing_rate(self.billing_entry.get_amount())
        sup.set_tax(self.tax_selection.get_ref())
        sup.set_tax_included(self.tax_check.get_active())
        curr = self.currency_edit.get_currency()
        root = self.book.get_root_account()
        sup.set_opening_balance(self.opening_balance.get_amount(), self.opening_date.get_date())
        sup.set_currency(curr)
        start = self.notes_buffer.get_start_iter()
        end = self.notes_buffer.get_end_iter()
        notes = self.notes_buffer.get_text(start, end, False)
        sup.set_notes(notes)
        sup.commit_edit()
        ComponentManager.resume()
        self.supplier = sup

    def finish_ok(self):
        gs_set_busy_cursor(None, Term)
        be = self.book.get_backend()
        if be is not None:
            with be.add_lock():
                self.to_supplier()
        self.created_supplier = self.supplier
        self.supplier = None
        self.close()
        ComponentManager.close(self.component_id)
        gs_unset_busy_cursor(None)

    @Gtk.Template.Callback()
    def validate(self, *_):
        model = self.display_name_combo.get_model()
        _iter = self.display_name_combo.get_active_iter()
        sensitive = True
        display_name = None
        if _iter is not None:
            display_name = model.get_value(_iter, 0)
        gs_widget_remove_style_context(self.full_name_entry, "error")
        self.full_name_entry.set_tooltip_text("")
        self.display_name_combo.set_tooltip_text("")
        gs_widget_remove_style_context(self.display_name_combo, "error")
        if display_name == "" or display_name is None:
            message = "Display name cannot be null."
            gs_widget_set_style_context(self.display_name_combo, "error")
            self.display_name_combo.set_tooltip_text(message)
            sensitive = False
        elif self.type == SupplierDialogType.NEW:
            if Supplier.lookup_display_name(Session.get_current_book(),display_name) is not None:
                message = "The supplier with display name %s already exists" % display_name
                gs_widget_set_style_context(self.display_name_combo, "error")
                self.display_name_combo.set_tooltip_text(message)
                sensitive = False

            full_name = self.full_name_entry.get_text()
            if Supplier.lookup_name(Session.get_current_book(), full_name) is not None:
                message = "The supplier %s already exists" % full_name
                self.full_name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning")
                gs_widget_set_style_context(self.full_name_entry, "error")
                self.full_name_entry.set_tooltip_text(message)
                sensitive = False
            else:
                self.full_name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "emblem-default")
        self.address_frame.street_entry.set_tooltip_text("")

        gs_widget_remove_style_context(self.address_frame.get_label_widget(), "error")
        if not self.address_frame.validate():
            self.address_frame.street_entry.set_tooltip_text("Please fill in the address details")
            gs_widget_set_style_context(self.address_frame.get_label_widget(), "error")
            sensitive = False

        self.terms.set_tooltip_text("")
        gs_widget_remove_style_context(self.terms_frame.get_label_widget(), "error")
        if self.terms.get_ref() is None:
            self.terms.set_tooltip_text("Please select or add a Term Agreement Under Payment and Billing>Payment Terms")
            gs_widget_set_style_context(self.terms_frame.get_label_widget(), "error")
            sensitive = False

        gs_widget_remove_style_context(self.payment_method_frame.get_label_widget(), "error")
        if self.payment_method.get_ref() is None:
            self.payment_method.set_tooltip_text(
                "Please select or add a payment method Under Payment and Billing>Payment Method")
            gs_widget_set_style_context(self.payment_method_frame.get_label_widget(), "error")
            sensitive = False
        if sensitive:
            self.ok_button.set_tooltip_text("Press Okay to save customer")
        else:
            self.ok_button.set_tooltip_text("Please fix the errors to continue")
        self.ok_button.set_sensitive(sensitive)

    def window_response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            dialog.close()

    @Gtk.Template.Callback()
    def display_name_changed(self, *_):
        model: Gtk.ListStore = self.display_name_combo.get_model()
        model.clear()
        human_name = HumanName(full_name=self.full_name_entry.get_text())

        for disp_name in sorted(set(
                map(str.strip,
                    ["{} {} {} {} {}".format(human_name.suffix, human_name.first,
                                             human_name.middle, human_name.last, human_name.suffix).replace("  ", " "),
                     "{} {}".format(human_name.first, human_name.last).replace("  ", " "),
                     "{} {}".format(human_name.last, human_name.first).replace("  ", " "),
                     "{} {} {}".format(human_name.middle, human_name.last, human_name.first).replace("  ", " ")
                     ]))):
            if len(disp_name) == 0:
                continue
            model.append([disp_name])
        self.display_name_combo.set_active(0)

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        inv = self.supplier
        if inv is None:
            ComponentManager.close(self.component_id)
            return
        if changes:
            info = ComponentManager.get_entity_events(changes, inv.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(SupplierDialog)
