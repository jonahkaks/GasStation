# -*- coding: utf-8 -*-
from gasstation.config import *
from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject

SPLASH_PERCENTAGE_UNKNOWN = 101
MARKUP_STRING = "<span size='small'>%s</span>"
PREF_SHOW_SPLASH = "show-splash-screen"


class SplashScreen(Gtk.Window):
    __gtype_name__ = "SplashScreen"

    def __init__(self):
        super().__init__(type=Gtk.WindowType.TOPLEVEL)
        self.set_skip_taskbar_hint(True)
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_type_hint(Gdk.WindowTypeHint.SPLASHSCREEN)
        self.set_decorated(False)
        self.set_modal(False)
        pixmap = Gtk.Image.new_from_resource('/org/jonah/Gasstation/icons/hicolor/scalable/actions/splash.png')
        frame = Gtk.Frame.new()
        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 3)
        vbox.set_homogeneous(False)
        hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 3)
        hbox.set_homogeneous(False)

        ver_string = "%s: %s, %s: %s" % ("Version", VERSION, "Build ID", REVISION)
        version = Gtk.Label.new()
        version.set_markup(MARKUP_STRING % ver_string)
        separator = Gtk.Separator.new(Gtk.Orientation.HORIZONTAL)
        self.progress = Gtk.Label()
        self.progress.set_max_width_chars(34)
        markup = MARKUP_STRING % "Loading..."
        self.progress.set_markup(markup)
        frame.add(pixmap)
        pixmap.show()
        frame.show()
        self.progress_bar = Gtk.ProgressBar.new()
        self.progress_bar.set_show_text(True)
        vbox.pack_start(frame, True, True, 0)
        vbox.pack_start(version, False, False, 0)
        vbox.pack_start(separator, False, False, 0)
        hbox.pack_start(self.progress, True, True, 0)
        hbox.pack_start(self.progress_bar, False, False, 0)
        vbox.pack_start(hbox, True, True, 0)
        self.add(vbox)
        vbox.show()
        self.connect("button-press-event", self.button_press_cb)
        self.show_all()

    def button_press_cb(self, *_):
        self.destroy()
        return True

    def update(self, string, percentage):
        percentage = int(percentage)
        if string:
            markup = MARKUP_STRING % string
            self.progress.set_markup(markup)
        self.progress_bar.set_text(str(percentage) + "%")
        if percentage < 0:
            self.progress_bar.set_fraction(0.0)
        elif percentage <= 100:
            self.progress_bar.set_fraction(percentage / 100)
        else:
            self.progress_bar.pulse()
        while Gtk.events_pending():
            Gtk.main_iteration()


GObject.type_register(SplashScreen)
