from gasstation.plugins.pages import *
from gasstation.utilities.custom_dialogs import show_error

DIALOG_ASSOC_CM_CLASS = "dialog-trans-assoc"
PREFS_GROUP = "dialogs.trans-assoc"


class AssocColumn:
    DATE_TRANS = 0
    DESC_TRANS = 1
    URI_U = 2
    AVAILABLE = 3
    URI_SPLIT = 4
    URI = 5
    URI_RELATIVE = 6


class AssocDialog:
    def __init__(self):
        self.window = None
        self.view = None
        self.path_head = None
        self.path_head_set = False

    def window_destroy_cb(self, *args):
        ComponentManager.unregister_by_data(DIALOG_ASSOC_CM_CLASS, self)
        if self.window is not None:
            self.window.destroy()
            self.window = None

    def window_key_press_cb(self, widget, event):
        if event.keyval == Gdk.KEY_Escape:
            self.close_handler()
            return True
        else:
            return False

    @staticmethod
    def sort_iter_compare_func(model, a, b):
        uri1 = model.get(a, AssocColumn.URI_U)
        uri2 = model.get(b, AssocColumn.URI_U)
        ret = GLib.utf8_collate(uri1, uri2)
        return ret

    def sort(self):
        model = self.view.get_model()
        sorts = model.get_sort_column_id()

        if sorts is not None:
            _id, order = sorts
            if order == Gtk.SortType.ASCENDING:
                order = Gtk.SortType.DESCENDING
            else:
                order = Gtk.SortType.ASCENDING
        else:
            model.set_sort_func(AssocColumn.URI, self.sort_iter_compare_func)
            order = Gtk.SortType.ASCENDING
        model.set_sort_column_id(AssocColumn.URI, order)

    def convert_uri_to_filename(self, uri, scheme):
        file_path = ""

        if scheme is None:
            # if self.path_head_set:
            #     file_path = gnc_file_path_absolute(uri_get_path (self.path_head), uri)
            # else:
            #     file_path = gnc_file_path_absolute(None, uri)
            pass
        if uri_is_file_scheme(scheme):
            file_path = "/" + uri_get_path(uri)
        return file_path

    def convert_uri_to_unescaped(self, uri, scheme):
        file_path = self.convert_uri_to_filename(uri, scheme)
        if file_path:
            uri_u = GLib.uri_unescape_string(file_path)
            if os.name == "windows":
                uri_u = GLib.strdelimit(uri_u, "/", '\\')
        else:
            uri_u = GLib.uri_unescape_string(uri)
        return uri_u

    def update(self):
        model = self.view.get_model()
        self.view.set_model(None)
        _iter = model.get_iter_first()
        while _iter is not None:
            uri = model.get_value(_iter, AssocColumn.URI)
            scheme = uri_get_scheme(uri)
            filename = self.convert_uri_to_unescaped(uri, scheme)
            if scheme is not None and len(scheme) == 0 or uri_is_file_scheme(scheme):
                if GLib.file_test(filename, GLib.FileTest.EXISTS):
                    model.set_value(_iter, AssocColumn.AVAILABLE, "File Found")
                else:
                    model.set_value(_iter, AssocColumn.AVAILABLE, "File Not Found")
            else:
                escaped = GLib.uri_escape_string(uri, ":/.", True)
                nm = Gio.network_monitor_get_default()
                conn = Gio.NetworkAddress.parse_uri(escaped, 80)
                if conn:
                    if nm.can_reach(conn, None):
                        model.set_value(_iter, AssocColumn.AVAILABLE, "Address Found")
                    else:
                        model.set_value(_iter, AssocColumn.AVAILABLE, "Address Not Found")
            _iter = model.iter_next(_iter)

        self.view.set_model(model)

    def sort_button_cb(self, widget):
        self.sort()

    def check_button_cb(self, widget):
        self.update()

    def close_button_cb(self, widget):
        ComponentManager.close_by_data(DIALOG_ASSOC_CM_CLASS, self)

    def row_selected_cb(self, view, path, col):
        model = self.view.get_model()
        _iter = model.get_iter(path)
        if _iter is None:
            return
        uri = model.get_value(_iter, AssocColumn.URI)
        split = model.get_value(_iter, AssocColumn.URI_SPLIT)
        if self.view.get_column(AssocColumn.URI_U) == col:
            uri_scheme = uri_get_scheme(uri)
            uri_out = None
            file_path = None
            if uri_scheme is None or len(uri_scheme) == 0:
                # if self.path_head_set:
                #     file_path = gnc_file_path_absolute (uri_get_path (self.path_head), uri)
                # else
                #     file_path = gnc_file_path_absolute (None, uri)
                #
                uri_out = uri_create_uri("file", None, 0, None, None, file_path)

            if uri_out is None:
                uri_out = uri
            uri_out_scheme = uri_get_scheme(uri_out)
            win = self.window

            if uri_out_scheme is not None and len(uri_out_scheme):
                Gtk.show_uri_on_window(win, uri_out, Gdk.CURRENT_TIME)
            else:
                show_error(win, "This transaction is not associated with a valid URI.")

        if self.view.get_column(AssocColumn.DESC_TRANS) == col:
            if split is None:
                return
            account = split.get_account()
            if account is None:
                return

            page = RegisterPluginPage.new(account, False)
            MainWindow.get_main_window(None).open_page(page)
            gsr = page.get_gsr()
            if gsr is None:
                return
            gsr.jump_to_split(split)

    @staticmethod
    def convert_associate_uri(trans):
        uri = trans.get_association_url()
        part = ""
        if uri is None or len(uri) == 0:
            return ""
        if uri.startswith("file:") and not uri.startswith("file://"):
            if uri.startswith("file:/") and not uri.startswith("file://"):
                part = uri + len("file:/")
            elif uri.startswith("file:") and not uri.startswith("file://"):
                part = uri + len("file:")

            if part:
                trans.set_association_url(part)
                return part
        return uri

    def get_trans_info(self):
        book = Session.get_current_book()
        model = self.view.get_model()
        self.view.set_model(None)
        for trans in filter(lambda a: a.association_url != "" and a.association_url is not None,
                            book.get_collection(ID_TRANS).values()):
            uri = Uri(self.convert_associate_uri(trans))
            if uri is not None and len(uri):
                rel = False
                scheme = uri_get_scheme(uri)
                t = trans.get_post_date()
                if t == 0:
                    t = datetime.datetime.now()
                datebuff = print_datetime(t)
                if scheme is None:
                    rel = True
                uri_u = Uri(self.convert_uri_to_unescaped(uri, scheme))
                model.append([datebuff, trans.get_description(), uri_u, "Unknown", trans.get_split(0), uri,
                              "emblem-default" if rel else ""])

        self.view.set_model(model)

    def create(self, parent):
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/trans-assoc.ui",
                                          ["transaction_association_window"])
        window = builder.get_object("transaction_association_window")
        self.window = window
        window.set_transient_for(parent)
        button = builder.get_object("sort_button")
        button.connect("clicked", self.sort_button_cb)
        button = builder.get_object("check_button")
        button.connect("clicked", self.check_button_cb)
        button = builder.get_object("close_button")
        button.connect("clicked", self.close_button_cb)
        self.window.set_title("Transaction Associations")
        gs_widget_set_style_context(window, "TransAssocDialog")
        self.view = builder.get_object("treeview")
        list_store = Gtk.ListStore(str, str, str, str, object, str, str)
        self.view.set_model(list_store)
        path_head = builder.get_object("path-head")
        self.path_head = gs_pref_get_string(PREFS_GROUP_GENERAL, "assoc-head")
        if self.path_head and self.path_head != "":
            path_head_ue_str = GLib.uri_unescape_string(self.path_head, None)
            path_head_str = uri_get_path(path_head_ue_str)
            path_head_s = ""
            if os.name == "windows":
                path_head_str = GLib.strdelimit(path_head_str, "/", '\\')
            if GLib.file_test(path_head_str, GLib.FileTest.IS_DIR):
                path_head_label = "Path head for files is, " + path_head_s
            else:
                path_head_label = "Path head does not exist, " + path_head_str
            self.path_head_set = True
            path_head.set_text(path_head_label)
        else:
            doc = GLib.get_user_special_dir(GLib.USER_DIRECTORY_DOCUMENTS)
            if doc:
                path_ret = doc
            else:
                # path_ret = g_strdup (gnc_userdata_dir ())
                path_ret = ""

            path_head_label = "Path head not set, using '%s' for relative paths" % path_ret
            self.path_head_set = False
            path_head.set_text(path_head_label)
        gs_widget_set_style_context(path_head, "gnc-class-highlight")
        tree_column = Gtk.TreeViewColumn()
        tree_column.set_title("Relative")
        self.view.append_column(tree_column)
        tree_column.set_alignment(0.5)
        cr = Gtk.CellRendererPixbuf()
        tree_column.pack_start(cr, True)
        tree_column.set_attributes(cr, icon_name=AssocColumn.URI_RELATIVE)
        cr.set_alignment(0.5, 0.5)
        self.view.connect("row-activated", self.row_selected_cb)
        tree_column = builder.get_object("uri-entry")
        tree_column.set_expand(True)
        self.view.set_grid_lines(gs_tree_view_get_grid_lines_pref())
        selection = self.view.get_selection()
        selection.set_mode(Gtk.SelectionMode.SINGLE)
        self.window.connect("destroy", self.window_destroy_cb)
        self.window.connect("key_press_event", self.window_key_press_cb)
        builder.connect_signals(self)
        gs_restore_window_size(PREFS_GROUP, self.window, parent)
        window.show_all()
        self.get_trans_info()
        self.update()

    def close_handler(self, *args):
        gs_save_window_size(PREFS_GROUP, self.window)
        self.window.destroy()

    def refresh_handler(self, changes):
        pass

    def show_handler(self, klass, component_id, iter_data):
        self.window.present()
        return True

    @classmethod
    def new(cls, parent):
        if ComponentManager.forall(DIALOG_ASSOC_CM_CLASS, cls.show_handler, None):
            return
        self = cls()
        self.create(parent)
        ComponentManager.register(DIALOG_ASSOC_CM_CLASS, self.refresh_handler, self.close_handler, self)
