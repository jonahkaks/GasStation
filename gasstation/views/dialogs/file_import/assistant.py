import pandas as pd

from gasstation.views.main_window import *
from libgasstation import ImportStrategy


class Mapping(GObject.GObject):
    __gtype_name__ = "Mapping"
    strategy_name = GObject.Property(type=GObject.TYPE_STRING, flags=GObject.ParamFlags.READWRITE)
    mapping_store = GObject.Property(type=Gtk.TreeModel, flags=GObject.ParamFlags.READWRITE)

    def __init__(self, name, store):
        GObject.GObject.__init__(self)
        self.strategy_name = name
        self.mapping_store = store


GObject.type_register(Mapping)


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/assistant-file-import.ui")
class ImportDialog(Gtk.Assistant):
    __gtype_name__ = "ImportDialog"
    file_chooser: Gtk.FileChooserWidget = Gtk.Template.Child()
    sheet_combo: Gtk.ComboBoxText = Gtk.Template.Child()
    column_mappings_listbox: Gtk.ListBox = Gtk.Template.Child()
    preview_treeview: Gtk.TreeView = Gtk.Template.Child()
    summary_label: Gtk.Label = Gtk.Template.Child()
    import_type_combo: Gtk.ComboBox = Gtk.Template.Child()

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if parent is not None:
            self.set_transient_for(parent)
        self.strategy = None
        self.work_book: pd.ExcelFile = None
        self.work_book_sheet: pd.DataFrame = None
        self.connect("destroy", self.destroy_cb)
        header_box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 0)
        header_box.pack_start(Gtk.Label("GASSTATION"), True, True, 0)
        header_box.pack_start(Gtk.Label("MY COLUMNS"), True, True, 0)
        self.column_mappings_listbox.set_header_func(lambda a, b: header_box)
        header_box.show()
        self.show()

    def delete_dialog(self):
        gs_save_window_size(PREFS_GROUP, self)
        self.destroy()
        del self

    def destroy_cb(self, _):
        self.final_account_tree = None
        self.balance_hash = None
        del self
        return True

    @Gtk.Template.Callback()
    def file_set_cb(self, _):
        self.sheet_combo.remove_all()
        file_url = self.file_chooser.get_filename()

        try:
            self.work_book = pd.ExcelFile(file_url)
            sheet_names = self.work_book.sheet_names
            for sheet_name in sheet_names:
                self.sheet_combo.append_text(sheet_name)
        except IOError as e:
            show_error(self.get_parent(), "Unsupported file type")
            return

    def create_widget_func(self, item: Mapping):
        box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        label = Gtk.Label(item.strategy_name)
        label.set_halign(Gtk.Align.START)
        box.pack_start(label, True, True, 0)
        sheet_name_combo = Gtk.ComboBox()
        renderer_text = Gtk.CellRendererText()
        sheet_name_combo.pack_start(renderer_text, True)
        sheet_name_combo.add_attribute(renderer_text, "text", 0)
        sheet_name_combo.set_model(item.mapping_store)
        sheet_name_combo.show()
        sheet_name_combo.set_active(0)
        box.pack_start(sheet_name_combo, False, False, 0)
        box.show()
        label.show()
        lrow = Gtk.ListBoxRow()
        lrow.add(box)
        return lrow

    @Gtk.Template.Callback()
    def sheet_combo_changed_cb(self, widget):
        currentpage = self.get_nth_page(self.get_current_page())
        self.set_page_complete(currentpage, widget.get_active_text() != "")

    @Gtk.Template.Callback()
    def import_type_selected_cb(self, type_combo):
        tree_iter = type_combo.get_active_iter()
        model = type_combo.get_model()
        sheet_column_names = self.work_book_sheet.keys()
        sheet_column_store = Gtk.ListStore(str)
        for column in sheet_column_names:
            sheet_column_store.append([column])
        store: Gio.ListStore = Gio.ListStore.new(Mapping)

        if tree_iter is not None and any(sheet_column_names):
            strategy: ImportStrategy = model[tree_iter][1]
            for strategy_column_name in strategy.get_column_names():
                store.append(Mapping(strategy_column_name, sheet_column_store))
        self.column_mappings_listbox.bind_model(store, self.create_widget_func)

    @Gtk.Template.Callback()
    def on_prepare(self, *args):
        current_page = self.get_current_page()
        if current_page == 1:
            self.on_sheet_page_prepare()
        if current_page == 2:
            self.on_mapping_page_prepare()
        elif current_page == 3:
            self.on_preview_page_prepare()
        elif current_page == 4:
            self.on_summary_prepare()

    def on_sheet_page_prepare(self):
        self.sheet_combo.set_entry_text_column(0)

    def on_mapping_page_prepare(self):
        self.work_book_sheet = self.work_book.parse(sheet_name=self.sheet_combo.get_active_text())
        strategies_model = Gtk.ListStore(str, object)
        strategies: List[ImportStrategy] = ImportStrategy.get_all()

        for strategy in strategies:
            strategies_model.append([strategy.__type_name__, strategy])
        renderer_text = Gtk.CellRendererText()
        self.import_type_combo.pack_start(renderer_text, True)
        self.import_type_combo.add_attribute(renderer_text, "text", 0)
        self.import_type_combo.set_model(strategies_model)
        self.import_type_combo.set_active(0)

    def on_preview_page_prepare(self):
        tree_view = self.preview_treeview
        # ADD DATAFRAME MODEL HERE
        self.categories_page_enable_next()

    def on_summary_page_prepare(self):
        label = self.summary_label

    @Gtk.Template.Callback()
    def on_cancel(self, _):
        ComponentManager.suspend()
        if self.new_book:
            self.optionwin.destroy()
        self.delete_dialog()
        self.delete_our_account_tree()
        ComponentManager.resume()

    @Gtk.Template.Callback()
    def on_finish(self, _):
        com = self.currency_selector.get_currency()
        self.account_categories_tree_view.destroy()
        if self.our_account_tree is not None:
            self.our_account_tree.foreach_descendant(self.starting_balance_helper)
        if self.initial_category:
            self.initial_category = None
        self.delete_dialog()
        ComponentManager.suspend()
        if self.new_book:
            self.optionwin.destroy()
        if self.final_account_tree is not None:
            self.final_account_tree.destroy()
            self.final_account_tree = None
        self.delete_our_account_tree()
        root = Session.get_current_root()
        root.set_commodity(com)
        ComponentManager.resume()
        if self.when_completed is not None:
            self.when_completed()

    def __repr__(self):
        return '<ImportDialog>'


GObject.type_register(ImportDialog)
