import pandas as pd
from gi.repository import Gtk, GObject, GLib

from gasstation.utilities.custom_dialogs import show_error, ask_ok_cancel
from gasstation.utilities.ui import gs_set_default_directory
from gasstation.views import PREFS_GROUP
from libgasstation.core.component import ComponentManager
from libgasstation.core.uri import Uri


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/import/transaction.ui")
class TransactionImportAssistant(Gtk.Assistant):
    __gtype_name__ = "TransactionImportAssistant"
    file_page = Gtk.Template.Child()
    preview_page:Gtk.Box=Gtk.Template.Child()
    combo_hbox:Gtk.Box = Gtk.Template.Child()

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if parent is not None:
            self.set_transient_for(parent)
        settings_store = Gtk.ListStore(object, str)
        settings_combo = Gtk.ComboBox.new_with_model_and_entry(settings_store)
        settings_combo.set_entry_text_column(0)
        settings_combo.set_active(0)
    #
    #     combo_hbox = GTK_WIDGET(gtk_builder_get_object(builder, "combo_hbox"))
    #     gtk_box_pack_start(GTK_BOX(combo_hbox), GTK_WIDGET(settings_combo), True, True, 6)
    #     settings_combo.show()    #
    #     g_signal_connect(G_OBJECT(settings_combo), "changed",
    #                       G_CALLBACK(csv_tximp_preview_settings_sel_changed_cb))
    #
    #
    #     emb_entry = gtk_bin_get_child(GTK_BIN(settings_combo))
    #     g_signal_connect(G_OBJECT(emb_entry), "changed",
    #                       G_CALLBACK(csv_tximp_preview_settings_text_changed_cb))
    #     g_signal_connect(G_OBJECT(emb_entry), "insert-text",
    #                       G_CALLBACK(csv_tximp_preview_settings_text_inserted_cb))
    #
    #
    #     save_button = GTK_WIDGET(gtk_builder_get_object(builder, "save_settings"))
    #
    #
    #     del_button = GTK_WIDGET(gtk_builder_get_object(builder, "delete_settings"))
    #
    #     start_row_spin = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "start_row"))
    #     end_row_spin = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "end_row"))
    #     skip_alt_rows_button = GTK_WIDGET(gtk_builder_get_object(builder, "skip_rows"))
    #     skip_errors_button = GTK_WIDGET(gtk_builder_get_object(builder, "skip_errors_button"))
    #     multi_split_cbutton = GTK_WIDGET(gtk_builder_get_object(builder, "multi_split_button"))
    #     separator_table = GTK_WIDGET(gtk_builder_get_object(builder, "separator_table"))
    #     fw_instructions_hbox = GTK_WIDGET(gtk_builder_get_object(builder, "fw_instructions_hbox"))
    #
    #
    #                                                                                  const char* sep_button_names[] = {
    #         "space_cbutton",
    #         "tab_cbutton",
    #         "comma_cbutton",
    #         "colon_cbutton",
    #         "semicolon_cbutton",
    #         "hyphen_cbutton"
    #     }
    #     for(int i = 0 i < SEP_NUM_OF_TYPES i++)
    #     sep_button[i]
    #     =(GtkCheckButton*)GTK_WIDGET(gtk_builder_get_object(builder, sep_button_names[i]))
    #
    #
    #     custom_cbutton
    #     =(GtkCheckButton*)GTK_WIDGET(gtk_builder_get_object(builder, "custom_cbutton"))
    #
    #
    #     custom_entry =(GtkEntry*)GTK_WIDGET(gtk_builder_get_object(builder, "custom_entry"))
    #
    #
    #     acct_selector = gs_account_sel_new()
    #     account_hbox = GTK_WIDGET(gtk_builder_get_object(builder, "account_hbox"))
    #     gtk_box_pack_start(GTK_BOX(account_hbox), acct_selector, TRUE, TRUE, 6)
    #     gs_account_sel_set_hexpand(GS_ACCOUNT_SEL(acct_selector), True)
    #     gtk_widget_show(acct_selector)
    #
    #     g_signal_connect(G_OBJECT(acct_selector), "account_sel_changed",
    #     G_CALLBACK(csv_tximp_preview_acct_sel_cb))
    #
    #
    #
    #     encselector = GO_CHARMAP_SEL(go_charmap_sel_new(GO_CHARMAP_SEL_TO_UTF8))
    #
    #     g_signal_connect(G_OBJECT(encselector), "charmap_changed",
    #     G_CALLBACK(csv_tximp_preview_enc_sel_cb))
    #
    #     encoding_container = GTK_CONTAINER(gtk_builder_get_object(builder, "encoding_container"))
    #     gtk_container_add(encoding_container, GTK_WIDGET(encselector))
    #     gtk_widget_set_hexpand(GTK_WIDGET(encselector), True)
    #     gtk_widget_show_all(GTK_WIDGET(encoding_container))
    #
    #
    #     instructions_label = GTK_LABEL(gtk_builder_get_object(builder, "instructions_label"))
    #     instructions_image = GTK_IMAGE(gtk_builder_get_object(builder, "instructions_image"))
    #
    #
    #     date_format_combo = GTK_COMBO_BOX_TEXT(gtk_combo_box_text_new())
    #     for(auto& date_fmt : GncDate::c_formats)
    #     gtk_combo_box_text_append_text(date_format_combo, _(date_fmt.m_fmt))
    #     gtk_combo_box_set_active(GTK_COMBO_BOX(date_format_combo), 0)
    #     g_signal_connect(G_OBJECT(date_format_combo), "changed",
    #                       G_CALLBACK(csv_tximp_preview_date_fmt_sel_cb))
    #
    #
    #                      date_format_container = GTK_CONTAINER(gtk_builder_get_object(builder, "date_format_container"))
    #     gtk_container_add(date_format_container, GTK_WIDGET(date_format_combo))
    #     gtk_widget_set_hexpand(GTK_WIDGET(date_format_combo), True)
    #     gtk_widget_show_all(GTK_WIDGET(date_format_container))
    #
    #
    #                                                                        currency_format_combo = GTK_COMBO_BOX_TEXT(gtk_combo_box_text_new())
    #     for(int i = 0 i < num_currency_formats i++)
    #     {
    #         gtk_combo_box_text_append_text(currency_format_combo, _(currency_format_user[i]))
    #     }
    #
    #     gtk_combo_box_set_active(GTK_COMBO_BOX(currency_format_combo), 0)
    #     g_signal_connect(G_OBJECT(currency_format_combo), "changed",
    #     G_CALLBACK(csv_tximp_preview_currency_fmt_sel_cb))
    #
    #
    #     currency_format_container = GTK_CONTAINER(gtk_builder_get_object(builder, "currency_format_container"))
    #     gtk_container_add(currency_format_container, GTK_WIDGET(currency_format_combo))
    #     gtk_widget_set_hexpand(GTK_WIDGET(currency_format_combo), True)
    #     gtk_widget_show_all(GTK_WIDGET(currency_format_container))
    #
    #
    #     csv_button = GTK_WIDGET(gtk_builder_get_object(builder, "csv_button"))
    #     fixed_button = GTK_WIDGET(gtk_builder_get_object(builder, "fixed_button"))
    #
    #
    #     treeview =(GtkTreeView*)GTK_WIDGET(gtk_builder_get_object(builder, "treeview"))
    #     gtk_tree_view_set_headers_clickable(treeview, True)
    #
    #
    #     encoding_selected_called = False
    #     }
    #
    #
    #     account_match_page  = GTK_WIDGET(gtk_builder_get_object(builder, "account_match_page"))
    #     account_match_view  = GTK_WIDGET(gtk_builder_get_object(builder, "account_match_view"))
    #     account_match_label = GTK_WIDGET(gtk_builder_get_object(builder, "account_match_label"))
    #     account_match_btn = GTK_WIDGET(gtk_builder_get_object(builder, "account_match_change"))
    #
    #
    #     doc_page = GTK_WIDGET(gtk_builder_get_object(builder, "doc_page"))
    #
    #
    #     match_page  = GTK_WIDGET(gtk_builder_get_object(builder, "match_page"))
    #     match_label = GTK_WIDGET(gtk_builder_get_object(builder, "match_label"))
    #
    #
    # gs_csv_importer_gui = gs_gen_trans_assist_new(GTK_WIDGET(csv_imp_asst),
    #                                                  match_page, None, False, 42)
    #
    #
    # summary_page  = GTK_WIDGET(gtk_builder_get_object(builder, "summary_page"))
    # summary_label = GTK_WIDGET(gtk_builder_get_object(builder, "summary_label"))
    #
    # gs_restore_window_size(GS_PREFS_GROUP,
    #                          self, gs_ui_get_main_window(None))
    #
    # gtk_builder_connect_signals(builder)
    # g_object_unref(G_OBJECT(builder))
    #
    # gtk_widget_show_all(GTK_WIDGET(csv_imp_asst))
    # gs_window_adjust_for_screen(self)
    #
    #
    # new_book = gs_is_new_book()
    # }
        self.strategy = None
        self.work_book: pd.ExcelFile = None
        self.work_book_sheet: pd.DataFrame = None

    @Gtk.Template.Callback()
    def file_set_cb(self, _):
        self.sheet_combo.remove_all()
        file_url = self.file_chooser.get_filename()
        try:
            self.work_book = pd.ExcelFile(file_url)
        except IOError as e:
            show_error(self.get_parent(), "Unsupported file type")
            return
        sheet_names = self.work_book.sheet_names
        for sheet_name in sheet_names:
            self.sheet_combo.append_text(sheet_name)

    @Gtk.Template.Callback()
    def sheet_combo_changed_cb(self, widget):
        currentpage = self.get_nth_page(self.get_current_page())
        self.set_page_complete(currentpage, widget.get_active_text() != "")

    def prepare_cb(self, assistant, page):
        self.prepare_cb(page)

    def close_cb(self, assistant):
        ComponentManager.close_by_data(ASSISTANT_CSV_IMPORT_TRANS_CM_CLASS, self)

    def finish_cb(self, assistant):
        self.finish()


    def file_activated_cb(self, chooser):
        self.file_activated_cb()


    def file_selection_changed_cb(self, chooser):
        self.file_selection_changed_cb()


    def preview_del_settings_cb(self, button):
        self.preview_settings_delete()


    def preview_save_settings_cb(self, button):
        self.preview_settings_save()

    def preview_settings_sel_changed_cb(self, combo):
        self.preview_settings_load()

    def preview_settings_text_inserted_cb(self, entry, new_text,new_text_length, position):
        info = None
        if new_text is None:
            return

        base_txt = new_text
        mod_txt = base_txt
        mod_txt = mod_txt.replace('[', '(')
        mod_txt = mod_txt.replace(']', ')')
        if base_txt == mod_txt:
            return
        entry.handler_block_by_function(self.preview_settings_text_inserted_cb, info)
        entry.insert_text(mod_txt, mod_txt.size() , position)
        entry.handler_unblock_by_function(self.preview_settings_text_inserted_cb, info)
        entry.stop_emission_by_name("insert_text")

    def preview_settings_text_changed_cb(self, entry):
        self.preview_settings_name(entry)

    def preview_srow_cb(self, spin):
        self.preview_update_skipped_rows()


    def preview_erow_cb(self, spin):
        self.preview_update_skipped_rows()


    def preview_skiprows_cb(self, checkbox):
        self.preview_update_skipped_rows()


    def preview_skiperrors_cb(self, checkbox):
        self.preview_update_skipped_rows()


    def preview_multisplit_cb(self, checkbox):
        self.preview_multi_split(checkbox.get_active())

    def preview_sep_button_cb(self, widget):
        self.preview_update_separators(widget)

    def preview_sep_fixed_sel_cb(self, csv_button):
        self.preview_update_file_format()

    def preview_acct_sel_cb(self, widget):
        self.preview_update_account()

    def preview_enc_sel_cb(self, selector,  encoding):
        self.preview_update_encoding(encoding)

    def preview_date_fmt_sel_cb(self, format_selector):
        self.preview_update_date_format()

    def preview_currency_fmt_sel_cb(format_selector,self):
        self.preview_update_currency_format()

    def preview_col_type_changed_cb(cbox,self):
        self.preview_update_col_type(cbox)

    def preview_treeview_clicked_cb(self, treeview, event):
        self.preview_update_fw_columns(treeview, event)
        return False

    def acct_match_button_clicked_cb(self, widget):
        self.acct_match_via_button()

    def acct_match_view_clicked_cb(self, widget, event):
        return self.acct_match_via_view_dblclick(event)

    # def __del__(self):
    #     gs_gen_trans_list_delete(gs_csv_importer_gui)
    #     gs_csv_importer_gui = None
    #     gtk_widget_destroy(GTK_WIDGET(csv_imp_asst))

    def check_for_valid_filename(self):
        file_name = self.file_chooser.get_filename()
        if file_name is None or GLib.file_test(file_name, GLib.FileTest.IS_DIR):
            return False
        filepath = Uri(file_name).get_path()
        starting_dir = GLib.path_get_dirname(filepath)
        self.m_file_name = file_name
        gs_set_default_directory(PREFS_GROUP, starting_dir)
        return True

    def file_activated_cb(self):
        self.set_page_complete(self.file_page, False)
        if self.check_for_valid_filename():
            self.set_page_complete(self.file_page, True)
            self.next_page(self.csv_imp_asst)

    def file_selection_changed_cb(self):
        self.set_page_complete(self.file_page,self.check_for_valid_filename())

    def preview_populate_settings_combo(self):
        model = self.settings_combo.get_model()
        model.clear()
        presets = self.get_import_presets_trans()
        for preset in self.presets:
            model.append([preset.get(), preset.m_name])

    def preview_handle_save_del_sensitivity(self, combo):
        can_delete = False
        can_save = False
        entry = combo.get_child()
        entry_text = entry.get_text()
        _iter = combo.get_active_iter()
        if _iter is not None:
            model = combo.get_model()
            preset = model[_iter][0]

            if preset and not self.preset_is_reserved_name(preset.m_name):
                can_delete = True
                can_save = True
        elif entry_text and len(entry_text) > 0 and not preset_is_reserved_name(entry_text):
            can_save = True
            self.save_button.set_sensitive(can_save)
            self.del_button.set_sensitive(can_delete)

    def preview_settings_name(self, entry):
        text = entry.get_text()
        if text:
            self.settings_name(text)
        box = entry.get_parent()
        combo = box.get_parent()
        self.preview_handle_save_del_sensitivity(combo)

    def preview_settings_load(self):
        _iter = self.settings_combo.get_active_iter()
        if _iter is None:
            return
        model = self.settings_combo.get_model()
        preset = model[_iter][0]
        if not preset:
            return
        self.settings(preset)
        if preset.m_load_error:
            show_error(self,"There were problems reading some saved settings, continuing to load.\n"
                                      "Please review and save again.")
        self.preview_refresh()
        self.preview_handle_save_del_sensitivity(self.settings_combo)

    def preview_settings_delete(self):
        _iter = self.settings_combo.get_active_iter()
        if _iter is None:
            return
        model = self.settings_combo.get_model()
        preset = model[_iter][0]
        response = ask_ok_cancel(self,Gtk.ResponseType.CANCEL,"Delete the Import Settings.")
        if response == Gtk.ResponseType.OK:
            preset.remove()
            self.preview_populate_settings_combo()
            self.settings_combo.set_active(0)
            self.preview_refresh()

    def preview_settings_save(self):
        new_name = self.settings_name()
        _iter = self.settings_combo.get_active_iter()
        if _iter is None:
            model = self.setting_combo.get_model()
            _iter = model.get_iter_first()
            while _iter is not None:

                {
                    CsvTransImpSettings .preset
                gtk_tree_model_get(model, &iter, SET_GROUP, &preset, -1)

                if(preset and(preset.m_name == new_name)))
                {
                response = ask_ok_cancel(self,
                Gtk.ResponseType.OK,
                "%s", _("Setting name already exists, overwrite?"))
                if(response != Gtk.ResponseType.OK)
                return

                break
            }
            valid = gtk_tree_model_iter_next(model, &iter)
            }
        }

        if(not self.save_settings())
        {
        gs_info_dialog(self,
        "%s", _("The settings have been saved."))

        preview_populate_settings_combo()
        model = gtk_combo_box_get_model(settings_combo)

        GtkTreeIter   iter
        valid = gtk_tree_model_get_iter_first(model, &iter)
        while(valid)
            {
                gchar .name = None
        gtk_tree_model_get(model, &iter, SET_NAME, &name, -1)

        if(g_strcmp0(name, new_name) == 0)
        gtk_combo_box_set_active_iter(settings_combo, &iter)



        valid = gtk_tree_model_iter_next(model, &iter)
        }
        }
        else
        show_error(self,
        "%s", _("There was a problem saving the settings, please try again."))


    def preview_update_skipped_rows(self):
        self.update_skipped_lines(gtk_spin_button_get_value_as_int(start_row_spin),
        gtk_spin_button_get_value_as_int(end_row_spin),
        gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(skip_alt_rows_button)),
        gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(skip_errors_button)))

        adj = gtk_spin_button_get_adjustment(end_row_spin)
        adj.set_upper(self.m_parsed_lines.size()
        - self.skip_start_lines() -1)

        adj = gtk_spin_button_get_adjustment(start_row_spin)
        adj.set_upper(self.m_parsed_lines.size()
        - self.skip_end_lines() - 1)
        self.preview_refresh_table()


    def preview_multi_split(multi):
        self.multi_split(multi)
        self.preview_refresh()



    def preview_update_separators(GtkWidget. widget):
        if(self.file_format() != GncImpFileFormat::CSV)
        return

        checked_separators = )
        const stock_sep_chars = " \t,:-")
        for(int i = 0 i < SEP_NUM_OF_TYPES i++)
            {
            if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sep_button[i])))
        checked_separators += stock_sep_chars[i]
        }

        if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(custom_cbutton)))
            {
                custom_sep = gtk_entry_get_text(custom_entry)
        if(custom_sep[0] != '\0') /. Don't add a blank separator(bad things will happennot ). ./
        checked_separators += custom_sep
        }

        self.separators(checked_separators)

        try
            {
                self.tokenize(False)
        self.preview_refresh_table()
        }
        except(std::range_error &e)
        {
            show_error(self, "Error in parsing")
        if(not widget)
        return
        if(widget == GTK_WIDGET(custom_entry))
            gtk_entry_set_text(GTK_ENTRY(widget), "")
        else
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget),
            not gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)))
            return
        }
        }


    def preview_update_file_format(self):
        try
            {
            if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(csv_button)))
        {
            self.file_format(GncImpFileFormat::CSV)
        g_signal_handlers_disconnect_by_func(G_OBJECT(treeview),
                                            (gpointer)csv_tximp_preview_treeview_clicked_cb,(gpointer)this)
        gtk_widget_set_visible(separator_table, True)
        gtk_widget_set_visible(fw_instructions_hbox, False)
        }
        else
        {
            self.file_format(GncImpFileFormat::FIXED_WIDTH)
        g_signal_connect(G_OBJECT(treeview), "button-press-event",
                          G_CALLBACK(csv_tximp_preview_treeview_clicked_cb),(gpointer)this)
        gtk_widget_set_visible(separator_table, False)
        gtk_widget_set_visible(fw_instructions_hbox, True)

        }

        self.tokenize(False)
        self.preview_refresh_table()
        }
        except(std::range_error &e)
        {
            show_error(self, "%s", e.what())
        return
        }
        except(..)
        {
        PWARN("Got an error during file loading")
        }
        }


    def preview_update_account(self):
        acct = self.account_selector.get_account()
        self.base_account(acct)
        self.preview_refresh_table()

    def preview_update_encoding(self, encoding):
        if self.encoding_selected_called:
            previous_encoding = self.m_tokenizer.encoding()
            try:
                self.encoding(encoding)
                self.preview_refresh_table()
            except ValueError:
                show_error(self, "Invalid encoding selected")
                go_charmap_sel_set_encoding(encselector, previous_encoding)
        encoding_selected_called = not self.encoding_selected_called

    def preview_update_date_format(self):
        self.date_format(self.date_format_combo.get_active())
        self.preview_refresh_table()

    def preview_update_currency_format(self):
        self.currency_format(self.currency_format_combo.get_active())
        self.preview_refresh_table()

    def preview_queue_rebuild_table(self):
        self.preview_refresh_table()
        return False


        # enum PreviewHeaderComboCols { COL_TYPE_NAME, COL_TYPE_ID }
        # enum PreviewDataTableCols {
        # PREV_COL_FCOLOR,
        # PREV_COL_BCOLOR,
        # PREV_COL_STRIKE,
        # PREV_COL_ERROR,
        # PREV_COL_ERR_ICON,
        # PREV_N_FIXED_COLS }

    def  preview_update_col_type(cbox)
        {

        model = gtk_combo_box_get_model(cbox)
        gtk_combo_box_get_active_iter(cbox, &iter)
        new_col_type = GncTransPropType::NONE
        gtk_tree_model_get(model, &iter, COL_TYPE_ID, &new_col_type, -1)

        col_num = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(cbox), "col-num"))
        self.set_column_type(col_num, new_col_type)

        g_idle_add((GSourceFunc)csv_imp_preview_queue_rebuild_table)

#
# enum
# {
# CONTEXT_STF_IMPORT_MERGE_LEFT = 1,
# CONTEXT_STF_IMPORT_MERGE_RIGHT = 2,
# CONTEXT_STF_IMPORT_SPLIT = 3,
# CONTEXT_STF_IMPORT_WIDEN = 4,
# CONTEXT_STF_IMPORT_NARROW = 5
# }
#
#     popup_elements[] =
#         {
#         {
#             N_("Merge with column on _left"), "list-remove",
#             0, 1 << CONTEXT_STF_IMPORT_MERGE_LEFT, CONTEXT_STF_IMPORT_MERGE_LEFT
#         },
#         {
#             N_("Merge with column on _right"), "list-remove",
#             0, 1 << CONTEXT_STF_IMPORT_MERGE_RIGHT, CONTEXT_STF_IMPORT_MERGE_RIGHT
#         },
#         { "", None, 0, 0, 0 },
#         {
#             N_("_Split this column"), None,
#             0, 1 << CONTEXT_STF_IMPORT_SPLIT, CONTEXT_STF_IMPORT_SPLIT
#         },
#         { "", None, 0, 0, 0 },
#         {
#             N_("_Widen this column"), "go-next",
#             0, 1 << CONTEXT_STF_IMPORT_WIDEN, CONTEXT_STF_IMPORT_WIDEN
#         },
#         {
#             N_("_Narrow this column"), "go-previous",
#             0, 1 << CONTEXT_STF_IMPORT_NARROW, CONTEXT_STF_IMPORT_NARROW
#         },
#         { None, None, 0, 0, 0 },
#

    def get_new_col_rel_pos(GtkTreeViewColumn .tcol, int dx):
        renderers = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT(tcol))
        cell = GTK_CELL_RENDERER(renderers.data)
        g_list_free(renderers)
        PangoFontDescription .font_desc
        g_object_get(G_OBJECT(cell), "font_desc", &font_desc, None)

        PangoLayout .layout = gtk_widget_create_pango_layout(GTK_WIDGET(treeview), "x")
        pango_layout_set_font_description(layout, font_desc)
        int width
        pango_layout_get_pixel_size(layout, &width, None)
        if(width < 1) width = 1
        uint32_t charindex =(dx + width / 2) / width
        g_object_unref(layout)
        pango_font_description_free(font_desc)

        return charindex

    def fixed_context_menu_handler(GnumericPopupMenuElement const .element,gpointer userdata)
            info =(CsvImpTransAssist.)userdata
            fwtok = dynamic_cast<GncFwTokenizer.>(self.self.m_tokenizer.get())

            switch(element.index)
            {
                case CONTEXT_STF_IMPORT_MERGE_LEFT:
            fwtok.col_delete(self.fixed_context_col - 1)
            break
            case CONTEXT_STF_IMPORT_MERGE_RIGHT:
            fwtok.col_delete(self.fixed_context_col)
            break
            case CONTEXT_STF_IMPORT_SPLIT:
            fwtok.col_split(self.fixed_context_col, self.fixed_context_offset)
            break
            case CONTEXT_STF_IMPORT_WIDEN:
            fwtok.col_widen(self.fixed_context_col)
            break
            case CONTEXT_STF_IMPORT_NARROW:
            fwtok.col_narrow(self.fixed_context_col)
            break
            default:
             /. Nothing ./
            }

            try
                {
                    self.self.tokenize(False)
            }
            except(std::range_error& e)
            {
                show_error(GTK_WINDOW(self.csv_imp_asst), "%s", e.what())
            return False
            }
            self.self.preview_refresh_table()
            return True


    def fixed_context_menu(GdkEventButton .event,
        int col, int offset)
        {
        fwtok = dynamic_cast<GncFwTokenizer.>(self.m_tokenizer.get())
        fixed_context_col = col
        fixed_context_offset = offset

        int sensitivity_filter = 0
        if(not fwtok.col_can_delete(col - 1))
        sensitivity_filter |=(1 << CONTEXT_STF_IMPORT_MERGE_LEFT)
        if(not fwtok.col_can_delete(col))
        sensitivity_filter |=(1 << CONTEXT_STF_IMPORT_MERGE_RIGHT)
        if(not fwtok.col_can_split(col, offset))
        sensitivity_filter |=(1 << CONTEXT_STF_IMPORT_SPLIT)
        if(not fwtok.col_can_widen(col))
        sensitivity_filter |=(1 << CONTEXT_STF_IMPORT_WIDEN)
        if(not fwtok.col_can_narrow(col))
        sensitivity_filter |=(1 << CONTEXT_STF_IMPORT_NARROW)

        gnumeric_create_popup_menu(popup_elements, &fixed_context_menu_handler,
        this, 0,
        sensitivity_filter, event)
        }

    def preview_split_column(int col, int offset)
        fwtok = dynamic_cast<GncFwTokenizer.>(self.m_tokenizer.get())
        fwtok.col_split(col, offset)
        try
            {
                self.tokenize(False)
        }
        except(std::range_error& e)
        {
            show_error(self, "%s", e.what())
        return
        }
        preview_refresh_table()

    def preview_update_fw_columns(GtkTreeView. treeview, GdkEventButton. event)
        if(event.window != gtk_tree_view_get_bin_window(treeview))
        return

        GtkTreeViewColumn .tcol = None
        int cell_x = 0
        success = gtk_tree_view_get_path_at_pos(treeview,
                                                     (int)event.x,(int)event.y,
                                                                                 None, &tcol, &cell_x, None)
        if(not success)
            return

        tcol_list = gtk_tree_view_get_columns(treeview)
        tcol_num = g_list_index(tcol_list, tcol)
        g_list_free(tcol_list)
        if(tcol_num <= 0)
            return

        dcol = tcol_num - 1
        offset = get_new_col_rel_pos(tcol, cell_x)
        if(event.type == GDK_2BUTTON_PRESS and event.button == 1)
            preview_split_column(dcol, offset)
        elif(event.type == GDK_BUTTON_PRESS and event.button == 3)
        fixed_context_menu(event, dcol, offset)
        }


    def preview_row_fill_state_cells(GtkListStore .store, GtkTreeIter .iter,std::string& err_msg, skip)
        const char .c_err_msg = None
        const char .icon_name = None
        const char .fcolor = None
        const char .bcolor = None
        if(not skip and not err_msg.empty()):
            fcolor = "black"
            bcolor = "pink"
            c_err_msg = err_msg
            icon_name = "dialog-error"
        gtk_list_store_set(store, iter,
                            PREV_COL_FCOLOR, fcolor,
                            PREV_COL_BCOLOR, bcolor,
                            PREV_COL_STRIKE, skip,
                            PREV_COL_ERROR, c_err_msg,
                            PREV_COL_ERR_ICON, icon_name, -1)

    def preview_cbox_factory(GtkTreeModel. model, uint32_t colnum):

        cbox = gtk_combo_box_new_with_model(model)

        renderer = gtk_cell_renderer_text_new()
        gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(cbox),
                                    renderer, True)
        gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(cbox),
                                       renderer, "text", COL_TYPE_NAME)

        valid = gtk_tree_model_get_iter_first(model, &iter)
        while(valid)
            {
                gint stored_col_type
        gtk_tree_model_get(model, &iter,
        COL_TYPE_ID, &stored_col_type, -1)
        if(stored_col_type == static_cast<int>( self.column_types()[colnum]))
        break
        valid = gtk_tree_model_iter_next(model, &iter)
        }
        if(valid)
        gtk_combo_box_set_active_iter(GTK_COMBO_BOX(cbox), &iter)

        g_object_set_data(G_OBJECT(cbox), "col-num", GUINT_TO_POINTER(colnum))
        g_signal_connect(G_OBJECT(cbox), "changed",
        G_CALLBACK(csv_tximp_preview_col_type_changed_cb),(gpointer)this)

        gtk_widget_show(cbox)
        return cbox

    def preview_style_column(uint32_t col_num, GtkTreeModel. model):
        col = gtk_tree_view_get_column(treeview, col_num)
        renderer = col.get_cells()
        if col_num == 0:
                gtk_tree_view_column_set_attributes(col, renderer,
                                                     "icon-name", PREV_COL_ERR_ICON,
                                                     "cell-background", PREV_COL_BCOLOR, None)
            g_object_set(G_OBJECT(renderer), "stock-size", GTK_ICON_SIZE_MENU, None)
            g_object_set(G_OBJECT(col), "sizing", GTK_TREE_VIEW_COLUMN_FIXED,
                          "fixed-width", 20, None)
            gtk_tree_view_column_set_resizable(col, False)
        else:
            gtk_tree_view_column_set_attributes(col, renderer,
                                                 "foreground", PREV_COL_FCOLOR,
                                                 "background", PREV_COL_BCOLOR,
                                                 "strikethrough", PREV_COL_STRIKE,
                                                 "text", col_num + PREV_N_FIXED_COLS -1, None)

            g_object_set(G_OBJECT(renderer), "family", "monospace", None)

            cbox = preview_cbox_factory(GTK_TREE_MODEL(model), col_num - 1)
            gtk_tree_view_column_set_widget(col, cbox)

            gtk_tree_view_column_set_resizable(col, True)
            gtk_tree_view_column_set_clickable(col, True)

    def make_column_header_model(multi_split):
        combostore = Gtk.ListStore(str, int)
        for(col_type : gs_csv_col_type_strs):
            if(sanitize_trans_prop(col_type.first, multi_split) == col_type.first):
                gtk_list_store_append(combostore, &iter)
                gtk_list_store_set(combostore, &iter,
                COL_TYPE_NAME, _(col_type.second),
                COL_TYPE_ID, static_cast<int>(col_type.first), -1)
        return combostore


    def preview_refresh_table()
        self.preview_validate_settings()
        ncols = PREV_N_FIXED_COLS + self.column_types().size()
        model_col_types = g_new(GType, ncols)
        model_col_types[PREV_COL_FCOLOR] = str
        model_col_types[PREV_COL_BCOLOR] = str
        model_col_types[PREV_COL_ERROR] = str
        model_col_types[PREV_COL_ERR_ICON] = str
        model_col_types[PREV_COL_STRIKE] = G_TYPE_BOOLEAN
        for(guint i = PREV_N_FIXED_COLS i <  ncols i++)
            model_col_types[i] = str
        store = gtk_list_store_newv(ncols, model_col_types)


        for(parse_line : self.m_parsed_lines)
            {

        gtk_list_store_append(store, &iter)
        preview_row_fill_state_cells(store, &iter,
        std::get<PL_ERROR>(parse_line), std::get<PL_SKIP>(parse_line))

        for(cell_str_it = std::get<PL_INPUT>(parse_line).cbegin() cell_str_it != std::get<PL_INPUT>(parse_line).cend() cell_str_it++)
        {
            uint32_t pos = PREV_N_FIXED_COLS + cell_str_it - std::get<PL_INPUT>(parse_line).cbegin()
        gtk_list_store_set(store, &iter, pos, cell_str_it, -1)
        }
        }
        gtk_tree_view_set_model(treeview, GTK_TREE_MODEL(store))
        gtk_tree_view_set_tooltip_column(treeview, PREV_COL_ERROR)


        ntcols = gtk_tree_view_get_n_columns(treeview)

        while(ntcols > ncols - PREV_N_FIXED_COLS + 1)
            {
                col = gtk_tree_view_get_column(treeview, ntcols - 1)
        gtk_tree_view_column_clear(col)
        ntcols = gtk_tree_view_remove_column(treeview, col)
        }

        while(ntcols < ncols - PREV_N_FIXED_COLS + 1)
            {
                renderer = gtk_cell_renderer_text_new()
        if(ntcols == 0)
        renderer = gtk_cell_renderer_pixbuf_new()
        col = gtk_tree_view_column_new(self)
        gtk_tree_view_column_pack_start(col, renderer, False)
        ntcols = gtk_tree_view_append_column(treeview, col)
        }

        combostore = make_column_header_model(self.multi_split())
        for(uint32_t i = 0 i < ntcols i++)
            preview_style_column(i, combostore)

        base_acct = gs_account_sel_get_account(GS_ACCOUNT_SEL(acct_selector))
        if(self.base_account() != base_acct):
            g_signal_handlers_block_by_func(acct_selector,self.preview_acct_sel_cb)
            gs_account_sel_set_account(GS_ACCOUNT_SEL(acct_selector),
                                        self.base_account() , False)
            g_signal_handlers_unblock_by_func(acct_selector,self.preview_acct_sel_cb)


        gtk_widget_show_all(GTK_WIDGET(treeview))


    def preview_refresh():
        skip_start_lines = self.skip_start_lines()
        skip_end_lines = self.skip_end_lines()
        skip_alt_lines = self.skip_alt_lines()

        adj = gtk_spin_button_get_adjustment(start_row_spin)
        adj.set_upper(self.m_parsed_lines.size())
        gtk_spin_button_set_value(start_row_spin, skip_start_lines)

        adj = gtk_spin_button_get_adjustment(end_row_spin)
        adj.set_upper(self.m_parsed_lines.size())
        gtk_spin_button_set_value(end_row_spin, skip_end_lines)

        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(skip_alt_rows_button),
                                      skip_alt_lines)

        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(multi_split_cbutton),
                                      self.multi_split())
        acct_selector.set_sensitive(not self.multi_split())

        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(csv_button),
                                     (self.file_format() == GncImpFileFormat::CSV))
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(fixed_button),
                                     (self.file_format() != GncImpFileFormat::CSV))

        gtk_combo_box_set_active(GTK_COMBO_BOX(date_format_combo),
                                  self.date_format())
        gtk_combo_box_set_active(GTK_COMBO_BOX(currency_format_combo),
                                  self.currency_format())
        go_charmap_sel_set_encoding(encselector, self.encoding())

        if(self.file_format() == GncImpFileFormat::CSV)
        {
            separators = self.separators()
        const stock_sep_chars = " \t,:-")
        for(int i = 0 i < SEP_NUM_OF_TYPES i++)
        {
            g_signal_handlers_block_by_func(sep_button[i],self.preview_sep_button_cb)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(sep_button[i]),
                                      separators.find(stock_sep_chars[i]) != std::string::npos)
        g_signal_handlers_unblock_by_func(sep_button[i],self.preview_sep_button_cb)
        }

        pos = separators.find_first_of(stock_sep_chars)
        while(not separators.empty() and pos != std::string::npos)
        {
            separators.erase(pos)
        pos = separators.find_first_of(stock_sep_chars)
        }
        g_signal_handlers_block_by_func(custom_cbutton,self.preview_sep_button_cb)
        g_signal_handlers_block_by_func(custom_entry,self.preview_sep_button_cb)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(custom_cbutton),
        not separators.empty())
        gtk_entry_set_text(GTK_ENTRY(custom_entry), separators)
        g_signal_handlers_unblock_by_func(custom_cbutton,self.preview_sep_button_cb)
        g_signal_handlers_unblock_by_func(custom_entry,self.preview_sep_button_cb)
        csv_tximp_preview_sep_button_cb(GTK_WIDGET(custom_cbutton))
        }

        self.preview_refresh_table()

    def preview_validate_settings(self)
        error_msg = self.verify()
        self.set_page_complete(preview_page, error_msg.empty())
        gtk_label_set_markup(GTK_LABEL(instructions_label), error_msg)
        gtk_widget_set_visible(GTK_WIDGET(instructions_image), not error_msg.empty())

        if(error_msg.empty())
        gtk_widget_set_visible(GTK_WIDGET(account_match_page),
        not self.accounts().empty())

    def acct_match_set_accounts(self)
        store = gtk_tree_view_get_model(GTK_TREE_VIEW(account_match_view))
        store.clear()
        accts = self.accounts()
        for acct in accts:
            GtkTreeIter acct_iter
            store.append(&acct_iter)
            store.set(&acct_iter, MAPPING_STRING, acct,
            MAPPING_FULLPATH, _("No Linked Account"), MAPPING_ACCOUNT, None, -1)


    def acct_match_check_all(model):
        valid = gtk_tree_model_get_iter_first(model, &iter)
        while valid is not None:
            gtk_tree_model_get(model, &iter, MAPPING_ACCOUNT, &account, -1)
            if(not account)
                return False
            valid = gtk_tree_model_iter_next(model, &iter)
        return True

    def acct_match_text_parse(std::string acct_name):
        sep = gs_get_account_separator_string(self)
        sep_pos = acct_name.rfind(sep)
        if(sep_pos == std::string::npos)
            return acct_name

        parent = acct_name.substr(0, sep_pos)
        root = gs_get_current_root_account(self)

        if(gs_account_lookup_by_full_name(root, parent))
            return acct_name
        else:
            if(g_strcmp0(sep,":") == 0):
                alt_sep = "-"
            else:
                alt_sep = ":"
            for(sep_pos = acct_name.find(sep) sep_pos != std::string::npos
            sep_pos = acct_name.find(sep))
            acct_name.replace(sep_pos, strlen(sep), alt_sep)
            return acct_name

    def acct_match_select(model, iter)
        gtk_tree_model_get(model, iter, MAPPING_STRING, &text,
        MAPPING_ACCOUNT, &account, -1)

        acct_name = csv_tximp_acct_match_text_parse(text)
        gs_acc = gs_import_select_account(GTK_WIDGET(csv_imp_asst), None, True,
        acct_name, None, ACCT_TYPE_NONE, account, None)

        if(gs_acc):
            fullpath = gs_account_get_full_name(gs_acc)
            gtk_list_store_set(GTK_LIST_STORE(model), iter,
                                MAPPING_ACCOUNT, gs_acc,
                                MAPPING_FULLPATH, fullpath, -1)

            gs_csv_account_map_change_mappings(account, gs_acc, text)
        self.set_page_complete(account_match_page,
        csv_tximp_acct_match_check_all(model))

    def acct_match_via_button(self):
        model = gtk_tree_view_get_model(GTK_TREE_VIEW(account_match_view))
        selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(account_match_view))

        if(gtk_tree_selection_get_selected(selection, &model, &iter))
        acct_match_select(model, &iter)

    def acct_match_via_view_dblclick(GdkEventButton .event)
        if(event.button == 1 and event.type == GDK_2BUTTON_PRESS)
        {
        window = gtk_tree_view_get_bin_window(GTK_TREE_VIEW(account_match_view))
        if(event.window != window)
            return False

        GtkTreePath .path
        if(gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(account_match_view),(gint) event.x,(gint) event.y,
        &path, None, None, None))
        {
            DEBUG("event.x is %d and event.y is %d",(gint)event.x,(gint)event.y)

        model = gtk_tree_view_get_model(GTK_TREE_VIEW(account_match_view))

        if(gtk_tree_model_get_iter(model, &iter, path))
        acct_match_select(model, &iter)
        gtk_tree_path_free(path)
        }
        return True
        }
        return False
        }

    def file_page_prepare(self):
        starting_dir = gs_get_default_directory(GS_PREFS_GROUP)
        if(starting_dir)
            gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser), starting_dir)
        self.set_page_complete(account_match_page, False)

    def preview_page_prepare(self):
        go_back = False
        if(self.)
            self.reset()

        self. = std::unique_ptr<GncTxImport>(new GncTxImport)

        try
            {
                self.file_format(GncImpFileFormat::CSV)
        self.load_file(m_file_name)
        self.tokenize(True)
        }
        except(std::ifstream::failure& e)
        {
            show_error(self, "%s", e.what())
        go_back = True
        }
        except(std::range_error &e)
        {
            show_error(self, "%s", _(e.what()))
        go_back = True
        }

        if go_back:
            self.previous_page(csv_imp_asst)
        else:
            self.preview_refresh()
            self.preview_populate_settings_combo()
            self.settings_combo.set_active(0)
            self.req_mapped_accts(False)
            self.set_page_complete(self.preview_page, False)

            g_idle_add((GSourceFunc)csv_imp_preview_queue_rebuild_table)

    def account_match_page_prepare(self):
        self.req_mapped_accts(True)
        acct_match_set_accounts(self)
        store = self.account_match_view.get_model()
        # gs_csv_account_map_load_mappings(store)

        text = "<span size=\"medium\" color=\"red\"><b>"
        text += "To change mapping, double click on a row or select a row and press the button.."
        text += "</b></span>"
        self.account_match_label.set_markup(text)
        self.account_match_view.set_sensitive(True)
        self.account_match_btn.set_sensitive(True)
        self.set_page_complete(self.account_match_page, csv_tximp_acct_match_check_all(store))

    def doc_page_prepare():
        self.commit()
        try:
            col_types = self.column_types()
            acct_col = std::find(col_types.begin(),
                                       col_types.end(), GncTransPropType::ACCOUNT)
            if(acct_col != col_types.end())
                self.set_column_type(acct_col - col_types.begin(),
            GncTransPropType::ACCOUNT, True)
            acct_col = std::find(col_types.begin(),
                                  col_types.end(), GncTransPropType::TACCOUNT)
            if(acct_col != col_types.end())
            self.set_column_type(acct_col - col_types.begin(),
            GncTransPropType::TACCOUNT, True)
            }
        except TypeError as err:
            show_error(self,"An unexpected error has occurred while mapping accounts. Please report this as a bug.\n\n"
                                "Error message:\n%s" % err)
            self.set_current_page(2)
        # if new_book:
        #     new_book = gs_new_book_option_display(GTK_WIDGET(csv_imp_asst))
        cancel_button = Gtk.Button.new_with_mnemonic("_Cancel")
        self.add_action_widget(cancel_button)
        button_area = cancel_button.get_parent()

        if isinstance(button_area, Gtk.HeaderBar):
            button_area.child_set(cancel_button,"pack-type", Gtk.PackType.START,None)

        cancel_button.connect("clicked",self.close_cb)
        cancel_button.show()



    def match_page_prepare():
        try:
            self.create_transactions()
        except TypeError as err:
            show_error(self,"An unexpected error has occurred while creating transactions. Please report this as a bug.\n\n"
                                "Error message:\n%s" %  err)
        self.set_current_page(2)
        self.commit()
        text =  "<span size=\"medium\" color=\"red\"><b>"
        text += "Double click on rows to change, then click on Apply to Import"
        text += "</b></span>"
        self.match_label.set_markup(text)
        help_button = Gtk.Button.new_with_mnemonic("_Help")
        self.add_action_widget(help_button)
        button_area = help_button.get_parent()
        if isinstance(button_area, Gtk.HeaderBar):
            button_area.child_set(help_button,"pack-type", Gtk.PackType.START)
        else:
            button_area.set_halign(Gtk.Align.FILL)
            button_area.set_hexpand(True)
            button_area.set_child_packing(help_button,False, False, 0, Gtk.PackType.START)

        help_button.connect("clicked", self.on_matcher_help_clicked)
        help_button.show()

        for trans_it in self.m_transactions:
            draft_trans = trans_it.second
            if draft_trans.trans:
                self.gen_trans_list_add_trans(draft_trans.trans)
                draft_trans.trans = None
        self.show_all()

    def summary_page_prepare(self):
        self.remove_action_widget(self.help_button)
        self.remove_action_widget(self.cancel_button)
        text = "<span size=\"medium\"><b>"
        text +="The transactions were imported from file '{1}'." % self.m_file_name
        text += "</b></span>"
        self.set_markup(text)

    def prepare_cb(self, page):
        if page == self.file_page:
            self.file_page_prepare()
        elif page == self.preview_page:
            self.preview_page_prepare()
        elif page == self.account_match_page:
            self.account_match_page_prepare()
        elif page == self.doc_page:
            self.doc_page_prepare()
        elif page == self.match_page:
            self.match_page_prepare()
        elif page == self.summary_page:
            self.summary_page_prepare()

    def finish(self):
        if self.m_transactions is not None:
            self.gen_trans_assist_start(local_csv_imp_gui)

    def compmgr_close(self):
        gs_save_window_size(GS_PREFS_GROUP, self)

    def close_handler(self):
        ComponentManager.unregister_by_data(ASSISTANT_CSV_IMPORT_TRANS_CM_CLASS, self)
        self.compmgr_close()

GObject.type_register(TransactionImportAssistant)
