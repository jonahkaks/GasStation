from nameparser import HumanName

from gasstation.utilities.cursors import gs_set_busy_cursor, gs_unset_busy_cursor
from gasstation.utilities.dialog import *
from gasstation.utilities.ui import PrintAmountInfo, gs_default_currency
from gasstation.views.components.address import AddressFrame
from gasstation.views.components.avatar import Avatar
from gasstation.views.dialogs.location.location import LocationDialog
from gasstation.views.dialogs.payment.payment_method import PaymentMethodDialog
from gasstation.views.dialogs.tax import NewTaxDialog
from gasstation.views.selections.amount_edit import AmountEntry
from gasstation.views.selections.currency import CurrencySelection
from gasstation.views.selections.date_edit import DateSelection
from gasstation.views.selections.reference import ReferenceSelection
from gasstation.views.selections.telephone_entry import TelNumberEntry
from libgasstation import Staff, ID_STAFF, Location, Gender
from libgasstation import Tax
from libgasstation.core.address import Address
from libgasstation.core.component import *
from libgasstation.core.contact import Contact
from libgasstation.core.email import Email, EmailForType
from libgasstation.core.payment import PaymentMethod
from libgasstation.core.phone import PhoneType, Phone
from libgasstation.core.session import Session

DIALOG_NEW_STAFF_CM_CLASS = "dialog-new-staff"
DIALOG_EDIT_STAFF_CM_CLASS = "dialog-edit-staff"
from gi.repository import GObject


class StaffDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/staff/staff.ui')
class StaffDialog(Gtk.Assistant):
    __gtype_name__ = "StaffDialog"
    avatar: Avatar = Gtk.Template.Child()
    next_of_kin_avatar: Avatar = Gtk.Template.Child()
    full_name_entry: Gtk.Entry = Gtk.Template.Child()
    next_of_kin_full_name_entry: Gtk.Entry = Gtk.Template.Child()
    display_name_combo: Gtk.ComboBox = Gtk.Template.Child()

    email_entry: Gtk.Entry = Gtk.Template.Child()
    next_of_kin_email_entry: Gtk.Entry = Gtk.Template.Child()
    phone_entry: TelNumberEntry = Gtk.Template.Child()
    next_of_kin_phone_entry: TelNumberEntry = Gtk.Template.Child()
    mobile_entry: TelNumberEntry = Gtk.Template.Child()
    next_of_kin_mobile_entry: TelNumberEntry = Gtk.Template.Child()
    tin_entry: Gtk.Entry = Gtk.Template.Child()
    nin_entry: Gtk.Entry = Gtk.Template.Child()
    ssn_entry: Gtk.Entry = Gtk.Template.Child()
    date_of_birth: DateSelection = Gtk.Template.Child()
    date_of_hire: DateSelection = Gtk.Template.Child()
    notes_entry: Gtk.TextView = Gtk.Template.Child()

    salary_payment_method_frame: Gtk.Frame = Gtk.Template.Child()
    currency_frame: Gtk.Frame = Gtk.Template.Child()
    salary_advance_limit_frame: Gtk.Frame = Gtk.Template.Child()
    tax_frame: Gtk.Frame = Gtk.Template.Child()

    opening_balance_date_frame: Gtk.Frame = Gtk.Template.Child()
    opening_balance_date: DateSelection = Gtk.Template.Child()
    opening_balance_amount_frame: Gtk.Frame = Gtk.Template.Child()
    location_frame: Gtk.Frame = Gtk.Template.Child()

    male_radio: Gtk.RadioButton = Gtk.Template.Child()
    female_radio: Gtk.RadioButton = Gtk.Template.Child()
    address_frame: AddressFrame = Gtk.Template.Child()
    next_of_kin_address_frame: AddressFrame = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, staff=None, name=None):
        super().__init__(use_header_bar=False)
        self.book = book
        self.address_frame.street_entry.connect("focus-out-event", self.profile_page_enable_next)
        self.next_of_kin_address_frame.street_entry.connect("focus-out-event", self.next_of_kin_page_enable_next)
        self.dialog_type = StaffDialogType.NEW if staff is None else StaffDialogType.EDIT
        self.staff = staff
        self.created_staff = None
        self.notes_buffer = self.notes_entry.get_buffer()
        self.get_child().get_children()[0].destroy()
        if parent is not None:
            self.set_transient_for(parent)

        self.get_style_context().add_class("StaffDialog")
        self.display_name_combo.set_model(Gtk.ListStore(str))

        staff = self.staff
        if staff:
            currency = staff.get_currency()
        else:
            currency = gs_default_currency()

        self.display_name_combo.set_model(Gtk.ListStore(str))

        self.location_selection = ReferenceSelection(Location, LocationDialog)
        self.location_selection.connect("selection_changed", self.hire_information_enable_next)
        self.location_selection.set_new_ability(True)
        self.location_frame.add(self.location_selection)
        self.location_selection.show()

        self.tax_selection = ReferenceSelection(Tax, NewTaxDialog)
        self.tax_selection.set_new_ability(True)
        self.tax_selection.connect("selection_changed", self.hire_information_enable_next)
        self.tax_frame.add(self.tax_selection)
        self.tax_selection.show()

        self.currency_selection = CurrencySelection()
        self.currency_selection.set_currency(currency)
        self.currency_selection.connect("changed", self.hire_information_enable_next)
        self.currency_frame.add(self.currency_selection)
        self.currency_selection.show()

        self.salary_advance_limit = AmountEntry()
        print_info = PrintAmountInfo.commodity(currency, False)
        self.salary_advance_limit.set_evaluate_on_enter(True)
        self.salary_advance_limit.connect("amount_changed", self.hire_information_enable_next)
        self.salary_advance_limit.set_print_info(print_info)
        self.salary_advance_limit_frame.add(self.salary_advance_limit)
        self.salary_advance_limit.show()

        self.salary_payment_method = ReferenceSelection(PaymentMethod, PaymentMethodDialog)
        self.salary_payment_method.set_new_ability(True)
        self.salary_payment_method.connect("selection_changed", self.hire_information_enable_next)
        self.salary_payment_method_frame.add(self.salary_payment_method)
        self.salary_payment_method.show()

        edit = AmountEntry()
        edit.set_evaluate_on_enter(True)
        print_info = PrintAmountInfo.integral(2)
        print_info.max_decimal_places = 5
        edit.set_print_info(print_info)
        self.opening_balance = edit
        self.opening_balance_amount_frame.add(edit)
        edit.show()
        if name is not None:
            self.full_name_entry.set_text(name)
        gs_window_adjust_for_screen(self)
        self.component_id = ComponentManager.register(DIALOG_NEW_STAFF_CM_CLASS,
                                                      self.refresh_handler,
                                                      None if modal else self.close_handler)

        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id,
                                           ID_STAFF,
                                           EVENT_MODIFY | EVENT_DESTROY)
        if staff is not None:
            self.to_ui()
        self.avatar.set_icon_name("avatar-default")
        self.next_of_kin_avatar.set_icon_name("avatar-default")
        self.show()

    def to_ui(self):
        staff = self.staff
        contact = staff.get_contact()
        if contact is not None:
            self.full_name_entry.set_text(contact.get_full_name())
            if self.dialog_type == StaffDialogType.EDIT:
                model = self.display_name_combo.get_model()
                self.display_name_changed()
                d = contact.get_display_name()
                _iter = model.get_iter_first()
                while _iter is not None:
                    tree_string = model.get_value(_iter, 0)
                    if GLib.utf8_collate(d, tree_string) == 0:
                        self.display_name_combo.set_active_iter(_iter)
                        break
                    _iter = model.iter_next(_iter)
            if any(contact.emails):
                self.email_entry.set_text(",".join(set(ea.get_address() for ea in contact.emails)))
            if any(contact.phones):
                works = list(filter(lambda a: a.type == PhoneType.WORK, contact.phones))
                if any(works):
                    self.phone_entry.set_number(works[0].get_number())
                mobiles = list(filter(lambda a: a.type == PhoneType.MOBILE, contact.phones))
                if any(mobiles):
                    self.mobile_entry.set_number(mobiles[0].get_number())
            if any(contact.addresses):
                ba = contact.addresses[0]
                self.address_frame.set_address(ba)

        url = contact.get_image_url()
        self.avatar.set_fullname(contact.get_full_name())
        self.avatar.set_url(url)

        next_of_kin_contact = self.staff.get_next_of_kin()
        if next_of_kin_contact is not None:
            self.next_of_kin_full_name_entry.set_text(next_of_kin_contact.get_full_name())
            if any(next_of_kin_contact.emails):
                self.next_of_kin_email_entry.set_text(
                    ",".join(set(ea.get_address() for ea in next_of_kin_contact.emails)))
            if any(next_of_kin_contact.phones):
                works = list(filter(lambda a: a.type == PhoneType.WORK, next_of_kin_contact.phones))
                if any(works):
                    self.next_of_kin_phone_entry.set_number(works[0].get_number())
                mobiles = list(filter(lambda a: a.type == PhoneType.MOBILE, next_of_kin_contact.phones))
                if any(mobiles):
                    self.next_of_kin_mobile_entry.set_number(mobiles[0].get_number())

            if any(next_of_kin_contact.addresses):
                ba = next_of_kin_contact.addresses[0]
                self.next_of_kin_address_frame.set_address(ba)

        url = next_of_kin_contact.get_image_url()
        if url is None:
            self.next_of_kin_avatar.set_fullname(next_of_kin_contact.get_full_name())
        else:
            self.next_of_kin_avatar.set_url(url)

        self.notes_buffer.set_text(staff.get_notes() or "")

        self.tin_entry.set_text(staff.get_tin() or "")
        self.nin_entry.set_text(staff.get_nin() or "")
        self.ssn_entry.set_text(staff.get_ssn() or "")
        self.date_of_hire.set_date(staff.get_date_of_hire() or "")
        self.date_of_birth.set_date(staff.get_date_of_birth() or "")
        self.salary_advance_limit.set_amount(staff.get_salary_advance_limit())
        self.currency_selection.set_currency(staff.get_currency())
        self.salary_payment_method.set_ref(staff.get_payment_method())
        self.location_selection.set_ref(staff.get_location())
        if staff.get_gender() == Gender.MALE:
            self.male_radio.set_active(True)
        else:
            self.female_radio.set_active(True)
        self.tax_selection.set_ref(staff.get_tax())
        txn = staff.get_opening_balance_transaction()
        if txn is not None:
            self.opening_balance.set_amount(staff.get_opening_balance())
            self.opening_balance_date.set_date(txn.get_post_date())

    def to_staff(self):
        staff = self.staff
        ComponentManager.suspend()
        if staff is None:
            staff = Staff(self.book)
            self.staff = staff
        contact = staff.get_contact()

        if contact is None:
            contact = Contact(self.book)
            staff.set_contact(contact)
        self.to_staff_contact(contact)

        next_of_kin_contact = staff.get_next_of_kin()
        if self.next_of_kin_full_name_entry.get_text() != "":
            if next_of_kin_contact is None:
                next_of_kin_contact = Contact(self.book)
                staff.set_next_of_kin(next_of_kin_contact)
            self.to_next_of_kin_contact(next_of_kin_contact)
        else:
            if next_of_kin_contact is not None:
                next_of_kin_contact.destroy()

        staff.set_payment_method(self.salary_payment_method.get_ref())
        staff.set_salary_advance_limit(self.salary_advance_limit.get_amount())
        curr = self.currency_selection.get_currency()
        staff.set_currency(curr)

        start = self.notes_buffer.get_start_iter()
        end = self.notes_buffer.get_end_iter()
        notes = self.notes_buffer.get_text(start, end, False)
        staff.set_notes(notes)
        staff.set_gender(Gender.MALE if self.male_radio.get_active() else Gender.FEMALE)
        staff.set_opening_balance(self.opening_balance.get_amount(), self.opening_balance_date.get_date())
        staff.set_date_of_birth(self.date_of_birth.get_date())
        staff.set_date_of_hire(self.date_of_hire.get_date())
        staff.set_tax(self.tax_selection.get_ref())
        staff.set_tin(self.tin_entry.get_text())
        staff.set_location(self.location_selection.get_ref())
        staff.commit_edit()
        ComponentManager.resume()
        self.staff = staff

    @Gtk.Template.Callback()
    def on_prepare(self, *args):
        current_page = self.get_current_page()
        if current_page == 0:
            self.profile_page_enable_next()
        elif current_page == 1:
            self.hire_information_enable_next()
        elif current_page == 2:
            self.next_of_kin_page_enable_next()

    def to_next_of_kin_contact(self, next_of_kin_contact):
        next_of_kin_contact.begin_edit()
        url = self.next_of_kin_avatar.get_url()
        if url is not None:
            next_of_kin_contact.set_image_url(url)
        full_name = self.next_of_kin_full_name_entry.get_text()
        next_of_kin_contact.set_full_name(full_name)
        next_of_kin_contact.set_display_name(full_name)
        next_of_kin_contact_emails = dict(map(lambda a: (a.get_address(), a), next_of_kin_contact.emails))
        for i, e in enumerate(set(self.next_of_kin_email_entry.get_text().split(","))):
            if e in next_of_kin_contact_emails.keys():
                next_of_kin_contact_emails.pop(e)
                continue
            if e == "":
                continue
            ea = Email(self.book)
            ea.begin_edit()
            if i != 0:
                ea.set_type(EmailForType.OTHER)
            ea.set_address(e)
            ea.set_contact(next_of_kin_contact)
            ea.commit_edit()
        for remaining in next_of_kin_contact_emails.values():
            remaining.begin_edit()
            remaining.set_dirty()
            remaining.set_destroying()
            remaining.commit_edit()

        work_number = self.next_of_kin_phone_entry.get_number()
        if work_number is not None:
            works = list(filter(lambda a: a.type == PhoneType.WORK, next_of_kin_contact.phones))
            if any(works):
                work_phone = works[0]
                work_phone.set_number(work_number)
            else:
                work_phone = Phone(self.book)
                work_phone.begin_edit()
                work_phone.set_type(PhoneType.WORK)
                work_phone.set_number(work_number)
                work_phone.set_contact(next_of_kin_contact)
                work_phone.commit_edit()

        mobile_number = self.next_of_kin_mobile_entry.get_number()
        if mobile_number is not None:
            mobiles = list(filter(lambda a: a.type == PhoneType.MOBILE, next_of_kin_contact.phones))
            if any(mobiles):
                mobile_phone = list(mobiles)[0]
                mobile_phone.set_number(mobile_number)
            else:
                mobile_phone = Phone(self.book)
                mobile_phone.begin_edit()
                mobile_phone.set_type(PhoneType.MOBILE)
                mobile_phone.set_number(mobile_number)
                mobile_phone.set_contact(next_of_kin_contact)
                mobile_phone.commit_edit()

        if any(next_of_kin_contact.addresses):
            address = next_of_kin_contact.addresses[0]
        else:
            address = Address(self.book)
        address.begin_edit()
        self.next_of_kin_address_frame.copy_to_address(address)
        address.set_contact(next_of_kin_contact)
        address.commit_edit()

    def to_staff_contact(self, contact):
        contact.begin_edit()
        url = self.avatar.get_url()
        if url is not None:
            contact.set_image_url(url)
        contact.set_full_name(self.full_name_entry.get_text())
        model = self.display_name_combo.get_model()
        display_name = model.get_value(self.display_name_combo.get_active_iter(), 0)
        contact.set_display_name(display_name)
        contact_emails = dict(map(lambda a: (a.get_address(), a), contact.emails))
        for i, e in enumerate(set(self.email_entry.get_text().split(","))):
            if e in contact_emails.keys():
                contact_emails.pop(e)
                continue
            if e == "":
                continue
            ea = Email(self.book)
            ea.begin_edit()
            if i != 0:
                ea.set_type(EmailForType.OTHER)
            ea.set_address(e)
            ea.set_contact(contact)
            ea.commit_edit()
        for remaining in contact_emails.values():
            remaining.begin_edit()
            remaining.set_dirty()
            remaining.set_destroying()
            remaining.commit_edit()

        work_number = self.phone_entry.get_number()
        if work_number is not None:
            works = list(filter(lambda a: a.type == PhoneType.WORK, contact.phones))
            if any(works):
                work_phone = works[0]
                work_phone.set_number(work_number)
            else:
                work_phone = Phone(self.book)
                work_phone.begin_edit()
                work_phone.set_type(PhoneType.WORK)
                work_phone.set_number(work_number)
                work_phone.set_contact(contact)
                work_phone.commit_edit()

        mobile_number = self.mobile_entry.get_number()
        if mobile_number is not None:
            mobiles = list(filter(lambda a: a.type == PhoneType.MOBILE, contact.phones))
            if any(mobiles):
                mobile_phone = list(mobiles)[0]
                mobile_phone.set_number(mobile_number)
            else:
                mobile_phone = Phone(self.book)
                mobile_phone.begin_edit()
                mobile_phone.set_type(PhoneType.MOBILE)
                mobile_phone.set_number(mobile_number)
                mobile_phone.set_contact(contact)
                mobile_phone.commit_edit()

        if any(contact.addresses):
            address = contact.addresses[0]
        else:
            address = Address(self.book)
        address.begin_edit()
        self.address_frame.copy_to_address(address)
        address.set_contact(contact)
        address.commit_edit()

    def run(self):
        self.show()

    @Gtk.Template.Callback()
    def on_finish(self, *args):
        gs_set_busy_cursor(None, True)
        be = self.book.get_backend()
        if be is not None:
            with be.add_lock():
                self.to_staff()
                self.staff.commit_edit()

        self.created_staff = self.staff
        self.staff = None
        gs_unset_busy_cursor(None)

    @Gtk.Template.Callback()
    def profile_page_enable_next(self, *args):
        model = self.display_name_combo.get_model()
        _iter = self.display_name_combo.get_active_iter()
        enable_next = True
        display_name = None
        if _iter is not None:
            display_name = model.get_value(_iter, 0)
        gs_widget_remove_style_context(self.full_name_entry, "error")
        self.full_name_entry.set_tooltip_text("")
        self.display_name_combo.set_tooltip_text("")
        gs_widget_remove_style_context(self.display_name_combo, "error")
        if display_name == "" or display_name is None:
            message = "Display name cannot be null."
            gs_widget_set_style_context(self.display_name_combo, "error")
            self.display_name_combo.set_tooltip_text(message)
            enable_next = False
        elif self.dialog_type == StaffDialogType.NEW:
            if Staff.lookup_display_name(Session.get_current_book(), display_name) is not None:
                message = "The staff with display name %s already exists" % display_name
                gs_widget_set_style_context(self.display_name_combo, "error")
                self.display_name_combo.set_tooltip_text(message)
                enable_next = False

            full_name = self.full_name_entry.get_text()
            if Staff.lookup_name(Session.get_current_book(), full_name) is not None:
                message = "The staff %s already exists" % full_name
                self.full_name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning")
                gs_widget_set_style_context(self.full_name_entry, "error")
                self.full_name_entry.set_tooltip_text(message)
                enable_next = False
            else:
                self.full_name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "emblem-default")
        gs_widget_remove_style_context(self.address_frame.get_label_widget(), "error")
        self.address_frame.street_entry.set_tooltip_text("")
        gs_widget_remove_style_context(self.address_frame.street_entry, "error")
        if not self.address_frame.validate():
            self.address_frame.street_entry.set_tooltip_text("Please fill in the address details")
            gs_widget_set_style_context(self.address_frame.get_label_widget(), "error")
            enable_next = False
        self.enable_page(enable_next)

    @Gtk.Template.Callback()
    def hire_information_enable_next(self, *args):
        enable_next = True
        self.location_selection.set_tooltip_text("")
        gs_widget_remove_style_context(self.location_frame.get_label_widget(), "error")
        if self.location_selection.get_ref() is None:
            self.location_selection.set_tooltip_text(
                "Please select or add a location Under Payment and Billing>Location")
            gs_widget_set_style_context(self.location_frame.get_label_widget(), "error")
            enable_next = False
        gs_widget_remove_style_context(self.salary_payment_method_frame.get_label_widget(), "error")
        if self.salary_payment_method.get_ref() is None:
            print("PAYMENT METHOD IS NONE TOO")
            self.salary_payment_method.set_tooltip_text(
                "Please select or add a payment method Under Payment and Billing>Payment Method")
            gs_widget_set_style_context(self.salary_payment_method_frame.get_label_widget(), "error")
            enable_next = False
        self.enable_page(enable_next)

    @Gtk.Template.Callback()
    def next_of_kin_page_enable_next(self, *args):
        enable_next = True
        gs_widget_remove_style_context(self.next_of_kin_full_name_entry, "error")
        self.next_of_kin_full_name_entry.set_tooltip_text("")
        full_name = self.next_of_kin_full_name_entry.get_text()
        if self.dialog_type == StaffDialogType.NEW:

            if full_name != "" and Contact.lookup_name(Session.get_current_book(), full_name) is not None:
                message = "The next of kin %s already exists" % full_name
                self.next_of_kin_full_name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning")
                gs_widget_set_style_context(self.next_of_kin_full_name_entry, "error")
                self.next_of_kin_full_name_entry.set_tooltip_text(message)
                enable_next = False
            else:
                self.next_of_kin_full_name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "emblem-default")
        gs_widget_remove_style_context(self.next_of_kin_address_frame.get_label_widget(), "error")
        self.next_of_kin_address_frame.street_entry.set_tooltip_text("")
        gs_widget_remove_style_context(self.next_of_kin_address_frame.street_entry, "error")
        if full_name != "" and not self.next_of_kin_address_frame.validate():
            self.next_of_kin_address_frame.street_entry.set_tooltip_text("Please fill in the address details")
            gs_widget_set_style_context(self.next_of_kin_address_frame.get_label_widget(), "error")
            enable_next = False
        self.enable_page(enable_next)

    def enable_page(self, ok):
        currentpagenum = self.get_current_page()
        currentpage = self.get_nth_page(currentpagenum)
        self.set_page_complete(currentpage, ok)

    @Gtk.Template.Callback()
    def on_cancel(self, *args):
        ComponentManager.close(self.component_id)

    @Gtk.Template.Callback()
    def display_name_changed(self, *args):
        model = self.display_name_combo.get_model()
        _iter = self.display_name_combo.get_active_iter()
        model.clear()
        human_name = HumanName(full_name=self.full_name_entry.get_text())

        for disp_name in sorted(set(
                map(str.strip,
                    ["{} {} {} {} {}".format(human_name.suffix, human_name.first,
                                             human_name.middle, human_name.last, human_name.suffix).replace("  ", " "),
                     "{} {}".format(human_name.first, human_name.last).replace("  ", " "),
                     "{} {}".format(human_name.last, human_name.first).replace("  ", " "),
                     "{} {} {}".format(human_name.middle, human_name.last, human_name.first).replace("  ", " ")
                     ]))):
            if len(disp_name) == 0:
                continue
            model.append([disp_name])
        self.display_name_combo.set_active(0)

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        inv = self.staff
        if inv is None:
            ComponentManager.close(self.component_id)
            return
        if changes:
            info = ComponentManager.get_entity_events(changes, inv.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(StaffDialog)
