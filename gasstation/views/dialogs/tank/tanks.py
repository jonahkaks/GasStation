from gasstation.utilities.cursors import *
from gasstation.utilities.custom_dialogs import *
from gasstation.views.tank import *
from libgasstation import Location
from libgasstation.core.session import Session
from .tank import TankDialog

DIALOG_TANKS_CM_CLASS = "dialog-tanks"
STATE_SECTION = "dialogs/edit_tanks"


class TanksDialog(GObject.Object):
    def __init__(self):
        GObject.Object.__init__(self)
        self.dialog = None
        self.session = None
        self.book = None
        self.tank_tree = None
        self.edit_button = None
        self.remove_button = None
        self.component_id = 0

    def window_destroy_cb(self, widget):
        ComponentManager.unregister(self.component_id)

    def edit_clicked(self):
        tanks = self.tank_tree.get_selected_tanks()
        if not any(tanks):
            return
        book = Session.get_current_book()
        for tank in tanks:
            TankDialog(self.dialog,book, tank=tank).run()
        ComponentManager.refresh_all()

    def row_activated_cb(self, view, path, column):
        self.edit_clicked()

    def remove_clicked(self):
        from gasstation.views.dialogs.object_references import ObjectReferencesDialog
        tanks = self.tank_tree.get_selected_tanks()
        if tanks is None or not any(tanks):
            return
        dialog = Gtk.MessageDialog(transient_for=self.dialog, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE,
                                   message_format="The following tanks will be deleted %s" % ",".join([tank.get_name() for tank in tanks]))
        dialog.add_buttons("Cancel", Gtk.ResponseType.CANCEL, "Delete", Gtk.ResponseType.ACCEPT)
        dialog.set_default_response(Gtk.ResponseType.CANCEL)
        response = dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            gs_set_busy_cursor(None, True)
            ComponentManager.suspend()
            for tank in tanks:
                lis = tank.get_referrers()
                if len(lis):
                    exp = "The list below shows objects which make use of the tank which you want to delete.\n" \
                          "Before you can delete it, you must either delete those objects or else modify them so they " \
                          "make use of another tank"
                    ObjectReferencesDialog(exp, lis)
                    continue
                tank.begin_edit()
                tank.destroy()
            ComponentManager.resume()
            gs_unset_busy_cursor(None)

    def add_clicked(self):
        dialog = TankDialog(self.dialog, Session.get_current_book())
        dialog.run()

    def dialog_response(self, dialog, response):
        if response == RESPONSE_NEW:
            self.add_clicked()
            return

        elif response == RESPONSE_DELETE:
            self.remove_clicked()
            return

        elif response == RESPONSE_EDIT:
            self.edit_clicked()
            return
        else:
            ComponentManager.close(self.component_id)

    def selection_changed(self, selection):
        inv = any(self.tank_tree.get_selected_tanks())
        self.edit_button.set_sensitive(inv)
        self.remove_button.set_sensitive(inv)

    def create(self, parent):
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/common.ui", ["common_dialog"])
        self.dialog = builder.get_object("common_dialog")
        self.dialog.set_title("Tanks Dialog")
        self.session = Session.get_current_session()
        self.book = self.session.get_book()
        gs_widget_set_style_context(self.dialog, "TanksDialog")
        builder.connect_signals(self)
        if parent is not None:
            self.dialog.set_transient_for(parent)
        self.remove_button = builder.get_object("remove_button")
        self.edit_button = builder.get_object("edit_button")
        scrolled_window = builder.get_object("common_list_window")
        view = TankView(state_section=STATE_SECTION)
        self.tank_tree = view
        scrolled_window.add(view)
        scrolled_window.show()
        view.set_headers_visible(True)
        selection = view.get_selection()
        self.tank_tree.reference_ids[selection].append(selection.connect("changed", self.selection_changed))
        self.tank_tree.reference_ids[self.tank_tree].append(self.tank_tree.connect("row-activated", self.row_activated_cb))

    def close_handler(self):
        self.dialog.destroy()

    def show_handler(self, klass):
        self.dialog.present()
        return True

    @classmethod
    def new(cls, parent):
        if ComponentManager.forall(DIALOG_TANKS_CM_CLASS, cls.show_handler):
            return
        if not any(Location.get_list(Session.get_current_book())):
            show_error(parent, "To add a tank you need to create some locations under Business>Location")
            return
        iv = GObject.new(cls)
        iv.create(parent)
        iv.component_id = ComponentManager.register(DIALOG_TANKS_CM_CLASS, None, iv.close_handler)
        ComponentManager.set_session(iv.component_id, iv.session)
        iv.tank_tree.grab_focus()
        iv.dialog.show()
GObject.type_register(TanksDialog)
