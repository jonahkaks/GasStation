from gasstation.utilities.dialog import *
from gasstation.views.dialogs.location.location import LocationDialog
from gasstation.views.dialogs.product import ProductDialog
from gasstation.views.selections import ReferenceSelection
from gasstation.views.selections.amount_edit import *
from libgasstation import Location
from libgasstation.core import Tank
from libgasstation.core._declbase import ID_PRODUCT
from libgasstation.core.component import *
from libgasstation.core.product import Product
from libgasstation.core.session import Session

DIALOG_NEW_INVENTORY_CM_CLASS = "dialog-new-tank"
DIALOG_EDIT_INVENTORY_CM_CLASS = "dialog-edit-tank"



class TankDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path="/org/jonah/Gasstation/gtkbuilder/fuel/tank.ui")
class TankDialog(Gtk.Dialog):
    __gtype_name__ = "TankDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()
    capacity_entry: Gtk.SpinButton = Gtk.Template.Child()
    height_entry: Gtk.SpinButton = Gtk.Template.Child()
    temperature_entry: Gtk.SpinButton = Gtk.Template.Child()
    density_entry: Gtk.SpinButton = Gtk.Template.Child()
    tank_grid: Gtk.Grid = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, tank=None, name=None):
        super().__init__()
        self.book = book
        self.dialog_type = TankDialogType.NEW if tank is None else TankDialogType.EDIT
        self.tank = tank
        self.set_title("Add/Edit Tank")
        if parent is not None:
            self.set_transient_for(parent)
        self.get_style_context().add_class("TankDialog")
        if not modal:
            self.connect("response", self.window_response_cb)
        else:
            self.set_modal(True)
        self.set_default_response(Gtk.ResponseType.OK)
        self.location = ReferenceSelection(Location, LocationDialog)
        self.tank_grid.attach(self.location, 1, 2, 1, 1)
        self.location.set_new_ability(True)
        self.location.show()

        self.product = ReferenceSelection(Product, ProductDialog)
        self.tank_grid.attach(self.product, 1, 1, 1, 1)
        self.product.set_new_ability(True)
        self.product.show()
        if name is not None:
            self.name_entry.set_text(name)
        gs_window_adjust_for_screen(self)
        self.component_id = ComponentManager.register(DIALOG_NEW_INVENTORY_CM_CLASS,
                                                      self.refresh_handler,
                                                      None if modal else self.close_handler)

        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id,
                                           ID_PRODUCT,
                                           EVENT_MODIFY | EVENT_DESTROY)
        self.show()
        if tank is not None:
            self.to_ui()

    def to_ui(self):
        tank = self.tank
        self.name_entry.set_text(tank.get_name())
        self.capacity_entry.set_value(tank.get_capacity())
        self.height_entry.set_value(tank.get_height())
        self.density_entry.set_value(tank.get_density())
        self.temperature_entry.set_value(tank.get_temperature())
        self.product.set_ref(tank.get_product())
        self.location.set_ref(tank.get_location())

    def to_tank(self):
        tank = self.tank
        if tank is None:
            self.tank = Tank(self.book)
            tank = self.tank
        name = self.name_entry.get_text()
        tank.begin_edit()
        tank.set_name(name)

        tank.set_capacity(self.capacity_entry.get_value())
        tank.set_height(self.height_entry.get_value())
        tank.set_density(self.density_entry.get_value())
        tank.set_temperature(self.temperature_entry.get_value())
        tank.set_product(self.product.get_ref())
        tank.set_location(self.location.get_ref())

    def finish_ok(self):
        self.to_tank()
        self.tank.commit_edit()
        self.close()
        ComponentManager.close(self.component_id)

    def common_ok(self):
        name = self.name_entry.get_text()
        if name == "":
            message = "The Tank must be given a name."
            show_error(self, message)
            return False
        loc = self.location.get_ref()
        if Tank.lookup_name_with_location(Session.get_current_book(), name, loc) \
                is not None and self.dialog_type == TankDialogType.NEW:
            show_error(self, "The tank %s already exists" % name)
            return False
        if loc is None:
            show_error(self, "The tank must belong to a business location")
            return False
        desc = self.capacity_entry.get_text()
        if desc == "":
            message = "The Tank must be given a capacity."
            show_error(self, message)
            return False
        return True

    def window_response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            if not self.common_ok():
                return False
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            dialog.close()

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        if self.tank is None:
            ComponentManager.close(self.component_id)
            return
        if changes:
            info = ComponentManager.get_entity_events(changes, self.tank.get_guid())
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(TankDialog)
