from gi import require_version

require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GLib


@Gtk.Template(resource_path='/org/jonah/Gasstation/about.ui')
class AboutDialog(Gtk.AboutDialog):
    """About dialog"""
    __gtype_name__ = 'AboutDialog'

    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        copyright = "Copyright \xc2\xa9 2016-2021 - Kafeero Augustine Jonah"
        version = self.get_version() + "\n<small>Running against GTK+ %d.%d.%d</small>" % (Gtk.get_major_version(),
                                                                                           Gtk.get_minor_version(),
                                                                                           Gtk.get_micro_version())
        self.set_modal(True)
        self.set_program_name(GLib.get_prgname())
        self.set_transient_for(parent)
        self.set_version(version)
        self.set_copyright(copyright)
        self.set_comments("Free, easy, personal accounting for gasstations")
        self.set_license_type(Gtk.License.LGPL_3_0)

    def __repr__(self):
        return '<AboutDialog>'


GObject.type_register(AboutDialog)
