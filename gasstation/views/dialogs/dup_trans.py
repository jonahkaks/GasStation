from gi import require_version

from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.views.selections.date_edit import DateSelection, gdate_get_time64

require_version('Gtk', '3.0')
from gi.repository import GObject, Gtk


def parse_num(string):
    if string is None or len(string) == 0:
        return False
    if not str(string).isdigit():
        return False
    number = int(string)
    return number


class DupTransDialog(GObject.Object):
    __gtype_name__ = "DupTransDialog"
    dialog = None
    focus_out = False
    date_edit = None
    num_edit = None
    tnum_edit = None
    assoc_edit = None
    duplicate_title_label = None
    duplicate_table = None
    date_label = None
    num_label = None
    tnum_label = None
    assoc_label = None

    def __init__(self):
        super().__init__(self)

    def output_cb(self, spinbutton):
        txt = spinbutton.get_chars(0, -1)
        num = parse_num(txt)
        is_number = bool(num)
        if not is_number:
            spinbutton.set_text("")
        return not is_number

    def create(self, parent, show_date, date, num_str, tnum_str):
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/plugin_page/register.ui",
                                          ["num_adjustment", "tnum_adjustment", "duplicate_transaction_dialog"])

        dialog = builder.get_object("duplicate_transaction_dialog")
        self.dialog = dialog
        gs_widget_set_style_context(dialog, "DupTransDialog")

        if parent is not None:
            dialog.set_transient_for(parent)

        self.date_label = builder.get_object("date_label")
        date_edit = DateSelection()
        if show_date:

            date_edit.set_date(date)
            hbox = builder.get_object("date_hbox")
            date_edit.show()
            date_edit.make_mnemonic_target(self.date_label)
            hbox.pack_end(date_edit, True, True, 0)
        self.date_edit = date_edit
        self.duplicate_title_label = builder.get_object("duplicate_title_label")
        self.duplicate_table = builder.get_object("duplicate_table")
        self.num_label = builder.get_object("num_label")
        self.tnum_label = builder.get_object("tnum_label")
        num_spin = builder.get_object("num_spin")
        tnum_spin = builder.get_object("tnum_spin")
        self.num_edit = num_spin
        self.tnum_edit = tnum_spin

        num_spin.set_activates_default(True)
        num_spin.connect("output", self.output_cb)
        tnum_spin.connect("output", self.output_cb)

        if num_str:
            num = parse_num(num_str)
            if bool(num):
                num_spin.set_value(num + 1)
            else:
                num_spin.set_text("")
        else:
            num_spin.set_text("")

        if tnum_str:
            tnum = parse_num(tnum_str)
            if bool(tnum):
                tnum_spin.set_value(tnum + 1)
            else:
                tnum_spin.set_text("")
        else:
            tnum_spin.set_text("")
        self.assoc_label = builder.get_object("assoc_label")
        self.assoc_edit = builder.get_object("assoc_check_button")
        builder.connect_signals(self)

    @classmethod
    def internal(cls, parent, title, show_date, date_p, gdate_p, num, out_num, tnum, out_tnum, tassoc, out_tassoc):
        self = GObject.new(cls)
        self.create(parent, show_date, date_p, num, tnum)
        if not show_date:
            self.date_label.set_visible(False)
            if self.date_edit:
                self.date_edit.set_visible(False)
            if out_num is not None:
                self.num_edit.grab_focus()
        else:
            gde = self.date_edit
            entry = gde.date_entry
            entry.grab_focus()
        if title:
            full_text = "<b>%s</b>" % title
            self.duplicate_title_label.set_markup(full_text)
        if out_num is None:
            self.num_label.set_visible(False)
            self.num_edit.set_visible(False)
        if tnum is None:
            self.tnum_label.set_visible(False)
            self.tnum_edit.set_visible(False)
        if not show_date and not tnum or tnum is None:
            self.num_label.set_markup("Action/Number:")
        if tnum and tnum is not None:
            self.num_edit.set_activates_default(False)
            self.tnum_edit.set_activates_default(True)

        if tassoc and tassoc is not None:
            self.assoc_label.set_visible(True)
            self.assoc_edit.set_visible(True)
        else:
            self.assoc_label.set_visible(False)
            self.assoc_edit.set_visible(False)
        result = self.dialog.run()

        if result == Gtk.ResponseType.OK:
            date_p = self.date_edit.get_date()
            if gdate_p is not None:
                gdate_p = self.date_edit.get_gdate()
            if out_num is not None:
                out_num = self.num_edit.get_text()
            if tnum is not None:
                out_tnum = self.tnum_edit.get_text()
            if tassoc:
                if self.assoc_edit.get_active():
                    out_tassoc = tassoc
            ok = True
        else:
            ok = False
        self.dialog.destroy()
        return ok, date_p, gdate_p, out_num, out_tnum, out_tassoc

    @classmethod
    def trans_dialog(cls, parent, title, show_date, date_p, num, out_num, tnum, out_tnum, tassoc, out_tassoc):
        return cls.internal(parent, title, show_date, date_p, None, num, out_num, tnum, out_tnum, tassoc, out_tassoc)

    @classmethod
    def trans_dialog_gdate(cls, parent, gdate_p, num, out_num):
        tmp_time = gdate_get_time64(gdate_p)
        return cls.internal(parent, None, True, tmp_time, gdate_p, num, out_num, None, None, None, None)

    @classmethod
    def date_dialog(cls, parent, title, gdate_p):
        tmp_time = gdate_get_time64(gdate_p)
        return cls.internal(parent, title, True, tmp_time, gdate_p, None, None, None, None, None, None)


GObject.type_register(DupTransDialog)
