#
#
# #include <config.h>
#
# #include <gtk/gtk.h>
# #include <glib/gi18n.h>
#
# #include "dialog-utils.h"
# #include "gnc-component-manager.h"
# #include "gnc-ui-util.h"
# #include "gnc-ui.h"
# #include "gnc-gui-query.h"
# #include "gnc-query-view.h"
# #include "gnc-prefs.h"
# #include "gnc-session.h"
# #include "qof.h"
# #include "engine-helpers.h"
# #include "qofbookslots.h"
#
# #include "Transaction.h"
#
# #include "dialog-search.h"
# #include "search-core-type.h"
# #include "search-param.h"
#
#
# # static QofLogModule log_module = G_LOG_DOMAIN;
#
# #define DIALOG_SEARCH_CM_CLASS "dialog-search"
# #define GNC_PREFS_GROUP_SEARCH_GENERAL "dialogs.search"
# #define GNC_PREF_NEW_SEARCH_LIMIT  "new-search-limit"
# #define GNC_PREF_ACTIVE_ONLY       "search-for-active-only"
#
# typedef enum {
#     GNC_SEARCH_MATCH_ALL = 0,
#                            GNC_SEARCH_MATCH_ANY = 1
# } GNCSearchType;
#
# enum search_cols {
#     SEARCH_COL_NAME = 0,
#                       SEARCH_COL_POINTER,
#                       NUM_SEARCH_COLS
# };
#
# struct _GNCSearchWindow {
#     GtkWidget *dialog;
# GtkWidget *grouping_combo;
# GtkWidget *match_all_label;
# GtkWidget *criteria_table;
# GtkWidget *criteria_scroll_window;
# GtkWidget *result_hbox;
#
#
# GtkWidget *result_view;
#
#
# GtkWidget *new_rb;
# GtkWidget *narrow_rb;
# GtkWidget *add_rb;
# GtkWidget *del_rb;
# GtkWidget *active_only_check;
#
#
# GtkWidget *select_button;
# GList *button_list;
#
#
# GtkWidget *close_button;
# GtkWidget *cancel_button;
#
#
# GNCSearchResultCB result_cb;
# GNCSearchNewItemCB new_item_cb;
# GNCSearchCallbackButton *buttons;
# GNCSearchFree free_cb;
# gpointer user_data;
#
# GNCSearchSelectedCB selected_cb;
# gpointer select_arg;
# gboolean allow_clear;
#
#
# const gchar *type_label;
# QofIdTypeConst search_for;
# GNCSearchType grouping;
# const QofParam *get_guid;
# int search_type;
#
#
# QofQuery *q;
# QofQuery *start_q;
#
#
# GNCSearchParam *last_param;
# GList *params_list;
# GList *display_list;
# gint num_cols;
# GList *crit_list;
#
# gint component_id;
# const gchar *prefs_group;
# };
#
# struct _crit_data {
#     GNCSearchParam *param;
# GNCSearchCoreType *element;
# GtkWidget *elemwidget;
# GtkWidget *container;
# GtkWidget *button;
# GtkDialog *dialog;
# };
#
# static void search_clear_criteria(GNCSearchWindow *sw);
#
# static void gnc_search_dialog_display_results(GNCSearchWindow *sw);
#
# static void
# gnc_search_callback_button_execute(GNCSearchCallbackButton *cb,
#                                    GNCSearchWindow *sw) {
#     GNCQueryView *qview = GNC_QUERY_VIEW(sw->result_view);
#
#
# g_assert(qview);
#
#
# if (cb->cb_multiselect_fn && (!cb->cb_fcn)) {
#     GList *entries = gnc_query_view_get_selected_entry_list(qview);
#
# (cb->cb_multiselect_fn)(GTK_WINDOW (sw->dialog), entries, sw->user_data);
# g_list_free(entries);
# } else {
#
#     gpointer entry = gnc_query_view_get_selected_entry(qview);
# if (cb->cb_fcn)
# (cb->cb_fcn)(GTK_WINDOW (sw->dialog), &entry, sw->user_data);
# }
# }
#
# static void
# gnc_search_dialog_result_clicked(GtkButton *button, GNCSearchWindow *sw) {
#     GNCSearchCallbackButton *cb;
#
# cb = g_object_get_data(G_OBJECT (button), "data");
# gnc_search_callback_button_execute(cb, sw);
# }
#
# static void
# gnc_search_dialog_select_buttons_enable(GNCSearchWindow *sw, gint selected) {
#     gboolean enable, read_only;
# GList *blist;
#
# read_only = qof_book_is_readonly(gnc_get_current_book());
#
# for (blist = sw->button_list; blist; blist = blist->next) {
#     GNCSearchCallbackButton *button_spec = g_object_get_data(G_OBJECT(blist->data), "data");
#
# if (selected == 0) {
# gtk_widget_set_sensitive(GTK_WIDGET(blist->data), FALSE);
# continue;
# }
#
# if (read_only == TRUE) {
# if ((selected > 1) && (!(button_spec->cb_multiselect_fn == NULL)) &&
#         (button_spec->sensitive_if_readonly == TRUE))
# enable = TRUE;
# else
# enable = FALSE;
#
# if ((selected == 1) && (button_spec->sensitive_if_readonly == TRUE))
#     enable = TRUE;
# } else {
# if ((selected > 1) && (!(button_spec->cb_multiselect_fn == NULL)))
# enable = TRUE;
# else
# enable = FALSE;
#
# if (selected == 1)
#     enable = TRUE;
# }
# gtk_widget_set_sensitive(GTK_WIDGET(blist->data), enable);
# }
# }
#
# static void
# gnc_search_dialog_select_cb(GtkButton *button, GNCSearchWindow *sw) {
#     gpointer entry;
# g_return_if_fail (sw->selected_cb);
#
# entry = gnc_query_view_get_selected_entry(GNC_QUERY_VIEW (sw->result_view));
# if (!entry && !sw->allow_clear) {
# char *msg = _("You must select an item from the list");
# gnc_error_dialog(GTK_WINDOW (sw->dialog), "%s", msg);
# return;
# }
#
# (sw->selected_cb)(GTK_WINDOW (sw->dialog), entry, sw->select_arg);
# gnc_search_dialog_destroy(sw);
# }
#
# static void
# gnc_search_dialog_select_row_cb(GNCQueryView *qview,
#                                 gpointer item,
#                                          gpointer user_data) {
#     GNCSearchWindow *sw = user_data;
# gint number_of_rows = GPOINTER_TO_INT(item);
# gnc_search_dialog_select_buttons_enable(sw, number_of_rows);
# }
#
# static void
# gnc_search_dialog_double_click_cb(GNCQueryView *qview,
#                                   gpointer item,
#                                            gpointer user_data) {
#     GNCSearchWindow *sw = user_data;
#
# if (sw->selected_cb)
#
# gnc_search_dialog_select_cb(NULL, sw);
# else if (sw->buttons)
#
# gnc_search_callback_button_execute(sw->buttons, sw);
#
#
# }
#
# static void
# gnc_search_dialog_init_result_view(GNCSearchWindow *sw) {
# GtkTreeSelection *selection;
#
# sw->result_view = gnc_query_view_new(sw->display_list, sw->q);
#
#
# selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(sw->result_view));
# gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
#
#
# gnc_query_sort_order(GNC_QUERY_VIEW(sw->result_view), 1, GTK_SORT_ASCENDING);
#
#
# g_signal_connect (GNC_QUERY_VIEW(sw->result_view), "row_selected",
# G_CALLBACK(gnc_search_dialog_select_row_cb), sw);
#
# g_signal_connect (GNC_QUERY_VIEW(sw->result_view), "double_click_entry",
# G_CALLBACK(gnc_search_dialog_double_click_cb), sw);
# }
#
# static void
# gnc_search_dialog_display_results(GNCSearchWindow *sw) {
#     gdouble max_count;
#
#
# if (sw->result_view == NULL) {
# GtkWidget *scroller, *frame, *button_box, *button;
#
#
# gnc_search_dialog_init_result_view(sw);
#
# frame = gtk_frame_new(NULL);
#
#
# scroller = gtk_scrolled_window_new(NULL, NULL);
# gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (scroller),
#                                GTK_POLICY_AUTOMATIC,
#                                GTK_POLICY_AUTOMATIC);
# gtk_widget_set_size_request(GTK_WIDGET(scroller), 300, 100);
# gtk_container_add(GTK_CONTAINER (scroller), sw->result_view);
# gtk_container_add(GTK_CONTAINER(frame), scroller);
#
#
# button_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
# gtk_box_set_homogeneous(GTK_BOX (button_box), FALSE);
#
#
# if (sw->buttons) {
# int i;
#
# button = gtk_button_new_with_label(_("Select"));
# g_signal_connect (G_OBJECT(button), "clicked",
# G_CALLBACK(gnc_search_dialog_select_cb), sw);
# gtk_box_pack_start(GTK_BOX (button_box), button, FALSE, FALSE, 3);
# sw->select_button = button;
#
# for (i = 0; sw->buttons[i].label; i++) {
# GNCSearchCallbackButton *button_spec = sw->buttons + i;
# button = gtk_button_new_with_label(_(button_spec->label));
# g_object_set_data(G_OBJECT (button), "data", button_spec);
#
# if (qof_book_is_readonly(gnc_get_current_book()))
# gtk_widget_set_sensitive(GTK_WIDGET(button), button_spec->sensitive_if_readonly);
#
#
# sw->button_list = g_list_append(sw->button_list, button);
#
# g_signal_connect (G_OBJECT(button), "clicked",
# G_CALLBACK(gnc_search_dialog_result_clicked), sw);
# gtk_box_pack_start(GTK_BOX (button_box), button, FALSE, FALSE, 3);
# }
# }
#
#
# gtk_box_pack_end(GTK_BOX (sw->result_hbox), button_box, FALSE, FALSE, 3);
# gtk_box_pack_end(GTK_BOX (sw->result_hbox), frame, TRUE, TRUE, 3);
#
#
# gtk_widget_show_all(sw->result_hbox);
#
#
# if (!sw->selected_cb)
#     gtk_widget_hide(sw->select_button);
# } else
#
# gnc_query_view_reset_query(GNC_QUERY_VIEW(sw->result_view), sw->q);
#
#
# gnc_search_dialog_select_buttons_enable(sw, 0);
# gnc_query_view_unselect_all(GNC_QUERY_VIEW(sw->result_view));
#
#
# max_count = gnc_prefs_get_float(GNC_PREFS_GROUP_SEARCH_GENERAL, GNC_PREF_NEW_SEARCH_LIMIT);
# if (gnc_query_view_get_num_entries(GNC_QUERY_VIEW(sw->result_view)) < max_count)
# gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON (sw->new_rb), TRUE);
#
#
# if (gnc_query_view_get_num_entries(GNC_QUERY_VIEW(sw->result_view)) == 1) {
# GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(sw->result_view));
# GtkTreePath *path = gtk_tree_path_new_first();
# gtk_tree_selection_select_path(selection, path);
# gtk_tree_path_free(path);
# }
# }
#
# static void
# match_combo_changed(GtkComboBoxText *combo_box, GNCSearchWindow *sw) {
#     sw->grouping = gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box));
# }
#
# static void
# search_type_cb(GtkToggleButton *button, GNCSearchWindow *sw) {
#     GSList *buttongroup = gtk_radio_button_get_group(GTK_RADIO_BUTTON(button));
#
# if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (button))) {
# sw->search_type =
# g_slist_length(buttongroup) - g_slist_index(buttongroup, button) - 1;
# }
# }
#
# static void
# search_active_only_cb(GtkToggleButton *button, GNCSearchWindow *sw) {
#
#     gnc_prefs_set_bool(sw->prefs_group, GNC_PREF_ACTIVE_ONLY,
# gtk_toggle_button_get_active(button));
# }
#
# static QofQuery *
#        create_query_fragment(QofIdTypeConst search_for, GNCSearchParam *param, QofQueryPredData *pdata) {
#     GNCSearchParamKind kind = gnc_search_param_get_kind(param);
# QofQuery *q = qof_query_create_for(search_for);
#
# if (kind == SEARCH_PARAM_ELEM) {
#
# qof_query_add_term(q, gnc_search_param_get_param_path(GNC_SEARCH_PARAM_SIMPLE (param)),
#                    pdata, QOF_QUERY_OR);
# } else {
# GList *plist = gnc_search_param_get_search(GNC_SEARCH_PARAM_COMPOUND (param));
#
# for (; plist; plist = plist->next) {
#     QofQuery *new_q;
# GNCSearchParam *param2 = plist->data;
# QofQuery *q2 = create_query_fragment(search_for, param2,
# qof_query_core_predicate_copy(pdata));
# new_q = qof_query_merge(q, q2, kind == SEARCH_PARAM_ANY ?
# QOF_QUERY_OR : QOF_QUERY_AND);
# qof_query_destroy(q);
# qof_query_destroy(q2);
# q = new_q;
# }
# qof_query_core_predicate_free(pdata);
# }
# return q;
# }
#
# static void
# search_update_query(GNCSearchWindow *sw) {
# static GSList *active_params = NULL;
# QofQuery *q, *q2, *new_q;
# GList *node;
# QofQueryOp op;
#
# if (sw->grouping == GNC_SEARCH_MATCH_ANY)
#     op = QOF_QUERY_OR;
# else
#     op = QOF_QUERY_AND;
#
# if (active_params == NULL)
#     active_params = g_slist_prepend(NULL, QOF_PARAM_ACTIVE);
#
#
# if (sw->start_q == NULL) {
# sw->start_q = qof_query_create_for(sw->search_for);
# qof_query_set_book(sw->start_q, gnc_get_current_book());
# } else {
#
# qof_query_purge_terms(sw->start_q, active_params);
# }
#
#
# q = qof_query_create_for(sw->search_for);
#
#
# for (node = sw->crit_list; node; node = node->next) {
#     struct _crit_data *data = node->data;
# QofQueryPredData *pdata;
#
# pdata = gnc_search_core_type_get_predicate(data->element);
# if (pdata) {
# q2 = create_query_fragment(sw->search_for, GNC_SEARCH_PARAM (data->param), pdata);
# q = qof_query_merge(q, q2, op);
# }
# }
#
#
# switch (sw->search_type) {
#     case 0:
# new_q = qof_query_merge(sw->start_q, q, QOF_QUERY_AND);
# qof_query_destroy(q);
# break;
# case 1:
# new_q = qof_query_merge(sw->q, q, QOF_QUERY_AND);
# qof_query_destroy(q);
# break;
# case 2:
# new_q = qof_query_merge(sw->q, q, QOF_QUERY_OR);
# qof_query_destroy(q);
# break;
# case 3:
# q2 = qof_query_invert(q);
# new_q = qof_query_merge(sw->q, q2, QOF_QUERY_AND);
# qof_query_destroy(q2);
# qof_query_destroy(q);
# break;
# default:
# g_warning ("bad search type: %d", sw->search_type);
# new_q = q;
# break;
# }
#
# if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (sw->active_only_check))) {
# qof_query_add_boolean_match(new_q, active_params, TRUE, QOF_QUERY_AND);
# active_params = NULL;
# }
#
#
# if (sw->q)
# qof_query_destroy(sw->q);
#
#
# sw->q = new_q;
# }
#
# static void
# gnc_search_dialog_show_close_cancel(GNCSearchWindow *sw) {
# if (sw->selected_cb) {
# gtk_widget_show(sw->cancel_button);
# gtk_widget_hide(sw->close_button);
# } else {
# gtk_widget_hide(sw->cancel_button);
# gtk_widget_show(sw->close_button);
# }
# }
#
# static void
# gnc_search_dialog_reset_widgets(GNCSearchWindow *sw) {
#     gboolean sens = (sw->q != NULL);
# gboolean crit_list_vis = FALSE;
#
# gtk_widget_set_sensitive(GTK_WIDGET(sw->narrow_rb), sens);
# gtk_widget_set_sensitive(GTK_WIDGET(sw->add_rb), sens);
# gtk_widget_set_sensitive(GTK_WIDGET(sw->del_rb), sens);
#
# if (sw->q) {
# gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON (sw->new_rb), FALSE);
# gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON (sw->narrow_rb), TRUE);
# }
#
# if (sw->crit_list)
# crit_list_vis = TRUE;
#
# gtk_widget_set_sensitive(sw->grouping_combo, crit_list_vis);
# gtk_widget_set_visible(sw->criteria_scroll_window, crit_list_vis);
# gtk_widget_set_visible(sw->match_all_label, !crit_list_vis);
# }
#
# static gboolean
# gnc_search_dialog_crit_ok(GNCSearchWindow *sw) {
# struct _crit_data *data;
# GList *l;
# gboolean ret;
#
# if (!sw->crit_list)
# return TRUE;
#
# l = g_list_last(sw->crit_list);
# data = l->data;
# ret = gnc_search_core_type_validate(data->element);
#
# if (ret)
#     sw->last_param = data->param;
#
# return ret;
# }
#
# static void
# search_find_cb(GtkButton *button, GNCSearchWindow *sw) {
# if (!gnc_search_dialog_crit_ok(sw))
# return;
#
# search_update_query(sw);
# search_clear_criteria(sw);
# gnc_search_dialog_reset_widgets(sw);
#
# if (sw->result_cb) {
# gpointer entry = NULL;
# if (sw->result_view) {
# GNCQueryView *qview = GNC_QUERY_VIEW (sw->result_view);
# entry = gnc_query_view_get_selected_entry(qview);
# }
# (sw->result_cb)(sw->q, sw->user_data, &entry);
# } else
# gnc_search_dialog_display_results(sw);
# }
#
# static void
# search_new_item_cb(GtkButton *button, GNCSearchWindow *sw) {
# gpointer res;
#
# g_return_if_fail (sw->new_item_cb);
#
# res = (sw->new_item_cb)(GTK_WINDOW (sw->dialog), sw->user_data);
#
# if (res) {
# const GncGUID *guid = (const GncGUID *) ((sw->get_guid->param_getfcn)(res, sw->get_guid));
# QofQueryOp op = QOF_QUERY_OR;
#
# if (!sw->q) {
# if (!sw->start_q) {
# sw->start_q = qof_query_create_for(sw->search_for);
# qof_query_set_book(sw->start_q, gnc_get_current_book());
# }
# sw->q = qof_query_copy(sw->start_q);
# op = QOF_QUERY_AND;
# }
#
# qof_query_add_guid_match(sw->q, g_slist_prepend(NULL, QOF_PARAM_GUID),
# guid, op);
#
#
# gnc_gui_component_watch_entity(sw->component_id, guid, QOF_EVENT_MODIFY);
# }
# }
#
# static void
# search_cancel_cb(GtkButton *button, GNCSearchWindow *sw) {
#
# gnc_search_dialog_destroy(sw);
# }
#
# static void
# search_help_cb(GtkButton *button, GNCSearchWindow *sw) {
# gnc_gnome_help(GTK_WINDOW(sw->dialog), HF_HELP, HL_FIND_TRANSACTIONS);
# }
#
# static void
# remove_element(GtkWidget *button, GNCSearchWindow *sw) {
# GtkWidget *element;
# struct _elem_data *data;
#
# if (!sw->crit_list)
#     return;
#
# element = g_object_get_data(G_OBJECT (button), "element");
# data = g_object_get_data(G_OBJECT (element), "data");
#
#
# sw->crit_list = g_list_remove(sw->crit_list, data);
#
#
# gtk_container_remove(GTK_CONTAINER (sw->criteria_table), element);
# gtk_container_remove(GTK_CONTAINER (sw->criteria_table), button);
#
#
# if (!sw->crit_list) {
# gtk_widget_set_sensitive(sw->grouping_combo, FALSE);
# gtk_widget_show(sw->match_all_label);
# gtk_widget_hide(sw->criteria_scroll_window);
# }
# }
#
# static void
# attach_element(GtkWidget *element, GNCSearchWindow *sw, int row) {
# GtkWidget *remove;
# struct _crit_data *data;
#
# data = g_object_get_data(G_OBJECT (element), "data");
#
# gnc_search_core_type_pass_parent(data->element, GTK_WINDOW(sw->dialog));
#
# gtk_grid_attach(GTK_GRID (sw->criteria_table), element, 0, row, 1, 1);
# gtk_widget_set_hexpand(element, TRUE);
# gtk_widget_set_halign(element, GTK_ALIGN_FILL);
# g_object_set(element, "margin", 0, NULL);
#
# remove = gtk_button_new_with_mnemonic(_("_Remove"));
# g_object_set_data(G_OBJECT (remove), "element", element);
# g_signal_connect (G_OBJECT(remove), "clicked", G_CALLBACK(remove_element), sw);
#
# gtk_grid_attach(GTK_GRID (sw->criteria_table), remove, 1, row, 1, 1);
# gtk_widget_set_hexpand(remove, FALSE);
# gtk_widget_set_halign(remove, GTK_ALIGN_CENTER);
# g_object_set(remove, "margin", 0, NULL);
#
# gtk_widget_show(remove);
# data->button = remove;
# }
#
# static void
# combo_box_changed(GtkComboBox *combo_box, struct _crit_data *data) {
# GNCSearchParam *param;
# GNCSearchCoreType *newelem;
# GtkTreeModel *model;
# GtkTreeIter iter;
#
# if (!gtk_combo_box_get_active_iter(combo_box, &iter))
# return;
# model = gtk_combo_box_get_model(combo_box);
# gtk_tree_model_get(model, &iter, SEARCH_COL_POINTER, &param, -1);
#
# if (gnc_search_param_type_match(param, data->param)) {
#
# data->param = param;
# return;
# }
# data->param = param;
#
#
# if (data->elemwidget)
# gtk_container_remove(GTK_CONTAINER (data->container), data->elemwidget);
# g_object_unref(G_OBJECT (data->element));
#
# newelem = gnc_search_core_type_new_type_name \
# (gnc_search_param_get_param_type(param));
# data->element = newelem;
# data->elemwidget = gnc_search_core_type_get_widget(newelem);
# if (data->elemwidget) {
# gtk_box_pack_start(GTK_BOX (data->container), data->elemwidget,
#                                                     FALSE, FALSE, 0);
# }
#
# gnc_search_core_type_pass_parent(data->element, GTK_WINDOW(data->dialog));
#
#
# gtk_widget_show_all(data->container);
#
#
# gtk_widget_queue_resize(GTK_WIDGET (data->dialog));
#
#
# gnc_search_core_type_grab_focus(newelem);
# gnc_search_core_type_editable_enters(newelem);
# }
#
# static void
# search_clear_criteria(GNCSearchWindow *sw) {
#     GList *node;
#
# for (node = sw->crit_list; node;) {
# GList *tmp = node->next;
# struct _crit_data *data = node->data;
# g_object_ref (data->button);
# remove_element(data->button, sw);
# node = tmp;
# }
# }
#
# static GtkWidget *
#        get_comb_box_widget(GNCSearchWindow *sw, struct _crit_data *data) {
#     GtkWidget *combo_box;
# GtkListStore *store;
# GtkTreeIter iter;
# GtkCellRenderer *cell;
# GList *l;
# int index = 0, current = 0;
#
# store = gtk_list_store_new(NUM_SEARCH_COLS, G_TYPE_STRING, G_TYPE_POINTER);
# combo_box = gtk_combo_box_new_with_model(GTK_TREE_MODEL(store));
# g_object_unref(store);
#
# cell = gtk_cell_renderer_text_new();
# gtk_cell_layout_pack_start(GTK_CELL_LAYOUT (combo_box), cell, TRUE);
# gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT (combo_box), cell,
# "text", SEARCH_COL_NAME,
# NULL);
#
# for (l = sw->params_list; l; l = l->next) {
# GNCSearchParam *param = l->data;
#
# gtk_list_store_append(store, &iter);
# gtk_list_store_set(store, &iter,
# SEARCH_COL_NAME, _(param->title),
# SEARCH_COL_POINTER, param,
# -1);
#
# if (param == sw->last_param)
#     current = index;
#
# index++;
# }
#
# gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box), current);
# g_signal_connect (combo_box, "changed", G_CALLBACK(combo_box_changed), data);
#
# return combo_box;
# }
#
# static GtkWidget *
#        get_element_widget(GNCSearchWindow *sw, GNCSearchCoreType *element) {
# GtkWidget *combo_box, *hbox, *p;
# struct _crit_data *data;
#
# data = g_new0 (struct _crit_data, 1);
# data->element = element;
# data->dialog = GTK_DIALOG (sw->dialog);
#
# hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
# gtk_box_set_homogeneous(GTK_BOX (hbox), FALSE);
#
#
# g_object_set_data_full(G_OBJECT (hbox), "data", data, g_free);
#
# p = gnc_search_core_type_get_widget(element);
# data->elemwidget = p;
# data->container = hbox;
# data->param = sw->last_param;
#
# combo_box = get_comb_box_widget(sw, data);
# gtk_box_pack_start(GTK_BOX (hbox), combo_box, FALSE, FALSE, 0);
# if (p)
#     gtk_box_pack_start(GTK_BOX (hbox), p, FALSE, FALSE, 0);
# gtk_widget_show_all(hbox);
#
# return hbox;
# }
#
# static void
# gnc_search_dialog_book_option_changed(gpointer new_val, gpointer user_data) {
# GList *l;
# GNCSearchWindow *sw = user_data;
# gboolean *new_data = (gboolean *) new_val;
#
# GtkWidget *focused_widget = gtk_window_get_focus(GTK_WINDOW(sw->dialog));
#
# g_return_if_fail (sw);
# if (strcmp(sw->search_for, GNC_ID_SPLIT) != 0)
# return;
#
#
# for (l = sw->params_list; l; l = l->next) {
#     GNCSearchParam *param = l->data;
#
# if (*new_data) {
# if (strcmp(param->title, N_("Action")) == 0)
# gnc_search_param_set_title(param, N_("Number/Action"));
# if (strcmp(param->title, N_("Number")) == 0)
# gnc_search_param_set_title(param, N_("Transaction Number"));
# } else {
# if (strcmp(param->title, N_("Number/Action")) == 0)
# gnc_search_param_set_title(param, N_("Action"));
# if (strcmp(param->title, N_("Transaction Number")) == 0)
# gnc_search_param_set_title(param, N_("Number"));
# }
# }
#
# for (l = sw->crit_list; l; l = l->next) {
#     struct _crit_data *data = l->data;
# GList *children;
#
#
# for (children = gtk_container_get_children(GTK_CONTAINER(data->container));
# children; children = children->next) {
# GtkWidget *combo_box = children->data;
#
#
# if (GTK_IS_COMBO_BOX(combo_box)) {
# GtkWidget *new_combo_box;
# gint index;
#
#
# index = gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box));
#
# new_combo_box = get_comb_box_widget(sw, data);
#
# if (focused_widget == combo_box)
# focused_widget = new_combo_box;
# gtk_widget_destroy(combo_box);
#
# gtk_combo_box_set_active(GTK_COMBO_BOX(new_combo_box), index);
# gtk_box_pack_start(GTK_BOX (data->container), new_combo_box,
# FALSE, FALSE, 0);
# gtk_box_reorder_child(GTK_BOX (data->container), new_combo_box, 0);
# gtk_widget_show_all(data->container);
# }
# }
# }
# gtk_widget_grab_focus(focused_widget);
# }
#
# struct grid_size {
#
# GtkGrid *grid;
#
# gint cols, rows;
# };
#
# static void
# get_grid_size(GtkWidget *child, gpointer data) {
# struct grid_size *gridsize = data;
# gint top, left, height, width;
#
# gtk_container_child_get(GTK_CONTAINER(gridsize->grid), child,
#                                                        "left-attach", &left,
#                                                                        "top-attach", &top,
#                                                                                       "height", &height,
#                                                                                                  "width", &width,
#                                                                                                            NULL);
#
# if (left + width >= gridsize->cols)
#     gridsize->cols = left + width;
#
# if (top + height >= gridsize->rows)
#     gridsize->rows = top + height;
# }
#
# static void
# gnc_search_dialog_add_criterion(GNCSearchWindow *sw) {
# GNCSearchCoreType *new_sct;
# struct grid_size gridsize;
#
# gridsize.cols = 0;
# gridsize.rows = 0;
#
#
# if (sw->crit_list) {
# if (!gnc_search_dialog_crit_ok(sw))
# return;
# } else {
# sw->last_param = sw->params_list->data;
#
#
# gtk_widget_set_sensitive(sw->grouping_combo, TRUE);
# gtk_widget_hide(sw->match_all_label);
# gtk_widget_show(sw->criteria_scroll_window);
# }
#
# new_sct = gnc_search_core_type_new_type_name
# (gnc_search_param_get_param_type(sw->last_param));
#
# if (new_sct) {
# struct _crit_data *data;
# GtkWidget *w;
#
# w = get_element_widget(sw, new_sct);
# data = g_object_get_data(G_OBJECT (w), "data");
# sw->crit_list = g_list_append(sw->crit_list, data);
#
# gridsize.grid = GTK_GRID (sw->criteria_table);
# gtk_container_foreach(GTK_CONTAINER(sw->criteria_table), get_grid_size, &gridsize);
#
# attach_element(w, sw, gridsize.rows);
#
# gnc_search_core_type_grab_focus(new_sct);
# gnc_search_core_type_editable_enters(new_sct);
# }
# }
#
# static void
# add_criterion(GtkWidget *button, GNCSearchWindow *sw) {
#     gint number_of_buttons = g_list_length(sw->crit_list) + 1;
# gint button_height = gtk_widget_get_allocated_height(button);
# gint min_height = MIN (number_of_buttons * button_height, 5 * button_height);
#
#
# gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(
#     sw->criteria_scroll_window),
# min_height + (button_height / 2));
#
# gnc_search_dialog_add_criterion(sw);
# }
#
# static int
# gnc_search_dialog_close_cb(GtkDialog *dialog, GNCSearchWindow *sw) {
#     g_return_val_if_fail (sw, TRUE);
#
#
# if (strcmp(sw->search_for, GNC_ID_SPLIT) == 0)
# gnc_book_option_remove_cb(OPTION_NAME_NUM_FIELD_SOURCE,
# gnc_search_dialog_book_option_changed, sw);
#
# gnc_unregister_gui_component(sw->component_id);
#
#
# g_list_free(sw->crit_list);
#
#
# g_list_free(sw->button_list);
#
#
# if (sw->q) qof_query_destroy(sw->q);
# if (sw->start_q) qof_query_destroy(sw->start_q);
#
#
# if (sw->free_cb)
# (sw->free_cb)(sw->user_data);
#
#
# g_free(sw);
# return FALSE;
# }
#
# static void
# refresh_handler(GHashTable *changes, gpointer data) {
# GNCSearchWindow *sw = data;
#
# g_return_if_fail (sw);
#
# if (!sw->result_cb && (sw->result_view != NULL))
# gnc_search_dialog_display_results(sw);
# }
#
# static void
# close_handler(gpointer data) {
# GNCSearchWindow *sw = data;
#
# g_return_if_fail (sw);
# gtk_widget_destroy(sw->dialog);
#
# }
#
# static const gchar *
#              type_label_to_new_button(const gchar *type_label) {
# if (g_strcmp0(type_label, _("Bill")) == 0) {
# return _("New Bill");
# } else if (g_strcmp0(type_label, _("Customer")) == 0) {
# return _("New Customer");
# } else if (g_strcmp0(type_label, _("Staff")) == 0) {
# return _("New Staff");
# } else if (g_strcmp0(type_label, _("Expense Voucher")) == 0) {
# return _("New Expense Voucher");
# } else if (g_strcmp0(type_label, _("Invoice")) == 0) {
# return _("New Invoice");
# } else if (g_strcmp0(type_label, _("Job")) == 0) {
# return _("New Job");
# } else if (g_strcmp0(type_label, _("Order")) == 0) {
# return _("New Order");
# } else if (g_strcmp0(type_label, _("Transaction")) == 0) {
# return _("New Transaction");
# } else if (g_strcmp0(type_label, _("Split")) == 0) {
# return _("New Split");
# } else if (g_strcmp0(type_label, _("Vendor")) == 0) {
# return _("New Vendor");
# } else {
# PWARN("No translatable new-button label found for search type \"%s\", please add one into dialog-search.c!",
#       type_label);
# return C_(
#     "Item represents an unknown object type (in the sense of bill, customer, invoice, transaction, split,...)!",
#     "New item");
# }
# }
#
# static void
# gnc_search_dialog_init_widgets(GNCSearchWindow *sw, const gchar *title) {
#     GtkBuilder *builder;
# GtkWidget *label, *add, *box;
# GtkComboBoxText *combo_box;
# GtkWidget *widget;
# GtkWidget *new_item_button;
# const char *type_label;
# gboolean active;
#
# builder = gtk_builder_new();
# gnc_builder_add_from_file(builder, "dialog-search.glade", "search_dialog");
#
#
# sw->dialog = GTK_WIDGET(gtk_builder_get_object(builder, "search_dialog"));
# gtk_window_set_title(GTK_WINDOW(sw->dialog), title);
# g_object_set_data(G_OBJECT (sw->dialog), "dialog-info", sw);
#
#
# gtk_widget_set_name(GTK_WIDGET(sw->dialog), "gnc-id-search");
# gnc_widget_style_context_add_class(GTK_WIDGET(sw->dialog), "gnc-class-search");
#
#
# sw->result_hbox = GTK_WIDGET(gtk_builder_get_object(builder, "result_hbox"));
#
#
# sw->criteria_table = GTK_WIDGET(gtk_builder_get_object(builder, "criteria_table"));
# sw->criteria_scroll_window = GTK_WIDGET(gtk_builder_get_object(builder, "criteria_scroll_window"));
#
#
# label = GTK_WIDGET(gtk_builder_get_object(builder, "type_label"));
# if (sw->type_label)
# type_label = sw->type_label;
# else
# type_label = _(qof_object_get_type_label(sw->search_for));
# gtk_label_set_text(GTK_LABEL (label), type_label);
#
#
# add = gtk_button_new_with_mnemonic(_("_Add"));
#
# g_signal_connect (G_OBJECT(add), "clicked", G_CALLBACK(add_criterion), sw);
# box = GTK_WIDGET(gtk_builder_get_object(builder, "add_button_box"));
# gtk_box_pack_start(GTK_BOX (box), add, FALSE, FALSE, 3);
# gtk_widget_show(add);
#
#
# sw->grouping_combo = gtk_combo_box_text_new();
# combo_box = GTK_COMBO_BOX_TEXT(sw->grouping_combo);
# gtk_combo_box_text_append_text(combo_box, _("all criteria are met"));
# gtk_combo_box_text_append_text(combo_box, _("any criteria are met"));
# gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box), sw->grouping);
# g_signal_connect(combo_box, "changed", G_CALLBACK(match_combo_changed), sw);
#
# box = GTK_WIDGET(gtk_builder_get_object(builder, "type_menu_box"));
# gtk_box_pack_start(GTK_BOX (box), GTK_WIDGET(combo_box), FALSE, FALSE, 3);
# gtk_widget_show(GTK_WIDGET(combo_box));
#
#
# sw->match_all_label = GTK_WIDGET(gtk_builder_get_object(builder, "match_all_label"));
#
#
# sw->new_rb = GTK_WIDGET(gtk_builder_get_object(builder, "new_search_radiobutton"));
# g_signal_connect (sw->new_rb, "toggled",
# G_CALLBACK(search_type_cb), sw);
# sw->narrow_rb = GTK_WIDGET(gtk_builder_get_object(builder, "narrow_search_radiobutton"));
# g_signal_connect (sw->narrow_rb, "toggled",
# G_CALLBACK(search_type_cb), sw);
# sw->add_rb = GTK_WIDGET(gtk_builder_get_object(builder, "add_search_radiobutton"));
# g_signal_connect (sw->add_rb, "toggled",
# G_CALLBACK(search_type_cb), sw);
# sw->del_rb = GTK_WIDGET(gtk_builder_get_object(builder, "delete_search_radiobutton"));
# g_signal_connect (sw->del_rb, "toggled",
# G_CALLBACK(search_type_cb), sw);
#
# active = gnc_prefs_get_bool(sw->prefs_group, GNC_PREF_ACTIVE_ONLY);
# sw->active_only_check = GTK_WIDGET(gtk_builder_get_object(builder, "active_only_check"));
# gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON (sw->active_only_check), active);
# g_signal_connect (sw->active_only_check, "toggled",
# G_CALLBACK(search_active_only_cb), sw);
#
#
# if (qof_class_get_parameter(sw->search_for, QOF_PARAM_ACTIVE) == NULL)
# gtk_widget_set_sensitive(sw->active_only_check, FALSE);
#
#
# widget = GTK_WIDGET(gtk_builder_get_object(builder, "find_button"));
# g_signal_connect (widget, "clicked",
# G_CALLBACK(search_find_cb), sw);
#
#
# sw->cancel_button = GTK_WIDGET(gtk_builder_get_object(builder, "cancel_button"));
# g_signal_connect (sw->cancel_button, "clicked",
# G_CALLBACK(search_cancel_cb), sw);
#
#
# sw->close_button = GTK_WIDGET(gtk_builder_get_object(builder, "close_button"));
# g_signal_connect (sw->close_button, "clicked",
# G_CALLBACK(search_cancel_cb), sw);
#
#
# new_item_button = GTK_WIDGET(gtk_builder_get_object(builder, "new_item_button"));
# gtk_button_set_label(GTK_BUTTON(new_item_button),
# type_label_to_new_button(type_label));
# g_signal_connect (new_item_button, "clicked",
# G_CALLBACK(search_new_item_cb), sw);
#
#
# widget = GTK_WIDGET(gtk_builder_get_object(builder, "help_button"));
# g_signal_connect (widget, "clicked",
# G_CALLBACK(search_help_cb), sw);
#
#
# gnc_search_dialog_add_criterion(sw);
#
#
# if (strcmp(sw->search_for, GNC_ID_SPLIT) == 0)
# gnc_book_option_register_cb(OPTION_NAME_NUM_FIELD_SOURCE,
# gnc_search_dialog_book_option_changed, sw);
#
#
# if (!sw->new_item_cb)
# gtk_widget_hide(new_item_button);
#
#
# gtk_builder_connect_signals(builder, sw);
#
#
# sw->component_id = gnc_register_gui_component(DIALOG_SEARCH_CM_CLASS,
# refresh_handler,
# close_handler, sw);
# gnc_gui_component_set_session(sw->component_id,
# gnc_get_current_session());
#
#
# g_signal_connect (G_OBJECT(sw->dialog), "destroy",
# G_CALLBACK(gnc_search_dialog_close_cb), sw);
#
# gnc_search_dialog_reset_widgets(sw);
# gnc_search_dialog_show_close_cancel(sw);
#
# g_object_unref(G_OBJECT(builder));
# }
#
# void
# gnc_search_dialog_destroy(GNCSearchWindow *sw) {
# if (!sw) return;
# if (sw->prefs_group)
#     gnc_save_window_size(sw->prefs_group, GTK_WINDOW(sw->dialog));
#     gnc_close_gui_component(sw->component_id);
#     }
#
#     void
#     gnc_search_dialog_raise(GNCSearchWindow *sw) {
#     if (!sw) return;
#     gtk_window_present(GTK_WINDOW(sw->dialog));
#     }
#
#     GNCSearchWindow *
#     gnc_search_dialog_create(GtkWindow *parent,
#     QofIdTypeConst obj_type, const gchar *title,
#     GList *param_list,
#     GList *display_list,
#     QofQuery *start_query, QofQuery *show_start_query,
#     GNCSearchCallbackButton *callbacks,
#     GNCSearchResultCB result_callback,
#     GNCSearchNewItemCB new_item_cb,
#     gpointer user_data, GNCSearchFree free_cb,
#     const gchar *prefs_group,
#     const gchar *type_label,
#     const gchar *style_class) {
# GNCSearchWindow *sw = g_new0 (GNCSearchWindow, 1);
#
# g_return_val_if_fail (obj_type, NULL);
# g_return_val_if_fail (*obj_type != '\0', NULL);
# g_return_val_if_fail (param_list, NULL);
#
#
# g_return_val_if_fail ((callbacks && !result_callback) ||
#                       (!callbacks && result_callback), NULL);
#
# if (callbacks)
#     g_return_val_if_fail (display_list, NULL);
#
# sw->search_for = obj_type;
# sw->params_list = param_list;
# sw->display_list = display_list;
# sw->buttons = callbacks;
# sw->result_cb = result_callback;
# sw->new_item_cb = new_item_cb;
# sw->user_data = user_data;
# sw->free_cb = free_cb;
# sw->prefs_group = prefs_group;
# sw->type_label = type_label;
#
#
# sw->get_guid = qof_class_get_parameter(sw->search_for, QOF_PARAM_GUID);
# if (start_query)
#     sw->start_q = qof_query_copy(start_query);
# sw->q = show_start_query;
#
# gnc_search_dialog_init_widgets(sw, title);
# if (sw->prefs_group)
#     gnc_restore_window_size(sw->prefs_group, GTK_WINDOW(sw->dialog), parent);
#     gtk_window_set_transient_for(GTK_WINDOW(sw->dialog), parent);
#     gtk_widget_show(sw->dialog);
#
#
#     gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(
#         sw->criteria_scroll_window),
#     gtk_widget_get_allocated_height(
#         GTK_WIDGET(sw->grouping_combo)) * 1.5);
#
#
#     if (style_class != NULL)
#         gnc_widget_style_context_add_class(GTK_WIDGET(sw->dialog), style_class);
#
#
#         if (callbacks && show_start_query) {
#         gnc_search_dialog_reset_widgets(sw);
#         gnc_search_dialog_display_results(sw);
#         }
#
#         return sw;
#     }
#
#
#     guint gnc_search_dialog_connect_on_close(GNCSearchWindow *sw,
#     GCallback func,
#     gpointer user_data) {
# g_return_val_if_fail (sw, 0);
# g_return_val_if_fail (func, 0);
# g_return_val_if_fail (user_data, 0);
#
# return g_signal_connect (G_OBJECT(sw->dialog), "destroy",
#                                                func, user_data);
#
# }
#
#
# void gnc_search_dialog_disconnect(GNCSearchWindow *sw, gpointer user_data) {
# g_return_if_fail (sw);
# g_return_if_fail (user_data);
#
# g_signal_handlers_disconnect_matched(sw->dialog, G_SIGNAL_MATCH_DATA,
#                                          0, 0, NULL, NULL, user_data);
# }
#
#
# void gnc_search_dialog_set_select_cb(GNCSearchWindow *sw,
# GNCSearchSelectedCB selected_cb,
# gpointer user_data,
# gboolean allow_clear) {
# g_return_if_fail (sw);
#
# sw->selected_cb = selected_cb;
# sw->select_arg = user_data;
# sw->allow_clear = allow_clear;
#
#
# if (sw->select_button) {
# if (selected_cb)
# gtk_widget_show(sw->select_button);
# else
# gtk_widget_hide(sw->select_button);
# }
#
#
# gnc_search_dialog_show_close_cancel(sw);
# }
#
#
# static GList *
#        get_params_list(QofIdTypeConst type) {
# GList *list = NULL;
#
# list = gnc_search_param_prepend(list, "Txn: All Accounts",
#                                 ACCOUNT_MATCH_ALL_TYPE,
#                                 type, SPLIT_TRANS, TRANS_SPLITLIST,
#                                 SPLIT_ACCOUNT_GUID, NULL);
# list = gnc_search_param_prepend(list, "Split Account", GNC_ID_ACCOUNT,
#                                 type, SPLIT_ACCOUNT, QOF_PARAM_GUID,
#                                 NULL);
# list = gnc_search_param_prepend(list, "Split->Txn->Void?", NULL, type,
#                                 SPLIT_TRANS, TRANS_VOID_STATUS, NULL);
# list = gnc_search_param_prepend(list, "Split Int64", NULL, type,
#                                 "d-share-int64", NULL);
# list = gnc_search_param_prepend(list, "Split Amount (double)", NULL, type,
#                                 "d-share-amount", NULL);
# list = gnc_search_param_prepend(list, "Split Value (debcred)", NULL, type,
#                                 SPLIT_VALUE, NULL);
# list = gnc_search_param_prepend(list, "Split Amount (numeric)", NULL, type,
#                                 SPLIT_AMOUNT, NULL);
# list = gnc_search_param_prepend(list, "Date Reconciled (date)", NULL, type,
#                                 SPLIT_DATE_RECONCILED, NULL);
# list = gnc_search_param_prepend(list, "Split Memo (string)", NULL, type,
#                                 SPLIT_MEMO, NULL);
#
# return list;
# }
#
# static GList *
#        get_display_list(QofIdTypeConst type) {
# GList *list = NULL;
#
# list = gnc_search_param_prepend(list, "Amount", NULL, type, SPLIT_AMOUNT,
#                                 NULL);
# list = gnc_search_param_prepend(list, "Memo", NULL, type, SPLIT_MEMO, NULL);
# list = gnc_search_param_prepend(list, "Date", NULL, type, SPLIT_TRANS,
#                                 TRANS_DATE_POSTED, NULL);
#
# return list;
# }
#
#
# static void
# do_nothing(GtkWindow *dialog, gpointer *a, gpointer b) {
# return;
# }
#
# void
# gnc_search_dialog_test(void) {
# static GList *params = NULL;
# static GList *display = NULL;
# static GNCSearchCallbackButton buttons[] =
# {
#
#     {("View Split"),   do_nothing, NULL, TRUE},
#     {("New Split"),    do_nothing, NULL, TRUE},
#     {("Do Something"), do_nothing, NULL, TRUE},
#     {("Do Nothing"),   do_nothing, NULL, TRUE},
#     {("Who Cares?"),   do_nothing, NULL, FALSE},
#     {NULL}
# };
#
# if (params == NULL)
#     params = get_params_list(GNC_ID_SPLIT);
#
# if (display == NULL)
#     display = get_display_list(GNC_ID_SPLIT);
#
#
# gnc_search_dialog_create(NULL, GNC_ID_SPLIT,
#                          _("Find Transaction"),
#                          params, display,
#                          NULL, NULL, buttons, NULL, NULL, NULL, NULL,
#                          NULL, NULL, NULL);
# }
