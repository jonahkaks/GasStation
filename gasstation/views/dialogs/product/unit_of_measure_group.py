from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import *
from libgasstation.core._declbase import ID_UNIT_OF_MEASURE_GROUP
from libgasstation.core.component import *
from libgasstation.core.product import UnitOfMeasureGroup, UnitOfMeasure
from libgasstation.core.session import Session

DIALOG_NEW_UNIT_OF_MEASURE_GROUP_CM_CLASS = "dialog-new-product-unit-group"
DIALOG_EDIT_UNIT_OF_MEASURE_GROUP_CM_CLASS = "dialog-edit-product-unit-group"
from gi.repository import GObject


class UnitOfMeasureGroupDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/product/unit_group.ui')
class UnitOfMeasureGroupDialog(Gtk.Dialog):
    __gtype_name__ = "UnitOfMeasureGroupDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()
    primary_unit_entry: Gtk.Entry = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, name=None, unit_group=None):
        super().__init__()
        self.book = book
        self.dialog_type = UnitOfMeasureGroupDialogType.NEW if unit_group is None else UnitOfMeasureGroupDialogType.EDIT
        self.unit_of_measure_group: UnitOfMeasureGroup = unit_group
        if name is not None:
            self.name_entry.set_text(name)
        self.set_title("Add/Edit UnitOfMeasureGroup")
        if parent is not None:
            self.set_transient_for(parent)
        self.get_style_context().add_class("UnitOfMeasureGroupDialog")
        self.set_default_response(Gtk.ResponseType.OK)
        if not modal:
            self.connect("response", self.window_response_cb)
        else:
            self.set_modal(True)
        gs_window_adjust_for_screen(self)
        component_id = ComponentManager.register(DIALOG_NEW_UNIT_OF_MEASURE_GROUP_CM_CLASS,
                                                 self.refresh_handler,
                                                 None if modal else self.close_handler)

        ComponentManager.set_session(component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(component_id,
                                           ID_UNIT_OF_MEASURE_GROUP,
                                           EVENT_MODIFY | EVENT_DESTROY)
        self.component_id = component_id
        self.to_ui()

    def to_ui(self):
        if self.dialog_type == UnitOfMeasureGroupDialogType.EDIT:
            self.name_entry.set_text(self.unit_of_measure_group.get_name())
            primary = self.unit_of_measure_group.get_primary_unit()
            self.primary_unit_entry.set_text(primary.get_name() if primary is not None else "")

    def to_unit_of_measure_group(self):
        unit_group = self.unit_of_measure_group
        if unit_group is None:
            self.unit_of_measure_group = UnitOfMeasureGroup(self.book)
            unit_group = self.unit_of_measure_group
        unit_group.begin_edit()
        unit_group.set_name(self.name_entry.get_text())
        primary_unit = self.unit_of_measure_group.get_primary_unit()
        if primary_unit is None:
            primary_unit = UnitOfMeasure(self.book)
        primary_unit.begin_edit()
        primary_unit.set_name(self.primary_unit_entry.get_text())
        primary_unit.set_group(unit_group)
        primary_unit.commit_edit()
        unit_group.commit_edit()

    def finish_ok(self):
        self.to_unit_of_measure_group()
        self.close()
        ComponentManager.close(self.component_id)

    def common_ok(self):
        name = self.name_entry.get_text()
        unit_name = self.primary_unit_entry.get_text()
        if name == "":
            message = "The UnitOfMeasureGroup must be given a name."
            gs_widget_set_style_context(self.name_entry, "error")
            self.name_entry.set_tooltip_text(message)
            show_error(self, message)
            return False

        if unit_name == "":
            message = "The UnitOfMeasure must be given a name."
            self.primary_unit_entry.set_tooltip_text(message)
            gs_widget_set_style_context(self.primary_unit_entry, "error")
            show_error(self, message)
            return False

        if UnitOfMeasureGroup.lookup_name(Session.get_current_book(),
                                        name) is not None and (self.dialog_type == UnitOfMeasureGroupDialogType.NEW or
                                                               self.unit_of_measure_group.name != name):
            show_error(self, "%s already exists" % name)
            return False
        if UnitOfMeasure.lookup_name(Session.get_current_book(), unit_name) is not None and \
                (
                        self.dialog_type == UnitOfMeasureGroupDialogType.NEW or self.unit_of_measure_group.get_primary_unit().get_name() != name):
            show_error(self, "%s already exists" % name)
            return False
        return True

    def window_response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            if not self.common_ok():
                return False
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            dialog.close()

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        inv = self.unit_of_measure_group
        if inv is None:
            ComponentManager.close(self.component_id)
            return

        if changes:
            info = ComponentManager.get_entity_events(changes, inv.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(UnitOfMeasureGroupDialog)
