from gi.repository import GObject

from gasstation.utilities.cursors import *
from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.views.unit_of_measure import UnitOfMeasureView
from libgasstation.core.component import *
from .unit_of_measure import UnitOfMeasureDialog

DIALOG_PUMPS_CM_CLASS = "dialog-unit_of_measures"
STATE_SECTION = "dialogs/edit_unit_of_measures"
from libgasstation.core.session import Session


class UnitOfMeasuresDialog(GObject.Object):
    def __init__(self):
        GObject.Object.__init__(self)
        self.dialog = None
        self.session = None
        self.book = None
        self.unit_of_measure_tree = None
        self.edit_button = None
        self.remove_button = None
        self.component_id = 0

    def window_destroy_cb(self, widget):
        ComponentManager.unregister(self.component_id)

    def edit_clicked(self):
        unit_of_measures = self.unit_of_measure_tree.get_selected_unit_of_measures()
        if not any(unit_of_measures):
            return
        for unit_of_measure in unit_of_measures:
            UnitOfMeasureDialog(self.dialog, self.book, unit=unit_of_measure).run()
        ComponentManager.refresh_all()

    def row_activated_cb(self, view, path, column):
        self.edit_clicked()

    def remove_clicked(self):
        from gasstation.views.dialogs.object_references import ObjectReferencesDialog
        unit_of_measures = self.unit_of_measure_tree.get_selected_unit_of_measures()
        if not any(unit_of_measures):
            return
        dialog = Gtk.MessageDialog(transient_for=self.dialog, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE,
                                   message_format="The following units will be deleted %s" % ",".join(
                                       [unit_of_measure.get_name() for unit_of_measure in unit_of_measures]))
        dialog.add_buttons("Cancel", Gtk.ResponseType.CANCEL, "Delete", Gtk.ResponseType.ACCEPT)
        dialog.set_default_response(Gtk.ResponseType.CANCEL)
        response = dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            gs_set_busy_cursor(None, True)
            ComponentManager.suspend()
            for unit_of_measure in unit_of_measures:
                lis = unit_of_measure.get_referrers()
                if len(lis):
                    exp = "The list below shows objects which make use of the unit_of_measure which you want to delete.\n" \
                          "Before you can delete it, you must either delete those objects or else modify them so they " \
                          "make use of another unit_of_measure"
                    ObjectReferencesDialog(exp, lis)
                    continue
                unit_of_measure.destroy()
            ComponentManager.resume()
            gs_unset_busy_cursor(None)

    def add_clicked(self):
        UnitOfMeasureDialog(self.dialog, Session.get_current_book()).run()

    def dialog_response(self, dialog, response):
        if response == RESPONSE_NEW:
            self.add_clicked()
            return

        elif response == RESPONSE_DELETE:
            self.remove_clicked()
            return

        elif response == RESPONSE_EDIT:
            self.edit_clicked()
            return

        else:
            ComponentManager.close(self.component_id)

    def selection_changed(self, selection):
        inv = any(self.unit_of_measure_tree.get_selected_unit_of_measures())
        self.edit_button.set_sensitive(inv)
        self.remove_button.set_sensitive(inv)

    def create(self, parent):
        builder = Gtk.Builder()
        builder.add_objects_from_resource("/org/jonah/Gasstation/gtkbuilder/common.ui", ["common_dialog"])
        self.dialog = builder.get_object("common_dialog")
        self.dialog.set_title("UnitOfMeasures Dialog")
        self.session = Session.get_current_session()
        self.book = self.session.get_book()
        gs_widget_set_style_context(self.dialog, "UnitOfMeasuresDialog")
        builder.connect_signals(self)
        if parent is not None:
            self.dialog.set_transient_for(parent)
        self.remove_button = builder.get_object("remove_button")
        self.edit_button = builder.get_object("edit_button")
        scrolled_window = builder.get_object("common_list_window")
        view = UnitOfMeasureView(state_section=STATE_SECTION)
        self.unit_of_measure_tree = view
        scrolled_window.add(view)
        scrolled_window.show()
        view.set_headers_visible(True)
        selection = view.get_selection()
        selection.connect("changed", self.selection_changed)
        self.unit_of_measure_tree.connect("row-activated", self.row_activated_cb)

    def close_handler(self):
        self.unit_of_measure_tree.destroy()
        self.dialog.destroy()

    def show_handler(self, klass, component_id):
        self.dialog.present()
        return True

    @classmethod
    def new(cls, parent):
        if ComponentManager.forall(DIALOG_PUMPS_CM_CLASS, cls.show_handler):
            return
        iv = GObject.new(cls)
        iv.create(parent)
        iv.component_id = ComponentManager.register(DIALOG_PUMPS_CM_CLASS, None, iv.close_handler)
        ComponentManager.set_session(iv.component_id, iv.session)
        iv.unit_of_measure_tree.grab_focus()
        iv.dialog.show()
