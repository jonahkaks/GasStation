from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import *
from gasstation.views.selections import ReferenceSelection
from libgasstation.core._declbase import ID_PRODUCT_UNIT
from libgasstation.core.component import *
from libgasstation.core.product import UnitOfMeasure
from libgasstation.core.session import Session
from .unit_of_measure_group import UnitOfMeasureGroupDialog

DIALOG_NEW_UNIT_OF_MEASURE_CM_CLASS = "dialog-new-product-unit"
DIALOG_EDIT_UNIT_OF_MEASURE_CM_CLASS = "dialog-edit-product-unit"
from gi.repository import GObject


class UnitOfMeasureDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/product/unit.ui')
class UnitOfMeasureDialog(Gtk.Dialog):
    __gtype_name__ = "UnitOfMeasureDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()
    quantity_entry: Gtk.SpinButton = Gtk.Template.Child()
    unit_grid: Gtk.Grid = Gtk.Template.Child()

    def __init__(self, parent, book, modal=False, unit: UnitOfMeasure = None, name=None):
        super().__init__()
        self.base_selection = ReferenceSelection(UnitOfMeasure, UnitOfMeasureGroupDialog)
        self.book = book
        self.dialog_type = UnitOfMeasureDialogType.NEW if unit is None else UnitOfMeasureDialogType.EDIT
        self.unit_of_measure: UnitOfMeasure = unit
        self.set_title("Add/Edit UnitOfMeasure")
        if parent is not None:
            self.set_transient_for(parent)
        self.get_style_context().add_class("UnitOfMeasureDialog")

        if not modal:
            self.connect("response", self.window_response_cb)
        else:
            self.set_modal(True)
        if name is not None:
            self.name_entry.set_text(name)
        self.base_selection.set_new_ability(True)
        self.unit_grid.attach(self.base_selection, 1, 2, 1, 1)
        self.base_selection.show()
        self.set_default_response(Gtk.ResponseType.OK)

        gs_window_adjust_for_screen(self)

        component_id = ComponentManager.register(DIALOG_NEW_UNIT_OF_MEASURE_CM_CLASS,
                                                 self.refresh_handler,
                                                 None if modal else self.close_handler)

        ComponentManager.set_session(component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(component_id,
                                           ID_PRODUCT_UNIT,
                                           EVENT_MODIFY | EVENT_DESTROY)
        self.component_id = component_id
        self.to_ui()

    def to_ui(self):
        if self.dialog_type == UnitOfMeasureDialogType.EDIT:
            p = self.unit_of_measure
            self.name_entry.set_text(p.get_name() or "")
            self.quantity_entry.set_value(p.get_quantity() or 0)
            self.base_selection.set_ref(p.get_base())

    def to_unit_of_measure(self):
        p = self.unit_of_measure
        if p is None:
            self.unit_of_measure = UnitOfMeasure(self.book)
            p = self.unit_of_measure
        p.begin_edit()
        p.set_name(self.name_entry.get_text().strip())
        p.set_quantity(self.quantity_entry.get_value())
        base = self.base_selection.get_ref()
        p.set_group(base.get_group())
        p.set_base(base)
        p.commit_edit()

    def finish_ok(self):
        self.to_unit_of_measure()
        self.close()
        ComponentManager.close(self.component_id)

    def common_ok(self):
        name = self.name_entry.get_text()
        if name == "":
            message = "The UnitOfMeasure must be given a name."
            gs_widget_set_style_context(self.name_entry, "error")
            show_error(self, message)
            return False
        elif UnitOfMeasure.lookup_name(Session.get_current_book(),
                                     name) is not None and self.dialog_type == UnitOfMeasureDialogType.NEW:
            gs_widget_set_style_context(self.name_entry, "error")
            show_error(self, "The unit %s already exists" % name)
            return False
        gs_widget_set_style_context(self.name_entry, None)
        return True

    def window_response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            if not self.common_ok():
                return False
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            dialog.close()
        return True

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        inv = self.unit_of_measure
        if inv is None:
            ComponentManager.close(self.component_id)
            return

        if changes:
            info = ComponentManager.get_entity_events(changes, inv.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(UnitOfMeasureDialog)
