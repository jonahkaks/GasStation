from gasstation.utilities.custom_dialogs import *
from gasstation.utilities.dialog import *
from libgasstation.core._declbase import ID_PRODUCT_CATEGORY
from libgasstation.core.component import *
from libgasstation.core.product import ProductCategory, ProductType
from libgasstation.core.session import Session

DIALOG_NEW_PRODUCT_CATEGORY_CM_CLASS = "dialog-new-product-category"
DIALOG_EDIT_PRODUCT_CATEGORY_CM_CLASS = "dialog-edit-product-category"
from gi.repository import GObject


class ProductCategoryDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/product/category.ui')
class ProductCategoryDialog(Gtk.Dialog):
    __gtype_name__ = "ProductCategoryDialog"
    name_entry: Gtk.Entry = Gtk.Template.Child()

    def __init__(self, parent, book, category=None, name=None):
        GObject.Object.__init__(self)
        self.book = book
        self.dialog_type = ProductCategoryDialogType.NEW if category is None else ProductCategoryDialogType.EDIT
        self.product_category = category
        self.created_product_category = None
        self.product_category_type = ProductType.INVENTORY
        self.set_title("Add/Edit ProductCategory")
        if parent is not None:
            self.set_transient_for(parent)
        self.get_style_context().add_class("ProductCategoryDialog")

        if name is not None:
            self.name_entry.set_text(name)
        self.set_default_response(Gtk.ResponseType.OK)
        gs_window_adjust_for_screen(self)
        self.component_id = ComponentManager.register(DIALOG_EDIT_PRODUCT_CATEGORY_CM_CLASS, self.refresh_handler,
                                                    self.close_handler)
        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id, ID_PRODUCT_CATEGORY, EVENT_MODIFY | EVENT_DESTROY)
        self.to_ui()

    def to_ui(self):
        if self.dialog_type == ProductCategoryDialogType.EDIT:
            p = self.product_category
            self.name_entry.set_text(p.get_name())

    def to_product_category(self):
        p = self.product_category
        if p is None:
            self.product_category = ProductCategory(self.book)
            p = self.product_category
        p.begin_edit()
        p.set_name(self.name_entry.get_text())
        p.commit_edit()

    def finish_ok(self):
        self.to_product_category()
        self.created_product_category = self.product_category
        self.product_category = None
        self.close()
        ComponentManager.close(self.component_id)

    def common_ok(self):
        name = self.name_entry.get_text()
        if name == "":
            message = "The ProductCategory must be given a name."
            show_error(self, message)
            return False
        elif ProductCategory.lookup_name(Session.get_current_book(),
                                         name) is not None and self.dialog_type == ProductCategoryDialogType.NEW:
            show_error(self, "The category %s already exists" % name)
            return False

        return True

    @Gtk.Template.Callback()
    def response_cb(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            if not self.common_ok():
                return True
            self.finish_ok()
        elif response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            ComponentManager.close(self.component_id)
            self.close()
        self.destroy()
        return True

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        inv = self.product_category
        if inv is None:
            ComponentManager.close(self.component_id)
            return

        if changes:
            info = ComponentManager.get_entity_events(changes, inv.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return

    def __repr__(self):
        return '<ProductCategoryDialog>'


GObject.type_register(ProductCategoryDialog)
