from gasstation.utilities.dialog import *
from gasstation.utilities.ui import *
from gasstation.views.components.avatar import Avatar
from gasstation.views.dialogs.product.product_category import ProductCategoryDialog
from gasstation.views.dialogs.supplier import SupplierDialog
from gasstation.views.dialogs.tax.new import NewTaxDialog
from gasstation.views.selections.account import AccountSelection
from gasstation.views.selections.date_edit import DateSelection
from gasstation.views.selections.reference import ReferenceSelection
from libgasstation import Supplier
from libgasstation.core.component import ComponentManager
from libgasstation.core.product import *
from libgasstation.core.scrub import Scrub, EquityType
from libgasstation.core.tax import Tax
from .unit_of_measure import UnitOfMeasureDialog

DIALOG_NEW_INVENTORY_CM_CLASS = "dialog-new-product"
DIALOG_EDIT_INVENTORY_CM_CLASS = "dialog-edit-product"
from gi.repository import GObject


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/product/location.ui')
class ProductLocationGrid(Gtk.Grid):
    __gtype_name__ = "ProductLocationGrid"
    as_of_date_box: Gtk.Box = Gtk.Template.Child()
    opening_stock: Gtk.SpinButton = Gtk.Template.Child()
    cost_price_entry: Gtk.SpinButton = Gtk.Template.Child()
    reorder_entry: Gtk.SpinButton = Gtk.Template.Child()
    selling_price_entry: Gtk.SpinButton = Gtk.Template.Child()

    def __init__(self, location, **properties):
        super().__init__(**properties)
        self.as_of_date = DateSelection()
        self.as_of_date_box.add(self.as_of_date)
        self.as_of_date.show()
        self.location = location
        self.value = None

    def has_value_changed(self):
        return (self.opening_stock.get_value_as_int() != 0 and self.cost_price_entry.get_value_as_int() != 0) \
               or self.reorder_entry.get_value_as_int() != 0 or self.selling_price_entry.get_value_as_int() != 0

    def remove_new_rows(self):
        self.opening_stock.set_sensitive(False)
        self.remove_row(1)
        self.remove_row(1)

    def get_value(self) -> ProductLocation:
        return self.value

    def set_value(self, value: ProductLocation):
        self.value = value


GObject.type_register(ProductLocationGrid)


class ProductDialogType(GObject.GEnum):
    NEW = 0
    EDIT = 1


@Gtk.Template(resource_path='/org/jonah/Gasstation/gtkbuilder/product/product.ui')
class ProductDialog(Gtk.Assistant):
    __gtype_name__ = "ProductDialog"
    avatar: Avatar = Gtk.Template.Child()
    name_entry: Gtk.Entry = Gtk.Template.Child()
    sku_entry: Gtk.Entry = Gtk.Template.Child()
    description_entry: Gtk.TextView = Gtk.Template.Child()
    category_frame: Gtk.Frame = Gtk.Template.Child()
    inventory_radio: Gtk.RadioButton = Gtk.Template.Child()
    non_inventory_radio: Gtk.RadioButton = Gtk.Template.Child()
    service_radio: Gtk.RadioButton = Gtk.Template.Child()
    tax_frame: Gtk.Frame = Gtk.Template.Child()
    supplier_frame: Gtk.Frame = Gtk.Template.Child()
    supplier_unit_frame: Gtk.Frame = Gtk.Template.Child()
    reorder_window: Gtk.ScrolledWindow = Gtk.Template.Child()
    tax_check: Gtk.CheckButton = Gtk.Template.Child()
    sales_account_frame: Gtk.Frame = Gtk.Template.Child()

    def __init__(self, parent, book, product=None, modal=False, name=None):
        super().__init__(use_header_bar=False)
        self.get_child().get_children()[0].destroy()
        self.category_selection = ReferenceSelection(ProductCategory, ProductCategoryDialog, use_first=False)
        self.category_selection.connect("selection-changed", self.information_page_enable_next)
        self.default_supplier_selection = ReferenceSelection(Supplier, SupplierDialog, use_first=False)
        self.supplier_unit_selection = ReferenceSelection(UnitOfMeasure, UnitOfMeasureDialog, use_first=True)
        self.book = book
        self.dialog_type = ProductDialogType.NEW if product is None else ProductDialogType.EDIT
        self.product: Product = product
        self.created_product = None
        self.product_location_grids: List[ProductLocationGrid] = []
        if name is not None:
            self.name_entry.set_text(name)

        if parent is not None:
            self.set_transient_for(parent)
        self.get_style_context().add_class("ProductDialog")


        self.description_entry.connect("focus-in-event", self.description_focus_cb)
        self.description_entry_buffer = self.description_entry.get_buffer()

        self.category_selection.set_new_ability(True)
        self.category_frame.add(self.category_selection)
        self.category_frame.show()

        self.tax_menu = ReferenceSelection(Tax, NewTaxDialog, use_first=False)
        self.tax_menu.set_new_ability(True)
        self.tax_frame.add(self.tax_menu)

        self.sales_account = AccountSelection()
        self.sales_account.set_account_filters([AccountType.INCOME])
        self.sales_account.set_new_account_ability(True)
        self.sales_account_frame.add(self.sales_account)
        self.sales_account_frame.show()
        self.sales_account.show()

        box = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        locations = Location.get_list(self.book)
        for loc in sorted(locations):
            frame = Gtk.Frame()
            label = Gtk.Label()
            label.set_use_markup(True)
            label.set_markup("<b>{}</b>".format(loc.get_name()))
            frame.set_label_widget(label)
            label.show()
            frame.set_label_align(0, 0.5)
            frame.set_shadow_type(Gtk.ShadowType.ETCHED_IN)
            grid = ProductLocationGrid(loc)
            self.product_location_grids.append(grid)
            grid.show()
            frame.add(grid)
            frame.show()
            box.pack_start(frame, False, False, 0)
        self.reorder_window.add(box)
        box.show()
        self.reorder_window.show()

        self.default_supplier_selection.set_new_ability(True)
        self.supplier_frame.add(self.default_supplier_selection)
        self.default_supplier_selection.connect("selection-changed", self.information_page_enable_next)
        self.supplier_frame.show()
        self.default_supplier_selection.show()

        self.supplier_unit_selection.set_new_ability(True)
        self.supplier_unit_frame.add(self.supplier_unit_selection)
        self.supplier_unit_selection.connect("selection-changed", self.information_page_enable_next)
        self.supplier_unit_selection.show()
        self.supplier_unit_frame.show()
        gs_window_adjust_for_screen(self)
        self.component_id = ComponentManager.register(DIALOG_EDIT_INVENTORY_CM_CLASS, self.refresh_handler,
                                                      self.close_handler)
        ComponentManager.set_session(self.component_id, Session.get_current_session())
        ComponentManager.watch_entity_type(self.component_id, ID_PRODUCT, EVENT_MODIFY | EVENT_DESTROY)
        if product is not None:
            self.to_ui()

    def run(self):
        self.show()
        self.tax_check.set_active(False)
        self.tax_check_toggled_cb(self.tax_check)

    @Gtk.Template.Callback()
    def product_type_changed_cb(self, *w):
        product_type = ProductType.INVENTORY if self.inventory_radio.get_active() else ProductType.NON_INVENTORY \
            if self.non_inventory_radio.get_active() else ProductType.SERVICE
        if product_type == ProductType.SERVICE:
            self.supplier_unit_frame.hide()
        else:
            self.supplier_unit_frame.show()
        self.information_page_enable_next()

    def to_ui(self):
        product: Product = self.product
        category = product.get_category()
        self.name_entry.set_text(product.get_name() or "")
        uom = product.get_unit_of_measure()
        if uom is not None:
            self.supplier_unit_selection.set_ref(uom.get_supplier_unit())
        self.sku_entry.set_text(product.get_sku() or "")
        self.description_entry_buffer.set_text(product.get_description() or "")
        self.tax_menu.set_ref(product.get_tax())
        self.tax_check.set_active(product.get_tax_included())
        self.category_selection.set_ref(category)
        self.default_supplier_selection.set_ref(product.get_default_supplier())
        self.sales_account.set_account(product.get_sales_account())
        url = product.get_image_url()
        if url is None:
            self.avatar.set_fullname(product.get_full_name())
        else:
            self.avatar.set_url(url)
        pqtys = {pq.get_location(): pq for pq in product.locations}

        ptype = product.get_type()
        if ptype == ProductType.INVENTORY:
            self.inventory_radio.set_active(True)
        elif ptype == ProductType.NON_INVENTORY:
            self.non_inventory_radio.set_active(True)
        else:
            self.service_radio.set_active(True)
        product_account = Scrub.get_or_make_account_with_fullname_parent(self.book.get_root_account(), self.currency,
                                                                         "Inventory Asset",AccountType.CURRENT_ASSET,
                                                                         "Asset", "Current Assets")
        for grid in self.product_location_grids:
            try:
                pq = pqtys.pop(grid.location, None)
                if pq is None:
                    continue
                if ptype == ProductType.INVENTORY:
                    grid.reorder_entry.set_value(pq.get_reorder_level())
                    qty = product.get_quantity_at_hand_on_date(grid.location)
                    txn = pq.get_opening_transaction()
                    cost = txn.get_account_value(product_account) / qty

                    grid.opening_stock.set_value(qty)
                    grid.cost_price_entry.set_value(cost)
                else:
                    grid.reorder_entry.hide()
                    grid.opening_stock.hide()
                grid.selling_price_entry.set_value(pq.get_selling_price())
                grid.set_value(pq)
            except KeyError:
                continue
        self.set_current_page(0)

    def to_product(self):
        product: Product = self.product
        if product is None:
            product = Product(self.book)
            self.product = product
        product.begin_edit()
        category = self.category_selection.get_ref()
        product.set_name(self.name_entry.get_text())
        product.set_sku(self.sku_entry.get_text())
        start = self.description_entry_buffer.get_start_iter()
        end = self.description_entry_buffer.get_end_iter()
        desc = self.description_entry_buffer.get_text(start, end, False)
        product.set_description(desc)
        product.set_image_url(self.avatar.get_url())
        product_type = ProductType.INVENTORY if self.inventory_radio.get_active() else ProductType.NON_INVENTORY \
            if self.non_inventory_radio.get_active() else ProductType.SERVICE
        product.set_type(product_type)
        sales_account = self.sales_account.get_account()
        if sales_account is None:
            sales_account = Scrub.get_or_make_account(Session.get_current_root(), gs_default_currency(), "Sales",
                                                      AccountType.INCOME, False)
        product.set_sales_account(sales_account)
        for grid in self.product_location_grids:
            product_location = grid.get_value()
            if product_location is None and grid.has_value_changed():
                product_location = ProductLocation(self.book)
                product_location.begin_edit()
                if product_type == ProductType.INVENTORY:
                    inventory = Inventory(self.book)
                    inventory.begin_edit()
                    inventory.set_date(grid.as_of_date.get_date())
                    qty = Decimal(grid.opening_stock.get_value())
                    inventory.set_quantity(qty)
                    inventory.set_remaining_quantity(qty)
                    inventory.set_price(Decimal(grid.cost_price_entry.get_value()))
                    inventory.set_product_location(product_location)
                    inventory.commit_edit()
                product_location.set_location(grid.location)
                product.locations.append(product_location)
            else:
                if product_location is None:
                    continue
                product_location.begin_edit()

            if product_type == ProductType.INVENTORY:
                product_location.set_reorder_level(Decimal(grid.reorder_entry.get_value()))
            product_location.set_selling_price(Decimal(grid.selling_price_entry.get_value()))
            product_location.commit_edit()

        product.set_tax(self.tax_menu.get_ref())
        product.set_tax_included(self.tax_check.get_active())
        product.set_category(category)

        if product_type != ProductType.SERVICE:
            supplier_unit = self.supplier_unit_selection.get_ref()
            default_unit = supplier_unit.get_base()
            uom = product.get_unit_of_measure()
            if uom is None:
                uom = ProductUnit(self.book)
            uom.begin_edit()
            if default_unit is None:
                default_unit = supplier_unit
            uom.set_default_unit(default_unit)
            uom.set_supplier_unit(supplier_unit)
            product.set_unit_of_measure(uom)
            uom.commit_edit()
            pqtys = {pq.get_location(): pq for pq in self.product.locations}
            for grid in self.product_location_grids:
                product_location = pqtys.pop(grid.location, None)
                if product_location is None:
                    continue
                opening_date = grid.as_of_date.get_date()
                opening_stock = Decimal(grid.opening_stock.get_value())
                cost_price = Decimal(grid.cost_price_entry.get_value())
                product_account = Scrub.get_or_make_account(Session.get_current_root(), gs_default_currency(),
                                                            "Inventory Asset", AccountType.CURRENT_ASSET, False)
                txn = product_location.get_opening_transaction()
                if txn is not None:
                    txn.clear_readonly()
                    txn.destroy()
                product_location.set_opening_transaction(Scrub.create_opening_balance(product_account,
                                                                                      opening_stock * cost_price,
                                                                                      opening_date,
                                                                                      Session.get_current_book(),
                                                                                      description="Product-{}--{}".format(
                                                                                          self.product.get_name(),
                                                                                          grid.location.get_name()),
                                                                                      location=grid.location,
                                                                                      equity_type=EquityType.RETAINED_EARNINGS))
        product.set_default_supplier(self.default_supplier_selection.get_ref())

    @Gtk.Template.Callback()
    def tax_check_toggled_cb(self, w):
        self.tax_menu.set_visible(w.get_active())

    @Gtk.Template.Callback()
    def on_prepare(self, *args):
        current_page = self.get_current_page()
        if current_page == 0:
            self.information_page_enable_next()
        elif current_page == 1:
            self.pricing_page_enable_next()

    def enable_page(self, ok):
        currentpagenum = self.get_current_page()
        currentpage = self.get_nth_page(currentpagenum)
        self.set_page_complete(currentpage, ok)

    def pricing_page_enable_next(self, *args):
        self.enable_page(True)

    @Gtk.Template.Callback()
    def information_page_enable_next(self, *args):
        enable_next = True
        current_page = self.get_current_page()
        if current_page != 0:
            return
        name = self.name_entry.get_text()
        sku = self.sku_entry.get_text()
        gs_widget_remove_style_context(self.name_entry, "error")
        gs_widget_remove_style_context(self.sku_entry, "error")
        if name == "":
            message = "Please add a name."
            self.name_entry.set_tooltip_text(message)
            gs_widget_set_style_context(self.name_entry, "error")
            enable_next = False

        elif self.dialog_type == ProductDialogType.NEW:
            book = Session.get_current_book()
            if Product.lookup_name(book, name) is not None:
                message = "The product \"%s\" already exists" % name
                self.name_entry.set_tooltip_text(message)
                self.name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning")
                gs_widget_set_style_context(self.name_entry, "error")
                enable_next = False
            else:
                self.name_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "emblem-default")
            if sku != "" and Product.lookup_sku(book, sku) is not None:
                message = "The Barcode or SKU \"%s\" is assigned to a product" % sku
                self.sku_entry.set_tooltip_text(message)
                self.sku_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning")
                gs_widget_set_style_context(self.sku_entry, "error")
                enable_next = False
            else:
                self.sku_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "emblem-default")

        cat = self.category_selection.get_ref()
        gs_widget_remove_style_context(self.category_frame.get_label_widget(), "error")
        if cat is None:
            message = "Please select or create a new category"
            self.category_selection.set_tooltip_text(message)
            gs_widget_set_style_context(self.category_frame.get_label_widget(), "error")
            enable_next = False
        else:
            gs_widget_remove_style_context(self.supplier_unit_frame.get_label_widget(), "error")
            product_type = ProductType.INVENTORY if self.inventory_radio.get_active() else ProductType.NON_INVENTORY \
                if self.non_inventory_radio.get_active() else ProductType.SERVICE
            if product_type != ProductType.SERVICE:
                supplier_unit = self.supplier_unit_selection.get_ref()
                if supplier_unit is None:
                    message = "Please select or create a new supplier unit with a base unit"
                    self.supplier_unit_selection.set_tooltip_text(message)
                    gs_widget_set_style_context(self.supplier_unit_frame.get_label_widget(), "error")
                    enable_next = False
        self.enable_page(enable_next)

    @Gtk.Template.Callback()
    def on_finish(self, *_):
        be = self.book.get_backend()
        if be is not None:
            with be.add_lock():
                self.to_product()
        self.created_product = self.product
        self.product = None

    @Gtk.Template.Callback()
    def on_cancel(self, *_):
        ComponentManager.close(self.component_id)

    def description_focus_cb(self, widget, event):
        start = self.description_entry_buffer.get_start_iter()
        end = self.description_entry_buffer.get_end_iter()
        desc = self.description_entry_buffer.get_text(start, end, False)
        if desc is None or desc == "":
            self.description_entry_buffer.set_text(self.name_entry.get_text())

    def close_handler(self, *args):
        self.destroy()

    def refresh_handler(self, changes):
        inv = self.product
        if inv is None:
            ComponentManager.close(self.component_id)
            return
        if changes:
            info = ComponentManager.get_entity_events(changes, inv.guid)
            if info and info.event_mask & EVENT_DESTROY:
                ComponentManager.close(self.component_id)
                return


GObject.type_register(ProductDialog)
