from gasstation.models.unit_of_measure import *
from gasstation.views.tree import *


class UnitOfMeasureView(TreeView):
    __gtype_name__ = "UnitOfMeasureView"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        model = UnitOfMeasureModel.new()
        self.add_text_column(0, title="NAME", pref_name="name", sizing_text="UnitOfMeasure 1",
                             data_col=UnitOfMeasureModelColumn.NAME, visible_always=1)
        self.add_text_column(1, title="QUANTITY", pref_name="qty", sizing_text="25000",
                             data_col=UnitOfMeasureModelColumn.QUANTITY, visible_default=True)
        self.add_text_column(2, title="GROUP", pref_name="group", sizing_text="KSLDSKLSKLSKDLS  ",
                             data_col=UnitOfMeasureModelColumn.GROUP, visible_default=True)
        self.add_text_column(3, title="BASE", pref_name="base", sizing_text="KILOGRAMS",
                             data_col=UnitOfMeasureModelColumn.BASE, visible_default=True)
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        selection = self.get_selection()
        selection.unselect_all()
        f_model = model.filter_new()
        s_model = Gtk.TreeModelSort(model=f_model)
        self.set_model(s_model)
        self.configure_columns()
        self.expand_columns("name", "qty", "base", "group")
        self.set_state_section("UnitOfMeasure")
        self.show()

    @staticmethod
    def get_unit_of_measure_from_iter(s_model, s_iter):
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        return model.get_unit_of_measure(_iter)

    def cdf0(self, col, cell, s_model, s_iter, user_data):
        path = s_model.get_path(s_iter)
        indices = path.get_indices()[0]
        cell.set_properties(text="{}".format(indices + 1), editable=False, foreground_set=0, visible=True)

    def get_unit_of_measure_from_path(self, path):
        s_model = self.get_model()
        s_iter = s_model.get_iter(path)
        return self.get_unit_of_measure_from_iter(s_model, s_iter)

    def get_selected_unit_of_measures(self):
        selection = self.get_selection()
        s_model, paths = selection.get_selected_rows()
        if s_model is None or paths is None:
            return []
        _iters = map(s_model.get_iter, paths)
        return list(map(lambda _iter: self.get_unit_of_measure_from_iter(s_model, _iter), _iters))


GObject.type_register(UnitOfMeasureView)
