from gasstation.models.location import LocationModel, LocationModelColumn
from gasstation.views.tree import *


class LocationViewColumn(GObject.GEnum):
    NAME = 0


class LocationView(TreeView):
    __gtype_name__ = "LocationView"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        model = LocationModel.new()
        sel = self.get_selection()
        sel.set_mode(Gtk.SelectionMode.MULTIPLE)
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        self.add_text_column(0, title="Name", pref_name="name",
                             sizing_text="Location 1", data_col=LocationModelColumn.NAME,
                             visible_always=True)
        self.add_text_column(1, title="Address", pref_name="address",
                             sizing_text="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nkkdklsdksdlslkdddddddddd",
                             data_col=LocationModelColumn.ADDRESS,
                             visible_default=True)

        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        selection = self.get_selection()
        selection.unselect_all()
        f_model = model.filter_new()
        s_model = Gtk.TreeModelSort(model=f_model)
        self.set_model(s_model)
        self.configure_columns()
        self.expand_columns("name", "address")
        self.set_state_section("Location")
        self.show()

    @staticmethod
    def get_location_from_iter(s_model, s_iter):
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        return model.get_location(_iter)

    def cdf0(self, col, cell, s_model, s_iter, user_data):
        path = s_model.get_path(s_iter)
        indices = path.get_indices()[0]
        cell.set_properties(text="{}".format(indices + 1), editable=False, foreground_set=0, visible=True)

    def get_location_from_path(self, path):
        s_model = self.get_model()
        s_iter = s_model.get_iter(path)
        return self.get_location_from_iter(s_model, s_iter)

    def get_selected_locations(self):
        selection = self.get_selection()
        s_model, paths = selection.get_selected_rows()
        if s_model is None or paths is None:
            return []
        _iters = map(s_model.get_iter, paths)
        return list(map(lambda _iter: self.get_location_from_iter(s_model, _iter), _iters))


GObject.type_register(LocationView)
