from gasstation.models.tank import *
from gasstation.views.tree import *


class TankViewColumn(GObject.GEnum):
    NAME = 0
    INVENTORY = 1
    CAPACITY = 2
    LOCATION = 3


class TankView(TreeView):
    __gtype_name__ = "TankView"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        model = TankModel.new()
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        selection = self.get_selection()
        selection.unselect_all()
        f_model = model.filter_new()
        s_model = Gtk.TreeModelSort(model=f_model)
        self.set_model(s_model)

        self.set_state_section("Tank")
        self.add_text_column(TankViewColumn.NAME, title="Name", pref_name="name",
                             sizing_text="TRANSMISSION 20LTRS", data_col=TankModelColumn.NAME, visible_default=True)
        self.add_text_column(TankViewColumn.INVENTORY, title="Fuel Type", pref_name="product",
                             sizing_text="Lubricants", data_col=TankModelColumn.INVENTORY)
        self.add_numeric_column(TankViewColumn.CAPACITY, title="Capacity", pref_name="capacity",
                                sizing_text="400,000", visible_default=True, data_col=TankModelColumn.CAPACITY)
        self.add_text_column(TankViewColumn.LOCATION, title="Location", pref_name="location",
                             sizing_text="Busuula ***********", data_col=TankModelColumn.LOCATION,
                             visible_default=True)
        self.configure_columns()
        self.expand_columns("name", "product", "capacity", "location")
        self.show()

    @staticmethod
    def get_tank_from_iter(s_model, s_iter):
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        return model.get_tank(_iter)

    def cdf0(self, col, cell, model, s_iter, userdata):
        cell.set_property("xalign", 1.0)
        viewcol = DataStore.get_user_data(cell, "view_column")
        if viewcol == TankViewColumn.NAME or viewcol == TankViewColumn.INVENTORY:
            cell.set_property("xalign", 0.0)
        cell.set_property("editable", False)
        return False

    def cdf_num(self, col, cell, s_model, s_iter, user_data):
        path = s_model.get_path(s_iter)
        indices = path.get_indices()[0]
        cell.set_properties(text="{}".format(indices + 1), editable=False, foreground_set=0, visible=True)

    def get_tank_from_path(self, path):
        s_model = self.get_model()
        s_iter = s_model.get_iter(path)
        return self.get_tank_from_iter(s_model, s_iter)

    def get_selected_tanks(self):
        selection = self.get_selection()
        s_model, paths = selection.get_selected_rows()
        if s_model is None or paths is None:
            return []
        _iters = map(s_model.get_iter, paths)
        return list(map(lambda _iter: self.get_tank_from_iter(s_model, _iter), _iters))

    def dispose_cb(self, widget):
        model = self.get_model_from_view()
        super().dispose_cb(widget)
        model.dispose_cb()


GObject.type_register(TankView)
