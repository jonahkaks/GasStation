from gasstation.views.invoice_entry.view import *
from libgasstation import ENTRY_INVOICE

ENTRYLEDGER_CLASS = "invoice-ledger-class"


class InvoiceLedger:
    def __init__(self, book):
        self.book = book
        self.blank_entry_guid = None
        self.blank_entry_edited = False
        self.traverse_to_new = False
        self.loading = False
        self.last_date_entered = 0
        self.hint_entry = None
        self.parent = None
        self.invoice = None
        self.query = None
        self.model = None
        self.view = None
        self.prefs_group = None
        self.full_refresh = True
        self.component_id = ComponentManager.register(ENTRYLEDGER_CLASS, self.refresh_handler, self.close_handler)
        if book.is_readonly():
            self.set_readonly(True)

    def set_prefs_group(self, string):
        self.prefs_group = string

    def set_default_invoice(self, invoice):
        self.invoice = invoice
        self.model = InvoiceEntryModel(invoice=invoice)
        if self.query is None and invoice is not None:
            self.query = QueryPrivate.create_for(ID_INVOICE_ENTRY)
            self.query.set_book(invoice.get_book())
            self.query.add_guid_match([ENTRY_INVOICE, PARAM_GUID], invoice.get_guid(), QueryOp.AND)
        self.refresh()

    def set_parent(self, parent: Gtk.Widget):
        self.parent = parent

    def set_view(self, view):
        self.view = view

    def set_readonly(self, readonly):
        if not readonly and self.book.is_readonly():
            return
        self.model.set_read_only(readonly)
        if readonly:
            self.model.clear_blank_entry()
        self.refresh()

    def get_model(self):
        return self.model

    def get_entries(self):
        if self.query is not None:
            return self.query.run()
        return []

    def refresh_internal(self, entries):
        if self.loading:
            return
        self.loading = True
        self.model.load(entries)
        self.loading = False

    def set_watches(self, entries):
        _type = None
        ComponentManager.clear_watches(self.component_id)
        ComponentManager.watch_entity(self.component_id, self.invoice.get_customer().get_guid(), EVENT_MODIFY)
        ComponentManager.watch_entity_type(self.component_id, ID_INVOICE, EVENT_MODIFY | EVENT_DESTROY)
        ComponentManager.watch_entity_type(self.component_id, ID_ACCOUNT,
                                           EVENT_MODIFY | EVENT_DESTROY | EVENT_ITEM_CHANGED)
        ComponentManager.watch_entity_type(self.component_id, ID_TAXTABLE, EVENT_MODIFY | EVENT_DESTROY)
        for entry in entries:
            ComponentManager.watch_entity(self.component_id, entry.get_guid(), EVENT_MODIFY)

    def refresh_handler(self, changes):
        self.refresh()

    def finish(self):
        ComponentManager.close(self.component_id)

    def refresh(self):
        if self.loading:
            return
        entries = self.get_entries()
        self.set_watches(entries)
        self.refresh_internal(entries)

    def close_handler(self, *args):
        ComponentManager.unregister(self.component_id)
        self.model = None
        self.view.destroy()
        self.view = None
        if self.query is not None:
            self.query.destroy()
        self.query = None
        self.invoice = None
