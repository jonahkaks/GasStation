from gasstation.plugins.pages import *
from gasstation.plugins.plugin import Plugin
from gasstation.utilities.actions import update_actions, set_important_actions
from gasstation.utilities.cursors import *
from gasstation.utilities.icons import ICON_APP
from gasstation.views.window import *
from libgasstation.core.book import Book, ID_BOOK
from libgasstation.core.engine import Engine
from libgasstation.core.uri import Uri
from .dialogs.preferences import *
from .dialogs.reset_warning import ResetWarningsDialog

STATE_FILE_TOP = "Top"
STATE_FILE_BOOK_GUID = "BookGuid"
STATE_FILE_EXT = ".gcm"
PREF_SHOW_CLOSE_BUTTON = "tab-close-buttons"
PREF_TAB_NEXT_RECENT = "tab-next-recent"
PREF_TAB_POSITION_TOP = "tab-position-top"
PREF_TAB_POSITION_BOTTOM = "tab-position-bottom"
PREF_TAB_POSITION_LEFT = "tab-position-left"
PREF_TAB_POSITION_RIGHT = "tab-position-right"
PREF_TAB_WIDTH = "tab-width"
PREF_TAB_COLOR = "show-account-color-tabs"
PREF_SAVE_CLOSE_EXPIRES = "save-on-close-expires"
PREF_SAVE_CLOSE_WAIT_TIME = "save-on-close-wait-time"

MAIN_WINDOW_NAME = "MainWindow"

DIALOG_BOOK_OPTIONS_CM_CLASS = "dialog-book-options"
window_type = 0

secs_to_save = 0
MSG_AUTO_SAVE = "Changes will be saved automatically in %u seconds"

WINDOW_COUNT = "WindowCount"
WINDOW_STRING = "Window %d"
WINDOW_GEOMETRY = "WindowGeometry"
WINDOW_POSITION = "WindowPosition"
WINDOW_MAXIMIZED = "WindowMaximized"
TOOLBAR_VISIBLE = "ToolbarVisible"
STATUSBAR_VISIBLE = "StatusbarVisible"
SUMMARY_BAR_VISIBLE = "SummaryBarVisible"
WINDOW_FIRSTPAGE = "FirstPage"
WINDOW_PAGECOUNT = "PageCount"
WINDOW_PAGEORDER = "PageOrder"
PAGE_TYPE = "PageType"
PAGE_NAME = "PageName"
PAGE_STRING = "Page %d"


class MenuUpdate:
    action_name = None
    label = None
    visible = True


class ActionStatus:
    action = None
    statusbar = None


toggle_actions = [("ViewToolbarAction", "_Toolbar", "Show/hide the toolbar on this window", True),
                  ("ViewSummaryAction", "Su_mmary Bar", "Show/hide the summary bar on this window", True),
                  ("ViewStatusbarAction", "Stat_us Bar", "Show/hide the status bar on this window", True)]

radio_entries = [("Window0Action", None, "Window _1", None, None, 0),
                 ("Window1Action", None, "Window _2", None, None, 1),
                 ("Window2Action", None, "Window _3", None, None, 2),
                 ("Window3Action", None, "Window _4", None, None, 3),
                 ("Window4Action", None, "Window _5", None, None, 4),
                 ("Window5Action", None, "Window _6", None, None, 5),
                 ("Window6Action", None, "Window _7", None, None, 6),
                 ("Window7Action", None, "Window _8", None, None, 7),
                 ("Window8Action", None, "Window _9", None, None, 8),
                 ("Window9Action", None, "Window _0", None, None, 9)]

gs_menu_important_actions = ["FileCloseAction"]
always_insensitive_actions = ["Filelogging.debug()Action"]
initially_insensitive_actions = ["FileCloseAction"]
always_hidden_actions = ["ViewSortByAction", "ViewFilterByAction"]
immutable_page_actions = ["FileCloseAction"]
multiple_page_actions = ["WindowMovePageAction"]


class MainWindowSaveData:
    key_file = None
    group_name = None
    window_num = 0
    page_num = 0
    page_offset = 0


class MergedActionEntry:
    merge_id = 0
    action_group = None


class MainWindowActionData:
    window = None
    data = None


class MainWindow(Gtk.ApplicationWindow, Window):
    window_type = GLib.quark_from_static_string("gs-main-window")
    __gtype_name__ = MAIN_WINDOW_NAME
    __gsignals__ = {
        "page_added": (GObject.SignalFlags.RUN_FIRST, None, (PluginPage,)),
        "page_changed": (GObject.SignalFlags.RUN_FIRST, None, (PluginPage,)),
    }

    active_windows = []
    gs_menu_actions = None
    __user_data = defaultdict(dict)

    def __init__(self, *args, **kwargs):
        Gtk.ApplicationWindow.__init__(self, *args, **kwargs)
        Hook.add_dangler(HOOK_BOOK_SAVED, self.update_all_titles)
        Hook.add_dangler(HOOK_BOOK_OPENED, self.attach_to_book)
        self.gs_menu_actions = [("FileAction", None, "_File", None, None, None,),
                                ("EditAction", None, "_Edit", None, None, None),
                                ("ViewAction", None, "_View", None, None, None),
                                ("ActionsAction", None, "_Actions", None, None, None),
                                ("TransactionAction", None, "Tra_nsaction", None, None, None),
                                ("SalesAction", None, "_Sales", None, None, None),
                                ("ReportsAction", None, "_Reports", None, None, None),
                                ("ToolsAction", None, "_Tools", None, None, None),
                                ("ExtensionsAction", None, "E_xtensions", None, None, None),
                                ("WindowsAction", None, "_Windows", None, None, None),
                                ("HelpAction", None, "_Help", None, None, None),
                                ("FileImportAction", None, "_Import", None, None, None),
                                ("FileExportAction", None, "_Export", None, None, None),
                                ("FilePrintAction", "document-print", "_Print...", "<primary>p",
                                 "Print the currently active page", None),
                                (
                                    "FilePageSetupAction", "document-page-setup", "Pa_ge Setup...", "<primary><shift>p",
                                    "Specify the page size and orientation for printing",
                                    self.cmd_page_setup
                                ),
                                (
                                    "FilePropertiesAction", "document-properties", "Proper_ties", "<Alt>Return",
                                    "Edit the properties of the current file",
                                    self.cmd_file_properties
                                ),
                                (
                                    "FileCloseAction", "window-close", "_Close", "<primary>W",
                                    "Close the currently active page",
                                    self.cmd_file_close
                                ),
                                (
                                    "FileQuitAction", "application-exit", "_Quit", "<primary>Q",
                                    "Quit this application",
                                    self.cmd_file_quit
                                ),
                                (
                                    "EditCutAction", "edit-cut", "Cu_t", "<primary>X",
                                    "Cut the current selection and copy it to clipboard",
                                    self.cmd_edit_cut
                                ),
                                (
                                    "EditCopyAction", "edit-copy", "_Copy", "<primary>C",
                                    "Copy the current selection to clipboard",
                                    self.cmd_edit_copy
                                ),
                                (
                                    "EditPasteAction", "edit-paste", "_Paste", "<primary>V",
                                    "Paste the clipboard content at the cursor position",
                                    self.cmd_edit_paste
                                ),
                                (
                                    "EditPreferencesAction", "preferences-system", "Pr_eferences", None,
                                    "Edit the global preferences of GasStation",
                                    self.cmd_edit_preferences
                                ),

                                (
                                    "ViewSortByAction", None, "_Sort By...", None,
                                    "Select sorting criteria for this page view", None
                                ),
                                (
                                    "ViewFilterByAction", None, "_Filter By...", None,
                                    "Select the account types that should be displayed.", None
                                ),
                                (
                                    "ViewRefreshAction", "view-refresh", "_Refresh", "<primary>r",
                                    "Refresh this window",
                                    self.cmd_view_refresh
                                ),

                                ("ScrubMenuAction", None, "_Check & Repair", None, None, None),
                                (
                                    "ActionsForgetWarningsAction", None, "Reset _Warnings...", None,
                                    "Reset the state of all warning messages so they will be shown again.",
                                    self.cmd_actions_reset_warnings
                                ),
                                (
                                    "ActionsRenamePageAction", None, "Re_name Page", None,
                                    "Rename this page.",
                                    self.cmd_actions_rename_page
                                ),

                                (
                                    "WindowNewAction", None, "_New Window", None,
                                    "Open a new top-level GasStation window.",
                                    self.cmd_window_new
                                ),
                                (
                                    "WindowMovePageAction", None, "New Window with _Page", None,
                                    "Move the current page to a new top-level GasStation window.",
                                    self.cmd_window_move_page
                                ),

                                (
                                    "HelpTutorialAction", "help-browser", "Tutorial and Concepts _Guide", "<primary>H",
                                    "Open the GasStation Tutorial",
                                    self.cmd_help_tutorial
                                ),
                                (
                                    "HelpContentsAction", "help-browser", "_Contents", "F1",
                                    "Open the GasStation Help",
                                    None
                                ),
                                (
                                    "HelpAboutAction", "help-about", "_About", None,
                                    "About GasStation",
                                    self.cmd_help_about

                                )]

        self.toggle_actions = [("ViewToolbarAction", None, "_Toolbar", None, "Show/hide the toolbar on this window",
                                self.cmd_view_toolbar, True),
                               ("ViewSummaryAction", None, "Su_mmary Bar", None,
                                "Show/hide the summary bar on this window", self.cmd_view_summary, True),
                               ("ViewStatusbarAction", None, "Stat_us Bar", None,
                                "Show/hide the status bar on this window", self.cmd_view_statusbar, True)]
        gs_pref_register_cb(PREFS_GROUP_GENERAL,
                            PREF_SHOW_CLOSE_BUTTON,
                            self.update_tab_close,
                            None)
        gs_pref_register_cb(PREFS_GROUP_GENERAL,
                            PREF_TAB_WIDTH,
                            self.update_tab_width,
                            None)
        self.menu_dock = None
        self.side_bar = None
        self.toolbar = None
        self.notebook = None
        self.show_color_tabs = None
        self.statusbar = None
        self.progressbar = None
        self.about_dialog = None
        self.action_group = None
        self.just_plugin_prefs = False
        self.current_page = None
        self.pos = []
        self.merged_actions_table = {}
        self.quiting = False
        self.gtk_window = None
        self.ui_merge = None
        self.already_dead = False
        self.installed_pages = []
        self.usage_order = []
        self.notification = Gio.Notification()
        self.notification.set_icon(Gio.Icon.new_for_string(ICON_APP))
        self.merged_actions_table = {}
        gs_widget_set_style_context(self, "MainWindow")
        self.event_handler_id = Event.register_handler(self.event_handler)
        self.show_color_tabs = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_TAB_COLOR)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_TAB_COLOR, self.update_tab_color)
        self.setup_window()
        Tracking.remember(self)
        self.set_default_size(800, 600)
        old_window = self.get_main_window(None)
        if old_window:
            width, height = old_window.get_size()
            self.resize(width, height)
            if old_window.get_window().get_state() & Gdk.WindowState.MAXIMIZED != 0:
                self.maximize()
        self.__class__.active_windows.append(self)
        self.update_title()
        self.quiting = False
        self.just_plugin_prefs = False
        self.update_all_menu_items()
        Engine.add_commit_error_callback(self.engine_commit_error_callback)

    @staticmethod
    def get_user_data(obj, key):
        return MainWindow.__user_data.get(obj, {}).get(key, None)

    @staticmethod
    def set_user_data(obj, key, userdata):
        MainWindow.__user_data[obj][key] = userdata

    def foreach_page(self, fn, *args):
        logging.debug("For each page applying function %s" % fn.__name__)
        for w in self.active_windows:
            for page in w.installed_pages:
                fn(page, *args)

    def restore_page(self, data):
        page_group = PAGE_STRING % (data.page_offset + data.page_num)
        try:
            page_type = data.key_file.get_string(page_group, PAGE_TYPE)
        except GLib.Error:
            return
        try:
            page = self.installed_pages[data.page_num]
            class_type = page.plugin_name
            if page_type != class_type:
                return
        except IndexError:
            page = PluginPage.do_recreate_page(self, page_type, data.key_file, page_group)
            if page is not None:
                if page.window is None:
                    page.set_use_new_window(False)
                    GLib.idle_add(self.open_page, self, page, priority=GLib.PRIORITY_HIGH_IDLE)
                try:
                    name = data.key_file.get_string(page_group, PAGE_NAME)
                    MainWindow.update_page_name(page, name)
                except GLib.Error:
                    pass

    @classmethod
    def restore_window(cls, window, data):
        window_group = WINDOW_STRING % (data.window_num + 1)
        if not data.key_file.has_group(window_group):
            if window is not None:
                cls.restore_default_state(window)
                window.show()
            return

        page_count = data.key_file.get_integer(window_group, WINDOW_PAGECOUNT)
        if page_count == 0: return
        page_start = data.key_file.get_integer(window_group, WINDOW_FIRSTPAGE)
        if window is None:
            window = cls()
        geom = data.key_file.get_integer_list(window_group, WINDOW_GEOMETRY)
        if len(geom) != 2:
            logging.debug("invalid number of values for group %s key %s" % (window_group, WINDOW_GEOMETRY))
        else:
            window.resize(geom[0], geom[1])
        pos = data.key_file.get_integer_list(window_group, WINDOW_POSITION)
        if len(pos) != 2:
            logging.debug("invalid number of values for group %s key %s" % (window_group, WINDOW_POSITION))
        elif (pos[0] + (geom[0] if geom else 0) < 0) or (pos[0] > Gdk.Screen.width()) \
                or (pos[1] + (geom[1] if geom else 0) < 0) or (pos[1] > Gdk.Screen.height()):
            logging.debug("position %dx%d, size%dx%d is offscreen will not move" % (pos[0],
                                                                                    pos[1], geom[0] if geom else 0,
                                                                                    geom[1] if geom else 0))
        else:
            window.move(pos[0], pos[1])
            window.pos = pos
        max = data.key_file.get_boolean(window_group, WINDOW_MAXIMIZED)
        if max:
            window.maximize()
        action = window.find_action("ViewToolbarAction")
        visible = action.get_active()
        desired_visibility = data.key_file.get_boolean(window_group, TOOLBAR_VISIBLE)
        if visible != desired_visibility:
            action.set_active(desired_visibility)
        action = window.find_action("ViewSummaryAction")
        visible = action.get_active()
        desired_visibility = data.key_file.get_boolean(window_group, SUMMARY_BAR_VISIBLE)
        if visible != desired_visibility:
            action.set_active(desired_visibility)
        action = window.find_action("ViewStatusbarAction")
        visible = action.get_active()
        desired_visibility = data.key_file.get_boolean(window_group, STATUSBAR_VISIBLE)
        if visible != desired_visibility:
            action.set_active(desired_visibility)
        for i in range(page_count):
            data.page_offset = page_start
            data.page_num = i
            window.restore_page(data)
            while Gtk.events_pending():
                Gtk.main_iteration()

        order = data.key_file.get_integer_list(window_group, WINDOW_PAGEORDER)
        if len(order) != page_count:
            pass
        else:
            window.usage_order = []
            for i in range(len(order)):
                try:
                    page = window.installed_pages[order[i] - 1]
                    window.usage_order.append(page)
                except IndexError:
                    pass
            window.notebook.set_current_page(order[0] - 1)
        window.show()

    @classmethod
    def attach_to_book(cls, session):
        if session is None:
            return
        book = session.get_book()
        book.set_dirty_cb(cls.book_dirty_cb)
        cls.update_all_titles(session)
        cls.update_all_menu_items()

    @staticmethod
    def statusbar_notification_off(msg_id):
        mainwindow = MainWindow.get_main_window(None)
        if msg_id == 0:
            return False
        if mainwindow is not None:
            statusbar = mainwindow.get_statusbar()
            statusbar.remove(0, msg_id)
        return False

    @staticmethod
    def generate_statusbar_last_modified_message():
        if Session.current_session_exist():
            book_id = Session.get_current_session().get_url()
            if book_id is None:
                return None
            else:
                if book_id.is_file():
                    file_path = book_id.get_path()
                    filename = GLib.path_get_basename(file_path)
                    mt = os.path.getmtime(file_path)
                    time_string = "Last modified on " + time.strftime("%a, %b %d, %Y at %I:%M %p", time.localtime(mt))
                    message = "File %s opened. %s" % (filename, time_string)
                    return message

    @classmethod
    def statusbar_notification_last_modified(cls):
        for win in cls.active_windows:
            statusbar = win.get_statusbar()
            msg = cls.generate_statusbar_last_modified_message()
            if msg is not None:
                message_id = statusbar.push(0, msg)
                GLib.timeout_add(10 * 1000, cls.statusbar_notification_off, message_id)

    @classmethod
    def restore_all_windows(cls, keyfile):
        window_count = 0
        data = MainWindowSaveData()
        data.key_file = keyfile
        try:
            window_count = data.key_file.get_integer(STATE_FILE_TOP, WINDOW_COUNT)
        except GLib.GError as e:
            if e.code == Gio.IOErrorEnum.PERMISSION_DENIED:
                logging.debug("Could not read file permmision denied")
            elif e.code == Gio.IOErrorEnum.IS_DIRECTORY:
                logging.debug("you can't read a directory.")
            else:
                logging.debug(str(e))
        gs_set_busy_cursor(None, True)
        for i in range(window_count):
            data.window_num = i
            try:
                window = cls.active_windows[i]
            except IndexError:
                continue
            cls.restore_window(window, data)
        gs_unset_busy_cursor(None)
        cls.statusbar_notification_last_modified()

    @classmethod
    def restore_default_state(cls, window):
        if window is None:
            window = cls.active_windows[0]
        window.show()
        actions = map(window.find_action, ["ViewAccountTreeAction"])
        for action in actions:
            action.activate()

    @staticmethod
    def save_page(page, data):
        plugin_name = page.__class__.__name__
        page_name = PluginPage.get_name(page)
        if plugin_name is None or page_name is None:
            return
        page_group = PAGE_STRING % data.page_num
        data.page_num += 1
        data.key_file.set_string(page_group, PAGE_TYPE, plugin_name)
        data.key_file.set_string(page_group, PAGE_NAME, page_name)
        page.save_page(data.key_file, page_group)

    def save_window(self, data):
        num_pages = self.notebook.get_n_pages()
        if 0 == num_pages:
            return
        window_group = WINDOW_STRING % data.window_num
        data.window_num += 1
        data.key_file.set_integer(window_group, WINDOW_PAGECOUNT, num_pages)
        data.key_file.set_integer(window_group, WINDOW_FIRSTPAGE, data.page_num)
        order = []
        for i in range(num_pages):
            page = self.usage_order[i]
            order.append(self.installed_pages.index(page) + 1)
        data.key_file.set_integer_list(window_group, WINDOW_PAGEORDER, order)
        coords = self.get_position(), self.get_size()
        maximized = (self.get_window().get_state() & Gdk.WindowState.MAXIMIZED) != 0
        minimized = (self.get_window().get_state() & Gdk.WindowState.ICONIFIED) != 0
        if minimized:
            pos = self.pos
            data.key_file.set_integer_list(window_group, WINDOW_POSITION, list(pos[0]))
        else:
            data.key_file.set_integer_list(window_group, WINDOW_POSITION, list(coords[0]))
        data.key_file.set_integer_list(window_group, WINDOW_GEOMETRY, list(coords[1]))
        data.key_file.set_boolean(window_group, WINDOW_MAXIMIZED, maximized)
        action = self.find_action("ViewToolbarAction")
        visible = action.get_active()
        data.key_file.set_boolean(window_group, TOOLBAR_VISIBLE, visible)
        action = self.find_action("ViewSummaryAction")
        visible = action.get_active()
        data.key_file.set_boolean(window_group, SUMMARY_BAR_VISIBLE, visible)
        action = self.find_action("ViewStatusbarAction")
        visible = action.get_active()
        data.key_file.set_boolean(window_group, STATUSBAR_VISIBLE, visible)
        for p in self.installed_pages:
            self.save_page(p, data)

    @classmethod
    def save_all_windows(cls, keyfile):
        data = MainWindowSaveData()
        data.key_file = keyfile
        data.window_num = 1
        data.page_num = 1
        data.key_file.set_integer(STATE_FILE_TOP, WINDOW_COUNT, len(cls.active_windows))
        for w in cls.active_windows:
            w.save_window(data)

    def finish_pending(self):
        if self.installed_pages is not None:
            for item in self.installed_pages:
                if not item.finish_pending():
                    return False
        return True

    @staticmethod
    def all_finish_pending():
        windows = Tracking.get_list(MAIN_WINDOW_NAME)
        for win in windows:
            if not win.finish_pending():
                return False
        return True

    def engine_commit_error_callback(self, errcode):
        reason = errcode.args[0] if isinstance(errcode, Exception) else ""
        self.notification.set_title("Unable to save data")
        self.notification.set_body(reason)
        self.notification.set_priority(Gio.NotificationPriority.HIGH)
        self.get_application().send_notification(None, self.notification)

    @staticmethod
    def page_exists(page):
        for window in MainWindow.active_windows:
            if page in window.installed_pages:
                return True
        return False

    @staticmethod
    def auto_save_countdown(dialog, label):
        global secs_to_save
        if not isinstance(dialog, Gtk.Dialog):
            return False
        if not isinstance(label, Gtk.Label):
            return False
        if secs_to_save:
            secs_to_save -= 1
        timeoutstr = MSG_AUTO_SAVE % secs_to_save
        label.set_text(timeoutstr)
        if not secs_to_save:
            dialog.response(Gtk.ResponseType.APPLY)
            return False
        return True

    def prompt_for_save(self):
        from gasstation.utilities.auto_save import AutoSave
        from gasstation.views.dialogs.setup import File
        title = "Save changes to file %s before closing?"
        message_hours = "If you don't save, changes from the past %d hours and %d minutes will be discarded."
        message_days = "If you don't save, changes from the past %d days and %d hours will be discarded."
        if not Session.current_session_exist():
            return False
        session = Session.get_current_session()
        book = session.get_book()
        if not book.session_not_saved():
            return False
        filename = session.get_url()
        if filename is None or not len(filename):
            filename = "<unknown>"
        filename = GLib.path_get_basename(filename)
        AutoSave.remove_timer(book)
        dialog = Gtk.MessageDialog(parent=self, modal=True, message_type=Gtk.MessageType.WARNING,
                                   buttons=Gtk.ButtonsType.NONE,
                                   message_format=title % filename)
        oldest_change = book.get_session_dirty_time()
        minutes = (int((datetime.datetime.now() - oldest_change).total_seconds())) / 60 + 1
        hours = int(minutes / 60)
        minutes = int(minutes % 60)
        days = int(hours / 24)
        hours = int(hours % 24)
        if int(days) > 0:
            dialog.format_secondary_text(message_days % (days, hours))
        elif int(hours) > 0:
            dialog.format_secondary_text(message_hours % (hours, minutes))
        else:
            dialog.format_secondary_text(
                "If you don't save, changes from the past %d minute will be discarded." % minutes)

        dialog.add_buttons("Close _Without Saving", Gtk.ResponseType.CLOSE,
                           "_Cancel", Gtk.ResponseType.CANCEL,
                           "_Save", Gtk.ResponseType.APPLY)
        dialog.set_default_response(Gtk.ResponseType.APPLY)

        if gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_SAVE_CLOSE_EXPIRES):
            secs_to_save = gs_pref_get_int(PREFS_GROUP_GENERAL, PREF_SAVE_CLOSE_WAIT_TIME)
            timeoutstr = MSG_AUTO_SAVE % secs_to_save
            label = Gtk.Label(label=timeoutstr)
            label.show()
            msg_area = dialog.get_message_area()
            msg_area.pack_end(label, True, True, 0)
            label.set_property("xalign", 0.0)
            GLib.timeout_add_seconds(1, self.auto_save_countdown, dialog, label)
        response = dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.APPLY:
            File.save(self)
            return False

        elif response == Gtk.ResponseType.CLOSE:
            book.mark_session_saved()
            return False
        else:
            return True

    def add_plugin(self, plugin):
        plugin.do_add_to_window(self, window_type)

    def remove_plugin(self, plugin):
        plugin.do_remove_from_window(self, window_type)

    def timed_quit(self):
        from gasstation.views.dialogs.setup import File
        if File.save_in_progress():
            return True
        if File.query_save(MainWindow.get_main_window(None), False):
            application = self.get_application()
            application.shutdown(0)
        return False

    def quit(self):
        from gasstation.views.dialogs.setup import File
        do_shutdown = True
        if Session.current_session_exist():
            session = Session.get_current_session()
            needs_save = session.get_book().session_not_saved() and not File.save_in_progress()
            do_shutdown = not needs_save or (needs_save and not self.prompt_for_save())
        if do_shutdown:
            self.remove_prefs()
            GLib.timeout_add(250, self.timed_quit)
            return True
        return False

    def delete_event(self, window, event):
        if self.already_dead:
            return True

        if not self.finish_pending():
            return True

        if len(MainWindow.active_windows) > 1:
            return False
        self.already_dead = self.quit()
        return self.already_dead

    def event_handler(self, entity, event_type, *user_data):
        if not isinstance(entity, Book):
            return
        if event_type != EVENT_DESTROY:
            return
        for page in self.installed_pages:
            if page.has_book(entity):
                self.close_page(page)

    def generate_title(self):
        dirty = ""
        readonly_text = None
        book_id = ""
        filename = ""
        if Session.current_session_exist():

            session = Session.get_current_session()
            book_id = session.get_url()
            book = Session.get_current_book()
            if book is not None:
                if book.session_not_saved():
                    dirty = "*"
                if book.is_readonly():
                    readonly_text = "(read-only)"
        readonly = readonly_text if readonly_text is not None else ""
        if book_id is None:
            filename = "Unsaved Book"
        elif isinstance(book_id, Uri):
            if book_id.is_file():
                filename = GLib.path_get_basename(book_id.get_path())
            else:
                filename = str(book_id)
        page = self.current_page
        if page is not None:
            title = "%s%s%s - %s - GasStation" % (dirty, filename, readonly, PluginPage.get_name(page))
        else:
            title = "%s%s%s - GasStation" % (dirty, filename, readonly)
        immutable = page is not None and page.get_user_data(page, PLUGIN_PAGE_IMMUTABLE) is not None
        update_actions(self.action_group, immutable_page_actions, "sensitive", not immutable)
        self.emit("page_changed", page)
        return title

    def update_title(self):
        title = self.generate_title()
        self.set_title(title)

    @classmethod
    def update_all_titles(cls, *args):
        for w in cls.active_windows:
            w.update_title()

    @classmethod
    def book_dirty_cb(cls, book, dirty, *user_data):
        from gasstation.utilities.auto_save import AutoSave
        cls.update_all_titles()
        AutoSave.dirty_handler(book, dirty)

    def update_radio_button(self):
        try:
            index = MainWindow.active_windows.index(self)
        except ValueError:
            return
        if index >= len(radio_entries): return
        action_name = "Window%dAction" % index
        action = self.action_group.get_action(action_name)
        action_list = action.get_group()
        if action_list is not None:
            first_action = action_list[-1]
            first_action.handler_block_by_func(self.cmd_window_raise)
            action.set_active(True)
            first_action.handler_unblock_by_func(self.cmd_window_raise)

    def update_menu_item(self):
        data = MenuUpdate()
        try:
            index = MainWindow.active_windows.index(self)
        except ValueError:
            return
        if index > len(radio_entries): return
        title = self.generate_title()
        strings = title.split("_")
        expanded = "__".join(strings)
        if index < 10:
            data.label = "_%d %s" % ((index + 1) % 10, expanded)
        else:
            data.label = expanded
        data.visible = True
        data.action_name = "Window%dAction" % index
        for w in MainWindow.active_windows:
            w.update_one_menu_action(data)

    @staticmethod
    def update_all_menu_items():
        data = MenuUpdate()
        for a in MainWindow.active_windows:
            a.update_menu_item()
            a.update_radio_button()
        data.visible = False
        for i in range(len(MainWindow.active_windows), len(radio_entries), 1):
            data.action_name = "Window%dAction" % i
            label = "Window _%d" % ((i - 1) % 10)
            data.label = label
            for w in MainWindow.active_windows:
                w.update_one_menu_action(data)

    def update_one_menu_action(self, data):
        action = self.action_group.get_action(data.action_name)
        if action is not None:
            action.set_property("label", data.label)
            action.set_property("visible", data.visible)

    def update_tab_close_one_page(self, page, user_data):
        close_button = self.get_user_data(page, PLUGIN_PAGE_CLOSE_BUTTON)
        if close_button is None: return
        if user_data:
            close_button.show()
        else:
            close_button.hide()

    def update_tab_close(self, prefs, pref, *args):
        new_value = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_SHOW_CLOSE_BUTTON)
        self.foreach_page(self.update_tab_close_one_page, new_value)

    def update_tab_color_one_page(self, page):
        color_string = page.get_color()
        self.update_page_color(page, color_string)

    def update_tab_color(self, gsettings, pref):
        if PREF_TAB_COLOR == pref:
            self.show_color_tabs = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_TAB_COLOR)
            self.foreach_page(self.update_tab_color_one_page)

    @staticmethod
    def set_tab_ellipsize(label, tab_width):
        lab_text = label.get_text()
        if tab_width != 0:
            text_length = len(lab_text)
            if text_length < tab_width:
                label.set_width_chars(text_length)
                label.set_ellipsize(Pango.EllipsizeMode.NONE)
            else:
                label.set_width_chars(tab_width)
                label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        else:
            label.set_width_chars(15)
            label.set_ellipsize(Pango.EllipsizeMode.NONE)

    @staticmethod
    def update_tab_width_one_page(page, userdata):
        label = MainWindow.get_user_data(page, PLUGIN_PAGE_TAB_LABEL)
        if not label:
            return
        MainWindow.set_tab_ellipsize(label, userdata)

    def update_tab_width(self, prefs, pref, *args):
        new_value = gs_pref_get_float(PREFS_GROUP_GENERAL, PREF_TAB_WIDTH)
        self.foreach_page(self.update_tab_width_one_page, new_value)

    def find_tab_items(self, page):
        label_p = None
        entry_p = None
        if not page.get_widget(): return False
        tab_widget = self.notebook.get_tab_label(page.get_widget())
        if isinstance(tab_widget, Gtk.EventBox):
            tab_hbox = tab_widget.get_child()
        elif isinstance(tab_widget, Gtk.Box):
            tab_hbox = tab_widget
        else:
            return False
        children = tab_hbox.get_children()
        for widget in children:
            if isinstance(widget, Gtk.Label):
                label_p = widget
            elif isinstance(widget, Gtk.Entry):
                entry_p = widget
        return label_p, entry_p

    def find_tab_widget(self, page):
        if not page.get_widget():
            return False
        widget_p = self.notebook.get_tab_label(page.get_widget())
        return widget_p

    @staticmethod
    def update_page_name(page, name_in):
        if name_in is None or not isinstance(page, PluginPage):
            return
        old_page_name = PluginPage.get_name(page)
        old_page_long_name = PluginPage.get_long_name(page)
        name = name_in.strip()
        if name == old_page_name:
            return
        page.set_name(name)
        window = page.window
        if not window: return
        if window.find_tab_items(page):
            label, entry = window.find_tab_items(page)
            label.set_text(name)
        lab_width = gs_pref_get_float(PREFS_GROUP_GENERAL, PREF_TAB_WIDTH)
        window.update_tab_width_one_page(page, lab_width)
        if old_page_long_name and old_page_name and old_page_long_name.find(old_page_name):
            string_position = len(old_page_long_name) - len(old_page_name)
            new_page_long_name = old_page_long_name[:string_position] + name
            page.set_long_name(new_page_long_name)
            if window.find_tab_widget(page):
                tab_widget = window.find_tab_widget(page)
                tab_widget.set_tooltip_text(new_page_long_name)
        if page.get_widget():
            label = window.notebook.get_menu_label(page.get_widget())
            label.set_text(name)
        window.update_title()

    @staticmethod
    def update_page_color(page, color_in):
        color_string = ""
        want_color = False
        if color_in is not None:
            want_color = True
            color_string = color_in.strip()
        window = page.window
        if want_color:
            page.set_color(color_string)
        else:
            page.set_color(None)
        tab_widget = window.find_tab_widget(page)
        tab_color = Gdk.RGBA()
        if want_color and Gdk.RGBA.parse(tab_color, color_string) and window.show_color_tabs:
            provider = Gtk.CssProvider.new()
            if not isinstance(tab_widget, Gtk.EventBox):
                event_box = Gtk.EventBox()
                window.notebook.set_tab_label(page.get_widget(), event_box)
                event_box.add(tab_widget)
                tab_widget = event_box
                stylectxt = tab_widget.get_style_context()
                col_str = tab_color.to_string()
                widget_css = "*{\n  background-color:" + col_str + "\n}\n"
                provider.load_from_data(bytes(widget_css.encode()))
                stylectxt.add_provider(provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        else:
            if isinstance(tab_widget, Gtk.EventBox):
                tab_hbox = tab_widget.get_child()
                tab_widget.remove(tab_hbox)
                window.notebook.set_tab_label(page.get_widget(), tab_hbox)

    @staticmethod
    def tab_entry_activate(entry, page):
        if not isinstance(entry, Gtk.Entry) or not isinstance(page, PluginPage):
            return
        if not page.window.find_tab_items(page):
            return
        label, entry2 = page.window.find_tab_items(page)
        MainWindow.update_page_name(page, entry.get_text())
        entry.hide()
        label.show()

    @staticmethod
    def tab_entry_editing_done(entry, page):
        MainWindow.tab_entry_activate(entry, page)
        return False

    @staticmethod
    def tab_entry_focus_out_event(entry, event, page):
        entry.editing_done()
        return False

    @staticmethod
    def tab_entry_key_press_event(entry, event, page):
        if event.keyval == Gdk.KEY_Escape:
            if not isinstance(entry, Gtk.Entry) or not isinstance(page, PluginPage):
                return False
            if not page.window.find_tab_items(page):
                return False
            label, entry2 = page.window.find_tab_items(page)
            entry2.set_text(label.get_text())
            entry.hide()
            label.show()
        return False

    def connect_page(self, page, tab_hbox, menu_label):
        if not isinstance(page, PluginPage):
            return
        page.window = self
        notebook = self.notebook
        self.installed_pages.append(page)
        self.usage_order.insert(0, page)
        widget = page.get_widget()
        if widget is None:
            return
        notebook.append_page_menu(widget, tab_hbox, menu_label)
        notebook.set_tab_reorderable(widget, True)
        page.inserted()
        notebook.set_current_page(-1)
        page.window_changed(self)
        self.emit("page_added", page)
        page.get_widget().connect("popup-menu", self.popup_menu_cb, page)
        page.get_widget().connect_after("button-press-event", self.button_press_cb, page)

    def disconnect_page(self, page):
        if not isinstance(page, PluginPage):
            return
        page.get_widget().disconnect_by_func(self.popup_menu_cb)
        page.get_widget().disconnect_by_func(self.button_press_cb)
        if self.current_page == page:
            page.unmerge_actions(self.ui_merge)
            page.unselected()
            self.current_page = None
        self.installed_pages.remove(page)
        if page in self.usage_order:
            self.usage_order.remove(page)
        notebook = self.notebook
        if gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_TAB_NEXT_RECENT):
            try:
                new_page = self.usage_order[0]
                if new_page:
                    page_num = notebook.page_num(new_page.get_widget())
                    notebook.set_current_page(page_num)
                    while Gtk.events_pending():
                        Gtk.main_iteration()
            except IndexError:
                pass
        page_num = notebook.page_num(page.get_widget())
        notebook.remove_page(page_num)
        if notebook.get_current_page() == -1:
            self.switch_page(notebook, None, -1)
        page.removed()
        self.ui_merge.ensure_update()
        self.set_status(page, None)

    @staticmethod
    def display_page(page):
        if not isinstance(page, PluginPage):
            return
        window = page.window
        notebook = window.notebook
        page_num = notebook.page_num(page.get_widget())
        notebook.set_current_page(page_num)
        window.present()

    def open_page(self, page):
        if not isinstance(page, PluginPage):
            return
        if not page.has_books():
            return
        if self.page_exists(page):
            self.display_page(page)
            return
        window = None
        if page.get_use_new_window():
            for w in MainWindow.active_windows:
                if not len(w.installed_pages):
                    window = w
                    break
            if window is None:
                window = MainWindow()
            window.show()
        page.window = self if not page.get_use_new_window() else window
        page.create_widget()
        self.set_user_data(page.get_widget(), PLUGIN_PAGE_LABEL, page)
        width = gs_pref_get_float(PREFS_GROUP_GENERAL, PREF_TAB_WIDTH)

        lab_text = PluginPage.get_name(page)
        label = Gtk.Label(lab_text)
        label.set_hexpand(True)
        label.set_vexpand(True)
        self.set_user_data(page, PLUGIN_PAGE_TAB_LABEL, label)
        self.set_tab_ellipsize(label, tab_width=width)
        label.show()
        tab_hbox = Gtk.Box(Gtk.Orientation.HORIZONTAL, 6)
        tab_hbox.set_homogeneous(False)
        tab_hbox.set_hexpand(False)
        tab_hbox.show()
        if page.tab_icon is not None:
            image = Gtk.Image.new_from_icon_name(page.tab_icon, Gtk.IconSize.MENU)
            image.set_margin_start(5)
            tab_hbox.pack_start(image, False, False, 0)
            image.show()
        tab_hbox.pack_start(label, True, True, 0)
        text = page.get_long_name()
        if text is not None:
            tab_hbox.set_tooltip_text(text)
        entry = Gtk.Entry()
        entry.hide()
        tab_hbox.pack_start(entry, True, True, 0)
        entry.connect("activate", self.tab_entry_activate, page)
        entry.connect("focus-out-event", self.tab_entry_focus_out_event, page)
        entry.connect("key-press-event", self.tab_entry_key_press_event, page)
        entry.connect("editing-done", self.tab_entry_editing_done, page)

        if not page.get_user_data(page, PLUGIN_PAGE_IMMUTABLE):
            close_button = Gtk.Button.new_from_icon_name("window-close-symbolic", Gtk.IconSize.BUTTON)
            close_button.set_relief(Gtk.ReliefStyle.NONE)
            if gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_SHOW_CLOSE_BUTTON):
                close_button.show()
            else:
                close_button.hide()
            close_button.set_always_show_image(True)
            close_button.connect("clicked", lambda b: self.close_page(page))
            tab_hbox.pack_start(close_button, False, False, 0)
            page.set_user_data(page, PLUGIN_PAGE_CLOSE_BUTTON, close_button)
        label = Gtk.Label(PluginPage.get_name(page))
        self.connect_page(page, tab_hbox, label)
        color_string = page.get_color()
        self.update_page_color(page, color_string)

    @classmethod
    def close_page(cls, page):
        if page is None or not page.get_widget():
            return
        if not page.finish_pending():
            return
        if not isinstance(page.window, MainWindow):
            return
        window = page.window
        if window is None: return
        cls.__user_data.pop(page)
        cls.__user_data.pop(page.get_widget())
        window.disconnect_page(page)
        page.destroy_widget()

        if not any(window.installed_pages):
            window.just_plugin_prefs = False
            window.remove_prefs()
            if len(cls.active_windows) > 1:
                window.destroy()

    def get_current_page(self):
        return self.current_page

    def manual_merge_actions(self, group_name, group, merge_id):
        if group_name is None or merge_id < 0:
            return
        entry = MergedActionEntry()
        entry.action_group = group
        entry.merge_id = merge_id
        self.ui_merge.ensure_update()
        self.merged_actions_table[group_name] = entry

    def merge_actions(self, group_name, actions, toggle_actions, filename, *user_data):
        logging.debug("Merging actions %s from file %s" % (group_name, filename))
        if group_name is None or actions is None or len(actions) <= 0 or filename is None:
            return
        data = MainWindowActionData()
        data.window = self
        data.data = user_data
        entry = MergedActionEntry
        entry.action_group = Gtk.ActionGroup.new(group_name)
        # entry.action_group.set_translation_domain(gettext.)
        for name, stock_id, label, accelerator, tooltip, callback in actions:
            action = Gtk.Action(name, label, tooltip, None)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                action.connect('activate', callback, data)
            entry.action_group.add_action_with_accel(action, accelerator)
        if toggle_actions is not None and len(toggle_actions) > 0:
            for name, stock_id, label, accelerator, tooltip, callback, is_active in toggle_actions:
                action = Gtk.ToggleAction(name, label, tooltip, None)
                action.set_active(is_active)
                if stock_id is not None:
                    action.set_icon_name(stock_id)
                if callback is not None:
                    action.connect('toggled', callback)
                entry.action_group.add_action_with_accel(action, accelerator)

        self.ui_merge.insert_action_group(entry.action_group, 0)
        entry.merge_id = self.ui_merge.add_ui_from_resource('/org/jonah/Gasstation/ui/' + filename)
        if entry.merge_id:
            self.ui_merge.ensure_update()
            self.merged_actions_table[group_name] = entry
        else:
            logging.debug("error occured while trying to merge actions")

    def unmerge_actions(self, group_name):
        logging.debug("Unmerging actions %s" % group_name)
        if self.merged_actions_table is None: return
        entry = self.merged_actions_table.get(group_name, None)
        if entry is None:
            return
        self.ui_merge.remove_action_group(entry.action_group)
        self.ui_merge.remove_ui(entry.merge_id)
        self.ui_merge.ensure_update()
        self.merged_actions_table.pop(group_name)

    def actions_updated(self):
        force = Gtk.ActionGroup.new("force_update")
        self.ui_merge.insert_action_group(force, 0)
        self.ui_merge.ensure_update()
        self.ui_merge.remove_action_group(force)

    def find_action(self, name):
        groups = self.ui_merge.get_action_groups()
        for tmp in groups:
            action = tmp.get_action(name)
            if action is not None:
                return action

    def lookup_group(self, group_name):
        if self.merged_actions_table is None:
            return
        entry = self.merged_actions_table.get(group_name, None)
        if entry is None:
            return None
        return entry.action_group

    def get_action_group(self, group_name):
        return self.lookup_group(group_name)

    def update_tab_position(self, prefs, pref):
        logging.debug("Updating tab position")
        position = Gtk.PositionType.TOP
        if gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_TAB_POSITION_BOTTOM):
            position = Gtk.PositionType.BOTTOM
        elif gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_TAB_POSITION_LEFT):
            position = Gtk.PositionType.LEFT
        elif gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_TAB_POSITION_RIGHT):
            position = Gtk.PositionType.RIGHT
        self.notebook.set_tab_pos(position)

    def update_edit_actions_sensitivity(self, hide):
        widget = self.get_focus()
        page = self.current_page
        if page is not None:
            page.update_edit_menu_actions(hide)
            return
        if isinstance(widget, Gtk.Editable):
            has_selection = widget.get_selection_bounds(None, None)
            can_copy = has_selection
            can_cut = has_selection
            can_paste = True
        elif isinstance(widget, Gtk.TextView):
            text_buffer = widget.get_buffer()
            has_selection = text_buffer.get_selection_bounds()
            can_copy = has_selection
            can_cut = has_selection
            can_paste = True
        else:
            can_copy = can_cut = can_paste = False
        action = self.find_action("EditCopyAction")
        action.set_sensitive(can_copy)
        action.set_visible(not hide or can_copy)
        action = self.find_action("EditCutAction")
        action.set_sensitive(can_cut)
        action.set_visible(not hide or can_cut)
        action = self.find_action("EditPasteAction")
        action.set_sensitive(can_paste)
        action.set_visible(not hide or can_paste)

    def enable_edit_actions_sensitivity(self):
        action = self.find_action("EditCopyAction")
        action.set_sensitive(True)
        action.set_visible(True)
        action = self.find_action("EditCutAction")
        action.set_sensitive(True)
        action.set_visible(True)
        action = self.find_action("EditPasteAction")
        action.set_sensitive(True)
        action.set_visible(True)

    def edit_menu_show_cb(self, menu):
        self.update_edit_actions_sensitivity(False)

    def edit_menu_hide_cb(self, menu):
        self.enable_edit_actions_sensitivity()

    def init_menu_updaters(self):
        edit_menu_item = self.ui_merge.get_widget("/menubar/Edit")
        edit_menu = edit_menu_item.get_submenu()
        edit_menu.connect("show", self.edit_menu_show_cb)
        edit_menu.connect("hide", self.edit_menu_hide_cb)

    def set_tip(self, widget):
        data = self.get_user_data(widget, "action-status")
        if data is not None:
            tooltip = data.action.get_property("tooltip")
            data.statusbar.push(0, tooltip if tooltip else "")

    def unset_tip(self, widget):
        data = self.get_user_data(widget, "action-status")
        if data is not None:
            data.statusbar.pop(0)

    def connect_proxy(self, _, action, proxy, statusbar):
        if isinstance(proxy, (Gtk.MenuItem, Gtk.ImageMenuItem)):
            data = self.get_user_data(proxy, "action-status")
            if data is not None:
                data.action = action
                data.statusbar = statusbar
            else:
                data = ActionStatus()
                data.action = action
                data.statusbar = statusbar
                self.set_user_data(proxy, "action-status", data)
            proxy.connect("select", self.set_tip)
            proxy.connect("deselect", self.unset_tip)

    def window_menu(self):
        self.ui_merge.add_ui_from_resource("/org/jonah/Gasstation/ui/gs-windows-menu-ui.xml")
        self.action_group.add_radio_actions(entries=radio_entries, value=0, on_change=self.cmd_window_raise)

    def page_focus_in(self, widget, event):
        page = self.get_current_page()
        self.emit("page_changed", page)
        return False

    def setup_window(self):
        logging.debug("Setting up window")
        self.connect("delete-event", self.delete_event)
        main_vbox = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        main_vbox.set_homogeneous(False)
        main_vbox.show()
        self.add(main_vbox)
        self.menu_dock = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        # self.horizontal_box = Gtk.Box.new(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.menu_dock.set_homogeneous(False)
        self.menu_dock.show()
        main_vbox.pack_start(self.menu_dock, False, True, 0)
        self.notebook = Gtk.Notebook()
        self.notebook.set_property("scrollable", True)
        self.notebook.set_property("enable-popup", True)
        self.notebook.show()
        self.notebook.connect("switch-page", self.switch_page)
        self.notebook.connect("page-reordered", self.page_reordered)
        self.notebook.connect("focus-in-event", self.page_focus_in)

        # self.horizontal_box.pack_start(self.notebook, True, True, 0)
        main_vbox.pack_start(self.notebook, True, True, 0)
        self.statusbar = Gtk.Statusbar.new()
        self.statusbar.show()
        main_vbox.pack_start(self.statusbar, False, True, 0)
        self.progressbar = Gtk.ProgressBar.new()
        self.progressbar.set_text(" ")
        self.progressbar.show()
        self.statusbar.pack_start(self.progressbar, False, True, 0)
        self.progressbar.set_pulse_step(0.01)
        self.ui_merge = Gtk.UIManager()

        self.action_group = Gtk.ActionGroup.new(name="MainWindowActions")
        # self.action_group.set_translation_domain( self.action_group , GETTEXT_PACKAGE )
        for name, stock_id, label, accelerator, tooltip, callback in self.gs_menu_actions:
            action = Gtk.Action(name, label, tooltip, None)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                action.connect('activate', callback)
            self.action_group.add_action_with_accel(action, accelerator)

        for name, stock_id, label, accelerator, tooltip, callback, is_active in self.toggle_actions:
            action = Gtk.ToggleAction(name, label, tooltip, None)
            action.set_active(is_active)
            if stock_id is not None:
                action.set_icon_name(stock_id)
            if callback is not None:
                action.connect('toggled', callback)
            self.action_group.add_action_with_accel(action, accelerator)
        update_actions(self.action_group, initially_insensitive_actions, "sensitive", False)
        update_actions(self.action_group, always_insensitive_actions, "sensitive", False)
        update_actions(self.action_group, always_hidden_actions, "visible", False)
        set_important_actions(self.action_group, gs_menu_important_actions)
        self.ui_merge.insert_action_group(self.action_group, 0)
        self.ui_merge.connect("add_widget", self.add_widget)
        self.ui_merge.connect("connect-proxy", self.connect_proxy, self.statusbar)
        merge_id = self.ui_merge.add_ui_from_resource('/org/jonah/Gasstation/ui/gs-main-window-ui.xml')
        if merge_id:
            self.add_accel_group(self.ui_merge.get_accel_group())
            self.ui_merge.ensure_update()
        self.window_menu()
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_TAB_POSITION_TOP, self.update_tab_position)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_TAB_POSITION_BOTTOM, self.update_tab_position)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_TAB_POSITION_LEFT, self.update_tab_position)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_TAB_POSITION_RIGHT, self.update_tab_position)
        self.update_tab_position(None, None)
        self.init_menu_updaters()
        if not gs_pref_is_extra_enabled():
            action = self.action_group.get_action("ExtensionsAction")
            action.set_visible(False)

        for p in Plugin.__subclasses__():
            self.add_plugin(p())

    def add_widget(self, merge, widget):
        if isinstance(widget, Gtk.Toolbar):
            self.toolbar = widget
            self.toolbar.set_style(Gtk.ToolbarStyle.BOTH)
            self.toolbar.set_icon_size(Gtk.IconSize.SMALL_TOOLBAR)
        self.menu_dock.pack_start(widget, False, False, 0)
        widget.show()

    def show_summary_bar(self, action):
        if action is None:
            action = self.find_action("ViewSummaryAction")
        if action is None:
            return True
        return action.get_active()

    def switch_page(self, notebook, notebook_page, pos):
        if self.current_page is not None:
            page = self.current_page
            page.unmerge_actions(self.ui_merge)
            page.unselected()
        child = notebook.get_nth_page(pos)
        if child:
            page = self.get_user_data(child, PLUGIN_PAGE_LABEL)
        else:
            page = None
        self.current_page = page
        if page is not None:
            page.merge_actions(self.ui_merge)
            visible = self.show_summary_bar(None)
            page.show_summary_bar(visible)
            page.selected()
            self.update_status(page)
            if page in self.usage_order:
                self.usage_order.remove(page)
            self.usage_order.insert(0, page)
            update_actions(self.action_group, multiple_page_actions, "sensitive", len(self.installed_pages) > 2)

            self.update_title()
            self.update_menu_item()
            self.emit("page_changed", page)

    def page_reordered(self, notebook, child, pos):
        if child is None: return
        page = self.get_user_data(child, PLUGIN_PAGE_LABEL)
        if page is None: return
        old_link = self.installed_pages.count(page)
        if not old_link: return
        self.installed_pages.remove(page)
        self.installed_pages.insert(pos, page)

    def cmd_page_setup(self, _):
        from gasstation.utilities.print_settings import PrintOperation
        window = self.get_gtk_window()
        PrintOperation.page_setup_dialog(window)

    @staticmethod
    def book_options_dialog_apply_helper(options):
        book = Session.get_current_book()
        use_split_action_for_num_before = book.use_split_action_for_num_field()
        use_book_currency_before = book.use_currency()
        use_read_only_threshold_before = book.get_num_of_days_auto_readonly()
        return_val = False
        if options is None:
            return return_val
        results = options.commit()
        for mess in results:
            dialog = Gtk.MessageDialog(transient_for=MainWindow.get_main_window(None), modal=True,
                                       destroy_with_parent=True,
                                       message_type=Gtk.MessageType.ERROR,
                                       buttons=Gtk.ButtonsType.OK,
                                       message_format=mess)
            dialog.run()
            dialog.destroy()
        book.begin_edit()
        options.save(book, True)
        book.commit_edit()
        use_split_action_for_num_after = book.use_split_action_for_num_field()
        use_book_currency_after = book.use_currency()
        book.cached_num_days_auto_readonly_isvalid = False
        use_read_only_threshold_after = book.get_num_of_days_auto_readonly()

        if use_split_action_for_num_before != use_split_action_for_num_after:
            book.option_num_field_source_changed_cb(use_split_action_for_num_after)
            return_val = True
        if use_book_currency_before != use_book_currency_after:
            book.option_book_currency_selected_cb(use_book_currency_after)
            return_val = True
        if use_read_only_threshold_before != use_read_only_threshold_after:
            return_val = True
        book.commit_edit()
        return return_val

    @staticmethod
    def book_options_dialog_apply_cb(optionwin, options):
        if options is None:
            return
        if MainWindow.book_options_dialog_apply_helper(options):
            ComponentManager.refresh_all()

    @staticmethod
    def book_options_dialog_close_cb(optionwin, options):
        optionwin.destroy()
        options.destroy()

    @staticmethod
    def show_handler(class_name, component_id, optwin, *args):
        if optwin is None:
            return False
        widget = optwin.window
        widget.present()
        return True

    @classmethod
    def book_options_dialog_cb(cls, modal, title, parent):
        from gasstation.views.dialogs.options import OptionDB, OptionDialog
        book = Session.get_current_book()
        options = OptionDB.new_for_type(ID_BOOK)
        options.load(book)
        options.clean()
        if ComponentManager.forall(DIALOG_BOOK_OPTIONS_CM_CLASS, cls.show_handler, None):
            return None
        optionwin = OptionDialog.new_modal(modal, title if title is not None else "Book Options",
                                           DIALOG_BOOK_OPTIONS_CM_CLASS, parent)
        optionwin.build_contents(options)
        # optionwin.set_book_options_help_cb()

        optionwin.set_apply_cb(cls.book_options_dialog_apply_cb, options)
        optionwin.set_close_cb(cls.book_options_dialog_close_cb, options)
        # if modal:
        #     options_dialog_set_new_book_option_values (options);
        return optionwin.window

    def cmd_file_properties(self, action):
        self.book_options_dialog_cb(False, None, self)

    def cmd_file_close(self, action):
        page = self.current_page
        self.close_page(page)

    def cmd_file_quit(self, action):
        if not self.all_finish_pending():
            return
        self.quit()

    def cmd_edit_cut(self, action):
        widget = self.get_focus()
        if isinstance(widget, Gtk.Editable):
            widget.cut_clipboard()
        elif isinstance(widget, Gtk.TextView):
            text_buffer = widget.get_buffer()
            clipboard = text_buffer.get_clipboard(Gdk.SELECTION_CLIPBOARD)
            editable = widget.get_editable()
            text_buffer.cut_clipboard(clipboard, editable)

    def cmd_edit_copy(self, action):
        widget = self.get_focus()
        if isinstance(widget, Gtk.Editable):
            widget.copy_clipboard()
        elif isinstance(widget, Gtk.TextView):
            text_buffer = widget.get_buffer()
            clipboard = text_buffer.get_clipboard(Gdk.SELECTION_CLIPBOARD)
            text_buffer.copy_clipboard(clipboard)

    def cmd_edit_paste(self, action):
        widget = self.get_focus()
        if isinstance(widget, Gtk.Editable):
            widget.paste_clipboard()
        elif isinstance(widget, Gtk.TextView):
            text_buffer = widget.get_buffer()
            clipboard = text_buffer.get_clipboard(Gdk.SELECTION_CLIPBOARD)
            text_buffer.paste_clipboard(clipboard, None, False)

    def cmd_edit_preferences(self, _):
        PreferencesDialog(self)

    def cmd_view_refresh(self, action):
        pass

    def cmd_actions_reset_warnings(self, _):
        ResetWarningsDialog(self)

    def cmd_actions_rename_page(self, action):
        page = self.current_page
        if page is None: return
        if not self.find_tab_items(page):
            return
        label, entry = self.find_tab_items(page)
        entry.set_text(label.get_text())
        entry.select_region(0, -1)
        label.hide()
        entry.show()
        entry.grab_focus()

    def cmd_view_toolbar(self, action):
        if action.get_active():
            self.toolbar.show()
        else:
            self.toolbar.hide()

    def cmd_view_summary(self, action):
        visible = self.show_summary_bar(action)
        for item in self.installed_pages:
            item.show_summary_bar(visible)

    def cmd_view_statusbar(self, action):
        if action.get_active():
            self.statusbar.show()
        else:
            self.statusbar.hide()

    def cmd_window_new(self, action):
        new_window = MainWindow()
        new_window.show()

    def cmd_help_tutorial(self, action):
        pass

    def cmd_window_move_page(self, _):
        page = self.current_page
        if page is None: return
        if page.get_widget() is None: return
        notebook = self.notebook
        tab_widget = notebook.get_tab_label(page.get_widget())
        menu_widget = notebook.get_menu_label(page.get_widget())
        self.disconnect_page(page)
        new_window = MainWindow()
        new_window.show()
        new_window.connect_page(page, tab_widget, menu_widget)

    def cmd_window_raise(self, _, current):
        value = current.get_current_value()
        new_window = MainWindow.active_windows[value]
        new_window.present()
        GLib.idle_add(self.update_radio_button)

    def cmd_help_about(self, action):
        from gasstation.views.dialogs.about import AboutDialog
        about_dialog = AboutDialog(self)
        about_dialog.run()
        about_dialog.destroy()

    @staticmethod
    def url_signal_cb(dialog, uri, data):
        # gs_launch_assoc( uri )
        return True

    @classmethod
    def show_all_windows(cls):
        for window in cls.active_windows:
            window.show()

    @staticmethod
    def get_gtk_window(widget=None):
        if widget is None:
            return None
        return widget.get_toplevel()

    @classmethod
    def get_main_window(cls, widget):
        toplevel = cls.get_gtk_window(widget)
        while toplevel is not None:
            toplevel = toplevel.get_transient_for()
        if toplevel is not None:
            return toplevel
        for window in cls.active_windows:
            if window.is_active():
                return window

        for window in cls.active_windows:
            if window.get_mapped():
                return window
        return

    def get_statusbar(self):
        return self.statusbar

    def get_progressbar(self):
        return self.progressbar

    def ui_set_sensitive(self, sensitive):
        for window in MainWindow.active_windows:
            groups = window.ui_merge.get_action_groups()
            for group in groups:
                group.set_sensitive(sensitive)

            for page in window.installed_pages:
                close_button = page.get_user_data(page, PLUGIN_PAGE_CLOSE_BUTTON)
                if close_button is None:
                    continue
                close_button.set_sensitive(sensitive)

    @staticmethod
    def do_popup_menu(page, event):
        ui_merge = page.get_ui_merge()
        if ui_merge is None: return
        menu = ui_merge.get_widget("/MainPopup")
        if menu is None: return
        menu.popup_at_pointer(event)

    def popup_menu_cb(self, widget, page):
        if self.do_popup_menu(page, None):
            return True
        return False

    @staticmethod
    def button_press_cb(widget, event, page):
        if event.button == 3 and event.type == Gdk.EventType.BUTTON_PRESS:
            if MainWindow.do_popup_menu(page, event):
                return True
        return False

    def get_uimanager(self):
        return self.ui_merge

    def do_destroy(self):
        MainWindow.active_windows.remove(self)
        if self.merged_actions_table:
            while self.current_page is not None:
                self.close_page(self.current_page)
            if self.get_progressbar_window() == self:
                self.set_progressbar_window(None)
            MainWindow.update_all_menu_items()
            self.remove_prefs()
            Event.unregister_handler(self.event_handler_id)
            self.event_handler_id = 0
            self.merged_actions_table.clear()
            self.merged_actions_table = None
            Tracking.forget(self)
        super().destroy()

    def remove_prefs(self):
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_TAB_COLOR,
                                  self.update_tab_color)

        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_SHOW_CLOSE_BUTTON,
                                  self.update_tab_close)

        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_TAB_WIDTH,
                                  self.update_tab_width)

        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_TAB_POSITION_TOP,
                                  self.update_tab_position)

        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_TAB_POSITION_BOTTOM,
                                  self.update_tab_position)

        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_TAB_POSITION_LEFT,
                                  self.update_tab_position)

        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL,
                                  PREF_TAB_POSITION_RIGHT,
                                  self.update_tab_position)

        if gs_pref_get_reg_negative_color_pref_id() > 0 and self.quiting:
            gs_pref_remove_cb_by_id(PREFS_GROUP_GENERAL, gs_pref_get_reg_negative_color_pref_id())
            gs_pref_set_reg_negative_color_pref_id(0)

        if gs_pref_get_reg_auto_raise_lists_id() > 0 and self.quiting:
            gs_pref_remove_cb_by_id(PREFS_GROUP_GENERAL_REGISTER, gs_pref_get_reg_auto_raise_lists_id())
            gs_pref_set_reg_auto_raise_lists_id(0)

    def __repr__(self):
        return "<MainWindow %d>" % id(self)


GObject.type_register(MainWindow)
