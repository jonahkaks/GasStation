from gasstation.models.nozzle import *
from gasstation.views.tree import *


class NozzleViewColumn(GObject.GEnum):
    NAME = 0
    PUMP = 1
    TANK = 2


class NozzleView(TreeView):
    __gtype_name__ = "NozzleView"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        model = NozzleModel.new()
        self.add_text_column(0, title="Name", pref_name="name",
                             sizing_text="NOZZLE 2", data_col=NozzleModelColumn.NAME,
                             visible_default=True, editable=False)
        self.add_text_column(1, title="Pump", pref_name="pump", sizing_text="Pump1", data_col=NozzleModelColumn.PUMP,
                             visible_default=True, editable=False)
        self.add_text_column(2, title="Tank", pref_name="tank", sizing_text="Tank 1", data_col=NozzleModelColumn.TANK,
                             visible_default=True, editable=False)
        self.add_text_column(2, title="Location", pref_name="location", sizing_text="Kakooge ",
                             data_col=NozzleModelColumn.LOCATION, visible_default=True, editable=False)
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        selection = self.get_selection()
        selection.unselect_all()
        f_model = model.filter_new()
        s_model = Gtk.TreeModelSort(model=f_model)
        self.set_model(s_model)
        self.configure_columns()
        self.expand_columns("name", "pump", "tank")
        self.set_state_section("Nozzle")
        self.show()

    def cdf0(self, col, cell, model, s_iter, userdata):
        cell.set_property("xalign", 1.0)
        viewcol = DataStore.get_user_data(cell, "view_column")
        if viewcol == NozzleViewColumn.NAME:
            cell.set_property("xalign", 0.0)
        cell.set_property("editable", False)
        return False

    @staticmethod
    def get_nozzle_from_iter(s_model, s_iter):
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        return model.get_nozzle(_iter)

    def get_nozzle_from_path(self, path):
        s_model = self.get_model()
        s_iter = s_model.get_iter(path)
        return self.get_nozzle_from_iter(s_model, s_iter)

    def get_selected_nozzles(self):
        selection = self.get_selection()
        s_model, paths = selection.get_selected_rows()
        if s_model is None or paths is None:
            return []
        _iters = map(s_model.get_iter, paths)
        return list(map(lambda _iter: self.get_nozzle_from_iter(s_model, _iter), _iters))


GObject.type_register(NozzleView)
