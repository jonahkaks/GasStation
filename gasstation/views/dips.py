# -*- coding: utf-8 -*-

from gasstation.utilities.warnings import *

from gasstation.models.dips import *
from gasstation.utilities.ui import add_toolbar_label
from gasstation.views.tree import *
from libgasstation.core.helpers import parse_exp_decimal


class DipsViewColumn(GObject.GEnum):
    END_OF_LIST = -1
    TANK = 1
    OPENING = 2
    CLOSING = 3
    TOTAL = 4
    METER = 5
    STOCK_LOSS = 6


class DipsConfirm(GObject.GEnum):
    RESET = 0
    ACCEPT = 1
    DISCARD = 2
    CANCEL = 3


class DipsView(TreeView):
    def __init__(self):
        super(DipsView, self).__init__()
        model = DipsModel.new()
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        selection = self.get_selection()
        selection.unselect_all()
        s_model = Gtk.TreeModelSort(model=model)
        self.set_model(s_model)
        self.add_combo_column(DipsViewColumn.TANK, title="Tank",
                              pref_name="tank", sizing_text="Tank1", visible_always=1, data_func=self.cdf0,
                              edited=self.edited_cb, model_col=0, model=model.get_tank_model(),
                              data_col=DipsModelColumn.TANK, editing_started=self.editing_started)
        self.add_numeric_column(DipsViewColumn.OPENING, title="Opening Dips",
                                data_func=self.cdf0,
                                pref_name="opening", sizing_text="10000000000000", visible_always=1,
                                data_col=DipsModelColumn.OPENING,
                                edited=self.edited_cb, editing_started=self.editing_started)
        self.add_numeric_column(DipsViewColumn.CLOSING, title="Closing Dips",
                                data_func=self.cdf0,
                                pref_name="closing", sizing_text="10000000000000", visible_always=1,
                                edited=self.edited_cb, data_col=DipsModelColumn.CLOSING,
                                editing_started=self.editing_started)
        self.add_numeric_column(DipsViewColumn.TOTAL, title="Sales by Tank", pref_name="dips",
                                data_func=self.cdf0,
                                sizing_text="10000000000000", data_col=DipsModelColumn.TOTAL, visible_always=1)
        self.add_numeric_column(DipsViewColumn.METER, title="Sales by Meter", pref_name="meter", data_func=self.cdf0,
                                sizing_text="10000000000000", visible_always=1, data_col=DipsModelColumn.METER
                                )
        self.add_numeric_column(DipsViewColumn.STOCK_LOSS, title="Stock Loss", pref_name="stockloss",
                                sizing_text="1000000000", data_col=DipsModelColumn.STOCK_LOSS, data_func=self.cdf0,
                                visible_always=1
                                )
        self.configure_columns()
        self.expand_columns("tank", "opening", "closing", "dips", "meter", "stockloss")
        self.set_state_section("Dips")
        hub = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        hub.set_border_width(6)
        sw = Gtk.ScrolledWindow()
        sw.set_shadow_type(Gtk.ShadowType.ETCHED_IN)
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        hub.pack_start(sw, True, True, 0)
        sw.add(self)
        tbar = self.create_toolbar()
        hub.pack_start(tbar, False, False, 0)
        self.container = hub
        # self.jump_to_blank()
        self.update_toolbar()
        self.temp_cr = None
        self.fo_handler_id = 0
        self.dirty_dips = None
        self.current_dips = None

        self.current_date = 0
        self.change_allowed = False
        self.dips_confirm = DipsConfirm.RESET
        self.auto_complete = True
        self.connect("key-press-event", self.key_press_cb)
        gs_widget_set_style_context(self, "DipsView")

    def cdf0(self, col, cell, s_model, s_iter, userdata):
        viewcol = self.get_user_data(cell, "view_column")
        model = self.get_model_from_view()
        spath = s_model.get_path(s_iter)
        editable = True
        indices = spath.get_indices()
        row_color = model.get_row_color(indices[0])
        if row_color is not None:
            cell.set_property("cell-background", row_color)
            cell.set_property("foreground", "black")
        else:
            cell.set_property("cell-background-set", 0)
            cell.set_property("foreground-set", 0)
        cell.set_property("xalign", 1.0)
        if viewcol == DipsViewColumn.TANK:
            cell.set_property("xalign", 0.0)
        elif viewcol == DipsViewColumn.STOCK_LOSS or viewcol == DipsViewColumn.METER \
                or viewcol == DipsViewColumn.TOTAL:
            editable = False
        cell.set_property("editable", editable)

    def edited_cb(self, cell, path_string, new_text):
        if new_text is None:
            return
        editable = self.get_user_data(cell, "cell-editable")
        if self.fo_handler_id != 0:
            editable.disconnect(self.fo_handler_id)
            self.fo_handler_id = 0
        self.grab_focus()
        old_text = self.get_user_data(cell, "current-string")
        if old_text == new_text:
            if not self.stop_cell_move:
                return
        m_iter = self.get_model_iter_from_view_string(path_string)
        model = self.get_model_from_view()
        viewcol = self.get_user_data(cell, "view_column")
        dips = model.get_dips_at_iter(m_iter)
        if dips is None: return
        self.begin_edit(dips)
        if viewcol == DipsViewColumn.TANK:
            tank = model.get_tank_from_name(new_text)
            dips.set_tank(tank)
            last_dips = model.get_last_dips_by_tank(tank, dips.get_date())
            if last_dips is not None:
                dips.set_opening(last_dips.get_closing())

        elif viewcol == DipsViewColumn.OPENING:
            dips.set_opening(parse_exp_decimal(new_text))

        elif viewcol == DipsViewColumn.CLOSING:
            dips.set_closing(parse_exp_decimal(new_text))
        self.update_toolbar()

    def create_toolbar(self):
        tbar = Gtk.Toolbar()
        gs_widget_set_style_context(tbar, "summary-tbar")
        tbar.set_icon_size(Gtk.IconSize.MENU)
        tbar.set_style(Gtk.ToolbarStyle.ICONS)
        tbar.get_style_context().add_class(Gtk.STYLE_CLASS_INLINE_TOOLBAR)
        label = Gtk.Label("Tank Dips")
        label.set_valign(Gtk.Align.CENTER)
        label.set_halign(Gtk.Align.START)

        toolitem = Gtk.ToolItem()
        toolitem.add(label)
        tbar.insert(toolitem, -1)

        toolitem = Gtk.SeparatorToolItem()
        toolitem.set_expand(True)
        toolitem.set_draw(False)
        tbar.insert(toolitem, -1)

        self.dips_total = add_toolbar_label(tbar, "Total Dips:")
        self.stock_total = add_toolbar_label(tbar, "Total Stock Loss:")
        return tbar

    def update_toolbar(self):
        model = self.get_model_from_view()
        l, a = model.get_dips_totals()
        self.dips_total.set_text("{:,.2f}".format(a))
        self.stock_total.set_text("{:,.2f}".format(l))

    def motion_cb(self, sel):
        model = self.get_model_from_view()
        t, m_iter = self.get_model_iter_from_selection(sel)
        if t:
            dips = model.get_dips_at_iter(m_iter)
            old_dips = self.current_dips
            self.current_dips = dips
            model.current_dips = dips
            if dips != old_dips and old_dips == self.dirty_dips:
                if self.dips_changed():
                    return
            if self.dips_confirm == DipsConfirm.CANCEL:
                return

    def begin_edit(self, dips):
        if dips != self.dirty_dips:
            t = dips.get_date()
            if not dips.is_open():
                dips.begin_edit()
            self.dirty_dips = dips
            if t is None or t == 0:
                dips.set_date(self.current_date)

    def enter(self):
        self.finish_edit()
        if self.dips_changed():
            return False
        if self.dips_confirm == DipsConfirm.DISCARD:
            return False
        return True

    def refresh(self, widget, d, loc):
        model = self.get_model_from_view()
        b = model.get_blank_dips()
        if b is not None:
            b.dispose()
        self.current_date = d
        model.refresh(d, loc)
        self.update_toolbar()

    def editing_started(self, cr, editable, path_string):
        viewcol = self.get_user_data(cr, "view_column")
        self.set_user_data(cr, "cell-editable", editable)
        if viewcol == DipsViewColumn.TANK:
            model = self.get_model_from_view()
            model.update_models()
            cat_list = editable.get_model()
            _iter = editable.get_active_iter()
            if _iter is not None:
                text = cat_list.get_value(_iter, 0)
            else:
                text = ""
            self.set_user_data(cr, "current-string", text)
            editable.connect("remove-widget", self.remove_edit_combo)
        else:
            entry = editable
            entry.connect("key-press-event", self.ed_key_press_cb)
            self.set_user_data(cr, "current-string", entry.get_text())
            entry.connect("remove-widget", self.remove_edit_entry)
        self.temp_cr = cr
        self.editing = True
        self.set_user_data(cr, "editing-canceled", False)

    def ed_key_press_cb(self, widget, event):
        spath, col = self.get_cursor()
        if event.type != Gdk.EventType.KEY_PRESS:
            return False
        if event.keyval == Gdk.KEY_Up or event.keyval == Gdk.KEY_Down:
            if spath is None:
                return True
            self.finish_edit()
            if self.stop_cell_move:
                return True
            if event.keyval == Gdk.KEY_Up:
                spath.prev()
            else:
                spath.next()
            self.set_cursor(spath, col, True)
        elif event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return:
            if spath is None:
                return True
            if self.enter():
                GLib.idle_add(self.jump_to_blank)
            return True

    def jump_to_blank(self):
        model = self.get_model_from_view()
        bdips = model.get_blank_dips()
        model.current_dips = bdips
        spath = model.get_path_from_dips(bdips)
        self.scroll_to_cell(spath, None, False, 1.0, 0.0)
        self.set_cursor(spath, None, False)

    def dips_changed(self):
        spath, col = self.get_cursor()
        if spath is None:
            return False
        self.dips_confirm = DipsConfirm.RESET
        if self.get_user_data(self, "data-edited") and self.dips_changed_confirm(None):
            if self.dips_confirm == DipsConfirm.CANCEL:
                return True
            if self.dips_confirm == DipsConfirm.DISCARD:
                self.dirty_dips = None
        return False

    def dips_changed_confirm(self, new_dips):
        window = self.get_toplevel()
        title = "Save the changed dips?"
        message = "The current dips has changed. Would you like to " \
                  "record the changes, or discard the changes?"
        if self.dirty_dips is None or self.dirty_dips == new_dips:
            return False
        dialog = Gtk.MessageDialog(transient_for=window,
                                   modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE, title=title)
        dialog.format_secondary_text("%s" % message)
        dialog.add_buttons("_Discard Changes", Gtk.ResponseType.REJECT,
                           "_Cancel", Gtk.ResponseType.CANCEL,
                           "_Record Changes", Gtk.ResponseType.ACCEPT)
        response = gs_dialog_run(dialog, PREF_WARN_DIPS_MOD)
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            self.set_user_data(self, "data-edited", False)
            self.dirty_dips.commit_edit()
            self.dirty_dips = None
            self.change_allowed = False
            self.auto_complete = False
            self.dips_confirm = DipsConfirm.ACCEPT
            return False

        elif response == Gtk.ResponseType.REJECT:
            if self.dirty_dips is not None:
                self.set_user_data(self, "data-edited", False)
                self.change_allowed = False
                self.auto_complete = False
                self.dips_confirm = DipsConfirm.DISCARD
            return True

        elif response == Gtk.ResponseType.CANCEL:
            self.dips_confirm = DipsConfirm.CANCEL
            return True
        else:
            return False

    def fuel_changed_cb(self, widget, fuel):
        model = self.get_model_from_view()
        model.update_values(fuel)

    def key_press_cb(self, widget, event):
        editing = False
        step_off = False
        dips_changed = False
        spath, col = self.get_cursor()
        if event.type != Gdk.EventType.KEY_PRESS:
            return False
        if event.keyval in [Gdk.KEY_plus, Gdk.KEY_minus, Gdk.KEY_KP_Add, Gdk.KEY_KP_Subtract]:
            return True

        elif event.keyval in [Gdk.KEY_Up, Gdk.KEY_Down]:

            if event.keyval == Gdk.KEY_Up:
                pass
            return False

        elif event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return:
            if spath is None:
                return True
            if self.enter():
                GLib.idle_add(self.jump_to_blank)
            return True

        elif event.keyval in [Gdk.KEY_Tab, Gdk.KEY_ISO_Left_Tab, Gdk.KEY_KP_Tab]:
            if spath is None:
                return True
            if event.state & Gdk.ModifierType.CONTROL_MASK:
                self.auto_complete = True
            self.finish_edit()
            if self.stop_cell_move:
                return True
            while not editing and not step_off:
                start_spath = spath.copy()
                start_indices = start_spath.get_indices()
                ncol = self.keynav(col, spath, event)
                next_indices = spath.get_indices()
                if start_indices[0] != next_indices[0]:
                    if self.dirty_dips is not None:
                        dips_changed = True
                    self.change_allowed = False

                if spath is None or not self.path_is_valid(spath) or dips_changed:
                    if self.dips_changed():
                        return True
                    step_off = True
                if self.dips_confirm != DipsConfirm.DISCARD:
                    self.set_cursor(spath, ncol, True)
                editing = self.get_editing(ncol)
            return True
        else:
            return False

    def destroy(self):
        model = self.get_model_from_view()
        model.destroy()
        super().destroy()


GObject.type_register(DipsView)
