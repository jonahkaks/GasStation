from gasstation.utilities.accounting_period import *
from gasstation.utilities.dialog import gs_widget_set_style_context
from gasstation.utilities.ui import *
from libgasstation.core.account import ID_ACCOUNT, AccountType
from libgasstation.core.component import *
from libgasstation.core.price_db import PriceDB

WINDOW_summary_bar_CM_CLASS = "summary-bar"
PREFS_GROUP = "window.pages.account-tree.summary"
PREF_GRAND_TOTAL = "grand-total"
PREF_NON_CURRENCY = "non-currency"

TOTAL_SINGLE = 0
TOTAL_CURR_TOTAL = 1
TOTAL_NON_CURR_TOTAL = 2
TOTAL_GRAND_TOTAL = 3


def CLAMP(x, low, high):
    return high if x > high else low if x < low else x


COLUMN_MNEMONIC_TYPE = 0
COLUMN_ASSETS = 1
COLUMN_ASSETS_VALUE = 2
COLUMN_PROFITS = 3
COLUMN_PROFITS_VALUE = 4
COLUMN_ASSETS_NEG = 5
COLUMN_PROFITS_NEG = 6
N_COLUMNS = 7


class CurrencyAcc:
    def __init__(self):
        self.currency = None
        self.assets = Decimal(0)
        self.profits = Decimal(0)
        self.total_mode = 0

class MainSummary:
    def __init__(self):
        self.hbox = None
        self.totals_combo = None
        self.datamodel = None
        self.component_id = 0
        self.cnxn_id = 0
        self.combo_popped = False
        self.show_negative_color = False
        self.negative_color = self.get_negative_color_str()
        self.user_data = defaultdict(dict)

    def get_user_data(self, obj, key):
        return self.user_data.get(obj, {}).get(key, None)

    def set_user_data(self, obj, key, userdata):
        self.user_data[obj][key] = userdata

    @staticmethod
    def get_currency_accumulator(a, currency, total_mode):
        for found in a:
            if currency.equiv(found.currency) and found.total_mode == total_mode:
                return found
        found = CurrencyAcc()
        found.currency = currency
        found.assets = Decimal(0)
        found.profits = Decimal(0)
        found.total_mode = total_mode
        a.append(found)
        return found

    @staticmethod
    def accounts_recurse(parent, currency_list, options):
        currency_accum = None
        grand_total_accum = None
        non_curr_accum = None
        non_currency = False
        if parent is None:
            return
        book = Session.get_current_book()
        price_db = PriceDB.get_db(book)
        for account in parent.children:
            to_curr = options.default_currency
            account_type = account.get_type()
            account_currency = account.get_commodity()
            if options.grand_total:
                grand_total_accum = MainSummary.get_currency_accumulator(currency_list, to_curr, TOTAL_GRAND_TOTAL)

            if not account_currency.is_currency():
                non_currency = True
                non_curr_accum = MainSummary.get_currency_accumulator(currency_list, to_curr, TOTAL_NON_CURR_TOTAL)

            if not non_currency or options.non_currency:
                currency_accum = MainSummary.get_currency_accumulator(currency_list, account_currency, TOTAL_SINGLE)

            if account_type in [AccountType.BANK,
                                AccountType.CASH,
                                AccountType.CURRENT_ASSET,
                                AccountType.FIXED_ASSET,
                                AccountType.STOCK,
                                AccountType.MUTUAL,
                                AccountType.CREDIT,
                                AccountType.CURRENT_LIABILITY,
                                AccountType.LONG_TERM_LIABILITY,
                                AccountType.PAYABLE,
                                AccountType.RECEIVABLE]:
                end_amount = account.get_balance_as_of_date(options.end_date.date())
                end_amount_default_currency = price_db.convert_balance_nearest_price_t64(end_amount,
                                                                                         account_currency,
                                                                                         to_curr,
                                                                                         options.end_date)

                if not non_currency or options.non_currency:
                    currency_accum.assets += end_amount

                if non_currency:
                    non_curr_accum.assets += end_amount_default_currency

                if options.grand_total:
                    grand_total_accum.assets += end_amount_default_currency

                MainSummary.accounts_recurse(account, currency_list, options)

            elif account_type in [AccountType.INCOME, AccountType.EXPENSE]:
                start_amount = account.get_balance_as_of_date(options.start_date.date())
                start_amount_default_currency = price_db.convert_balance_nearest_price_t64(start_amount,
                                                                                           account_currency,
                                                                                           to_curr, options.start_date)
                end_amount = account.get_balance_as_of_date(options.end_date.date())
                end_amount_default_currency = price_db.convert_balance_nearest_price_t64(end_amount, account_currency,
                                                                                         to_curr, options.end_date)

                if not non_currency or options.non_currency:
                    currency_accum.profits += start_amount
                    currency_accum.profits -= end_amount
                if non_currency:
                    non_curr_accum.profits += start_amount_default_currency
                    non_curr_accum.profits -= end_amount_default_currency
                if options.grand_total:
                    grand_total_accum.profits += start_amount_default_currency
                    grand_total_accum.profits -= end_amount_default_currency

                MainSummary.accounts_recurse(account, currency_list, options)

    @staticmethod
    def get_total_mode_label(mnemonic, total_mode):
        if total_mode == TOTAL_CURR_TOTAL:
            label_str = "%s, Total:" % mnemonic
        elif total_mode == TOTAL_NON_CURR_TOTAL:
            label_str = "%s, Non Currency Commodities Total:" % mnemonic
        elif total_mode == TOTAL_GRAND_TOTAL:
            label_str = "%s, Grand Total:" % mnemonic
        else:
            label_str = "%s:" % mnemonic
        return label_str

    def refresh(self):
        class SummaryOptions:
            def __init__(self):
                self.default_currency = None
                self.grand_total = False
                self.non_currency = False
                self.start_date = 0
                self.end_date = 0
        root = Session.get_current_root()
        options = SummaryOptions()
        options.default_currency = gs_default_currency()
        if options.default_currency is None:
            options.default_currency = root.get_commodity()
        options.grand_total = gs_pref_get_bool(PREFS_GROUP, PREF_GRAND_TOTAL)
        options.non_currency = gs_pref_get_bool(PREFS_GROUP, PREF_NON_CURRENCY)
        options.start_date = datetime.datetime.combine(AccountingPeriod.fiscal_start(),
                                                       datetime.time(hour=0, minute=0, second=0, microsecond=0))
        options.end_date = datetime.datetime.combine(AccountingPeriod.fiscal_end(),
                                                     datetime.time(hour=23, minute=59, second=59, microsecond=99))
        currency_list = []
        if options.grand_total:
            MainSummary.get_currency_accumulator(currency_list, options.default_currency, TOTAL_GRAND_TOTAL)
        MainSummary.get_currency_accumulator(currency_list, options.default_currency, TOTAL_SINGLE)
        MainSummary.accounts_recurse(root, currency_list, options)
        self.totals_combo.set_model(None)
        self.datamodel.clear()
        for currency_accum in currency_list:
            mnemonic = currency_accum.currency.get_nice_symbol()
            if mnemonic is None:
                mnemonic = ""
            asset_amount_string = sprintamount(currency_accum.assets,
                                               PrintAmountInfo.commodity(currency_accum.currency, True))
            profit_amount_string = sprintamount(currency_accum.profits,
                                                PrintAmountInfo.commodity(currency_accum.currency, True))

            _iter = self.datamodel.append()
            total_mode_label = self.get_total_mode_label(mnemonic, currency_accum.total_mode)
            self.datamodel.set(_iter, COLUMN_MNEMONIC_TYPE, total_mode_label,
                               COLUMN_ASSETS, "Net Assets:",
                               COLUMN_ASSETS_VALUE, asset_amount_string,
                               COLUMN_ASSETS_NEG, currency_accum.assets < 0,
                               COLUMN_PROFITS, "Profits:",
                               COLUMN_PROFITS_VALUE, profit_amount_string,
                               COLUMN_PROFITS_NEG, currency_accum.profits < 0)

        self.totals_combo.set_model(self.datamodel)
        self.totals_combo.set_active(0)

    @staticmethod
    def get_negative_color_str():
        label = Gtk.Label("Color")
        context = label.get_style_context()
        context.add_class("negative-numbers")
        rgba = context.get_color(Gtk.StateFlags.NORMAL)
        color_str = "#%02X%02X%02X" % (int(0.5 + CLAMP(rgba.red, 0., 1.) * 255.),
                                       int(0.5 + CLAMP(rgba.green, 0., 1.) * 255.),
                                       int(0.5 + CLAMP(rgba.blue, 0., 1.) * 255.))
        return color_str

    def update_color(self, gsettings, key):
        self.negative_color = self.get_negative_color_str()
        self.show_negative_color = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED)
        GLib.idle_add(self.refresh, priority=GLib.PRIORITY_HIGH_IDLE)

    def destroy_cb(self, *_):
        gs_pref_remove_cb_by_id(PREFS_GROUP, self.cnxn_id)
        if self.component_id > 0:
            ComponentManager.unregister(self.component_id)
            self.component_id = 0
        gs_pref_remove_cb_by_func(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED,
                                  self.update_color)

    def refresh_handler(self, changes):
        GLib.idle_add(self.refresh, priority=GLib.PRIORITY_HIGH_IDLE)

    def prefs_changed_cb(self, prefs, pref):
        GLib.idle_add(self.refresh, priority=GLib.PRIORITY_HIGH_IDLE)

    @staticmethod
    def check_string_for_markup(ret_string):
        if ret_string.find("&") != -1:
            strings = ret_string.split("&")
            ret_string = "&amp;".join(strings)

        if ret_string.find("<") != -1:
            strings = ret_string.split("<")
            ret_string = "&lt;".join(strings)

        if ret_string.find(">") != -1:
            strings = ret_string.split(">")

            ret_string = "&gt;".join(strings)

        if ret_string.find("\"") != -1:
            strings = ret_string.split("\"")

            ret_string = "&quot;".join(strings)

        if ret_string.find("'") != -1:
            strings = ret_string.split("'")
            ret_string = "&apos;".join(strings)

        return ret_string

    def cdf(self, cell_layout, cell, tree_model, _iter):
        viewcol = self.get_user_data(cell, "view_column")
        if self.combo_popped:
            cell.set_property("xalign", 0.0)
        else:
            cell.set_property("xalign", 0.5)

        _type = tree_model.get_value(_iter, COLUMN_MNEMONIC_TYPE)
        assets = tree_model.get_value(_iter, COLUMN_ASSETS)
        assets_val = tree_model.get_value(_iter, COLUMN_ASSETS_VALUE)
        profits = tree_model.get_value(_iter, COLUMN_PROFITS)
        profits_val = tree_model.get_value(_iter, COLUMN_PROFITS_VALUE)
        assets_neg = tree_model.get_value(_iter, COLUMN_ASSETS_NEG)
        profits_neg = tree_model.get_value(_iter, COLUMN_PROFITS_NEG)
        if viewcol == 0:
            cell.set_property("text", _type)

        if viewcol == 2:
            checked_string = self.check_string_for_markup(assets_val)
            if self.show_negative_color and assets_neg:
                a_string = assets + " <span foreground='" + self.negative_color + "'>" + checked_string + "</span>"
            else:
                a_string = assets + " " + checked_string
            cell.set_property("markup", a_string)

        if viewcol == 4:
            checked_string = self.check_string_for_markup(profits_val)
            if self.show_negative_color and profits_neg:
                p_string = profits + " <span foreground='" + self.negative_color + "'>" + checked_string + "</span>"
            else:
                p_string = profits + " " + checked_string
            cell.set_property("markup", p_string)

    def combo_popped_cb(self, widget, *args):
        self.combo_popped = not self.combo_popped

    @classmethod
    def new(cls):
        retval = cls()
        retval.datamodel = Gtk.ListStore(str, str, str, str, str, bool, bool)
        retval.hbox = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        retval.hbox.set_homogeneous(False)
        gs_widget_set_style_context(retval.hbox, "summary-bar")
        retval.totals_combo = Gtk.ComboBox.new_with_model(retval.datamodel)
        retval.negative_color = cls.get_negative_color_str()
        retval.show_negative_color = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED)
        gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED, retval.update_color)
        retval.component_id = ComponentManager.register(WINDOW_summary_bar_CM_CLASS, retval.refresh_handler, None)
        ComponentManager.watch_entity_type(retval.component_id, ID_ACCOUNT, EVENT_DESTROY | EVENT_ITEM_CHANGED)
        retval.totals_combo.connect("notify::popup-shown", retval.combo_popped_cb)
        retval.combo_popped = False
        for i in range(0, N_COLUMNS - 2, 2):
            text_renderer = Gtk.CellRendererText()
            text_renderer.set_fixed_size(50, -1)
            retval.totals_combo.pack_start(text_renderer, True)
            retval.set_user_data(text_renderer, "view_column", i)
            retval.totals_combo.set_cell_data_func(text_renderer, retval.cdf)
        retval.hbox.set_border_width(2)
        retval.hbox.pack_start(retval.totals_combo, True, True, 5)
        retval.totals_combo.show()
        retval.hbox.show()
        retval.hbox.connect("destroy", retval.destroy_cb)
        GLib.idle_add(retval.refresh, priority=GLib.PRIORITY_HIGH_IDLE)
        retval.cnxn_id = gs_pref_register_cb(PREFS_GROUP, None, retval.prefs_changed_cb)
        return retval.hbox
