from gi.repository import GObject, Gtk, Pango, Gdk, GLib

from gasstation.utilities.preferences import gs_pref_get_bool
from libgasstation import Session
from libgasstation.core.component import ComponentManager
from libgasstation.core.transquery import *
from .query import QueryView

PREFS_GROUP_RECONCILE = "dialogs.reconcile"
PREF_CHECK_CLEARED = "check-cleared"


class ReconcileViewType(GObject.GEnum):
    DEBIT = 0
    CREDIT = 1


class ReconcileViewColumn(GObject.GEnum):
    POINTER = 0
    DATE = 1
    NUM = 2
    DESC = 3
    AMOUNT = 4
    RECN = 5


class ReconcileView(QueryView):
    __gsignals__ = {
        'toggle_reconciled': (GObject.SignalFlags.RUN_FIRST, None, (object,)),
        'line_selected': (GObject.SignalFlags.RUN_FIRST, None, (object,)),
        'double_click_split': (GObject.SignalFlags.RUN_FIRST, None, (object,)),
    }

    def __init__(self, query, *args):
        super().__init__()
        num_action = Session.get_current_book().use_split_action_for_num_field()
        self.reconciled = []
        self.account = None
        self.sibling = None
        self.statement_date = 0
        self.view_type = None
        self.no_toggle = False

        # param = gnc_search_param_simple_new()
        # gnc_search_param_set_param_fcn(param, QOF_TYPE_BOOLEAN,
        #                                self.is_reconciled, view)
        # gnc_search_param_set_title((GNCSearchParam *) param, C_("Column header for 'Reconciled'", "R"))
        # gnc_search_param_set_justify((GNCSearchParam *) param, GTK_JUSTIFY_CENTER)
        # gnc_search_param_set_passive((GNCSearchParam *) param, False)
        # gnc_search_param_set_non_resizeable((GNCSearchParam *) param, True)
        # columns = g_list_prepend(columns, param)
        #
        # columns = gnc_search_param_prepend_with_justify(columns, _("Amount"),
        #                                                 GTK_JUSTIFY_RIGHT,
        #                                                 None, GNC_ID_SPLIT,
        #                                                 SPLIT_AMOUNT, None)
        # columns = gnc_search_param_prepend(columns, _("Description"), None,
        #                                    GNC_ID_SPLIT, SPLIT_TRANS,
        #                                    TRANS_DESCRIPTION, None)
        # columns = num_action ?
        # gnc_search_param_prepend_with_justify(columns, _("Num"),
        #                                       GTK_JUSTIFY_CENTER,
        #                                       None, GNC_ID_SPLIT,
        #                                       SPLIT_ACTION, None) :
        # gnc_search_param_prepend_with_justify(columns, _("Num"),
        #                                       GTK_JUSTIFY_CENTER,
        #                                       None, GNC_ID_SPLIT,
        #                                       SPLIT_TRANS, TRANS_NUM, None)
        # self.column_list = gnc_search_param_prepend(columns, _("Date"),
        #                                    None, GNC_ID_SPLIT,
        #                                    SPLIT_TRANS,
        #                                    TRANS_DATE_POSTED, None)

    def tooltip_cb(self, x, y, keyboard_mode, tooltip):
        check, x, y, model, path, _iter = self.get_tooltip_context(x, y, keyboard_mode)
        if check:
            if keyboard_mode == False:
                values = self.get_path_at_pos(x, y)
                if values is None:
                    return False
                col = values[1]
            else:
                col = self.get_cursor()[1]
            cols = self.get_columns()
            col_width = col.get_width()
            col_pos = cols[col]
            if col_pos != (ReconcileViewColumn.DESC - 1):
                return False
            desc_text = model.get_value(_iter, ReconcileViewColumn.DESC)
            if desc_text:
                layout = self.create_pango_layout(desc_text)
                text_width = layout.get_pixel_size()[0]
                if (text_width + 10) <= col_width:
                    return False
                if not keyboard_mode:
                    parent_window = self.get_parent_window()
                    display = parent_window.get_display()
                    seat = display.get_default_seat()
                    pointer = seat.get_pointer()
                    pointer, cur_x, cur_y, mask = parent_window.get_device_position(pointer)
                    root_x, root_y = parent_window.get_origin()
                    win_list = Gtk.Window.list_toplevels()
                    tip_win = None
                    for tip_win in win_list:
                        if tip_win.get_name() == "gtk-tooltip":
                            break
                    tooltip.set_text(desc_text)
                    if isinstance(tip_win, Gtk.Window):
                        requisition = tip_win.get_preferred_size()[0]
                        x = root_x + cur_x + 10
                        y = root_y + cur_y + 10
                        mon = display.get_monitor_at_point(x, y)
                        monitor = mon.get_geometry()
                        if x + requisition.width > monitor.x + monitor.width:
                            x -= x - (monitor.x + monitor.width) + requisition.width
                        elif x < monitor.x:
                            x = monitor.x
                        if y + requisition.height > monitor.y + monitor.height:
                            y -= y - (monitor.y + monitor.height) + requisition.height
                        tip_win.move(x, y)
                tooltip.set_text(desc_text)

            return True
        return False

    def get_column_width(self, column):
        col = self.get_column(column - 1)
        return col.get_width()

    def add_padding(self, column, xpadding):
        col = self.get_column(column - 1)
        renderers = col.get_cells()
        cr0 = renderers[0]
        xpad, ypad = cr0.get_padding()
        cr0.set_padding(xpadding, ypad)

    @staticmethod
    def sort_date_helper(date_a, date_b):
        ret = 0
        if date_a < date_b:
            ret = -1
        elif date_a > date_b:
            ret = 1
        return ret

    @staticmethod
    def sort_iter_compare_func(model, a, b):
        split_a = model.get_value(a, ReconcileViewColumn.POINTER)
        recn_a = model.get_value(a, ReconcileViewColumn.RECN)
        split_b = model.get_value(b, ReconcileViewColumn.POINTER)
        recn_b = model.get_value(b, ReconcileViewColumn.RECN)
        date_a = split_a.get_transaction().get_post_date()
        date_b = split_b.get_transaction().get_post_date()
        if recn_a > recn_b:
            ret = -1
        elif recn_b > recn_a:
            ret = 1
        else:
            ret = ReconcileView.sort_date_helper(date_a, date_b)
        return ret

    def __new__(cls, account, _type: ReconcileViewType, statement_date):
        self = super().__new__(cls)
        liststore = Gtk.ListStore(object, str, str, str, str, bool)
        self.set_model(liststore)
        self.account = account
        self.view_type = _type
        self.statement_date = statement_date
        query = Query.create_for(ID_SPLIT)
        query.set_book(Session.get_current_book())
        # include_children = xaccAccountGetReconcileChildrenStatus(account)
        accounts = []
        include_children = False
        if include_children:
            accounts = account.get_descendants()
        accounts.insert(0, account)
        query.add_account_match(accounts, GuidMatch.ANY, QueryOp.AND)
        sign = NumericMatch.CREDIT if _type == ReconcileViewType.CREDIT else NumericMatch.DEBIT
        query.add_numeric_match(Decimal(0), sign, QueryCompare.GTE, QueryOp.AND, SPLIT_AMOUNT, None)
        query.add_cleared_match(ClearedMatch.NO | ClearedMatch.CLEARED, QueryOp.AND)
        inv_sort = False
        if self.view_type == ReconcileViewType.CREDIT:
            inv_sort = True
        self.set_numerics(True, inv_sort)
        col = self.get_column(ReconcileViewColumn.DESC - 1)
        col.set_expand(True)
        renderers = col.get_cells()
        cr0 = renderers[0]
        cr0.set_property("ellipsize", Pango.EllipsizeMode.END)
        self.set_has_tooltip(True)
        selection = self.get_selection()
        selection.set_mode(Gtk.SelectionMode.MULTIPLE)
        self.connect("column_toggled", self.line_toggled)
        self.connect("double_click_entry", self.double_click_entry)
        self.connect("row_selected", self.row_selected)
        self.connect("key_press_event", self.key_press_cb)
        self.connect("query-tooltip", self.tooltip_cb)
        auto_check = gs_pref_get_bool(PREFS_GROUP_RECONCILE, PREF_CHECK_CLEARED)
        if auto_check:
            statement_date_day_end = datetime.datetime.combine(statement_date,
                                                               datetime.time(hour=23, minute=59, second=60))
            for split in query.run():
                recn = split.get_reconcile_state()
                trans_date = split.get_transaction().get_post_date()
                if recn == SplitState.CLEARED and trans_date < statement_date_day_end:
                    self.reconciled.append(split)
        sortable = self.get_model()
        sortable.set_sort_func(sortable, ReconcileViewColumn.RECN, self.sort_iter_compare_func,
                               ReconcileViewColumn.RECN)
        query.destroy()
        return self

    def toggle_split(self, split):
        if self.reconciled is None:
            return
        if split in self.reconciled:
            self.reconciled.remove(split)
        else:
            self.reconciled.append(split)

    def toggle(self, split):
        if self.reconciled is None:
            return
        self.toggle_split(split)
        self.emit("toggle-reconciled", split)

    def follow_select_tree_path(self):
        if self.rowref is not None:
            tree_path = Gtk.TreeRowReference.get_path(self.rowref)
            selection = self.get_selection()
            selection.unselect_all()
            selection.select_path(tree_path)
            self.scroll_to_cell(tree_path, None, False, 0.0, 0.0)
            self.rowref = None
        return False

    def line_toggled(self, item):
        model = self.get_model()
        _iter = model.iter_nth_child(None, self.toggle_row)
        tree_path = model.get_path(_iter)
        self.rowref = Gtk.TreeRowReference.new(model, tree_path)
        model.set_value(_iter, self.toggle_row, item)
        tree_path = self.rowref.get_path()
        _iter = model.get_iter(tree_path)
        if _iter is not None:
            split = model.get_value(_iter, ReconcileViewColumn.POINTER)
            self.toggle(split)
        if self.sort_column == ReconcileViewColumn.RECN:
            GLib.idle_add(self.follow_select_tree_path)
        else:
            self.rowref = None

    def double_click_entry(self, item):
        self.emit("double_click_split", item)

    def row_selected(self, item):
        self.emit("line_selected", item)

    def set_list(self, reconcile):
        last_tree_path = None
        model = self.get_model()
        selection = self.get_selection()
        model, selected_paths = selection.get_selected_rows()
        for rowref in reversed(list(map(lambda path: Gtk.TreeRowReference.new(model, path), selected_paths))):
            path = rowref.get_path()
            _iter = model.get_iter(path)
            if _iter is not None:
                split = model.get_value(_iter, ReconcileViewColumn.POINTER)
                toggled = model.get_value(_iter, ReconcileViewColumn.RECN)
                model.set_value(_iter, ReconcileViewColumn.RECN, reconcile)
                if last_tree_path is not None:
                    last_tree_path = rowref.get_path()
                if reconcile != toggled:
                    self.toggle(split)

        if last_tree_path is not None:
            if self.sort_column == ReconcileViewColumn.RECN - 1:
                self.scroll_to_cell(last_tree_path, None, False, 0.0, 0.0)
        self.queue_draw()

    def num_selected(self):
        selection = self.get_selection()
        return selection.count_selected_rows()

    def set_toggle(self):
        num_toggled = 0
        selection = self.get_selection()
        model, selected_paths = selection.get_selected_rows()
        num_selected = selection.count_selected_rows()
        for selected_path in selected_paths:
            _iter = model.get_iter(selected_path)
            if _iter is not None:
                toggled = model.get_value(_iter, ReconcileViewColumn.RECN)
                if toggled:
                    num_toggled += 1
        if num_toggled == num_selected:
            return False
        else:
            return True

    def key_press_cb(self, widget, event):
        if event.keyval == Gdk.KEY_space:
            widget.stop_emission_by_name("key_press_event")
            toggle = self.set_toggle()
            self.set_list(toggle)
            return True
        return False

    def get_num_splits(self):
        return self.get_num_entries()

    def get_current_split(self):
        return self.get_selected_entry()

    def is_reconciled(self, item):
        if self.reconciled is None:
            return
        return item in self.reconciled

    def refresh(self):
        # gnc_query_force_scroll_to_selection(qview)
        if self.reconciled:
            for v in self.reconciled.copy():
                if not self.item_in_view(v):
                    self.reconciled.remove(v)

    def reconciled_balance(self):
        total = Decimal(0)
        if self.reconciled is None:
            return total
        for split in self.reconciled:
            total += split.get_amount()
        return abs(total)

    def commit(self, date):
        if self.reconciled is None:
            return
        ComponentManager.suspend()
        for split in self.reconciled:
            split.set_reconcile_state(SplitState.RECONCILED)
            split.set_reconcile_date(date)
        ComponentManager.resume()

    def postpone(self):
        if self.reconciled is None:
            return
        model = self.get_model()
        _iter = model.get_iter_first()
        num_splits = self.get_num_entries()

        ComponentManager.suspend()
        for i in range(num_splits):
            split = model.get_value(_iter, ReconcileViewColumn.POINTER)
            split_date = split.get_transaction().get_post_date()
            if self.statement_date >= split_date or split in self.reconciled:
                recn = SplitState.CLEARED if split in self.reconciled else SplitState.UNRECONCILED
                split.set_reconcile_state(recn)
            _iter = model.iter_next(_iter)
        ComponentManager.resume()

    def changed(self):
        return len(self.reconciled) != 0
