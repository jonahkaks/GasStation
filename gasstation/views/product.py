from gasstation.models.product import *
from gasstation.views.tree import *

SAMPLE_SELL_VALUE = "$1,000,000.00"
PRODUCT_SELECTED_LABEL = "SelectedProducts"


class ProductView(TreeView):
    __gtype_name__ = "ProductView"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        sel = self.get_selection()
        sel.set_mode(Gtk.SelectionMode.MULTIPLE)
        self.filter_fn = None
        self.filter_data = None
        self.filter_destroy = None
        model = ProductModel.new()
        self.set_fixed_height_mode(True)
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        selection = self.get_selection()
        selection.unselect_all()
        f_model = model.filter_new()
        s_model = Gtk.TreeModelSort(model=f_model)
        self.set_model(s_model)
        self.add_numeric_column(0, title="#", pref_name="num", sizing_text="100000", visible_always=True,
                                data_func=self.index_cdf)
        self.add_text_column(1, title="CATEGORY", pref_name="category", sizing_text="Lubricants",
                             data_col=ProductModelColumn.CATEGORY, visible_always=True)
        self.name_column = self.add_text_column(2, title="NAME", pref_name="name", sizing_text="TRANSMISSION 20LTRS",
                                                data_col=ProductModelColumn.NAME,pixbuf_col=ProductModelColumn.AVATAR,
                                                visible_always=True)
        self.add_text_column(3, title="ID", pref_name="id", sizing_text="Prod 1000", data_col=ProductModelColumn.ID)

        self.add_text_column(4, title="SKU", pref_name="sku", sizing_text="######################",
                             data_col=ProductModelColumn.SKU, visible_default=True)
        self.add_text_column(5, title="TYPE", pref_name="type", sizing_text="Non Inventory",
                             data_col=ProductModelColumn.TYPE, visible_default=True)

        self.configure_columns()
        f_model.set_visible_func(self.filter_helper)
        s_model.set_sort_column_id(ProductModelColumn.NAME, Gtk.SortType.ASCENDING)
        self.show()

    def index_cdf(self, col, cell, s_model, _iter, *user_data):
        path = s_model.get_path(_iter)
        cell.set_properties(text=str(path.get_indices()[0] + 1), editable=False)

    def filter_helper(self, model, _iter, _):
        if _iter is None:
            return True
        product = model.get_product_at_iter(_iter)
        if self.filter_fn is not None:
            if self.filter_data is not None:
                return self.filter_fn(product, self.filter_data)
            else:
                return self.filter_fn(product)
        else:
            return True

    @staticmethod
    def get_product_from_iter(s_model, s_iter):
        f_iter = s_model.convert_iter_to_child_iter(s_iter)
        f_model = s_model.get_model()
        _iter = f_model.convert_iter_to_child_iter(f_iter)
        model = f_model.get_model()
        return model.get_product_at_iter(_iter)

    def get_product_from_path(self, path):
        s_model = self.get_model()
        s_iter = s_model.get_iter(path)
        return self.get_product_from_iter(s_model, s_iter)

    def get_selected_products_helper(self, s_model, s_path, s_iter, return_list):
        product = self.get_product_from_iter(s_model, s_iter)
        return_list.append(product)

    def get_selected_products(self):
        return_list = []
        selection = self.get_selection()
        selection.selected_foreach(self.get_selected_products_helper, return_list)
        return return_list

    def get_selected_product(self):
        selection = self.get_selection()
        if selection is None:
            return
        mode = selection.get_mode()
        if mode != Gtk.SelectionMode.SINGLE and mode != Gtk.SelectionMode.BROWSE:
            pdts = self.get_selected_products()
            return pdts[-1] if pdts is not None and len(pdts) else None
        s_model, s_iter = selection.get_selected()
        if s_model is None or s_iter is None:
            return None
        return self.get_product_from_iter(s_model, s_iter)

    def save_selected_row(self, key_file, group_name):
        products = self.get_selected_products()
        if not any(products) or not isinstance(products, list):
            return
        key_file.set_string(group_name, PRODUCT_SELECTED_LABEL, ",".join(map(lambda p: str(p.get_guid()), products)))

    def save(self, key_file, group_name):
        if key_file is None or group_name is None:
            return
        self.save_selected_row(key_file, group_name)

    def restore_selected_row(self, product_guid_str):
        if product_guid_str is None:
            return
        book = Session.get_current_book()
        coll = book.get_collection(ID_PRODUCT)
        product = coll.get(product_guid_str)
        if product is not None:
            self.set_selected_product(product)

    def restore(self, key_file, group_name):
        try:
            value = key_file.get_string(group_name, PRODUCT_SELECTED_LABEL)
            selection = self.get_selection()
            selection.unselect_all()
            for v in value.split(","):
                if len(v):
                    self.restore_selected_row(uuid.UUID(v))
        except GLib.Error:
            pass

    def set_selected_product(self, product):
        selection = self.get_selection()
        if product is None:
            return
        s_model = self.get_model()
        f_model = s_model.get_model()
        model = f_model.get_model()

        path = model.get_path_from_product(product)
        if path is None:
            return
        f_path = f_model.convert_child_path_to_path(path)
        if f_path is None:
            return
        s_path = s_model.convert_child_path_to_path(f_path)
        if s_path is None:
            return
        selection.select_path(s_path)
        while Gtk.events_pending():
            Gtk.main_iteration()
        self.scroll_to_cell(s_path, None, False, 0.0, 0.0)

    def set_filter(self, func, data=None, destroy=None):
        if self.filter_destroy is not None:
            self.filter_destroy(self.filter_data)
        self.filter_destroy = destroy
        self.filter_data = data
        self.filter_fn = func
        self.refilter()

    def refilter(self):
        s_model = self.get_model()
        if s_model is None:
            return False
        f_model = s_model.get_model()
        f_model.clear_cache()
        f_model.refilter()


GObject.type_register(ProductView)
