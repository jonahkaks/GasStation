from gasstation.utilities.warnings import *

from gasstation.models.fuel import *
from gasstation.utilities.cursors import gs_set_busy_cursor, gs_unset_busy_cursor
from gasstation.utilities.custom_dialogs import show_error
from gasstation.utilities.ui import add_toolbar_label
from gasstation.views.tree import *
from libgasstation.core.helpers import *


class FuelViewColumn(GObject.GEnum):
    END_OF_LIST = -1
    CONTROL = 0
    NOZZLE = 1
    OPENING = 2
    CLOSING = 3
    LITRES = 4
    PRICE = 5
    DEBTORS = 6
    AMOUNT = 7


class FuelConfirm(GObject.GEnum):
    RESET = 0
    ACCEPT = 1
    DISCARD = 2
    CANCEL = 3


class FuelView(TreeView):
    __gtype_name__ = "FuelView"
    __gsignals__ = {
        "fuel-changed": (GObject.SignalFlags.RUN_LAST, None, (object,)),
        "refreshed": (GObject.SignalFlags.RUN_FIRST, None, (object, object))}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.temp_cr = None
        self.fo_handler_id = 0
        self.container = None
        self.litres_total = None
        self.amount_total = None
        self.location = None
        model = FuelModel.new()
        self.add_combo_column(FuelViewColumn.NOZZLE, title="Nozzle", pref_name="nozzle",
                              sizing_text="Petroleum Motor spirit", data_col=FuelModelColumn.NOZZLE,
                              model=model.get_nozzle_model(), data_func=self.cdf0,
                              visible_default=True, edited=self.edited_cb, editing_started=self.editing_started)
        self.add_numeric_column(FuelViewColumn.OPENING, title="Opening Meter", pref_name="opening",
                                sizing_text="1000000000000000000000", data_col=FuelModelColumn.OPENING,
                                data_func=self.cdf0,
                                visible_always=True, edited=self.edited_cb, editing_started=self.editing_started)
        self.add_numeric_column(FuelViewColumn.CLOSING, data_col=FuelModelColumn.CLOSING, title="Closing Meter",
                                data_func=self.cdf0,
                                pref_name="closing", sizing_text="1000000000000000000000", visible_always=True,
                                edited=self.edited_cb, editing_started=self.editing_started)
        self.add_numeric_column(FuelViewColumn.LITRES, data_col=FuelModelColumn.LITRES, title="Litres",
                                data_func=self.cdf0,
                                pref_name="litres", sizing_text="70000000000000", visible_always=True)
        self.add_numeric_column(FuelViewColumn.PRICE, data_col=FuelModelColumn.PRICE, title="Price",
                                data_func=self.cdf0,
                                pref_name="price", sizing_text="3500000000",
                                edited=self.edited_cb, editing_started=self.editing_started, visible_always=True)
        self.add_numeric_column(FuelViewColumn.DEBTORS, data_col=FuelModelColumn.DEBTORS, title="Debtor's Amount",
                                data_func=self.cdf0,
                                pref_name="debtors", sizing_text="3500000000",
                                edited=self.edited_cb, editing_started=self.editing_started, visible_always=True)
        self.add_numeric_column(FuelViewColumn.AMOUNT, data_col=FuelModelColumn.AMOUNT, title="Amount",
                                data_func=self.cdf0,
                                pref_name="amount", sizing_text="10000000000000000", visible_always=True)

        self.dirty_fuel:Fuel = None
        self.current_fuel = None
        self.current_date = datetime.datetime.now()
        self.change_allowed = False
        gs_widget_set_style_context(self, "FuelView")
        self.fuel_confirm = FuelConfirm.RESET
        self.auto_complete = True
        self.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        selection = self.get_selection()
        selection.unselect_all()
        s_model = Gtk.TreeModelSort.new_with_model(model)
        self.set_model(s_model)
        self.configure_columns()
        self.expand_columns("nozzle", "opening", "closing", "litres", "amount")
        self.set_state_section("Fuel")
        s_model.set_sort_column_id(FuelModelColumn.DATE, Gtk.SortType.ASCENDING)
        hub = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        hub.set_border_width(6)
        sw = Gtk.ScrolledWindow()
        sw.set_shadow_type(Gtk.ShadowType.ETCHED_IN)
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        hub.pack_start(sw, True, True, 0)
        sw.add(self)
        tbar = self.create_toolbar()
        hub.pack_start(tbar, False, False, 0)
        self.container = hub
        self.reference_ids[self].append(self.connect("key-press-event", self.key_press_cb))
        selection = self.get_selection()
        selection.set_mode(Gtk.SelectionMode.BROWSE)
        self.reference_ids[selection].append(selection.connect("changed", self.motion_cb))
        self.update_toolbar()

    def motion_cb(self, sel):
        model = self.get_model_from_view()
        t, m_iter = self.get_model_iter_from_selection(sel)
        if t:
            fuel = model.get_fuel_at_iter(m_iter)
            old_fuel = self.current_fuel
            self.current_fuel = fuel
            model.current_fuel = fuel
            if fuel != old_fuel and old_fuel == self.dirty_fuel:
                if self.fuel_changed():
                    return
            if self.fuel_confirm == FuelConfirm.CANCEL:
                return

    def create_toolbar(self):
        tbar = Gtk.Toolbar()
        gs_widget_set_style_context(tbar, "summary-tbar")
        tbar.set_icon_size(Gtk.IconSize.MENU)
        tbar.set_style(Gtk.ToolbarStyle.ICONS)
        tbar.get_style_context().add_class(Gtk.STYLE_CLASS_INLINE_TOOLBAR)
        label = Gtk.Label("Fuel Sales")
        label.set_valign(Gtk.Align.CENTER)
        label.set_halign(Gtk.Align.START)

        toolitem = Gtk.ToolItem()
        toolitem.add(label)
        tbar.insert(toolitem, -1)

        toolitem = Gtk.SeparatorToolItem()
        toolitem.set_expand(True)
        toolitem.set_draw(False)
        tbar.insert(toolitem, -1)
        self.litres_total = add_toolbar_label(tbar, "Total Litres:")
        self.amount_total = add_toolbar_label(tbar, "Total Amount:")
        return tbar

    @staticmethod
    def make_label_group(strs):
        label = Gtk.Label(strs)
        label.set_valign(Gtk.Align.CENTER)
        label.set_halign(Gtk.Align.START)
        return label

    def update_toolbar(self):
        model = self.get_model_from_view()
        l, a = model.get_fuel_totals()
        self.litres_total.set_text("{:,.2f}".format(l))
        self.amount_total.set_text("{:,.2f}".format(a))

    def cdf0(self, col, cell, s_model, s_iter, path_string):
        viewcol = self.get_user_data(cell, "view_column")
        model = self.get_model_from_view()
        spath = s_model.get_path(s_iter)
        editable = True
        indices = spath.get_indices()
        row_color = model.get_row_color(indices[0])
        if row_color is not None:
            cell.set_property("cell-background", row_color)
            cell.set_property("foreground", "black")
        else:
            cell.set_property("cell-background-set", 0)
            cell.set_property("foreground-set", 0)
        cell.set_property("xalign", 1.0)
        if viewcol == FuelViewColumn.NOZZLE:
            cell.set_property("xalign", 0.0)
        if viewcol == FuelViewColumn.LITRES:
            editable = False
        elif viewcol == FuelViewColumn.AMOUNT:
            editable = False
        cell.set_property("editable", editable)
        return False

    def edited_cb(self, cell, path_string, new_text):
        if new_text is None:
            return
        editable = self.get_user_data(cell, "cell-editable")
        if self.fo_handler_id != 0:
            editable.disconnect(self.fo_handler_id)
            self.fo_handler_id = 0
        self.grab_focus()
        old_text = self.get_user_data(cell, "current-string")
        if old_text == new_text:
            if not self.stop_cell_move:
                return
        m_iter = self.get_model_iter_from_view_string(path_string)
        model = self.get_model_from_view()
        viewcol = self.get_user_data(cell, "view_column")
        fuel = model.get_fuel_at_iter(m_iter)
        bfuel = model.get_blank_fuel()
        if fuel is None:
            return
        self.begin_edit(fuel)
        if viewcol == FuelViewColumn.NOZZLE:
            nozzle = model.get_nozzle_from_text(new_text)
            fuel.set_nozzle(nozzle)
            fuel.set_location(self.location)
            if fuel == bfuel:
                fuel.set_opening(nozzle.get_opening_meter())
                product = nozzle.get_product()
                for pq in product.locations:
                    if pq.get_location() == self.location:
                        fuel.set_price(pq.get_selling_price())

        elif viewcol == FuelViewColumn.OPENING:
            fuel.set_opening(parse_exp_decimal(new_text))

        elif viewcol == FuelViewColumn.CLOSING:
            closing = parse_exp_decimal(new_text)
            fuel.set_closing(closing)
            nozzle = fuel.get_nozzle()
            if nozzle is not None:
                nozzle.begin_edit()
                nozzle.set_opening_meter(closing)

        elif viewcol == FuelViewColumn.PRICE:
            last_price = parse_exp_decimal(new_text)
            fuel.set_price(last_price)
            product = fuel.nozzle.get_product()
            if product is not None:
                for pq in product.quantities:
                    if pq.get_location() == self.location:
                        pq.set_selling_price(last_price)

        elif viewcol == FuelViewColumn.DEBTORS:
            last_price = parse_exp_decimal(new_text)
            fuel.set_debtor_amount(last_price)
        self.update_toolbar()
        self.emit("fuel-changed", fuel)

    def begin_edit(self, fuel):
        if fuel != self.dirty_fuel:
            t = fuel.get_date()
            if not fuel.is_open():
                fuel.begin_edit()
            if t is None or t != self.current_date:
                t = datetime.date.today()
                fuel.set_date(t)
            self.dirty_fuel = fuel

    def enter(self):
        self.finish_edit()
        if self.fuel_changed():
            return False
        if self.fuel_confirm == FuelConfirm.DISCARD:
            return False
        return True

    def refresh(self, calendar, loc):
        if loc is None:
            return
        y, m, d = calendar.get_date()
        self.location = loc
        model = self.get_model_from_view()
        b = model.get_blank_fuel()
        if b is not None:
            b.dispose()
        self.current_date = datetime.date(year=y, month=m + 1, day=d)
        self.emit("refreshed", self.current_date, loc)
        model.refresh(self.current_date, loc)
        for f in model.flist:
            self.emit("fuel-changed", f)
        self.update_toolbar()

    def editing_started(self, cr, editable, _):
        viewcol = self.get_user_data(cr, "view_column")
        self.set_user_data(cr, "cell-editable", editable)
        if viewcol == FuelViewColumn.NOZZLE:
            model = self.get_model_from_view()
            model.update_models(self.location)
            cat_list = editable.get_model()
            _iter = editable.get_active_iter()
            if _iter is not None:
                text = cat_list.get_value(_iter, 0)
            else:
                text = ""
            self.set_user_data(cr, "current-string", text)
            self.reference_ids[editable].append(editable.connect("remove-widget", self.remove_edit_combo))
        else:
            entry = editable
            self.reference_ids[editable].append(entry.connect("key-press-event", self.ed_key_press_cb))
            self.set_user_data(cr, "current-string", entry.get_text())
            self.reference_ids[editable].append(entry.connect("remove-widget", self.remove_edit_entry))
        self.temp_cr = cr
        self.editing = True
        self.set_user_data(cr, "editing-canceled", False)

    def ed_key_press_cb(self, widget, event):
        spath, col = self.get_cursor()
        if event.type != Gdk.EventType.KEY_PRESS:
            return False
        if event.keyval == Gdk.KEY_Up or event.keyval == Gdk.KEY_Down:
            if spath is None:
                return True
            self.finish_edit()
            if self.stop_cell_move:
                return True
            if event.keyval == Gdk.KEY_Up:
                spath.prev()
            else:
                spath.next()
            self.set_cursor(spath, col, True)
        elif event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return:
            if spath is None:
                return True
            if self.enter():
                GLib.idle_add(self.jump_to_blank)
            return True

    def jump_to_blank(self):
        model = self.get_model_from_view()
        bfuel = model.get_blank_fuel()
        model.current_fuel = bfuel
        spath = model.get_path_from_fuel(bfuel)
        self.scroll_to_cell(spath, None, False, 1.0, 0.0)
        self.set_cursor(spath, None, False)

    def fuel_changed(self):
        spath, col = self.get_cursor()
        if spath is None:
            return False
        self.fuel_confirm = FuelConfirm.RESET
        if self.dirty_fuel is not None and self.dirty_fuel.get_amount().is_zero():
            show_error(None, "Fuel Sales Total can't be zero")
            return False

        if self.get_user_data(self, "data-edited") and self.fuel_changed_confirm(None):
            if self.fuel_confirm == FuelConfirm.CANCEL:
                return True
            if self.fuel_confirm == FuelConfirm.DISCARD:
                self.dirty_fuel = None
        return False

    def fuel_changed_confirm(self, new_fuel):
        window = self.get_toplevel()
        title = "Save the changed fuel?"
        message = "The current fuel has changed. Would you like to " \
                  "record the changes, or discard the changes?"
        if self.dirty_fuel is None or self.dirty_fuel == new_fuel:
            return False
        dialog = Gtk.MessageDialog(transient_for=window,
                                   modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE, title=title)
        dialog.format_secondary_text("%s" % message)
        dialog.add_buttons("_Discard Changes", Gtk.ResponseType.REJECT,
                           "_Cancel", Gtk.ResponseType.CANCEL,
                           "_Record Changes", Gtk.ResponseType.ACCEPT)
        response = gs_dialog_run(dialog, PREF_WARN_FUEL_MOD)
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            amount = self.dirty_fuel.get_amount()
            if amount < 0:
                show_error(self.get_toplevel(), "Fuel Amount cannot be negative")
                return True
            self.set_user_data(self, "data-edited", False)
            book = self.dirty_fuel.get_book()
            be = book.get_backend()
            gs_set_busy_cursor(None, True)
            with be.add_lock():
                self.dirty_fuel.nozzle.commit_edit()
                self.dirty_fuel.commit_edit()
                #Lookup an invoice with this fuel product at this location
                # customer = Customer.lookup(self.book, "")
            gs_unset_busy_cursor(None)
            self.dirty_fuel = None
            self.change_allowed = False
            self.auto_complete = False
            self.fuel_confirm = FuelConfirm.ACCEPT
            return False

        elif response == Gtk.ResponseType.REJECT:
            if self.dirty_fuel is not None:
                self.set_user_data(self, "data-edited", False)
                self.change_allowed = False
                self.auto_complete = False
                self.fuel_confirm = FuelConfirm.DISCARD
            return True

        elif response == Gtk.ResponseType.CANCEL:
            self.fuel_confirm = FuelConfirm.CANCEL
            return True
        else:
            return False

    def key_press_cb(self, widget, event):
        editing = False
        step_off = False
        fuel_changed = False
        spath, col = self.get_cursor()
        if event.type != Gdk.EventType.KEY_PRESS:
            return False
        if event.keyval in [Gdk.KEY_plus, Gdk.KEY_minus, Gdk.KEY_KP_Add, Gdk.KEY_KP_Subtract]:
            return True

        elif event.keyval in [Gdk.KEY_Up, Gdk.KEY_Down]:

            if event.keyval == Gdk.KEY_Up:
                pass
            return False

        elif event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return:
            if spath is None:
                return True
            if self.enter():
                GLib.idle_add(self.jump_to_blank)
            return True

        elif event.keyval in [Gdk.KEY_Tab, Gdk.KEY_ISO_Left_Tab, Gdk.KEY_KP_Tab]:
            if spath is None:
                return True
            if event.state & Gdk.ModifierType.CONTROL_MASK:
                self.auto_complete = True
            self.finish_edit()
            if self.stop_cell_move:
                return True
            while not editing and not step_off:
                start_spath = spath.copy()
                start_indices = start_spath.get_indices()
                ncol = self.keynav(col, spath, event)
                next_indices = spath.get_indices()
                if start_indices[0] != next_indices[0]:
                    if self.dirty_fuel is not None:
                        fuel_changed = True
                    self.change_allowed = False

                if spath is None or not self.path_is_valid(spath) or fuel_changed:
                    if self.fuel_changed():
                        return True
                    step_off = True
                if self.fuel_confirm != FuelConfirm.DISCARD:
                    self.set_cursor(spath, ncol, True)
                editing = self.get_editing(ncol)
            return True
        else:
            return False

    def dispose_cb(self, widget):
        model = self.get_model_from_view()
        super(FuelView, self).dispose_cb(widget)
        model.destroy()


GObject.type_register(FuelView)
