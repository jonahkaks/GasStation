# -*- coding: utf-8 -*-

from gasstation.models.purchase import *
from gasstation.utilities.warnings import *

from gasstation.utilities.custom_dialogs import show_error
from gasstation.utilities.ui import add_summary_label
from gasstation.views.tree import *

STATE_SECTION = "dialogs/edit_purchases"
class PurchaseViewColumn(GObject.GEnum):
    END_OF_LIST = -1
    CONTROL = 0
    NAME = 1
    QUANTITY = 2
    PRICE = 3
    AMOUNT = 4


class PurchaseConfirm(GObject.GEnum):
    RESET = 0
    ACCEPT = 1
    DISCARD = 2
    CANCEL = 3


class PurchaseView(TreeView):
    __gtype_name__ = "PurchaseView"

    def __init__(self):
        TreeView.__init__(self)
        self.temp_cr = None
        self.fo_handler_id = 0
        self.column_data = {
            PurchaseViewColumn.NAME: ColDef(PurchaseViewColumn.NAME, PurchaseModelColumn.NAME, "Name",
                                         "name", "Rubia G 6200000000000000", TREE_VIEW_COLUMN_VISIBLE_ALWAYS, 1,
                                         self.edited_cb, self.editing_started, None),
            PurchaseViewColumn.QUANTITY: ColDef(PurchaseViewColumn.QUANTITY, PurchaseModelColumn.QUANTITY, "Quantity",
                                             "quantity", "100000000", TREE_VIEW_COLUMN_VISIBLE_ALWAYS,
                                             1,
                                             self.edited_cb, self.editing_started, None),

            PurchaseViewColumn.PRICE: ColDef(PurchaseViewColumn.PRICE, PurchaseModelColumn.PRICE, "Price",
                                          "price", "3500000000", TREE_VIEW_COLUMN_VISIBLE_ALWAYS, 1,
                                          self.edited_cb, self.editing_started, None),
            PurchaseViewColumn.AMOUNT: ColDef(PurchaseViewColumn.AMOUNT, PurchaseModelColumn.AMOUNT, "Amount",
                                           "amount", "100000000000000000", TREE_VIEW_COLUMN_VISIBLE_ALWAYS, 1,
                                           None, None, None)
        }
        self.dirty_purchase = None
        self.current_purchase = None
        self.current_date = time64_get_day_neutral(datetime.datetime.now())
        self.change_allowed = False
        self.purchase_confirm = PurchaseConfirm.RESET
        gs_widget_set_style_context(self, "PurchaseView")
        self.auto_complete = True
        self.container = None
        self.quantity_total = None
        self.amount_total = None

    @classmethod
    def new(cls):
        view = GObject.new(cls, state_section=STATE_SECTION, show_column_menu=False)
        model = PurchaseModel.new()
        view.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        s_model = Gtk.TreeModelSort(model=model)
        view.set_cols(model, [PurchaseViewColumn.NAME,
                              PurchaseViewColumn.QUANTITY,
                              PurchaseViewColumn.PRICE,
                              PurchaseViewColumn.AMOUNT])
        view.set_model(s_model)

        view.configure_columns()
        view.expand_columns("name", "quantity", "price", "amount")
        view.set_state_section("Purchase")
        s_model.set_sort_column_id(PurchaseModelColumn.DATE, Gtk.SortType.ASCENDING)
        hub = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        hub.set_border_width(6)
        sw = Gtk.ScrolledWindow()
        sw.set_shadow_type(Gtk.ShadowType.ETCHED_IN)
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        hub.pack_start(sw, True, True, 0)
        sw.add(view)

        view.container = hub
        tbar = view.create_toolbar()
        hub.pack_start(tbar, False, False, 0)
        # view.jump_to_blank()
        view.connect("key-press-event", view.key_press_cb)
        selection = view.get_selection()
        selection.set_mode(Gtk.SelectionMode.BROWSE)
        selection.connect("changed", view.motion_cb)
        view.set_user_data(view, "data-edited", 0)
        view.update_toolbar()
        return view

    def create_toolbar(self):
        tbar = Gtk.Toolbar()
        gs_widget_set_style_context(tbar, "summary-tbar")
        tbar.set_icon_size(Gtk.IconSize.MENU)
        tbar.set_style(Gtk.ToolbarStyle.ICONS)
        tbar.get_style_context().add_class(Gtk.STYLE_CLASS_INLINE_TOOLBAR)
        label = Gtk.Label("Purchases")
        label.set_valign(Gtk.Align.CENTER)
        label.set_halign(Gtk.Align.START)

        toolitem = Gtk.ToolItem()
        toolitem.add(label)
        tbar.insert(toolitem, -1)

        toolitem = Gtk.SeparatorToolItem()
        toolitem.set_expand(True)
        toolitem.set_draw(False)
        tbar.insert(toolitem, -1)

        box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
        self.quantity_total = add_summary_label(box, "Total Quantity:")
        self.amount_total = add_summary_label(box, "Total Amount:")

        toolitem = Gtk.ToolItem()
        toolitem.add(box)
        tbar.insert(toolitem, -1)
        return tbar

    def update_toolbar(self):
        model = self.get_model_from_view()
        l, a = model.get_purchase_totals()
        self.quantity_total.set_text(sprintamount(l, PrintAmountInfo.default()))
        self.amount_total.set_text(sprintamount(a, PrintAmountInfo.default(use_symbol=1)))

    def cdf0(self, col, cell, s_model, s_iter, userdata):
        viewcol = DataStore.get_user_data(cell, "view_column")
        model = self.get_model_from_view()
        spath = s_model.get_path(s_iter)
        editable = True
        indices = spath.get_indices()
        row_color = model.get_row_color(indices[0])
        cell.set_property("cell-background", row_color)
        cell.set_property("xalign", 1.0)
        if viewcol == PurchaseViewColumn.NAME:
            cell.set_property("xalign", 0.0)

        if viewcol == PurchaseViewColumn.AMOUNT:
            editable = False
        cell.set_property("editable", editable)
        return False

    def set_cols(self, model, col_list):
        for c in col_list:
            df = self.column_data[c]
            if c == PurchaseViewColumn.NAME:
                col = self.add_combo_column(title=df.title, pref_name=df.pref_name, sizing_text=df.sizer,
                                            data_col=df.modelcol,
                                            visible_default=df.visibility_model_col,
                                            combo_tree_model=model.get_product_model(), combo_model_text_column=0)
            else:
                col = self.add_text_column(title=df.title, pref_name=df.pref_name,
                                           sizing_text=df.sizer, data_col=df.modelcol,
                                           visible_default=df.visibility_model_col)
            renderers = col.get_cells()
            cr0 = renderers[0]
            cr0.set_property("xalign", 1.0)

            if df.editing_started_cb:
                cr0.connect("editing-started", df.editing_started_cb)

            cr0.connect("editing-canceled", self.editing_canceled_cb)
            col.set_property("resizable", True)

            if df.edited_cb:
                cr0.set_property("editable", True)
                cr0.connect("edited", df.edited_cb)
            DataStore.set_user_data(cr0, "view_column", df.viewcol)
            self.set_user_data(col, DEFAULT_VISIBLE, 1)
            col.set_cell_data_func(cr0, self.cdf0)
        self.connect("key-press-event", self.key_press_cb)
        selection = self.get_selection()
        selection.set_mode(Gtk.SelectionMode.BROWSE)
        selection.connect("changed", self.motion_cb)

    def motion_cb(self, sel):
        model = self.get_model_from_view()
        t, m_iter = self.get_model_iter_from_selection(sel)
        if t:
            purchase = model.get_purchase_at_iter(m_iter)
            old_purchase = self.current_purchase
            self.current_purchase = purchase
            model.current_purchase = purchase
            if purchase != old_purchase and old_purchase == self.dirty_purchase:
                if self.purchase_changed():
                    return
            if self.purchase_confirm == PurchaseConfirm.CANCEL:
                return

    def edited_cb(self, cell, path_string, new_text):
        if new_text is None:
            return
        editable = DataStore.get_user_data(cell, "cell-editable")
        if self.fo_handler_id != 0:
            editable.disconnect(self.fo_handler_id)
            self.fo_handler_id = 0
        self.grab_focus()
        old_text = DataStore.get_user_data(cell, "current-string")
        if old_text == new_text:
            if not self.stop_cell_move:
                return
        self.edited_normal_cb(cell, path_string, new_text)

    def edited_normal_cb(self, cell, path_string, new_text):
        m_iter = self.get_model_iter_from_view_string(path_string)
        model = self.get_model_from_view()
        viewcol = DataStore.get_user_data(cell, "view_column")
        purchase = model.get_purchase_at_iter(m_iter)

        if purchase is None:
            return
        self.begin_edit(purchase)
        if viewcol == PurchaseViewColumn.NAME:
            inv = model.get_product_from_text(new_text)
            purchase.set_product(inv)
            if purchase is not None and inv is not None:
                PurchaseModel.used[m_iter.user_data] = inv.get_name()

        elif viewcol == PurchaseViewColumn.QUANTITY:
            purchase.set_quantity(parse_exp_decimal(new_text))

        elif viewcol == PurchaseViewColumn.PRICE:
            purchase.set_price(parse_exp_decimal(new_text))
        self.update_toolbar()

    def begin_edit(self, purchase):
        if purchase != self.dirty_purchase:
            t = purchase.get_post_date()
            if not purchase.is_open():
                purchase.begin_edit()
            self.dirty_purchase = purchase
            if t is None or t == 0:
                purchase.set_post_date(self.current_date)

    def enter(self):
        self.finish_edit()
        if self.purchase_changed():
            return False
        if self.purchase_confirm == PurchaseConfirm.DISCARD:
            return False
        return True

    def destroy(self):
        model = self.get_model_from_view()
        b = model.get_blank_purchase()
        b.dispose()
        del model
        super().destroy()

    def refresh(self, calendar):
        y, m, d = calendar.get_date()
        model = self.get_model_from_view()
        b = model.get_blank_purchase()
        b.dispose()
        self.current_date = datetime.datetime.now().replace(year=y, month=m + 1, day=d).timetuple()
        model.refresh(time.mktime(self.current_date))
        self.update_toolbar()

    def editing_started(self, cr, editable, path_string):
        viewcol = DataStore.get_user_data(cr, "view_column")
        DataStore.set_user_data(cr, "cell-editable", editable)
        if viewcol == PurchaseViewColumn.NAME:
            model = self.get_model_from_view()
            model.update_models()
            cat_list = editable.get_model()
            _iter = editable.get_active_iter()
            if _iter is not None:
                text = cat_list.get_value(_iter, 0)
            else:
                text = ""
            DataStore.set_user_data(cr, "current-string", text)
            editable.connect("remove-widget", self.remove_edit_combo)
        else:
            entry = editable
            entry.connect("key-press-event", self.ed_key_press_cb)
            DataStore.set_user_data(cr, "current-string", entry.get_text())
            entry.connect("focus-out-event", self.focus_out_cb)
            entry.connect("remove-widget", self.remove_edit_entry)
        self.temp_cr = cr
        self.editing = True
        DataStore.set_user_data(cr, "editing-canceled", False)

    def ed_key_press_cb(self, widget, event):
        spath, col = self.get_cursor()
        if event.type != Gdk.EventType.KEY_PRESS:
            return False
        if event.keyval == Gdk.KEY_Up or event.keyval == Gdk.KEY_Down:
            if spath is None:
                return True
            self.finish_edit()
            if self.stop_cell_move:
                return True
            if event.keyval == Gdk.KEY_Up:
                spath.prev()
            else:
                spath.next()
            self.set_cursor(spath, col, True)
        elif event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return:
            if spath is None:
                return True
            if self.enter():
                GLib.idle_add(self.jump_to_blank)
            return True

    def jump_to_blank(self):
        model = self.get_model_from_view()
        bpurchase = model.get_blank_purchase()
        model.current_purchase = bpurchase
        spath = model.get_path_from_purchase(bpurchase)
        self.scroll_to_cell(spath, None, False, 1.0, 0.0)
        self.set_cursor(spath, None, False)

    def purchase_changed(self):
        spath, col = self.get_cursor()
        if spath is None:
            return False
        self.purchase_confirm = PurchaseConfirm.RESET
        if self.get_user_data(self, "data-edited") and self.purchase_changed_confirm(None):
            if self.purchase_confirm == PurchaseConfirm.CANCEL:
                return True
            if self.purchase_confirm == PurchaseConfirm.DISCARD:
                self.dirty_purchase = None
        return False

    def purchase_changed_confirm(self, new_purchase):
        window = self.get_toplevel()
        title = "Save the changed purchase?"
        message = "The current purchase has changed. Would you like to " \
                  "record the changes, or discard the changes?"
        if self.dirty_purchase is None or self.dirty_purchase == new_purchase:
            return False
        dialog = Gtk.MessageDialog(transient_for=window,
                                   modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE, title=title)
        dialog.format_secondary_text("%s" % message)
        dialog.add_buttons("_Discard Changes", Gtk.ResponseType.REJECT,
                           "_Cancel", Gtk.ResponseType.CANCEL,
                           "_Record Changes", Gtk.ResponseType.ACCEPT)
        response = gs_dialog_run(dialog, PREF_WARN_PURCHASE_MOD)
        dialog.destroy()
        if response == Gtk.ResponseType.ACCEPT:
            book = Session.get_current_book()
            c = book.get_prefered_cash_account()
            s = book.get_prefered_sales_account()
            if c is None or s is None:
                from gasstation.views.dialogs.gs_prefered_account import DialogPreferredAccount
                DialogPreferredAccount.new(self.get_toplevel())
            amount = self.dirty_purchase.get_amount()
            if amount < 0 or amount.is_zero():
                show_error(self.get_toplevel(), "Purchase Amount cannot be zero or negative")
                return True
            self.set_user_data(self, "data-edited", False)
            self.dirty_purchase.commit_edit()
            self.dirty_purchase = None
            self.change_allowed = False
            self.auto_complete = False
            self.purchase_confirm = PurchaseConfirm.ACCEPT
            return False

        elif response == Gtk.ResponseType.REJECT:
            if self.dirty_purchase is not None:
                self.set_user_data(self, "data-edited", False)
                self.change_allowed = False
                self.auto_complete = False
                self.purchase_confirm = PurchaseConfirm.DISCARD
            return True

        elif response == Gtk.ResponseType.CANCEL:
            self.purchase_confirm = PurchaseConfirm.CANCEL
            return True
        else:
            return False

    def key_press_cb(self, widget, event):
        editing = False
        step_off = False
        purchase_changed = False
        spath, col = self.get_cursor()
        if event.type != Gdk.EventType.KEY_PRESS:
            return False
        if event.keyval in [Gdk.KEY_plus, Gdk.KEY_minus, Gdk.KEY_KP_Add, Gdk.KEY_KP_Subtract]:
            return True

        elif event.keyval in [Gdk.KEY_Up, Gdk.KEY_Down]:

            if event.keyval == Gdk.KEY_Up:
                pass
            return False

        elif event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return:
            if spath is None:
                return True
            if self.enter():
                GLib.idle_add(self.jump_to_blank)
            return True

        elif event.keyval in [Gdk.KEY_Tab, Gdk.KEY_ISO_Left_Tab, Gdk.KEY_KP_Tab]:
            if spath is None:
                return True
            if event.state & Gdk.ModifierType.CONTROL_MASK:
                self.auto_complete = True
            self.finish_edit()
            if self.stop_cell_move:
                return True
            while not editing and not step_off:
                start_spath = spath.copy()
                start_indices = start_spath.get_indices()
                ncol = self.keynav(col, spath, event)
                next_indices = spath.get_indices()
                if start_indices[0] != next_indices[0]:
                    if self.dirty_purchase is not None:
                        purchase_changed = True
                    self.change_allowed = False

                if spath is None or not self.path_is_valid(spath) or purchase_changed:
                    if self.purchase_changed():
                        return True
                    step_off = True
                if self.purchase_confirm != PurchaseConfirm.DISCARD:
                    self.set_cursor(spath, ncol, True)
                editing = self.get_editing(ncol)
            return True
        else:
            return False


GObject.type_register(PurchaseView)
