from uuid import UUID

from gasstation.views.dialogs.customer import CustomerDialog
from gasstation.views.dialogs.customer.invoice import InvoiceDialog
from gasstation.views.dialogs.supplier import SupplierDialog
from gasstation.views.reports.webkit import *
from libgasstation import Price
from libgasstation.core._declbase import *
from libgasstation.core.session import Session


class HtmlUrlHandler:
    @classmethod
    def validate_type(cls, url_type, location, entity_type, result):
        result.load_to_stream = False
        book = Session.get_current_book()
        guid = UUID(location.replace(url_type, ""))
        col = book.get_collection(entity_type)
        entity = col.lookup_entity(guid)
        if entity is None:
            result.error_message = "Entity Not Found: %s" % location
            raise ValueError(result.error_message)
        return guid, entity

    @classmethod
    def register_cb(cls, location, label, new_window, result):
        from gasstation.plugins.pages import RegisterPluginPage
        account = None
        split = None
        if location is None or result is None:
            return False
        if location.startswith("account="):
            account = Session.get_current_root().lookup_by_full_name(location[9:])
        elif location.startswith("acct-guid="):
            try:
                guid, account = cls.validate_type("acct-guid=", location, ID_ACCOUNT, result)
            except ValueError:
                return False
        elif location.startswith("trans-guid="):
            try:
                guid, trans = cls.validate_type("trans-guid=", location, ID_TRANS, result)
            except ValueError:
                return False

            for split in trans.get_splits():
                account = split.get_account()
                if account is not None:
                    break
            if account is None:
                result.error_message = "Transaction with no Accounts: %s" % location
                return False

        elif location.startswith("split-guid="):
            try:
                guid, split = cls.validate_type("split-guid=", location, ID_SPLIT, result)
            except ValueError as e:
                return False
            account = split.get_account()
        else:
            result.error_message = "Unsupported entity type: %s" % location
            return False

        page = RegisterPluginPage.new(account, False)
        if result.parent is None:
            result.error_message = "No window to attach object"
            return False
        result.parent.open_page(page)
        if split is not None:
            gsr = page.get_gsr()
            gsr.jump_to_split(split)
        return True

    @classmethod
    def price_cb(cls, location, label, new_window, result):
        if location is None or result is None:
            return False

        if location.startswith("price-guid="):
            try:
                guid, price = cls.validate_type("acct-guid=", location, ID_ACCOUNT, result)
            except ValueError:
                return False

            if not Price.lookup(guid, Session.get_current_book()):
                result.error_message = "No such price: %s" % location
                return False
        else:
            result.error_message = "Badly formed URL %s" % location
            return False
        return True

    @classmethod
    def invoice_cb(cls, location, label, new_window, result):
        try:
            guid, invoice = cls.validate_type("invoice=", location, ID_INVOICE, result)
            InvoiceDialog.new_edit(result.parent, invoice)
        except (TypeError, ValueError):
            return False
        return True

    @classmethod
    def customer_cb(cls, location, label, new_window, result):
        try:
            guid, customer = cls.validate_type("customer=", location, ID_CUSTOMER, result)
            dialog= CustomerDialog(result.parent,book=Session.get_current_book(), customer=customer)
            dialog.run()
        except (TypeError, ValueError):
            return False
        return True

    @classmethod
    def supplier_cb(cls, location, label, new_window, result):
        try:
            guid, supplier = cls.validate_type("supplier=", location, ID_SUPPLIER, result)
            dialog = SupplierDialog(result.parent,book=Session.get_current_book(), supplier=supplier)
            dialog.run()
        except TypeError:
            return False
        return True

    @classmethod
    def init(cls):
        WebKitWebView.register_url_handlers(URL_TYPE_REGISTER, cls.register_cb)
        WebKitWebView.register_url_handlers(URL_TYPE_PRICE, cls.price_cb)
        WebKitWebView.register_url_handlers(URL_TYPE_INVOICE, cls.invoice_cb)
        WebKitWebView.register_url_handlers(URL_TYPE_CUSTOMER, cls.customer_cb)
        WebKitWebView.register_url_handlers(URL_TYPE_SUPPLIER, cls.supplier_cb)
