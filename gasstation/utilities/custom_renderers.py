from gi import require_version

require_version('Gtk', '3.0')
require_version('PangoCairo', '1.0')
from collections import defaultdict
from gi.repository import Gtk, GObject, Gdk
from gasstation.utilities.date import *
from .dialog import gs_handle_date_accelerator


def get_button_width():
    window = Gtk.Window(type=Gtk.WindowType.POPUP)
    button = Gtk.Button.new()
    button.show()
    window.add(button)
    arrow = Gtk.Image.new_from_icon_name("pan-down-symbolic", Gtk.IconSize.BUTTON)
    arrow.show()
    button.add(arrow)
    window.move(-500, -500)
    window.show()
    req = window.get_preferred_size()
    window.destroy()
    return req[0].width


class DataStore:
    data = defaultdict(dict)

    @classmethod
    def set_user_data(cls, self, k, v):
        cls.data[self][k] = v

    @classmethod
    def get_user_data(cls, self, k):
        return cls.data.get(self, {}).get(k, None)


class CellRendererPopUp(Gtk.CellRendererText):
    __gtype_name__ = "CellRendererPopUp"
    __gsignals__ = {
        'show-popup': (GObject.SignalFlags.RUN_LAST, None, (str, int, int, int, int)),
        'hide-popup': (GObject.SignalFlags.RUN_LAST, None, (int,))
    }

    def __init__(self):
        super().__init__()
        self.shown = False
        self.direction = 0
        self.popup_window = Gtk.Window.new(type=Gtk.WindowType.POPUP)
        self.popup_window.set_resizable(False)
        self.button_width = -1
        self.editable = None
        self.focus_window = None
        self.cell_text = ""
        self.text_column = 0
        self.editing_canceled = False
        self.entry_key_press = None
        self.focus_out_id = 0
        self.key_press_id = 0
        self.popup_window.connect("button-press-event", self.button_press_event)
        self.popup_window.connect("key-press-event", self.key_press_event)
        self.popup_window.connect("style-set", self.style_set)

    def style_set(self, *args):
        self.button_width = -1

    @staticmethod
    def grab_on_window(window):
        display = Gdk.Display().get_default()
        event = Gtk.get_current_event()
        seat = display.get_default_seat()
        if seat.grab(window, Gdk.SeatCapabilities.POINTER, True, None,
                     event, None, None) == Gdk.GrabStatus.SUCCESS:
            if seat.grab(window, Gdk.SeatCapabilities.KEYBOARD, True, None,
                         event, None, None) == Gdk.GrabStatus.SUCCESS:
                return True
        seat.ungrab()
        return False

    def show_popup(self, path, x1, y1, x2, y2):
        self.shown = True
        self.popup_window.realize()
        self.popup_window.move(-500, -500)
        self.popup_window.show()
        alloc = self.popup_window.get_allocation()
        x = x2
        y = y2
        button_height = y2 - y1
        screen_height = Gdk.Screen.height() - y
        screen_width = Gdk.Screen.width()
        if alloc.height > screen_height:
            if y - button_height > screen_height:
                y -= alloc.height + button_height
                if y < 0:
                    y = 0
        if x > screen_width:
            x = screen_width
        x -= alloc.width
        if x < 0:
            x = 0
        self.popup_window.grab_add()
        self.popup_window.move(x, y)
        self.popup_window.show()
        self.focus_window.grab_focus()
        self.grab_on_window(self.popup_window.get_window())

    def hide_popup(self):
        self.popup_window.grab_remove()
        self.popup_window.hide()
        if self.editable is not None:
            self.editable.editing_done()
        if self.editable is not None:
            self.editable.remove_widget()
        self.shown = False
        self.editing_canceled = False

    def arrow_clicked(self, editable, event, path):
        self.direction = not self.direction
        if self.shown:
            self.editing_canceled = True
            self.hide_popup()
        window = event.get_window()
        if not self.grab_on_window(window):
            return
        win, x, y = window.get_origin()
        alloc = editable.get_allocation()
        if self.focus_window is not None:
            self.focus_window.grab_focus()
        self.show_popup(path, alloc.x, alloc.y, alloc.x + alloc.width, y + alloc.height)

    def key_press_event(self, _, event):
        if event.keyval not in (Gdk.KEY_Escape, Gdk.KEY_Return, Gdk.KEY_KP_Enter,
                                Gdk.KEY_ISO_Enter, Gdk.KEY_3270_Enter):
            return False
        if event.keyval == Gdk.KEY_Escape:
            self.editing_canceled = True
        else:
            self.editing_canceled = False
        self.hide_popup()
        return True

    def button_press_event(self, widget, event):
        if event.button != 1:
            return False
        x = event.x_root
        y = event.y_root
        xoffset, yoffset = widget.get_window().get_root_origin()
        alloc = widget.get_allocation()
        xoffset += alloc.x
        yoffset += alloc.y
        alloc = self.popup_window.get_allocation()
        x1 = alloc.x + xoffset
        y1 = alloc.y + yoffset
        x2 = x1 + alloc.width
        y2 = y1 + alloc.height
        if x1 < x < x2 and y1 < y < y2:
            return False
        self.editing_canceled = True
        self.hide_popup()
        return False

    def focus_out_event(self, editable, _):
        if self.shown:
            return False
        self.editing_canceled = True
        editable.editing_done()
        editable.remove_widget()
        return False

    def editing_done(self, _, path, focus_id, key_press_id, icon_press_id):
        self.editable.disconnect(focus_id)
        self.editable.disconnect(key_press_id)
        self.editable.disconnect(icon_press_id)
        canceled = self.editing_canceled
        self.stop_editing(canceled)
        if canceled:
            return
        new_text = self.editable.get_text()
        self.emit("edited", path, new_text)

    def do_start_editing(self, event, widget, path, background_area, cell_area, flags):
        self.editing_canceled = False
        if not self.get_property("editable"):
            return
        self.shown = False
        xalign, yalign = self.get_alignment()
        editable = Gtk.Entry()
        editable.set_has_frame(False)
        editable.set_alignment(xalign)
        editable.grab_focus()
        editable.show()
        text = self.get_property("text")
        self.cell_text = text
        if text:
            editable.set_text(text)
        editable.select_region(0, -1)
        editable.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "pan-down-symbolic")
        focus_out_id = editable.connect("focus-out-event", self.focus_out_event)
        key_press_id = editable.connect("key_press_event", self.entry_key_press_event)
        icon_press_id = editable.connect("icon-press", lambda entry, icon_pos, event: self.arrow_clicked(entry, event, path))
        editable.connect("editing-done", self.editing_done, path, focus_out_id, key_press_id, icon_press_id)

        self.editable = editable
        return self.editable

    def entry_key_press_event(self, editable, key_event):
        if key_event.keyval == Gdk.KEY_Escape:
            self.editing_canceled = True
            editable.editing_done()
            editable.remove_widget()
            return False
        date_string = editable.get_text()
        try:
            if scan_date(date_string):
                day, month, year = scan_date(date_string)
                when = datetime.date(year, month, day)
                try:
                    self.set_time(gs_handle_date_accelerator(key_event, when, date_string))
                except ValueError:
                    return False
                editable.set_text(print_datetime(time.mktime(time.localtime())))
                return True
        except ValueError:
            return False
        except KeyError:
            return False
        return False


GObject.type_register(CellRendererPopUp)


class CellRendererDate(CellRendererPopUp):
    __gtype_name__ = "CellRendererDate"
    __gproperties__ = {
        "use-buttons": (bool, None, None, False,
                        GObject.ParamFlags.READWRITE),
    }

    def __init__(self, use_buttons=False):
        super().__init__()
        frame = Gtk.Frame()
        self.use_buttons = False
        self.popup_window.add(frame)
        frame.set_shadow_type(Gtk.ShadowType.OUT)
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        vbox.set_homogeneous(False)
        frame.add(vbox)
        self.calendar = Gtk.Calendar()
        self.focus_window = self.calendar
        vbox.pack_start(self.calendar, True, True, 0)
        self.button_box = Gtk.ButtonBox.new(orientation=Gtk.Orientation.HORIZONTAL)
        self.button_box.set_spacing(6)
        vbox.pack_start(self.button_box, False, True, 0)
        button = Gtk.Button(label="Cancel")
        self.button_box.add(button)
        button.connect("clicked", self.cancel_clicked)
        self.today_button = Gtk.Button(label="Today")
        self.button_box.add(self.today_button)
        self.today_button.connect("clicked", self.today_clicked)
        button = Gtk.Button(label="Select")
        self.button_box.add(button)
        self.calendar.connect("day-selected", self.day_selected)
        button.connect("clicked", self.okay_clicked)
        self.calendar.connect("day-selected-double-click", self.selected_double_click)

        self.time = datetime.datetime.now()
        frame.show_all()
        self.set_property("use-buttons", use_buttons)

    def do_get_property(self, prop):
        if prop.name == 'use-buttons':
            return self.use_buttons

    def do_set_property(self, prop, value):
        if not isinstance(value, bool):
            raise TypeError("Expected boolean got %s" % type(value))

        if prop.name == 'use-buttons':
            self.use_buttons = value
            if self.use_buttons:
                self.button_box.show()
            else:
                self.button_box.hide()

        else:
            raise AttributeError('unknown property %s' % prop.name)

    def show_popup(self, path, x1, y1, x2, y2):
        super().show_popup(path, x1, y1, x2, y2)
        self.calendar.handler_block_by_func(self.day_selected)
        text = self.editable.get_text()
        if text == "":
            self.time = datetime.datetime.now()
            day, month, year = self.time.day, self.time.month, self.time.year
        else:
            day, month, year = scan_date(text)
        self.calendar.clear_marks()
        self.calendar.select_month(month - 1, year)
        self.calendar.select_day(day)
        self.calendar.mark_day(day)
        self.calendar.handler_unblock_by_func(self.day_selected)

    def okay_clicked(self, widget):
        self.day_selected(widget)
        self.editing_canceled = False
        self.hide_popup()

    def today_clicked(self, _):
        self.time = datetime.date.today()
        self.calendar.clear_marks()
        self.calendar.select_month(self.time.month-1, self.time.year)
        self.calendar.select_day(self.time.day)
        self.calendar.mark_day(self.time.day)

    def selected_double_click(self, widget):
        self.okay_clicked(widget)

    def cancel_clicked(self, _):
        self.editing_canceled = True
        self.hide_popup()

    def day_selected(self, _):
        year, month, day = self.calendar.get_date()
        self.time = datetime.date(year, month+1, day)
        self.editable.set_text(print_datetime(self.time))
        self.editable.select_region(0, -1)

    def set_time(self, time):
        self.time = time
        self.editable.set_text(print_datetime(self.time))

GObject.type_register(CellRendererDate)
