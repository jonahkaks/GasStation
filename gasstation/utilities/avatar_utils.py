import math
import uuid

import cairo
from gi.repository import GLib, Gdk, GdkPixbuf, Gtk
from gi.repository import PangoCairo, Pango

from gasstation.utilities.gtk import ensure_dir

IMAGE_SIZE = 512


def round_image(pixbuf):
    size = GdkPixbuf.Pixbuf.get_width(pixbuf)
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, size, size)
    cr = cairo.Context(surface)
    cr.arc(size / 2, size / 2, size / 2, 0, 2 * math.pi)
    cr.clip()
    cr.new_path()
    Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0)
    cr.paint()
    dest = Gdk.pixbuf_get_from_surface(surface, 0, 0, size, size)
    return dest


def extract_initials_from_name(name):
    if name is None:
        return
    return "".join([a[0] for a in name.strip().replace("  "," ").split(" ") if a != " "])

def get_color_for_name(name):
    gnome_color_palette = [
        (98, 160, 234),
        (53, 132, 228),
        (28, 113, 216),
        (26, 95, 180),
        (87, 227, 137),
        (51, 209, 122),
        (46, 194, 126),
        (38, 162, 105),
        (248, 228, 92),
        (246, 211, 45),
        (245, 194, 17),
        (229, 165, 10),
        (255, 163, 72),
        (255, 120, 0),
        (230, 97, 0),
        (198, 70, 0),
        (237, 51, 59),
        (224, 27, 36),
        (192, 28, 40),
        (165, 29, 45),
        (192, 97, 203),
        (163, 71, 186),
        (129, 61, 156),
        (97, 53, 131),
        (181, 131, 90),
        (152, 106, 68),
        (134, 94, 60),
        (99, 69, 44)
    ]
    color = Gdk.RGBA(255, 255, 255, 1.0)
    if name is None or len(name) == 0:
        return color
    i = name[0].lower()
    idx = ord(i) - 95 if i.isalpha() else 0
    color.red, color.green, color.blue = gnome_color_palette[idx]
    return color


def generate_user_picture(name, size):
    initials = extract_initials_from_name(name)
    font = "Sans %d" % int(math.ceil(size / 3.5))
    color = get_color_for_name(name)
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, size, size)
    cr = cairo.Context(surface)
    cr.rectangle(0, 0, size, size)
    cr.set_source_rgb(color.red / 255.0, color.green / 255.0, color.blue / 255.0)
    cr.fill()
    cr.set_source_rgb(1.0, 1.0, 1.0)
    layout = PangoCairo.create_layout(cr)
    layout.set_text(initials, -1)
    font_desc = Pango.FontDescription.from_string(font)
    layout.set_font_description(font_desc)
    width, height = layout.get_size()
    cr.translate(size / 2, size / 2)
    cr.move_to(- (width / Pango.SCALE) / 2, - (height / Pango.SCALE) / 2)
    PangoCairo.show_layout(cr, layout)
    return surface


def save_icon(pixbuf: GdkPixbuf.Pixbuf):
    path = GLib.build_filenamev([GLib.get_user_data_dir(), "gasstation", "profile-images", uuid.uuid4().hex])
    ensure_dir(path)
    if isinstance(pixbuf, Gtk.Image):
        pixbuf = pixbuf.get_pixbuf()
    try:
        pixbuf.savev(path, "png", None, None)
    except GLib.Error:
        return
    return path


def generate_default_avatar(full_name, size):
    surface = generate_user_picture(full_name, size)
    return Gdk.pixbuf_get_from_surface(surface, 0, 0, size, size)
