from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import Gtk

RESPONSE_NEW = 1
RESPONSE_DELETE = 2
RESPONSE_EDIT = 3


def ask_ok_cancel(parent=None, default_response=Gtk.ResponseType.OK, message="", title=""):
    dialog = Gtk.MessageDialog(transient_for=parent, title=title, modal=True, destroy_with_parent=True,
                               message_type=Gtk.MessageType.QUESTION,
                               buttons=Gtk.ButtonsType.OK_CANCEL,
                               message_format=message)
    if parent is None:
        dialog.set_skip_taskbar_hint(False)
    dialog.set_default_response(default_response)
    response = dialog.run()
    dialog.destroy()
    return response


def ask_yes_no(parent=None, yes_is_default=True, message="", title=""):
    dialog = Gtk.MessageDialog(transient_for=parent, title=title, modal=True, destroy_with_parent=True,
                               message_type=Gtk.MessageType.QUESTION,
                               buttons=Gtk.ButtonsType.YES_NO,
                               message_format=message)
    if parent is None:
        dialog.set_skip_taskbar_hint(False)
    dialog.set_default_response(Gtk.ResponseType.YES if yes_is_default else Gtk.ResponseType.NO)
    response = dialog.run()
    dialog.destroy()
    return response == Gtk.ResponseType.YES


def show_warning(parent, info):
    dialog_common(parent, Gtk.MessageType.WARNING, info)


def dialog_common(parent, msg_type, buffer):
    dialog = Gtk.MessageDialog(transient_for=parent, modal=True, destroy_with_parent=True,
                               message_type=msg_type,
                               buttons=Gtk.ButtonsType.CLOSE,
                               message_format=buffer)
    if parent is None:
        dialog.set_skip_taskbar_hint(False)
    response = dialog.run()
    dialog.destroy()
    return response


def show_info(parent, info):
    dialog_common(parent, Gtk.MessageType.INFO, info)


def show_error(parent, info):
    dialog_common(parent, Gtk.MessageType.ERROR, info)


def choose_radio_option_dialog(parent, title, msg, button_name, default_value, radio_list):
    class RadioResult:
        def __init__(self):
            self.a = 0

    radio_result = RadioResult()
    group = None
    main_vbox = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=3)
    main_vbox.set_homogeneous(False)
    main_vbox.show()
    label = Gtk.Label(label=msg)
    label.set_justify(Gtk.Justification.LEFT)
    main_vbox.pack_start(label, True, True, 0)
    label.show()

    vbox = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=3)
    vbox.set_homogeneous(True)
    vbox.set_border_width(6)
    main_vbox.add(vbox)
    vbox.show()
    for i, r in enumerate(radio_list):
        radio_button = Gtk.RadioButton.new_with_mnemonic_from_widget(group, r)
        group = radio_button
        radio_button.set_halign(Gtk.Align.START)
        if i == default_value:
            radio_button.set_active(True)
            radio_result.a = default_value
        radio_button.show()
        vbox.pack_start(radio_button, False, False, 0)
        radio_button.connect("clicked", choose_radio_button_cb, i, radio_result)

    if button_name is None:
        button_name = "_OK"
    dialog = Gtk.Dialog(title, parent, Gtk.DialogFlags.DESTROY_WITH_PARENT)
    dialog.add_buttons("_Cancel", Gtk.ResponseType.CANCEL, button_name, Gtk.ResponseType.OK)
    dialog.set_default_response(Gtk.ResponseType.OK)
    dvbox = dialog.get_content_area()
    dvbox.pack_start(main_vbox, True, True, 0)
    if dialog.run() != Gtk.ResponseType.OK:
        radio_result.a = -1
    dialog.destroy()
    return radio_result.a


def choose_radio_button_cb(w, i, mrr):
    if w.get_active():
        mrr.a = i
