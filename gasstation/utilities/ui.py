from decimal import Decimal

from gi.repository import Gtk

from gasstation.utilities.date import *
from gasstation.utilities.preferences import *
from libgasstation.core import Account, AccountType, Session, PriceDB
from libgasstation.core.commodity import *
from libgasstation.core.hook import *

PREFS_GROUP_OPEN_SAVE = "dialogs.open-save"
PREFS_GROUP_EXPORT = "dialogs.export-accounts"
PREFS_GROUP_REPORT = "dialogs.report"
PREF_AUTO_DECIMAL_POINT = "auto-decimal-point"
PREF_AUTO_DECIMAL_PLACES = "auto-decimal-places"

PREF_CURRENCY_CHOICE_LOCALE = "currency-choice-locale"
PREF_CURRENCY_CHOICE_OTHER = "currency-choice-other"
PREF_CURRENCY_OTHER = "currency-other"
PREF_REVERSED_ACCTS_NONE = "reversed-accounts-none"
PREF_REVERSED_ACCTS_CREDIT = "reversed-accounts-credit"
PREF_REVERSED_ACCTS_INC_EXP = "reversed-accounts-incomeexpense"
PREF_PRICES_FORCE_DECIMAL = "force-price-decimal"

auto_decimal_enabled = False
user_default_currency = None
user_report_currency = None
auto_decimal_places = 2
maximum_decimals = 15
cached_default_currency = None
cached_default_report_currency = None
locale.setlocale(locale.LC_ALL, '')
lc = locale.localeconv()

pow_10 = [1, 10, 100, 1000, 10000, 100000, 1000000,
          10000000, 100000000, 1000000000, 10000000000,
          100000000000, 1000000000000, 10000000000000,
          100000000000000, 1000000000000000]


def gs_get_account_name_for_split_register(account, show_leaf_accounts):
    if account is None:
        return ""
    if show_leaf_accounts:
        return account.get_name()
    else:
        return account.get_full_name()


def gs_get_account_name_for_register(account):
    if account is None:
        return ""
    show_leaf_accounts = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                          PREF_SHOW_LEAF_ACCT_NAMES)
    return gs_get_account_name_for_split_register(account, show_leaf_accounts)


def gs_account_lookup_for_register(base_account, name):
    if base_account is None:
        return ""
    show_leaf_accounts = gs_pref_get_bool(PREFS_GROUP_GENERAL_REGISTER,
                                          PREF_SHOW_LEAF_ACCT_NAMES)
    if show_leaf_accounts:
        return base_account.lookup_by_name(name)
    else:
        return base_account.lookup_by_full_name(name)


def gs_set_default_directory(section, directory):
    gs_pref_set_string(section, PREF_LAST_PATH, directory)


def gs_get_default_directory(section):
    dir = gs_pref_get_string(section, PREF_LAST_PATH)
    if dir is None:
        dir = GLib.get_home_dir()
    return dir


def gs_set_auto_decimal_enabled(settings, key, *user_data):
    global auto_decimal_enabled
    auto_decimal_enabled = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_AUTO_DECIMAL_POINT)


def gs_set_auto_decimal_places(settings, key, *user_data):
    global auto_decimal_places
    auto_decimal_places = gs_pref_get_int(PREFS_GROUP_GENERAL, PREF_AUTO_DECIMAL_PLACES)


def gs_auto_decimal_init():
    global auto_decimal_enabled, auto_decimal_places
    auto_decimal_enabled = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_AUTO_DECIMAL_POINT)
    auto_decimal_places = gs_pref_get_int(PREFS_GROUP_GENERAL, PREF_AUTO_DECIMAL_PLACES)





reverse_type = {}
reverse_balance_inited = False


def gs_configure_reverse_balance(*args):
    global reverse_type, reverse_balance_inited
    for i in range(AccountType.NUM_ACCOUNT_TYPES):
        reverse_type[i] = True
    # if gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_REVERSED_ACCTS_INC_EXP):
    #     reverse_type[AccountType.ASSET] = True
    #     reverse_type[AccountType.EXPENSE] = True
    #     reverse_type[AccountType.RECEIVABLE] = True
    # elif gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_REVERSED_ACCTS_CREDIT):
    #     reverse_type[AccountType.LIABILITY] = True
    #     reverse_type[AccountType.PAYABLE] = True
    #     reverse_type[AccountType.EQUITY] = True
    #     reverse_type[AccountType.INCOME] = True
    #     reverse_type[AccountType.CREDIT] = True


def add_summary_label(summary_bar, label_str):
    hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
    hbox.set_homogeneous(False)
    summary_bar.pack_start(hbox, False, False, 5)
    label = Gtk.Label(label=label_str)
    label.set_alignment(1.0, 0.5)
    hbox.pack_start(label, False, False, 0)
    label = Gtk.Label.new()
    label.set_alignment(1.0, 0.5)
    hbox.pack_start(label, False, False, 0)
    return label


def add_toolbar_label(toolbar, label_str):
    hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=4)
    hbox.set_margin_right(10)
    hbox.set_homogeneous(False)
    toolitem = Gtk.ToolItem()
    toolitem.add(hbox)
    toolbar.insert(toolitem, -1)
    label = Gtk.Label()
    label.set_use_markup(True)
    label.set_markup("<b>{}</b>".format(label_str))
    label.set_alignment(1.0, 0.5)
    hbox.pack_start(label, False, False, 0)
    label = Gtk.Label.new()
    label.set_alignment(1.0, 0.5)
    hbox.pack_start(label, False, False, 0)
    return label


def gs_locale_default_iso_currency_code():
    code = lc.get("int_curr_symbol", "UGX").strip()
    return code


def gs_locale_decimal_places():
    code = lc.get("int_frac_digits", "2").strip()
    return int(code)


def gs_locale_default_currency_nodefault():
    table = Session.get_current_commodity_table()
    code = gs_locale_default_iso_currency_code()
    currency = table.lookup(COMMODITY_NAMESPACE_NAME_CURRENCY, code)
    return currency


def gs_locale_default_currency():
    currency = gs_locale_default_currency_nodefault()
    return currency if currency is not None else Session.get_current_commodity_table().lookup(
        COMMODITY_NAMESPACE_NAME_CURRENCY,
        "UGX")


def gs_default_currency_common(requested_currency, section):
    currency = None
    if requested_currency is not None:
        return Session.get_current_commodity_table().lookup(COMMODITY_NAMESPACE_NAME_CURRENCY, requested_currency)
    if Session.get_current_book().use_currency():
        currency = Session.get_current_book().get_currency()
        if currency is not None:
            return currency

    if gs_pref_get_bool(section, PREF_CURRENCY_CHOICE_OTHER):
        mnemonic = gs_pref_get_string(section, PREF_CURRENCY_OTHER)
        currency = Session.get_current_commodity_table().lookup(COMMODITY_NAMESPACE_NAME_CURRENCY, mnemonic)
    if currency is None:
        currency = gs_locale_default_currency()
    return currency


def gs_default_currency():
    global user_default_currency
    return gs_default_currency_common(user_default_currency, PREFS_GROUP_GENERAL)


def gs_account_or_default_currency(account):
    if account is None:
        currency_from_account_found = False
        return gs_default_currency(), currency_from_account_found
    currency = account.get_currency_or_parent()
    if currency:
        currency_from_account_found = True
    else:
        currency_from_account_found = False
        currency = gs_default_currency()
    return currency, currency_from_account_found


def gs_default_report_currency():
    global user_report_currency
    return gs_default_currency_common(user_report_currency, PREFS_GROUP_GENERAL_REPORT)


def gs_currency_changed_cb(settings, key, *args):
    global user_default_currency, user_report_currency
    user_default_currency = None
    user_report_currency = None
    Hook.run(HOOK_CURRENCY_CHANGED)


def is_decimal_fraction(fraction):
    max_decimal_places = 0
    if fraction <= 0:
        return False, max_decimal_places

    while fraction != 1:
        if fraction % 10 != 0:
            return False, max_decimal_places
        fraction /= 10
        max_decimal_places += 1
    return True, max_decimal_places


class PrintAmountInfo:
    def __init__(self):
        self.commodity = gs_default_currency()
        self.max_decimal_places = 0
        self.min_decimal_places = 0
        self.use_separators = 1
        self.use_symbol = 1
        self.use_locale = 1
        self.round = 1
        self.force_fit = 0
        self.product = None

    @classmethod
    def default(cls, use_symbol=0, use_separator=1):
        info = cls()
        info.max_decimal_places = lc["int_frac_digits"]
        info.min_decimal_places = lc["int_frac_digits"]
        info.use_separators = use_separator
        info.use_locale = 1
        info.monetary = 1
        info.force_fit = 0
        info.round = 0
        info.use_symbol = use_symbol
        return info

    @classmethod
    def commodity(cls, commodity, use_symbol):
        if commodity is None:
            return cls.default(use_symbol)
        info = cls()
        info.commodity = commodity
        is_iso = commodity.is_iso()
        t, info.max_decimal_places = is_decimal_fraction(commodity.get_fraction() if commodity is not None else 1)
        if t:
            if is_iso:
                info.min_decimal_places = info.max_decimal_places
            else:
                info.min_decimal_places = 0
        else:
            info.max_decimal_places = info.min_decimal_places = 0

        info.use_separators = 1
        info.use_symbol = 1 if use_symbol else 0
        info.use_locale = 1 if is_iso else 0
        info.monetary = 1
        info.force_fit = 0
        info.round = 0
        return info

    @classmethod
    def product(cls, product, use_symbol):
        if product is None:
            return cls.default(use_symbol)
        info = cls()
        info.product = product
        info.commodity = None
        info.use_separators = 1
        info.use_symbol = 1 if use_symbol else 0
        info.use_locale = 1
        info.monetary = 1
        info.force_fit = 0
        info.round = 0
        return info

    @classmethod
    def account(cls, account, use_symbol):
        if account is None:
            return cls.default(use_symbol)
        info = cls()
        info.commodity = account.get_commodity()
        is_iso = info.commodity.is_iso() if info.commodity is not None else False
        scu = account.get_commodity_scu()
        t, info.max_decimal_places = is_decimal_fraction(scu if scu is not None else 1)
        if t:
            if is_iso:
                info.min_decimal_places = info.max_decimal_places
            else:
                info.min_decimal_places = 0
        else:
            info.max_decimal_places = info.min_decimal_places = 0

        info.use_separators = 1
        info.use_symbol = 1 if use_symbol else 0
        info.use_locale = 1 if is_iso else 0
        info.monetary = 1
        info.force_fit = 0
        info.round = 0
        return info

    @classmethod
    def split(cls, split, use_symbol):
        return cls.account(split.get_account(), use_symbol)

    @classmethod
    def helper(cls, decplaces):
        info = cls()
        info.commodity = None
        info.max_decimal_places = decplaces
        info.min_decimal_places = 0
        info.use_separators = 1
        info.use_symbol = 0
        info.use_locale = 1
        info.monetary = 1
        info.force_fit = 0
        info.round = 0
        return info

    @classmethod
    def share(cls):
        return cls.helper(5)

    @classmethod
    def share_places(cls, decplaces):
        info = cls.share()
        info.max_decimal_places = decplaces
        info.min_decimal_places = decplaces
        info.force_fit = 1
        info.round = 1
        return info

    @classmethod
    def price(cls, curr):
        force = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_PRICES_FORCE_DECIMAL)
        info = cls()
        info.commodity = curr
        if info.commodity is not None:
            frac = curr.get_fraction()
            decplaces = 2
            while frac != 1 and frac % 10 == 0:
                frac /= 10
                if not frac: break
                decplaces += 1
            info.max_decimal_places = decplaces
            info.min_decimal_places = decplaces
        else:
            info.max_decimal_places = 6
            info.min_decimal_places = 0
        info.use_separators = 1
        info.use_symbol = 0
        info.use_locale = 1
        info.monetary = 1

        info.force_fit = force
        info.round = force
        return info

    @classmethod
    def integral(cls, places):
        return cls.helper(0)


def sprintamount(val, info):
    assert isinstance(val, Decimal)
    bufp = ""
    print_sign = True
    print_absolute = False
    if info.use_locale:
        if val < 0:
            cs_precedes = lc["n_cs_precedes"]
            sep_by_space = lc["n_sep_by_space"]
        else:
            cs_precedes = lc["p_cs_precedes"]
            sep_by_space = lc["p_sep_by_space"]
    else:
        cs_precedes = True
        sep_by_space = True
    currency_symbol = ""
    if info.commodity is not None and info.use_symbol:
        currency_symbol = info.commodity.get_nice_symbol()
        if not info.commodity.is_iso():
            cs_precedes = False
            sep_by_space = True
    elif info.product is not None and info.use_symbol:
        unit = info.product.get_unit()
        if unit is not None:
            currency_symbol = unit.get_abbreviation()
    if val < 0:
        sign = lc["negative_sign"]
        sign_posn = lc["n_sign_posn"]
    else:
        sign = lc["positive_sign"]
        sign_posn = lc["p_sign_posn"]
    if val.is_zero() or not sign:
        print_sign = False
    if print_sign and sign_posn == 1:
        bufp += sign
    if cs_precedes:
        if print_sign and sign_posn == 3:
            bufp += sign
        if info.use_symbol:
            bufp += currency_symbol
            if sep_by_space:
                bufp += " "
        if print_sign and sign_posn == 4:
            bufp += sign

    if print_sign and sign_posn == 0:
        bufp += "("
        print_absolute = True
    bufp += print_amount_internal(val.__abs__() if print_absolute else val, info)
    if print_sign and sign_posn == 0:
        bufp += ")"
    if not cs_precedes:
        if print_sign and sign_posn == 3:
            bufp += sign
        if info.use_symbol:
            if sep_by_space:
                bufp += " "
            bufp += currency_symbol
        if print_sign and sign_posn == 4:
            bufp += sign

    if print_sign and sign_posn == 2:
        bufp += sign
    return bufp


def gs_account_print_amount(num, info):
    return sprintamount(num, info if info is not None else PrintAmountInfo.default(True))


def print_amount_internal(val, info):
    min_dp = info.min_decimal_places
    val = val.__abs__()
    f = "{:,.%df}" % min_dp
    if not info.use_separators:
        buf = f.format(val).replace(",", "")
    else:
        decimal_point = lc.get("mon_decimal_point", ".") if info.monetary else lc.get("decimal_point", ".")
        separator = lc.get("mon_thousands_sep", ",") if info.monetary else lc.get("thousands_sep", ",")
        buf = f.format(val).replace(",", "X").replace(".", decimal_point).replace("X", separator)

    return buf


def gs_normalize_account_separator(separator):
    if not separator or separator == "colon":
        new_sep = ":"
    elif separator == "slash":
        new_sep = "/"
    elif separator == "backslash":
        new_sep = "\\"
    elif separator == "dash":
        new_sep = "-"
    elif separator == "period":
        new_sep = "."
    else:
        new_sep = separator
    return new_sep


def gs_configure_account_separator(*args):
    string = gs_pref_get_string(PREFS_GROUP_GENERAL, PREF_ACCOUNT_SEPARATOR)
    separator = gs_normalize_account_separator(string)
    Account.set_separator(separator)


def gs_ui_util_init():
    from libgasstation.core.component import ComponentManager
    gs_configure_account_separator()
    gs_auto_decimal_init()
    gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_ACCOUNT_SEPARATOR,
                        gs_configure_account_separator)
    gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_REVERSED_ACCTS_NONE,
                        gs_configure_reverse_balance)
    gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_REVERSED_ACCTS_CREDIT,
                        gs_configure_reverse_balance)
    gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_REVERSED_ACCTS_INC_EXP,
                        gs_configure_reverse_balance)
    gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_CURRENCY_CHOICE_LOCALE,
                        gs_currency_changed_cb)
    gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_CURRENCY_CHOICE_OTHER,
                        gs_currency_changed_cb)
    gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_CURRENCY_OTHER,
                        gs_currency_changed_cb)
    gs_pref_register_cb(PREFS_GROUP_GENERAL_REPORT, PREF_CURRENCY_CHOICE_LOCALE,
                        gs_currency_changed_cb)
    gs_pref_register_cb(PREFS_GROUP_GENERAL_REPORT, PREF_CURRENCY_CHOICE_OTHER,
                        gs_currency_changed_cb)
    gs_pref_register_cb(PREFS_GROUP_GENERAL_REPORT, PREF_CURRENCY_OTHER,
                        gs_currency_changed_cb)
    gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_AUTO_DECIMAL_POINT,
                        gs_set_auto_decimal_enabled)
    gs_pref_register_cb(PREFS_GROUP_GENERAL, PREF_AUTO_DECIMAL_PLACES,
                        gs_set_auto_decimal_places)
    gs_configure_date_format()
    gs_configure_date_completion()
    gs_pref_register_cb(PREFS_GROUP_GENERAL,
                        PREF_DATE_FORMAT,
                        gs_configure_date_format)
    gs_pref_register_cb(PREFS_GROUP_GENERAL,
                        PREF_DATE_COMPL_THISYEAR,
                        gs_configure_date_completion)
    gs_pref_register_cb(PREFS_GROUP_GENERAL,
                        PREF_DATE_COMPL_SLIDING,
                        gs_configure_date_completion)
    gs_pref_register_cb(PREFS_GROUP_GENERAL,
                        PREF_DATE_BACKMONTHS,
                        gs_configure_date_completion)
    gs_pref_register_group_cb(PREFS_GROUP_GENERAL,
                              lambda *d: ComponentManager.refresh_all())


def gs_configure_date_format(*args):
    df = DateFormat(gs_pref_get_int(PREFS_GROUP_GENERAL, PREF_DATE_FORMAT))
    if df > DateFormat.LOCALE:
        return
    date_format_set(df)


def gs_configure_date_completion(*args):
    dc = DateCompletion.THISYEAR
    backmonths = gs_pref_get_float(PREFS_GROUP_GENERAL, PREF_DATE_BACKMONTHS)
    if backmonths < 0:
        backmonths = 0
    elif backmonths > 11:
        backmonths = 11

    if gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_DATE_COMPL_SLIDING):
        dc = DateCompletion.SLIDING
    date_completion_set(dc, backmonths)


def gs_ui_account_get_balance_full(fn, account, recurse, commodity):
    neg = False
    balance = fn(account, commodity, recurse)
    if gs_reverse_balance(account):
        balance = balance * -1
    if balance < 0:
        neg = True
    return balance, neg


def gs_ui_account_get_balance(account, recurse):
    return gs_ui_account_get_balance_full(Account.get_balance_in_currency, account, recurse, None)


def gs_ui_account_get_reconciled_balance(account, recurse):
    return gs_ui_account_get_balance_full(Account.get_reconciled_balance_in_currency, account, recurse, None)


def gs_ui_account_get_print_balance(fn, account, recurse):
    balance = fn(account, None, recurse)
    return sprintamount(balance, PrintAmountInfo.account(account, True)), balance < 0


def gs_ui_account_get_print_report_balance(fn, account, recurse):
    report_commodity = gs_default_report_currency()
    balance = fn(account, report_commodity, recurse)
    return sprintamount(balance, PrintAmountInfo.commodity(report_commodity, True)), balance < 0


def gs_ui_account_get_balance_as_of_date(account, date, include_children):
    book = account.get_book()
    pdb = PriceDB.get_db(book)
    if account is None:
        return Decimal(0)
    currency = account.get_commodity()
    balance = account.get_balance_as_of_date(date)
    if include_children:
        children = account.get_descendants()
        for child in children:
            child_currency = child.get_commodity()
            child_balance = child.get_balance_as_of_date(date)
            child_balance = pdb.convert_balance_latest_price(child_balance, child_currency, currency)
            balance += child_balance
    if gs_reverse_balance(account):
        balance = balance * -1
    return balance


def gs_ui_person_get_balance_full(person, commodity):
    if person is None:
        return Decimal(0)
    balance = person.get_balance_in_currency(commodity)
    return balance, balance < 0


def gs_ui_person_get_print_balance(person):
    balance, negative = gs_ui_person_get_balance_full(person, None)
    print_info = PrintAmountInfo.commodity(person.get_currency(), True)
    return sprintamount(balance, print_info), negative


def gs_ui_person_get_print_report_balance(person):
    report_commodity = gs_default_report_currency()
    balance, negative = gs_ui_person_get_balance_full(person, report_commodity)
    print_info = PrintAmountInfo.commodity(report_commodity, True)
    return sprintamount(balance, print_info), negative
