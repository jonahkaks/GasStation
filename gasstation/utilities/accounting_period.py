from gasstation.utilities.date import *
from .preferences import *


class AccountingPeriodType(IntEnum):
    INVALID = -1
    TODAY = 0
    MONTH = 1
    MONTH_PREV = 2
    QUARTER = 3
    QUARTER_PREV = 4
    CYEAR = 5
    CYEAR_PREV = 6
    CYEAR_LAST = 7
    FYEAR = CYEAR_LAST
    FYEAR_PREV = 8
    FYEAR_LAST = 9
    LAST = FYEAR_LAST


class AccountingPeriod:

    @classmethod
    def __set_fiscal_year_start(cls, date: datetime.date, fy_end: datetime.date):
        if date is None or fy_end is None:
            return
        temp = fy_end
        temp = temp.replace(year=fy_end.year)
        new_fy = date > temp
        date = temp
        date += relativedelta(days=1)
        if not new_fy:
            date -= relativedelta(years=1)
        return date

    @classmethod
    def __set_fiscal_year_end(cls, date: datetime.date, fy_end: datetime.date):
        if date is None or fy_end is None:
            return
        temp = fy_end
        temp = temp.replace(year=fy_end.year)
        new_fy = date > temp
        date = temp
        date += relativedelta(days=1)
        if not new_fy:
            date += relativedelta(years=1)
        return date

    @classmethod
    def __set_prev_fiscal_year_start(cls, date, fy_end):
        if date is None or fy_end is None:
            return
        return cls.__set_fiscal_year_start(date, fy_end) - relativedelta(years=1)

    @classmethod
    def __set_prev_fiscal_year_end(cls, date, fy_end):
        if date is None or fy_end is None:
            return
        return cls.__set_fiscal_year_end(date, fy_end) - relativedelta(years=1)

    @staticmethod
    def lookup_start_date_option(fy_end):
        if gs_pref_get_bool(PREFS_GROUP_ACCT_SUMMARY, PREF_START_CHOICE_ABS):
            t = gs_pref_get_int64(PREFS_GROUP_ACCT_SUMMARY, PREF_START_DATE)
        else:
            which = gs_pref_get_int(PREFS_GROUP_ACCT_SUMMARY, PREF_START_PERIOD)
            t = AccountingPeriod.start_date(which, fy_end, None)
        return t

    @staticmethod
    def lookup_end_date_option(fy_end):
        if gs_pref_get_bool(PREFS_GROUP_ACCT_SUMMARY, PREF_END_CHOICE_ABS):
            t = gs_pref_get_int64(PREFS_GROUP_ACCT_SUMMARY, PREF_END_DATE)
        else:
            which = gs_pref_get_int(PREFS_GROUP_ACCT_SUMMARY, PREF_END_PERIOD)
            t = AccountingPeriod.end_date(which, fy_end, None)
        return t

    @classmethod
    def get_fy_end(cls):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        date = book.get_fiscal_year_end()
        return date

    @classmethod
    def fiscal_start(cls):
        fy_end = cls.get_fy_end()
        t = cls.lookup_start_date_option(fy_end)
        return t

    @classmethod
    def fiscal_end(cls):
        fy_end = cls.get_fy_end()
        t = cls.lookup_end_date_option(fy_end)
        return t

    @classmethod
    def start_date(cls, which, fy_end, contains):
        if contains is not None:
            date = contains
        else:
            date = datetime.date.today()

        if which == AccountingPeriodType.MONTH:
            date = date.replace(day=1)

        elif which == AccountingPeriodType.MONTH_PREV:
            date = date.replace(day=1) - relativedelta(months=1)

        elif which == AccountingPeriodType.QUARTER:
            date = (date.replace(day=1) - relativedelta(months=(date.month - 1) % 3))

        elif which == AccountingPeriodType.QUARTER_PREV:
            date = (date.replace(day=1) - relativedelta(months=(date.month - 1) % 3)) - relativedelta(months=3)

        elif which == AccountingPeriodType.CYEAR:
            date = date.replace(month=1, day=1)

        elif which == AccountingPeriodType.CYEAR_PREV:
            date = date.replace(month=1, day=1) - relativedelta(years=1)

        elif which == AccountingPeriodType.FYEAR:
            if fy_end is None:
                return None
            date = cls.__set_fiscal_year_start(date, fy_end)

        elif which == AccountingPeriodType.FYEAR_PREV:
            if fy_end is None:
                return None
            date = cls.__set_prev_fiscal_year_start(date, fy_end)

        return date

    @classmethod
    def end_date(cls, which, fy_end, contains):
        if contains is not None:
            date = contains
        else:
            date = datetime.date.today()

        if which == AccountingPeriodType.TODAY:
            pass
        elif which == AccountingPeriodType.MONTH:
            date = date.replace(day=1) + relativedelta(months=1) - relativedelta(days=1)

        elif which == AccountingPeriodType.MONTH_PREV:
            date = date.replace(day=1) - relativedelta(days=1)

        elif which == AccountingPeriodType.QUARTER:
            date = date.replace(day=1) + relativedelta(months=3 - ((date.month - 1) % 3)) - relativedelta(days=1)

        elif which == AccountingPeriodType.QUARTER_PREV:
            date = (date.replace(day=1) + relativedelta(months=3 - ((date.month - 1) % 3)) - relativedelta(days=1)) \
                   - relativedelta(months=3)

        elif which == AccountingPeriodType.CYEAR:
            date = date.replace(month=12, day=31)

        elif which == AccountingPeriodType.CYEAR_PREV:
            date = date.replace(month=12, day=31) - relativedelta(years=1)

        elif which == AccountingPeriodType.FYEAR:
            if fy_end is None:
                return None
            date = cls.__set_fiscal_year_end(date, fy_end)

        elif which == AccountingPeriodType.FYEAR_PREV:
            if fy_end is None:
                return None
            date = cls.__set_prev_fiscal_year_end(date, fy_end)
        else:
            return None
        return date
