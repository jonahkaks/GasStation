import datetime
import locale
import os
import time
from enum import IntEnum

from dateutil.relativedelta import relativedelta

if os.name == "posix":
    D_FMT = locale.nl_langinfo(locale.D_FMT)
    D_T_FMT = locale.nl_langinfo(locale.D_T_FMT)
    T_FMT = locale.nl_langinfo(locale.T_FMT)
else:
    D_FMT = '%x'
    D_T_FMT = '%a %b %e %H:%M:%S %Y'
    T_FMT = '%H:%M:%S'
MAX_DATE_LENGTH = 34
UTC_DATE_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


class DateFormat(IntEnum):
    US = 0
    UK = 1
    CE = 2
    ISO = 3
    LOCALE = 4
    CUSTOM = 5
    UNSET = 6
    UTC = 7


DATE_FORMAT_FIRST = DateFormat.US
DATE_FORMAT_LAST = DateFormat.UTC


class DateCompletion(IntEnum):
    THISYEAR = 0
    SLIDING = 1


class DateMonthFormat(IntEnum):
    NUMBER = 0
    ABBREV = 1
    NAME = 2


class DayPart(IntEnum):
    start = 0
    neutral = 1
    end = 2


dateFormat = DateFormat.LOCALE
prevQofDateFormat = DateFormat.LOCALE
dateCompletion = DateCompletion.THISYEAR
dateCompletionBackMonths = 6


def date_format_get():
    global dateFormat
    return dateFormat


def date_format_set(df):
    global prevQofDateFormat, dateFormat
    if DATE_FORMAT_FIRST <= df <= DATE_FORMAT_LAST:
        prevQofDateFormat = dateFormat
        dateFormat = df
    else:
        prevQofDateFormat = dateFormat
        dateFormat = DateFormat.ISO


def date_completion_set(dc, backmonths):
    global dateCompletion, dateCompletionBackMonths
    dateCompletion = DateCompletion.THISYEAR
    if dc == DateCompletion.THISYEAR or dc == DateCompletion.SLIDING:
        dateCompletion = dc
    if backmonths < 0:
        backmonths = 0
    elif backmonths > 11:
        backmonths = 11
    dateCompletionBackMonths = backmonths


def date_format_get_string(df):
    global dateFormat
    if df == DateFormat.US:
        return "%m/%d/%Y"
    elif df == DateFormat.UK:
        return "%d/%m/%Y"
    elif df == DateFormat.CE:
        return "%d.%m.%Y"
    elif df == DateFormat.UTC:
        return "%Y-%m-%dT%H:%M:%SZ"
    elif df == DateFormat.ISO:
        return "%Y-%m-%d"
    elif df == DateFormat.UNSET:
        return date_format_get_string(dateFormat)
    return "%x"


def date_text_format_get_string(df):
    global dateFormat
    if df == DateFormat.US:
        return "%b %d, %Y"
    elif df == DateFormat.UK or df == DateFormat.CE:
        return "%d %b %Y"
    elif df == DateFormat.UTC:
        return "%Y-%m-%dT%H:%M:%SZ"
    elif df == DateFormat.ISO:
        return "%Y-%b-%d"
    elif df == DateFormat.UNSET:
        return date_text_format_get_string(dateFormat)
    return "%x"


def date_string_to_dateformat(fmt_str):
    if fmt_str == "us":
        format = DateFormat.US
    elif fmt_str == "uk":
        format = DateFormat.UK
    elif fmt_str == "ce":
        format = DateFormat.CE
    elif fmt_str == "utc":
        format = DateFormat.UTC
    elif fmt_str == "iso":
        format = DateFormat.ISO
    elif fmt_str == "locale":
        format = DateFormat.LOCALE
    elif fmt_str == "custom":
        format = DateFormat.CUSTOM
    elif fmt_str == "unset":
        format = DateFormat.UNSET
    else:
        format = None
    return format


def date_dateformat_to_string(f):
    if f == DateFormat.US:
        return "us"
    elif f == DateFormat.UK:
        return "uk"
    elif f == DateFormat.CE:
        return "ce"
    elif f == DateFormat.ISO:
        return "iso"
    elif f == DateFormat.UTC:
        return "utc"
    elif f == DateFormat.LOCALE:
        return "locale"
    elif f == DateFormat.CUSTOM:
        return "custom"
    elif f == DateFormat.UNSET:
        return "unset"
    else:
        return None


def date_monthformat_to_string(fmt):
    if fmt == DateMonthFormat.NUMBER:
        return "number"
    elif fmt == DateMonthFormat.ABBREV:
        return "abbrev"
    elif fmt == DateMonthFormat.NAME:
        return "name"
    else:
        return "number"


def date_string_to_monthformat(fmt_str):
    if fmt_str == "number":
        return DateMonthFormat.NUMBER
    elif fmt_str == "abbrev":
        return DateMonthFormat.ABBREV
    elif fmt_str == "name":
        return DateMonthFormat.NAME
    return DateMonthFormat.NUMBER


def dateSeparator():
    global dateFormat
    if dateFormat == DateFormat.CE:
        return '.'
    elif dateFormat == DateFormat.ISO or dateFormat == DateFormat.UTC:
        return '-'
    elif dateFormat == DateFormat.US or dateFormat == DateFormat.UK:
        return '/'
    elif dateFormat == DateFormat.LOCALE:
        st = date_format_get_string(dateFormat)
        for x in st:
            if not x.isdigit():
                return x
    else:
        return "/"


def print_datetime(dt):
    if dt is None:
        dt = datetime.date.today()
    return dt.strftime(date_format_get_string(dateFormat))


def print_dmy_buff(day, month, year):
    global dateFormat
    return datetime.date(year, month, day).strftime(date_format_get_string(dateFormat))


def floordiv(a, b):
    if a >= 0:
        return a / b
    else:
        return -((-a - 1) / b) - 1


import re

clean_date = re.compile('\D+')


def strtok(buf):
    c = clean_date.split(buf)
    if len(c) == 3:
        return c
    elif len(c) == 2:
        return c[0], c[1], None
    elif len(c) == 1:
        return c[0], None, None
    elif len(c) == 0:
        return None, None, None
    else:
        return c[:3]


def scan_date_internal(buff, which):
    global prevQofDateFormat, dateCompletionBackMonths, dateCompletion
    if not buff or buff is None:
        return False
    buff = buff.strip()
    if which == DateFormat.UTC:
        try:
            tm = time.strptime(buff, UTC_DATE_FORMAT)
            return tm.tm_mday, tm.tm_mon, tm.tm_year
        except ValueError:
            return False
    dupe = buff
    tmp = dupe
    first_field = None
    second_field = None
    third_field = None
    if tmp:
        first_field, second_field, third_field = strtok(tmp)
    now = datetime.datetime.now().timetuple()
    now_day = now.tm_mday
    now_month = now.tm_mon
    now_year = now.tm_year
    iday = now_day
    imonth = now_month
    iyear = -1
    if which == DateFormat.LOCALE:
        if len(buff):
            try:
                thetime = time.strptime(buff, "%x")
            except ValueError:
                thetime = time.localtime()
            if third_field is not None:
                iyear = thetime.tm_year
                iday = thetime.tm_mday
                imonth = thetime.tm_mon
            elif second_field is not None:
                if thetime.tm_year == -1:
                    iday = thetime.tm_mday
                    imonth = thetime.tm_mon
                elif thetime.tm_mon != -1:
                    imonth = int(first_field)
                    iday = int(second_field)
                else:
                    iday = int(first_field)
                    imonth = int(second_field)

            elif first_field is not None:
                iday = int(first_field)

    elif which == DateFormat.UK or which == DateFormat.CE:
        if third_field is not None:
            iday = int(first_field)
            imonth = int(second_field)
            iyear = int(third_field)
        elif second_field is not None:
            iday = int(first_field)
            imonth = int(second_field)
        elif first_field is not None:
            iday = int(first_field)
    elif which == DateFormat.ISO:
        if third_field is not None:
            iyear = int(first_field)
            imonth = int(second_field)
            iday = int(third_field)
        elif second_field is not None:
            imonth = int(first_field)
            iday = int(second_field)
        elif first_field is not None:
            iday = int(first_field)
    else:
        if third_field is not None:
            imonth = int(first_field)
            iday = int(second_field)
            iyear = int(third_field)
        elif second_field is not None:
            imonth = int(first_field)
            iday = int(second_field)
        elif first_field is not None:
            iday = int(first_field)

    if imonth == 0 or iday == 0:
        return False
    if 12 < imonth or 31 < iday:
        if which != prevQofDateFormat and scan_date_internal(buff, prevQofDateFormat):
            return scan_date_internal(buff, prevQofDateFormat)
        if imonth > 12 >= iday:
            tmp = imonth
            imonth = iday
            iday = tmp
        else:
            return False
    if iyear == -1:
        if dateCompletion == DateCompletion.THISYEAR:
            iyear = now_year
        else:
            iyear = now_year - floordiv(imonth - now_month + dateCompletionBackMonths, 12)

    if iyear < 100:
        iyear += int((now_year + 50 - iyear) / 100) * 100
    return int(iday), int(imonth), int(iyear)


def scan_date(buf):
    global dateFormat
    return scan_date_internal(buf, dateFormat)


def dmy2time64_internal(day, month, year, day_part):
    date = datetime.date(year, month, day)
    if day_part == DayPart.start:
        date = datetime.datetime.combine(date, datetime.time(0, 0, 0, 0))
    elif day_part == DayPart.end:
        date = datetime.datetime.combine(date, datetime.time(23, 59, 59, 0))
    elif day_part == DayPart.neutral:
        date = datetime.datetime.combine(date, datetime.time(10, 59, 59, 0))
    return date


def dmy2time64(day, month, year):
    return dmy2time64_internal(day, month, year, DayPart.start)


def dmy2time64_end(day, month, year):
    return dmy2time64_internal(day, month, year, DayPart.end)


def dmy2time64_neutral(day, month, year):
    return dmy2time64_internal(day, month, year, DayPart.neutral)


class RelativeDate:
    rel_hash = {}

    @classmethod
    def get_absolute(cls, date_symbol):
        info = cls.rel_hash.get(date_symbol)
        fnc = info[2]
        return fnc()

    @classmethod
    def get_date_strings(cls, date_symbol):
        info = cls.rel_hash.get(date_symbol)
        return info[0], info[1]

    @classmethod
    def get_date_string(cls, date_symbol):
        info = cls.rel_hash.get(date_symbol)
        return info[0]

    @classmethod
    def get_date_desc(cls, date_symbol):
        info = cls.rel_hash.get(date_symbol)
        return info[1]

    @classmethod
    def get_start_cal_year(cls):
        return datetime.date.today().replace(month=1, day=1)

    @classmethod
    def get_end_cal_year(cls):
        return datetime.date.today().replace(month=12, day=31)

    @classmethod
    def get_start_prev_year(cls):
        return (datetime.date.today() - relativedelta(years=1)).replace(month=1, day=1)

    @classmethod
    def get_start_next_year(cls):
        return (datetime.date.today() + relativedelta(years=1)).replace(month=1, day=1)

    @classmethod
    def get_end_prev_year(cls):
        return (datetime.date.today() - relativedelta(years=1)).replace(month=12, day=31)

    @classmethod
    def get_end_next_year(cls):
        return (datetime.date.today() + relativedelta(years=1)).replace(month=12, day=31)

    @classmethod
    def get_start_accounting_period(cls):
        from .accounting_period import AccountingPeriod
        return AccountingPeriod.fiscal_start()

    @classmethod
    def get_end_accounting_period(cls):
        from .accounting_period import AccountingPeriod
        return AccountingPeriod.fiscal_end()

    @classmethod
    def get_start_this_month(cls):
        return datetime.date.today().replace(day=1)

    @classmethod
    def get_end_this_month(cls):
        return datetime.date.today().replace(day=1) + relativedelta(months=1) - relativedelta(days=1)

    @classmethod
    def get_start_prev_month(cls):
        return datetime.date.today().replace(day=1) - relativedelta(months=1)

    @classmethod
    def get_start_next_month(cls):
        return datetime.date.today().replace(day=1) + relativedelta(months=1)

    @classmethod
    def get_end_prev_month(cls):
        return datetime.date.today().replace(day=1) - relativedelta(days=1)

    @classmethod
    def get_end_next_month(cls):
        return (datetime.date.today() + relativedelta(months=1)).replace(day=1) + relativedelta(months=1) \
               - relativedelta(days=1)

    @classmethod
    def get_start_current_quarter(cls):
        return cls.__set_quarter_start(datetime.date.today())

    @classmethod
    def get_end_current_quarter(cls):
        return cls.__set_quarter_end(datetime.date.today())

    @classmethod
    def get_start_prev_quarter(cls):
        return cls.__set_quarter_start(datetime.date.today()) - relativedelta(months=3)

    @classmethod
    def get_end_prev_quarter(cls):
        return cls.__set_quarter_end(datetime.date.today()) - relativedelta(months=3)

    @classmethod
    def get_start_next_quarter(cls):
        return cls.__set_quarter_start(datetime.date.today()) + relativedelta(months=3)

    @classmethod
    def get_end_next_quarter(cls):
        return cls.__set_quarter_end(datetime.date.today()) + relativedelta(months=3)

    @classmethod
    def __set_quarter_start(cls, date: datetime.date):
        return date.replace(day=1) - relativedelta(months=(date.month - 1) % 3)

    @classmethod
    def __set_quarter_end(cls, date):
        return date.replace(day=1) + relativedelta(months=3 - ((date.month - 1) % 3)) - relativedelta(days=1)

    @classmethod
    def get_today(cls):
        return datetime.date.today()

    @classmethod
    def get_one_month_ago(cls):
        return datetime.date.today() - relativedelta(months=1)

    @classmethod
    def get_three_months_ago(cls):
        return datetime.date.today() - relativedelta(months=3)

    @classmethod
    def get_six_months_ago(cls):
        return datetime.date.today() - relativedelta(months=6)

    @classmethod
    def get_one_year_ago(cls):
        return datetime.date.today() - relativedelta(years=1)

    @classmethod
    def get_one_month_ahead(cls):
        return datetime.date.today() + relativedelta(months=1)

    @classmethod
    def get_three_months_ahead(cls):
        return datetime.date.today() + relativedelta(months=3)

    @classmethod
    def get_six_months_ahead(cls):
        return datetime.date.today() + relativedelta(months=6)

    @classmethod
    def get_one_year_ahead(cls):
        return datetime.date.today() + relativedelta(years=1)

    @classmethod
    def initialize(cls):
        relative_date_values = [
            ('start-cal-year', 'Start of this year', 'First day of the current calendar year.', cls.get_start_cal_year),
            ('end-cal-year', 'End of this year', 'Last day of the current calendar year.', cls.get_end_cal_year), (
                'start-prev-year', 'Start of previous year', 'First day of the previous calendar year.',
                cls.get_start_prev_year),
            ('start-next-year', 'Start of next year', 'First day of the next calendar year.', cls.get_start_next_year),
            ('end-prev-year', 'End of previous year', 'Last day of the previous calendar year.', cls.get_end_prev_year),
            ('end-next-year', 'End of next year', 'Last day of the next calendar year.', cls.get_end_next_year), (
                'start-accounting-period', 'Start of accounting period',
                'First day of the accounting period, as set in the global preferences.',
                cls.get_start_accounting_period),
            ('end-accounting-period', 'End of accounting period',
             'Last day of the accounting period, as set in the global preferences.', cls.get_end_accounting_period),
            ('start-this-month', 'Start of this month', 'First day of the current month.', cls.get_start_this_month),
            ('end-this-month', 'End of this month', 'Last day of the current month.', cls.get_end_this_month),
            ('start-prev-month', 'Start of previous month', 'First day of the previous month.',
             cls.get_start_prev_month),
            ('end-prev-month', 'End of previous month', 'Last day of previous month.', cls.get_end_prev_month),
            ('start-next-month', 'Start of next month', 'First day of the next month.', cls.get_start_next_month),
            ('end-next-month', 'End of next month', 'Last day of next month.', cls.get_end_next_month), (
                'start-current-quarter', 'Start of current quarter',
                'First day of the current quarterly accounting period.', cls.get_start_current_quarter), (
                'end-current-quarter', 'End of current quarter', 'Last day of the current quarterly accounting period.',
                cls.get_end_current_quarter),
            (
                'start-prev-quarter', 'Start of previous quarter',
                'First day of the previous quarterly accounting period.',
                cls.get_start_prev_quarter),
            ('end-prev-quarter', 'End of previous quarter', 'Last day of previous quarterly accounting period.',
             cls.get_end_prev_quarter),
            ('start-next-quarter', 'Start of next quarter', 'First day of the next quarterly accounting period.',
             cls.get_start_next_quarter),
            ('end-next-quarter', 'End of next quarter', 'Last day of next quarterly accounting period.',
             cls.get_end_next_quarter),
            ('today', 'Today', 'The current date.', cls.get_today),
            ('one-month-ago', 'One Month Ago', 'One Month Ago.', cls.get_one_month_ago),
            ('three-months-ago', 'Three Months Ago', 'Three Months Ago.', cls.get_three_months_ago),
            ('six-months-ago', 'Six Months Ago', 'Six Months Ago.', cls.get_six_months_ago),
            ('one-year-ago', 'One Year Ago', 'One Year Ago.', cls.get_one_year_ago),
            ('one-month-ahead', 'One Month Ahead', 'One Month Ahead.', cls.get_one_month_ahead),
            ('three-months-ahead', 'Three Months Ahead', 'Three Months Ahead.', cls.get_three_months_ahead),
            ('six-months-ahead', 'Six Months Ahead', 'Six Months Ahead.', cls.get_six_months_ahead),
            ('one-year-ahead', 'One Year Ahead', 'One Year Ahead.', cls.get_one_year_ahead)]

        for a in relative_date_values:
            cls.rel_hash[a[0]] = a[1:]


RelativeDate.initialize()
