import logging
from abc import ABCMeta, abstractmethod

from gi.repository import GLib, Gio

CLIENT_TAG = "%s-%s-client"
NOTIFY_TAG = "%s-%s-notify_id"
PREF_MIGRATE_PREFS_DONE = "migrate-prefs-done"
defaultEntityLoader = None
GSET_SCHEMA_PREFIX = "org.jonah"
reg_auto_raise_lists_id = 0
reg_negative_color_pref_id = 0


class PreferencesBackEnd(metaclass=ABCMeta):
    @staticmethod
    @abstractmethod
    def register_cb(cls, group, pref_name, func, user_data):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def remove_cb_by_func(cls, group, pref_name, func, user_data):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def remove_cb_by_id(group, id):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def register_group_cb(group, func, user_data):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def remove_group_cb_by_func(group, func, user_data):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def bind(cls, group, pref_name, object, property):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def get_bool(cls, group, pref_name):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def get_int(cls, group, pref_name):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def get_float(cls, group, pref_name):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def get_string(cls, group, pref_name):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def get_enum(cls, group, pref_name):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def get_value(cls, group, pref_name):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def set_bool(cls, group, pref_name, value):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def set_int(cls, group, pref_name, value):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def set_float(cls, group, pref_name, value):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def set_string(cls, group, pref_name, value):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def set_enum(cls, group, pref_name, value):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def set_value(cls, group, pref_name, value):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def reset(cls, group, pref_name):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def reset_group(group):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def block_all():
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def unblock_all():
        raise NotImplementedError


class Settings(PreferencesBackEnd):
    prefix = None
    schema_hash = {}
    registered_handlers_hash = {}
    function_hash = {}

    @classmethod
    def register_cb(cls, group, pref_name, func, *user_data):
        signal = "changed"
        settings_ptr = cls.get_settings_ptr(group)
        if func is None or settings_ptr is None:
            return
        if pref_name is not None or pref_name == "":
            if cls.is_valid_key(settings_ptr, pref_name):
                signal = "changed::" + pref_name
        signal_id = settings_ptr.connect(signal, func, *user_data)
        if signal_id != 0:
            cls.registered_handlers_hash[signal_id] = settings_ptr
            cls.function_hash[signal_id] = func
        return signal_id

    @classmethod
    def remove_cb_by_func(cls, group, pref_name, func, *user_data):
        handler_id = 0
        if func is None:
            return
        for k, v in cls.function_hash.items():
            if v == func:
                handler_id = k
                break
        while handler_id:
            cls.remove_cb_by_id(group, handler_id)
            handler_id = 0
            for k, v in cls.function_hash.items():
                if id(v) == id(func):
                    handler_id = k
                    break

    @classmethod
    def remove_cb_by_id(cls, group, d):
        try:
            cls.registered_handlers_hash.pop(d)
            cls.function_hash.pop(d)
        except KeyError:
            return
        settings_ptr = cls.get_settings_ptr(group)
        settings_ptr.disconnect(d)

    @classmethod
    def register_group_cb(cls, group, func, *user_data):
        cls.register_cb(group, None, func, *user_data)

    @classmethod
    def remove_group_cb_by_func(cls, group, func, *user_data):
        cls.remove_cb_by_func(group, None, func, *user_data)

    @classmethod
    def bind(cls, group, pref_name, obj, prop):
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return
        if cls.is_valid_key(settings_ptr, pref_name):
            settings_ptr.bind(pref_name, obj, prop, 0)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)

    @classmethod
    def get_bool(cls, group, pref_name):
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return
        if cls.is_valid_key(settings_ptr, pref_name):
            return settings_ptr.get_boolean(pref_name)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
            return False

    @classmethod
    def get_int(cls, group, pref_name):
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return
        if cls.is_valid_key(settings_ptr, pref_name):
            return settings_ptr.get_int(pref_name)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
            return False

    @classmethod
    def get_float(cls, group, pref_name):
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return
        if cls.is_valid_key(settings_ptr, pref_name):
            return settings_ptr.get_double(pref_name)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
            return False

    @classmethod
    def get_string(cls, group, pref_name):
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return
        if cls.is_valid_key(settings_ptr, pref_name):
            return settings_ptr.get_string(pref_name)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
            return False

    @classmethod
    def get_enum(cls, group, pref_name):
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return
        if cls.is_valid_key(settings_ptr, pref_name):
            return settings_ptr.get_enum(pref_name)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
            return False

    @classmethod
    def get_value(cls, group, pref_name):
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return
        if cls.is_valid_key(settings_ptr, pref_name):
            return settings_ptr.get_value(pref_name)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
            return False

    @classmethod
    def set_bool(cls, group, pref_name, value):
        result = False
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return False
        if cls.is_valid_key(settings_ptr, pref_name):
            result = settings_ptr.set_boolean(pref_name, value)
            if not result:
                logging.error("Unable to set value for key %s in schema %s", pref_name, group)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
        return result

    @classmethod
    def set_int(cls, group, pref_name, value):
        result = False
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return False
        if cls.is_valid_key(settings_ptr, pref_name):
            result = settings_ptr.set_int(pref_name, value)
            if not result:
                logging.error("Unable to set value for key %s in schema %s", pref_name, group)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
        return result

    @classmethod
    def set_float(cls, group, pref_name, value):
        result = False
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return False
        if cls.is_valid_key(settings_ptr, pref_name):
            result = settings_ptr.set_double(pref_name, value)
            if not result:
                logging.error("Unable to set value for key %s in schema %s", pref_name, group)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
        return result

    @classmethod
    def set_string(cls, group, pref_name, value):
        result = False
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return False
        if cls.is_valid_key(settings_ptr, pref_name):
            result = settings_ptr.set_string(pref_name, value)
            if not result:
                logging.error("Unable to set value for key %s in schema %s", pref_name, group)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
        return result

    @classmethod
    def set_enum(cls, group, pref_name, value):
        result = False
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return False
        if cls.is_valid_key(settings_ptr, pref_name):
            result = settings_ptr.set_enum(pref_name, value)
            if not result:
                logging.error("Unable to set value for key %s in schema %s", pref_name, group)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
        return result

    @classmethod
    def set_value(cls, group, pref_name, value):
        result = False
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return False
        if cls.is_valid_key(settings_ptr, pref_name):
            result = settings_ptr.set_value(pref_name, value)
            if not result:
                logging.error("Unable to set value for key %s in schema %s", pref_name, group)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)
        return result

    @classmethod
    def reset(cls, group, pref_name):
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return None
        if cls.is_valid_key(settings_ptr, pref_name):
            settings_ptr.reset(pref_name)
        else:
            logging.error("Invalid key %s for schema %s", pref_name, group)

    @classmethod
    def reset_group(cls, group):
        settings_ptr = cls.get_settings_ptr(group)
        if settings_ptr is None:
            return None
        schema = settings_ptr.get_property("settings-schema")
        if schema is None:
            return
        keys = schema.list_keys()
        if keys is None or not keys:
            return
        for k in keys:
            cls.reset(group, k)

    @classmethod
    def block_all(cls):
        for k, v in cls.registered_handlers_hash.items():
            cls.handlers_hash_block_helper(k, v)

    @classmethod
    def unblock_all(cls):
        for k, v in cls.registered_handlers_hash.items():
            cls.handlers_hash_unblock_helper(k, v)

    @classmethod
    def is_valid_key(cls, settings, key):
        if settings is None:
            return None
        schema = settings.get_property("settings-schema")
        if schema is None:
            return
        keys = schema.list_keys()
        if keys is None or not keys:
            return
        for k in keys:
            if k == key:
                return True
        return False

    @classmethod
    def get_settings_ptr(cls, schema_str):
        full_name = cls.normalize_schema_name(schema_str)
        settings = cls.schema_hash.get(full_name)
        if settings is None:
            try:
                settings = Gio.Settings(schema=full_name)
                cls.schema_hash[full_name] = settings
            except Gio.IOErrorEnum as e:
                logging.error("Schema {} does not exist ".format(full_name))
        return settings

    @classmethod
    def handlers_hash_block_helper(cls, key, settings_ptr):
        settings_ptr.handler_block(key)

    @classmethod
    def handlers_hash_unblock_helper(cls, key, settings_ptr):
        settings_ptr.handler_unblock(key)

    @classmethod
    def set_prefix(cls, prefix):
        cls.prefix = prefix

    @classmethod
    def get_prefix(cls):
        if cls.prefix is None:
            prefix = GLib.getenv("GSETTINPREFIX")
            if prefix:
                cls.prefix = prefix
            else:
                cls.prefix = GSET_SCHEMA_PREFIX

        return cls.prefix

    @classmethod
    def normalize_schema_name(cls, name):
        if name is None or name.strip() == "":
            return cls.get_prefix()
        if name.startswith(cls.get_prefix()):
            return name
        return ".".join([cls.get_prefix(), name])


namespace_regexp = None
is_debugging = False
extras_enabled = True
use_compression = True
file_retention_policy = 1
file_retention_days = 30
prefsbackend = Settings()
PREFS_GROUP_GENERAL = "general"
PREFS_GROUP_GENERAL_REGISTER = "general.register"
PREFS_GROUP_GENERAL_REPORT = "general.report"
PREFS_GROUP_WARNINGS = "warnings"
PREFS_GROUP_WARNINTEMP = "warnings.temporary"
PREFS_GROUP_WARNINPERM = "warnings.permanent"
PREFS_GROUP_ACCT_SUMMARY = "window.pages.account-tree.summary"
PREF_VERSION = "prefs-version"
PREF_SAVE_GEOMETRY = "save-window-geometry"
PREF_LAST_PATH = "last-path"
PREF_USE_NEW = "use-new-window"
PREF_ACCOUNTING_LABELS = "use-accounting-labels"
PREF_ACCOUNT_SEPARATOR = "account-separator"
PREF_NEGATIVE_IN_RED = "negative-in-red"
PREF_NUM_SOURCE = "num-source"
PREF_DATE_FORMAT = "date-format"
PREF_DATE_COMPL_THISYEAR = "date-completion-thisyear"
PREF_DATE_COMPL_SLIDING = "date-completion-sliding"
PREF_DATE_BACKMONTHS = "date-backmonths"
PREF_SHOW_LEAF_ACCT_NAMES = "show-leaf-account-names"
PREF_ENTER_MOVES_TO_END = "enter-moves-to-end"

PREF_DRAW_HOR_LINES = "draw-horizontal-lines"
PREF_DRAW_VERT_LINES = "draw-vertical-lines"
PREF_ALT_COLOR_BY_TRANS = "alternate-color-by-transaction"
PREF_USE_THEME_COLORS = "use-theme-colors"
PREF_USE_GASSTATION_COLOR_THEME = "use-gasstation-color-theme"
PREF_TAB_TRANS_MEMORISED = "tab-to-transfer-on-memorised"
PREF_FUTURE_AFTER_BLANK = "future-after-blank-transaction"
PREF_START_CHOICE_ABS = "start-choice-absolute"
PREF_START_CHOICE_REL = "start-choice-relative"
PREF_START_DATE = "start-date"
PREF_START_PERIOD = "start-period"
PREF_END_CHOICE_ABS = "end-choice-absolute"
PREF_END_CHOICE_REL = "end-choice-relative"
PREF_END_DATE = "end-date"
PREF_END_PERIOD = "end-period"
PREF_CURRENCY_OTHER = "currency-other"
PREF_CURRENCY_CHOICE_LOCALE = "currency-choice-locale"
PREF_CURRENCY_CHOICE_OTHER = "currency-choice-other"


def gs_pref_get_namespace_regexp():
    return namespace_regexp


def gs_pref_set_namespace_regexp(st):
    if st is not None and st != "":
        global namespace_regexp
        namespace_regexp = st


def gs_pref_is_debugging_enabled(): return is_debugging


def gs_pref_set_debugging(d):
    global is_debugging
    is_debugging = d


def gs_pref_is_extra_enabled():
    global extras_enabled
    return extras_enabled


def gs_pref_set_extra(enabled):
    global extras_enabled
    extras_enabled = enabled


def gs_pref_get_file_save_compressed():
    return use_compression


def gs_pref_set_file_save_compressed(compressed):
    global use_compression
    use_compression = compressed


def gs_pref_get_file_retention_policy():
    return file_retention_policy


def gs_pref_set_file_retention_policy(policy):
    global file_retention_policy
    file_retention_policy = policy


def gs_pref_get_file_retention_days():
    return file_retention_days


def gs_pref_set_file_retention_days(days):
    global file_retention_days
    file_retention_days = days


def gs_pref_get_long_version():
    return 0.1
    # return GNUCASH_MAJOR_VERSION * 1000000 + GNUCASH_MINOR_VERSION


def gs_pref_register_cb(group, pref_name, func, *args):
    if prefsbackend is not None:
        return prefsbackend.register_cb(group, pref_name, func, *args)
    else:
        logging.error("no backend loaded, or the backend doesn't define register_cb, returning 0")
        return 0


def gs_pref_remove_cb_by_func(group, pref_name, func, *args):
    if prefsbackend is not None:
        prefsbackend.remove_cb_by_func(group, pref_name, func, *args)


def gs_pref_remove_cb_by_id(group, id):
    if prefsbackend is not None:
        prefsbackend.remove_cb_by_id(group, id)


def gs_pref_register_group_cb(group, func, *args):
    if prefsbackend is not None:
        return prefsbackend.register_group_cb(group, func, *args)
    else:
        return 0


def gs_pref_remove_group_cb_by_func(group, func, *args):
    if prefsbackend is not None:
        prefsbackend.remove_group_cb_by_func(group, func, *args)


def gs_pref_bind(group, pref_name, object, property):
    if prefsbackend is not None:
        prefsbackend.bind(group, pref_name, object, property)


def gs_pref_get_bool(group, pref_name):
    if prefsbackend is not None:
        return prefsbackend.get_bool(group, pref_name)
    else:
        return False


def gs_pref_get_int(group, pref_name):
    if prefsbackend is not None:
        return prefsbackend.get_int(group, pref_name)
    else:
        return 0


def gs_pref_get_int64(group, pref_name):
    var = gs_pref_get_value(group, pref_name)
    result = var.get_int64()
    return result


def gs_pref_get_float(group, pref_name):
    if prefsbackend is not None:
        return prefsbackend.get_float(group, pref_name)
    else:
        return 0.0


def gs_pref_get_string(group, pref_name):
    if prefsbackend is not None:
        return prefsbackend.get_string(group, pref_name)
    else:
        return None


def gs_pref_get_enum(group, pref_name):
    if prefsbackend is not None:
        return prefsbackend.get_enum(group, pref_name)
    else:
        return 0


def gs_pref_get_coords(group, pref_name):
    coords = gs_pref_get_value(group, pref_name)
    x, y = coords.get("(dd)")
    return x, y


def gs_pref_get_value(group, pref_name):
    if prefsbackend is not None:
        return prefsbackend.get_value(group, pref_name)
    else:
        return None


def gs_pref_set_bool(group, pref_name, value):
    if prefsbackend is not None:
        return prefsbackend.set_bool(group, pref_name, value)
    else:
        return False


def gs_pref_set_int(group, pref_name, value):
    if prefsbackend is not None:
        return prefsbackend.set_int(group, pref_name, value)
    else:
        return False


def gs_pref_set_int64(group, pref_name, value):
    var = GLib.Variant.new("x", value)
    return gs_pref_set_value(group, pref_name, var)


def gs_pref_set_float(group, pref_name, value):
    if prefsbackend is not None:
        return prefsbackend.set_float(group, pref_name, value)
    else:
        return False


def gs_pref_set_string(group, pref_name, value):
    if prefsbackend is not None:
        return prefsbackend.set_string(group, pref_name, value)
    else:
        return False


def gs_pref_set_enum(group, pref_name, value):
    if prefsbackend is not None:
        return prefsbackend.set_enum(group, pref_name, value)
    else:
        return False


def gs_pref_set_coords(group, pref_name, x, y):
    var = GLib.Variant.new("(dd)", x, y)
    return gs_pref_set_value(group, pref_name, var)


def gs_pref_set_value(group, pref_name, value):
    if prefsbackend is not None:
        return prefsbackend.set_value(group, pref_name, value)
    else:
        return False


def gs_pref_reset(group, pref_name):
    if prefsbackend is not None:
        prefsbackend.reset(group, pref_name)


def gs_pref_reset_group(group):
    if prefsbackend is not None:
        prefsbackend.reset_group(group)


def gs_pref_is_set_up():
    return prefsbackend is not None


def gs_pref_block_all():
    if prefsbackend is not None:
        prefsbackend.block_all()


def gs_pref_unblock_all():
    if prefsbackend is not None:
        prefsbackend.unblock_all()


def gs_pref_get_reg_auto_raise_lists_id():
    return reg_auto_raise_lists_id


def gs_pref_set_reg_auto_raise_lists_id(i):
    global reg_auto_raise_lists_id
    reg_auto_raise_lists_id = i


def gs_pref_get_reg_negative_color_pref_id():
    return reg_negative_color_pref_id


def gs_pref_set_reg_negative_color_pref_id(i):
    global reg_negative_color_pref_id
    reg_negative_color_pref_id = i
