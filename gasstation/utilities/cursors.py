from gi.repository import Gdk, Gtk


class CursorType:
    NORMAL = -1
    BUSY = Gdk.CursorType.WATCH


def gs_set_cursor(win, _type, update_now):
    cursor = None
    if win is None:
        return

    if _type != CursorType.NORMAL:
        cursor = Gdk.Cursor.new_for_display(Gdk.Display.get_default(), _type)
    win.set_cursor(cursor)

    if update_now and _type != CursorType.NORMAL:
        while Gtk.events_pending():
            Gtk.main_iteration()


def gs_set_busy_cursor(w, update_now):
    if w is not None:
        gs_set_cursor(w.get_window(), CursorType.BUSY, update_now)
    else:
        for w in Gtk.Window.list_toplevels():
            if not w or not w.get_realized() or not w.get_has_window():
                continue
            gs_set_cursor(w.get_window(), CursorType.BUSY, update_now)


def gs_unset_busy_cursor(w):
    if w is not None:
        gs_set_cursor(w.get_window(), CursorType.NORMAL, False)
    else:
        for w in Gtk.Window.list_toplevels():
            if not w or not w.get_has_window():
                continue
            gs_set_cursor(w.get_window(), CursorType.NORMAL, False)
