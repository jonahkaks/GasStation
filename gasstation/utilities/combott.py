from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject, GLib, Gdk
from .dialog import gs_widget_set_style_context, gs_draw_arrow_cb, gs_label_set_alignment


class ComboTT(Gtk.Box):
    __gtype_name__ = "ComboTT"
    __gsignals__ = {
        'changed': (GObject.SignalFlags.RUN_FIRST, None, ()), }
    __gproperties__ = {'model': (Gtk.TreeModel,
                                 "Combott model",
                                 "The model for the combo tooltip",
                                 GObject.ParamFlags.READWRITE),
                       'text-col': (GObject.TYPE_INT,
                                    "text column",
                                    "Column for the text",
                                    0,
                                    GLib.MAXINT,
                                    0,
                                    GObject.ParamFlags.READWRITE),
                       'tip-col': (GObject.TYPE_INT,
                                   "tip column",
                                   "Column for the tip",
                                   0,
                                   GLib.MAXINT,
                                   0,
                                   GObject.ParamFlags.READWRITE)}

    def __init__(self, **kwargs):
        super().__init__(self, **kwargs)
        self.model = None
        self.menu = None
        self.active_iter = None
        self.active = 0
        self.text_col = 0
        self.tip_col = 0
        self.max_number_char = 0
        self.num_items = 0
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        gs_widget_set_style_context(self, "Combott")
        self.text_col = 0
        self.tip_col = 1
        hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        hbox.set_homogeneous(False)
        arrow = Gtk.Image.new_from_icon_name("pan-down-symbolic", Gtk.IconSize.BUTTON)
        arrow.connect("draw", gs_draw_arrow_cb, 1)
        arrow.set_margin_start(5)
        hbox.pack_end(arrow, False, False, 0)
        sep = Gtk.Separator.new(Gtk.Orientation.VERTICAL)
        hbox.pack_end(sep, False, False, 0)
        label = Gtk.Label.new()
        hbox.pack_start(label, False, False, 0)
        self.label = label
        button = Gtk.Button.new()
        button.add(hbox)
        self.button = button
        self.add(button)
        button.connect("event", self.button_press_cb)
        self.set_has_tooltip(True)
        self.connect("query-tooltip", self.which_tooltip_cb)
        self.connect("size-allocate", self.button_getsize_cb)
        self.button.show()

    def do_get_property(self, prop):
        if prop.name == 'model':
            return self.model
        elif prop.name == "active":
            self.get_active()
        elif prop.name == "text-col":
            return self.text_col
        elif prop.name == "tip-col":
            return self.tip_col
        else:
            raise AttributeError('unknown property %s' % prop.name)

    def do_set_property(self, prop, value):
        if __name__ == '__main__':
            if prop.name == 'model':
                self.set_model(value)
            elif prop.name == "active":
                self.set_active(value)
            elif prop.name == "text-col":
                self.text_col = value
            elif prop.name == "tip-col":
                self.tip_col = value
                self.refresh_menu(self.model)

    def set_model(self, model):
        if model is None or not isinstance(model, Gtk.TreeModel):
            return
        self.rebuild_menu(model)
        self.model = model

    def rebuild_menu(self, model):
        if model is None or not isinstance(model, Gtk.TreeModel):
            return
        num = 1
        items = 0
        self.menu = None
        self.menu = Gtk.Menu.new()
        _iter = model.get_iter_first()
        while _iter is not None:
            str_data = model.get_value(_iter, self.text_col)
            tip_data = model.get_value(_iter, self.tip_col)
            menu_items = Gtk.MenuItem.new_with_label(str_data)
            if len(str_data) > num:
                num = len(str_data)
            label = menu_items.get_child()
            label.set_tooltip_text(tip_data)
            gs_label_set_alignment(label, 0, 0.5)
            self.menu.append(menu_items)
            menu_items.connect("activate", self.menuitem_response_cb)
            menu_items.show()
            items += 1
            _iter = model.iter_next(_iter)

        self.menu.connect("size-allocate", self.menu_getsize_cb)
        self.max_number_char = num
        self.label.set_width_chars(self.max_number_char)
        self.num_items = items

    def refresh_menu(self, model):
        if model is None or not isinstance(model, Gtk.TreeModel):
            return
        self.rebuild_menu(model)

    def changed(self):
        pass

    def button_getsize_cb(self, widget, allocation):
        self.width = allocation.width
        self.height = allocation.height
        self.x = allocation.x
        self.y = allocation.y

    def menu_getsize_cb(self, widget, allocation):
        widget.set_size_request(self.width - 6, allocation.height)

    def which_tooltip_cb(self, widget, x, y, keyboard_mode, tooltip):
        if self.active != 0:
            text = self.model.get_value(self.active_iter, self.tip_col)
            if text != "":
                tooltip.set_text(text)
                return True
            else:
                return False
        return False

    def button_press_cb(self, widget, event):
        if self.model is not None:
            if event.type == Gdk.EventType.BUTTON_PRESS:
                self.menu.popup_at_widget(widget,
                                          Gdk.Gravity.SOUTH_WEST,
                                          Gdk.Gravity.NORTH_WEST,
                                          event)
            return True
        return False

    def menuitem_response_cb(self, item):
        active = 1
        active_now = 1
        iter_now = Gtk.TreeIter()
        iter_now.user_data = 0
        label_text = item.get_label()
        self.label.set_text(label_text)
        gs_label_set_alignment(self.label, 0, 0.5)
        _iter = self.model.get_iter_first()
        while _iter is not None:
            str_data = self.model.get_value(_iter, self.text_col)
            if str_data == label_text:
                active_now = active
                iter_now = _iter
            active += 1
            _iter = self.model.iter_next(_iter)

        if self.active != active_now:
            self.active = active_now
            self.active_iter = iter_now
            self.emit("changed")

    @classmethod
    def new(cls):
        return cls()

    def get_active(self):
        return self.active - 1

    def set_active(self, index):
        active = 1
        if index <= -1:
            return
        if self.model is not None:
            if index + 1 != self.active:
                if index == -1:
                    self.active = 0
                    self.label.set_text("")
                    self.emit("changed")
                else:
                    _iter = self.model.get_iter_first()
                    while _iter is not None:
                        str_data = self.model.get_value(_iter, self.text_col)
                        if index + 1 == active:
                            self.active = index + 1
                            self.active_iter = _iter
                            self.label.set_text(str_data)
                            gs_label_set_alignment(self.label, 0, 0.5)
                            self.emit("changed")
                        active += 1
                        _iter = self.model.iter_next(_iter)


GObject.type_register(ComboTT)
