from gi.repository import GLib

from .gtk import ensure_dir


class KeyFile:
    @staticmethod
    def save_to_file(filename, key_file):
        success = True
        if filename is None or key_file is None:
            return False
        if not ensure_dir(filename):
            return False
        try:
            key_file.save_to_file(filename)
        except GLib.GError as e:
            pass
        return success

    @staticmethod
    def load_from_file(file_name, ignore_error=True, return_empty=False):
        if file_name is None:
            return None
        if not GLib.file_test(file_name, GLib.FileTest.EXISTS):
            return None
        key_file = GLib.KeyFile.new()
        if key_file is None:
            return None
        try:
            if key_file.load_from_file(file_name, GLib.KeyFileFlags.NONE):
                return key_file
        except GLib.GError as e:
            if not ignore_error:
                return e
            elif not return_empty:
                return GLib.KeyFile.new()
            if ignore_error and return_empty:
                return None
        return key_file
