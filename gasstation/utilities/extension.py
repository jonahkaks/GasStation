from gi.repository import Gtk, GLib


class ExtensionInfo:
    def __init__(self):
        self.extension = None
        self.ae = None
        self.path = None
        self.sort_key = None
        self.typeStr = None
        self.type = None
        self.accel_assigned = False


class Extension:
    def __init__(self, type, name, guid, document_string, path, script, *args):
        self.type = type
        self.name = name
        self.guid = guid
        self.document_string = document_string
        self.path = path
        self.script = script
        self.script_data = args


class Extentions:
    extension_list = []

    @classmethod
    def get_menu_list(cls):
        return cls.extension_list

    @staticmethod
    def make_menu_item(name, guid, documentation_string, path, script, *args):
        return Extension("menu-item", name, guid, documentation_string, path, script, *args)

    @staticmethod
    def make_menu(name, path):
        return Extension("menu", name, name, "", path, None)

    @staticmethod
    def make_separator(path):
        return Extension("separator", "", "", "", path, None)

    @staticmethod
    def gen_action_name(name):
        n = ""
        for s in name:
            if not s.isalnum():
                n += '_'
            else:
                n += s
        return n

    @staticmethod
    def get_info_path(extension):
        path = extension.path
        if path is None or len(path) == 0:
            return ""
        path.insert(0, "/menubar")
        return "/".join(path)

    @classmethod
    def add_extention(cls, extension: Extension):
        if extension is None:
            return
        ext_info = ExtensionInfo()
        ext_info.extension = extension
        ext_info.path = cls.get_info_path(extension)
        if extension.type == "menu-item":
            ext_info.type = Gtk.UIManagerItemType.MENUITEM
        elif extension.type == "menu":
            ext_info.type = Gtk.UIManagerItemType.MENU
        elif extension.type == "separator":
            ext_info.type = Gtk.UIManagerItemType.SEPARATOR
        else:
            return
        name = extension.name
        guid = extension.guid
        ext_info.ae = Gtk.Action(cls.gen_action_name(guid), name, extension.document_string)
        ext_info.sort_key = GLib.utf8_collate_key("/".join([ext_info.path, ext_info.ae.get_label()]), -1)
        if ext_info.type == Gtk.UIManagerItemType.MENU:
            type_str = "menu"
        elif ext_info.type == Gtk.UIManagerItemType.MENUITEM:
            type_str = "menuitem"
        else:
            type_str = "unk"

        ext_info.typeStr = type_str
        cls.extension_list.append(ext_info)
        return True

    @staticmethod
    def invoke_cb(extension, window):
        extension.script(window, *extension.script_data)
