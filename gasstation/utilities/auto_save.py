from gasstation.views.dialogs.setup import File, Session
from .dialog import gs_widget_set_style_context, Gtk
from .preferences import *

PREF_AUTOSAVE_SHOW_EXPLANATION = "autosave-show-explanation"
PREF_AUTOSAVE_INTERVAL = "autosave-interval-minutes"
AUTOSAVE_SOURCE_ID = "autosave_source_id"
import logging


class AutoSave:
    @staticmethod
    def confirm(toplevel):
        logging.info("#############CONFIRMING AUTO SAVE ############")
        interval_mins = gs_pref_get_float(PREFS_GROUP_GENERAL, PREF_AUTOSAVE_INTERVAL)
        YES_THIS_TIME = 1
        YES_ALWAYS = 2
        NO_NEVER = 3
        NO_NOT_THIS_TIME = 4

        dialog = Gtk.MessageDialog(toplevel, modal=True, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.NONE, message_format="Save file automatically?")

        gs_widget_set_style_context(dialog, "AutoSaveDialog")
        dialog.format_secondary_text("Your data file needs to be saved to your hard disk to save your changes. "
                                     "GasStation has a feature to save the file automatically every %d minute, "
                                     "just as if you had pressed the \"Save\" button each time. \n\n "
                                     "You can change the time interval or turn off this feature under "
                                     "Edit -> Preferences -> General -> Auto-save time interval. \n\n"
                                     "Should your file be saved automatically?" % interval_mins)
        dialog.add_buttons("_Yes, this time", YES_THIS_TIME, "Yes, _always", YES_ALWAYS, "No, n_ever", NO_NEVER,
                           "_No, not this time", NO_NOT_THIS_TIME)
        dialog.set_default_response(NO_NOT_THIS_TIME)
        response = dialog.run()
        dialog.destroy()
        if response == YES_THIS_TIME:
            switch_off_autosave = False
            show_expl_again = True
            save_now = True

        elif response == YES_ALWAYS:
            switch_off_autosave = False
            show_expl_again = False
            save_now = True
        elif response == NO_NEVER:
            switch_off_autosave = True
            show_expl_again = False
            save_now = False
        else:
            switch_off_autosave = False
            show_expl_again = True
            save_now = False
        gs_pref_set_bool(PREFS_GROUP_GENERAL, PREF_AUTOSAVE_SHOW_EXPLANATION, show_expl_again)
        if switch_off_autosave:
            gs_pref_set_float(PREFS_GROUP_GENERAL, PREF_AUTOSAVE_INTERVAL, 0)
        return save_now

    @staticmethod
    def timeout_cb(book):
        from gasstation.views.main_window import MainWindow, Window
        save_now = True
        if File.save_in_progress() or not Session.current_session_exist() or book.is_readonly():
            return False
        toplevel = MainWindow.get_main_window(None)
        show_explanation = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_AUTOSAVE_SHOW_EXPLANATION)
        if show_explanation:
            save_now = AutoSave.confirm(toplevel)
        if save_now:
            if isinstance(toplevel, MainWindow):
                MainWindow.set_progressbar_window(toplevel)
            else:
                if isinstance(toplevel, Window):
                    Window.set_progressbar_window(toplevel)
            File.save(toplevel)
            MainWindow.set_progressbar_window(None)
            return False
        else:
            return True

    @staticmethod
    def remove_timer_cb(book, key, autosave_source_id):
        if book is None: return
        if autosave_source_id is None or not autosave_source_id:
            return
        if autosave_source_id > 0:
            GLib.source_remove(autosave_source_id)
            book.set_data_fin(AUTOSAVE_SOURCE_ID, 0, AutoSave.remove_timer_cb)

    @classmethod
    def remove_timer(cls, book):
        if book is None: return
        cls.remove_timer_cb(book, AUTOSAVE_SOURCE_ID, book.get_data(AUTOSAVE_SOURCE_ID) or 0)

    @classmethod
    def add_timer(cls, book):
        if book is None: return
        interval_mins = gs_pref_get_float(PREFS_GROUP_GENERAL, PREF_AUTOSAVE_INTERVAL)
        if interval_mins > 0 and not File.save_in_progress() and Session.current_session_exist():
            autosave_source_id = GLib.timeout_add_seconds(interval_mins * 60, cls.timeout_cb, book)
            book.set_data_fin(AUTOSAVE_SOURCE_ID, autosave_source_id, cls.remove_timer_cb)

    @classmethod
    def dirty_handler(cls, book, dirty):
        if dirty:
            if book.is_readonly():
                return
            if not book.is_shutting_down():
                cls.remove_timer(book)
                cls.add_timer(book)
        else:
            cls.remove_timer(book)
