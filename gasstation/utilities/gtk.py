import os
from collections import defaultdict

from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

LAST_INDEX = "last_index"
CHANGED_ID = "changed_id"


def ensure_dir(path):
    d = os.path.dirname(path)
    if not os.path.exists(d):
        try:
            os.makedirs(d)
        except PermissionError:
            return False
    return True


class Combo:
    data = defaultdict(dict)

    @classmethod
    def set_by_string(cls, cbwe, text):
        if text is None or len(text) == 0: return

        model = cbwe.get_model()
        _iter = model.get_iter_first()
        if _iter is None:
            cbwe.set_active(-1)
            return
        column = cbwe.get_entry_text_column()
        while _iter is not None:
            tree_string = model.get_value(_iter, column)
            if GLib.utf8_collate(text, tree_string) == 0:
                cid = cls.data[cbwe][CHANGED_ID]
                cbwe.handler_block(cid)
                cbwe.set_active_iter(_iter)
                cbwe.handler_unblock(cid)
                index = cbwe.get_active()
                cls.data[cbwe][LAST_INDEX] = index
                return
            _iter = model.iter_next(_iter)

    @classmethod
    def changed_cb(cls, widget, cbwe):
        index = widget.get_active()
        if index == -1:
            return False
        cls.data[cbwe][LAST_INDEX] = index
        return True

    @classmethod
    def _match_selected_cb(cls, completion, comp_model, comp_iter, cbwe):
        column = cbwe.get_entry_text_column()
        text = comp_model.get_value(comp_iter, column)
        cls.set_by_string(cbwe, text)
        return True

    @classmethod
    def focus_out_cb(cls, entry, event, cbwe):
        text = entry.get_text()
        cls.set_by_string(cbwe, text)
        index = cls.data[cbwe].get(LAST_INDEX, -1)
        cbwe.set_active(index)
        return True

    @classmethod
    def add_completion(cls, cbwe):
        entry = cbwe.get_child()
        completion = entry.get_completion()
        if completion:
            return
        completion = Gtk.EntryCompletion()
        model = cbwe.get_model()
        completion.set_model(model)
        completion.set_text_column(0)
        completion.set_inline_completion(True)
        completion.set_match_func(cls.match_selected_cb)
        entry.set_completion(completion)

    @staticmethod
    def match_selected_cb(widget: Gtk.EntryCompletion, search_text, _iter):
        model = widget.get_model()
        col = widget.get_text_column()
        item = model.get_value(_iter, col)
        if item is None:
            return False
        return item.lower().__contains__(search_text.lower())

    @classmethod
    def require_list_item(cls, cbwe):
        cls.add_completion(cbwe)
        entry = cbwe.get_child()
        completion = entry.get_completion()
        index = cbwe.get_active()
        if index == -1:
            model = completion.get_model()
            if model.get_iter_first():
                cbwe.set_active(0)
                index = 0
        cls.data[cbwe][LAST_INDEX] = index
        cid = cbwe.connect("changed", cls.changed_cb, cbwe)
        completion.connect("match_selected", cls._match_selected_cb, cbwe)
        entry.connect("focus-out-event", cls.focus_out_cb, cbwe)
        cls.data[cbwe][CHANGED_ID] = cid
