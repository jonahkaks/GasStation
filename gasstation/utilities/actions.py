def init_short_names(action_group, toolbar_labels):
    for c in toolbar_labels:
        action = action_group.get_action(c.action_name)
        if action is not None:
            action.set_short_label(c.label)


def set_important_actions(action_group, name):
    for c in name:
        action = action_group.get_action(c)
        if action is not None:
            action.set_property("is_important", True)


def update_actions(action_group, action_names, property_name, value):
    for c in action_names:
        action = action_group.get_action(c)
        if action is not None:
            action.set_property(property_name, value)


def add_actions(ui_merge, action_group, filename):
    if ui_merge is None or action_group is None or filename is None or len(filename) == 0:
        return 0
    ui_merge.insert_action_group(action_group, 0)
    merge_id = ui_merge.add_ui_from_resource('/org/jonah/Gasstation/ui/' + filename)
    if merge_id:
        ui_merge.ensure_update()
    return merge_id
