from gasstation.utilities.preferences import *
from libgasstation.core.uri import Uri

PLUGIN_FILE_HISTORY_NAME = "file-history"
FILENAME_STRING = "filename"
MAX_HISTORY_FILES = 10
PREFS_GROUP_HISTORY = "history"
PREF_HISTORY_MAXFILES = "maxfiles"
HISTORY_STRING_FILE_N = "file%d"
PLUGIN_ACTIONS_NAME = "plugin/file-history-actions"

PLUGIN_UI_FILENAME = "plugin/file-history-ui.xml"

GNOME1_HISTORY = "History"
GNOME1_MAXFILES = "MaxFiles"


class FileHistory:
    @staticmethod
    def index_to_pref_name(index: int):
        return HISTORY_STRING_FILE_N % index

    @staticmethod
    def pref_name_to_index(pref: str):
        try:
            i = int(pref.replace("file", ""))
            if i < 0 or i > MAX_HISTORY_FILES:
                return -1
            return i
        except ValueError:
            return -1

    @classmethod
    def add_file(cls, newfile):
        if newfile is None:
            return
        if not GLib.utf8_validate(newfile.encode()):
            return
        last = MAX_HISTORY_FILES - 1
        for i in range(MAX_HISTORY_FILES):
            filename = gs_pref_get_string(PREFS_GROUP_HISTORY, cls.index_to_pref_name(i))
            if not filename:
                last = i
                break
            if GLib.utf8_collate(newfile, filename) == 0:
                last = i
                break
        to = cls.index_to_pref_name(last)
        for i in range(last - 1, -1, -1):
            fro = cls.index_to_pref_name(i)
            filename = gs_pref_get_string(PREFS_GROUP_HISTORY, fro)
            if filename:
                gs_pref_set_string(PREFS_GROUP_HISTORY, to, filename)
            else:
                gs_pref_reset(PREFS_GROUP_HISTORY, to)
            to = fro
        gs_pref_set_string(PREFS_GROUP_HISTORY, to, newfile)

    @classmethod
    def remove_file(cls, oldfile):
        if oldfile is None:
            return
        if not GLib.utf8_validate(oldfile.encode()):
            return
        j = 0
        for i in range(MAX_HISTORY_FILES):
            fro = cls.index_to_pref_name(i)
            filename = gs_pref_get_string(PREFS_GROUP_HISTORY, fro)
            if filename:
                if GLib.utf8_collate(oldfile, filename) == 0:
                    gs_pref_reset(PREFS_GROUP_HISTORY, fro)
                else:
                    if i != j:
                        to = cls.index_to_pref_name(j)
                        gs_pref_set_string(PREFS_GROUP_HISTORY, to, filename)
                        gs_pref_reset(PREFS_GROUP_HISTORY, fro)

                    j += 1

    @classmethod
    def test_for_file(cls, oldfile:str):
        found = False
        if oldfile is None:
            return False
        if not GLib.utf8_validate(oldfile.encode()):
            return False
        for i in range(MAX_HISTORY_FILES):
            fro = cls.index_to_pref_name(i)
            filename = gs_pref_get_string(PREFS_GROUP_HISTORY, fro)
            if filename and GLib.utf8_collate(oldfile, filename) == 0:
                found = True
                break
        return found

    @classmethod
    def get_last(cls):
        pref = cls.index_to_pref_name(0)
        filename = gs_pref_get_string(PREFS_GROUP_HISTORY, pref)
        return filename

    @staticmethod
    def generate_label(index:int, filename:str):
        if filename is None:
            return ""
        f = Uri(filename)
        if f.is_file():
            filepath = f.get_path()
            label = GLib.path_get_basename(filepath)
        else:
            label = str(f)
        splitlabel = label.split("_")
        label = "__".join(splitlabel)
        result = "_%d %s" % ((index + 1) % 10, label)
        return result

    @staticmethod
    def generate_tooltip(index:int, filename:str):
        if filename is None:
            return ""
        f = Uri(filename)
        if f.is_file():
            return GLib.path_get_basename(f.get_path())
        else:
            return str(f)
