import logging

from gi.repository import Gio

from .key_file import *

STATE_FILE_TOP = "Top"
STATE_FILE_BOOK_GUID = "BookGuid"
STATE_FILE_EXT = ".gcm"
state_file_name = None
state_file_name_pre_241 = None
state_file = None


class State:
    @staticmethod
    def set_base(session):
        global state_file_name, state_file_name_pre_241
        state_file_name = None
        state_file_name_pre_241 = None
        uri = session.get_url()
        if uri is None:
            return
        book = session.get_book()
        if book is None: return
        guid_string = str(book.get_guid())
        if uri.is_file():
            path = uri.get_path()
            basename = GLib.path_get_basename(path)
        else:
            basename = "_".join([uri.scheme, uri.hostname, uri.username, uri.path])
        original = GLib.build_filenamev([GLib.get_user_data_dir(), "gasstation", "books", basename])
        sf_extension = STATE_FILE_EXT
        i = 1
        while 1:
            if i == 1:
                filename = original + sf_extension
            else:
                filename = "%s_%d%s" % (original, i, sf_extension)

            key_file = KeyFile.load_from_file(filename)
            if key_file is None:
                if sf_extension == STATE_FILE_EXT:
                    i = 1
                    sf_extension = ""
                    state_file_name = filename
                    continue
                break
            try:
                file_guid = key_file.get_string(STATE_FILE_TOP, STATE_FILE_BOOK_GUID)
            except GLib.GError as e:
                if e.code == Gio.IOErrorEnum.PERMISSION_DENIED:
                    logging.debug("Could not read file permmision denied")
                elif e.code == Gio.IOErrorEnum.IS_DIRECTORY:
                    logging.debug("you can't read a directory.")
                else:
                    logging.debug(str(e))
                file_guid = None
            if guid_string == file_guid:
                if sf_extension == STATE_FILE_EXT:
                    state_file_name = filename
                else:
                    state_file_name_pre_241 = filename

                break
            i += 1

    @classmethod
    def load(cls, session):
        global state_file
        if state_file:
            state_file = None
        cls.set_base(session)
        if state_file_name_pre_241 is not None:
            state_file = KeyFile.load_from_file(state_file_name_pre_241)
        elif state_file_name is not None:
            state_file = KeyFile.load_from_file(state_file_name)
        return cls.get_current()

    @classmethod
    def save(cls, session):
        global state_file
        if not session.get_url() or len(session.get_url()) <= 0:
            return
        cls.set_base(session)
        if state_file_name is not None:
            KeyFile.save_to_file(state_file_name, state_file)

    @staticmethod
    def get_current():
        global state_file
        if state_file is None:
            state_file = GLib.KeyFile.new()
        return state_file

    @staticmethod
    def drop_sections_for(partial_name:str):
        global state_file
        if state_file is None:
            return
        groups, num_groups = state_file.get_groups()
        counted = 0
        for g in groups:
            if g.find(str(partial_name)) > 0:
                if state_file.remove_group(g):
                    counted += 1
        return counted
