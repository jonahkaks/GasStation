from gi import require_version

require_version('Secret', '1')
from gi.repository import Secret


class KeyRing:
    schema = Secret.Schema.new("org.jonah.password", Secret.SchemaFlags.NONE, {
        "scheme": Secret.SchemaAttributeType.STRING,
        "hostname": Secret.SchemaAttributeType.STRING,
        "port": Secret.SchemaAttributeType.INTEGER,
        "user": Secret.SchemaAttributeType.STRING,
        "NULL": 0})

    def __init__(self, scheme, hostname, port=0, username=None):
        self.scheme = scheme
        self.hostname = hostname
        self.port = port
        self.username = username

    def set_scheme(self, scheme):
        self.scheme = scheme

    def set_hostname(self, hostname):
        self.hostname = hostname

    def set_port(self, port):
        self.port = port

    def set_username(self, username):
        self.username = username

    @property
    def attributes(self):
        return {"scheme": self.scheme, "hostname": self.hostname, "user": self.username, "port": str(self.port)}

    def set_password(self, password):
        if password is not None:
            label = "Password for {}://{}@{}".format(self.scheme, self.username, self.hostname)
            Secret.password_store_sync(self.schema, self.attributes, Secret.COLLECTION_DEFAULT,
                                       label, password)
            return True

    def get_password(self):
        return Secret.password_lookup_sync(self.schema, self.attributes)

    def remove_password(self):
        return Secret.password_clear_sync(self.schema, self.attributes)
