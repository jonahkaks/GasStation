from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib
from .key_file import KeyFile

STATE_FILE_TOP = "Top"
STATE_FILE_BOOK_GUID = "BookGuid"
STATE_FILE_EXT = ".gps"


class PrintOperation(Gtk.PrintOperation):
    _print_settings = None
    _page_setup = None
    state_file_name = None
    state_file_name_pre_241 = None
    state_file = None

    def __init__(self, jobname):
        super().__init__()
        self.set_print_settings(PrintOperation._print_settings)
        self.set_default_page_setup(PrintOperation._page_setup)
        self.set_job_name(job_name=jobname)

    @classmethod
    def get_settings(cls):
        return cls._print_settings

    @classmethod
    def get_page_setup(cls):
        return cls._page_setup

    @classmethod
    def page_setup_dialog(cls, parent):
        cls._page_setup = Gtk.print_run_page_setup_dialog(parent, cls._page_setup, cls._print_settings)

    def to_print_settings(self):
        PrintOperation._print_settings = self.get_print_settings()

    @classmethod
    def set_base(cls, session):
        uri = session.get_url()
        if uri is None:
            return
        if uri.is_file():
            path = uri.get_path()
            basename = GLib.path_get_basename(path)
        else:
            basename = "_".join([uri.scheme, uri.hostname, uri.username, uri.path])
        original = GLib.build_filenamev([GLib.get_user_data_dir(), "gasstation", "print", basename])
        sf_extension = STATE_FILE_EXT
        cls.state_file_name = original + sf_extension

    @classmethod
    def load(cls, session):
        state_file = None
        cls.set_base(session)
        if cls.state_file_name is not None:
            state_file = KeyFile.load_from_file(cls.state_file_name)
        if state_file is None:
            state_file = GLib.KeyFile.new()
        cls._print_settings = Gtk.PrintSettings()
        cls._page_setup = Gtk.PageSetup()
        try:
            cls._print_settings.load_key_file(state_file)
        except GLib.Error:
            pass
        try:
            cls._page_setup.load_key_file(state_file)
        except GLib.Error:
            pass
        cls.state_file = state_file

    @classmethod
    def save(cls, session):
        if cls._page_setup is None or cls._print_settings is None:
            return
        if not session.get_url() or len(session.get_url()) <= 0:
            return
        cls.set_base(session)
        cls._page_setup.to_key_file(cls.state_file)
        cls._print_settings.to_key_file(cls.state_file)
        if cls.state_file_name is not None:
            KeyFile.save_to_file(cls.state_file_name, cls.state_file)
