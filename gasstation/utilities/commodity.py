from enum import IntEnum

from gi.repository import GLib

from libgasstation import Session
from libgasstation.core.commodity import *


class CommodityMode(IntEnum):
    CURRENCY = 0
    NON_CURRENCY = 1
    NON_CURRENCY_SELECT = 2
    ALL = 3


# Fixme To remove this file and replace it with EnumSelection

known_timezones = ["Asia/Tokyo",
                   "Australia/Sydney",
                   "America/New_York",
                   "America/Chicago",
                   "Europe/London",
                   "Europe/Paris"]


def update_commodity_picker(cbwe, name_space, init_string):
    current = 0
    match = 0
    if name_space is None or cbwe is None:
        return
    combo_box = cbwe
    model = combo_box.get_model()
    model.clear()
    entry = combo_box.get_child()
    entry.delete_text(0, -1)
    combo_box.set_active(-1)
    table = CommodityTable.get_table(Session.get_current_book())

    commodities = table.get_commodities(name_space)
    if not any(commodities):
        return
    commodity_items = sorted([p.get_printname() for p in commodities])
    for p in commodity_items:
        model.append([p])
        if init_string and GLib.utf8_collate(p, init_string) == 0:
            match = current
        current += 1
    combo_box.set_active(match)


def update_namespace_picker(cbwe, init_string, mode):
    matched = False
    match = None
    _iter = None
    if cbwe is None:
        return
    combo_box = cbwe
    model = combo_box.get_model()
    model.clear()
    if mode == CommodityMode.ALL:
        namespaces = Session.get_current_commodity_table().get_namespaces()

    elif mode == CommodityMode.NON_CURRENCY or mode == CommodityMode.NON_CURRENCY_SELECT:
        namespaces = Session.get_current_commodity_table().get_namespaces()
        try:
            namespaces.remove(COMMODITY_NAMESPACE_NAME_CURRENCY)
        except ValueError:
            pass
        if CommodityNameSpace.is_iso(init_string):
            init_string = None
    else:
        namespaces = [COMMODITY_NAMESPACE_NAME_CURRENCY]
    if mode == CommodityMode.CURRENCY or mode == CommodityMode.ALL:
        _iter = model.append()
        model.set_value(_iter, 0, COMMODITY_NAMESPACE_NAME_ISO_GUI)

    if init_string is not None and GLib.utf8_collate(COMMODITY_NAMESPACE_NAME_ISO_GUI, init_string) == 0:
        matched = True
        match = _iter

    if mode == CommodityMode.NON_CURRENCY_SELECT or mode == CommodityMode.ALL:
        _iter = model.append()
        model.set_value(_iter, 0, COMMODITY_NAMESPACE_NAME_NONCURRENCY)
    namespaces = sorted(namespaces)
    for node in namespaces:
        if GLib.utf8_collate(node, COMMODITY_NAMESPACE_NAME_LEGACY) == 0 or \
                GLib.utf8_collate(node, COMMODITY_NAMESPACE_NAME_TEMPLATE) == 0 \
                or GLib.utf8_collate(node, COMMODITY_NAMESPACE_NAME_CURRENCY) == 0:
            continue
        _iter = model.append()
        model.set_value(_iter, 0, node)
        if init_string and GLib.utf8_collate(node, init_string) == 0:
            matched = True
            match = _iter

    if not matched:
        match = model.get_iter_first()
    combo_box.set_active_iter(match)


def namespace_picker_ns(cbwe):
    if cbwe is None:
        return
    name_space = cbwe.get_child().get_text()
    if name_space == COMMODITY_NAMESPACE_NAME_ISO or name_space == COMMODITY_NAMESPACE_NAME_ISO_GUI or name_space == "Dummy namespace Line":
        return COMMODITY_NAMESPACE_NAME_CURRENCY
    else:
        return name_space
