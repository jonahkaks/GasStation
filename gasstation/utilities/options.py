from gasstation.utilities.ui import *
from libgasstation.core.options import *


class OptionUtils:

    @staticmethod
    def add_currency_selection(options, pagename, name_show_foreign, name_report_currency, sort_tag):
        options.register_option(SimpleBooleanOption())

    @staticmethod
    def add_price_source(options, pagename, optname, sort_tag, default):
        options.register_option(MultiChoiceOption(pagename, optname, sort_tag, "The source of price information.",
                                                  default, [("average-cost", "Average Cost"
                                                             , "The volume-weighted average cost of purchases."),
                                                            ("weighted-average", "Weighted Average",
                                                             "The weighted average of all currency transactions of "
                                                             "the past."),
                                                            ("pricedb-latest", "Most recent",
                                                             "The most recent recorded price."),
                                                            ("pricedb-nearest", "Nearest in time",
                                                             "The price recorded nearest in time to the report date.")]))

    @staticmethod
    def add_plot_size(options, pagename, name_width, name_height, sort_tag, default_width, default_height):
        options.register_option(NumberPlotSizeOption(pagename, name_width, sort_tag + "a",
                                                     "Width of plot in pixels.", default_width, 100, 20000, 0, 5))
        options.register_option(NumberPlotSizeOption(pagename, name_height, sort_tag + "a",
                                                     "Width of plot in pixels.", default_height, 100, 20000, 0, 5))

    @staticmethod
    def add_sort_method(options, pagename, optname, sort_tag, default):
        options.register_option(
            MultiChoiceOption(pagename, optname, sort_tag, "Choose the method for sorting accounts.",
                              default, [("acct-code", "Account Code", "Alphabetical by account code."),
                                        ("alphabetical", "Alphabetical", "Alphabetical by account name."),
                                        ("amount", "Amount", "By amount, largest to smallest.")]))

    @staticmethod
    def get_color_info(option: ColorOption, use_default, color):
        if option is None:
            return False
        if use_default:
            value = option.get_default()
        else:
            value = option.get_value()
        if not isinstance(value, (list, tuple)):
            return False
        scale = option.range()
        if scale <= 0:
            return False
        rgba = round(value[0] / scale, 2)
        color.red = min(1.0, rgba)
        rgba = round(value[1] / scale, 2)
        color.green = min(1.0, rgba)
        rgba = round(value[2] / scale, 2)
        color.blue = min(1.0, rgba)
        rgba = round(value[3] / scale, 2)
        color.aplha = min(1.0, rgba)
        return True

    @staticmethod
    def dateformat_parse(value):
        if not isinstance(value, (list, tuple)):
            return True
        fmt = date_string_to_dateformat(value[0])
        months = date_string_to_monthformat(value[1])
        years = value[2]
        custom = value[3]
        return fmt, months, years, custom

    @staticmethod
    def add_report_date(options, pagename, optname, sort_tag):
        return make_end_date(options, pagename, optname, sort_tag, "Select a date to report on")

    @staticmethod
    def add_date_interval(options, pagename, name_from, name_to, sort_tag):
        return make_date_interval(options, pagename, name_from, "Starting of a reporting period",
                                  name_to, "End of a reporting period", sort_tag)

    @staticmethod
    def add_interval_choice(options, pagename, optname, sort_tag, default):
        options.register_option(MultiChoiceOption(pagename, optname, sort_tag,
                                                  "The amount of time between data points.", default,
                                                  [("DayDelta", "Day", "One Day."),
                                                   ("WeekDelta", "Week", "One Week."),
                                                   ("TwoWeekDelta", "2Week", "Two Weeks."),
                                                   ("MonthDelta", "Month", "One Month."),
                                                   ("QuarterDelta", "Quarter", "One Quarter."),
                                                   ("HalfYearDelta", "Half Year", "Half Year."),
                                                   ("YearDelta", "Year", "One Year.")]))

    @staticmethod
    def add_account_levels(options, pagename, name_display_depth, sort_tag, help_string, default_depth):
        options.register_option(MultiChoiceOption(pagename, name_display_depth, sort_tag, help_string, default_depth,
                                                  [("all", "All", "All accounts"),
                                                   (1, "1", "Top-level."),
                                                   (2, "2", "Second-level."),
                                                   (3, "3", "Third-level."),
                                                   (4, "4", "Fourth-level."),
                                                   (5, "5", "Fifth-level."),
                                                   (6, "6", "Sixth-level.")
                                                   ]))

    @staticmethod
    def add_currency(options, pagename, name_report_currency, sort_tag):
        options.register_option(CurrencyOption(pagename, name_report_currency, sort_tag,
                                               "Select the currency to display the values of this report in.",
                                               gs_default_currency()))

    @staticmethod
    def add_subtotal_view(options, pagename, optname_parent_balance_mode, optname_parent_total_mode, sort_tag):
        options.register_option(MultiChoiceOption(pagename, optname_parent_balance_mode, sort_tag + "a",
                                                  "How to show the balances of parent accounts.", "immediate-bal",
                                                  [("immediate-bal", "Account Balance",
                                                    "Show only the balance in the parent account, excluding any subaccounts."),
                                                   ("recursive-bal", "Subtotal",
                                                    "Calculate the subtotal for this parent "
                                                    "account and all of its subaccounts, and "
                                                    "show this as the parent account balance."),
                                                   ("omit-bal", "Do not show",
                                                    "Do not show any balances of parent accounts.")]))
        options.register_option(MultiChoiceOption(pagename, optname_parent_total_mode, sort_tag + "b",
                                                  "How to show account subtotals for parent accounts.", False,
                                                  [(True, "Show subtotals",
                                                    "Show subtotals for selected parent accounts which have subaccounts."),
                                                   (False, "Do not show",
                                                    "Do not show any subtotals for parent accounts."),
                                                   ("canonically-tabbed",
                                                    "Text book style (experimental)",
                                                    "Show parent account subtotals, indented per accounting text book practice (experimental).")]))

    @classmethod
    def add_account_selection(cls, options, pagename, name_display_depth, name_show_subaccounts, name_accounts,
                              sort_tag, default_depth, default_accounts, default_show_subaccounts):
        cls.add_account_levels(options, pagename, name_display_depth, sort_tag + "a",
                               "Show accounts to this depth, overriding any other option.", default_depth)

        options.register_option(SimpleBooleanOption(pagename, name_show_subaccounts, sort_tag + "b",
                                                    "Override account_selection and show sub_accounts of all selected "
                                                    "accounts?",
                                                    default_show_subaccounts))

        options.register_option(AccountListOption(pagename, name_accounts, sort_tag + "c",
                                                  "Report on these accounts, if display depth allows.",
                                                  default_accounts, False, True))
