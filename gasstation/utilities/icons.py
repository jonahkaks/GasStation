ICON_ACCOUNT = "account"
ICON_SALES = "view-grid-symbolic"
ICON_ACCOUNT_REPORT = "display-projector-symbolic"
ICON_DELETE_ACCOUNT = "task-past-due-symbolic"
ICON_EDIT_ACCOUNT = "document-edit-symbolic"
ICON_NEW_ACCOUNT = "document-new-symbolic"
ICON_OPEN_ACCOUNT = "document-open-symbolic"
ICON_SPLIT_TRANS = "media-playlist-shuffle-symbolic"
ICON_SCHEDULE = "scheduled-new"
ICON_TRANSFER = "document-send-symbolic"
ICON_JUMP_TO = "object-rotate-right"
ICON_INVOICE = "invoice"
ICON_INVOICE_PAY = "payment"
ICON_INVOICE_NEW = "view-dual-symbolic"
ICON_INVOICE_EDIT = "document-edit-symbolic"
ICON_INVOICE_DUPLICATE = "invoice-duplicate"
ICON_PDF_EXPORT = "pdf"
ICON_BUDGET = "account"
ICON_NEW_BUDGET = "account"
ICON_OPEN_BUDGET = "account-open"
ICON_DELETE_BUDGET = "account-delete"
ICON_APP = "org.jonah.Gasstation-symbolic"
ICON_PRODUCT = "product"
ICON_PURCHASE = "purchase"
ICON_PRICE = "price"
ICON_INVENTORY_CATEGORY = "category"


# def load_app_icons():
#     from gi.repository import Gtk
#     icon_theme = Gtk.IconTheme.get_default()
#     icon_theme.add_resource_path('/org/jonah/Gasstation/icons')
