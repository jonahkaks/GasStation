from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import Gdk, Gtk

PREF_LAST_GEOMETRY = "last-geometry"
PREF_GRID_LINES_HORIZONTAL = "grid-lines-horizontal"
PREF_GRID_LINES_VERTICAL = "grid-lines-vertical"

from .accounting_period import *


def gs_set_label_color(label, value):
    if not gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_NEGATIVE_IN_RED) or label is None or value is None:
        return
    deficit = True if value < 0 else False
    if deficit:
        gs_widget_set_style_context(label, "negative-numbers")
    else:
        gs_widget_set_style_context(label, "default-color")


def gs_restore_window_size(group, window, parent):
    if group is None or window is None:
        return
    if not gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_SAVE_GEOMETRY):
        return
    geometry = gs_pref_get_value(group, PREF_LAST_GEOMETRY)
    if geometry.is_of_type(GLib.VariantType.new("(iiii)")):
        display = Gdk.Display.get_default()
        w, x, y, z = geometry
        mon = display.get_monitor_at_point(w, x)
        monitor_size = mon.get_geometry()
        if (w != -1) and (x != -1):
            if w - monitor_size.x + y > monitor_size.x + monitor_size.width:
                w = monitor_size.x + monitor_size.width - y

            if x - monitor_size.y + z > monitor_size.y + monitor_size.height:
                x = monitor_size.y + monitor_size.height - z
            if w < monitor_size.x:
                w = monitor_size.x

            if x < monitor_size.y:
                x = monitor_size.y
            window.move(w, x)
        else:
            if parent is not None:
                parent_w, parent_x = parent.get_position()
                parent_y, parent_z = parent.get_size()
                window_y, window_z = window.get_size()
                if (window_y == 200) and (window_z == 200):
                    pass
                else:
                    window.move(parent_w + (parent_y - window_y) / 2,
                                parent_x + (parent_z - window_z) / 2)

        if (y > 0) and (z > 0):
            y = min(y, monitor_size.width - 10)
            z = min(z, monitor_size.height - 10)
            window.resize(y, z)


def gs_save_window_size(group, window):
    if group is None or window is None:
        return
    if not gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_SAVE_GEOMETRY):
        return
    wpos = window.get_position()
    wsize = window.get_size()
    geometry = GLib.Variant("(iiii)", wpos + wsize)
    gs_pref_set_value(group, PREF_LAST_GEOMETRY, geometry)


def gs_window_adjust_for_screen(window):
    if window is None or not isinstance(window, Gtk.Window) or window.get_window() is None:
        return
    win = window.get_window()
    display = win.get_display()
    w, x = window.get_position()
    width, height = window.get_size()
    mon = display.get_monitor_at_point(w, x)
    monitor_size = mon.get_geometry()

    if (width <= monitor_size.width) and (height <= monitor_size.height):
        return
    if w - monitor_size.x + width > monitor_size.x + monitor_size.width:
        w = monitor_size.x + monitor_size.width - width

    if x - monitor_size.y + height > monitor_size.y + monitor_size.height:
        x = monitor_size.y + monitor_size.height - height

    if w < monitor_size.x:
        w = monitor_size.x

    if x < monitor_size.y:
        x = monitor_size.y

    window.move(w, x)

    width = min(width, monitor_size.width - 10)
    height = min(height, monitor_size.height - 20)
    window.resize(width, height)
    window.queue_resize()


def gs_label_set_alignment(widget, xalign, yalign):
    widget.set_xalign(xalign)
    widget.set_yalign(yalign)


def gs_tree_view_get_grid_lines_pref():
    h_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_GRID_LINES_HORIZONTAL)
    v_lines = gs_pref_get_bool(PREFS_GROUP_GENERAL, PREF_GRID_LINES_VERTICAL)
    if h_lines:
        if v_lines:
            grid_lines = Gtk.TreeViewGridLines.BOTH
        else:
            grid_lines = Gtk.TreeViewGridLines.HORIZONTAL
    elif v_lines:
        grid_lines = Gtk.TreeViewGridLines.VERTICAL
    else:
        grid_lines = Gtk.TreeViewGridLines.NONE
    return grid_lines


def gs_widget_set_style_context(widget, klass):
    if widget is None:
        return
    context = widget.get_style_context()
    if klass is not None and not context.has_class(klass):
        context.add_class(klass)


def gs_widget_remove_style_context(widget, clas):
    if widget is None:
        return
    context = widget.get_style_context()
    if clas is not None and context.has_class(clas):
        context.remove_class(clas)


def gs_dialog_add_button(dialog, label, icon_name, response):
    button = Gtk.Button.new_with_mnemonic(label)
    if icon_name:
        image = Gtk.Image.new_from_icon_name(icon_name, Gtk.IconSize.BUTTON)
        button.set_image(image)
        button.set_property("always-show-image", True)
    button.set_property("can-default", True)
    button.show()
    dialog.add_action_widget(button, response)


def gs_perm_button_cb(perm, user_data):
    perm_active = perm.get_active()
    if user_data is not None:
        user_data.set_sensitive(not perm_active)


def gs_dialog_run(dialog, pref_name):
    response = gs_pref_get_int(PREFS_GROUP_WARNINPERM, pref_name)
    if response != 0:
        return response
    response = gs_pref_get_int(PREFS_GROUP_WARNINTEMP, pref_name)
    if response != 0:
        return response
    if isinstance(dialog, Gtk.MessageDialog):
        _type = dialog.get_property("message-type")
        ask = (_type == Gtk.MessageType.QUESTION)
    else:
        ask = False
    perm = Gtk.CheckButton.new_with_mnemonic("Remember and don't _ask me again." if ask else "Don't _tell me again.")
    temp = Gtk.CheckButton.new_with_mnemonic(
        "Remember and don't ask me again this _session." if ask else "Don't tell me again this _session.")
    perm.show()
    temp.show()
    perm.set_hexpand(True)
    perm.set_vexpand(True)
    temp.set_hexpand(True)
    temp.set_vexpand(True)
    dialog.get_content_area().pack_start(perm, False, False, 0)
    dialog.get_content_area().pack_start(temp, False, False, 0)
    perm.connect("clicked", gs_perm_button_cb, temp)
    response = dialog.run()
    if response == Gtk.ResponseType.NONE or response == Gtk.ResponseType.DELETE_EVENT:
        return Gtk.ResponseType.CANCEL
    if response != Gtk.ResponseType.CANCEL:
        if perm.get_active():
            gs_pref_set_int(PREFS_GROUP_WARNINPERM, pref_name, response)
        elif temp.get_active():
            gs_pref_set_int(PREFS_GROUP_WARNINTEMP, pref_name, response)
    return response


def gs_draw_arrow_cb(widget, cr, direction):
    context = widget.get_style_context()
    width = widget.get_allocated_width()
    height = widget.get_allocated_height()
    Gtk.render_background(context, cr, 0, 0, width, height)
    context.add_class(Gtk.STYLE_CLASS_ARROW)
    size = min(width / 2, height / 2)
    if not direction:
        Gtk.render_arrow(context, cr, 0,
                         (width - size) / 2, (height - size) / 2, size)
    else:
        Gtk.render_arrow(context, cr, GLib.PI,
                         (width - size) / 2, (height - size) / 2, size)
    return True


def gdate_in_valid_range(test_date, warn):
    from libgasstation.core.session import Session
    from gasstation.views.main_window import MainWindow
    use_auto_readonly = Session.get_current_book().uses_auto_readonly()
    max_date = GLib.Date.new_dmy(1, 1, 10000)
    max_date_ok = False
    min_date_ok = False
    if use_auto_readonly:
        t = Session.get_current_book().get_auto_readonly_date()
        min_date = GLib.Date()
        min_date.set_dmy(t.day, t.month, t.year)
    else:
        min_date = GLib.Date.new_dmy(1, 1, 1400)
    if GLib.Date.compare(max_date, test_date) > 0:
        max_date_ok = True
    if GLib.Date.compare(min_date, test_date) <= 0:
        min_date_ok = True
    if use_auto_readonly and warn:
        ret = max_date_ok
    else:
        ret = min_date_ok & max_date_ok
    if warn and not ret:
        dialog_msg = "The entered date is out of the range " \
                     "01/01/1400 - 31/12/9999, resetting to this year"
        dialog_title = "Date out of range"
        dialog = Gtk.MessageDialog(MainWindow.get_main_window(None),
                                   0,
                                   Gtk.MessageType.ERROR,
                                   Gtk.ButtonsType.OK, dialog_title)
        dialog.format_secondary_text(dialog_msg)
        dialog.run()
        dialog.destroy()
    return ret


def gs_handle_date_accelerator(event, tm: datetime.date, date_str):
    if event is None or tm is None or date_str is None: return False
    if event.type != Gdk.EventType.KEY_PRESS:
        raise ValueError("Expected KeyPress event")
    if tm.day <= 0 or tm.month == -1 or tm.year == -1:
        raise ValueError("Invaalid date")

    keyval = Gdk.keyval_name(event.keyval)
    if keyval == 'plus' or keyval == 'equal' or keyval == 'KP_Add':
        if event.state & Gdk.ModifierType.SHIFT_MASK:
            return tm + relativedelta(days=7)
        elif event.state & Gdk.ModifierType.MOD1_MASK:
            return tm + relativedelta(months=1)
        elif event.state & Gdk.ModifierType.CONTROL_MASK:
            return tm + relativedelta(years=1)
        else:
            return tm + relativedelta(days=1)

    elif keyval == 'underscore' or keyval == 'minus' or keyval == 'KP_Subtract':
        if len(date_str) != 0 and (dateSeparator() == '-'):
            if date_str.count("-") < 2:
                return False, tm
        if event.state & Gdk.ModifierType.SHIFT_MASK:
            return tm - relativedelta(days=7)
        elif event.state & Gdk.ModifierType.MOD1_MASK:
            return tm - relativedelta(months=1)
        elif event.state & Gdk.ModifierType.CONTROL_MASK:
            return tm - relativedelta(years=1)
        else:
            return tm - relativedelta(days=1)
    if event.state & Gdk.ModifierIntent.DEFAULT_MOD_MASK:
        raise ValueError("Invalid event state")
    if keyval == 'bracketright':
        return tm + relativedelta(months=1)
    elif keyval == 'bracketleft':
        return tm - relativedelta(months=1)

    elif keyval == 'M' or keyval == 'm':
        return tm.replace(day=1)

    elif keyval == 'H' or keyval == 'h':
        return (tm.replace(day=1) + relativedelta(months=1)) - relativedelta(days=1)

    elif keyval == 'y' or keyval == 'Y':
        return tm.replace(day=1, month=1)

    elif keyval == 'R' or keyval == 'r':
        return (tm.replace(day=1, month=1) + relativedelta(years=1)) - relativedelta(days=1)

    elif keyval == 't' or keyval == 'T':
        return datetime.date.today()
    return tm


allsymbols = None


def gs_builder_connect_full_func(builder, signal_object, signal_name, handler_name, connect_object, flags, *user_data):
    global allsymbols
    # if allsymbols is None:
    #     allsymbols = GModule.Module.build_path(None, 0)
    #     if not allsymbols.symbol(handler_name, func)
    # {
    # #ifdef HAVE_DLSYM
    # /* Fallback to dlsym -- necessary for *BSD linkers */
    # func = dlsym(RTLD_DEFAULT, handler_name);
    # #else
    # func = NULL;
    # #endif
    # if (func == NULL)
    # {
    # PWARN("ggaff: could not find signal handler '%s'.", handler_name);
    # return;
    # }
    # }
    func = None
    if connect_object is not None:
        signal_object.connect(signal_name, func, connect_object, flags)
    else:
        signal_object.connect_data(signal_name, func, user_data, None, flags)
