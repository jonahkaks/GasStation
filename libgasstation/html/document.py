from libgasstation.html.stylesheets.text import *


class HtmlDocument:
    def __init__(self):
        self.stylesheet = None
        self.style_stack = []
        self.style = HtmlStyleTable()
        self.style_text = None
        self.title = None
        self.headline = None
        self.objects = []
        self.data = None
        self.pe = None

    def get_title(self):
        return self.title

    def set_title(self, t):
        self.title = t

    def set_stylesheet(self, css):
        self.stylesheet = css

    def get_stylesheet(self):
        return self.stylesheet

    def set_headline(self, hl):
        self.headline = hl

    def get_headline(self):
        return self.headline

    def set_style_stack(self, sstack):
        self.style_stack = sstack

    def get_style_stack(self):
        return self.style_stack

    def set_objects(self, objs: list):
        self.objects += objs

    def get_objects(self):
        return self.objects

    def set_style(self, tag_s, *args):
        self.style.set(tag_s,
                       HtmlStyleDataInfo(*args) if len(args) == 2 and callable(args[1]) else HtmlStyleMarkupInfo(*args))

    def get_style(self):
        return self.style

    def set_style_text(self, st):
        self.style_text = st

    def get_style_text(self):
        return self.style_text

    def set_style_internal(self, style):
        self.style = style

    def set_percentage_cb(self, pe):
        self.pe = pe

    def update_progress(self, p=None, m=None):
        if self.pe is not None:
            self.pe(m, p)

    def render(self, headers: bool = True):
        if self.stylesheet is not None:
            return self.stylesheet.render(self, headers)
        self.style.compile(self.style_stack)
        self.push_style(self.style)
        retval = DIV() if not headers else HTML(doctype="html5")
        objs = self.objects
        work_to_do = len(self.objects)
        work_done = 0
        if headers:
            head = HEAD(META(_content="text/html", _http_equiv="content-type", _charset="utf-8"))
            retval.append(head)
            if self.style_text is not None:
                head.append(STYLE(self.style_text, _type="text/css"))
            if self.title is not None:
                head.append(TITLE(self.title))
            body = BODY()
            for obj in objs:
                body.append(obj)
                work_done += 1
                self.update_progress((work_done / work_to_do) * 100)
            retval.append(body.render(self))
        else:
            for obj in objs:
                retval.append(obj)
                work_done += 1
                self.update_progress((work_done / work_to_do) * 100)
        self.pop_style()
        self.style.uncompile()
        return str(retval)

    def add_object(self, obj):
        if isinstance(obj, (list, tuple, set)):
            self.append_objects(obj)
            return
        self.objects.append(obj)

    def push_style(self, style):
        self.style_stack.insert(0, style)

    def pop_style(self):
        self.style_stack.pop(0)

    def append_objects(self, objs):
        self.objects.extend(objs)

    def fetch_markup_style(self, markup):
        stack = self.style_stack
        if stack is not None and len(stack) > 0:
            return stack[0].fetch(stack[1:], markup)
        else:
            return HtmlStyleMarkupInfo()

    def fetch_data_style(self, data):
        pass
