import json
import uuid

from libgasstation.html.stylesheets.text import *

js_number_to_string = """
var toLocaleStringSupportsOptions = (typeof Intl == 'object' && Intl && typeof Intl.NumberFormat == 'function');

// format a number e.g. 2.5 into monetary e.g. \"$2.50\"
function numformat(amount) {
  if (toLocaleStringSupportsOptions) {
      return amount.toLocaleString(undefined, {style:'currency', currency:curriso});
  } else {
      return currsym + amount.toLocaleString();
  }
}
"""
js_setup = """function tooltipLabel(tooltipItem,data) {
  var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || 'Other';
  var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
  switch (typeof(label)) {
    case 'number':
      return datasetLabel + ': ' + numformat(label);
    default:
      return '';
  }
}

function tooltipTitle(array,data) {
  return chartjsoptions.data.labels[array[0].index]; }

// draw the background color
Chart.pluginService.register({
  beforeDraw: function (chart, easing) {
    if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
      var ctx = chart.chart.ctx;
      var chartArea = chart.chartArea;
      ctx.save();
      ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
      ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
      ctx.restore();
    }
  }
})

// copy font info from css into chartjs.
bodyStyle = window.getComputedStyle (document.querySelector ('body'));
Chart.defaults.global.defaultFontSize = parseInt (bodyStyle.fontSize);
Chart.defaults.global.defaultFontFamily = bodyStyle.fontFamily;
Chart.defaults.global.defaultFontStyle = bodyStyle.fontStyle;

titleStyle = window.getComputedStyle (document.querySelector ('h3'));
chartjsoptions.options.title.fontSize = parseInt (titleStyle.fontSize);
chartjsoptions.options.title.fontFamily = titleStyle.fontFamily;
chartjsoptions.options.title.fontStyle = titleStyle.fontStyle;

document.getElementById(chartid).onclick = function(evt) {
  var activepoints = myChart.getElementAtEvent(evt);
  var anchor = document.getElementById(jumpid);
  switch (activepoints.length)  {
    case 0:
      anchor.href = '';
      anchor.textContent = '';
      anchor.style = 'display: none';
      break;
    default:
      var index = activepoints[0]['_index'];
      var datasetIndex = activepoints[0]['_datasetIndex'];
      var datasetURLs = myChart.data.datasets[datasetIndex].urls;
      // console.log('index=',index,'datasetIndex=',datasetIndex);
      anchor.style = 'position:absolute; top:' + (evt.clientY - 30) + 'px; left:' + (evt.clientX - 20) + 'px; display: block; padding: 5px; border-radius: 5px; background: #4E9CAF; text-align:center; color:white; z-index: 999;';
      switch (typeof(datasetURLs)) {
        case 'string':
          anchor.href = datasetURLs;
          anchor.textContent = loadstring;
          break;
        case 'object':
          anchor.href = datasetURLs[index];
          anchor.textContent = loadstring;
          break;
        default:
          anchor.href = '';
          anchor.textContent = '';
          anchor.style = 'display: none';
      }
  }
}\n\n"""


class HtmlChart(DIV):
    def __init__(self, width=("percent", 100), height=("percent", 100), options=None, currency_iso="XXX",
                 currency_symbol="UGX", *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.width = None
        self.height = None
        self.set_width(width)
        self.set_height(height)
        if options is None:
            options = {"type": "bar",
                       "data": {
                           "labels": [],
                           "datasets": []
                       },
                       "options": {
                           "maintainAspectRatio": False,
                           "animation": {
                               "duration": 0},
                           "chartArea": {
                               "backgroundColor": "#fffdf6"},
                           "legend": {
                               "position": "right",
                               "reverse": False,
                               "labels": {
                                   "fontColor": "black"}
                           },
                           "elements": {
                               "line": {
                                   "tension": 0
                               },
                               "point": {
                                   "pointStyle": False
                               }
                           },
                           "tooltips":
                               {"callbacks": {
                                   "label": False
                               }
                               },
                           "scales": {
                               "xAxes": [{"display": True,
                                          "type": "category",
                                          "distribution": "series",
                                          "offset": True,
                                          "gridLines": {
                                              "display": True,
                                              "lineWidth": 1.5
                                          },
                                          "scaleLabel": {
                                              "display": True,
                                              "labelString": ""
                                          },
                                          "ticks": {
                                              "maxRotation": 30
                                          }
                                          },
                                         {"position": "top",
                                          "ticks": {
                                              "display": False
                                          },
                                          "gridLines": {
                                              "display": False
                                          },
                                          "drawTicks": False
                                          }],
                               "yAxes": [{
                                   "stacked": False,
                                   "display": True,
                                   "gridLines": {
                                       "display": True,
                                       "lineWidth": 1.5
                                   },
                                   "scaleLabel": {
                                       "display": True,
                                       "labelString": ""
                                   },
                                   "ticks": {
                                       "beginAtZero": False
                                   }
                               },
                                   {
                                       "position": "right",
                                       "ticks": {
                                           "display": False
                                       },
                                       "gridLines": {
                                           "display": False,
                                           "drawTicks": False}
                                   }
                               ]
                           },
                           "title": {
                               "display": True,
                               "fontStyle": "",
                               "text": ""
                           }}}
        self.options = options
        self.custom_y_axis = True
        self.custom_x_axis = True
        self.currency_iso = currency_iso
        self.currency_symbol = currency_symbol

    def set_type(self, type: str):
        self.options["type"] = type

    def get_type(self):
        return self.options.get("type")

    def get_colors(self, data):
        if any(data):
            base_colors = ["#FF4136", "#FF851B", "#FFDC00", "#2ECC40",
                           "#0074D9", "#001f3f", "#85144b", "#7FDBFF",
                           "#F012BE", "#3D9970", "#39CCCC", "#f39c12",
                           "#e74c3c", "#e67e22", "#9b59b6", "#8e44ad",
                           "#16a085", "#d35400"]
            cols = len(data[0] if isinstance(data[0], list) else data)
            i = 0
            ret = base_colors[:cols]
            for _ in range(cols):
                ret.append(base_colors[i])
                i += 1
                if i > 17:
                    i = 0
            return ret
        else:
            return []

    def get_color_string(self):
        return ", ".join(map(lambda color: "\"%s\"" % color, self.get_colors()))

    def get_width(self):
        return self.width

    def set_width(self, w):
        w_string = "{}px;" if w[0] == "pixels" else "{}%;"
        self.width = w_string.format(int(w[1]))

    def get_height(self):
        return self.height

    def set_height(self, h):
        h_string = "{}px" if h[0] == "pixels" else "{}%"
        self.height = h_string.format(int(h[1]))

    def get_data_series(self):
        return self.options["data"]["datasets"]

    def add_data_series(self, label, data, color, **kwargs):
        new_vec = kwargs
        new_vec["label"] = label
        new_vec["backgroundColor"] = color
        new_vec["borderColor"] = color
        new_vec["data"] = data
        self.options["data"]["datasets"].append(new_vec)

    def set_title(self, title):
        self.options["options"]["title"]["text"] = title

    def get_title(self) -> str:
        return self.options["options"]["title"]["text"]

    def set_data_labels(self, labels):
        self.options["data"]["labels"] = labels

    def set_axes_display(self, display):
        self.options["options"]["scales"]["xAxes"][0]["display"] = display
        self.options["options"]["scales"]["yAxes"][0]["display"] = display

    def clear_data_series(self):
        self.options["data"]["datasets"] = []

    def set_x_axis_label(self, label):
        self.options["options"]["scales"]["xAxes"][0]["labelString"] = label

    def set_stacking(self, stack):
        self.options["options"]["scales"]["xAxes"][0]["stacked"] = stack
        self.options["options"]["scales"]["yAxes"][0]["stacked"] = stack

    def set_grid(self, grid):
        self.options["options"]["scales"]["xAxes"][0]["gridLines"]["display"] = grid
        self.options["options"]["scales"]["yAxes"][0]["gridLines"]["display"] = grid

    def set_y_axis_label(self, label):
        self.options["options"]["scales"]["yAxes"][0]["labelString"] = label

    def get_options_string(self):
        return json.dumps(self.options, indent=4)

    def render(self, doc=None):
        chart_id = uuid.uuid4().hex
        self["_style"] = "width:{}height:{}".format(self.width, self.height)
        push = self.append
        if not any(self.get_data_series()):
            return ""
        else:
            push(SCRIPT(_type="text/javascript", _src="Chart.bundle.min.js"))
            push(H3(_style='display:none'))
            push(ANCHOR(_id="jump-%s" % chart_id, _href=""))
            push(CANVAS(_id=chart_id))
            script_source = []
            spush = script_source.append
            spush("var curriso = '%s';\n" % self.currency_iso)
            spush("var currsym = '%s';\n" % self.currency_symbol)
            spush("var chartid = '%s';\n" % chart_id)
            spush("var jumpid = 'jump-%s';\n" % chart_id)
            spush("var loadstring = 'Load';\n")
            spush("var chartjsoptions = %s;\n\n" % self.get_options_string())
            spush(js_number_to_string)
            if self.custom_y_axis:
                spush("chartjsoptions.options.scales.yAxes[0].ticks.callback = yAxisDisplay;\n")
                spush("function yAxisDisplay(value,index,values) { return numformat(value); };\n")
            if self.custom_x_axis:
                spush("chartjsoptions.options.scales.xAxes[0].ticks.callback = xAxisDisplay;\n")
                spush("function xAxisDisplay(value,index,values) { return chartjsoptions.data.labels[index]; };\n")
            spush("chartjsoptions.options.tooltips.callbacks.label = tooltipLabel;\n")
            spush("chartjsoptions.options.tooltips.callbacks.title = tooltipTitle;\n")
            spush(js_setup)
            spush("var myChart = new Chart(chartid, chartjsoptions);\n")
            push(TAG("\n"))
            push(SCRIPT("".join(script_source), _id="script-%s" % chart_id))
            push(TAG("\n"))
        return super().render(doc)
