from .chart import *


class HtmlPieChart(HtmlChart):
    def __init__(self):
        super().__init__()
        self.button_1_slice_urls = None
        self.button_2_slice_urls = None
        self.button_3_slice_urls = None
        self.labels = None

    def set_labels(self, labels):
        self.labels = labels

    def get_labels(self):
        return self.labels

    def get_button_1_slice_urls(self):
        return self.button_1_slice_urls

    def get_button_2_slice_urls(self):
        return self.button_2_slice_urls

    def get_button_3_slice_urls(self):
        return self.button_3_slice_urls

    def render(self, doc):
        color_str = self.get_color_string()
        chart_id = uuid.uuid4().hex
        if isinstance(self.data, list) and any(self.data):
            push = self.append
            push(SCRIPT(_type="text/javascript", _src="jqplot/jquery.min.js"))
            push(TAG("\n"))
            push(SCRIPT(_type="text/javascript", _src="jqplot/jquery.jqplot.js"))
            push(TAG("\n"))
            push(SCRIPT(_type="text/javascript", _src="jqplot/jqplot.pieRenderer.js"))
            push(TAG("\n"))
            push(LINK(_rel="stylesheet", _type="text/css", _href="jqplot/jquery.jqplot.css"))
            push(TAG("\n"))
            push(DIV(_id=chart_id, _style="width:%s; height:%s;" % (self.width, self.height)))
            push(TAG("\n"))
            t = []
            push = t.append
            push("$(function () {\n")
            push("var data = [];\n")
            for i, datum in enumerate(self.data):
                push("  data.push([\"%s\",%s]);\n" % (self.labels[i], datum))
            push("var options = {"
                 "seriesDefaults: {"
                 "renderer: $.jqplot.PieRenderer,"
                 "},"
                 "legend: {"
                 "show: true,"
                 "placement: \"outsideGrid\", },"
                 "highlighter: {"
                 " show: false },"
                 "cursor: {"
                 "showTooltip: false },"
                 "seriesColors: false,"
                 "};\n")
            if self.title is not None:
                push("  options.title = \"%s\";\n" % self.title)
            if self.subtitle is not None:
                push("  options.title += \"(%s)\";\n" % self.subtitle)
            if len(color_str):
                push("options.seriesColors = [%s];\n" % color_str)
            push("$.jqplot.config.enablePlugins = true;\n")
            push("$(document).ready(function() {var plot = $.jqplot('")
            push(chart_id)
            push("', [data], options);\n"
                 "plot.replot();"
                 "var timer;"
                 " $(window).resize(function () {"
                 "clearTimeout(timer);"
                 "timer = setTimeout(function () {"
                 "plot.replot();"
                 "}, 100);"
                 "});"
                 "});\n")
            push("});\n")
            self.append(SCRIPT("".join(t), _id="source"))
            return super().render(doc)
