from libgasstation.html.stylesheets.text import *
from .chart import *


class HtmlScatterChart(HtmlChart):
    def __init__(self):
        super().__init__()
        self.marker = None
        self.x_axis_label = None
        self.y_axis_label = None
        self.marker_color = None

    def set_x_axis_label(self, lab):
        self.x_axis_label = lab

    def get_marker(self):
        return self.marker

    def set_marker(self, marker):
        self.marker = marker

    def get_marker_color(self):
        return self.marker_color

    def set_marker_color(self, marker_color):
        self.marker_color = marker_color

    def set_y_axis_label(self, lab):
        self.y_axis_label = lab

    def get_x_axis_label(self):
        return self.x_axis_label

    def get_y_axis_label(self):
        return self.y_axis_label

    def set_labels(self, labels):
        self.labels = labels

    def get_labels(self):
        return self.labels

    def render(self, doc):
        chart_id = uuid.uuid4().hex
        if isinstance(self.data, list) and any(self.data):
            retval = []
            push = retval.append
            push(SCRIPT(_type="text/javascript", _src="jqplot/jquery.min.js"))
            push(TAG("\n"))
            push(SCRIPT(_type="text/javascript", _src="jqplot/jquery.jqplot.js"))
            push(TAG("\n"))
            push(LINK(_rel="stylesheet", _type="text/css", _href="jqplot/jquery.jqplot.css"))
            push(TAG("\n"))
            push(DIV(_id=chart_id, _style="width:%s; height:%s;" % (self.width, self.height)))
            push(TAG("\n"))
            retval = []
            push = retval.append
            push("$(function () {\n")
            push("var data = [];\n")
            push("var series = [];\n")
            for x, y in self.data:
                push("  data.push([%s,%s]);\n" % (x, y))
            push("var options = {"
                 "legend: { show: false, },"
                 "seriesDefaults: {"
                 "markerOptions: {"
                 "style: '%s', color:'%s'}" % (self.marker, self.marker_color))
            push("},series: series,"
                 "axesDefaults: {"
                 "},"
                 " axes: {"
                 "xaxis: {"
                 "},"
                 "yaxis: {"
                 "autoscale: true,"
                 "},"
                 "},"
                 "};\n")
            if self.title is not None:
                push("  options.title = \"%s\";\n" % self.title)
            if self.subtitle is not None:
                push("  options.title += ' <br />' + %s;\n" % self.subtitle)
            if self.x_axis_label is not None and isinstance(self.x_axis_label, str) and len(self.x_axis_label):
                push("  options.axes.xaxis.label = \"")
                push(self.x_axis_label)
                push("\";\n")

            if self.y_axis_label is not None and isinstance(self.y_axis_label, str) and len(self.y_axis_label):
                push("  options.axes.yaxis.label = \"")
                push(self.y_axis_label)
                push("\";\n")
            push("$.jqplot.config.enablePlugins = true;\n")
            push("$(document).ready(function() {var plot = $.jqplot('")
            push(chart_id)
            push("', [data], options);\n"
                 "plot.replot();"
                 "var timer;"
                 " $(window).resize(function () {"
                 "clearTimeout(timer);"
                 "timer = setTimeout(function () {"
                 "plot.replot();"
                 "}, 100);"
                 "});"
                 "});\n")
            push("});\n")
            push(SCRIPT("".join(retval), _id="source"))
            return retval
