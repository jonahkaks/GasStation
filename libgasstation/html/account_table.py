# import datetime
#
# from libgasstation.html.stylesheets.text import *
#
#
# class HtmlAccountTable:
#     def __init__(self, matrix=None, env=None, accounts=None):
#         self.matrix = matrix if matrix is not None else TABLE()
#         self.env = env
#         if accounts is not None:
#             self.add_accounts(accounts)
#
#     @staticmethod
#     def account_path_less_p(a, b):
#         return a.get_full_name()<b.get_full_name()
#
#     def add_accounts(self, accounts):
#         def get_val(alist, key):
#             lst = alist.get(key)
#             return lst[0]
#
#         # def add_row(env):
#         #     table =
#         #     (row (gnc:html-table-num-rows html-table)))
#         #     (gnc:html-table-set-cell! html-table row 0 env)
#         #     row))
#         #
#         #
#         # def append-to-row row env)
#         # (gnc:html-acct-table-set-row-env! acct-table row
#         # (append (gnc:html-acct-table-get-row-env acct-table row) env)))
#         #
#         env = self.env
#         depth_limit = get_val(env, "display-tree-depth")
#         limit_behavior = get_val(env, "depth-limit-behavior")  or "summarize"
#         indent= get_val(env, "initial_indent") or 0
#         # (less_p (let ((pred=get_val(env, "account_less_p")))
#         # (if (eq? pred #t) gnc:account_code_less_p pred)))
#         start_date = get_val(env, "start_date")
#         end_date = get_val(env, "end_date") or datetime.date.today()
#         report_commodity = get_val(env, "report_commodity") or Session.get_current_book().get_default_currency()
#         exchange_fn=get_val(env, "exchange_fn")
#         get_balance_fn=get_val(env, "get_balance_fn")
#         column_header=  (let ((cell=get_val(env, "column_header)))
#         (if (eq? cell #t)
#         (gnc:make_html_table_cell "Account name")
#         cell)))
#         (subtotal_mode=get_val(env, "parent_account_subtotal_mode))
#         (zero_mode (let ((mode=get_val(env, "zero_balance_mode)))
#         (if (boolean? mode) 'show_leaf_acct mode)))
#         (label_mode (or=get_val(env, "account_label_mode) 'anchor))
#         (balance_mode (or=get_val(env, "balance_mode") 'post_closing))
#         (closing_pattern (or=get_val(env, "closing_pattern)
#         (list
#         (list 'str (G_ "Closing Entries"))
#         (list 'cased #f)
#         (list 'regexp #f)
#         (list 'closing #t))))
#         (report_budget (or=get_val(env, "report_budget) #f))
#         ;; local variables
#         (toplvl_accts
#         (gnc_account_get_children_sorted (gnc_get_current_root_account)))
#         (acct_depth_reached 0)
#         (logi_depth_reached (if depth_limit (_ depth_limit 1) 0))
#         (disp_depth_reached 0)
#         )
#         def calculate_balances accts start_date end_date get_balance_fn)
#         (define ret_hash (make_hash_table))
#         def calculate_balances_helper)
#         (for_each
#         (lambda (acct)
#         (hash_set! ret_hash (gncAccountGetGUID acct)
#         (get_balance_fn acct start_date end_date)))
#         accts))
#
#         def calculate_balances_simple)
#         def merge_splits splits subtract?)
#         (for_each
#         (lambda (split)
#         (let* ((acct (xaccSplitGetAccount split))
#         (guid (gncAccountGetGUID acct))
#         (acct_comm (xaccAccountGetCommodity acct))
#         (shares (xaccSplitGetAmount split))
#         (hash (hash_ref ret_hash guid)))
#         (unless hash
#         (set! hash (gnc:make_commodity_collector))
#         (hash_set! ret_hash guid hash))
#         (hash 'add acct_comm (if subtract? (_ shares) shares))))
#         splits))
#
#         (merge_splits (gnc:account_get_trans_type_splits_interval
#         accts #f start_date end_date)
#         #f)
#
#         (case balance_mode
#         ((post_closing) #f)
#
#         ;; remove closing entries
#         ((pre_closing)
#          (merge_splits (gnc:account_get_trans_type_splits_interval
#         accts closing_pattern start_date end_date) #t))
#
#         (else
#          (display "you fail it\n"))))
#
#         (if get_balance_fn
#          (calculate_balances_helper)
#         (calculate_balances_simple))
#         ret_hash)
#
#         def traverse_accounts! accts acct_depth logi_depth new_balances)
#
#         def use_acct(acct):
#             (and (or (eq? limit_behavior 'flatten)
#              (< logi_depth depth_limit))
#             (member acct accounts)))
#
#
#         def get_balance acct_balances acct)
#             (let ((this_collector (gnc:make_commodity_collector))
#              (acct_coll (hash_ref acct_balances (gncAccountGetGUID acct)
#             (gnc:make_commodity_collector))))
#             (this_collector 'merge acct_coll #f)
#             this_collector))
#
#         def get_balance_sub acct_balances account)
#         (let ((this_collector (gnc:make_commodity_collector)))
#         (for_each
#         (lambda (acct)
#          (this_collector 'merge (get_balance acct_balances acct) #f))
#          (gnc:accounts_and_all_descendants (list account)))
#         this_collector))
#
#         (let lp ((accounts (if less_p (sort accts less_p) accts))
#         (row_added? #f)
#         (disp_depth (if (integer? depth_limit)
#         (min (1_ depth_limit) logi_depth)
#         logi_depth)))
#
#         (cond
#
#          ((null? accounts) row_added?)
#
#             (else
#         (let* ((acct (car accounts))
#         (subaccts (gnc_account_get_children_sorted acct))
#
#         ;; These next two are commodity_collectors.
#         (account_bal (get_balance new_balances acct))
#         (recursive_bal (get_balance_sub new_balances acct))
#
#         ;; These next two are of type <gnc:monetary>
#                                            (report_comm_account_bal
#                                            (gnc:sum_collector_commodity
#                                             account_bal report_commodity exchange_fn))
#         (report_comm_recursive_bal
#         (gnc:sum_collector_commodity
#          recursive_bal report_commodity exchange_fn))
#
#         (grp_env
#         (cons*
#          (list 'initial_indent indent)
#          (list 'account acct)
#          (list 'account_name (xaccAccountGetName acct))
#          (list 'account_code (xaccAccountGetCode acct))
#          (list 'account_type (xaccAccountGetType acct))
#          (list 'account_type_string (xaccAccountGetTypeStr
#          (xaccAccountGetType acct)))
#         (list 'account_guid (gncAccountGetGUID acct))
#         (list 'account_description (xaccAccountGetDescription acct))
#         (list 'account_notes (xaccAccountGetNotes acct))
#         (list 'account_path (gnc_account_get_full_name acct))
#         (list 'account_parent (gnc_account_get_parent acct))
#         (list 'account_children subaccts)
#         (list 'account_depth acct_depth)
#         (list 'logical_depth logi_depth)
#         (list 'account_commodity (xaccAccountGetCommodity acct))
#         (list 'account_anchor (gnc:html_account_anchor acct))
#         (list 'account_bal account_bal)
#         (list 'recursive_bal recursive_bal)
#         (list 'report_comm_account_bal report_comm_account_bal)
#         (list 'report_comm_recursive_bal report_comm_recursive_bal)
#         (list 'report_commodity report_commodity)
#         (list 'exchange_fn exchange_fn)
#         env))
#         (label (case label_mode
#         ((anchor) (gnc:html_account_anchor acct))
#         ((name) (gnc:make_html_text (xaccAccountGetName acct)))))
#         (row #f)
#          (children_displayed? #f))
#
#          (set! acct_depth_reached (max acct_depth_reached acct_depth))
#         (set! logi_depth_reached (max logi_depth_reached logi_depth))
#         (set! disp_depth_reached (max disp_depth_reached disp_depth))
#
#         (unless (or (not (use_acct? acct))
#          ;; ok, so we'll consider parent accounts with zero
#          ;; recursive_bal to be zero balance leaf accounts
#          (and (gnc_commodity_collector_allzero? recursive_bal)
#         (eq? zero_mode 'omit_leaf_acct)
#                            (or (not report_budget)
#                        (zero? (gnc:budget_account_get_rolledup_net
#         report_budget acct #f #f)))))
#         (set! row
#         (add_row
#         (cons* (list 'account_label label)
#         (list 'row_type 'account_row)
#         (list 'display_depth disp_depth)
#         (list 'indented_depth (+ disp_depth indent))
#         grp_env))))
#
#         ;; Recurse:
#         ;; Dive into an account even if it isn't selected! \
#         ;; why? because some subaccts may be selected.
#         (set! children_displayed?
#         (traverse_accounts! subaccts
#         (1+ acct_depth)
#             (if (use_acct? acct)
#         (1+ logi_depth)
#         logi_depth)
#         new_balances))
#
#         ;; record whether any children were displayed
#         (when row
#         (append_to_row
#         row (list (list 'children_displayed? children_displayed?)))) \
#
#         ;; after the return from recursion: subtotals
#         (unless (or (not (use_acct? acct))
#         (not subtotal_mode)
#         (not children_displayed?)
#             (and (gnc_commodity_collector_allzero? recursive_bal)
#         (eq? zero_mode 'omit_leaf_acct)))
#         (let ((lbl_txt (gnc:make_html_text (G_ "Total") " ")))
#         (apply gnc:html_text_append! lbl_txt (gnc:html_text_body label))
#         (set! disp_depth_reached (max disp_depth_reached disp_depth))
#         (add_row
#         (cons* (list 'account_label lbl_txt)
#         (list 'row_type 'subtotal_row)
#         (list 'display_depth disp_depth)
#          (list 'indented_depth (+ disp_depth indent))
#          grp_env))))
#
#         (lp (cdr accounts)
#             (or row_added? children_displayed? row)
#         disp_depth))))))
#
#         ;; do it
#         (traverse_accounts!
#         toplvl_accts 0 0
#         (calculate_balances accounts start_date end_date get_balance_fn))
#
#         ;; now set the account_colspan entries
#         (let lp ((row 0)
#                  (rows (gnc:html_acct_table_num_rows acct_table)))
#         (when (< row rows)
#         (let* ((orig_env (gnc:html_acct_table_get_row_env acct_table row))
#         (display_depth (get_val orig_env 'display_depth))
#         (depth_limit (get_val orig_env 'display_tree_depth))
#         (indent (get_val orig_env 'initial_indent))
#         (label_cols (+ disp_depth_reached 1))
#         ;; these parameters *should* always, by now, be set...
#         (new_env
#         (cons*
#          (list 'account_colspan (_ label_cols display_depth))
#          (list 'label_cols label_cols)
#          (list 'account_cols (+ indent (max label_cols (or depth_limit 0))))
#          (list 'logical_cols (min (+ logi_depth_reached)
#          (or depth_limit +inf.0)))
#         orig_env)))
#         (gnc:html_acct_table_set_row_env! acct_table row new_env)
#         (lp (1+ row) rows))))))
#
#     def num_rows(self)
#         (gnc:html-table-num-rows (gnc:_html-acct-table-matrix_ acct-table)))
#
#     def get-cell acct-table row col)
#         ;; we'll only ever store one object in an html-table-cell \
#         ;; returns the first object stored in that cell
#                                                        (and-let* ((cell (gnc:html-table-get-cell
#         (gnc:_html-acct-table-matrix_ acct-table) row (1+ col))))
#         (car (gnc:html-table-cell-data cell))))
#
#     def set-cell! acct-table row col obj)
#         (gnc:html-table-set-cell!
#         (gnc:_html-acct-table-matrix_ acct-table)
#         row (+ col 1)
#         obj))
#
#     def get-row-env acct-table row)
#         (gnc:html-acct-table-get-cell acct-table row -1)
#         )
#
#     def set-row-env! acct-table row env)
#         (gnc:html-acct-table-set-cell! acct-table row -1 env))
#
#         ;;
#         ;; Here are some standard functions to help process gnc:html-acct-tables.
#         ;;
#
#         def gnc:html-make-nbsps n)
#         (let lp ((n n) (res '()))
#             (if (positive? n)
#         (lp (1- n) (cons "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" res))
#         (string-join res ""))))
#
#
#     def add-labeled-amount-line(
#
#             html-table
#         table-width       ;; if #f defaults to (amount-depth + amount-colspan)
#         row-markup        ;; optional
#         total-rule?       ;; Place an <hr> in the cell previous to label?
#         label             ;; the actual label text
#         label-depth       ;; defaults to zero
#         label-colspan     ;; defaults to one
#         label-markup      ;; optional
#         amount            ;; a <gnc:monetary> or #f
#                                     amount-depth      ;; defaults to (label-depth + label-colspan)
#         amount-colspan    ;; defaults to one
#         amount-markup)    ;; optional
#         (let* ((lbl-depth (or label-depth 0))
#         (lbl-colspan 1)
#         (amt-depth (or amount-depth (+ lbl-depth lbl-colspan)))
#         (amt-colspan 1)
#         (tbl-width (or table-width (+ amt-depth amt-colspan)))
#         (row
#          (append
#           (list
#                (if label-markup                      ;; the actual label
#          (gnc:make-html-table-cell/size/markup
#          1 1 label-markup (gnc:make-html-text (gnc:html-make-nbsps lbl-depth)) label)
#         (gnc:make-html-table-cell/size
#         1 1 (gnc:make-html-text (gnc:html-make-nbsps lbl-depth)) label))
#         )
#         (gnc:html-make-empty-cells             ;; padding after label
#         (+ (- amt-depth (floor (/ tbl-width 2)))
#         (if total-rule? -1 0)
#         )
#         )
#         (if total-rule?                        ;; include <hr>?
#          (list (gnc:make-html-table-cell
#          (gnc:make-html-text (gnc:html-markup-hr))))
#         (list)
#         )
#         (list
#              (if amount-markup                     ;; the amount
#          (gnc:make-html-table-cell/size/markup
#          1 amt-colspan amount-markup amount)
#         (gnc:make-html-table-cell/size
#         1 amt-colspan amount))
#         )
#         (gnc:html-make-empty-cells             ;; padding out to full width
#         (- tbl-width (+ amt-depth amt-colspan)))
#         )
#         ) ;; end of row
#         )
#         (if row-markup
#          (gnc:html-table-append-row/markup! html-table row-markup row)
#         (gnc:html-table-append-row! html-table row))))
#
#         def gnc-commodity-table amount report-commodity exchange-fn)
#
#         (let* ((table (gnc:make-html-table))
#         (spacer (gnc:make-html-table-cell))
#         (list-of-balances (amount 'format gnc:make-gnc-monetary #f)))
#         (gnc:html-table-cell-set-style! spacer "td"
#         'attribute (list "style" "min-width: 1em"))
#         (for-each
#         (lambda (bal)
#         (gnc:html-table-append-row!
#         table (list (gnc:make-html-table-cell/markup "number-cell" bal)
#         spacer
#         (gnc:make-html-table-cell/markup
#         "number-cell" (exchange-fn bal report-commodity)))))
#         list-of-balances)
#         (gnc:html-table-set-style! table "table"
#         'attribute (list "style" "width:100%; max-width:20em")
#         'attribute (list "cellpadding" "0"))
#         table))
#
#    def add-account-balances
#         html-table  ;; can be #f to create a new table
#         acct-table
#         params)
#         (let* ((num-rows (gnc:html-acct-table-num-rows acct-table))
#         (rownum 0)
#         (html-table (or html-table (gnc:make-html-table)))
#         (get-val (lambda (alist key)
#         (let ((lst (assoc-ref alist key)))
#             (if lst (car lst) lst))))
#         )
#
#         (while (< rownum num-rows)
#             (let* ((env (append
#                          (gnc:html-acct-table-get-row-env acct-table rownum)
#                 params))
#             (acct (get-val env 'account))
#             (children (get-val env 'account-children))
#             (label (get-val env 'account-label))
#             (acct-name (get-val env 'account-name)) ;; for diagnostics...
#             (report-commodity  (get-val env 'report-commodity))
#             (exchange-fn (get-val env 'exchange-fn))
#             (account-cols (get-val env 'account-cols))
#             (logical-cols (get-val env 'logical-cols))
#             (label-cols (get-val env 'label-cols))
#             (logical-depth (get-val env 'logical-depth))
#             (display-depth (get-val env 'display-depth))
#             (display-tree-depth (get-val env 'display-tree-depth))
#             (row-type (get-val env 'row-type))
#             (rule-mode (and (equal? row-type 'subtotal-row)
#             (get-val env 'rule-mode)))
#             (row-markup (and (equal? row-type 'subtotal-row)
#             "primary-subheading"))
#             (multicommodity-mode (get-val env 'multicommodity-mode))
#             (limit-behavior
#                 (or (get-val env 'depth-limit-behavior)
#             'summarize))
#             (parent-acct-bal-mode
#             (or (get-val env 'parent-account-balance-mode)
#             'omit-bal))
#
#             (bal-method
#             ;; figure out how to calculate our balance:
#             ;; 'immediate-bal|'recursive-bal ('omit-bal handled below)
#             (cond ((eq? row-type 'subtotal-row) 'recursive-bal)
#              ((eq? (1+ display-depth) display-tree-depth)
#             (cond ((eq? limit-behavior 'summarize) 'recursive-bal)
#                    ((null? children) 'immediate-bal)
#              (else parent-acct-bal-mode)))
#             ((not (null? children)) parent-acct-bal-mode)
#                 (else 'immediate-bal)))
#
#             (zero-mode (let ((mode (get-val env 'zero-balance-display-mode)))
#             (if (boolean? mode) 'show-balance mode)))
#
#             (amt (and-let* ((bal-syms '((immediate-bal . account-bal)
#             (recursive-bal . recursive-bal)
#                             (omit-bal . #f)))
#                             (bal-sym (assq-ref bal-syms bal-method))
#             (comm-amt (get-val env bal-sym)))
#             (cond
#              ((and (eq? zero-mode 'omit-balance)
#                (gnc-commodity-collector-allzero? comm-amt)) #f)
#              ((gnc-reverse-balance acct)
#               (gnc:commodity-collector-get-negated comm-amt))
#             (else comm-amt))))
#
#             (amount
#              (cond
#               ((not amt) #f)
#                ((and (not (gnc:uniform-commodity? amt report-commodity))
#                (eq? multicommodity-mode 'table)
#              (eq? row-type 'account-row))
#              (gnc-commodity-table amt report-commodity exchange-fn))
#             (else
#              (gnc:sum-collector-commodity amt report-commodity exchange-fn))))
#
#             (indented-depth (get-val env 'indented-depth))
#             (account-colspan (get-val env 'account-colspan))
#              )
#
#             ;; for each row do:
#
#                 (gnc:html-table-add-labeled-amount-line!
#                 html-table
#                 (+ account-cols logical-cols) ;; table-width
#             row-markup                    ;; row-markup
#             rule-mode
#             label
#             indented-depth
#             account-colspan               ;; label-colspan
#             "anchor-cell"                 ;; label-markup
#             amount
#             (+ account-cols (- 0 1)
#             (- logical-cols display-depth)
#             )                          ;; amount-depth
#             1                             ;; amount-colspan
#             "number-cell"                 ;; amount-markup
#             )
#
#             (set! rownum (+ rownum 1)) ;; increment rownum
#             )
#             ) ;; end of while
#         html-table
#         )
#         )
