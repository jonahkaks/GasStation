from .chart import *


class HtmlBarChart(HtmlChart):
    __type__ = "bar"

    def __init__(self):
        super().__init__()
        self.x_axis_label = None
        self.y_axis_label = None
        self.row_labels = []
        self.col_labels = []
        self.legend_reversed = False
        self.row_labels_rotated = False
        self.stacked = False
        self.markers = True
        self.minor_grid = True
        self.major_grid = True
        self.bar_width = 1.5
        self.currency_iso = None
        self.currency_symbol = None
        self.button_1_bar_urls = None
        self.button_2_bar_urls = None
        self.button_3_bar_urls = None

    def get_button_1_bar_urls(self):
        return self.button_1_bar_urls

    def get_button_2_bar_urls(self):
        return self.button_2_bar_urls

    def get_button_3_bar_urls(self):
        return self.button_3_bar_urls

    def set_x_axis_label(self, lab):
        self.x_axis_label = lab

    def set_y_axis_label(self, lab):
        self.y_axis_label = lab

    def get_x_axis_label(self):
        return self.x_axis_label

    def get_y_axis_label(self):
        return self.y_axis_label

    def set_row_labels(self, labs: list):
        self.row_labels[:] = labs

    def get_row_labels(self):
        return self.row_labels

    def set_col_labels(self, labs: list):
        self.col_labels[:] = labs

    def get_col_labels(self):
        return self.col_labels

    def get_row_labels_rotated(self):
        return self.row_labels_rotated

    def set_row_labels_rotated(self, rotated: bool = False):
        self.row_labels_rotated = rotated

    def get_stacked(self):
        return self.stacked

    def set_stacked(self, stacked: bool = False):
        self.stacked = stacked

    def get_minor_grid(self) -> bool:
        return self.minor_grid

    def set_minor_grid(self, minor: bool = False):
        self.minor_grid = minor

    def get_major_grid(self) -> bool:
        return self.major_grid

    def set_major_grid(self, major: bool = False):
        self.major_grid = major

    def set_legend_reversed(self, rev=True):
        self.legend_reversed = rev

    def get_legend_reversed(self):
        return self

    def set_bar_width(self, width):
        self.bar_width = width

    def get_bar_width(self):
        return self.bar_width

    def append_row(self, row):
        self.data.append(row)

    def prepend_row(self, row):
        self.data.insert(0, row)
