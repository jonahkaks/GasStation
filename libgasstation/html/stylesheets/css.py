from libgasstation.core.options import NewOptions, TextOption
from libgasstation.html.stylesheets.stylesheet import HtmlStyleSheetTemplate, HtmlStyleSheet
from libgasstation.html.stylesheets.text import H3

default_css = """
@media (prefers-color-scheme: dark) {
    body {
        color: #000; background-color: #fff;
    }
}

html, body {
    height: 100vh;
    margin: 0;
    font-family: \"Noto Sans\", Sans-Serif;
    font-size: 10pt;
}


body, p, table, tr, td, a, th {
    vertical-align: top;
}
table *{
border-width: 1px; border-style:solid; border-collapse: collapse
}

h3 {
    font-size: 15pt;
    font-weight: bold;
}

a {
    font-style: italic;
}

/* table elements as follows */
td, th {
    padding:4px;
}

tr {
    page-break-inside: avoid !important;
}

td, th {
    border-color: grey
}

td.total-number-cell, td.total-label-cell, td.centered-label-cell {
    font-size: 12pt;
    font-weight: bold;
}

th.column-heading-left {
    text-align: left;
}

td.centered-label-cell, th.column-heading-center {
    text-align: center;
}

td.number-header, th.column-heading-right, td.number-cell, td.total-number-cell {
    text-align: right;
}

td.neg {
    color: red;
}

td.number-cell, td.total-number-cell, td.anchor-cell, td.date-cell {
    white-space: nowrap;
}

td.highlight {
    background-color: #e1e1e1
}"""


class CssStyleSheet(HtmlStyleSheetTemplate):
    def __init__(self):
        super().__init__("CSS")

    def generate_options(self):
        self.options = NewOptions()
        self.options.register_option(TextOption("General", "CSS", "a",
                                                "This field specifies the CSS code for styling reports.", default_css))

    def renderer(self, doc):
        from libgasstation.html.document import HtmlDocument
        ssdoc = HtmlDocument()
        css = self.options.lookup_option("General", "CSS").get_value()
        report_css = doc.get_style_text()
        if report_css is None:
            report_css = ""
        all_css = "\n".join([css, report_css])
        headline = doc.get_headline() or doc.get_title()
        ssdoc.set_style("column-heading-left", "th", {"class": "column-heading-left"})
        ssdoc.set_style("column-heading-center", "th", {"class": "column-heading-center"})
        ssdoc.set_style("column-heading-right", "th", {"class": "column-heading-right"})
        ssdoc.set_style("date-cell", "td", {"class": "date-cell"})
        ssdoc.set_style("anchor-cell", "td", {"class": "anchor-cell"})
        ssdoc.set_style("number-cell", "td", {"class": "number-cell"})
        ssdoc.set_style("number-cell-neg", "td", {"class": "number-cell neg"})
        ssdoc.set_style("number-header", "th", {"class": "number-header"})
        ssdoc.set_style("text-cell", "td", {"class": "text-cell"})
        ssdoc.set_style("total-number-cell", "td", {"class": "total-number-cell"})
        ssdoc.set_style("total-number-cell-neg", "td", {"class": "total-number-cell neg"})
        ssdoc.set_style("total-label-cell", "td", {"class": "total-label-cell"})
        ssdoc.set_style("centered-label-cell", "td", {"class": "centered-label-cell"})
        ssdoc.set_style("normal-row", "tr")
        ssdoc.set_style("alternate-row", "tr")
        ssdoc.set_style("primary-subheading", "tr")
        ssdoc.set_style("secondary-subheading", "tr")
        ssdoc.set_style("grand-total", "tr")
        if all_css.find("</style>") != -1:
            ssdoc.set_style(default_css)
        else:
            ssdoc.set_style_text(all_css)
        if headline is not None and headline != "":
            ssdoc.add_object(H3(headline))
        ssdoc.append_objects(doc.get_objects())
        return ssdoc


HtmlStyleSheet("CSS-based stylesheet (experimental)", CssStyleSheet())
