from .stylesheet import *
from .text import *

if os.name == "posix":
    D_FMT = locale.nl_langinfo(locale.D_FMT)
    D_T_FMT = locale.nl_langinfo(locale.D_T_FMT)
    T_FMT = locale.nl_langinfo(locale.T_FMT)
else:
    D_FMT = '%d/%m/%y'
    D_T_FMT = '%a %b %e %H:%M:%S %Y'
    T_FMT = '%H:%M:%S'


class HeadTailStyleSheet(HtmlStyleSheetTemplate):
    def __init__(self):
        super().__init__("Head or Tail")

    def generate_options(self):
        self.options = NewOptions()
        opt_reg = self.options.register_option
        opt_reg(StringOption(
            "General",
            "Preparer", "a",
            "Name of person preparing the report.",
            ""))
        opt_reg(StringOption(
            "General",
            "Prepared for", "b",
            "Name of organization or company prepared for.",
            ""))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show preparer info", "c",
                "Name of organization or company.",
                True))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show receiver info", "d",
                "Name of organization or company the report is prepared for.",
                True))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show date", "e",
                "The creation date for this report.",
                True))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show time in Addition to date", "f",
                "The creation time for this report can only be shown if the date is shown.",
                True))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show GasStation Version", "g",
                "Show the currently used GasStation version.",
                True))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Enable Links", "h",
                "Enable hyperlinks in reports.",
                True))

        opt_reg(
            TextOption(
                "General",
                "Additional Comments", "i",
                "String for additional report information.",
                ""))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show preparer info at bottom", "j",
                "Per default the preparer info will be shown before the report data.",
                False))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show receiver info at bottom", "k",
                "Per default the receiver info will be shown before the report data.",
                False))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show date/time at bottom", "l",
                "Per default the date/time info will be shown before the report data.",
                False))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show comments at bottom", "m",
                "Per default the additional comments text will be shown before the report data.",
                False))
        opt_reg(
            SimpleBooleanOption(
                "General",
                "Show GasStation version at bottom", "m",
                "Per default the GasStation version will be shown before the report data.",
                False))

        opt_reg(PixMapOption("Images", "Background Tile", "a", "Background tile for reports.", ""))
        opt_reg(PixMapOption("Images", "Heading Banner", "b", "Banner for top of report.", ""))
        opt_reg(MultiChoiceOption("Images", "Heading Alignment", "c", "Banner for top of report.",
                                  "left", [("left", "Left", "Align the banner to the left."),
                                           ("center", "Center", "Align the banner in the center."),
                                           ("right", "Right", "Align the banner to the right.")]))
        opt_reg(PixMapOption("Images", "Logo", "d", "Company logo image.", ""))

        opt_reg(ColorOption("Colors", "Background Color", "a", "General background color for report.",
                            [0xff, 0xff, 0xff, 0xff],
                            255, False))
        opt_reg(ColorOption("Colors",
                            "Text Color", "b", "Normal body text color.",
                            [0x00, 0x00, 0x00, 0xff],
                            255, False))

        opt_reg(ColorOption("Colors",
                            "Link Color", "c", "Link text color.",
                            [0xb2, 0x22, 0x22, 0xff],
                            255, False))

        opt_reg(ColorOption("Colors",
                            "Table Cell Color", "c", "Default background for table cells.",
                            [0xff, 0xff, 0xff, 0xff],
                            255, False))

        opt_reg(ColorOption("Colors",
                            "Alternate Table Cell Color", "d",
                            "Default alternate background for table cells.",
                            [0xff, 0xff, 0xff, 0xff],
                            255, False))

        opt_reg(ColorOption("Colors",
                            "Subheading/Subtotal Cell Color", "e",
                            "Default color for subtotal rows.",
                            [0xee, 0xe8, 0xaa, 0xff],
                            255, False))

        opt_reg(ColorOption("Colors",
                            "Sub-subheading/total Cell Color", "f",
                            "Color for subsubtotals.",
                            [0xfa, 0xfa, 0xd2, 0xff],
                            255, False))

        opt_reg(ColorOption("Colors",
                            "Grand Total Cell Color", "g",
                            "Color for grand totals.",
                            [0xff, 0xff, 0x00, 0xff],
                            255, False))
        opt_reg(NumberRangeOption("Tables", "Table cell spacing", "a", "Space between table cells.",
                                  1, 0, 20, 0, 1))

        opt_reg(NumberRangeOption("Tables", "Table cell padding", "b", "Space between table cell edge and content.",
                                  1, 0, 20, 0, 1))

        opt_reg(NumberRangeOption("Tables", "Table border width", "c", "Bevel depth on tables.",
                                  1, 0, 20, 0, 1))
        self.register_font_options()

    def renderer(self, doc):
        from libgasstation.html.document import HtmlDocument
        ssdoc = HtmlDocument()
        color_val = lambda section, name: self.options.lookup_option(section, name).html()
        opt_val = lambda section, name: self.options.lookup_option(section, name).get_value()
        preparer = opt_val("General", "Preparer")
        prepared_for = opt_val("General", "Prepared for")
        show_preparer = opt_val("General", "Show preparer info")
        show_receiver = opt_val("General", "Show receiver info")
        show_date = opt_val("General", "Show date")
        show_time = opt_val("General", "Show time in Addition to date")
        show_gasstation_version = opt_val("General", "Show GasStation Version")
        show_preparer_at_bottom = opt_val("General", "Show preparer info at bottom")
        show_receiver_at_bottom = opt_val("General", "Show receiver info at bottom")
        show_date_time_at_bottom = opt_val("General", "Show date/time at bottom")
        show_comments_at_bottom = opt_val("General", "Show comments at bottom")
        show_gasstation_version_at_bottom = opt_val("General", "Show GasStation version at bottom")
        links = opt_val("General", "Enable Links")
        additional_comments = opt_val("General", "Additional Comments")
        bgcolor = color_val("Colors", "Background Color")
        textcolor = color_val("Colors", "Text Color")
        link_color = color_val("Colors", "Link Color")
        normal_row_color = color_val("Colors", "Table Cell Color")
        alternate_row_color = color_val("Colors", "Alternate Table Cell Color")
        primary_subheading_color = color_val("Colors", "Subheading/Subtotal Cell Color")
        secondary_subheading_color = color_val("Colors", "Sub-subheading/total Cell Color")
        grand_total_color = color_val("Colors", "Grand Total Cell Color")
        bgpixmap = opt_val("Images", "Background Tile")
        headpixmap = opt_val("Images", "Heading Banner")
        logopixmap = opt_val("Images", "Logo")
        align = opt_val("Images", "Heading Alignment")
        spacing = opt_val("Tables", "Table cell spacing")
        padding = opt_val("Tables", "Table cell padding")
        border = opt_val("Tables", "Table border width")
        headcolumn = 0

        ssdoc.set_style("body", {"bgcolor": bgcolor, "text": textcolor, "link": link_color})
        ssdoc.set_style("column_heading_left", "th", {"class": "column_heading_left"})
        ssdoc.set_style("column_heading_center", "th", {"class": "column_heading_center"})
        ssdoc.set_style("column_heading_right", "th", {"class": "column_heading_right"})
        ssdoc.set_style("date-cell", "td", {"class": "date-cell"})
        ssdoc.set_style("anchor-cell", "td", {"class": "anchor-cell"})
        ssdoc.set_style("number-cell", "td", {"class": "number-cell"})
        ssdoc.set_style("number-cell-neg", "td", {"class": "number-cell-neg"})
        ssdoc.set_style("number_header", "th", {"class": "number-header"})
        ssdoc.set_style("text-cell", "td", {"class": "text-cell"})
        ssdoc.set_style("total-number-cell", "td", {"class": "total-number-cell"})
        ssdoc.set_style("total-number-cell-neg", "td", {"class": "total-number-cell-neg"})
        ssdoc.set_style("total-label-cell", "td", {"class": "total-label-cell"})
        ssdoc.set_style("centered-label-cell", "td", {"class": "centered-label-cell"})
        if bgpixmap is not None and isinstance(bgpixmap, str) and len(bgpixmap) > 0:
            ssdoc.set_style("body", {"background": bgpixmap, "class": "bg"})
        ssdoc.set_style("table", {"_border": border, "_cellspacing": spacing, "_cellpadding": padding,
                                  "_style": "border-width: 1px; border-style:solid; border-collapse: collapse"})
        ssdoc.set_style("normal-row", {"bgcolor": normal_row_color}, "tr")
        ssdoc.set_style("alternate-row", {"bgcolor": alternate_row_color}, "tr")
        ssdoc.set_style("primary-subheading", {"bgcolor": primary_subheading_color}, "tr")
        ssdoc.set_style("secondary-subheading", {"bgcolor": secondary_subheading_color}, "tr")
        ssdoc.set_style("grand-total", {"bgcolor": grand_total_color}, "tr")
        if not links:
            ssdoc.set_style("a", "")
        doc.set_style_text("""body, html {
          height: 100%;
        }
        .logo {
         width: 100px; 
         height: 100px; 
         display: block; 
         text-indent: -9999px;
         background:inherit;
        }
        .bg{
          height: 100%;
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
          background-attachment: fixed;
          overflow:hidden;
        }""")
        self.add_css_information_to_doc(ssdoc, doc)
        t = TABLE()
        t.set_style("table", {"_style": "margin-left:auto; margin-right:auto;", "_border": 0})

        headline = doc.get_headline() or doc.get_title()
        d = []
        push = d.append
        if headline is not None:
            push(H3(headline))
        if show_preparer and not show_preparer_at_bottom and preparer is not None and len(preparer):
            push(I("Prepared by: %s" % preparer))
            push(BR())
        if show_receiver and not show_receiver_at_bottom and prepared_for is not None and len(prepared_for):
            push(I("Prepared for: %s" % prepared_for))
            push(BR())
        if show_date and not show_date_time_at_bottom:
            push(I("Report Creation Date: %s" % datetime.datetime.now().strftime(D_FMT if show_time else D_T_FMT)))
            push(BR())
        if show_gasstation_version and not show_gasstation_version_at_bottom:
            push(I("GasStation Version 1.2"))
            push(BR())
        if not show_comments_at_bottom and additional_comments:
            push(BR())
            push(I(additional_comments))
            push(BR())
        if (show_preparer and not show_preparer_at_bottom) or (show_receiver and not show_receiver_at_bottom) or (
                show_date and not show_date_time_at_bottom) or (
                show_gasstation_version and not show_gasstation_version_at_bottom) or not show_comments_at_bottom:
            push(BR())
            push("")
        if logopixmap and len(logopixmap) > 0:
            headcolumn = 1
            t.append(TR(TD(IMG(_src=logopixmap, _class="logo"))))
        if headpixmap and len(headpixmap) > 0:
            t.append(TR(TD(IMG(_src=headpixmap), _style="align:%s" % align, _rowspan=1, colspan=1)))
        t.append(TR(TD(" "), TD(*d, _colspan=headcolumn)))
        t.append(TR(TD(" "), TD(*doc.get_objects(), _colspan=headcolumn)))

        d.clear()
        if show_preparer and show_preparer_at_bottom and preparer is not None and len(preparer):
            push(I("Prepared by: %s" % preparer))
            push(BR())
        if show_receiver and show_receiver_at_bottom and prepared_for is not None and len(prepared_for):
            push(I("Prepared for: %s" % prepared_for))
            push(BR())
        if show_date and show_date_time_at_bottom:
            push(I("Report Creation Date: %s" % datetime.datetime.now().strftime(D_FMT if show_time else D_T_FMT)))
            push(BR())
        if show_gasstation_version and show_gasstation_version_at_bottom:
            push(I("GasStation Version 1.2"))
            push(BR())
        if show_comments_at_bottom and additional_comments:
            push(BR())
            push(I(additional_comments))
            push(BR())
        if any(d):
            t.append(TR(TD(" "), TD(*d, _rowspan=1, _colspan=headcolumn)))
        ssdoc.add_object(t)
        return ssdoc


HtmlStyleSheet("Head or Tail", HeadTailStyleSheet())
