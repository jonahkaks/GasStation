import json

from libgasstation.core.hook import Hook, HOOK_SAVE_OPTIONS, HOOK_RESTORE_OPTIONS
from libgasstation.core.options import *


class HtmlStyleMarkupInfo:
    def __init__(self, *args):
        self.tag = None
        self.attributes = {}
        self.font_face = None
        self.font_size = None
        self.font_color = None
        self.closing_font_tag = None
        self.inheritable = False
        for s in args:
            if isinstance(s, dict):
                self.attributes.update(s)
            else:
                self.tag = s

    def __repr__(self):
        return u"HtmlStyleMarkupInfo<tag={} {}>".format(self.tag, self.attributes)

    @staticmethod
    def merge(self, other):
        if not isinstance(self, HtmlStyleMarkupInfo):
            return other
        elif not isinstance(other, HtmlStyleMarkupInfo):
            return self
        else:
            info = HtmlStyleMarkupInfo(self.tag or other.tag)
            info.attributes = {}
            info.attributes.update(other.attributes)
            info.attributes.update(self.attributes)
            info.font_face = self.font_face or other.font_face
            info.font_size = self.font_size or other.font_size
            info.closing_font_tag = self.closing_font_tag or other.closing_font_tag
            info.font_color = self.font_color or other.font_color
            info.inheritable = self.inheritable
            return info


class HtmlStyleDataInfo:
    def __init__(self, data, renderer=None):
        self.data = data
        self.renderer = renderer
        self.inheritable = False

    @staticmethod
    def merge(self, other):
        if isinstance(self, HtmlStyleDataInfo):
            return self
        else:
            return other


def style_info_merge(s1, s2):
    if isinstance(s1, HtmlStyleMarkupInfo) or isinstance(s2, HtmlStyleMarkupInfo):
        return HtmlStyleMarkupInfo.merge(s1, s2)
    elif isinstance(s1, HtmlStyleDataInfo) or isinstance(s2, HtmlStyleDataInfo):
        return HtmlStyleDataInfo.merge(s1, s2)
    return None


class HtmlStyleTable:
    def __init__(self):
        self.primary = {}
        self.compiled = {}
        self.inheritable = {}

    def get_primary(self):
        return self.primary

    def set_primary(self, primary):
        self.primary = primary

    def get_compiled(self):
        return self.compiled

    def set_compiled(self, compiled):
        self.compiled = compiled

    def get_inheritable(self):
        return self.inheritable

    def set_inheritable(self, inh):
        self.inheritable = inh

    def is_inheritable(self):
        return self.inheritable is not None

    def set(self, markup, style_info):
        self.primary[markup] = style_info

    def uncompile(self):
        self.compiled = None
        self.inheritable = None

    def compile(self, antecedents):
        def key_merger(key, value):
            new_val = style_info_merge(self.compiled.get(key), value)
            self.compiled[key] = new_val
            if isinstance(value, (HtmlStyleMarkupInfo, HtmlStyleDataInfo)) and value.inheritable:
                self.inheritable[key] = new_val

        def compile_worker(table_list):
            for t in table_list:
                if t.compiled is not None and len(t.compiled):
                    b = t.compiled
                else:
                    b = t.primary
                for key, val in b.items():
                    key_merger(key, val)

        self.compiled = {}
        self.inheritable = {}
        for key, value in self.primary.items():
            key_merger(key, value)
        if len(antecedents):
            compile_worker(antecedents)

    def fetch(self, antecedents, markup):
        def get_inheritable_style(ht):
            s = ht.get(markup)
            if isinstance(s, (HtmlStyleDataInfo, HtmlStyleMarkupInfo)) and s.inheritable:
                return s
            return None

        def fetch_worker(style, antecedents):
            if antecedents is None or len(antecedents) == 0:
                return style
            elif antecedents[0] is None:
                return fetch_worker(style, antecedents[1:])
            elif antecedents[0].compiled is not None and any(antecedents[0].compiled):
                return style_info_merge(style, antecedents[0].inheritable.get(markup))
            else:
                return fetch_worker(style_info_merge(style,
                                                     get_inheritable_style(antecedents[0].primary)), antecedents[1:])

        if self.compiled is not None and len(self.compiled):
            return self.compiled.get(markup)
        return fetch_worker(self.primary.get(markup), antecedents)

    def __repr__(self):
        return u"HtmlStyleTable<{}>".format(self.compiled)


class HtmlStyleSheetTemplate(object):
    templates = {}

    def __init__(self, name: str, version=1, *args, **kwargs):
        self.version = version
        self.name = name
        self.options = None
        self.generate_options()
        HtmlStyleSheetTemplate.templates[name] = self

    @staticmethod
    def font_name_to_style_info(font_name):
        font_name = font_name.lower()
        font_style = ""
        font_weight = ""
        font_name, font_size = font_name.rsplit(' ', 1)
        if font_name.__contains__(" bold"):
            font_weight = "font-weight: bold; "
            font_name = " bold"

        if font_name.__contains__(" italic"):
            font_style = "font-style: italic; "
            font_name = font_name + " italic"

        elif font_name.__contains__(" oblique"):
            font_style = "font-style: oblique; "
            font_name = font_name + " oblique"
        return "font-family:" + font_name + ", Sans-Serif; ""font-size: " + font_size + "pt; " + font_style + font_weight

    def register_font_options(self):
        opt_register = self.options.register_option
        font_family = "Ubuntu"
        opt_register(FontOption("Fonts", "Title", "a", "Font info for the report title.",
                                "".join([font_family, " Bold 15"])))
        opt_register(FontOption("Fonts", "Account link", "b", "Font info for account name.",
                                "".join([font_family, " Italic 10"])))
        opt_register(FontOption("Fonts", "Number cell", "c", "Font info for regular number cells.",
                                "".join([font_family, " 10"])))
        opt_register(SimpleBooleanOption("Fonts", "Negative Values in Red", "d", "Display negative values in red.",
                                         True))
        opt_register(FontOption("Fonts", "Number header", "e", "Font info for number headers.",
                                "".join([font_family, " 10"])))
        opt_register(FontOption("Fonts", "Text cell", "f", "Font info for regular text cells.",
                                "".join([font_family, " 10"])))
        opt_register(FontOption("Fonts", "Total number cell", "g", "Font info for number cells containing a total.",
                                "".join([font_family, " Bold 12"])))
        opt_register(FontOption("Fonts", "Total label cell", "h", "Font info for cells containing total labels.",
                                "".join([font_family, " Bold 12"])))
        opt_register(FontOption("Fonts", "Centered label cell", "i", "Font info for centered label cells.",
                                "".join([font_family, " Bold 12"])))

    def add_css_information_to_doc(self, ssdoc, doc):
        opt_font_value = lambda name: self.options.lookup_option("Fonts", name).get_value()
        opt_style_info = lambda name: self.font_name_to_style_info(opt_font_value(name))
        negative_red = opt_font_value("Negative Values in Red")
        alternate_row_color = self.options.lookup_option("Colors", "Alternate Table Cell Color").html()
        title_info = opt_style_info("Title")
        account_link_info = opt_style_info("Account link")
        number_cell_info = opt_style_info("Number cell")
        number_header_info = opt_style_info("Number header")
        text_cell_info = opt_style_info("Text cell")
        total_number_cell_info = opt_style_info("Total number cell")
        total_label_cell_info = opt_style_info("Total label cell")
        centered_label_cell_info = opt_style_info("Centered label cell")
        ssdoc.set_style_text(
            "".join(["@media (prefers-color-scheme: dark) {body {color: #000; background-color: #fff;}}\n",
                     "h3 { ", title_info, " }\n", "a { ", account_link_info, " }\n",
                     "body, p, table, tr, td { vertical-align: top; ", text_cell_info, " }\n",
                     "tr.alternate-row { background: ", alternate_row_color,
                     " }\ntr { page-break-inside: avoid !important;}\n",
                     "html, body { height: 100vh; margin: 0; }\ntd, th { border-color: grey }\n",
                     "th.column-heading-left { text-align: left; ", number_header_info, " }\n",
                     "th.column-heading-center { text-align: center; ", number_header_info, " }\n",
                     "th.column-heading-right { text-align: right; ", number_header_info, " }\n",
                     "td.highlight {background-color:#e1e1e1}",
                     "td.neg { ", "color: red; " if negative_red else "", " }\n",
                     "td.number-cell, td.total-number-cell, td.number-cell { text-align:right; white-space: nowrap; }\n",
                     "td.date-cell { white-space: nowrap; }\n",
                     "td.anchor-cell { white-space: nowrap; ", text_cell_info, " }\n",
                     "td.number-cell { ", number_cell_info, " }\n",
                     "td.number-header { text-align: right; ", number_header_info, " }\n",
                     "td.text-cell { ", text_cell_info, " }\n",
                     "td.total-number-cell { ", total_number_cell_info, " }\n",
                     "td.total-label-cell { ", total_label_cell_info, " }\n",
                     "td.centered-label-cell { text-align: center; ", centered_label_cell_info, " }\n",
                     "@media print { html, body { height: unset; }}\n",
                     doc.get_style_text() or ""]))

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_version(self):
        return self.version

    def set_version(self, version):
        self.version = version

    def generate_options(self):
        raise NotImplementedError

    def renderer(self, doc):
        raise NotImplementedError

    @classmethod
    def find(cls, tname):
        return cls.templates.get(tname)

    @classmethod
    def get(cls):
        return list(cls.templates.values())


class HtmlStyleSheet:
    sheets = {}

    def __init__(self, name: str, template: HtmlStyleSheetTemplate):
        self.template = template
        self.name = name
        self.style = HtmlStyleTable()
        HtmlStyleSheet.sheets[name] = self

    def get_template(self):
        return self.template

    def get_options(self):
        return self.template.options if self.template is not None else None

    @classmethod
    def save_options(cls, file_path):
        option_dict = {}
        for name, sheet in cls.sheets.items():
            option_dict[name] = {"template": sheet.get_template().get_name(),
                                 "options": sheet.get_options().serialize()}
        try:
            with open(os.path.join(file_path, "stylesheets-2.0"), "w") as f:
                f.write(json.dumps(option_dict, indent=4))
        except FileNotFoundError:
            pass

    @classmethod
    def restore_options(cls, file_path):
        try:
            f = open(os.path.join(file_path, "stylesheets-2.0"), "r")
            option_dict = json.loads(f.read())
            for sheet_name, sheet_vars in option_dict.items():
                sheet = cls.sheets.get(sheet_name)
                if sheet is None:
                    template = HtmlStyleSheetTemplate.find(sheet_vars["template"])
                    if template is None:
                        continue
                    sheet = HtmlStyleSheet(sheet_name, template)
                sheet.get_options().deserialize(sheet_vars.get("options"))
        except FileNotFoundError:
            pass

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_style(self):
        return self.style

    def set_style(self, tag_s, *kwargs):
        self.style.set(tag_s, HtmlStyleDataInfo(*kwargs) if len(kwargs) == 1 else HtmlStyleMarkupInfo(*kwargs))

    @classmethod
    def find(cls, tname):
        return cls.sheets.get(tname)

    @classmethod
    def remove(cls, sheet):
        name = sheet.get_name()
        if name is None or not isinstance(name, str):
            name = "Default"
        cls.sheets.pop(name)

    def render(self, doc, headers=False):
        new_doc = self.template.renderer(doc)
        new_doc.set_title(doc.get_title())
        new_doc.set_headline(doc.get_headline())
        new_doc.push_style(self.get_style())
        new_doc.get_style().compile(new_doc.get_style_stack())
        new_doc.push_style(new_doc.get_style())
        new_doc.set_style_internal(doc.get_style())
        return new_doc.render(headers)

    @classmethod
    def get_sheets(cls):
        return cls.sheets.values()


Hook.add_dangler(HOOK_SAVE_OPTIONS, HtmlStyleSheet.save_options)
Hook.add_dangler(HOOK_RESTORE_OPTIONS, HtmlStyleSheet.restore_options)
