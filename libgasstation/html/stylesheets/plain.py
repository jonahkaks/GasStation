from libgasstation.html.stylesheets.stylesheet import *
from libgasstation.html.stylesheets.text import *


class Plain(HtmlStyleSheetTemplate):
    def __init__(self):
        super().__init__("Plain")

    def generate_options(self):
        self.options = NewOptions()
        opt_reg = self.options.register_option
        opt_reg(
            ColorOption("General", "Background Color", "a", "Background color for reports.", [0xff, 0xff, 0xff, 0], 255,
                        False))
        opt_reg(PixMapOption("General", "Background Pixmap", "b", "Background tile for reports.", ""))
        opt_reg(SimpleBooleanOption('General', "Enable Links", "c", "Enable hyperlinks in reports.", True))
        opt_reg(ColorOption("Colors", "Alternate Table Cell Color", "a", "Background color for alternate lines.",
                            [0xff, 0xff, 0xff, 0], 255, False))
        opt_reg(NumberRangeOption("Tables", "Table cell spacing", "a",
                                  "Space between table cells.",
                                  0.0, 0.0, 20.0, 0.0, 1.0))
        opt_reg(NumberRangeOption("Tables", "Table cell padding", "b",
                                  "Space between table cell edge and content.",
                                  4.0, 0.0, 20.0, 0.0, 1.0))
        opt_reg(NumberRangeOption("Tables", "Table border width", "c",
                                  "Bevel depth on tables.",
                                  0.0, 0.0, 20.0, 0.0, 1.0))
        self.register_font_options()

    def renderer(self, doc):
        from libgasstation.html.document import HtmlDocument
        ssdoc = HtmlDocument()
        color_val = lambda section, name: self.options.lookup_option(section, name).html()
        opt_val = lambda section, name: self.options.lookup_option(section, name).get_value()
        clropt = color_val('General', 'Background Color')
        dolinks = opt_val('General', 'Enable Links')
        bgpixmap = opt_val("General", "Background Pixmap")
        ssdoc.set_style("body", {"bgcolor": clropt})
        if bgpixmap is not None and len(bgpixmap):
            ssdoc.set_style("body", {"background": bgpixmap, "class": "bg"})
        bdropt = opt_val('Tables', 'Table border width')
        padopt = opt_val('Tables', 'Table cell padding')
        spcopt = opt_val('Tables', 'Table cell spacing')
        ssdoc.set_style("table", {"border": bdropt, "cellspacing": spcopt, "cellpadding": padopt})
        ssdoc.set_style("column-heading-left", "th", {"class": "column-heading-left"})
        ssdoc.set_style("column-heading-center", "th", {"class": "column-heading-center"})
        ssdoc.set_style("column-heading-right", "th", {"class": "column-heading-right"})
        ssdoc.set_style("date-cell", "td", {"class": "date-cell"})
        ssdoc.set_style("anchor-cell", "td", {"class": "anchor-cell"})
        ssdoc.set_style("number-cell", "td", {"class": "number-cell"})
        ssdoc.set_style("number-cell-neg", "td", {"class": "number-cell neg"})
        ssdoc.set_style("number-header", "th", {"class": "number-header"})
        ssdoc.set_style("text-cell", "td", {"class": "text-cell"})
        ssdoc.set_style("total-number-cell", "td", {"class": "total-number-cell"})
        ssdoc.set_style("total-number-cell-neg", "td", {"class": "total-number-cell neg"})
        ssdoc.set_style("total-label-cell", "td", {"class": "total-label-cell"})
        ssdoc.set_style("centered-label-cell", "td", {"class": "centered-label-cell"})
        ssdoc.set_style("normal-row", "tr")
        altopt = color_val('Colors', 'Alternate Table Cell Color')
        ssdoc.set_style("alternate-row", "tr", {"bgcolor": altopt})
        ssdoc.set_style("primary-subheading", "tr", {"bgcolor": clropt})
        ssdoc.set_style("secondary-subheading", "tr", {"bgcolor": clropt})
        ssdoc.set_style("grand-total", "tr", {"bgcolor": clropt})

        if not dolinks:
            ssdoc.set_style("a", "")

        # currently I think that the scheme styles were created before html had style sheets
        # eg css - now I think a lot of what the scheme style sheets used to do can be done 
        # with css styles
        # here we setup css styles for the style sheet
        doc.set_style_text("""body, html {
          height: 100%;
        }
        .bg{
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
          background-attachment: fixed;
          overflow-y:hidden;
        }""")
        self.add_css_information_to_doc(ssdoc, doc)

        # in scheme the title and objects are copied from 
        # the passed document
        # cant do this here as dont have title in python implmentation
        # title = doc.title
        # headline = doc.headline
        # if not headline:
        #    headline = title

        # so have to add a dummy entry
        # - other stylesheets do much more complicated headers
        # MUST add to ssdoc as not added anything to doc
        # use special name to identify as header
        headline = doc.get_headline() or doc.get_title()

        d = []
        push = d.append
        if headline is not None:
            push(H3(headline))

        ssdoc.append_objects(d)
        ssdoc.append_objects(doc.get_objects())
        return ssdoc


HtmlStyleSheet("Default", Plain())
