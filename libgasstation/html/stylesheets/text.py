import codecs
import os
import re

data_dir = os.environ.get('DATA_DIR', '')
js_dir = os.path.join(data_dir, "js")
css_dir = os.path.join(data_dir, "css")

from pydal._compat import PY2, reduce, pickle, copyreg, HTMLParser as _HTMLParser, name2codepoint, iteritems, unichr, \
    unicodeT, \
    to_bytes, to_native, to_unicode, implements_bool, text_type
from yatl import sanitizer
from .stylesheet import HtmlStyleTable, HtmlStyleMarkupInfo, HtmlStyleDataInfo

entitydefs = dict([(k_v[0], to_bytes(unichr(k_v[1]))) for k_v in iteritems(name2codepoint)])
entitydefs.setdefault('apos', to_bytes("'"))

# None represents a potentially variable byte. "##" in the XML spec...
autodetect_dict = {  # bytepattern     : ("name",
    (0x00, 0x00, 0xFE, 0xFF): ("ucs4_be"),
    (0xFF, 0xFE, 0x00, 0x00): ("ucs4_le"),
    (0xFE, 0xFF, None, None): ("utf_16_be"),
    (0xFF, 0xFE, None, None): ("utf_16_le"),
    (0x00, 0x3C, 0x00, 0x3F): ("utf_16_be"),
    (0x3C, 0x00, 0x3F, 0x00): ("utf_16_le"),
    (0x3C, 0x3F, 0x78, 0x6D): ("utf_8"),
    (0x4C, 0x6F, 0xA7, 0x94): ("EBCDIC")
}


def autoDetectXMLEncoding(buffer):
    """ buffer -> encoding_name
    The buffer should be at least 4 bytes long.
    Returns None if encoding cannot be detected.
    Note that encoding_name might not have an installed
    decoder (e.g. EBCDIC)
    """
    # a more efficient implementation would not decode the whole
    # buffer at once but otherwise we'd have to decode a character at
    # a time looking for the quote character...that's a pain

    encoding = "utf_8"
    # according to the XML spec, this is the default this code successively tries to refine the default
    # whenever it fails to refine, it falls back to the last place encoding was set.

    if len(buffer) >= 4:
        bytes = (byte1, byte2, byte3, byte4) = tuple(map(ord, buffer[0:4]))
        enc_info = autodetect_dict.get(bytes, None)
        if not enc_info:  # try autodetection again removing potentially
            # variable bytes
            bytes = (byte1, byte2, None, None)
            enc_info = autodetect_dict.get(bytes)
    else:
        enc_info = None

    if enc_info:
        encoding = enc_info  # we've got a guess... these are the new defaults

        # try to find a more precise encoding using xml declaration
        secret_decoder_ring = codecs.lookup(encoding)[1]
        (decoded, length) = secret_decoder_ring(buffer)
        first_line = decoded.split("\n")[0]
        if first_line and first_line.startswith("<?xml"):
            encoding_pos = first_line.find("encoding")
            if encoding_pos != -1:
                # look for double quote
                quote_pos = first_line.find('"', encoding_pos)

                if quote_pos == -1:  # look for single quote
                    quote_pos = first_line.find("'", encoding_pos)

                if quote_pos > -1:
                    quote_char, rest = (first_line[quote_pos],
                                        first_line[quote_pos + 1:])
                    encoding = rest[:rest.find(quote_char)]

    return encoding


def decoder(buffer):
    encoding = autoDetectXMLEncoding(buffer)
    return to_unicode(buffer, charset=encoding)


def local_html_escape(data, quote=False):
    """
    Works with bytes.
    Replace special characters "&", "<" and ">" to HTML-safe sequences.
    If the optional flag quote is true (the default), the quotation mark
    characters, both double quote (") and single quote (') characters are also
    translated.
    """
    if PY2:
        import cgi
        data = cgi.escape(data, quote)
        return data.replace("'", "&#x27;") if quote else data
    else:
        import html
        if isinstance(data, str):
            return html.escape(data, quote=quote)
        data = data.replace(b"&", b"&amp;")  # Must be done first!
        data = data.replace(b"<", b"&lt;")
        data = data.replace(b">", b"&gt;")
        if quote:
            data = data.replace(b'"', b"&quot;")
            data = data.replace(b'\'', b"&#x27;")
        return data


def xmlescape(data, quote=True):
    """
    Returns an escaped string of the provided data

    Args:
        data: the data to be escaped
        quote: optional (default False)
    """

    # first try the xml function
    if hasattr(data, 'xml') and callable(data.xml):
        return to_bytes(data.xml())

    if not (isinstance(data, (text_type, bytes))):
        # i.e., integers
        data = str(data)
    data = to_bytes(data, 'utf8', 'xmlcharrefreplace')

    # ... and do the escaping
    data = local_html_escape(data, quote)
    return data


def call_as_list(f, *a, **b):
    if not isinstance(f, (list, tuple)):
        f = [f]
    for item in f:
        item(*a, **b)


def truncate_string(text, length, dots='...'):
    text = to_unicode(text)
    if len(text) > length:
        text = to_native(text[:length - len(dots)]) + dots
    return text


class XmlComponent(object):
    """
    Abstract root for all Html components
    """

    # TODO: move some DIV methods to here

    def xml(self):
        raise NotImplementedError

    def __mul__(self, n):
        return CAT(*[self for i in range(n)])

    def __add__(self, other):
        if isinstance(self, CAT):
            components = self.components
        else:
            components = [self]
        if isinstance(other, CAT):
            components += other.components
        else:
            components += [other]
        return CAT(*components)

    def add_class(self, name):
        """
        add a class to _class attribute
        """
        c = self['_class']
        classes = (set(c.split()) if c else set()) | set(name.split())
        self['_class'] = ' '.join(classes) if classes else None
        return self

    def remove_class(self, name):
        """
        remove a class from _class attribute
        """
        c = self['_class']
        classes = (set(c.split()) if c else set()) - set(name.split())
        self['_class'] = ' '.join(classes) if classes else None
        return self


class XML(XmlComponent):
    """
    use it to wrap a string that contains XML/HTML so that it will not be
    escaped by the template

    Examples:

    >>> XML('<h1>Hello</h1>').xml()
    '<h1>Hello</h1>'
    """

    def __init__(
            self,
            text,
            sanitize=False,
            permitted_tags=[
                'a',
                'b',
                'blockquote',
                'br/',
                'i',
                'li',
                'ol',
                'ul',
                'p',
                'cite',
                'code',
                'pre',
                'img/',
                'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
                'table', 'tr', 'td', 'div',
                'strong', 'span',
            ],
            allowed_attributes={
                'a': ['href', 'title', 'target'],
                'img': ['src', 'alt'],
                'blockquote': ['type'],
                'td': ['colspan'],
            },
    ):
        """
        Args:
            text: the XML text
            sanitize: sanitize text using the permitted tags and allowed
                attributes (default False)
            permitted_tags: list of permitted tags (default: simple list of
                tags)
            allowed_attributes: dictionary of allowed attributed (default
                for A, IMG and BlockQuote).
                The key is the tag; the value is a list of allowed attributes.
        """
        if isinstance(text, unicodeT):
            text = to_native(text.encode('utf8', 'xmlcharrefreplace'))
        if sanitize:
            text = sanitizer.sanitize(text, permitted_tags, allowed_attributes)
        elif isinstance(text, bytes):
            text = to_native(text)
        elif not isinstance(text, str):
            text = str(text)
        self.text = text

    def xml(self):
        return to_bytes(self.text)

    def __str__(self):
        return self.text

    __repr__ = __str__

    def __add__(self, other):
        return '%s%s' % (self, other)

    def __radd__(self, other):
        return '%s%s' % (other, self)

    def __lt__(self, other):
        return str(self) < str(other)

    def __hash__(self):
        return hash(str(self))

    #    why was this here? Break unpickling in sessions
    #    def __getattr__(self, name):
    #        return getattr(str(self), name)

    def __getitem__(self, i):
        return str(self)[i]

    def __getslice__(self, i, j):
        return str(self)[i:j]

    def __iter__(self):
        for c in str(self):
            yield c

    def __len__(self):
        return len(str(self))

    def flatten(self, render=None):
        """
        returns the text stored by the XML object rendered
        by the `render` function
        """
        if render:
            return render(self.text, None, {})
        return self.text

    def elements(self, *args, **kargs):
        """
        to be considered experimental since the behavior of this method
        is questionable
        another option could be `TAG(self.text).elements(*args, **kwargs)`
        """
        return []


@implements_bool
class DIV(XmlComponent):
    """
    HTML helper, for easy generating and manipulating a DOM structure.
    Little or no validation is done.

    Behaves like a dictionary regarding updating of attributes.
    Behaves like a list regarding inserting/appending components.

    Examples:

    >>> DIV('hello', 'world', _style='color:red;').xml()
    '<div style=\"color:red;\">helloworld</div>'

    All other HTML helpers are derived from `DIV`.

    `_something="value"` attributes are transparently translated into
    `something="value"` HTML attributes
    """

    # name of the tag, subclasses should update this
    # tags ending with a '/' denote classes that cannot
    # contain components
    tag = 'div'

    def __init__(self, *components, **attributes):
        """
        Args:
            components: any components that should be nested in this element
            attributes: any attributes you want to give to this element

        Raises:
            SyntaxError: when a stand alone tag receives components
        """
        self.style = HtmlStyleTable()
        if self.tag[-1:] == '/' and components:
            raise SyntaxError('<%s> tags cannot have components'
                              % self.tag)
        if len(components) == 1 and isinstance(components[0], (list, tuple)):
            self.components = list(components[0])
        else:
            self.components = list(components)
        self.attributes = attributes
        self._fixup()
        # converts special attributes in components attributes
        self.parent = None
        for c in self.components:
            self._setnode(c)
        self._postprocessing()

    def update(self, **kargs):
        """
        dictionary like updating of the tag attributes
        """

        for (key, value) in iteritems(kargs):
            self[key] = value
        return self

    def append(self, value):
        """
        list style appending of components

        Examples:

        >>> a=DIV()
        >>> a.append(SPAN('x'))
        >>> print(a)
        <div><span>x</span></div>
        """
        self._setnode(value)
        ret = self.components.append(value)
        self._fixup()
        return ret

    def insert(self, i, value):
        """
        List-style inserting of components

        Examples:

        >>> a=DIV()
        >>> a.insert(0, SPAN('x'))
        >>> print(a)
        <div><span>x</span></div>
        """
        self._setnode(value)
        ret = self.components.insert(i, value)
        self._fixup()
        return ret

    def __getitem__(self, i):
        """
        Gets attribute with name 'i' or component #i.
        If attribute 'i' is not found returns None

        Args:
            i: index. If i is a string: the name of the attribute
                otherwise references to number of the component
        """

        if isinstance(i, str):
            try:
                return self.attributes[i]
            except KeyError:
                return None
        else:
            return self.components[i]

    def get(self, i):
        return self.attributes.get(i)

    def __setitem__(self, i, value):
        """
        Sets attribute with name 'i' or component #i.

        Args:
            i: index. If i is a string: the name of the attribute
                otherwise references to number of the component
            value: the new value
        """
        self._setnode(value)
        if isinstance(i, (str, unicodeT)):
            self.attributes[i] = value
        else:
            self.components[i] = value

    def __delitem__(self, i):
        """
        Deletes attribute with name 'i' or component #i.

        Args:
            i: index. If i is a string: the name of the attribute
                otherwise references to number of the component
        """

        if isinstance(i, str):
            del self.attributes[i]
        else:
            del self.components[i]

    def __len__(self):
        """
        Returns the number of included components
        """
        return len(self.components)

    def __bool__(self):
        """
        Always returns True
        """
        return True

    def _fixup(self):
        """
        Handling of provided components.

        Nothing to fixup yet. May be overridden by subclasses,
        eg for wrapping some components in another component or blocking them.
        """
        return

    def _wrap_components(self, allowed_parents,
                         wrap_parent=None,
                         wrap_lambda=None):
        """
        helper for _fixup. Checks if a component is in allowed_parents,
        otherwise wraps it in wrap_parent

        Args:
            allowed_parents: (tuple) classes that the component should be an
                instance of
            wrap_parent: the class to wrap the component in, if needed
            wrap_lambda: lambda to use for wrapping, if needed

        """
        components = []
        for c in self.components:
            if isinstance(c, (allowed_parents, CAT)):
                pass
            elif wrap_lambda:
                c = wrap_lambda(c)
            else:
                c = wrap_parent(c)
            if isinstance(c, DIV):
                c.parent = self
            components.append(c)
        self.components = components

    def _postprocessing(self):
        """
        Handling of attributes (normally the ones not prefixed with '_').

        Nothing to postprocess yet. May be overridden by subclasses
        """
        return

    def _traverse(self, status, hideerror=False):
        # TODO: docstring
        newstatus = status
        for c in self.components:
            if hasattr(c, '_traverse') and callable(c._traverse):
                c.vars = self.vars
                c.request_vars = self.request_vars
                c.errors = self.errors
                c.latest = self.latest
                c.session = self.session
                c.formname = self.formname
                if not c.attributes.get('hideerror'):
                    c['hideerror'] = hideerror or self.attributes.get('hideerror')
                newstatus = c._traverse(status, hideerror) and newstatus

        # for input, textarea, select, option
        # deal with 'value' and 'validation'

        name = self['_name']
        if newstatus:
            newstatus = self._validate()
            self._postprocessing()
        elif 'old_value' in self.attributes:
            self['value'] = self['old_value']
            self._postprocessing()
        elif name and name in self.vars:
            self['value'] = self.vars[name]
            self._postprocessing()
        if name:
            self.latest[name] = self['value']
        return newstatus

    def _validate(self):
        """
        nothing to validate yet. May be overridden by subclasses
        """
        return True

    def _setnode(self, value):
        if isinstance(value, DIV):
            value.parent = self

    def _xml(self):
        """
        Helper for xml generation. Returns separately:
        - the component attributes
        - the generated xml of the inner components

        Component attributes start with an underscore ('_') and
        do not have a False or None value. The underscore is removed.
        A value of True is replaced with the attribute name.

        Returns:
            tuple: (attributes, components)
        """

        # get the attributes for this component
        # (they start with '_', others may have special meanings)
        attr = []
        for key, value in iteritems(self.attributes):
            if key[:1] != '_':
                continue
            name = key[1:]
            if value is True:
                value = name
            elif value is False or value is None:
                continue
            attr.append((name, value))
        data = self.attributes.get('data', {})
        for key, value in iteritems(data):
            name = 'data-' + key
            value = data[key]
            attr.append((name, value))
        attr.sort()
        fa = b''
        for name, value in attr:
            fa += b' %s="%s"' % (to_bytes(name), xmlescape(value, True))

        # get the xml for the inner components
        co = b''.join([xmlescape(component) for component in self.components])
        return fa, co

    def xml(self):
        """
        generates the xml for this component.
        """

        (fa, co) = self._xml()

        if not self.tag:
            return co

        tagname = to_bytes(self.tag)
        if tagname[-1:] == b'/':
            # <tag [attributes] />
            return b'<%s%s />' % (tagname[:-1], fa)

        # else: <tag [attributes]>  inner components xml </tag>
        xml_tag = b'<%s%s>%s</%s>' % (tagname, fa, co, tagname)
        return xml_tag

    def __str__(self):
        """
        str(COMPONENT) returns COMPONENT.xml()
        """
        # In PY3 __str__ cannot return bytes (TypeError: __str__ returned non-string (type bytes))
        return to_native(self.xml())

    def flatten(self, render=None):
        """
        Returns the text stored by the DIV object rendered by the render function
        the render function must take text, tagname, and attributes
        `render=None` is equivalent to `render=lambda text, tag, attr: text`

        Examples:

        >>> markdown = lambda text, tag=None, attributes={}: \
                        {None: re.sub('\s+',' ',text), \
                         'h1':'#'+text+'\\n\\n', \
                         'p':text+'\\n'}.get(tag,text)
        >>> a=TAG('<h1>Header</h1><p>this is a     test</p>')
        >>> a.flatten(markdown)
        '#Header\\n\\nthis is a test\\n'
        """

        text = ''
        for c in self.components:
            if isinstance(c, XmlComponent):
                s = c.flatten(render)
            elif render:
                s = render(to_native(c))
            else:
                s = to_native(c)
            text += s
        if render:
            text = render(text, self.tag, self.attributes)
        return text

    regex_tag = re.compile('^[\w\-\:]+')
    regex_id = re.compile('#([\w\-]+)')
    regex_class = re.compile('\.([\w\-]+)')
    regex_attr = re.compile('\[([\w\-\:]+)=(.*?)\]')

    def elements(self, *args, **kargs):
        if len(args) == 1:
            args = [a.strip() for a in args[0].split(',')]
        if len(args) > 1:
            subset = [self.elements(a, **kargs) for a in args]
            return reduce(lambda a, b: a + b, subset, [])
        elif len(args) == 1:
            items = args[0].split()
            if len(items) > 1:
                subset = [a.elements(' '.join(
                    items[1:]), **kargs) for a in self.elements(items[0])]
                return reduce(lambda a, b: a + b, subset, [])
            else:
                item = items[0]
                if '#' in item or '.' in item or '[' in item:
                    match_tag = self.regex_tag.search(item)
                    match_id = self.regex_id.search(item)
                    match_class = self.regex_class.search(item)
                    match_attr = self.regex_attr.finditer(item)
                    args = []
                    if match_tag:
                        args = [match_tag.group()]
                    if match_id:
                        kargs['_id'] = match_id.group(1)
                    if match_class:
                        kargs['_class'] = re.compile('(?<!\w)%s(?!\w)' %
                                                     match_class.group(1).replace('-', '\\-').replace(':', '\\:'))
                    for item in match_attr:
                        kargs['_' + item.group(1)] = item.group(2)
                    return self.elements(*args, **kargs)
        # make a copy of the components
        matches = []
        # check if the component has an attribute with the same
        # value as provided
        tag = to_native(getattr(self, 'tag')).replace('/', '')
        check = not (args and tag not in args)
        for (key, value) in iteritems(kargs):
            if key not in ['first_only', 'replace', 'find_text']:
                if isinstance(value, (str, int)):
                    if str(self[key]) != str(value):
                        check = False
                elif key in self.attributes:
                    if not value.search(str(self[key])):
                        check = False
                else:
                    check = False
        if 'find' in kargs:
            find = kargs['find']
            is_regex = not isinstance(find, (str, int))
            for c in self.components:
                if (isinstance(c, str) and ((is_regex and find.search(c)) or
                                            (str(find) in c))):
                    check = True
        # if found, return the component
        if check:
            matches.append(self)

        first_only = kargs.get('first_only', False)
        replace = kargs.get('replace', False)
        find_text = replace is not False and kargs.get('find_text', False)
        is_regex = not isinstance(find_text, (str, int, bool))
        find_components = not (check and first_only)

        def replace_component(i):
            if replace is None:
                del self[i]
                return i
            else:
                self[i] = replace(self[i]) if callable(replace) else replace
                return i + 1

        # loop the components
        if find_text or find_components:
            i = 0
            while i < len(self.components):
                c = self[i]
                j = i + 1
                if check and find_text and isinstance(c, str) and \
                        ((is_regex and find_text.search(c)) or (str(find_text) in c)):
                    j = replace_component(i)
                elif find_components and isinstance(c, XmlComponent):
                    child_matches = c.elements(*args, **kargs)
                    if len(child_matches):
                        if not find_text and replace is not False and child_matches[0] is c:
                            j = replace_component(i)
                        if first_only:
                            return child_matches
                        matches.extend(child_matches)
                i = j
        return matches

    def element(self, *args, **kargs):
        """
        Finds the first component that matches the supplied attribute dictionary,
        or None if nothing could be found

        Also the components of the components are searched.
        """
        kargs['first_only'] = True
        elements = self.elements(*args, **kargs)
        if not elements:
            # we found nothing
            return None
        return elements[0]

    def siblings(self, *args, **kargs):
        """
        Finds all sibling components that match the supplied argument list
        and attribute dictionary, or None if nothing could be found
        """
        sibs = [s for s in self.parent.components if not s == self]
        matches = []
        first_only = False
        if 'first_only' in kargs:
            first_only = kargs.pop('first_only')
        for c in sibs:
            try:
                check = True
                tag = getattr(c, 'tag').replace("/", "")
                if args and tag not in args:
                    check = False
                for (key, value) in iteritems(kargs):
                    if c[key] != value:
                        check = False
                if check:
                    matches.append(c)
                    if first_only:
                        break
            except:
                pass
        return matches

    def sibling(self, *args, **kargs):
        """
        Finds the first sibling component that match the supplied argument list
        and attribute dictionary, or None if nothing could be found
        """
        kargs['first_only'] = True
        sibs = self.siblings(*args, **kargs)
        if not sibs:
            return None
        return sibs[0]

    def render(self, doc=None):
        if doc is not None:
            finder = self.attributes.get("_class", self.tag)
            self.style.compile(doc.style_stack)
            doc.push_style(self.style)
            if finder is not None:
                child_info = doc.fetch_markup_style(finder)
                if child_info is not None:
                    for k, v in child_info.attributes.items():
                        if not k.startswith("_"):
                            k = "_" + k
                        self.attributes[k] = v
            for component in self.components:
                if isinstance(component, DIV):
                    component.render(doc)
            doc.pop_style()
            self.style.uncompile()
        return self

    def set_style(self, tag_s, *args):
        self.style.set(tag_s,
                       HtmlStyleDataInfo(*args) if len(args) == 2 and callable(args[1]) else HtmlStyleMarkupInfo(tag_s,
                                                                                                                 *args))


class CAT(DIV):
    tag = ''


def TAG_unpickler(data):
    return pickle.loads(data)


def TAG_pickler(data):
    d = DIV()
    d.__dict__ = data.__dict__
    marshal_dump = pickle.dumps(d, pickle.HIGHEST_PROTOCOL)
    return (TAG_unpickler, (marshal_dump,))


class __tag_div__(DIV):
    def __init__(self, name, *a, **b):
        DIV.__init__(self, *a, **b)
        self.tag = name

    def __repr__(self):
        return self.tag


copyreg.pickle(__tag_div__, TAG_pickler, TAG_unpickler)


class __TAG__(XmlComponent):
    """
    TAG factory

    Examples:

    >>> print(TAG.first(TAG.second('test'), _key = 3))
    <first key=\"3\"><second>test</second></first>

    """

    def __getitem__(self, name):
        return self.__getattr__(name)

    def __getattr__(self, name):
        if name[-1:] == '_':
            name = name[:-1] + '/'
        return lambda *a, **b: __tag_div__(name, *a, **b)

    def __call__(self, html):
        return HTMLParser(decoder(html)).tree


TAG = __TAG__()


class HTML(DIV):
    """
    There are four predefined document type definitions.
    They can be specified in the 'doctype' parameter:

    - 'strict' enables strict doctype
    - 'transitional' enables transitional doctype (default)
    - 'frameset' enables frameset doctype
    - 'html5' enables HTML 5 doctype
    - any other string will be treated as user's own doctype

    'lang' parameter specifies the language of the document.
    Defaults to 'en'.

    See also `DIV`
    """

    tag = b'html'

    strict = b'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">\n'
    transitional = \
        b'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n'
    frameset = \
        b'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">\n'
    html5 = b'<!DOCTYPE HTML>\n'

    def xml(self):
        lang = self['lang']
        if not lang:
            lang = 'en'
        self.attributes['_lang'] = lang
        doctype = self['doctype']
        if doctype is None:
            doctype = b""
        elif doctype == 'strict':
            doctype = self.strict
        elif doctype == 'transitional':
            doctype = self.transitional
        elif doctype == 'frameset':
            doctype = self.frameset
        elif doctype == 'html5':
            doctype = self.html5
        elif doctype == '':
            doctype = b''
        else:
            doctype = b'%s\n' % to_bytes(doctype)
        (fa, co) = self._xml()

        return b'%s<%s%s>%s</%s>' % (doctype, self.tag, fa, co, self.tag)


class XHTML(DIV):
    """
    This is XHTML version of the HTML helper.

    There are three predefined document type definitions.
    They can be specified in the 'doctype' parameter:

    - 'strict' enables strict doctype
    - 'transitional' enables transitional doctype (default)
    - 'frameset' enables frameset doctype
    - any other string will be treated as user's own doctype

    'lang' parameter specifies the language of the document and the xml document.
    Defaults to 'en'.

    'xmlns' parameter specifies the xml namespace.
    Defaults to 'http://www.w3.org/1999/xhtml'.

    See also `DIV`
    """

    tag = b'html'

    strict = b'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n'
    transitional = b'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n'
    frameset = b'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">\n'
    xmlns = 'http://www.w3.org/1999/xhtml'

    def xml(self):
        xmlns = self['xmlns']
        if xmlns:
            self.attributes['_xmlns'] = xmlns
        else:
            self.attributes['_xmlns'] = self.xmlns
        lang = self['lang']
        if not lang:
            lang = 'en'
        self.attributes['_lang'] = lang
        self.attributes['_xml:lang'] = lang
        doctype = self['doctype']
        if doctype:
            if doctype == 'strict':
                doctype = self.strict
            elif doctype == 'transitional':
                doctype = self.transitional
            elif doctype == 'frameset':
                doctype = self.frameset
            else:
                doctype = b'%s\n' % to_bytes(doctype)
        else:
            doctype = self.transitional
        (fa, co) = self._xml()
        return b'%s<%s%s>%s</%s>' % (doctype, self.tag, fa, co, self.tag)


class HEAD(DIV):
    tag = 'head'


class TITLE(DIV):
    tag = 'title'


class META(DIV):
    tag = 'meta/'


class LINK(DIV):
    tag = 'link/'

    def _fixup(self):
        if self.attributes.get("_rel") == 'stylesheet':
            href = self.attributes.get('_href')
            if href is not None:
                self.attributes['_href'] = os.path.join('file://', css_dir, href)


class SCRIPT(DIV):
    tag = b'script'
    tagname = to_bytes(tag)

    def xml(self):
        fa, co = self._xml()
        fa = to_bytes(fa)
        # no escaping of subcomponents
        co = b'\n'.join([to_bytes(component) for component in
                         self.components])
        if co:
            return b'<%s%s>\n%s\n</%s>' % (self.tagname, fa, co, self.tagname)
        else:
            return DIV.xml(self)

    def _fixup(self):
        src = self.attributes.get('_src')
        if src is not None:
            self.attributes['_src'] = os.path.join('file://', js_dir, src)


class STYLE(DIV):
    tag = 'style'
    tagname = to_bytes(tag)

    def xml(self):
        (fa, co) = self._xml()
        fa = to_bytes(fa)
        # no escaping of subcomponents
        co = b'\n'.join([to_bytes(component) for component in
                         self.components])
        if co:
            return b'<%s%s>\n%s\n</%s>' % (self.tagname, fa, co, self.tagname)
        else:
            return DIV.xml(self)


class CAPTION(DIV):
    tag = 'caption'


class IMG(DIV):
    tag = 'img/'


class SPAN(DIV):
    tag = 'span'


class BODY(DIV):
    tag = 'body'


class H1(DIV):
    tag = 'h1'


class H2(DIV):
    tag = 'h2'


class H3(DIV):
    tag = 'h3'


class H4(DIV):
    tag = 'h4'


class H5(DIV):
    tag = 'h5'


class H6(DIV):
    tag = 'h6'


class P(DIV):
    """
    Will replace ``\\n`` by ``<br />`` if the `cr2br` attribute is provided.

    see also `DIV`
    """

    tag = 'p'

    def xml(self):
        text = DIV.xml(self)
        if self['cr2br']:
            text = text.replace(b'\n', b'<br />')
        return text


class STRONG(DIV):
    tag = 'strong'


class B(DIV):
    tag = 'b'


class BR(DIV):
    tag = 'br/'


class HR(DIV):
    tag = 'hr/'


class BUTTON(DIV):
    tag = 'button'


class EM(DIV):
    tag = 'em'


class EMBED(DIV):
    tag = 'embed/'


class TT(DIV):
    tag = 'tt'


class PRE(DIV):
    tag = 'pre'


class CENTER(DIV):
    tag = 'center'


class LABEL(DIV):
    tag = 'label'


class LI(DIV):
    tag = 'li'


class UL(DIV):
    """
    UL Component.

    If subcomponents are not LI-components they will be wrapped in a LI

    """

    tag = 'ul'

    def _fixup(self):
        self._wrap_components(LI, LI)


class OL(UL):
    tag = 'ol'


class TD(DIV):
    tag = 'td'


class TH(DIV):
    tag = 'th'


class TR(DIV):
    """
    TR Component.

    If subcomponents are not TD/TH-components they will be wrapped in a TD

    """

    tag = 'tr'

    def _fixup(self):
        self._wrap_components((TD, TH), TD)


class __TRHEAD__(DIV):
    """
    __TRHEAD__ Component, internal only

    If subcomponents are not TD/TH-components they will be wrapped in a TH

    """

    tag = 'tr'

    def _fixup(self):
        self._wrap_components((TD, TH), TH)


class THEAD(DIV):
    tag = 'thead'

    def _fixup(self):
        self._wrap_components((__TRHEAD__, TR), __TRHEAD__)


class TBODY(DIV):
    tag = 'tbody'

    def _fixup(self):
        self._wrap_components(TR, TR)


class TFOOT(DIV):
    tag = 'tfoot'

    def _fixup(self):
        self._wrap_components(TR, TR)


class ANCHOR(DIV):
    tag = "a"


class CANVAS(DIV):
    tag = "canvas"


class COL(DIV):
    tag = 'col/'


class COLGROUP(DIV):
    tag = 'colgroup'


class TABLE(DIV):
    """
    TABLE Component.

    If subcomponents are not TR/TBODY/THEAD/TFOOT-components
    they will be wrapped in a TR

    """

    tag = 'table'

    def _fixup(self):
        self._wrap_components((CAPTION, TR, TBODY, THEAD, TFOOT, COL, COLGROUP), TR)


class I(DIV):
    tag = 'i'


class IFRAME(DIV):
    tag = 'iframe'


class OBJECT(DIV):
    tag = 'object'


class FIELDSET(DIV):
    tag = 'fieldset'


class LEGEND(DIV):
    tag = 'legend'


class HTMLParser(_HTMLParser):
    """
    obj = web2pyHTMLParser(text) parses and html/xml text into web2py helpers.
    obj.tree contains the root of the tree, and tree can be manipulated
    """

    def __init__(self, text, closed=('input', 'link')):
        _HTMLParser.__init__(self)
        self.tree = self.parent = TAG['']()
        self.closed = closed
        self.last = None
        self.feed(text)

    def handle_starttag(self, tagname, attrs):
        if tagname in self.closed:
            tagname += '/'
        tag = TAG[tagname]()
        for key, value in attrs:
            tag['_' + key] = value
        tag.parent = self.parent
        self.parent.append(tag)
        if not tag.tag.endswith('/'):
            self.parent = tag
        else:
            self.last = tag.tag[:-1]

    def handle_data(self, data):
        if not isinstance(data, unicodeT):
            try:
                data = data.decode('utf8')
            except:
                data = data.decode('latin1')
        self.parent.append(data.encode('utf8', 'xmlcharref'))

    def handle_charref(self, name):
        if name.startswith('x'):
            self.parent.append(unichr(int(name[1:], 16)).encode('utf8'))
        else:
            self.parent.append(unichr(int(name)).encode('utf8'))

    def handle_entityref(self, name):
        self.parent.append(entitydefs[name])

    def handle_endtag(self, tagname):
        # this deals with unbalanced tags
        if tagname == self.last:
            return
        while True:
            try:
                parent_tagname = self.parent.tag
                self.parent = self.parent.parent
            except:
                raise RuntimeError("unable to balance tag %s" % tagname)
            if parent_tagname[:len(tagname)] == tagname:
                break
