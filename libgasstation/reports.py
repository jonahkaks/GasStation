from collections import namedtuple


class BalanceSheet(namedtuple('BalanceSheet',
                              'asset liability equity retained_earnings')):
    @property
    def total_assets(self):
        return sum(self.asset.values())

    @property
    def total_liabilities(self):
        return sum(self.liability.values())

    @property
    def total_equity(self):
        return sum(self.equity.values()) + self.retained_earnings

    @property
    def total_liaeq(self):
        return self.total_equity + self.total_liabilities


class TrialBalance(namedtuple('TrialBalance',
                              'debit, credit')):
    @property
    def total_debits(self):
        return sum(self.debit.values())

    @property
    def total_credits(self):
        return sum(self.credit.values())


class IncomeStatement(namedtuple('_IncomeStatement',
                                 'income expense')):
    @property
    def total_incomes(self):
        return sum(self.income.values())

    @property
    def total_expenses(self):
        return sum(self.expense.values())

    @property
    def net_result(self):
        return self.total_incomes - self.total_expenses

    @property
    def net_income(self):
        if self.net_result >= 0:
            return self.net_result
        return 0

    @property
    def net_loss(self):
        if self.net_result < 0:
            return - self.net_result
        return 0

