import cProfile
# from .numeric import *
from decimal import InvalidOperation

from libgasstation.core.numeric import *


def benchmark(func):
    def profiled_func(*args, **kwargs):
        profile = cProfile.Profile()
        try:
            profile.enable()
            result = func(*args, **kwargs)
            profile.disable()
            return result
        finally:
            profile.print_stats()

    return profiled_func


import os
import ctypes

chelpers = ctypes.CDLL(os.path.join(os.path.dirname(__file__), "libparse.{}".format("so" if os.name == "posix" else "dll")))
par = chelpers.parse_text
par.argtypes = [ctypes.c_char_p]
par.restype = ctypes.c_double


def parse_exp(a):
    MAX_NUMBER = 2 ** 63 - 1
    if a.isalpha():
        return Numeric.zero()
    try:
        d = Numeric(a)
    except ValueError:
        d = Numeric(par(a.encode()))
    denom = d.denominator
    num = d.numerator
    if not ((-MAX_NUMBER < num < MAX_NUMBER) and (-MAX_NUMBER < denom < MAX_NUMBER)):
        return Numeric.zero()
    else:
        return d


def parse_exp_decimal(a):
    MAX_NUMBER = 2 ** 63 - 1
    if a.isalpha() or len(a)==1 and not a.isdigit():
        return Decimal(0)
    try:
        d = Decimal(a)
    except InvalidOperation:
        d = Decimal(par(a.encode()))
    num, denom = d.as_integer_ratio()
    if not ((-MAX_NUMBER < num < MAX_NUMBER) and (-MAX_NUMBER < denom < MAX_NUMBER)):
        return Decimal(0)
    else:
        return d


def gs_set_num_action(trans, split, num, action):
    from .session import Session
    num_action = Session.get_current_book().use_split_action_for_num_field()
    if trans is not None and num is not None and split is None and action is None:
        trans.set_num(num)
        return

    if trans is None and num is None and split is not None and action is not None:
        split.set_action(action)
        return

    if trans is not None:
        if not num_action and num is not None:
            trans.set_num(num)
        if num_action and action is not None:
            trans.set_num(action)
    if split is not None:
        if not num_action and action is not None:
            split.set_action(action)
        if num_action and num is not None:
            split.set_action(num)


def gs_get_num_action(trans, split):
    from .session import Session
    num_action = Session.get_current_book().use_split_action_for_num_field()
    if trans is not None and split is None:
        return trans.get_num()

    if split is not None and trans is None:
        return split.get_action()

    if trans is not None and split is not None:
        if num_action:
            return split.get_action()
        else:
            return trans.get_num()


def gs_get_action_num(trans, split):
    from .session import Session
    num_action = Session.get_current_book().use_split_action_for_num_field()
    if trans and not split:
        return trans.get_num()

    if split and not trans:
        return split.get_action()

    if trans and split:
        if num_action:
            return trans.get_num()
        else:
            return split.get_action()


def qof_init():
    from libgasstation.core.query_private import QueryPrivate
    from .book import Book
    QueryPrivate.init()
    Book.register()

def qof_close():
    from libgasstation.core.query_private import QueryPrivate
    from libgasstation.core.object import ObjectClass
    from libgasstation.core.backend.backend import BackEnd
    QueryPrivate.shutdown()
    ObjectClass.shutdown()
    BackEnd.release()
