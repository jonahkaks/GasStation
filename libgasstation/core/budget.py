import uuid
from typing import List

from sqlalchemy import Column, VARCHAR, INTEGER, DECIMAL, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy_utils import UUIDType

from ._declbase import DeclarativeBaseGuid, DeclarativeBase
from .recurrence import Recurrence


class BudgetRecurrence(DeclarativeBase):
    __tablename__ = 'budget_recurrence'
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True,nullable=False)
    budget_guid = Column("budget_guid", UUIDType(binary=False), ForeignKey("budget.guid"), nullable=False)
    recurrence_guid = Column("recurrence_guid", UUIDType(binary=False),ForeignKey("recurrence.guid"),
                             nullable=False, primary_key=True)
    recurrence: Recurrence = relationship("Recurrence")
    budget = relationship("Budget", back_populates="recurrences")


class Budget(DeclarativeBaseGuid):
    """
    A GasStation Budget

    Attributes:
        name (str): name of the budget
        description (str): description of the budget
        amounts (list of :class:`budget.BudgetAmount`): list of amounts per account
    """
    __tablename__ = 'budget'

    __table_args__ = {}

    # column definitions
    # keep this line as we reference it in the primaryjoin
    name = Column('name', VARCHAR(length=2048), nullable=False)
    description = Column('description', VARCHAR(length=2048))
    num_periods = Column('num_periods', INTEGER(), nullable=False)

    recurrences: List[BudgetRecurrence] = relationship('BudgetRecurrence',
                                                       back_populates="budget",
                                                       lazy="joined",
                                                       cascade='all, delete-orphan',
                                                       )

    amounts = relationship('BudgetAmount',
                           back_populates="budget",
                           cascade='all, delete-orphan'
                           )

    def __repr__(self):
        return u"Budget<{}({}) for {} periods following pattern '{}' >".format(self.name, self.description,
                                                                               self.num_periods, self.recurrence)


class BudgetAmount(DeclarativeBase):
    """
    A GasStation BudgetAmount
    Attributes:
        amount (:class:`decimal.Decimal`): the budgeted amount
        account (:class:`core.account.Account`): the budgeted account
        budget (:class:`Budget`): the budget of the amount
    """
    __tablename__ = 'budget_amount'

    __table_args__ = {}

    # column definitions
    id = Column('id', INTEGER(), primary_key=True, nullable=False)
    budget_guid = Column('budget_guid', UUIDType(binary=False),
                         ForeignKey('budget.guid'), nullable=False)
    account_guid = Column('account_guid', UUIDType(binary=False),
                          ForeignKey('account.guid'), nullable=False)
    period_num = Column('period_num', INTEGER(), nullable=False)
    amount = Column('amount', DECIMAL(), nullable=False)

    # relation definitions
    account = relationship('Account', back_populates='budget_amounts')
    budget = relationship('Budget', back_populates="amounts")

    def __repr__(self):
        return u"BudgetAmount<{}={}>".format(self.period_num, self.amount)
