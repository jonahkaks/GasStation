import locale

from sqlalchemy import Column, VARCHAR, INTEGER, BOOLEAN
from sqlalchemy.orm import reconstructor, relationship, make_transient

from ._declbase import DeclarativeBaseGuid, ID_COMMODITY, ID_COMMODITY_TABLE, ID_COMMODITY_NAMESPACE
from .event import *
from .instance import Instance
from .object import *

COMMODITY_NAMESPACE_NAME_LEGACY = "LEGACY_CURRENCIES"
COMMODITY_NAMESPACE_NAME_TEMPLATE = "template"
COMMODITY_NAMESPACE_NAME_ISO = "ISO4217"
COMMODITY_NAMESPACE_NAME_CURRENCY = "CURRENCY"
COMMODITY_NAMESPACE_NAME_NASDAQ = "NASDAQ"
COMMODITY_NAMESPACE_NAME_NYSE = "NYSE"
COMMODITY_NAMESPACE_NAME_EUREX = "EUREX"
COMMODITY_NAMESPACE_NAME_MUTUAL = "FUND"
COMMODITY_NAMESPACE_NAME_AMEX = "AMEX"
COMMODITY_NAMESPACE_NAME_ASX = "ASX"
COMMODITY_NAMESPACE_NAME_NONCURRENCY = "All non-currency"
COMMODITY_NAMESPACE_NAME_ISO_GUI = "Currencies"
#FIXME Remove the String namespace
COMMODITY_MAX_FRACTION = 1000000000
multiple_quote_sources = []
single_quote_sources = []


class NewIsoCode:
    old_code = None
    new_code = None

    def __init__(self, o, n):
        self.old_code = o
        self.new_code = n


gs_new_iso_codes = [
    NewIsoCode("RUR", "RUB"),
    NewIsoCode("PLZ", "PLN"),
    NewIsoCode("UAG", "UAH"),
    NewIsoCode("NIS", "ILS"),
    NewIsoCode("MXP", "MXN"),
    NewIsoCode("TRL", "TRY")]

fq_version = None
new_quote_sources = []


class QuoteSourceType:
    SINGLE = 0
    MULTI = 1
    UNKNOWN = 2
    MAX = 3
    CURRENCY = MAX


class QuoteSource:
    supported = False
    _type = None
    index = 0
    user_name = None
    old_internal_name = None
    internal_name = None

    def __init__(self, supported=False, _type=None, index=0, username=None, old_internal=None, internal_name=None):
        self.supported = supported
        self._type = _type
        self.index = index
        self.user_name = username
        self.old_internal_name = old_internal
        self.internal_name = internal_name
        Event.gen(self, EVENT_CREATE)

    @staticmethod
    def fq_installed():
        global fq_version
        return fq_version is not None

    @staticmethod
    def fq_version():
        global fq_version
        return fq_version

    def __repr__(self):
        return u"QuoteSource({})".format(self.user_name)

    @staticmethod
    def num_entries(_type):
        if _type == QuoteSourceType.CURRENCY:
            return 1

        if _type == QuoteSourceType.SINGLE:
            return len(single_quote_sources)

        if _type == QuoteSourceType.MULTI:
            return len(multiple_quote_sources)
        return len(new_quote_sources)

    @staticmethod
    def init_tables():
        global currency_quote_source
        for i, s in enumerate(single_quote_sources):
            s.type = QuoteSourceType.SINGLE
            s.index = i
        for i, s in enumerate(multiple_quote_sources):
            s.type = QuoteSourceType.MULTI
            s.index = i
        currency_quote_source.type = QuoteSourceType.CURRENCY
        currency_quote_source.index = 0

    @staticmethod
    def add_new(source_name, supported):
        new_source = QuoteSource(supported=supported, _type=QuoteSourceType.UNKNOWN, index=len(new_quote_sources),
                                 username=source_name, old_internal=source_name, internal_name=source_name)
        new_quote_sources.append(new_source)
        return new_source

    @staticmethod
    def lookup_by_ti(_type, index):
        global currency_quote_source
        if _type == QuoteSourceType.CURRENCY:
            return currency_quote_source
        elif _type == QuoteSourceType.SINGLE:
            if index < len(single_quote_sources):
                return single_quote_sources[index]
        elif _type == QuoteSourceType.MULTI:
            if index < len(multiple_quote_sources):
                return multiple_quote_sources[index]
        else:
            if index < len(new_quote_sources):
                return new_quote_sources[index]
        return None

    @staticmethod
    def lookup_by_internal(name):
        global currency_quote_source
        if name is None or len(name) == 0:
            return None
        if name == currency_quote_source.internal_name or name == currency_quote_source.old_internal_name:
            return currency_quote_source

        for s in single_quote_sources + multiple_quote_sources + new_quote_sources:
            if name == s.internal_name or name == s.old_internal_name:
                return s
        return None

    def get_type(self):
        return self._type

    def get_index(self):
        return self.index

    def get_supported(self):
        return self.supported

    def get_user_name(self):
        return self.user_name

    def get_internal_name(self):
        return self.internal_name

    @staticmethod
    def set_fq_installed(version_string, sources_list):
        global fq_version
        if sources_list is None or not isinstance(sources_list, list):
            return
        if fq_version is not None:
            fq_version = None
        fq_version = version_string
        for source_name in sources_list:
            source = QuoteSource.lookup_by_internal(source_name)
            if source is not None:
                source.supported = True
                continue
            QuoteSource.add_new(source_name, True)


currency_quote_source = QuoteSource(True, 0, 0, "Currency", "CURRENCY", "currency")
single_quote_sources[:] = [
    QuoteSource(False, 0, 0, "Alphavantage, US", "ALPHAVANTAGE", "alphavantage"),
    QuoteSource(False, 0, 0, "Amsterdam Euronext eXchange, NL", "AEX", "aex"),
    QuoteSource(False, 0, 0, "American International Assurance, HK", "AIAHK", "aiahk"),
    QuoteSource(False, 0, 0, "Association of Mutual Funds in India", "AMFIINDIA", "amfiindia"),
    QuoteSource(False, 0, 0, "Athens Stock Exchange, GR", "ASEGR", "asegr"),
    QuoteSource(False, 0, 0, "Australian Stock Exchange, AU", "ASX", "asx"),
    QuoteSource(False, 0, 0, "BAMOSZ funds, HU", "BAMOSZ", "bamosz"),
    QuoteSource(False, 0, 0, "BMO NesbittBurns, CA", "BMONESBITTBURNS", "bmonesbittburns"),
    QuoteSource(False, 0, 0, "Bucharest Stock Exchange, RO", "BSERO", "bsero"),
    QuoteSource(False, 0, 0, "Budapest Stock Exchange (BET), ex-BUX, HU", "BSE", "bse"),
    QuoteSource(False, 0, 0, "Canada Mutual", "CANADAMUTUAL", "canadamutual"),
    QuoteSource(False, 0, 0, "Citywire Funds, GB", "citywire", "citywire"),
    QuoteSource(False, 0, 0, "Colombo Stock Exchange, LK", "CSE", "cse"),
    QuoteSource(False, 0, 0, "Cominvest, ex-Adig, DE", "COMINVEST", "cominvest"),
    QuoteSource(False, 0, 0, "Deka Investments, DE", "DEKA", "deka"),
    QuoteSource(False, 0, 0, "Dutch", "DUTCH", "dutch"),
    QuoteSource(False, 0, 0, "DWS, DE", "DWS", "dwsfunds"),
    QuoteSource(False, 0, 0, "Equinox Unit Trusts, ZA", "ZA_unittrusts", "za_unittrusts"),
    QuoteSource(False, 0, 0, "Fidelity Direct", "FIDELITY_DIRECT", "fidelity_direct"),
    QuoteSource(False, 0, 0, "Fidelity Fixed", "FIDELITY_DIRECT", "fidelityfixed"),
    QuoteSource(False, 0, 0, "Finance Canada", "FINANCECANADA", "financecanada"),
    QuoteSource(False, 0, 0, "Financial Times Funds service, GB", "FTFUNDS", "ftfunds"),
    QuoteSource(False, 0, 0, "Finanzpartner, DE", "FINANZPARTNER", "finanzpartner"),
    QuoteSource(False, 0, 0, "First Trust Portfolios, US", "FTPORTFOLIOS", "ftportfolios"),
    QuoteSource(False, 0, 0, "Fund Library, CA", "FUNDLIBRARY", "fundlibrary"),
    QuoteSource(False, 0, 0, "GoldMoney spot rates, JE", "GOLDMONEY", "goldmoney"),
    QuoteSource(False, 0, 0, "Greece", "GREECE", "greece"),
    QuoteSource(False, 0, 0, "Helsinki stock eXchange, FI", "HEX", "hex"),
    QuoteSource(False, 0, 0, "Hungary", "HU", "hu"),
    QuoteSource(False, 0, 0, "India Mutual", "INDIAMUTUAL", "indiamutual"),
    QuoteSource(False, 0, 0, "Man Investments, AU", "maninv", "maninv"),
    QuoteSource(False, 0, 0, "Morningstar, GB", "MSTARUK", "mstaruk"),
    QuoteSource(False, 0, 0, "Morningstar, JP", "MORNINGSTARJP", "morningstarjp"),
    QuoteSource(False, 0, 0, "Morningstar, SE", "MORNINGSTAR", "morningstar"),
    QuoteSource(False, 0, 0, "Motley Fool, US", "FOOL", "fool"),
    QuoteSource(False, 0, 0, "New Zealand stock eXchange, NZ", "NZX", "nzx"),
    QuoteSource(False, 0, 0, "Paris Stock Exchange/Boursorama, FR", "BOURSO", "bourso"),
    QuoteSource(False, 0, 0, "Paris Stock Exchange/LeRevenu, FR", "LEREVENU", "lerevenu"),
    QuoteSource(False, 0, 0, "Platinum Asset Management, AU", "PLATINUM", "platinum"),
    QuoteSource(False, 0, 0, "Romania", "romania", "romania"),
    QuoteSource(False, 0, 0, "SIX Swiss Exchange funds, CH", "SIXFUNDS", "sixfunds"),
    QuoteSource(False, 0, 0, "SIX Swiss Exchange shares, CH", "SIXSHARES", "sixshares"),
    QuoteSource(False, 0, 0, "Skandinaviska Enskilda Banken, SE", "SEB_FUNDS", "seb_funds"),
    QuoteSource(False, 0, 0, "Sharenet, ZA", "ZA", "za"),
    QuoteSource(False, 0, 0, "StockHouse Canada", "STOCKHOUSE_FUND", "stockhousecanada_fund"),
    QuoteSource(False, 0, 0, "TD Waterhouse Funds, CA", "TDWATERHOUSE", "tdwaterhouse"),
    QuoteSource(False, 0, 0, "TD Efunds, CA", "TDEFUNDS", "tdefunds"),
    QuoteSource(False, 0, 0, "TIAA-CREF, US", "TIAACREF", "tiaacref"),
    QuoteSource(False, 0, 0, "Toronto Stock eXchange, CA", "TSX", "tsx"),
    QuoteSource(False, 0, 0, "T. Rowe Price", "TRPRICE", "troweprice"),
    QuoteSource(False, 0, 0, "T. Rowe Price, US", "TRPRICE_DIRECT", "troweprice_direct"),
    QuoteSource(False, 0, 0, "Trustnet via tnetuk.pm, GB", "TNETUK", "tnetuk"),
    QuoteSource(False, 0, 0, "Trustnet via trustnet.pm, GB", "TRUSTNET", "trustnet"),
    QuoteSource(False, 0, 0, "U.K. Unit Trusts", "UKUNITTRUSTS", "uk_unit_trusts"),
    QuoteSource(False, 0, 0, "Union Investment, DE", "UNIONFUNDS", "unionfunds"),
    QuoteSource(False, 0, 0, "US Treasury Bonds", "usfedbonds", "usfedbonds"),
    QuoteSource(False, 0, 0, "US Govt. Thrift Savings Plan", "TSP", "tsp"),
    QuoteSource(False, 0, 0, "Vanguard", "VANGUARD", "vanguard"),
    QuoteSource(False, 0, 0, "VWD, DE (unmaintained)", "VWD", "vwd"),
    QuoteSource(False, 0, 0, "Yahoo as JSON", "YAHOO_JSON", "yahoo_json"),
    QuoteSource(False, 0, 0, "Yahoo as YQL", "YAHOO_YQL", "yahoo_yql")]

multiple_quote_sources[:] = [
    QuoteSource(False, 0, 0, "Australia (ASX, ...)", "AUSTRALIA", "australia"),
    QuoteSource(False, 0, 0, "Canada (Alphavantage, TSX, ...)", "CANADA", "canada"),
    QuoteSource(False, 0, 0, "Canada Mutual (Fund Library, StockHouse, ...)", "CANADAMUTUAL", "canadamutual"),
    QuoteSource(False, 0, 0, "Dutch (AEX, ...)", "DUTCH", "dutch"),
    QuoteSource(False, 0, 0, "Europe (asegr,.bsero, hex ...)", "EUROPE", "europe"),
    QuoteSource(False, 0, 0, "Greece (ASE, ...)", "GREECE", "greece"),
    QuoteSource(False, 0, 0, "Hungary (Bamosz, BET, ...)", "HU", "hu"),
    QuoteSource(False, 0, 0, "India Mutual (AMFI, ...)", "INDIAMUTUAL", "indiamutual"),
    QuoteSource(False, 0, 0, "Fidelity (Fidelity, ...)", "FIDELITY", "fidelity"),
    QuoteSource(False, 0, 0, "Finland (HEX, ...)", "FINLAND", "finland"),
    QuoteSource(False, 0, 0, "First Trust (First Trust, ...)", "FTPORTFOLIOS", "ftportfolios"),
    QuoteSource(False, 0, 0, "France (bourso, ĺerevenu, ...)", "FRANCE", "france"),
    QuoteSource(False, 0, 0, "Nasdaq (alphavantage, fool, ...)", "NASDAQ", "nasdaq"),
    QuoteSource(False, 0, 0, "New Zealand (NZX, ...)", "NZ", "nz"),
    QuoteSource(False, 0, 0, "NYSE (alphavantage, fool, ...)", "NYSE", "nyse"),
    QuoteSource(False, 0, 0, "South Africa (Sharenet, ...)", "ZA", "za"),
    QuoteSource(False, 0, 0, "Romania (BSE-RO, ...)", "romania", "romania"),
    QuoteSource(False, 0, 0, "T. Rowe Price", "TRPRICE", "troweprice"),
    QuoteSource(False, 0, 0, "U.K. Funds (citywire, FTfunds, MStar, tnetuk, ...)", "ukfunds", "ukfunds"),
    QuoteSource(False, 0, 0, "U.K. Unit Trusts (trustnet, ...)", "UKUNITTRUSTS", "uk_unit_trusts"),
    QuoteSource(False, 0, 0, "USA (Alphavantage, Fool, ...)", "USA", "usa")]


class CommodityNameSpace(Instance):
    def __init__(self, book):
        super().__init__(ID_COMMODITY_NAMESPACE, book)
        self.name = None
        self.iso4217 = False
        self.cm_table = None

    def __repr__(self):
        return u"CommodityNameSpace({})".format(self.name or "")

    def get_name(self):
        return self.name

    def get_gui_name(self):
        if self.name == COMMODITY_NAMESPACE_NAME_CURRENCY:
            return COMMODITY_NAMESPACE_NAME_ISO_GUI
        return self.name

    def get_commodity_list(self):
        return list(self.cm_table.values())

    @staticmethod
    def is_iso(name_space):
        return name_space == COMMODITY_NAMESPACE_NAME_ISO or name_space == COMMODITY_NAMESPACE_NAME_CURRENCY

    def refers_to(self, other):
        pass


class Commodity(DeclarativeBaseGuid):
    """
    A Commodity.

    Attributes:
        cusip (str): cusip code
        fraction (int): minimal unit of the commodity (e.g. 100 for 1/100)
        namespace (str): CURRENCY for currencies, otherwise any string to group multiple commodity together
        mnemonic (str): the ISO symbol for a currency or the stock symbol for stocks (used for online quotes)
        quote_flag (int): 1 if piecash/ quotes will retrieve online quotes for the commodity
        quote_source (str): the quote source for  (piecash always use yahoo for stock and quandl for currencies
        quote_tz (str): the timezone to assign on the online quotes
        base_currency (:class:`Commodity`): The base_currency for a commodity:

          - if the commodity is a currency, returns the "default currency" of the book (ie the one of the root_account)
          - if the commodity is not a currency, returns the currency encoded in the quoted_currency slot

        accounts (list of :class:`libgasstation.core.account.Account`): list of accounts which have the commodity as commodity
        transactions (list of :class:`libgasstation.core.transaction.Transaction`): list of transactions which have the commodity as currency
        prices (iterator of :class:`libgasstation.core.price_db.Price`): iterator on prices related to the commodity
         (it is a sqlalchemy query underneath)

    """
    __tablename__ = 'commodity'
    # column definitions
    namespace = Column('namespace', VARCHAR(length=200), nullable=False)
    mnemonic = Column('mnemonic', VARCHAR(length=10), nullable=False, unique=True)
    fullname = Column('fullname', VARCHAR(length=100), unique=True)
    cusip = Column('cusip', VARCHAR(length=50), unique=True)
    fraction = Column('fraction', INTEGER(), nullable=False, default=10000)
    quote_flag = Column('quote_flag', BOOLEAN(), nullable=False, default=False)
    quote_source = Column('quote_source', VARCHAR(length=100))
    quote_tz = Column('quote_tz', VARCHAR(length=200))
    default_symbol = Column('default_symbol', VARCHAR(length=40))
    user_symbol = Column('user_symbol', VARCHAR(length=40))
    auto_quote_control = Column('auto_quote_control', INTEGER(), nullable=False, default=0)

    # relation definitions
    accounts = relationship('Account', back_populates='commodity')
    transactions = relationship('Transaction', back_populates='currency')
    from .price_db import Price
    prices: List[Price] = relationship("Price", back_populates='commodity',
                                       foreign_keys=[Price.commodity_guid], cascade='all, delete-orphan')
    e_type = ID_COMMODITY
    usage_count = 0
    infant = True
    cache_factor = None

    def __init__(self, book, fullname=None,  namespace="ISO4217", mnemonic=None, cusip=None, fraction=100):
        super().__init__(ID_COMMODITY, book)
        self.begin_edit()
        self.printname = None
        if namespace is not None and namespace != "":
            if namespace == COMMODITY_NAMESPACE_NAME_TEMPLATE and mnemonic != "template":
                namespace = "User"
            self.set_namespace(namespace)
            if CommodityNameSpace.is_iso(namespace):
                self.set_quote_source(QuoteSource.lookup_by_internal("currency"))
        self.set_mnemonic(mnemonic)
        self.set_full_name(fullname)
        self.set_fraction(fraction)
        self.set_cusip(cusip)
        self.set_dirty()
        self.commit_edit()
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        self.infant = False
        self.usage_count = 0
        self.cache_factor = None
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_COMMODITY, book)
        table = CommodityTable.get_table(book)
        table.insert(self)

    def obtain_twin(self, book):
        comtbl = CommodityTable.get_table(book)
        if comtbl is None:
            return
        ucom = self.get_unique_name()
        twin = comtbl.lookup_unique(ucom)
        if twin is None:
            twin = self.clone(book)
            twin = comtbl.insert(twin)
        return twin

    def get_unique_name(self):
        return "{}::{}".format(self.namespace, self.mnemonic)

    def clone(self, book):
        c = DeclarativeBaseGuid.__new__(Commodity)
        c._sa_instance_state = self._sa_instance_state
        make_transient(c)
        Instance.__init__(c, ID_COMMODITY, book)
        c.namespace = self.namespace
        c.mnemonic = self.mnemonic
        c.fullname = self.fullname
        c.fraction = self.fraction
        c.cusip = self.cusip
        c.quote_flag = self.quote_flag
        c.quote_tz = self.quote_tz
        c.set_quote_source(self.get_quote_source())
        return c

    def copy(self, other):
        other.set_full_name(self.fullname)
        other.set_mnemonic(self.mnemonic)
        other.namespace = self.namespace
        other.set_fraction(self.fraction)
        other.set_cusip(self.cusip)
        other.set_quote_flag(self.quote_flag)
        other.set_quote_source(self.get_quote_source())
        other.set_quote_tz(self.quote_tz)

    def __repr__(self):
        return u"Commodity<{}:{}>".format(self.get_namespace(), self.get_mnemonic())

    def set_cusip(self, cusip):
        if not cusip or self.cusip == cusip: return
        self.begin_edit()
        self.cusip = cusip
        self.mark_dirty()
        self.commit_edit()

    def get_cusip(self):
        return self.cusip

    def get_quote_flag(self):
        return self.quote_flag

    def set_quote_flag(self, flag):
        if flag != self.quote_flag:
            self.begin_edit()
            self.quote_flag = flag
            self.mark_dirty()
            self.commit_edit()

    def user_set_quote_flag(self, flag):
        self.begin_edit()
        self.set_quote_flag(flag)
        if self.is_iso():
            self.set_auto_quote_control_flag((not flag and self.usage_count == 0) or (flag and self.usage_count != 0))
        self.commit_edit()

    def get_auto_quote_control_flag(self):
        return self.auto_quote_control

    def set_auto_quote_control_flag(self, flag):
        if flag != self.auto_quote_control:
            self.begin_edit()
            self.auto_quote_control = flag
            self.mark_dirty()
            self.commit_edit()

    def get_fraction(self):
        return self.fraction

    def set_fraction(self, value):
        if not value or self.fraction == value: return
        self.begin_edit()
        self.fraction = int(value)
        self.mark_dirty()
        self.commit_edit()

    def get_namespace(self):
        return self.namespace

    def get_namespace_ds(self) -> CommodityNameSpace:
        book = self.book
        table = CommodityTable.get_table(book)
        return table.add_namespace(self.namespace, book)

    def set_namespace(self, name_space):
        book = self.get_book()
        table = CommodityTable.get_table(book)
        nsp = table.add_namespace(name_space, book)
        if self.namespace == nsp.name:
            return
        self.begin_edit()
        self.namespace = nsp.name
        if nsp.iso4217:
            self.quote_source = QuoteSource.lookup_by_internal("currency")
        self.mark_dirty()
        self.commit_edit()

    def set_quote_source(self, qs):
        if qs is not None and isinstance(qs, QuoteSource) and qs.internal_name != self.quote_source:
            self.begin_edit()
            self.quote_source = qs.internal_name
            self.mark_dirty()
            self.commit_edit()

    def get_quote_source(self):
        global currency_quote_source
        if self.quote_source is None and self.is_iso():
            return currency_quote_source
        return QuoteSource.lookup_by_internal(self.quote_source)

    def get_default_quote_source(self):
        global currency_quote_source
        if self.is_iso():
            return currency_quote_source
        return QuoteSource.lookup_by_internal("alphavantage")

    def set_quote_tz(self, tz):
        if tz == self.quote_tz:
            return
        self.begin_edit()
        self.quote_tz = tz
        self.mark_dirty()
        self.commit_edit()

    def get_quote_tz(self):
        return self.quote_tz

    def get_full_name(self):
        return self.fullname

    def set_full_name(self, fullname):
        if fullname is None or fullname == self.fullname:
            return
        self.begin_edit()
        self.fullname = fullname
        self.mark_dirty()
        self.commit_edit()

    def get_mnemonic(self):
        return self.mnemonic

    def set_mnemonic(self, mnemonic):
        if mnemonic is None and self.mnemonic == mnemonic:
            return
        self.begin_edit()
        self.mnemonic = mnemonic
        self.mark_dirty()
        self.commit_edit()

    def get_printname(self):
        return u"%s (%s)" % (self.mnemonic, self.fullname)

    @property
    def precision(self):
        return len(str(self.fraction)) - 1

    def is_iso(self):
        return self.get_namespace_ds().iso4217

    def is_currency(self):
        ns_name = self.namespace
        return ns_name == COMMODITY_NAMESPACE_NAME_LEGACY or ns_name == COMMODITY_NAMESPACE_NAME_CURRENCY

    def set_default_symbol(self, sym):
        self.default_symbol = sym

    def get_default_symbol(self):
        return self.default_symbol

    def get_user_symbol(self):
        return self.user_symbol

    def set_user_symbol(self, sym):
        if sym == self.user_symbol:
            return
        self.begin_edit()
        lc = locale.localeconv()
        if lc.get('int_curr_symbol') == self.get_mnemonic() and sym == lc.get('user_symbol'):
            sym = None
        elif sym == self.default_symbol:
            sym = None
        if sym is not None:
            self.user_symbol = sym
        else:
            self.user_symbol = None
        self.mark_dirty()
        self.commit_edit()

    def get_nice_symbol(self):
        us = self.get_user_symbol()
        if us:
            return us
        ds = self.get_default_symbol()
        if ds:
            return ds
        return self.get_mnemonic()

    def increment_usage_count(self):
        if self.usage_count == 0 and not self.quote_flag and self.get_auto_quote_control_flag() and self.is_iso():
            self.begin_edit()
            self.set_quote_flag(True)
            self.set_quote_source(self.get_default_quote_source())
            self.commit_edit()
        self.usage_count += 1

    def decrement_usage_count(self):
        if self.usage_count == 0:
            return
        self.usage_count -= 1
        if self.usage_count == 0 and self.quote_flag and self.get_auto_quote_control_flag() and self.is_iso():
            self.set_quote_flag(False)

    def begin_edit(self):
        super().begin_edit()

    def comm_free(self):
        self.free()

    def noop(self):
        pass

    def free(self):
        book = self.get_book()
        table = CommodityTable.get_table(book)
        table.remove(self)
        Event.gen(self, EVENT_DESTROY)
        del self

    def commit_edit(self):
        if not super().commit_edit(): return
        self.commit_edit_part2(self.on_error, self.noop, self.comm_free)

    def mark_dirty(self):
        self.set_dirty()
        Event.gen(self, EVENT_MODIFY)

    def equal(self, other):
        if self == other: return True
        if other is None or self is None:
            return False
        same_book = self.book == other.book
        if (same_book and self.get_namespace_ds() != self.get_namespace_ds()) or (
                not same_book and self.get_namespace() != other.get_namespace()):
            return False
        if self.mnemonic != other.mnemonic: return False
        if self.fullname != other.fullname: return False
        if self.cusip != other.cusip: return False
        if self.fraction != other.fraction: return False
        return True

    def equiv(self, other):
        if self == other: return True
        if other is None or self is None:
            return False
        if self.namespace != other.namespace: return False
        if self.mnemonic != other.mnemonic: return False
        return True

    def compare(self, other):
        if self.equal(other):
            return 0
        return 1

    def refers_to(self, other):
        return False


class CommodityTable(Instance):
    def refers_to(self, other):
        pass

    def __init__(self, book):
        super().__init__(ID_COMMODITY_TABLE, book)
        self.ns_table = {}

    @classmethod
    def book_begin(cls, book):
        if cls.get_table(book) is not None:
            return
        a = cls(book)
        book.set_data(ID_COMMODITY_TABLE, a)
        a.add_default_data(book)

    @staticmethod
    def get_table(book):
        if book is None:
            return None
        return book.get_data(ID_COMMODITY_TABLE)

    @staticmethod
    def map_namespace(name_space):
        if name_space == COMMODITY_NAMESPACE_NAME_ISO:
            return COMMODITY_NAMESPACE_NAME_CURRENCY
        return name_space

    def add_namespace(self, namespace, book):
        if namespace is None or namespace == "":
            raise ValueError("NameSpace cant be None")
        namespace = CommodityTable.map_namespace(namespace)
        ns = self.find_namespace(namespace)
        if ns is None:
            ns = CommodityNameSpace(book)
            ns.name = namespace
            ns.cm_table = {}
            ns.iso4217 = CommodityNameSpace.is_iso(namespace)
            Event.gen(ns, EVENT_CREATE)
            if ns.name:
                self.ns_table[ns.name] = ns
            Event.gen(ns, EVENT_ADD)
        return ns

    def find_namespace(self, name_space):
        if name_space is None:
            return
        name_space = CommodityTable.map_namespace(name_space)
        return self.ns_table.get(name_space, None)

    def delete_namespace(self, name_space):
        ns = self.find_namespace(name_space)
        if ns is None:
            return
        Event.gen(ns, EVENT_REMOVE)
        self.ns_table.pop(name_space)

    def add_default_data(self, book):
        self.add_namespace(COMMODITY_NAMESPACE_NAME_AMEX, book)
        self.add_namespace(COMMODITY_NAMESPACE_NAME_NYSE, book)
        self.add_namespace(COMMODITY_NAMESPACE_NAME_NASDAQ, book)
        self.add_namespace(COMMODITY_NAMESPACE_NAME_EUREX, book)
        self.add_namespace(COMMODITY_NAMESPACE_NAME_MUTUAL, book)
        self.add_namespace(COMMODITY_NAMESPACE_NAME_TEMPLATE, book)
        c = Commodity(book, fullname="template", namespace=COMMODITY_NAMESPACE_NAME_TEMPLATE,
                      mnemonic="template",cusip="template",fraction= 1)
        c.mark_clean()
        self.insert(c)
        c = Commodity(book, fullname="Andorran Franc", mnemonic="ADF", cusip="950")
        self.insert(c)
        c.set_default_symbol("₣")

        c = Commodity(book, fullname="Andorran Peseta", mnemonic="ADP", cusip="724")
        self.insert(c)
        c.set_default_symbol("₧")

        c = Commodity(book, fullname="UAE Dirham", mnemonic="AED", cusip="784")
        self.insert(c)
        c.set_default_symbol("Dhs")

        c = Commodity(book, fullname="Afghani", mnemonic="AFA", cusip="004")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Afghani", mnemonic="AFN", cusip="971", fraction=1)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Lek", mnemonic="ALL", cusip="008")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Armenian Dram", mnemonic="AMD", cusip="051")
        self.insert(c)
        c.set_default_symbol("դր.")

        c = Commodity(book, fullname="Netherlands Antillian Guilder", mnemonic="ANG", cusip="532")
        self.insert(c)
        c.set_default_symbol("NAƒ")

        c = Commodity(book, fullname="Kwanza", mnemonic="AOA", cusip="973")
        self.insert(c)
        c.set_default_symbol("Kz")

        c = Commodity(book, fullname="Angola New Kwanza", mnemonic="AON", cusip="024")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Angola Kwanza Reajustado", mnemonic="AOR", cusip="982")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Argentine Austral", mnemonic="ARA", cusip="XXX")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Argentine Peso", mnemonic="ARS", cusip="032")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Austrian Schilling", mnemonic="ATS", cusip="040")
        self.insert(c)
        c.set_default_symbol("öS")

        c = Commodity(book, fullname="Australian Dollar", mnemonic="AUD", cusip="036")
        self.insert(c)
        c.set_default_symbol("$")

        c = Commodity(book, fullname="Aruban Guilder", mnemonic="AWG", cusip="533")
        self.insert(c)
        c.set_default_symbol("Afl.")

        c = Commodity(book, fullname="Azerbaijanian Manat", mnemonic="AZM", cusip="031")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Azerbaijanian Manat", mnemonic="AZN", cusip="944")
        self.insert(c)
        c.set_default_symbol("m")

        c = Commodity(book, fullname="Bosnia and Herzegovina Dinar", mnemonic="BAD", cusip="070")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Convertible Marks", mnemonic="BAM", cusip="977")
        self.insert(c)
        c.set_default_symbol("KM")

        c = Commodity(book, fullname="Barbados Dollar", mnemonic="BBD", cusip="052")
        self.insert(c)
        c.set_default_symbol("Bds$")

        c = Commodity(book, fullname="Taka", mnemonic="BDT", cusip="050")
        self.insert(c)
        c.set_default_symbol("৳")

        c = Commodity(book, fullname="Belgian Franc", mnemonic="BEF", cusip="056")
        self.insert(c)
        c.set_default_symbol("fr.")

        c = Commodity(book, fullname="Bulgarian Lev A/99", mnemonic="BGL", cusip="100")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Bulgarian Lev", mnemonic="BGN", cusip="975")
        self.insert(c)
        c.set_default_symbol("лв")

        c = Commodity(book, fullname="Bahraini Dinar", mnemonic="BHD", cusip="048", fraction=1000)
        self.insert(c)
        c.set_default_symbol("BD")

        c = Commodity(book, fullname="Burundi Franc", mnemonic="BIF", cusip="108")
        self.insert(c)
        c.set_default_symbol("FBu")

        c = Commodity(book, fullname="Bermudian Dollar", mnemonic="BMD", cusip="060")
        self.insert(c)
        c.set_default_symbol("BD$")

        c = Commodity(book, fullname="Brunei Dollar", mnemonic="BND", cusip="096")
        self.insert(c)
        c.set_default_symbol("B$")

        c = Commodity(book, fullname="Boliviano", mnemonic="BOB", cusip="068")
        self.insert(c)
        c.set_default_symbol("Bs.")

        c = Commodity(book, fullname="Mvdol", mnemonic="BOV", cusip="984")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Brazilian Cruzeiro", mnemonic="BRE", cusip="076")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Brazilian Real", mnemonic="BRL", cusip="986")
        self.insert(c)
        c.set_default_symbol("R$")

        c = Commodity(book, fullname="Brazilian Cruzeiro Real", mnemonic="BRR", cusip="987")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Bahamian Dollar", mnemonic="BSD", cusip="044")
        self.insert(c)
        c.set_default_symbol("B$")

        c = Commodity(book, fullname="Ngultrum", mnemonic="BTN", cusip="064")
        self.insert(c)
        c.set_default_symbol("Nu.")

        c = Commodity(book, fullname="Pula", mnemonic="BWP", cusip="072")
        self.insert(c)
        c.set_default_symbol("P")

        c = Commodity(book, fullname="Belarussian Rouble", mnemonic="BYB", cusip="", fraction=1)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Belarussian Ruble", mnemonic="BYR", cusip="974")
        self.insert(c)
        c.set_default_symbol("Br")

        c = Commodity(book, fullname="Belarussian Ruble", mnemonic="BYN", cusip="933")
        self.insert(c)
        c.set_default_symbol("Br")

        c = Commodity(book, fullname="Belize Dollar", mnemonic="BZD", cusip="084")
        self.insert(c)
        c.set_default_symbol("BZ$")

        c = Commodity(book, fullname="Canadian Dollar", mnemonic="CAD", cusip="124")
        self.insert(c)
        c.set_default_symbol("C$")

        c = Commodity(book, fullname="Franc Congolais", mnemonic="CDF", cusip="976")
        self.insert(c)
        c.set_default_symbol("FC")

        c = Commodity(book, fullname="WIR Euro", mnemonic="CHE", cusip="974")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Swiss Franc", mnemonic="CHF", cusip="756")
        self.insert(c)
        c.set_default_symbol("SFr.")

        c = Commodity(book, fullname="WIR Franc", mnemonic="CHW", cusip="948")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Unidades de fomento", mnemonic="CLF", cusip="990",
                      fraction=10000)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Chilean Peso", mnemonic="CLP", cusip="152", fraction=1)
        self.insert(c)
        c.set_default_symbol("$")

        c = Commodity(book, fullname="Yuan Renminbi", mnemonic="CNY", cusip="156")
        self.insert(c)
        c.set_default_symbol("CN¥")

        c = Commodity(book, fullname="Colombian Peso", mnemonic="COP", cusip="170")
        self.insert(c)
        c.set_default_symbol("$")

        c = Commodity(book, fullname="Unidad de Valor Real", mnemonic="COU", cusip="970")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Costa Rican Colon", mnemonic="CRC", cusip="188")
        self.insert(c)
        c.set_default_symbol("₡")

        c = Commodity(book, fullname="Cuban Peso", mnemonic="CUP", cusip="192")
        self.insert(c)
        c.set_default_symbol("$MN")

        c = Commodity(book, fullname="Cuban Convertible Peso", mnemonic="CUC", cusip="931")
        self.insert(c)
        c.set_default_symbol("CUC$")

        c = Commodity(book, fullname="Cape Verde Escudo", mnemonic="CVE", cusip="132")
        self.insert(c)
        c.set_default_symbol("Esc")

        c = Commodity(book, fullname="Cyprus Pound", mnemonic="CYP", cusip="196")
        self.insert(c)
        c.set_default_symbol("£")

        c = Commodity(book, fullname="Czech Koruna", mnemonic="CZK", cusip="203")
        self.insert(c)
        c.set_default_symbol("Kč")

        c = Commodity(book, fullname="Deutsche Mark", mnemonic="DEM", cusip="280")
        self.insert(c)
        c.set_default_symbol("DM")

        c = Commodity(book, fullname="Djibouti Franc", mnemonic="DJF", cusip="262", fraction=1)
        self.insert(c)
        c.set_default_symbol("Fdj")

        c = Commodity(book, fullname="Danish Krone", mnemonic="DKK", cusip="208")
        self.insert(c)
        c.set_default_symbol("kr")

        c = Commodity(book, fullname="Dominican Peso", mnemonic="DOP", cusip="214")
        self.insert(c)
        c.set_default_symbol("RD$")

        c = Commodity(book, fullname="Algerian Dinar", mnemonic="DZD", cusip="012")
        self.insert(c)
        c.set_default_symbol("DA")

        c = Commodity(book, fullname="Ecuador Sucre", mnemonic="ECS", cusip="218")
        self.insert(c)
        c.set_default_symbol("S/.")

        c = Commodity(book, fullname="Kroon", mnemonic="EEK", cusip="233")
        self.insert(c)
        c.set_default_symbol("kr")

        c = Commodity(book, fullname="Egyptian Pound", mnemonic="EGP", cusip="818")
        self.insert(c)
        c.set_default_symbol("£E")

        c = Commodity(book, fullname="Nakfa", mnemonic="ERN", cusip="232")
        self.insert(c)
        c.set_default_symbol("Nfa")

        c = Commodity(book, fullname="Spanish Peseta", mnemonic="ESP", cusip="724")
        self.insert(c)
        c.set_default_symbol("₧")

        c = Commodity(book, fullname="Ethiopian Birr", mnemonic="ETB", cusip="230")
        self.insert(c)
        c.set_default_symbol("Br")

        c = Commodity(book, fullname="Euro", mnemonic="EUR", cusip="978")
        self.insert(c)
        c.set_default_symbol("€")

        c = Commodity(book, fullname="Finnish Markka", mnemonic="FIM", cusip="246")
        self.insert(c)
        c.set_default_symbol("mk")

        c = Commodity(book, fullname="Fiji Dollar", mnemonic="FJD", cusip="242")
        self.insert(c)
        c.set_default_symbol("FJ$")

        c = Commodity(book, fullname="Falkland Islands Pound", mnemonic="FKP", cusip="238")
        self.insert(c)
        c.set_default_symbol("FK£")

        c = Commodity(book, fullname="French Franc", mnemonic="FRF", cusip="250")
        self.insert(c)
        c.set_default_symbol("₣")

        c = Commodity(book, fullname="Pound Sterling", mnemonic="GBP", cusip="826")
        self.insert(c)
        c.set_default_symbol("£")

        c = Commodity(book, fullname="Lari", mnemonic="GEL", cusip="981")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Cedi", mnemonic="GHC", cusip="288")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Ghana Cedi", mnemonic="GHS", cusip="936")
        self.insert(c)
        c.set_default_symbol("GH₵")

        c = Commodity(book, fullname="Gibraltar Pound", mnemonic="GIP", cusip="292")
        self.insert(c)
        c.set_default_symbol("£")

        c = Commodity(book, fullname="Dalasi", mnemonic="GMD", cusip="270")
        self.insert(c)
        c.set_default_symbol("D")

        c = Commodity(book, fullname="Guinea Franc", mnemonic="GNF", cusip="324")
        self.insert(c)
        c.set_default_symbol("FG")

        c = Commodity(book, fullname="Greek Drachma", mnemonic="GRD", cusip="200")
        self.insert(c)
        c.set_default_symbol("Δρ.")

        c = Commodity(book, fullname="Quetzal", mnemonic="GTQ", cusip="320")
        self.insert(c)
        c.set_default_symbol("Q")

        c = Commodity(book, fullname="Guinea-Bissau Peso", mnemonic="GWP", cusip="624")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Guyana Dollar", mnemonic="GYD", cusip="328")
        self.insert(c)
        c.set_default_symbol("G$")

        c = Commodity(book, fullname="Hong Kong Dollar", mnemonic="HKD", cusip="344")
        self.insert(c)
        c.set_default_symbol("HK$")

        c = Commodity(book, fullname="Lempira", mnemonic="HNL", cusip="340")
        self.insert(c)
        c.set_default_symbol("L")

        c = Commodity(book, fullname="Croatian Kuna", mnemonic="HRK", cusip="191")
        self.insert(c)
        c.set_default_symbol("kn")

        c = Commodity(book, fullname="Gourde", mnemonic="HTG", cusip="332")
        self.insert(c)
        c.set_default_symbol("G")

        c = Commodity(book, fullname="Forint", mnemonic="HUF", cusip="348")
        self.insert(c)
        c.set_default_symbol("Ft")

        c = Commodity(book, fullname="Rupiah", mnemonic="IDR", cusip="360")
        self.insert(c)
        c.set_default_symbol("Rp")

        c = Commodity(book, fullname="Irish Pound", mnemonic="IEP", cusip="372")
        self.insert(c)
        c.set_default_symbol("£")

        c = Commodity(book, fullname="New Israeli Sheqel", mnemonic="ILS", cusip="376")
        self.insert(c)
        c.set_default_symbol("₪")

        c = Commodity(book, fullname="Indian Rupee", mnemonic="INR", cusip="356")
        self.insert(c)
        c.set_default_symbol("₹")

        c = Commodity(book, fullname="Iraqi Dinar", mnemonic="IQD", cusip="368", fraction=1000)
        self.insert(c)
        c.set_default_symbol("ع.د")

        c = Commodity(book, fullname="Iranian Rial", mnemonic="IRR", cusip="364", fraction=1)
        self.insert(c)
        c.set_default_symbol("﷼")

        c = Commodity(book, fullname="Iceland Krona", mnemonic="ISK", cusip="352")
        self.insert(c)
        c.set_default_symbol("kr")

        c = Commodity(book, fullname="Italian Lira", mnemonic="ITL", cusip="380", fraction=1)
        self.insert(c)
        c.set_default_symbol("₤")

        c = Commodity(book, fullname="Jamaican Dollar", mnemonic="JMD", cusip="388")
        self.insert(c)
        c.set_default_symbol("J$")

        c = Commodity(book, fullname="Jordanian Dinar", mnemonic="JOD", cusip="400", fraction=1000)
        self.insert(c)
        c.set_default_symbol("JD")

        c = Commodity(book, fullname="Yen", mnemonic="JPY", cusip="392", fraction=1)
        self.insert(c)
        c.set_default_symbol("JP¥")

        c = Commodity(book, fullname="Kenyan Shilling", mnemonic="KES", cusip="404")
        self.insert(c)
        c.set_default_symbol("Ksh")

        c = Commodity(book, fullname="Som", mnemonic="KGS", cusip="417")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Riel", mnemonic="KHR", cusip="116")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Comoro Franc", mnemonic="KMF", cusip="174", fraction=1)
        self.insert(c)
        c.set_default_symbol("FC")

        c = Commodity(book, fullname="North Korean Won", mnemonic="KPW", cusip="408")
        self.insert(c)
        c.set_default_symbol("₩")

        c = Commodity(book, fullname="Won", mnemonic="KRW", cusip="410")
        self.insert(c)
        c.set_default_symbol("₩")

        c = Commodity(book, fullname="Kuwaiti Dinar", mnemonic="KWD", cusip="414", fraction=1000)
        self.insert(c)
        c.set_default_symbol("د.ك")

        c = Commodity(book, fullname="Cayman Islands Dollar", mnemonic="KYD", cusip="136")
        self.insert(c)
        c.set_default_symbol("CI$")

        c = Commodity(book, fullname="Tenge", mnemonic="KZT", cusip="398")
        self.insert(c)
        c.set_default_symbol("₸")

        c = Commodity(book, fullname="Kip", mnemonic="LAK", cusip="418")
        self.insert(c)
        c.set_default_symbol("₭")

        c = Commodity(book, fullname="Lebanese Pound", mnemonic="LBP", cusip="422")
        self.insert(c)
        c.set_default_symbol("ل.ل")

        c = Commodity(book, fullname="Sri Lanka Rupee", mnemonic="LKR", cusip="144")
        self.insert(c)
        c.set_default_symbol("₨")

        c = Commodity(book, fullname="Liberian Dollar", mnemonic="LRD", cusip="430")
        self.insert(c)
        c.set_default_symbol("L$")

        c = Commodity(book, fullname="Loti", mnemonic="LSL", cusip="426")
        self.insert(c)
        c.set_default_symbol("M")

        c = Commodity(book, fullname="Lithuanian Litas", mnemonic="LTL", cusip="440")
        self.insert(c)
        c.set_default_symbol("Lt")

        c = Commodity(book, fullname="Luxembourg Franc", mnemonic="LUF", cusip="442")
        self.insert(c)
        c.set_default_symbol("Flux")

        c = Commodity(book, fullname="Latvian Lats", mnemonic="LVL", cusip="428")
        self.insert(c)
        c.set_default_symbol("Ls")

        c = Commodity(book, fullname="Libyan Dinar", mnemonic="LYD", cusip="434", fraction=1000)
        self.insert(c)
        c.set_default_symbol("ل.د")

        c = Commodity(book, fullname="Moroccan Dirham", mnemonic="MAD", cusip="504")
        self.insert(c)
        c.set_default_symbol("د.م")

        c = Commodity(book, fullname="Moldovan Leu", mnemonic="MDL", cusip="498")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Malagasy Ariary", mnemonic="MGA", cusip="969", fraction=5)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Malagasy Franc", mnemonic="MGF", cusip="450", fraction=500)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Denar", mnemonic="MKD", cusip="807")
        self.insert(c)
        c.set_default_symbol("ден")

        c = Commodity(book, fullname="Mali Franc", mnemonic="MLF", cusip="466")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Kyat", mnemonic="MMK", cusip="104")
        self.insert(c)
        c.set_default_symbol("K")

        c = Commodity(book, fullname="Tugrik", mnemonic="MNT", cusip="496")
        self.insert(c)
        c.set_default_symbol("₮")

        c = Commodity(book, fullname="Pataca", mnemonic="MOP", cusip="446")
        self.insert(c)
        c.set_default_symbol("MOP$")

        c = Commodity(book, fullname="Ouguiya", mnemonic="MRO", cusip="478")
        self.insert(c)
        c.set_default_symbol("UM")

        c = Commodity(book, fullname="Ouguiya", mnemonic="MRU", cusip="929")
        self.insert(c)
        c.set_default_symbol("UM")

        c = Commodity(book, fullname="Maltese Lira", mnemonic="MTL", cusip="470")
        self.insert(c)
        c.set_default_symbol("Lm")

        c = Commodity(book, fullname="Mauritius Rupee", mnemonic="MUR", cusip="480")
        self.insert(c)
        c.set_default_symbol("₨")

        c = Commodity(book, fullname="Rufiyaa", mnemonic="MVR", cusip="462")
        self.insert(c)
        c.set_default_symbol(".ރ")

        c = Commodity(book, fullname="Kwacha", mnemonic="MWK", cusip="454")
        self.insert(c)
        c.set_default_symbol("MK")

        c = Commodity(book, fullname="Mexican Peso", mnemonic="MXN", cusip="484")
        self.insert(c)
        c.set_default_symbol("Mex$")

        c = Commodity(book, fullname="Mexican Unidad de Inversion (UDI)", mnemonic="MXV",
                      cusip="979")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Malaysian Ringgit", mnemonic="MYR", cusip="458")
        self.insert(c)
        c.set_default_symbol("RM")

        c = Commodity(book, fullname="Mozambique Metical", mnemonic="MZM", cusip="508")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Metical", mnemonic="MZN", cusip="943")
        self.insert(c)
        c.set_default_symbol("MTn")

        c = Commodity(book, fullname="Namibia Dollar", mnemonic="NAD", cusip="516")
        self.insert(c)
        c.set_default_symbol("N$")

        c = Commodity(book, fullname="Naira", mnemonic="NGN", cusip="566")
        self.insert(c)
        c.set_default_symbol("₦")

        c = Commodity(book, fullname="Nicaraguan Cordoba", mnemonic="NIC", cusip="558")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Cordoba Oro", mnemonic="NIO", cusip="558")
        self.insert(c)
        c.set_default_symbol("C$")

        c = Commodity(book, fullname="Netherlands Guilder", mnemonic="NLG", cusip="528")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Norwegian Krone", mnemonic="NOK", cusip="578")
        self.insert(c)
        c.set_default_symbol("kr")

        c = Commodity(book, fullname="Nepalese Rupee", mnemonic="NPR", cusip="524")
        self.insert(c)
        c.set_default_symbol("₨")

        c = Commodity(book, fullname="New Zealand Dollar", mnemonic="NZD", cusip="554")
        self.insert(c)
        c.set_default_symbol("NZ$")

        c = Commodity(book, fullname="Rial Omani", mnemonic="OMR", cusip="512", fraction=1000)
        self.insert(c)
        c.set_default_symbol("ر.ع.")

        c = Commodity(book, fullname="Balboa", mnemonic="PAB", cusip="590")
        self.insert(c)
        c.set_default_symbol("฿")

        c = Commodity(book, fullname="Nuevo Sol", mnemonic="PEN", cusip="604")
        self.insert(c)
        c.set_default_symbol("S/.")

        c = Commodity(book, fullname="Kina", mnemonic="PGK", cusip="598")
        self.insert(c)
        c.set_default_symbol("K")

        c = Commodity(book, fullname="Philippine Peso", mnemonic="PHP", cusip="608")
        self.insert(c)
        c.set_default_symbol("₱")

        c = Commodity(book, fullname="Pakistan Rupee", mnemonic="PKR", cusip="586")
        self.insert(c)
        c.set_default_symbol("Rs")

        c = Commodity(book, fullname="Zloty", mnemonic="PLN", cusip="985")
        self.insert(c)
        c.set_default_symbol("zł")

        c = Commodity(book, fullname="Portuguese Escudo", mnemonic="PTE", cusip="620")
        self.insert(c)
        c.set_default_symbol("$")

        c = Commodity(book, fullname="Guarani", mnemonic="PYG", cusip="600")
        self.insert(c)
        c.set_default_symbol("₲")

        c = Commodity(book, fullname="Qatari Rial", mnemonic="QAR", cusip="634")
        self.insert(c)
        c.set_default_symbol("ر.ق")

        c = Commodity(book, fullname="Romanian Old Leu", mnemonic="ROL", cusip="642")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="New Leu", mnemonic="RON", cusip="946")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Serbian Dinar", mnemonic="RSD", cusip="941")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Russian Rouble", mnemonic="RUB", cusip="643")
        self.insert(c)
        c.set_default_symbol("₽")

        c = Commodity(book, fullname="Rwanda Franc", mnemonic="RWF", cusip="646")
        self.insert(c)
        c.set_default_symbol("RF")

        c = Commodity(book, fullname="Saudi Riyal", mnemonic="SAR", cusip="682")
        self.insert(c)
        c.set_default_symbol("ر.س")

        c = Commodity(book, fullname="Solomon Islands Dollar", mnemonic="SBD", cusip="090")
        self.insert(c)
        c.set_default_symbol("SI$")

        c = Commodity(book, fullname="Seychelles Rupee", mnemonic="SCR", cusip="690")
        self.insert(c)
        c.set_default_symbol("SR")

        c = Commodity(book, fullname="Sudanese Dinar", mnemonic="SDD", cusip="736")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Sudanese Pound", mnemonic="SDG", cusip="938")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Sudanese Pound", mnemonic="SDP", cusip="736")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Swedish Krona", mnemonic="SEK", cusip="752")
        self.insert(c)
        c.set_default_symbol("kr")

        c = Commodity(book, fullname="Singapore Dollar", mnemonic="SGD", cusip="702")
        self.insert(c)
        c.set_default_symbol("S$")

        c = Commodity(book, fullname="Saint Helena Pound", mnemonic="SHP", cusip="654")
        self.insert(c)
        c.set_default_symbol("£")

        c = Commodity(book, fullname="Slovenian Tolar", mnemonic="SIT", cusip="705")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Slovak Koruna", mnemonic="SKK", cusip="703")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Leone", mnemonic="SLL", cusip="694")
        self.insert(c)
        c.set_default_symbol("Le")

        c = Commodity(book, fullname="Somali Shilling", mnemonic="SOS", cusip="706")
        self.insert(c)
        c.set_default_symbol("SoSh")

        c = Commodity(book, fullname="Surinam Dollar", mnemonic="SRD", cusip="968")
        self.insert(c)
        c.set_default_symbol("$")

        c = Commodity(book, fullname="Suriname Guilder", mnemonic="SRG", cusip="740")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Dobra", mnemonic="STD", cusip="678")
        self.insert(c)
        c.set_default_symbol("Db")

        c = Commodity(book, fullname="El Salvador Colon", mnemonic="SVC", cusip="222")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Syrian Pound", mnemonic="SYP", cusip="760")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Lilangeni", mnemonic="SZL", cusip="748")
        self.insert(c)
        c.set_default_symbol("E")

        c = Commodity(book, fullname="Baht", mnemonic="THB", cusip="764")
        self.insert(c)
        c.set_default_symbol("฿")

        c = Commodity(book, fullname="Tajik Rouble", mnemonic="TJR", cusip="762", fraction=1)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Somoni", mnemonic="TJS", cusip="972")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Manat", mnemonic="TMM", cusip="795")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Manat", mnemonic="TMT", cusip="934")
        self.insert(c)
        c.set_default_symbol("m")

        c = Commodity(book, fullname="Tunisian Dinar", mnemonic="TND", cusip="788", fraction=1000)
        self.insert(c)
        c.set_default_symbol("د.ت")

        c = Commodity(book, fullname="Pa'anga", mnemonic="TOP", cusip="776")
        self.insert(c)
        c.set_default_symbol("T$")

        c = Commodity(book, fullname="Turkish Lira", mnemonic="TRY", cusip="949")
        self.insert(c)
        c.set_default_symbol("₺")

        c = Commodity(book, fullname="Trinidad and Tobago Dollar", mnemonic="TTD", cusip="780")
        self.insert(c)
        c.set_default_symbol("TT$")

        c = Commodity(book, fullname="New Taiwan Dollar", mnemonic="TWD", cusip="901")
        self.insert(c)
        c.set_default_symbol("NT$")

        c = Commodity(book, fullname="Tanzanian Shilling", mnemonic="TZS", cusip="834")
        self.insert(c)
        c.set_default_symbol("/")

        c = Commodity(book, fullname="Hryvnia", mnemonic="UAH", cusip="980")
        self.insert(c)
        c.set_default_symbol("₴")

        c = Commodity(book, fullname="Uganda Shilling", mnemonic="UGX", cusip="800")
        self.insert(c)
        c.set_default_symbol("USh")

        c = Commodity(book, fullname="US Dollar", mnemonic="USD", cusip="840")
        self.insert(c)
        c.set_default_symbol("$")

        c = Commodity(book, fullname="US Dollar (Next day)", mnemonic="USN", cusip="997")
        self.insert(c)
        c.set_default_symbol("$n")

        c = Commodity(book, fullname="US Dollar (Same day)", mnemonic="USS", cusip="998")
        self.insert(c)
        c.set_default_symbol("$s")

        c = Commodity(book, fullname="Uruguay Peso en Unidades Indexadas", mnemonic="UYI",
                      cusip="940")
        self.insert(c)
        c.set_default_symbol("UI")

        c = Commodity(book, fullname="Peso Uruguayo", mnemonic="UYU", cusip="858")
        self.insert(c)
        c.set_default_symbol("$U")

        c = Commodity(book, fullname="Uzbekistan Sum", mnemonic="UZS", cusip="860")
        self.insert(c)
        c.set_default_symbol("som")

        c = Commodity(book, fullname="Venezuela Bolívar", mnemonic="VEB", cusip="862")
        self.insert(c)
        c.set_default_symbol("Bs.")

        c = Commodity(book, fullname="Bolivar Fuerte", mnemonic="VEF", cusip="937")
        self.insert(c)
        c.set_default_symbol("BsF.")

        c = Commodity(book, fullname="Bolivar Soberano", mnemonic="VES", cusip="928")
        self.insert(c)
        c.set_default_symbol("BsS.")

        c = Commodity(book, fullname="Dong", mnemonic="VND", cusip="704", fraction=1)
        self.insert(c)
        c.set_default_symbol("₫")

        c = Commodity(book, fullname="Vatu", mnemonic="VUV", cusip="548", fraction=1)
        self.insert(c)
        c.set_default_symbol("Vt")

        c = Commodity(book, fullname="Tala", mnemonic="WST", cusip="882")
        self.insert(c)
        c.set_default_symbol("WS$")

        c = Commodity(book, fullname="Yemeni Rial", mnemonic="YER", cusip="886")
        self.insert(c)
        c.set_default_symbol("Rl")

        c = Commodity(book, fullname="Yugoslavian Dinar", mnemonic="YUM", cusip="890")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Rand", mnemonic="ZAR", cusip="710")
        self.insert(c)
        c.set_default_symbol("R")

        c = Commodity(book, fullname="Kwacha (old)", mnemonic="ZMK", cusip="894")
        self.insert(c)
        c.set_default_symbol("ZK")

        c = Commodity(book, fullname="Zambian Kwacha", mnemonic="ZMW", cusip="967")
        self.insert(c)
        c.set_default_symbol("ZK")

        c = Commodity(book, fullname="Zimbabwe Dollar", mnemonic="ZWD", cusip="716")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Zimbabwe Dollar", mnemonic="ZWL", cusip="716")
        self.insert(c)
        c.set_default_symbol("Z.$")

        c = Commodity(book, fullname="CFA Franc BEAC", mnemonic="XAF", cusip="950")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="East Caribbean Dollar", mnemonic="XCD", cusip="951")
        self.insert(c)
        c.set_default_symbol("EC$")

        c = Commodity(book, fullname="SDR", mnemonic="XDR", cusip="960", fraction=1)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Gold-Franc", mnemonic="XFO", cusip="nil")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="UIC-Franc", mnemonic="XFU", cusip="nil")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="CFA Franc BCEAO", mnemonic="XOF", cusip="952")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="CFP Franc", mnemonic="XPF", cusip="953")
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Code for testing purposes", mnemonic="XTS", cusip="963",
                      fraction=1000000)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="No currency", mnemonic="XXX", cusip="999", fraction=1000000)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Silver", mnemonic="XAG", cusip="961", fraction=1000000)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Gold", mnemonic="XAU", cusip="959", fraction=1000000)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Palladium", mnemonic="XPD", cusip="964", fraction=1000000)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Platinum", mnemonic="XPT", cusip="962", fraction=1000000)
        self.insert(c)
        c.set_default_symbol("")

        c = Commodity(book, fullname="Sistema Unitario de Compensación Regional", mnemonic="XSU",
                      cusip="994", fraction=1)
        self.insert(c)
        c.set_default_symbol("")
        col = book.get_collection(ID_COMMODITY)
        col.mark_clean()
        col = book.get_collection(ID_COMMODITY_NAMESPACE)
        col.mark_clean()
        return True

    def lookup(self, name_space, mnemonic):
        if name_space is None or mnemonic is None:
            return None
        nsp = self.find_namespace(name_space)

        if nsp is not None:
            if nsp.iso4217:
                for p in gs_new_iso_codes:
                    if mnemonic == p.old_code:
                        mnemonic = p.new_code
                        break
            return nsp.cm_table.get(mnemonic, None)
        return None

    def insert(self, comm):
        if comm is None:
            return
        ns_name = comm.get_namespace()
        c = self.lookup(ns_name, comm.get_mnemonic())
        if c is not None:
            if c == comm:
                return comm
            if comm.get_namespace_ds().iso4217:
                for p in gs_new_iso_codes:
                    if not comm.get_mnemonic() or comm.mnemonic == p.old_code:
                        comm.set_mnemonic(p.new_code)
                        break
        if ns_name == COMMODITY_NAMESPACE_NAME_TEMPLATE and comm.mnemonic != "template":
            comm.set_namespace("User")
            ns_name = "User"
        book = comm.get_book()
        nsp = self.add_namespace(ns_name, book)
        nsp.cm_table[comm.mnemonic] = comm
        Event.gen(comm, EVENT_ADD)
        return comm

    def remove(self, comm):
        if comm is None:
            return
        ns_name = comm.get_namespace_ds().get_name()
        c = self.lookup(ns_name, comm.mnemonic)

        if c != comm: return
        Event.gen(comm, EVENT_REMOVE)
        nsp = self.find_namespace(ns_name)
        if nsp is None: return
        nsp.cm_table.pop(comm.mnemonic)

    def has_namespace(self, name_space):
        if name_space is None:
            return 0
        nsp = self.find_namespace(name_space)
        return 1 if nsp else 0

    def get_namespaces(self):
        return list(self.ns_table.keys())

    def get_namespaces_list(self):
        return list(self.ns_table.values())

    def get_all_noncurrency_commodity(self):
        retval = []
        nslist = self.get_namespaces()
        for node in nslist:
            if node == COMMODITY_NAMESPACE_NAME_CURRENCY or node == COMMODITY_NAMESPACE_NAME_TEMPLATE:
                continue
            ns = self.find_namespace(node)
            if ns is None:
                continue
            retval += ns.cm_table.values()
        return retval

    def get_commodities(self, name_space):
        if name_space == COMMODITY_NAMESPACE_NAME_NONCURRENCY:
            return self.get_all_noncurrency_commodity()
        ns = self.find_namespace(name_space)
        if ns is None:
            raise ValueError("Valid NameSpace Required to get commodities")
        return list(ns.cm_table.values())

    def foreach_commodity(self, func, *user_data):
        pass

    def get_quotable_commodity(self, expression=None):
        from re import compile, fullmatch, error, I, X
        nslist = self.get_namespaces()
        l = []
        if expression is not None:
            try:
                pattern = compile(expression, I | X)
                for namespace in nslist:
                    if fullmatch(pattern, namespace):
                        for comm in self.get_commodities(namespace):
                            qs = comm.get_quote_source()
                            if comm.quote_flag and qs is not None and qs.supported:
                                l.append(comm)
            except error:
                return l
        else:
            for namespace in nslist:
                for comm in self.get_commodities(namespace):
                    qs = comm.get_quote_source()
                    if comm.quote_flag and qs is not None and qs.supported:
                        l.append(comm)
        return l

    def find_full(self, name_space, fullname):
        if fullname is None or len(fullname) < 0:
            return
        for comm in self.get_commodities(name_space):
            if fullname == comm.get_printname():
                return comm

    def destroy(self):
        if self.ns_table is not None:
            self.ns_table.clear()
        self.ns_table = None
        del self

    def lookup_unique(self, unique_name: str):
        if unique_name.find("::") == -1: return None
        name_space, mnemonic = unique_name.split("::")
        commodity = self.lookup(name_space, mnemonic)
        return commodity

    @classmethod
    def register(cls):
        QuoteSource.init_tables()
        ObjectClass.register(cls, ID_COMMODITY_TABLE, None, [])
        ObjectClass.register(Commodity, ID_COMMODITY, None, [])
        ObjectClass.register(CommodityNameSpace, ID_COMMODITY_NAMESPACE, None, [])
