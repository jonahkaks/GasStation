import time
from collections import defaultdict
from os import environ

from finance_quote_python import Quote


class PriceQuotes:
    q = Quote()

    @classmethod
    def check_sources(cls):
        return cls.q.sources

    @classmethod
    def install_sources(cls):
        from .commodity import QuoteSource
        sources = cls.check_sources()
        QuoteSource.set_fq_installed(22, sources)

    @classmethod
    def get_quotes(cls, requests):
        # S = cls.q.currency('USD', 'UGX')
        print(requests)

    @classmethod
    def add_quotes(cls, success, error, warning, book, alphavantage_api_key='O2LJYJW1SL5INZSX'):
        from .commodity import CommodityTable
        from .price_db import PriceDB, Price, PriceSource
        commodity_table = CommodityTable.get_table(book)
        pricedb = PriceDB.get_db(book)

        def commodity_fq_call_data():
            h = defaultdict(list)
            for p in commodity_table.get_quotable_commodity():
                if p.is_currency():
                    if p.get_mnemonic() != 'XXX':
                        h["currency"].append(p)
                else:
                    h[p.get_quote_source().internal_name].append(p)
            return list(h.items())

        def fq_call_data_fq_calls(args):
            if args[0] == "currency":
                return list(map(lambda quote_item_info: ["currency", quote_item_info[0].get_mnemonic(),
                                                         quote_item_info[1].get_mnemonic()], args[1]))
            else:
                return [args[0], list(map(lambda quote_item_info: quote_item_info.get_mnemonic(), args[1]))]

        def fq_results_commod_tz_quote_triples(fq_call_data, fq_results):
            result_list = []

            def process_a_quote(call_data, call_result):
                pass

            def process_call_result_pair(call_data, call_result):
                pass

            if isinstance(fq_call_data, list) and isinstance(fq_results, list) and len(fq_call_data) == len(fq_results):
                for r in fq_results:
                    process_call_result_pair(fq_call_data, r)
                return reversed(result_list)

        def commodity_tz_quote_triple_price(commodity, time_zone, **quote_data):
            gs_time = quote_data.get("time_no_zone")
            currency_str = quote_data.get("currency")
            currency = commodity_table.lookup("ISO4217",
                                              currency_str.upper()) if commodity_table is not None and isinstance(
                currency_str, str) else None
            commodity_str = commodity.get_printname()
            if currency.get_printname() == commodity_str:
                symbol = quote_data.get("symbol")
                other_curr = commodity_table.lookup("ISO4217",
                                                    symbol.upper()) if commodity_table is not None and isinstance(
                    symbol, str) else None
                commodity = other_curr

            price_syms = ("last", "nav", "price")
            price_types = "last" "nav" "unknown"
            # (unless (null? price_syms)
            # (cond
            #  ((assq_ref quote_data (car price_syms)) =>
            # (lambda (p)
            #  (set! price (gnc_scm_to_numeric p))
            # (set! price_type (car price_types))))
            # (else (lp (cdr price_syms) (cdr price_types))))))

            if gs_time is not None:
                gs_time = time.strptime(gs_time, "%Y_%m_%d %H:%M:%S")
            else:
                gs_time = time.mktime(time.localtime())

            if not (commodity and currency and gs_time and price and price_type):
                return currency_str + ":" + commodity.get_mnemonic()
            else:
                saved_price = pricedb.lookup_day_t64(commodity, currency, gs_time)
                if saved_price is not None:
                    if saved_price.get_currency().equiv(commodity):

                        price = price.invert()
                    if saved_price.get_source() >= PriceSource.FQ:
                        saved_price.begin_edit()
                        saved_price.set_time64(gs_time)
                        saved_price.set_source(PriceSource.FQ)
                        saved_price.set_type(price_type)
                        saved_price.set_value(price)
                        saved_price.commit_edit()
                        return saved_price
                else:
                    price = Price(book)
                    price.begin_edit()
                    price.set_commodity(commodity)
                    price.set_currency(currency)
                    price.set_time64(gs_time)
                    price.set_source(PriceSource.FQ)
                    price.set_type(price_type)
                    price.set_value(price)
                    price.commit_edit()
                    return price

        def book_add_prices(prices):
            for price in prices:
                pricedb.add_price(price)
                price.unref()

        environ['ALPHAVANTAGE_API_KEY'] = alphavantage_api_key
        fq_call_data = commodity_fq_call_data()
        print(fq_call_data)
        fq_calls = list(map(fq_call_data_fq_calls, fq_call_data))
        print(fq_calls)
        fq_results = cls.get_quotes(fq_calls)
        print(fq_results)

