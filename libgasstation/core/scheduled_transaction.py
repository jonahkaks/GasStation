from typing import Any

from .account import Account, AccountType
from .helpers import gs_set_num_action
from .recurrence import *
from .scheduled_transaction_list import ScheduledTransactionList
from .transaction import *

SX_SHARES = "shares"
SX_FREQ_SPEC = "scheduled-frequency"
SX_NAME = "sched-xname"
SX_START_DATE = "sched-start-date"
SX_LAST_DATE = "sched-last-date"
SX_NUM_OCCUR = "sx-total-number"
SX_REM_OCCUR = "sx-remaining-num"


class SXTmpStateData:
    def __init__(self):
        self.last_date = 0
        self.num_occur_rem = 0
        self.num_inst = 0

    def clone(self):
        s = SXTmpStateData()
        s.last_date = self.last_date
        s.num_inst = self.num_inst
        s.num_occur_rem = self.num_occur_rem
        return s


class ScheduledTransactionRecurrence(DeclarativeBase):
    __tablename__ = 'scheduled_transaction_recurrence'
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True,nullable=False)
    scheduled_transaction_guid = Column("scheduled_transaction_guid", UUIDType(binary=False),
                                        ForeignKey("scheduled_transaction.guid"), nullable=False, primary_key=True)
    recurrence_guid = Column("recurrence_guid", UUIDType(binary=False),
                             ForeignKey("recurrence.guid"), nullable=False)
    recurrence = relationship("Recurrence")
    scheduled_transaction = relationship("ScheduledTransaction", back_populates="recurrences")


@dataclass
class ScheduledTransaction(DeclarativeBaseGuid):
    """
    A GasStation Scheduled Transaction.

    Attributes
        adv_creation (int) : days to create in advance (0 if disabled)
        adv_notify (int) : days to notify in advance (0 if disabled)
        auto_create (bool) :
        auto_notify (bool) :
        enabled (bool) :
        start_date (:class:`datetime.datetime`) : date to start the scheduled transaction
        last_occur (:class:`datetime.datetime`) : date of last occurence of the schedule transaction
        end_date (:class:`datetime.datetime`) : date to end the scheduled transaction (num/rem_occur should be 0)
        instance_count (int) :
        name (str) : name of the scheduled transaction
        num_occur (int) : number of occurences in total (end_date should be null)
        rem_occur (int) : number of remaining occurences (end_date should be null)
        template_account (:class:`core.account.Account`): template account of the transaction
    """

    __tablename__ = 'scheduled_transaction'

    # column definitions
    name: str = Column('name', VARCHAR(length=200))
    enabled = Column('enabled', BOOLEAN(), nullable=False, default=True)
    start_date: datetime.date = Column('start_date', Date(), nullable=False)
    end_date: datetime.date = Column('end_date', Date(), default=None)
    last_date: datetime.date = Column('last_occur', Date(), default=None)
    num_occurrences_total: int = Column('num_occur', INTEGER(), nullable=False, default=0)
    num_occurrences_remaining: int = Column('rem_occur', INTEGER(), nullable=False, default=0)
    auto_create_option: bool = Column('auto_create', BOOLEAN(), nullable=False, default=False)
    auto_create_notify: bool = Column('auto_notify', BOOLEAN(), nullable=False, default=False)
    advance_create_days: int = Column('adv_creation', INTEGER(), nullable=False, default=0)
    advance_remind_days: int = Column('adv_notify', INTEGER(), nullable=False, default=0)
    instance_num: int = Column('instance_count', INTEGER(), nullable=False, default=0)
    template_act_guid: UUID = Column('template_act_guid', UUIDType(binary=False), ForeignKey('account.guid'),
                                     nullable=False)

    template_account = relationship('Account')
    recurrences: List[ScheduledTransactionRecurrence] = relationship('ScheduledTransactionRecurrence',
                                                                     back_populates="scheduled_transaction",
                                                                     lazy="joined",
                                                                     cascade='all, delete-orphan',
                                                                     )

    def __init__(self, book):
        super().__init__(ID_SCHEDXACTION, book)
        self.enabled = 1
        self.num_occurrences_total = 0
        self.auto_create_option = False
        self.auto_create_notify = False
        self.advance_create_days = 0
        self.advance_remind_days = 0
        self.instance_num = 0
        self.deferredList = []
        self.template_account = Account(book)
        guid = self.get_guid()
        self.template_account.begin_edit()
        self.template_account.set_name(str(guid))
        from .commodity import CommodityTable, COMMODITY_NAMESPACE_NAME_TEMPLATE
        self.template_account.set_commodity(
            CommodityTable.get_table(book).lookup(COMMODITY_NAMESPACE_NAME_TEMPLATE, "template"))
        self.template_account.set_type(AccountType.BANK)
        ra = book.get_template_root()
        ra.append_child(self.template_account)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_SCHEDXACTION, book)
        self.infant = False
        self.deferredList = []
        ScheduledTransactionList(book).add(self)
        Event.gen(self, EVENT_CREATE)

    def __delete_template_transaction(self):
        templ_acct_splits = self.template_account.get_splits()
        templ_acct_transactions = []
        for curr_split in templ_acct_splits:
            split_trans = curr_split.get_transaction()
            if split_trans not in templ_acct_transactions:
                templ_acct_transactions.insert(0, split_trans)

        for tr in templ_acct_transactions:
            tr.begin_edit()
            tr.destroy()
            tr.commit_edit()

    def set_template_account(self, acc):
        old = self.template_account = acc
        self.template_account = acc
        if old is not None:
            old.destroy()

    def get_template_account(self):
        return self.template_account

    def get_schedule(self):
        return self.recurrences

    def set_schedule(self, schedule):
        self.begin_edit()
        self.recurrences = schedule
        self.set_dirty()
        self.commit_edit()

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.begin_edit()
        self.name = name
        self.set_dirty()
        self.commit_edit()

    def get_start_date(self):
        return self.start_date

    def set_start_date(self, sdate: datetime.date):
        self.begin_edit()
        self.start_date = sdate
        self.set_dirty()
        self.commit_edit()

    def get_end_date(self):
        return self.end_date

    def set_end_date(self, end_date: Any):
        self.begin_edit()
        self.end_date = end_date
        self.set_dirty()
        self.commit_edit()

    def get_last_occur_date(self):
        return self.last_date

    def set_last_occur_date(self, last_occur):
        self.begin_edit()
        self.last_date = last_occur
        self.set_dirty()
        self.commit_edit()

    def has_occur_def(self):
        return self.get_num_occur() != 0

    def get_num_occur(self):
        return self.num_occurrences_total

    def set_num_occur(self, new_num):
        if self.num_occurrences_total == new_num:
            return
        self.begin_edit()
        self.num_occurrences_remaining = self.num_occurrences_total = new_num
        self.set_dirty()
        self.commit_edit()

    def get_rem_occur(self):
        return self.num_occurrences_remaining

    def set_rem_occur(self, num_remain):
        if num_remain > self.num_occurrences_total:
            return
        else:
            if num_remain == self.num_occurrences_remaining:
                return
            self.begin_edit()
            self.num_occurrences_remaining = num_remain
            self.set_dirty()
            self.commit_edit()

    def get_enabled(self):
        return self.enabled

    def set_enabled(self, new_enabled):
        self.begin_edit()
        self.enabled = new_enabled
        self.set_dirty()
        self.commit_edit()

    def get_auto_create(self):
        return self.auto_create_option, self.auto_create_notify

    def set_auto_create(self, new_auto_create, new_notify):
        self.begin_edit()
        self.auto_create_option = new_auto_create
        self.auto_create_notify = new_notify
        self.set_dirty()
        self.commit_edit()

    def get_advance_creation(self):
        return self.advance_create_days

    def set_advance_creation(self, create_days):
        self.begin_edit()
        self.advance_create_days = create_days
        self.set_dirty()
        self.commit_edit()

    def get_advance_reminder(self):
        return self.advance_remind_days

    def set_advance_reminder(self, rem_days):
        self.begin_edit()
        self.advance_remind_days = rem_days
        self.set_dirty()
        self.commit_edit()

    def has_end_date(self):
        return self.end_date is not None

    def get_next_instance(self, tsd: SXTmpStateData):
        prev_occur = datetime.date.today()
        if tsd is not None:
            prev_occur = tsd.last_date
        if prev_occur is None and self.start_date is not None:
            prev_occur = self.start_date
            prev_occur -= relativedelta(days=1)

        next_occur = Recurrence.list_next_instance(self.get_schedule(), prev_occur)
        if self.has_end_date():
            end_date = self.get_end_date()
            if next_occur > end_date:
                next_occur = None
        elif self.has_occur_def():
            if tsd is not None and tsd.num_occur_rem == 0 or tsd is None and self.num_occurrences_remaining == 0:
                next_occur = None
        return next_occur

    def get_instance_count(self, tsd):
        if tsd is not None:
            to_ret = tsd.num_inst
        else:
            to_ret = self.instance_num
        return to_ret

    def set_instance_count(self, instance_num):
        if self.instance_num == instance_num:
            return
        self.begin_edit()
        self.instance_num = instance_num
        self.set_dirty()
        self.commit_edit()

    def get_splits(self):
        if self.template_account is not None:
            return self.template_account.get_splits()
        else:
            return []

    @staticmethod
    def pack_split_info(s_info, parent_acct, book):
        split = Split(book)
        split.set_memo(s_info.get_memo())
        gs_set_num_action(None, split, None, s_info.get_action())
        split.set_account(parent_acct)
        split.scheduled_account = s_info.get_account()
        split.set_debit_formula(s_info.get_debit_formula())
        split.set_credit_formula(s_info.get_credit_formula())
        return split

    def set_template_transaction(self, t_t_list, book):
        self.__delete_template_transaction()
        for tti in t_t_list:
            new_trans = Transaction(book)
            new_trans.begin_edit()
            new_trans.set_description(tti.get_description())
            new_trans.set_location(tti.get_location())
            new_trans.set_post_date(datetime.date.today())
            gs_set_num_action(new_trans, None, tti.get_num(), None)
            new_trans.set_notes(tti.get_notes())
            new_trans.set_currency(tti.get_currency())
            for s_info in tti.get_template_splits():
                new_split = self.pack_split_info(s_info, self.template_account, book)
                new_trans.append_split(new_split)
            new_trans.commit_edit()

    def free(self):
        Event.gen(self, EVENT_DESTROY)
        self.__delete_template_transaction()
        if not self.get_book().is_shutting_down():
            self.template_account.begin_edit()
            self.template_account.destroy()

    def commit_done(self):
        Event.gen(self, EVENT_MODIFY)

    def commit_edit(self):
        if not super().commit_edit():
            return
        self.template_account.commit_edit()
        self.commit_edit_part2(self.on_error, self.commit_done, self.free)

    def create_temporal_state(self) -> SXTmpStateData:
        to_ret = SXTmpStateData()
        to_ret.last_date = self.last_date
        to_ret.num_occur_rem = self.num_occurrences_remaining
        to_ret.num_inst = self.instance_num
        return to_ret

    def incr_temporal_state(self, tsd: SXTmpStateData):
        if tsd is None:
            return
        tsd.last_date = self.get_next_instance(tsd)
        if self.has_occur_def():
            tsd.num_occur_rem -= 1
        tsd.num_inst += 1

    def add_defer_instance(self, d):
        self.deferredList.append(d)
        self.deferredList.sort()

    def remove_defer_instance(self, d):
        self.deferredList.remove(d)

    def get_defer_instances(self):
        return self.deferredList

    def refers_to(self, other):
        if isinstance(other, Account):
            return self.template_account == other
        return False

    @classmethod
    def register(cls):
        params = [Param(SX_NAME, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(SX_START_DATE, TYPE_DATE, cls.get_start_date, cls.set_start_date),
                  Param(SX_LAST_DATE, TYPE_DATE, cls.get_last_occur_date, cls.set_last_occur_date),
                  Param(SX_NUM_OCCUR, TYPE_INT64, cls.get_num_occur, cls.set_num_occur),
                  Param(SX_REM_OCCUR, TYPE_INT64, cls.get_rem_occur, cls.set_rem_occur),
                  Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None)]
        ObjectClass.register(cls, ID_SCHEDXACTION, None, params)

    def __repr__(self):
        recurrences = self.get_schedule()
        return u"ScheduledTransaction<'{}' {}>".format(self.name,
                                                       str(recurrences) if any(recurrences) else "")
