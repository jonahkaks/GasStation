class EventData:
    def __init__(self, node=None, idx=0):
        self.node = node
        self.idx = idx

    def __repr__(self):
        return u"Node is %s with id %d" % (self.node, self.idx)


EVENT_BASE = 24
EVENT_NONE = 0
EVENT_CREATE = 1
EVENT_MODIFY = 2
EVENT_DESTROY = 4
EVENT_ADD = 8
EVENT_REMOVE = 16
EVENT_LAST = 128
EVENT_ALL = 0xff
EVENT_ITEM_ADDED = 256
EVENT_ITEM_REMOVED = 512
EVENT_ITEM_CHANGED = 1024


def event_to_string(e):
    if e == EVENT_BASE:
        return "BASE"
    elif e == EVENT_NONE:
        return "NONE"
    elif e == EVENT_CREATE:
        return "CREATE"
    elif e == EVENT_MODIFY:
        return "MODIFY"
    elif e == EVENT_DESTROY:
        return "DESTROY"
    elif e == EVENT_ADD:
        return "ADD"
    elif e == EVENT_REMOVE:
        return "REMOVE"
    elif e == EVENT_LAST:
        return "LAST"
    elif e == EVENT_ALL:
        return "ALL"
    elif e == EVENT_ITEM_ADDED:
        return "ADDED"
    elif e == EVENT_ITEM_REMOVED:
        return "REMOVED"
    elif e == EVENT_ITEM_CHANGED:
        return "CHANGED"


class HandlerInfo:
    def __init__(self):
        self.handler = None
        self.user_data = None
        self.handler_id = 0


class Event:
    suspend_counter = 0
    next_handler_id = 1
    handler_run_level = 0
    pending_deletes = 0
    handlers = set()

    @classmethod
    def register_handler(cls, handler, *user_data):
        if handler is None:
            return 0
        hi = HandlerInfo()
        hi.user_data = user_data
        hi.handler = handler
        hi.handler_id = cls.next_handler_id
        cls.next_handler_id += 1
        cls.handlers.add(hi)
        return hi.handler_id

    @classmethod
    def unregister_handler(cls, handler_id):
        for hi in cls.handlers.copy():
            if hi.handler_id != handler_id:
                continue
            if cls.handler_run_level == 0:
                cls.handlers.remove(hi)
            else:
                cls.pending_deletes += 1
        return

    @classmethod
    def suspend(cls):
        cls.suspend_counter += 1

    @classmethod
    def resume(cls):
        if cls.suspend_counter == 0:
            return
        cls.suspend_counter -= 1

    @classmethod
    def generate_internal(cls, entity, event_id, *event_data):
        if entity is None:
            return
        if event_id == EVENT_NONE:
            return
        cls.handler_run_level += 1
        for hi in cls.handlers:
            data = filter(lambda a: a is not None, event_data + hi.user_data)
            hi.handler(entity, event_id, *data)
        cls.handler_run_level -= 1

        if cls.handler_run_level == 0 and cls.pending_deletes:
            for hi in cls.handlers.copy():
                if hi.handler is None:
                    cls.handlers.remove(hi)
        cls.pending_deletes = 0

    @classmethod
    def force(cls, entity, event_id, *event_data):
        if entity is None: return
        cls.generate_internal(entity, event_id, *event_data)

    @classmethod
    def gen(cls, entity, event_id, *event_data):
        if entity is None:
            return
        if cls.suspend_counter:
            return
        cls.generate_internal(entity, event_id, *event_data)
