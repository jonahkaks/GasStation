import dataclasses
import json
import os
import xml.sax
import xml.sax.handler

from libgasstation import Account, AccountType, Book, CommodityTable
from libgasstation.core.helpers import qof_init

ACCOUNT_STRING = "gs-account-example"
ACCOUNT_SHORT = "gs-act:short-description"
ACCOUNT_LONG = "gs-act:long-description"
ACCOUNT_TITLE = "gs-act:title"
ACCOUNT_EXCLUDEP = "gs-act:exclude-from-select-all"
ACCOUNT_SELECTED = "gs-act:start-selected"


@dataclasses.dataclass
class ExampleAccount(xml.sax.handler.ContentHandler):
    title: str = None
    filename = None
    book = None
    root: Account = None
    short_description: str = None
    long_description: str = None
    exclude_from_select_all: bool = False
    start_selected = False
    buffer = None
    accounts = {}
    parents = {}
    current_account = None
    current_slot = ""

    def __init__(self):
        super().__init__()

    def startElement(self, name, attributes):
        self.buffer = ""
        if name == "act:id":
            self.accounts[self.current_account]["id"] = {'type': attributes['type']}
        elif name == "act:parent":
            self.accounts[self.current_account]["parent"] = {'type': attributes['type']}
        elif name == "act:commodity":
            self.accounts[self.current_account]["commodity"] = {}

    def characters(self, data):
        self.buffer += data.strip()

    def endElement(self, name):
        if name == ACCOUNT_TITLE:
            self.title = self.buffer
        elif name == ACCOUNT_SHORT:
            self.short_description = self.buffer
        elif name == ACCOUNT_LONG:
            self.long_description = self.buffer
        elif name == "act:name":
            self.current_account = self.buffer
            self.accounts[self.current_account] = {}
        elif name == "act:id":
            self.accounts[self.current_account]["id"]['value'] = self.buffer
        elif name == "act:type":
            self.accounts[self.current_account]['type'] = self.buffer
        elif name == "act:description":
            self.accounts[self.current_account]['description'] = self.buffer
        elif name == "act:parent":
            self.accounts[self.current_account]["parent"]['id'] = self.buffer
        elif name == "slot:key":
            self.accounts[self.current_account][self.buffer] = ""
            self.current_slot = self.buffer
        elif name == "slot:value":
            self.accounts[self.current_account][self.current_slot] = self.buffer
        elif name == "act:commodity-scu":
            self.accounts[self.current_account]["commodity-scu"] = self.buffer
        elif name == "cmdty:space":
            self.accounts[self.current_account]["commodity"]["isocode"] = self.buffer
        elif name == "cmdty:id":
            self.accounts[self.current_account]["commodity"]["mnemonic"] = self.buffer
        elif name == "gs:account":
            self.add_local_account()
        elif name == ACCOUNT_SELECTED:
            self.start_selected = bool(int(self.buffer))
        elif name == ACCOUNT_EXCLUDEP:
            self.exclude_from_select_all = bool(int(self.buffer))
        elif name == ACCOUNT_STRING:
            # self.clean_up()
            pass

    # def clean_up(self):
    #     del self.parents
    #     del self.accounts
    #     del self.buffer
    #     del self.current_account
    #     del self.current_slot

    @staticmethod
    def clear_up_commodity(ctbl, acc):
        if ctbl is None: return
        comm = acc.get_commodity()
        if comm is None:
            return
        gcomm = ctbl.lookup(comm.get_namespace(), comm.get_mnemonic())
        if gcomm == comm:
            return
        elif gcomm is None:
            ctbl.insert(comm)
        else:
            acc.set_commodity(gcomm)
            comm.destroy()

    def add_local_account(self):
        from libgasstation.core.commodity import CommodityTable, COMMODITY_NAMESPACE_NAME_CURRENCY
        a = self.accounts[self.current_account]
        table = CommodityTable.get_table(self.book)
        acc = Account(self.book)
        acc.begin_edit()
        acc.set_type(AccountType.from_name(a['type']))
        acc.set_name(self.current_account)
        acc.set_description(a.get('description', ''))
        acc.set_placeholder(json.loads(a.get('placeholder', 'false')))
        if a.get('parent', None) is None:
            self.book.set_root_account(acc)
            self.root = acc
            self.parents[a['id']['value']] = self.root
        else:
            self.parents[a['id']['value']] = acc
            self.parents[a['parent']['id']].append_child(acc)
        if a.get('commodity', None) is not None:
            cur = table.lookup(COMMODITY_NAMESPACE_NAME_CURRENCY, a['commodity']['mnemonic'])
            acc.set_commodity(cur)
        if a.get('commodity-scu', None) is not None:
            acc.set_commodity_scu(json.loads(a['commodity-scu']))
        acc.commit_edit()
        self.clear_up_commodity(table, acc)

    @classmethod
    def load_from_file(cls, filename):
        if filename is None or len(filename) <= 0:
            return
        f = cls()
        f.book = Book()
        parser = xml.sax.make_parser()
        parser.setContentHandler(f)
        f.filename = filename
        parser.parse(filename)
        del parser
        return f

    @classmethod
    def load_from_directory(cls, directory):
        qof_init()
        CommodityTable.register()
        return list(map(lambda name: cls.load_from_file(os.path.join(directory, name)),
                        filter(lambda name: name.endswith("xea"), os.listdir(directory))))

    def __repr__(self):
        return "<ExampleAccount %s>" % self.title

    def __del__(self):
        self.title = None
        self.filename = None
        self.book = None
        self.root = None
        self.short_description = None
        self.long_description = None
        self.exclude_from_select_all = False
        self.start_selected = False
        self.buffer = None
        self.accounts = None
        self.parents = None
        self.current_account = None
        self.current_slot = None

