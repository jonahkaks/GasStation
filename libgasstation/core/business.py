PERSON_EXPORT_PDF_DIRNAME = "export-pdf-directory"
LAST_POSTED_TO_ACCT = "last-posted-to-acct"
PAYMENT = "payment"
LAST_ACCOUNT = "last_acct"
from .object import *
from .query_private import PARAM_ACTIVE


class _GetListUserData:
    def __init__(self):
        self.result = []
        self.is_active_accessor_func = None


class Business:
    @staticmethod
    def get_list_cb(inst, data: _GetListUserData):
        if data.is_active_accessor_func is None or data.is_active_accessor_func(inst, None):
            data.result.append(inst)

    @classmethod
    def get_list(cls, book, type_name, all_including_inactive):
        data = _GetListUserData()
        if not all_including_inactive:
            data.is_active_accessor_func = ObjectClass.get_parameter_getter(type_name, PARAM_ACTIVE)
        ObjectClass.foreach(type_name, book, cls.get_list_cb, data)
        return data.result

    @staticmethod
    def get_person_list_cb(inst, data: _GetListUserData):
        if data.is_active_accessor_func is None or data.is_active_accessor_func(inst, None):
            data.result.append(inst)

    @classmethod
    def get_person_list(cls, book, type_name, all_including_inactive):
        print(type_name, all_including_inactive)
        data = _GetListUserData()
        if not all_including_inactive:
            data.is_active_accessor_func = ObjectClass.get_parameter_getter(type_name, PARAM_ACTIVE)
        ObjectClass.foreach(type_name, book, cls.get_person_list_cb, data)
        return data.result
