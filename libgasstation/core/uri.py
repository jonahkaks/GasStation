import re

DATAFILE_EXT = ".gas"
LOGFILE_EXT = ".log"
pattern = re.compile(r'''
                (?P<name>[\w+]+)://
                (?:
                    (?P<username>[^:/]*)
                    (?::(?P<password>.*))?
                @)?
                (?:
                    (?:
                        \[(?P<ipv6host>[^/]+)\] |
                        (?P<ipv4host>[^/:]+)
                    )?
                    (?::(?P<port>[^/]*))?
                )?
                (?:/(?P<database>.*))?
                ''', re.X)


class Uri:
    def __init__(self, file_url=None, **kwargs):
        self.scheme = kwargs.get("scheme", "file")
        self.driver = kwargs.get("driver")
        self.path = kwargs.get("path")
        self.query = kwargs.get("query")
        self.params = kwargs.get("params")
        self.hostname = kwargs.get("hostname")
        self.port = kwargs.get("port", 0)
        self.username = kwargs.get("username")
        self.password = kwargs.get("password")
        if file_url is not None and isinstance(file_url, str):
            split_url = file_url.split("://", 2)
            if len(split_url) == 1:
                self.path = split_url[0]
            else:
                m = [i for i in pattern.split(file_url) if i != '']
                self.scheme, self.hostname, self.port, self.username, self.password, self.path = m[0] or self.scheme, m[4], m[5] or 0, m[1], m[2] if m[
                                                                                                                      2] != "***" else None, \
                                                                   m[6]
                if self.scheme is not None and self.scheme.__contains__("+"):
                    self.scheme, self.driver = self.scheme.split("+")
        self.set_scheme(self.scheme)

    def is_file(self):
        return self.scheme is not None and self.scheme in ["file", "xml", "sqlite"]

    def is_valid(self):
        return self.scheme is not None and self.path is not None and (
                self.is_file() or self.hostname is not None)

    @staticmethod
    def _supported(scheme):
        return scheme in ["mysql", "sqlite", "postgresql", "file"]

    def supported(self):
        return self._supported(self.scheme)

    def get_scheme(self):
        return self.scheme

    def set_password(self, password: str):
        self.password = password
        return self

    def get_password(self):
        return self.password

    def set_driver(self, driver):
        self.driver = driver
        return self

    def set_scheme(self, scheme):
        if not self._supported(scheme):
            raise ValueError("This url is not supported")
        self.scheme = scheme
        return self

    def set_port(self, port: int):
        if not self.is_file():
            if not isinstance(port, int):
                raise TypeError("Expected type int on set port got {}".format(type(port)))
            self.port = port
        else:
            self.port = None
        return self

    def get_path(self):
        return self.path

    def add_extension(self, extension):
        if extension is None or not self.is_file() or self.path is None:
            return self
        if self.path is not None and self.path.endswith(extension):
            return self
        if not extension.startswith("."):
            extension = "." + extension
        self.path = self.path + extension
        return self

    def __to_string__(self, hide_password=True):
        s = "{}{}://".format(self.scheme, "+" + self.driver if self.driver is not None else "")

        if self.username is not None:
            s += self.username
            if self.password is not None:
                s += ":" + (
                    "***" if hide_password else self.password
                )
            s += "@"
        if self.hostname is not None:
            if ":" in self.hostname:
                s += "[%s]" % self.hostname
            else:
                s += self.hostname
        if self.port is not None:
            s += ":" + str(self.port)
        if self.path is not None:
            s += "/" + self.path
        if self.query:
            keys = list(self.query)
            keys.sort()
            s += "?" + "&".join(
                "%s=%s" % (k, element)
                for k in keys
                for element in self.query[k])
        return s

    def set_query(self, **kwargs):
        self.query = kwargs
        return self

    def get_query(self):
        return self.query

    def __str__(self):
        return self.__to_string__()

    def __repr__(self):
        return self.__to_string__()

    def __hash__(self):
        return hash(str(self))

    def __len__(self):
        return len(self.__to_string__())


