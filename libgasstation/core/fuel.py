from sqlalchemy import DECIMAL, Date, DateTime, ForeignKey
from sqlalchemy.orm import reconstructor, relationship

from ._declbase import *
from .event import *
from .query_private import *

FUEL_DATE = "fuel-date"
FUEL_NOZZLE = "fuel-nozzle"
FUEL_LOCATION = "fuel-location"


class Fuel(DeclarativeBaseGuid):
    __tablename__ = 'fuel'

    # column definitions
    nozzle_guid = Column('nozzle_id', UUIDType(binary=False), ForeignKey('nozzle.guid'), nullable=False)
    location_guid = Column('location_id', UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    date = Column('date', Date(), nullable=False)
    date_entered = Column('date_entered', DateTime())
    opening = Column('opening', DECIMAL(10, 2), nullable=False)
    closing = Column('closing', DECIMAL(10, 2), nullable=False)
    price = Column('price', DECIMAL(10, 2), nullable=False)
    debtor_amount = Column('debtor_amount', DECIMAL(10, 2), nullable=False)
    nozzle = relationship('Nozzle', back_populates="fuel")
    location = relationship('Location')

    def __init__(self, book):
        super().__init__(ID_FUEL, book)
        self.date = datetime.date.today()
        self.opening = Decimal(0)
        self.closing = Decimal(0)
        self.price = Decimal(0)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from ..core.session import Session
        book = Session.get_current_book()
        super().__init__(ID_FUEL, book)
        Event.gen(self, EVENT_CREATE)
        self.infant = False

    def set_date(self, date):
        if date is not None and date != self.date:
            self.begin_edit()
            self.date = date
            self.set_dirty()
            self.commit_edit()

    def get_date(self):
        return self.date

    def set_opening(self, value: Decimal):
        if value != self.opening:
            self.begin_edit()
            self.opening = value
            self.set_dirty()
            self.commit_edit()

    def set_closing(self, value: Decimal):
        if value != self.closing:
            self.begin_edit()
            self.closing = value
            self.set_dirty()
            self.commit_edit()

    def set_price(self, value: Decimal):
        if value != self.price:
            self.begin_edit()
            self.price = value
            self.set_dirty()
            self.commit_edit()

    def get_price(self):
        return self.price

    def set_debtor_amount(self, value: Decimal):
        if value != self.price:
            self.begin_edit()
            self.debtor_amount = value
            self.set_dirty()
            self.commit_edit()

    def get_debtor_amount(self):
        return self.debtor_amount

    def get_opening(self):
        return self.opening

    def get_closing(self):
        return self.closing

    def get_litres(self):
        return self.closing - self.opening

    def get_amount(self):
        return self.get_litres() * self.price

    def set_nozzle(self, nozzle):
        if nozzle is not None and nozzle != self.nozzle:
            self.begin_edit()
            self.nozzle = nozzle
            self.set_dirty()
            self.commit_edit()

    def get_nozzle(self):
        return self.nozzle

    def get_nozzle_name(self):
        return self.nozzle.get_name() if self.nozzle is not None else ""

    def get_location(self):
        return self.location

    def set_location(self, value):
        if value != self.location:
            self.begin_edit()
            self.location = value
            self.set_dirty()
            self.commit_edit()

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def __repr__(self):
        return u"<Fuel %s %s %s>" % (self.date, self.opening, self.closing)

    @classmethod
    def register(cls):
        params = [Param(FUEL_DATE, TYPE_DATE, cls.get_date, cls.set_date),
                  Param(FUEL_NOZZLE, ID_NOZZLE, cls.get_nozzle, cls.set_nozzle),
                  Param(FUEL_LOCATION, ID_LOCATION, cls.get_location, cls.set_location),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_FUEL, None, params)

    def refers_to(self, other):
        from .nozzle import Nozzle
        if isinstance(other, Nozzle):
            if self.nozzle == other:
                return True
        return False
