from ._declbase import ID_SXES, ID_SCHEDXACTION
from .event import *
from .instance import Instance
from .object import *


class ScheduledTransactionList(Instance):
    def refers_to(self, other):
        pass

    def __init__(self, book, *args):
        super().__init__(ID_SXES, book)
        self.sx_list = []
        self.guid = None

    def __new__(cls, book, *args, **kwargs):
        if book is not None:
            data = book.get_collection(ID_SCHEDXACTION).get_data()
            return data if data is not None else super(ScheduledTransactionList, cls).__new__(cls)

    def add(self, sx):
        self.sx_list.append(sx)
        Event.gen(self, EVENT_ITEM_ADDED, sx)

    @staticmethod
    def get_all(book):
        if book is not None:
            return book.get_collection(ID_SCHEDXACTION).get_data().sx_list
        return []

    def remove(self, sx):
        if sx in self.sx_list:
            self.sx_list.remove(sx)
            Event.gen(self, EVENT_ITEM_REMOVED, sx)

    @classmethod
    def book_begin(cls, book):
        col = book.get_collection(ID_SCHEDXACTION)
        col.set_data(cls(book))

    @classmethod
    def get_all_referencing_account(cls, book, acct):
        rtn = []
        acct_guid = acct.get_guid()
        scheduled_transactions = cls.get_all(book)
        if scheduled_transactions is None:
            return
        for sx in scheduled_transactions:
            splits = sx.get_splits()
            for s in splits:
                if s.scheduled_account_guid == acct_guid:
                    rtn.append(sx)
        return rtn

    def __repr__(self):
        return u"Scheduled Transactions List"

    @classmethod
    def register(cls):
        ObjectClass.register(cls, ID_SXES, None, [])
