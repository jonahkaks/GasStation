import enum

from sqlalchemy import Enum, ForeignKey, VARCHAR
from sqlalchemy.orm import relationship

from libgasstation.core._declbase import *


class DevicePlatform(enum.Enum):
    ANDROID = 0
    IOS = 1
    LINUX = 2
    WINDOWS = 3


@dataclass
class Device(DeclarativeBaseGuid):
    __tablename__ = 'device'
    __table_args__ = {}
    location_guid = Column("location_guid", UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    name: str = Column("name", VARCHAR(200), unique=True, nullable=False)
    model: str = Column("model", VARCHAR(32))
    version: str = Column("version", VARCHAR(32))
    push_id: uuid = Column("push_id", UUIDType(binary=False))
    uuid: uuid = Column("uuid", UUIDType(binary=False))
    platform: DevicePlatform = Column("platform", Enum(DevicePlatform))
    location = relationship("Location", back_populates="devices")

    def __init__(self, name: str, uuid, platform: DevicePlatform, model: str = None, version: str = None, *args,
                 **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name
        self.model = model
        self.version = version
        self.uuid = uuid
        self.platform = platform

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return "Device<{}-{}>".format(self.name, self.model)
