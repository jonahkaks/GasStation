from sqlalchemy import DECIMAL, VARCHAR, ForeignKey, Enum
from sqlalchemy.orm import reconstructor, relationship

from ._declbase import *
from .engine import *
from .event import *
from .query_private import *

ADDRESS_LINE1 = "address_line1"
ADDRESS_LINE2 = "address_line2"
ADDRESS_LINE3 = "address_line3"
ADDRESS_LINE4 = "address_line4"
ADDRESS_LINE5 = "address_line5"
ADDRESS_CITY = "address_city"
ADDRESS_STATE = "address_state"
ADDRESS_POSTAL_CODE = 'address_postal_code'
ADDRESS_COUNTRY = "address_country"
ADDRESS_LATITUDE = "address_latitude"
ADDRESS_LONGITUDE = "address_longitude"
ADDRESS_NOTE = "address_note"


class AddressType(IntEnum):
    HOME = 0
    WORK = 1
    OTHER = 2


class Address(DeclarativeBaseGuid):
    __tablename__ = 'address'
    __table_args__ = {}
    contact_guid = Column("contact_guid", UUIDType(binary=False), ForeignKey('contact.guid'), nullable=False)
    type = Column("type", Enum(AddressType), default=AddressType.WORK)
    line1 = Column("line1", VARCHAR(200), nullable=False)
    line2 = Column("line2", VARCHAR(100))
    line3 = Column("line3", VARCHAR(100))
    line4 = Column("line4", VARCHAR(100))
    line5 = Column("line5", VARCHAR(100))
    city = Column("city", VARCHAR(100))
    state = Column("state", VARCHAR(100))
    postal_code = Column("postal_code", VARCHAR(20))
    country = Column("country", VARCHAR(25))
    latitude = Column("lat", DECIMAL(8, 2))
    longitude = Column("long", DECIMAL(8, 2))
    note = Column("note", VARCHAR(100))
    contact = relationship("Contact", back_populates="addresses")

    def __init__(self, book):
        super().__init__(ID_ADDRESS, book)
        self.type = AddressType.WORK
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, ID_ADDRESS, book)
        Event.gen(self, EVENT_CREATE)

    def get_type(self):
        return self.type

    def set_type(self, t: AddressType):
        if t != self.type:
            self.begin_edit()
            self.type = t
            self.set_dirty()
            self.commit_edit()

    def get_line1(self):
        return self.line1

    def set_line1(self, line1):
        if line1 != self.line1 and len(line1):
            self.begin_edit()
            self.line1 = line1
            self.set_dirty()
            self.commit_edit()

    def get_line2(self):
        return self.line2

    def set_line2(self, line2):
        if line2 != self.line2 and len(line2):
            self.begin_edit()
            self.line2 = line2
            self.set_dirty()
            self.commit_edit()

    def get_line3(self):
        return self.line3

    def set_line3(self, line3):
        if line3 != self.line3 and len(line3):
            self.begin_edit()
            self.line3 = line3
            self.set_dirty()
            self.commit_edit()

    def get_line4(self):
        return self.line4

    def set_line4(self, line4):
        if line4 != self.line4 and len(line4):
            self.begin_edit()
            self.line4 = line4
            self.set_dirty()
            self.commit_edit()

    def get_line5(self):
        return self.line5

    def set_line5(self, line5):
        if line5 != self.line5 and len(line5):
            self.begin_edit()
            self.line5 = line5
            self.set_dirty()
            self.commit_edit()

    def get_latitude(self):
        return self.latitude

    def set_latitude(self, latitude):
        if latitude != self.latitude and len(latitude):
            self.begin_edit()
            self.latitude = latitude
            self.set_dirty()
            self.commit_edit()

    def get_longitude(self):
        return self.longitude

    def set_longitude(self, longitude):
        if longitude != self.longitude and len(longitude):
            self.begin_edit()
            self.longitude = longitude
            self.set_dirty()
            self.commit_edit()

    def get_note(self):
        return self.note

    def set_note(self, note):
        if note != self.note and len(note):
            self.begin_edit()
            self.note = note
            self.set_dirty()
            self.commit_edit()

    def get_city(self):
        return self.city

    def set_city(self, city):
        if city != self.city and len(city):
            self.begin_edit()
            self.city = city
            self.set_dirty()
            self.commit_edit()

    def get_state(self):
        return self.state

    def set_state(self, state):
        if state != self.state and len(state):
            self.begin_edit()
            self.state = state
            self.set_dirty()
            self.commit_edit()

    def get_postal_code(self):
        return self.postal_code

    def set_postal_code(self, postal_code):
        if postal_code != self.postal_code and len(postal_code):
            self.begin_edit()
            self.postal_code = postal_code
            self.set_dirty()
            self.commit_edit()

    def get_country(self):
        return self.country

    def set_country(self, country):
        if country != self.country and len(country):
            self.begin_edit()
            self.country = country
            self.set_dirty()
            self.commit_edit()

    def get_contact(self):
        return self.contact

    def set_contact(self, t):
        if t != self.contact:
            self.begin_edit()
            self.contact = t
            self.set_dirty()
            self.commit_edit()

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def on_error(self, error):
        Engine.signal_commit_error(error)

    def __eq__(self, other):
        if other is None:
            return False
        if self.line1 != other.line1:
            return False
        elif self.line2 != other.line2:
            return False
        elif self.line3 != other.line3:
            return False
        elif self.line4 != other.line4:
            return False
        elif self.line5 != other.line5:
            return False
        elif self.latitude != other.latitude:
            return False
        elif self.longitude != other.longitude:
            return False
        elif self.note != other.note:
            return False
        elif self.city != other.city:
            return False
        elif self.country != other.country:
            return False
        elif self.postal_code != other.postal_code:
            return False
        return self.guid == other.guid

    @classmethod
    def register(cls):
        params = [
            Param(ADDRESS_LINE1, TYPE_STRING, cls.get_line1, cls.set_line1),
            Param(ADDRESS_LINE2, TYPE_STRING, cls.get_line2, cls.set_line2),
            Param(ADDRESS_LINE3, TYPE_STRING, cls.get_line3, cls.set_line3),
            Param(ADDRESS_LINE4, TYPE_STRING, cls.get_line4, cls.set_line4),
            Param(ADDRESS_LINE5, TYPE_STRING, cls.get_line5, cls.set_line5),
            Param(ADDRESS_CITY, TYPE_STRING, cls.get_city, cls.set_city),
            Param(ADDRESS_STATE, TYPE_STRING, cls.get_state, cls.set_state),
            Param(ADDRESS_POSTAL_CODE, TYPE_STRING, cls.get_postal_code, cls.set_postal_code),
            Param(ADDRESS_COUNTRY, TYPE_STRING, cls.get_country, cls.set_country),
            Param(ADDRESS_LATITUDE, TYPE_DOUBLE, cls.get_latitude, cls.set_latitude),
            Param(ADDRESS_LONGITUDE, TYPE_DOUBLE, cls.get_longitude, cls.set_longitude),
            Param(ADDRESS_NOTE, TYPE_STRING, cls.get_note, cls.set_note),
            Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
            Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None)]
        ObjectClass.register(cls, ID_ADDRESS, cls.__eq__, params)

    def __str__(self):
        return "{}\n{} {} {}".format(self.line1, self.city or "", self.state or "",
                                     self.postal_code or "").strip()
