import locale


class Locale:
    lc = None
    code = None
    places = None

    @classmethod
    def conv(cls):
        if cls.lc is not None:
            return cls.lc
        lc = locale.localeconv()
        lc['decimal_point'] = "."
        lc['thousands_sep'] = ","
        lc['grouping'] = "\003"
        lc['int_curr_symbol'] = "UGX"
        lc['currency_symbol'] = "Ush"
        lc['mon_decimal_point'] = "."
        lc['mon_thousands_sep'] = "'] = "
        lc['mon_grouping'] = "\003"
        lc['negative_sign'] = "-"
        lc['positive_sign'] = ""
        lc['frac_digits'] = 2
        lc['int_frac_digits'] = 2
        lc['p_cs_precedes'] = 1
        lc['p_sep_by_space'] = 0
        lc['n_cs_precedes'] = 1
        lc['n_sep_by_space'] = 0
        lc['p_sign_posn'] = 1
        lc['n_sign_posn'] = 1
        cls.lc = lc
        return lc

    @classmethod
    def default_iso_currency_code(cls):
        if cls.code is not None:
            return cls.code
        lc = cls.conv()
        cls.code = lc['int_curr_symbol']
        return cls.code

    @classmethod
    def decimal_places(cls):
        if cls.places is not None:
            return cls.places
        lc = cls.conv()
        cls.places = lc['frac_digits']
        return cls.places

    @staticmethod
    def name():
        return locale.setlocale(locale.LC_ALL, None)

