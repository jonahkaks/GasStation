from sqlalchemy import Column, VARCHAR, ForeignKey, DECIMAL
from sqlalchemy.orm import relationship, reconstructor
from sqlalchemy_utils import UUIDType

from ._declbase import DeclarativeBaseGuid, ID_TANK, ID_PRODUCT
from .engine import Engine
from .event import *
from .product import Product
from .query_private import *

TANK_NAME = "tank-name"
TANK_INVENTORY = "tank-product"
TANK_DIPS = "tank-dips"


class Tank(DeclarativeBaseGuid):
    __tablename__ = 'tank'

    __table_args__ = {}

    # column definitions
    product_guid = Column(UUIDType(binary=False), ForeignKey('product.guid'), nullable=False)
    location_guid = Column(UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    name = Column(VARCHAR(length=20), nullable=False)
    height = Column(DECIMAL(10, 2), default=Decimal(0))
    density = Column(DECIMAL(10, 2), default=Decimal(0))
    temperature = Column(DECIMAL(10, 2), default=Decimal(0))
    capacity = Column(DECIMAL(10, 2), default=Decimal(0))
    product = relationship('Product')
    location = relationship('Location')
    dips = relationship('Dip', back_populates="tank", lazy="joined", cascade="all, delete-orphan")
    nozzles = relationship('Nozzle', back_populates="tank", cascade='all, delete-orphan')

    def __init__(self, book):
        super().__init__(ID_TANK, book)
        self.capacity = Decimal(0)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        self.infant = False
        book = Session.get_current_book()
        super().__init__(ID_TANK, book)
        Event.gen(self, EVENT_CREATE)

    def get_name(self):
        return self.name

    def set_name(self, name):
        if name is not None and name != self.name:
            self.begin_edit()
            self.name = name.strip()
            self.set_dirty()
            self.commit_edit()

    @classmethod
    def lookup_name(cls, book, name):
        a = book.get_collection(ID_TANK)
        for cat in a.values():
            if cat.get_name() == name.strip():
                return cat

    @classmethod
    def lookup_name_with_location(cls, book, name, location):
        a = book.get_collection(ID_TANK)
        for cat in a.values():
            if cat.name == name.strip() and cat.location == location:
                return cat

    def get_product(self):
        return self.product

    def get_product_str(self):
        return self.product.get_name() if self.product is not None else ""

    def set_product(self, inv):
        if inv is not None and inv != self.product:
            self.begin_edit()
            self.product = inv
            self.set_dirty()
            self.commit_edit()

    def get_dips(self):
        return self.dips

    def get_nozzles(self):
        return self.nozzles

    def set_capacity(self, cap: Decimal):
        if cap is not None and cap != self.capacity:
            self.begin_edit()
            self.capacity = cap
            self.set_dirty()
            self.commit_edit()

    def get_capacity(self):
        return self.capacity

    def set_height(self, cap):
        if cap is not None and cap != self.height:
            self.begin_edit()
            self.height = cap
            self.set_dirty()
            self.commit_edit()

    def get_height(self):
        return self.height

    def set_temperature(self, temp):
        if temp is not None and temp != self.temperature:
            self.begin_edit()
            self.temperature = temp
            self.set_dirty()
            self.commit_edit()

    def get_temperature(self):
        return self.temperature

    def set_density(self, density):
        if density is not None and density != self.density:
            self.begin_edit()
            self.density = density
            self.set_dirty()
            self.commit_edit()

    def get_density(self):
        return self.density

    def get_location(self):
        return self.location

    def set_location(self, location):
        if location != self.location:
            self.begin_edit()
            self.location = location
            self.set_dirty()
            self.commit_edit()

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        self.product = None
        self.capacity = Decimal(0)
        self.height = Decimal(0)
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def on_error(self, error):
        Engine.signal_commit_error(error)

    def refers_to(self, other):
        if isinstance(other, Product):
            if self.product == other:
                return True
        return False

    @classmethod
    def register(cls):
        params = [Param(TANK_NAME, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(TANK_INVENTORY, ID_PRODUCT, cls.get_product, cls.set_product),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_TANK, None, params)

    def __repr__(self):
        return "Tank<%s>" % self.get_name()
