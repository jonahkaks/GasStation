import weakref

HOOK_STARTUP = "hook_startup"
HOOK_SHUTDOWN = "hook_shutdown"
HOOK_UI_STARTUP = "hook_ui_startup"
HOOK_UI_POST_STARTUP = "hook_ui_post_startup"
HOOK_UI_SHUTDOWN = "hook_ui_shutdown"
HOOK_NEW_BOOK = "hook_new_book"
HOOK_REPORT = "hook_report"
HOOK_CURRENCY_CHANGED = "hook_currency_changed"
HOOK_SAVE_OPTIONS = "hook_save_options"
HOOK_RESTORE_OPTIONS = "hook_restore_options"
HOOK_ADD_EXTENSION = "hook_add_extension"
HOOK_BOOK_OPENED = "hook_book_opened"
HOOK_BOOK_CLOSED = "hook_book_closed"
HOOK_BOOK_SAVED = "hook_book_saved"


class Hook:
    initialised = False
    __hook_list = {}

    def __init__(self, desc, num):
        self.num_args = num
        self.desc = desc
        self.c_danglers = set()

    @classmethod
    def init(cls):
        cls.create(HOOK_STARTUP, 0,
                   "Functions to run at startup.  Hook args: ()")
        cls.create(HOOK_SHUTDOWN, 0,
                   "Functions to run at guile shutdown.  Hook args: ()")
        cls.create(HOOK_UI_STARTUP, 0,
                   "Functions to run when the ui comes up.  Hook args: ()")
        cls.create(HOOK_UI_POST_STARTUP, 0,
                   "Functions to run after the ui comes up.  Hook args: ()")
        cls.create(HOOK_UI_SHUTDOWN, 0,
                   "Functions to run at ui shutdown.  Hook args: ()")
        cls.create(HOOK_NEW_BOOK, 0,
                   "Run after a new (empty) book is opened, before the"
                   " book-opened-hook. Hook args: ()")
        cls.create(HOOK_REPORT, 0,
                   "Run just before the reports are pushed into the menus."
                   "  Hook args: ()")
        cls.create(HOOK_CURRENCY_CHANGED, 0,
                   "Functions to run when the user changes currency settings.  Hook args: ()")
        cls.create(HOOK_SAVE_OPTIONS, 1,
                   "Functions to run when saving options.  Hook args: (file_name)")
        cls.create(HOOK_RESTORE_OPTIONS, 1,
                   "Functions to run when saving options.  Hook args: (file_name)")
        cls.create(HOOK_ADD_EXTENSION, 0,
                   "Functions to run when the extensions menu is created."
                   "  Hook args: ()")

        cls.create(HOOK_BOOK_OPENED, 1,
                   "Run after book open.  Hook args: <gnc:Session*>.")
        cls.create(HOOK_BOOK_CLOSED, 1,
                   "Run before file close.  Hook args: <gnc:Session*>")
        cls.create(HOOK_BOOK_SAVED, 1,
                   "Run after file saved.  Hook args: <gnc:Session*>")

    @classmethod
    def create(cls, name, num_args, desc):
        if name is None or num_args < 0 or desc is None:
            raise ValueError("Review your create function")
        if cls.__hook_list.get(name):
            return
        cls.__hook_list[name] = Hook(desc, num_args)

    @classmethod
    def lookup(cls, name):
        if not cls.initialised:
            cls.init()
            cls.initialised = True
        return cls.__hook_list.get(name)

    @classmethod
    def add_dangler(cls, name, callback, *args):
        if not cls.initialised:
            cls.init()
            cls.initialised = True
        gs_hook = cls.lookup(name)
        if gs_hook is None:
            return
        try:
            d = (weakref.WeakMethod(callback), args)
        except TypeError:
            d = (callback, args)
        gs_hook.c_danglers.add(d)

    @classmethod
    def run(cls, name, *data):
        hook = cls.lookup(name)
        if hook is None:
            return
        for h in hook.c_danglers:
            d = (h[1] + data)
            if isinstance(h[0], weakref.WeakMethod):
                if any(d):
                    h[0]()(*d)
                else:
                    h[0]()()
            else:
                if any(d):
                    h[0](*d)
                else:
                    h[0]()

    @classmethod
    def remove_dangler(cls, name, callback):
        hook = cls.lookup(name)
        if hook is None:
            return
        for h in hook.c_danglers.copy():
            if h[0] == callback:
                hook.c_danglers.remove(h)
                break
