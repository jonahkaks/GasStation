default_data = {"uom": {
    "Weight": [
        {"name": "grams", "quantity": 1},
        {"name": "oz", "quantity": 0.035274},
        {"name": "lb", "quantity": 0.00220462},
        {"name": "kg", "quantity": 0.001},
        {"name": "t", "quantity": 9.8421e-7}
    ],
    "Working Time": [
        {"name": "Hours", "quantity": 1},
        {"name": "Days", "quantity": 0.041667}
    ],
    "Length / Distance mm":
        [
            {"name": "cm", "quantity": 1},
            {"name": "in", "quantity": 0.393701},
            {"name": "ft", "quantity": 0.0328084},
            {"name": "yd", "quantity": 0.0109361},
            {"name": "m", "quantity": 0.01},
            {"name": "km", "quantity": 1e-5},
            {"name": "mi", "quantity": 6.2137e-6}],
    "Surface": [
        {"name": "ft²", "quantity": 1},
        {"name": "m²", "quantity": 0.092903}],
    "Volume": [
        {"name": "in³", "quantity": 1},
        {"name": "fl oz (US)", "quantity": 0.554113},
        {"name": "qt (US)", "quantity": 0.017316},
        {"name": "L", "quantity": 0.0163871},
        {"name": "gal (US)", "quantity": 0.004329},
        {"name": "ft³", "quantity": 0.000578704},
        {"name": "m³", "quantity": 1.63871e-5}],
    "Units": [
        {"name": "Pc", "quantity": 1},
        {"name": "Dozen", "quantity": 12},
    ]
}

}
