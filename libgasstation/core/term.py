
from sqlalchemy import INTEGER, DECIMAL, Enum, VARCHAR, ForeignKey
from sqlalchemy.orm import reconstructor, relationship

from libgasstation.core.query_private import *
from ._declbase import *
from .event import *


class DiscountType(IntEnum):
    PRICE_VALUE = 0
    PRICE_PERCENT = 1
    TOTAL_VALUE = 2
    TOTAL_PERCENT = 3

    def __str__(self):
        return self.name.title()


class TermType(IntEnum):
    DAYS = 0
    PROXIMO = 1

    def __str__(self):
        return self.name.title()


TERM_NAME = "name"
TERM_DESC = "description"
TERM_DUEDAYS = "number of days due"
TERM_DISCDAYS = "number of discounted days"
TERM_CUTOFF = "cut off"
TERM_TYPE = "bill type"
TERM_DISCOUNT = "amount of discount"
TERM_DISCOUNT_TYPE = "type of discount"
TERM_REFCOUNT = "reference count"


@dataclass
class Term(DeclarativeBaseGuid):
    __tablename__ = 'term'

    __table_args__ = {}

    # column definitions
    guid: UUID = Column('guid', UUIDType(binary=False), primary_key=True, nullable=False, default=lambda: uuid.uuid4())
    name: str = Column('name', VARCHAR(length=2048), nullable=False)
    description: str = Column('description', VARCHAR(length=2048), nullable=False)
    refcount: int = Column('refcount', INTEGER(), nullable=False)
    invisible: bool = Column('invisible', BOOLEAN(), nullable=False)
    parent_guid: UUID = Column('parent', UUIDType(binary=False), ForeignKey('term.guid'))
    type: TermType = Column('type', Enum(TermType), nullable=False)
    due_days: int = Column('due_days', INTEGER(), default=0)
    discount_days: int = Column('discount_days', INTEGER())
    discount: Decimal = Column('discount', DECIMAL(), default=Decimal(0))
    discount_type: DiscountType = Column('discount_type', Enum(DiscountType), nullable=False)
    cutoff: int = Column('cutoff', INTEGER(), default=0)

    # relation definitions
    parent = relationship('Term', remote_side=guid)
    children = relationship('Term',
                            back_populates='parent',
                            cascade='all, delete-orphan')
    child = None

    def __init__(self, book):
        super().__init__(ID_TERM, book)
        self.discount_days = 0
        self.discount = Decimal(0)
        self.cutoff = 0
        self.refcount = 0
        self.invisible = False
        self.discount_type = DiscountType.PRICE_VALUE
        self.type = TermType.DAYS
        self.add_object(self)
        self.child = None
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_TERM, book)
        if self.parent is not None:
            self.parent.set_child(self)
        self.add_object(self)
        Event.gen(self, EVENT_CREATE)

    def copy(self):
        t = Term(self.book)
        t.begin_edit()
        t.set_name(self.get_name())
        t.set_description(self.get_description())
        t.type = self.type
        t.due_days = self.due_days
        t.disc_days = self.discount_days
        t.discount = self.discount
        t.cutoff = self.cutoff
        t.mark()
        t.commit_edit()
        return t

    def mark(self):
        self.set_dirty()
        Event.gen(self, EVENT_MODIFY)

    def set_name(self, name):
        if name != self.name:
            self.begin_edit()
            self.name = name
            self.mark()
            self.commit_edit()

    def get_name(self):
        return self.name

    def set_description(self, description):
        if description != self.description:
            self.begin_edit()
            self.description = description
            self.mark()
            self.commit_edit()

    def get_description(self):
        return self.description

    def set_refcount(self, refcount):
        if refcount != self.refcount:
            self.begin_edit()
            self.refcount = refcount
            self.mark()
            self.commit_edit()

    def get_refcount(self):
        return self.refcount

    def increment_ref(self):
        if self.parent is not None or self.invisible:
            return
        self.begin_edit()
        self.refcount += 1
        self.mark()
        self.commit_edit()

    def decrement_ref(self):
        if self.parent is not None or self.invisible or self.refcount <= 1:
            return
        self.begin_edit()
        self.refcount += 1
        self.mark()
        self.commit_edit()

    def set_parent(self, parent):
        if parent != self.parent:
            self.begin_edit()
            self.parent = parent
            self.mark()
            self.commit_edit()

    def get_parent(self):
        return self.parent

    def return_child(self, make_new):
        child = None
        if self.child is not None:
            return self.child
        if self.parent is not None or self.invisible:
            return self
        if make_new:
            child = self.copy()
            self.set_child(child)
            child.set_parent(self)
        return child

    def set_child(self, child):
        self.begin_edit()
        self.child = child
        self.mark()
        self.commit_edit()

    def set_discount(self, discount):
        if discount != self.discount:
            self.begin_edit()
            self.discount = discount
            self.mark()
            self.commit_edit()

    def get_discount(self):
        return self.discount

    def set_cutoff(self, cutoff):
        if cutoff != self.cutoff:
            self.begin_edit()
            self.cutoff = cutoff
            self.mark()
            self.commit_edit()

    def get_cutoff(self):
        return self.cutoff

    def set_discount_days(self, discount_days):
        if discount_days != self.discount_days:
            self.begin_edit()
            self.discount_days = discount_days
            self.mark()
            self.commit_edit()

    def get_discount_days(self):
        return self.discount_days

    def set_due_days(self, due_days):
        if due_days != self.due_days:
            self.begin_edit()
            self.due_days = due_days
            self.mark()
            self.commit_edit()

    def get_due_days(self):
        return self.due_days

    def set_type(self, type):
        if type != self.type:
            self.begin_edit()
            self.type = type
            self.mark()
            self.commit_edit()

    def get_type(self):
        return self.type

    def set_discount_type(self, discount_type):
        if discount_type != self.discount_type:
            self.begin_edit()
            self.discount_type = discount_type
            self.mark()
            self.commit_edit()

    def get_discount_type(self):
        return self.discount_type

    def set_invisible(self, invisible):
        if invisible != self.invisible:
            self.begin_edit()
            self.invisible = invisible
            self.mark()
            self.commit_edit()

    def get_invisible(self):
        return self.invisible

    @classmethod
    def get_terms(cls, book):
        bi = book.get_data(ID_TERM)
        return bi

    @classmethod
    def get_list(cls, book):
        return list(filter(lambda a: a.parent is None, cls.get_terms(book)))

    @classmethod
    def lookup_by_name(cls, book, name):
        l = cls.get_list(book)
        for x in l:
            if x.name == name:
                return x
        return None

    def __eq__(self, other):
        if self is None or other is None:
            return False
        if self.name != other.name:
            return False
        if self.description != other.description:
            return False
        return True

    @classmethod
    def add_object(cls, term):
        terms = cls.get_terms(term.get_book())
        terms.append(term)
        terms.sort(key=cmp_to_key(cls.__eq__))

    @classmethod
    def remove_object(cls, term):
        terms = cls.get_terms(term.get_book())
        terms.remove(term)

    @classmethod
    def book_begin(cls, book):
        book.set_data(ID_TERM, [])

    def is_dirty(self):
        return self.get_dirty_flag()

    def free(self):
        self.remove_object(self)
        if self.parent is not None:
            self.parent.remove_child(self)
        for child in self.children:
            child.set_parent(None)
        Event.gen(self, EVENT_DESTROY)

    def on_done(self):
        pass

    def changed(self):
        self.child = None

    def commit_edit(self):
        if not super().commit_edit():
            return
        self.commit_edit_part2(self.on_error, self.on_done, self.free)

    def refers_to(self, other):
        pass

    @classmethod
    def register(cls):
        params = [
            Param(TERM_NAME, TYPE_STRING, cls.get_name, cls.set_name),
            Param(TERM_DESC, TYPE_STRING, cls.get_description, cls.set_description),
            Param(TERM_TYPE, TYPE_ENUM, cls.get_type, cls.set_type),
            Param(TERM_DUEDAYS, TYPE_INT32, cls.get_due_days, cls.set_due_days),
            Param(TERM_DISCDAYS, TYPE_INT32, cls.get_discount_days, cls.set_discount_days),
            Param(TERM_DISCOUNT, TYPE_NUMERIC, cls.get_discount, cls.set_discount),
            Param(TERM_DISCOUNT_TYPE, TYPE_ENUM, cls.get_discount_type, cls.set_discount_type),
            Param(TERM_CUTOFF, TYPE_INT32, cls.get_cutoff, cls.set_cutoff),
            Param(TERM_REFCOUNT, TYPE_INT64, cls.get_refcount, None),
            Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
            Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None)]
        ObjectClass.register(cls, ID_TERM, cls.__eq__, params)

    def __repr__(self):
        return u"Term(%s)" % self.name
