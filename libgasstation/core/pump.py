from sqlalchemy import Column, VARCHAR, ForeignKey, DATE
from sqlalchemy.orm import relationship, reconstructor
from sqlalchemy_utils import UUIDType

from ._declbase import DeclarativeBaseGuid, ID_PUMP
from .event import *
from .query_private import *

PUMP_NAME = "pump-name"


class Pump(DeclarativeBaseGuid):
    __tablename__ = 'pump'
    location_guid = Column(UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    name = Column('name', VARCHAR(length=20), nullable=False)
    serial_number = Column('serial_number', VARCHAR(length=200), nullable=False, unique=True)
    manufactured_date = Column('manufactured_date', DATE(), nullable=False)
    nozzles = relationship('Nozzle', back_populates="pump", lazy="joined", cascade='all, delete-orphan')
    location = relationship("Location")

    def __init__(self, book):
        self.infant = True
        super().__init__(ID_PUMP, book)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        self.infant = False
        book = Session.get_current_book()
        super().__init__(ID_PUMP, book)
        Event.gen(self, EVENT_CREATE)

    def get_name(self):
        return self.name or "Pump %s" % Pump.count

    def set_name(self, name):
        if name != self.name:
            self.begin_edit()
            self.name = name.strip()
            self.set_dirty()
            self.commit_edit()

    @classmethod
    def lookup_name(cls, book, name):
        a = book.get_collection(ID_PUMP)
        for cat in a.values():
            if cat.get_name() == name:
                return cat
    @classmethod
    def lookup_name_with_location(cls, book, name, location):
        a = book.get_collection(ID_PUMP)
        for cat in a.values():
            if cat.name == name.strip() and cat.location == location:
                return cat

    def get_serial_number(self):
        return self.serial_number

    def set_serial_number(self, sn):
        if sn != self.serial_number:
            self.begin_edit()
            self.serial_number = sn
            self.set_dirty()
            self.commit_edit()

    def get_manufactured_date(self):
        return self.manufactured_date

    def set_manufactured_date(self, manufactured_date):
        if manufactured_date != self.manufactured_date:
            self.begin_edit()
            self.manufactured_date = manufactured_date
            self.set_dirty()
            self.commit_edit()

    def get_location(self):
        return self.location

    def set_location(self, location):
        if location != self.location:
            self.begin_edit()
            self.location = location
            self.set_dirty()
            self.commit_edit()

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        self.name = None
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    @classmethod
    def register(cls):
        params = [Param(PUMP_NAME, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_PUMP, None, params)

    def __repr__(self):
        return self.get_name()
