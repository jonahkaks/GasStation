from sqlalchemy import VARCHAR, ForeignKey
from sqlalchemy.orm import reconstructor, relationship
from sqlalchemy_utils import URLType

from ._declbase import *
from .bank import Bank
from .engine import *
from .event import *


class ContactImage(DeclarativeBaseGuid):
    __tablename__ = "contact_image"
    image_url = Column("image_url", URLType, nullable=False)
    contact_guid = Column('contact_guid', UUIDType(binary=False), ForeignKey('contact.guid'))
    contact = relationship("Contact", back_populates="profile_image")

    def __init__(self, book, image_url):
        super().__init__("ContactImage", book)
        self.image_url = image_url
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, "ContactImage", book)
        Event.gen(self, EVENT_CREATE)

    def refers_to(self, other):
        if isinstance(other, Contact):
            return self.contact == other
        return False


class ContactBankDetail(DeclarativeBaseGuid):
    __tablename__ = 'contact_bank_detail'
    contact_guid: str = Column("contact_guid", UUIDType(binary=False), ForeignKey('contact.guid'), nullable=False)
    bank_guid: str = Column("bank_guid", UUIDType(binary=False), ForeignKey('bank.guid'), nullable=False)
    contact = relationship("Contact", back_populates="bank_details")
    bank = relationship("Bank")

    def __init__(self, book):
        super().__init__("ContactBankDetail", book)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, "ContactBankDetail", book)
        Event.gen(self, EVENT_CREATE)

    def refers_to(self, other):
        if isinstance(other, Contact):
            return self.contact == other
        return False


class Contact(ImageMixin, DeclarativeBaseGuid):
    __tablename__ = "contact"
    full_name = Column("full_name", VARCHAR(200), nullable=False, unique=True)
    display_name = Column("display_as", VARCHAR(200), nullable=False, unique=True)
    website = Column("website", URLType)
    addresses = relationship("Address", lazy="immediate", cascade="delete", back_populates="contact")
    emails = relationship("Email", lazy="immediate", cascade="delete", back_populates="contact")
    phones = relationship("Phone", lazy="immediate", cascade="delete", back_populates="contact")
    profile_image:ContactImage = relationship("ContactImage", uselist=False, cascade="delete", back_populates="contact")
    bank_details:ContactBankDetail = relationship("ContactBankDetail", uselist=False, cascade="delete", back_populates="contact")

    def __init__(self, book):
        super().__init__(ID_CONTACT, book)
        Event.gen(self, EVENT_CREATE)

    @classmethod
    def lookup_name(cls, book, display_name):
        col = book.get_collection(ID_CONTACT)
        for cat in col.values():
            if cat.get_full_name().lower().replace(" ", "") == display_name.lower().replace(" ", ""):
                return cat

    def get_image_url(self):
        return self.profile_image.image_url if self.profile_image is not None else None

    def set_image_url(self, url):
        if url is not None:
            if self.profile_image is not None:
                if self.profile_image.image_url == url:
                    return
            else:
                self.profile_image = ContactImage(self.book, url)
            self.begin_edit()
            self.profile_image.begin_edit()
            self.profile_image.image_url = url
            self.profile_image.set_dirty()
            self.set_dirty()
            self.profile_image.commit_edit()
            self.commit_edit()

    def get_bank_details(self)->Bank:
        return self.bank_details.bank if self.bank_details is not None else None

    def set_bank_details(self, name=None, branch=None, account_name=None, account_number=None):
        self.begin_edit()
        if self.bank_details is not None:
            bank = self.bank_details.bank
        else:
            bank =  Bank(self.book)
            self.bank_details = ContactBankDetail(bank = bank)
        bank.name = name
        bank.branch = branch
        bank.account_name = account_name
        bank.account_number = account_number
        self.set_dirty()
        self.commit_edit()

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, ID_CONTACT, book)
        Event.gen(self, EVENT_CREATE)

    def get_full_name(self):
        return self.full_name if self.full_name is not None else ""

    def set_full_name(self, full_name: str):
        if full_name != self.full_name and len(full_name):
            self.begin_edit()
            self.full_name = full_name.strip()
            self.mark()
            self.commit_edit()

    def get_display_name(self):
        return self.display_name

    def set_display_name(self, display_name: str):
        if display_name != self.display_name:
            self.begin_edit()
            self.display_name = display_name
            self.mark()
            self.commit_edit()

    def get_website(self):
        return self.website

    def set_website(self, website):
        if website != self.website and len(website):
            self.begin_edit()
            self.website = website
            self.mark()
            self.commit_edit()

    def mark(self):
        self.set_dirty()
        Event.gen(self, EVENT_MODIFY)

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)
        if self.profile_image is not None:
            self.profile_image.commit_edit_part2(None, None, None)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        for address in self.addresses:
            address.dispose()
        for email in self.emails:
            email.dispose()
        for phone in self.phones:
            phone.dispose()
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def on_error(self, error):
        Engine.signal_commit_error(error)


    def __lt__(self, other):
        if other is None:
            return True
        try:
            return self.get_display_name() < other.get_display_name()
        except TypeError:
            return False
