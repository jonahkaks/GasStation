from sqlalchemy import func

from .product import Product
from .tax import TaxAmountType, Tax
from .transaction import *

ENTRY_DATE = "date"
ENTRY_DATE_ENTERED = "date-entered"
ENTRY_DESC = "desc"
ENTRY_QTY = "qty"
ENTRY_PRICE = "price"
ENTRY_PRODUCT = "product"
ENTRY_BILLABLE = "billable?"
ENTRY_ORDER = "order"
ENTRY_INVOICE = "invoice"
ENTRY_BILL = "bill"


class Entry(DeclarativeBaseGuid):
    __abstract__ = True
    __table_args__ = {'implicit_returning': False}
    date = Column('date', Date(), nullable=False)
    date_entered = Column('date_entered', DateTime(timezone=False), default=func.now())
    description = Column('description', VARCHAR(length=200))

    @declared_attr
    def uom_guid(cls):
        return Column('uom_guid', UUIDType(binary=False), ForeignKey("unit_of_measure.guid"), nullable=False)

    @declared_attr
    def unit_of_measure(cls):
        return relationship("UnitOfMeasure", foreign_keys=[cls.uom_guid])

    def set_date(self, date):
        if self.date == date:
            return
        self.begin_edit()
        self.date = date
        self.set_dirty()
        self.commit_edit()

    def get_date(self):
        return self.date

    def set_date_entered(self, date_entered):
        if self.date_entered == date_entered:
            return
        self.begin_edit()
        self.date_entered = date_entered
        self.set_dirty()
        self.commit_edit()

    def get_date_entered(self):
        return self.date_entered

    def set_description(self, desc):
        if desc != self.description:
            self.begin_edit()
            self.description = desc
            self.set_dirty()
            self.commit_edit()

    def get_description(self):
        return self.description

    def set_unit_of_measure(self, uom):
        if uom != self.unit_of_measure:
            self.begin_edit()
            self.unit_of_measure = uom
            self.set_dirty()
            self.commit_edit()

    def get_unit_of_measure(self):
        return self.unit_of_measure

    def refers_to(self, other):
        if isinstance(other, Product):
            return self.product == other
        elif isinstance(other, Tax):
            return other == self.tax
        return False

    def __lt__(self, other):
        if other is None:
            return False
        if self.description is None or other.description is None:
            return False
        return self.description < other.description

    def commit_edit(self):
        if not super().commit_edit():
            return
        Event.gen(self, EVENT_MODIFY)

    def __repr__(self):
        return u"Entry<{}>".format(self.description)

    @classmethod
    def _register(cls, child_type, parent_type):
        params = [Param(ENTRY_DESC, TYPE_STRING, cls.get_description, cls.set_description),
                  Param(ENTRY_DATE_ENTERED, TYPE_DATE, cls.get_date_entered, cls.set_date_entered),
                  Param(ENTRY_DATE, TYPE_DATE, cls.get_date, cls.set_date),
                  Param(PARAM_BOOK, ID_BOOK, cls.get_book),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid),
                  ]
        if parent_type == ID_BILL:
            params.append(Param(ENTRY_BILL, parent_type, cls.get_bill))
        elif parent_type == ID_INVOICE:
            params.append(Param(ENTRY_INVOICE, parent_type, cls.get_invoice))
        ObjectClass.register(cls, child_type, None, params)


class QuantityMixin:
    price = Decimal(0)
    quantity = Decimal(0)
    value = Decimal(0)
    tax_modtime = 0
    tax_values = defaultdict(Decimal)
    values_dirty = True
    product: Product
    tax_included = True

    def recompute_values(self):
        tax = self.product.get_tax() if self.product is not None else None
        if tax is not None:
            modtime = tax.get_last_modified_time()
            if self.tax_modtime != modtime:
                self.values_dirty = True
                self.tax_modtime = modtime
        if not self.values_dirty:
            return
        if self.tax_values is not None:
            self.tax_values.clear()
        self.compute_value_internal()
        self.values_dirty = False

    def get_value(self):
        self.recompute_values()
        return self.value

    def get_tax_value(self):
        self.recompute_values()
        return self.tax_values

    def compute_value_internal(self):
        net_price = self.price
        percent = Decimal(100)
        tpercent = Decimal(0)
        tvalue = Decimal(0)
        tax: Tax = self.product.get_tax() if self.product is not None else None
        aggregate = self.quantity * self.price
        entries = tax.get_entries() if tax is not None else []

        for entry in entries:
            amount = entry.get_rate()
            _type = entry.get_type()
            if _type == TaxAmountType.VALUE:
                tvalue += amount
            elif _type == TaxAmountType.PERCENT:
                tpercent += amount
        tpercent /= percent
        if tax is not None and self.tax_included:
            pretax = aggregate - tvalue
            pretax /= tpercent + Decimal(1)
            if not self.quantity.is_zero():
                net_price = pretax / self.quantity
        else:
            pretax = aggregate

        for entry in entries:
            amount = entry.get_rate()
            _type = entry.get_type()
            acc = entry.get_account()
            self.tax_values.setdefault(acc, Decimal(0))
            if _type == TaxAmountType.VALUE:
                self.tax_values[acc] = amount
            elif _type == TaxAmountType.PERCENT:
                amount /= percent
                tax = pretax * amount
                self.tax_values[acc] = tax

        self.value = aggregate
        self.price = net_price


class InvoiceEntry(Entry, QuantityMixin):
    __tablename__ = "invoice_entry"
    product_guid = Column("product_guid", UUIDType(binary=False), ForeignKey("product.guid"), nullable=False)
    invoice_guid = Column('invoice_guid', UUIDType(binary=False), ForeignKey("invoice.guid"), nullable=False)
    quantity = Column('quantity', DECIMAL(), default=Decimal(0))
    tax_included = Column('tax_included', BOOLEAN(), default=False)
    price = Column('price', DECIMAL(), default=Decimal(0))
    invoice = relationship("Invoice", back_populates="entries")
    product = relationship('Product')

    def __init__(self, book):
        super().__init__(ID_INVOICE_ENTRY, book)
        self.price = Decimal(0)
        self.quantity = Decimal(0)
        self.value = Decimal(0)
        self.tax_modtime = 0
        self.tax_values = defaultdict(Decimal)
        self.values_dirty = True
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_INVOICE_ENTRY, book)
        self.value = Decimal(0)
        self.values_dirty = True
        self.tax_values = defaultdict(Decimal)
        self.tax_modtime = 0
        Event.gen(self, EVENT_CREATE)

    def get_account(self):
        return self.product.get_sales_account() if self.product is not None else None

    def set_price(self, price):
        if price != self.price:
            self.begin_edit()
            self.price = price
            self.set_dirty()
            self.commit_edit()
            self.values_dirty = True

    def get_price(self):
        return self.price

    def set_quantity(self, qty, is_credit_note=False):
        if qty != self.quantity:
            self.begin_edit()
            self.quantity = -qty if is_credit_note else qty
            self.set_dirty()
            self.commit_edit()
            self.values_dirty = True

    def get_quantity(self):
        return self.quantity

    def set_product(self, product):
        if product != self.product:
            self.begin_edit()
            self.product = product
            self.set_dirty()
            self.commit_edit()
            self.values_dirty = True

    def get_product(self):
        return self.product

    def get_invoice(self):
        return self.invoice

    def get_product_name(self):
        return self.product.get_name() if self.product is not None else ""

    @classmethod
    def register(cls):
        super()._register(ID_INVOICE_ENTRY, ID_INVOICE)


class BillEntry(Entry, QuantityMixin):
    __tablename__ = "bill_entry"
    product_guid = Column("product_guid", UUIDType(binary=False), ForeignKey("product.guid"), nullable=False)
    bill_guid = Column('bill_guid', UUIDType(binary=False), ForeignKey("bill.guid"), nullable=False)
    quantity = Column('quantity', DECIMAL(), default=Decimal(0))
    tax_included = Column('tax_included', BOOLEAN(), default=True)
    price = Column('price', DECIMAL(), default=Decimal(0))
    bill = relationship("Bill", back_populates="entries")
    product = relationship('Product')

    def __init__(self, book):
        super().__init__(ID_BILL_ENTRY, book)
        self.price = Decimal(0)
        self.quantity = Decimal(0)
        self.value = Decimal(0)
        self.tax_modtime = 0
        self.values_dirty = True
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_BILL_ENTRY, book)
        self.value = Decimal(0)
        self.values_dirty = True
        self.tax_modtime = 0
        Event.gen(self, EVENT_CREATE)

    def get_bill(self):
        return self.bill

    def get_account(self):
        return self.product.get_inventory_account() if self.product is not None else None

    def set_price(self, price):
        if price != self.price:
            self.begin_edit()
            self.price = price
            self.set_dirty()
            self.commit_edit()
            self.values_dirty = True

    def get_price(self):
        return self.price

    def set_quantity(self, qty, is_debit_note=False):
        if qty != self.quantity:
            self.begin_edit()
            self.quantity = -qty if is_debit_note else qty
            self.set_dirty()
            self.commit_edit()
            self.values_dirty = True

    def get_quantity(self):
        return self.quantity

    def set_product(self, product):
        if product != self.product:
            self.begin_edit()
            self.product = product
            self.set_dirty()
            self.commit_edit()
            self.values_dirty = True

    def get_product(self):
        return self.product

    def get_product_name(self):
        return self.product.get_name() if self.product is not None else ""

    @classmethod
    def register(cls):
        super()._register(ID_BILL_ENTRY, ID_BILL)


class BillAccountEntry(Entry):
    __tablename__ = "bill_account_entry"
    account_guid = Column("account_guid", UUIDType(binary=False), ForeignKey("account.guid"), nullable=False)
    bill_guid = Column('bill_guid', UUIDType(binary=False), ForeignKey("bill.guid"), nullable=False)
    amount = Column('amount', DECIMAL(), default=Decimal(0))
    bill = relationship("Bill", back_populates="account_entries")
    account = relationship('Account')

    def __init__(self, book):
        super().__init__(ID_BILL_ACCOUNT_ENTRY, book)
        self.amount = Decimal(0)
        self.tax_modtime = 0
        self.values_dirty = True
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_BILL_ACCOUNT_ENTRY, book)
        self.values_dirty = True
        self.tax_modtime = 0
        Event.gen(self, EVENT_CREATE)

    def get_account(self):
        return self.account

    def set_account(self, account):
        if self.account != account:
            self.begin_edit()
            self.account = account
            self.commit_edit()

    def get_amount(self):
        return self.amount

    def set_amount(self, amount, is_debit_note=False):
        if self.amount != amount:
            self.begin_edit()
            if is_debit_note:
                amount *= -1
            self.amount = amount
            self.commit_edit()

    def get_bill(self):
        return self.bill

    @classmethod
    def register(cls):
        super()._register(ID_BILL_ACCOUNT_ENTRY, ID_BILL)

    def get_value(self):
        return self.amount
