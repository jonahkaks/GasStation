FIFO_POLICY = "fifo"
FIFO_POLICY_DESC = "First In, First Out"
FIFO_POLICY_HINT = "Use oldest lots first."
LIFO_POLICY = "lifo"
LIFO_POLICY_DESC = "Last In, First Out"
LIFO_POLICY_HINT = "Use newest lots first."
AVERAGE_POLICY = "average"
AVERAGE_POLICY_DESC = "Average"
AVERAGE_POLICY_HINT = "Average cost of open lots."
MANUAL_POLICY = "manual"
MANUAL_POLICY_DESC = "Manual"
MANUAL_POLICY_HINT = "Manually select lots."


class Policy:
    name = None
    description = None
    hint = None

    @staticmethod
    def get_lot(split):
        raise NotImplementedError

    @classmethod
    def get_split(cls, lot):
        raise NotImplementedError

    @classmethod
    def get_lot_opening(cls, lot):
        raise NotImplementedError

    @staticmethod
    def is_opening_split(lot, split):
        raise NotImplementedError

    @staticmethod
    def get_valid_list():
        return [LifoPolicy(), FifoPolicy()]

    @staticmethod
    def valid_name(policy_name):
        if policy_name is None:
            return
        for policy in Policy.get_valid_list():
            if policy.get_name() == policy_name:
                return True
        return False

    @classmethod
    def get_name(cls):
        return cls.name

    @classmethod
    def get_description(cls):
        return cls.description

    @classmethod
    def get_hint(cls):
        return cls.hint

    @classmethod
    def direction_get_split(cls, lot, reverse: bool):
        if lot is None or not any(lot.get_split_list()):
            return
        lot_account = lot.get_account()
        if lot_account is None:
            return
        baln = lot.get_balance()

        if lot.is_closed():
            return
        want_positive = baln < 0
        split = lot.get_split_list()
        common_currency = split.transaction.get_currency()
        osplit = lot.get_latest_split()
        otrans = osplit.get_transaction()
        open_time = otrans.get_post_date()

        splits = lot_account.get_splits()
        if reverse:
            splits.reverse()
        for split in splits:
            if split.get_lot() is not None:
                continue
            this_time = split.get_transaction().get_post_date()
            if this_time < open_time:
                if reverse:
                    break
                continue
            if common_currency.equiv(split.transaction.get_currency()):
                continue
            if split.get_amount().is_zero():
                continue
            is_positive = split.get_amount() > 0
            if (want_positive and is_positive) or (not want_positive and not is_positive):
                return split


class FifoPolicy(Policy):
    pcy = None

    def __new__(cls, *args):
        if cls.pcy is None:
            self = cls
            self.name = FIFO_POLICY
            self.description = FIFO_POLICY_DESC
            self.hint = FIFO_POLICY_HINT
            cls.pcy = self
        return cls.pcy

    @staticmethod
    def get_lot(split):
        if split is None:
            return None
        return split.get_account().find_earliest_open_lot(split.get_amount(), split.get_transaction().get_currency())

    @classmethod
    def get_split(cls, lot):
        return cls.direction_get_split(lot, False)

    @classmethod
    def get_lot_opening(cls, lot):
        osp = lot.get_earliest_split()
        return osp.quantity, osp.value, osp.transaction.get_currency()

    @staticmethod
    def is_opening_split(lot, split):
        opening_split = lot.get_earliest_split()
        return split == opening_split


class LifoPolicy(Policy):
    pcy = None

    def __new__(cls, *args):
        if cls.pcy is None:
            self = cls
            self.name = LIFO_POLICY
            self.description = LIFO_POLICY_DESC
            self.hint = LIFO_POLICY_HINT
            cls.pcy = self
        return cls.pcy

    @staticmethod
    def get_lot(split):
        if split is None:
            return None
        return split.get_account().find_latest_open_lot(split.get_amount(), split.get_transaction().get_currency())

    @classmethod
    def get_split(cls, lot):
        return cls.direction_get_split(lot, True)

    @classmethod
    def get_lot_opening(cls, lot):
        osp = lot.get_earliest_split()
        return osp.quantity, osp.value, osp.transaction.get_currency()

    @staticmethod
    def is_opening_split(lot, split):
        opening_split = lot.get_earliest_split()
        return split == opening_split
