import datetime
from collections import defaultdict

from sqlalchemy import Date

from .location import Location
from .split import *

TRANS_KVP = "kvp"
TRANS_NUM = "num"
TRANS_DESCRIPTION = "desc"
TRANS_DATE_ENTERED = "date-entered"
TRANS_DATE_POSTED = "date-posted"
TRANS_DATE_DUE = "date-due"
TRANS_IMBALANCE = "trans-imbalance"
TRANS_IS_BALANCED = "trans-balanced?"
TRANS_IS_CLOSING = "trans-is-closing?"
TRANS_NOTES = "notes"
TRANS_LOCATION = "location"
TRANS_ASSOCIATION = "assoc"
TRANS_TYPE = "type"
TRANS_VOID_STATUS = "void-p"
TRANS_VOID_REASON = "void-reason"
TRANS_VOID_TIME = "void-time"
TRANS_SPLITLIST = "split-list"
RECONCILED_MATCH_TYPE = "reconciled-match"


class TransactionType(IntEnum):
    NONE = 0
    INVOICE = 1
    PAYMENT = 2
    LINK = 3

    def __str__(self):
        return self.name[0]


class Transaction(DeclarativeBaseGuid):
    """
    A GasStation Transaction.

    Attributes:
        currency (:class:`core.commodity.Commodity`): currency of the transaction. This attribute is
            write-once (i.e. one cannot change it after being set)
        description (str): description of the transaction
        enter_date (:class:`datetime.datetime`): datetimetime at which transaction is entered
        post_date (:class:`datetime.date`): day on which transaction is posted
        num (str): user provided transaction number
        splits (list of :class:`.split.Split`): list of the splits of the transaction
        scheduled_transaction  (:class:`ScheduledTransaction`): scheduled transaction behind the transaction
        notes (str): notes on the transaction (provided via a slot)
    """
    __tablename__ = 'transaction'
    # column definitions
    currency_guid = Column('currency_guid', UUIDType(binary=False), ForeignKey('commodity.guid'), nullable=False)
    location_guid = Column('location_guid', UUIDType(binary=False), ForeignKey('location.guid'))
    num = Column('num', VARCHAR(length=15))
    post_date = Column('post_date', Date(), nullable=False)
    enter_date = Column('enter_date', DateTime(timezone=False), nullable=False,
                        default=datetime.datetime.now().replace(microsecond=0))
    description = Column('description', VARCHAR(length=250))
    notes = Column('notes', VARCHAR(length=200))
    void_former_notes = Column('void_former_notes', VARCHAR(length=200))
    void_reason = Column('void_reason', VARCHAR(length=200))
    reversed_by = Column('reversed_by', VARCHAR(length=30))
    void_time = Column('void_time', DateTime(timezone=False))
    type = Column('type', Enum(TransactionType), nullable=False, default=TransactionType.NONE)
    book_closing = Column('book_closing', BOOLEAN(), default=False)

    readonly_reason = Column("readonly_reason", VARCHAR(length=250), default=None)
    scheduled_guid = Column('scheduled_guid', UUIDType(binary=False), ForeignKey('scheduled_transaction.guid'))
    scheduled_transaction = relationship("ScheduledTransaction")
    # relation definitions
    currency = relationship('Commodity',
                            back_populates='transactions',
                            )
    location = relationship('Location')
    splits: List[Split] = relationship('Split',
                                       back_populates="transaction",
                                       cascade='all, delete-orphan')
    invoice = relationship("Invoice", back_populates="posted_txn", uselist=False)
    bill = relationship("Bill", back_populates="posted_txn", uselist=False)
    scrub_data = 0
    marker = 0
    isClosingTxn_cached = 0
    e_type = ID_TRANS

    @classmethod
    def enable_data_scrubbing(cls):
        cls.scrub_data = 1

    @classmethod
    def disable_data_scrubbing(cls):
        cls.scrub_data = 0

    def __init__(self, book):
        self.orig_trans = None
        self.post_date = datetime.date.today()
        self.enter_date = datetime.datetime.now()
        self.book_closing = False
        self.type = TransactionType.NONE
        if book is not None:
            super().__init__(ID_TRANS, book)
            Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        self.orig_trans = None
        from .session import Session
        self.infant = False
        book = Session.get_current_book()
        super().__init__(ID_TRANS, book)
        self.begin_edit()
        self.dirty = True

    def order(self, other):
        return self.order_num_action(None, other, None)

    def get_invoice(self):
        return self.invoice

    def get_bill(self):
        return self.bill

    def __gt__(self, other):
        return self.order_num_action(None, other, None)

    def order_num_action(self, action, other, oaction):
        if other is None:
            return False
        if self == other:
            return False
        if self.post_date != other.post_date:
            return True if self.post_date > other.post_date else False
        ia = self.is_closing()
        ib = other.is_closing()
        if ia != ib:
            return True if ia else False
        if self.enter_date != other.enter_date:
            return True if self.enter_date > other.enter_date else False
        if action is not None and oaction is not None:
            return True if action > oaction else False
        else:
            na = int(self.num or 0)
            nb = int(other.num or 0)
            if na != nb:
                return True if na > nb else False

        if self.description is not None and other.description is not None:
            return True if self.description > other.description else False

        if self.guid is not None and other.guid is not None and self.guid != other.guid:
            return True if self.guid > other.guid else False
        return False

    def get_split_equal_to_ancestor(self, anchor):
        if self is None:
            return
        for split in self.splits:
            split_acc = split.get_account()
            if not self.still_has_split(split):
                continue
            if split_acc == anchor:
                return split
            if anchor is not None and split_acc.has_ancestor(anchor):
                return split

    def get_account_balance(self, account):
        last_split = None
        for sp in self.splits:
            if not self.still_has_split(sp):
                continue
            if sp.account != account:
                continue

            if last_split is None:
                last_split = sp
                continue
            if last_split < sp:
                last_split = sp

        return last_split.balance if last_split is not None else Decimal(0)

    @staticmethod
    def set_new_value(split, curr, old_curr, rate):
        split_com = split.get_account().get_commodity()
        if curr.equal(split_com):
            split.set_value(split.get_amount())
        elif old_curr.equal(split_com):
            split.set_share_price(rate)
        else:
            old_rate = split.get_value() / split.get_amount()
            new_rate = old_rate / rate
            split.set_share_price(new_rate)

    def set_currency(self, curr):
        old_curr = self.currency
        if curr is None or self.currency == curr:
            return
        self.begin_edit()
        self.currency = curr
        if old_curr is not None and len(self.splits):
            rate = self.find_new_rate(curr)
            if not rate.is_zero():
                for sp in self.splits:
                    self.set_new_value(sp, curr, old_curr, rate)
            else:
                for sp in self.splits:
                    sp.set_value(sp.get_value())
        self.set_dirty()
        self.mark()
        self.commit_edit()

    def get_currency(self):
        return self.currency

    def get_location(self):
        return self.location

    def set_location(self, location: Location):
        if location != self.location:
            self.begin_edit()
            self.location = location
            self.set_dirty()
            self.mark()
            self.commit_edit()

    def set_post_date(self, post_date: datetime.date):
        print(post_date, self.post_date)
        if post_date != self.post_date:
            self.begin_edit()
            self.post_date = post_date
            self.set_dirty()
            self.mark()
            self.commit_edit()

    def set_enter_date(self, enter_date: datetime.datetime):
        if enter_date != self.enter_date:
            self.begin_edit()
            self.enter_date = enter_date
            self.set_dirty()
            self.mark()
            self.commit_edit()

    def get_post_date(self):
        return self.post_date if self.post_date is not None else datetime.date.today()

    def get_enter_date(self):
        if self.enter_date is None:
            self.enter_date = datetime.datetime.now()
        return self.enter_date

    def set_notes(self, notes):
        if notes != self.notes and notes is not None:
            self.begin_edit()
            self.notes = notes
            self.set_dirty()
            self.commit_edit()

    def get_notes(self):
        return self.notes

    def find_new_rate(self, curr):
        rate = Decimal(0)
        for sp in self.splits:
            split_com = sp.get_account().get_commodity() if sp.get_account() else None
            if curr.equal(split_com):
                rate = sp.get_amount() / sp.get_value()
                break
        return rate

    def find_split_by_account(self, acc):
        for split in self.splits:
            if split.account == acc:
                return split

    def set_description(self, description):
        if description != self.description and description is not None and description != "":
            self.begin_edit()
            self.description = description
            self.set_dirty()
            self.commit_edit()

    def get_description(self):
        return self.description

    def begin_edit(self):
        if not super().begin_edit():
            return False
        if self.book.is_shutting_down():
            return False
        if self.book is not None:
            be = self.book.get_backend()
            if be is not None and not be.m_loading:
                self.orig_trans = self.dupe()

    def commit_edit(self):
        from .scrub import Scrub
        if not super().commit_edit():
            return False
        self.increase_edit_level()
        if self.was_emptied():
            self.set_destroying(True)

        if not self.get_destroying() and self.scrub_data and not self.get_book().is_shutting_down():
            Scrub.trans_imbalance(self, None, None)

        if self.enter_date == 0 or self.enter_date is None:
            self.enter_date = datetime.datetime.now()
            self.set_dirty()
        if self.get_destroying():
            for sp in self.splits:
                if sp.transaction == self:
                    sp.destroy()
            for sp in self.splits:
                if sp.transaction == self:
                    sp.commit_edit()
        else:
            for sp in self.splits:
                if sp.get_destroying() and sp.transaction == self:
                    sp.commit_edit()
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy_cb)

    def do_destroy_cb(self):
        for sp in self.splits:
            sp.free()
        Event.gen(self, EVENT_DESTROY)
        self.splits.clear()
        self.free()

    def was_emptied(self):
        for split in self.splits:
            if self.still_has_split(split):
                return False
        return True

    def cleanup_commit(self):
        for i, s in enumerate(self.splits):
            if not s.get_dirty():
                continue
            if self != s.transaction or s.get_destroying():
                try:
                    self.splits.remove(s)
                    s.commit_edit()
                except ValueError:
                    pass
                Event.gen(s, EVENT_REMOVE, EventData(self, i))

            if s.transaction == self:
                if s.get_destroying():
                    Event.gen(s, EVENT_DESTROY)
                else:
                    Event.gen(s, EVENT_MODIFY)
                s.commit_edit()

        if self.orig_trans is not None:
            self.orig_trans.free()
            self.orig_trans = None
        self.sort_splits()
        self.decrease_edit_level()
        self.generate_event()
        Event.gen(self, EVENT_MODIFY)

    def generate_event(self):
        for sp in self.splits:
            acc = sp.get_account()
            lot = sp.get_lot()
            if acc is not None:
                Event.gen(acc, EVENT_ITEM_CHANGED, sp)
            if lot is not None:
                Event.gen(lot, EVENT_MODIFY)

    def in_future_by_post_date(self):
        end = datetime.datetime.now().replace(hour=23, minute=59, second=59, microsecond=99)
        this = datetime.datetime.combine(self.post_date, datetime.time(hour=10, minute=59, second=59, microsecond=99))
        if this > end:
            return True
        else:
            return False

    def set_num(self, num):
        self.begin_edit()
        self.num = num
        self.set_dirty()
        self.mark()
        self.commit_edit()

    def get_due_date(self):
        return self.invoice.get_date_due() if self.invoice is not None else self.bill.get_date_due() if self.bill is not None else self.post_date

    def get_num(self):
        return self.num

    def has_split_in_state(self, state):
        return any(filter(lambda sp: sp.get_reconcile_state() == state, self.splits))

    def set_splits(self, splits):
        for sp in splits:
            self.append_split(sp)

    def get_splits(self):
        return self.splits

    def append_split(self, split):
        if isinstance(split, Split):
            split.set_transaction(self)

    def get_account_splits(self, account):
        return [x for x in self.splits if x.account is not None and x.account == account]

    def get_payment_account_splits(self):
        return [x for x in self.splits if x.account is not None and (x.account.get_type().is_asset_liability()
                                                                     or x.account.get_type().is_equity())]

    def get_accounts_payable_receivable_splits(self, strict):
        from .person import Customer, Supplier
        sps = []
        for s in self.splits:
            account = s.get_account()
            if account is not None and account.get_type().is_payable_receivable():
                if not strict:
                    sps.append(s)
                else:
                    lot = s.get_lot()
                    if lot is not None and (lot.get_invoice() is not None or lot.get_bill() is not None
                                            or Customer.from_lot(lot) is not None or Supplier.from_lot(
                                lot) is not None):
                        sps.append(s)

        return sps

    def get_split_equal_to_account(self, account):
        for x in self.splits:
            if x.account == account:
                return x
        return None

    def get_split(self, index):
        try:
            return self.splits[index]
        except IndexError:
            return None

    def get_split_index(self, split):
        try:
            return self.splits.index(split)
        except ValueError:
            return -1

    def still_has_split(self, split):
        return split is not None and split.get_transaction() == self and not split.get_destroying()

    def get_type(self):
        return self.type

    def set_type(self, t):
        self.begin_edit()
        self.type = t
        self.set_dirty()
        self.commit_edit()

    def scrub_splits(self):
        from .scrub import Scrub
        self.begin_edit()
        for sp in self.splits:
            Scrub.split(sp)
        self.commit_edit()

    def has_reconciled_splits(self):
        for sp in self.splits:
            if sp.get_reconcile_state() in [SplitState.RECONCILED, SplitState.FROZEN]:
                return True
        return False

    def get_account_value(self, account):
        if account is None:
            return Decimal(0)
        return Decimal(sum(map(lambda s: s.get_value(), filter(lambda s: s.get_account() == account, self.splits))))

    def get_account_amount(self, account):
        if account is None:
            return Decimal(0)
        return Decimal(sum(map(lambda s: s.get_amount(), filter(lambda s: s.get_account() == account, self.splits))))

    def get_account_type_amount(self, account_type):
        return Decimal(sum(map(lambda s: s.get_amount(), filter(lambda s: s.get_account().get_type() == account_type, self.splits))))

    def is_balanced(self):
        imbal = Decimal(0)
        imbal_trading = Decimal(0)

        if self.use_trading_accounts():
            for sp in self.splits:
                acc = sp.get_account()
                if acc is None or acc.get_type().is_trading():
                    imbal += sp.get_value()
                else:
                    imbal_trading += sp.get_value()
        else:
            imbal = self.get_imbalance_value()
        if not imbal.is_zero() or not imbal_trading.is_zero():
            return False
        if not self.use_trading_accounts():
            return True
        imbal_list = self.get_imbalance()
        return not any(imbal_list)

    def mark(self):
        for split in self.splits:
            if self.still_has_split(split):
                split.mark()

    def get_imbalance_value(self):
        return Decimal(sum(map(lambda s: s.get_value(), self.splits)))

    def get_imbalance(self):
        imbal_list = defaultdict(Decimal)
        imbal_value = Decimal(0)

        trading_accts = self.use_trading_accounts()
        for sp in self.splits:
            acct = sp.get_account()
            commodity = acct.get_commodity() if acct is not None else None
            if trading_accts and (imbal_list or not commodity.equiv(self.currency) or
                                  sp.get_amount() != sp.get_value()):
                imbal_list.setdefault(self.currency, Decimal(0))
                imbal_list.setdefault(commodity, Decimal(0))
                if not any(imbal_list):
                    imbal_list[self.currency] += imbal_value
                imbal_list[commodity] += sp.get_amount()
            imbal_value += sp.get_value()

        if not any(imbal_list) and not imbal_value.is_zero():
            imbal_list[self.currency] += imbal_value
        return imbal_list

    def destroy(self):
        if not self.is_readonly() or self.book.is_shutting_down():
            self.begin_edit()
            self.set_destroying(True)
            self.commit_edit()

    def use_trading_accounts(self):
        if self.book is not None:
            return self.book.use_trading_accounts()
        return False

    def is_closing(self):
        return self.book_closing

    def set_closing(self, is_closing=True):
        self.begin_edit()
        if is_closing:
            self.book_closing = True
            self.isClosingTxn_cached = True
        else:
            self.book_closing = True
            self.isClosingTxn_cached = False
        self.set_dirty()
        self.commit_edit()

    def get_rate_for_commodity(self, split_com, split):
        if split_com is None or split is None:
            raise ConversionError()
        trans_curr = self.get_currency()
        if trans_curr.equal(split_com):
            return Decimal(1)

        for s in self.splits:
            if not self.still_has_split(s):
                continue
            if s == split:
                comm = s.get_account().get_commodity()
                if split_com.equal(comm):
                    amt = s.get_amount()
                    val = s.get_value()
                    if not amt.is_zero() and not val.is_zero():
                        return amt / val
        raise ConversionError()

    def get_account_conv_rate(self, acc):
        found_acc_match = False
        if acc is None:
            return Decimal(1)
        acc_commod = acc.get_commodity()
        if acc_commod.equal(self.currency):
            return Decimal(1)
        for s in self.splits:
            if not self.still_has_split(s): continue
            split_acc = s.get_account()
            split_commod = split_acc.get_commodity()
            if not (split_acc == acc or split_commod.equal(acc_commod)):
                continue
            found_acc_match = True
            amount = s.get_amount()
            if amount.is_zero():
                continue
            value = s.get_value()
            convrate = amount / value
            return convrate

        if acc and found_acc_match:
            return Decimal(0)
        return Decimal(1)

    def dupe(self):
        Event.suspend()
        to = Transaction(None)
        for sp in self.splits:
            to.splits.append(sp.dupe())
        to.custom_data = self.custom_data.copy() if self.custom_data is not None else None
        to.num = self.num
        to.location = self.location
        to.description = self.description
        to.enter_date = self.enter_date
        to.post_date = self.post_date
        to.currency = self.currency
        to.notes = self.notes
        to.type = self.type
        to.e_type = None
        to.guid = None
        to.book = self.book
        sess = object_session(to)
        if sess is not None:
            sess.expunge(to)
        Event.resume()
        return to

    def copy_to_clipboard(self):
        t = self.dupe()
        return t

    def copy_onto(self, other):
        other.copy_from_clipboard(self, None, None, True)

    def copy_from_clipboard(self, other, from_acc, to_acc, no_date):
        from .account import Account
        if other is None:
            return
        change_accounts = from_acc is not None and isinstance(to_acc, Account) and from_acc != to_acc
        self.begin_edit()
        for sp in self.get_splits():
            sp.destroy()
        self.splits.clear()
        self.set_location(other.get_location())
        self.set_currency(other.get_currency())
        self.set_description(other.get_description())
        if self.get_num() is None or self.get_num() == "":
            self.set_num(other.get_num())
        self.set_notes(other.get_notes())
        if not no_date:
            self.set_post_date(other.get_post_date())
        for split in other.splits:
            new_split = Split(other.get_book())
            split.copy_onto(new_split)
            if change_accounts and split.get_account() == from_acc:
                new_split.set_account(to_acc)
            new_split.set_transaction(self)
        self.commit_edit()

    def is_sx_template(self):
        split0 = self.get_split(0)
        if split0 is not None and (
                split0.scheduled_debit_formula is not None or split0.scheduled_credit_formula is not None):
            return True
        return False

    def is_readonly_by_post_date(self):
        book = self.get_book()
        if not book.uses_auto_readonly():
            return False
        if self.is_sx_template():
            return False
        threshold_date = book.get_auto_readonly_date()
        trans_date = self.get_post_date()
        return trans_date < threshold_date

    def set_readonly(self, reason):
        if reason is not None and len(reason):
            self.begin_edit()
            self.readonly_reason = reason
            self.set_dirty()
            self.commit_edit()

    def clear_readonly(self):
        self.readonly_reason = None

    def get_readonly(self):
        return self.readonly_reason

    def is_readonly(self):
        return self.readonly_reason is not None

    def void(self, reason):
        if reason is None or len(reason) == 0: return
        if self.is_readonly():
            return
        self.begin_edit()
        if bool(self.notes):
            self.void_former_notes = self.notes
        v = "Voided Transaction"
        self.set_notes(v)
        self.void_reason = reason
        self.void_time = datetime.datetime.now()
        for sp in self.splits:
            sp.void()
        self.set_readonly("Transaction Voided")
        self.commit_edit()

    def get_first_payable_receivable_split(self, strict):
        from .invoice import Invoice
        from .person import Person
        for s in self.splits:
            account = s.get_account()
            if account is not None and account.get_type().is_payable_receivable():
                if not strict:
                    return s
                lot = s.get_lot()
                if lot is not None and Invoice.from_lot(lot) is not None and Person.from_lot(lot) is not None:
                    return s

    def get_void_status(self):
        return self.void_reason is not None and len(self.void_reason)

    def get_void_reason(self):
        return self.void_reason

    def get_void_time(self):
        return self.void_time or 0

    def get_reversed_by(self):
        return self.reversed_by

    def reverse(self):
        trans = self.clone()
        trans.begin_edit()
        for sp in trans.splits:
            sp.set_value(-sp.get_value())
            sp.set_amount(-sp.get_amount())
            sp.set_reconcile_state(SplitState.UNRECONCILED)
        trans.reversed_by = self.guid
        trans.clear_readonly()
        trans.set_dirty()
        trans.commit_edit()
        return trans

    def clone_no_kvp(self):
        Event.suspend()
        to = Transaction(None)
        to.num = self.num
        to.location = self.location
        to.description = self.description
        to.enter_date = self.enter_date
        to.post_date = self.post_date
        to.currency = self.currency
        to.notes = self.notes
        to.type = self.type
        to.book = self.book
        Instance.__init__(to, ID_TRANS, self.book)
        self.begin_edit()
        for sp in self.splits:
            to.splits.append(sp.clone_no_kvp())
        to.set_dirty()
        to.commit_edit()
        Event.resume()
        return to

    def clone(self):
        to = self.clone_no_kvp()
        to.begin_edit()
        to.void_reason = self.void_reason
        to.void_former_notes = self.void_former_notes
        to.readonly_reason = self.readonly_reason
        to.void_time = self.void_time
        to.custom_data = self.custom_data.copy() if self.custom_data is not None else None
        for i, sp in enumerate(to.splits):
            sp.custom_data = self.splits[i].custom_data
        to.commit_edit()
        return to

    def unvoid(self):
        s = self.get_void_reason()
        if s is None or len(s) == 0: return
        self.begin_edit()
        v = self.void_former_notes
        self.set_notes(v)
        self.void_former_notes = None
        self.void_time = 0
        self.void_reason = None
        for sp in self.splits:
            sp.unvoid()
        self.clear_readonly()
        self.commit_edit()

    def equal(self, other, check_guids, check_splits, check_balances, assume_ordered):
        if other is None: return False

        if self == other: return True
        same_book = self.get_book() == other.get_book()
        if check_guids:
            if self.guid != other.guid:
                return False
        if self.currency != other.currency:
            return False

        if self.post_date != other.post_date:
            return False

        if (same_book and self.num != other.num) or (not same_book and self.num != other.num):
            return False

        if (same_book and self.description != self.description) or (
                not same_book and self.description != other.description):
            return False

        if check_splits:
            if self.splits and not other.splits or other.splits and not self.splits:
                return False
            if self.splits and other.splits:
                if len(self) != len(other):
                    return False
                for i, sp in enumerate(self.splits):
                    osp = other.get_split(i)
                    if sp != osp:
                        return False
        return True

    def sort_splits(self):
        self.splits.sort(key=lambda s: s.get_value())

    def check_open(self):
        if self.get_edit_level() <= 0:
            raise AssertionError("Transaction not open for editing")

    def rollback_edit(self):
        from .backend import BackEndError
        if not self.get_edit_level():
            return
        if self.get_edit_level() > 1:
            self.decrease_edit_level()
            return
        self.check_open()
        orig = self.orig_trans
        if orig is None:
            return
        self.num, orig.num = orig.num, self.num
        self.description, orig.description = orig.description, self.description
        self.post_date = orig.post_date
        self.enter_date = orig.enter_date
        self.currency, orig.currency = orig.currency, self.currency
        self.custom_data, orig.custom_data = orig.custom_data, self.custom_data
        num_preexist = len(orig.splits)
        slist = self.splits.copy()
        for i, s in enumerate(slist):
            try:
                so = orig.splits[i]
            except IndexError:
                so = None
            if not s.get_dirty():
                continue
            if i < num_preexist and so is not None:
                s.rollback_edit()
                s.action, so.action = so.action, s.action
                s.memo, so.memo = so.memo, s.memo
                s.custom_data, so.custom_data = so.custom_data, s.custom_data
                s.reconcile_state = so.reconcile_state
                s.mark_clean()
                so.free()
            else:
                if self != s.get_transaction():
                    self.splits.remove(s)
                    continue
                s.rollback_edit()
                self.splits.remove(s)

                if s.get_transaction() is None:
                    s.free()
        orig.splits.clear()
        be = self.get_book().get_backend()
        if be.can_rollback():
            errcode = be.get_error()
            while errcode != BackEndError.NO_ERR:
                errcode = be.get_error()
            be.rollback(self)
            errcode = be.get_error()
            if errcode == BackEndError.MOD_DESTROY:
                self.destroy()
                self.do_destroy_cb()
                be.set_error(errcode)
                return
            if errcode != BackEndError.NO_ERR:
                be.set_error(errcode)
        self.orig_trans.free()
        self.orig_trans = None
        self.set_destroying(False)
        self.decrease_edit_level()
        self.generate_event()

    @classmethod
    def register(cls):
        params = [Param(TRANS_NUM, TYPE_STRING, cls.get_num, cls.set_num),
                  Param(TRANS_DESCRIPTION, TYPE_STRING, cls.get_description, cls.set_description),
                  Param(TRANS_DATE_ENTERED, TYPE_DATE, cls.get_enter_date, cls.set_enter_date),
                  Param(TRANS_DATE_POSTED, TYPE_DATE, cls.get_post_date, cls.set_post_date),
                  Param(TRANS_IMBALANCE, TYPE_NUMERIC, cls.get_imbalance_value),
                  Param(TRANS_NOTES, TYPE_STRING, cls.get_notes, cls.set_notes),
                  Param(TRANS_LOCATION, ID_LOCATION, cls.get_location, cls.set_location),
                  Param(TRANS_IS_CLOSING, TYPE_BOOLEAN, cls.is_closing),
                  Param(TRANS_IS_BALANCED, TYPE_BOOLEAN, cls.is_balanced),
                  Param(TRANS_TYPE, TYPE_ENUM, cls.get_type, cls.set_type),
                  Param(TRANS_VOID_STATUS, TYPE_BOOLEAN, cls.get_void_status),
                  Param(TRANS_VOID_REASON, TYPE_STRING, cls.get_void_reason),
                  Param(TRANS_VOID_TIME, TYPE_DATE, cls.get_void_time),
                  Param(TRANS_SPLITLIST, ID_SPLIT, cls.get_splits),
                  Param(PARAM_BOOK, ID_BOOK, cls.get_book),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_TRANS, None, params)

    def free(self):
        for sp in self.splits:
            sp.free()
        self.splits.clear()
        self.description = None
        self.num = None
        self.enter_date = None
        self.post_date = None
        self.currency = None
        if self.orig_trans is not None:
            self.orig_trans.free()
            self.orig_trans = None
        del self

    def __len__(self):
        return len(self.splits)

    def __repr__(self):
        return u"Transaction<[{}] '{}' on {:%Y-%m-%d}{}>".format(
            self.currency.mnemonic if self.currency is not None else "UGX",
            self.description,
            self.post_date if self.post_date else datetime.date.today(),
            " (from sch tx)" if self.scheduled_guid else "")
