import datetime
import re


import pytz

from .collection import Collection
from .numeric import *
from .object import *

utc = pytz.UTC

TYPE_CHOICE = "choice"
query_string_type = TYPE_STRING

query_date_type = TYPE_DATE

query_numeric_type = TYPE_NUMERIC

query_guid_type = TYPE_GUID

query_int32_type = TYPE_INT32

query_int64_type = TYPE_INT64

query_double_type = TYPE_DOUBLE

query_boolean_type = TYPE_BOOLEAN

query_char_type = TYPE_CHAR

query_collect_type = TYPE_COLLECT

query_choice_type = TYPE_CHOICE
query_enum_type = TYPE_ENUM


class QueryString:
    def __init__(self):
        self.pd = None
        self.options = 0
        self.is_regex = False
        self.matchstring = None
        self.compiled = None


class QueryDate:
    def __init__(self):
        self.pd = None
        self.options = 0
        self.date = None

    def __repr__(self):
        return u"QueryDate(date={}, options={}>, pred={}) ".format(self.date, self.options, self.pd)


class QueryNumeric:
    def __init__(self):
        self.pd = None
        self.options = 0
        self.amount = None

    def __repr__(self):
        return u"QueryNumeric(amount={}, options={}>, pred={}) ".format(self.amount, self.options, self.pd)


class QueryGuid:
    def __init__(self):
        self.pd = None
        self.options = 0
        self.guids = None

    def __repr__(self):
        return u"QueryGuid(options={}, guids={}) ".format(self.options, self.guids)


class QueryInt32:
    def __init__(self):
        self.pd = None
        self.val = 0


class QueryInt64:
    def __init__(self):
        self.pd = None
        self.val = 0


class QueryDouble:
    def __init__(self):
        self.pd = None
        self.val = 0


class QueryBoolean:
    def __init__(self):
        self.pd = None
        self.val = 0


class QueryChar:
    def __init__(self):
        self.pd = None
        self.options = 0
        self.char_list = None


class QueryEnum:
    def __init__(self):
        self.pd = None
        self.options = 0
        self.enum_list = None


class QueryKvp:
    def __init__(self):
        self.pd = None
        self.path = None
        self.value = None


class QueryCollection:
    def __init__(self):
        self.pd = None
        self.options = 0
        self.coll = None
        self.guids = None

    def __repr__(self):
        return "<QueryCollection options={} guids={}>".format(self.options, self.guids)


class QueryChoice:
    def __init__(self):
        self.pd = None
        self.options = 0
        self.guid = None
        self.guids = None


class StringMatch(IntEnum):
    NORMAL = 1
    CASEINSENSITIVE = 2


class DateMatch(IntEnum):
    NORMAL = 1
    DAY = 2


class NumericMatch(IntEnum):
    DEBIT = 1
    CREDIT = 2
    ANY = 3


class GuidMatch(IntEnum):
    ANY = 1
    NONE = 2
    NULL = 3
    ALL = 4
    LIST_ANY = 5


class CharMatch(IntEnum):
    ANY = 1
    NONE = 2


class EnumMatch(IntEnum):
    ANY = 1
    NONE = 2


class QueryCompare(IntEnum):
    LT = 1
    LTE = 2
    EQUAL = 3
    GT = 4
    GTE = 5
    NEQ = 6
    CONTAINS = 7
    NCONTAINS = 8


class QueryPredData:
    type_name = None
    how = QueryCompare.LT

    def __init__(self, type_name: str, how: QueryCompare):
        self.type_name = type_name
        self.how = how

    def __repr__(self):
        return u"QueryPredData(type={}, how={}) ".format(self.type_name, self.how)


class QueryCore:
    initialized = False
    predTable = None
    cmpTable = None
    copyTable = None
    freeTable = None
    toStringTable = None
    predEqualTable = None

    @staticmethod
    def string_match_predicate(obj, getter, pdata):

        ret = 0
        s = getter.getfcn(obj)
        if s is None:
            s = ""
        if pdata.is_regex:
            match = re.match(pdata.compiled, s)
            if not match:
                ret = 1
        else:
            if pdata.options == StringMatch.CASEINSENSITIVE:
                if pdata.pd.how == QueryCompare.CONTAINS or pdata.pd.how == QueryCompare.NCONTAINS:

                    if s.lower().find(pdata.matchstring.lower()):
                        ret = 1
                else:
                    if s.lower() == pdata.matchstring.lower():
                        ret = 1

            else:
                if pdata.pd.how == QueryCompare.CONTAINS or pdata.pd.how == QueryCompare.NCONTAINS:
                    if s.find(s, pdata.matchstring):
                        ret = 1
                else:
                    if s == pdata.matchstring:
                        ret = 1

        if pdata.pd.how == QueryCompare.CONTAINS:
            return ret
        elif pdata.pd.how == QueryCompare.NCONTAINS:
            return not ret
        elif pdata.pd.how == QueryCompare.EQUAL:
            return ret
        elif pdata.pd.how == QueryCompare.NEQ:
            return not ret
        else:
            return 0

    @staticmethod
    def string_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None:
            return False
        s1 = getter.getfcn(a)
        s2 = getter.getfcn(b)
        if options == StringMatch.CASEINSENSITIVE:
            return s1.lower() == s2.lower()
        return s1 == s2

    @staticmethod
    def string_number_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None:
            return False
        s1 = getter.getfcn(a)
        s2 = getter.getfcn(b)
        if s1 == s2: return 0
        if s1 is None and s2: return -1
        if s1 and s2 is None: return 1
        sr1 = s1.lower()
        sr2 = s2.lower()
        if sr1 < sr2: return -1
        if sr1 > sr2: return 1
        if options == StringMatch.CASEINSENSITIVE:
            return sr1 == sr2
        return s1 == s2

    @staticmethod
    def string_copy_predicate(pdata):
        return QueryCore.string_predicate(pdata.pd.how, pdata.matchstring, pdata.options, pdata.is_regex)

    @staticmethod
    def string_predicate_equal(p1, p2):
        pd1 = p1
        pd2 = p2
        if pd1.options != pd2.options: return False
        if pd1.is_regex != pd2.is_regex: return False
        return pd1.matchstring == pd2.matchstring

    @classmethod
    def string_predicate(cls, how: QueryCompare, strg: str, options: StringMatch, is_regex: bool = False):
        if strg is None or len(strg) == 0: return
        if how != QueryCompare.CONTAINS or how != QueryCompare.NCONTAINS or \
                how != QueryCompare.EQUAL or how != QueryCompare.NEQ:
            return None
        pdata = QueryString()
        pdata.pd = QueryPredData(query_string_type, how)
        pdata.options = options
        pdata.matchstring = strg
        if is_regex:
            flags = 1
            if options == StringMatch.CASEINSENSITIVE:
                flags |= re.RegexFlag.IGNORECASE
            pdata.compiled = re.compile(strg, flags)
            pdata.matchstring = None
            pdata.is_regex = True
        return pdata

    @staticmethod
    def string_to_string(obj, getter):
        res = getter.getfcn(obj)
        return res

    @staticmethod
    def date_compare(ta, tb, options):
        if options == DateMatch.DAY:
            pass
        if isinstance(ta, datetime.datetime):
            ta = ta.replace(tzinfo=utc)
        if isinstance(tb, datetime.datetime):
            tb = tb.replace(tzinfo=utc)
        return (ta - tb).total_seconds()

    @staticmethod
    def date_match_predicate(obj, getter, pdata):
        objtime = getter.getfcn(obj)
        compare = QueryCore.date_compare(objtime, pdata.date, pdata.options)
        if pdata.pd.how == QueryCompare.LT:
            return compare < 0
        elif pdata.pd.how == QueryCompare.LTE:
            return compare <= 0
        elif pdata.pd.how == QueryCompare.EQUAL:
            return compare == 0
        elif pdata.pd.how == QueryCompare.GT:
            return compare > 0
        elif pdata.pd.how == QueryCompare.GTE:
            return compare >= 0
        elif pdata.pd.how == QueryCompare.NEQ:
            return compare != 0
        else:
            return 0

    @staticmethod
    def date_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None: return -1
        ta = getter.getfcn(a)
        tb = getter.getfcn(b)
        return QueryCore.date_compare(ta, tb, options)

    @staticmethod
    def date_copy_predicate(pdata):
        return QueryCore.date_predicate(pdata.pd.how, pdata.options, pdata.date)

    @staticmethod
    def date_predicate_equal(pd1, pd2):
        if pd1.options != pd2.options: return False
        return pd1.date == pd2.date

    @classmethod
    def date_predicate(cls, how: QueryCompare, options: DateMatch, date: (int, float)):
        if not isinstance(how, QueryCompare):
            raise ValueError("EXPECTED TYPE QueryCompare for parameter 1 got %s" % type(how))
        if not isinstance(options, DateMatch):
            raise ValueError("EXPECTED TYPE DateMatch for parameter 2 got %s" % type(options))
        if date is None: return
        pdata = QueryDate()
        pdata.pd = QueryPredData(query_date_type, how)
        pdata.options = options
        pdata.date = date
        return pdata

    @staticmethod
    def date_to_string(obj, getter):
        # res = getter.getfcn(obj)
        pass

    @staticmethod
    def numeric_match_predicate(obj, getter, pdata):
        obj_val = getter.getfcn(obj)
        if pdata.options == NumericMatch.CREDIT:
            if Numeric.positive_p(obj_val): return 0

        elif pdata.options == NumericMatch.DEBIT:
            if Numeric.negative_p(obj_val): return 0

        if pdata.pd.how == QueryCompare.EQUAL or pdata.pd.how == QueryCompare.NEQ:
            cmp_val = Numeric(1, 10000)
            compare = Numeric.compare(Numeric.abs(Numeric.sub(Numeric.abs(obj_val)
                                                                    , Numeric.abs(pdata.amount)
                                                                    , 100000, NumericRound.HALF_UP))
                                        , cmp_val) < 0
        else:
            compare = Numeric.compare(Numeric.abs(obj_val), pdata.amount)
        if pdata.pd.how == QueryCompare.LT:
            return compare < 0
        elif pdata.pd.how == QueryCompare.LTE:
            return compare <= 0
        elif pdata.pd.how == QueryCompare.EQUAL:
            return compare
        elif pdata.pd.how == QueryCompare.GT:
            return compare > 0
        elif pdata.pd.how == QueryCompare.GTE:
            return compare >= 0
        elif pdata.pd.how == QueryCompare.NEQ:
            return not compare
        else:
            return 0

    @staticmethod
    def numeric_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None: return -1
        va = getter.getfcn(a)
        vb = getter.getfcn(b)
        return Numeric.compare(va, vb)

    @staticmethod
    def numeric_copy_predicate(pdata):
        return QueryCore.numeric_predicate(pdata.pd.how, pdata.options, pdata.date)

    @staticmethod
    def numeric_predicate_equal(pd1, pd2):
        if pd1.options != pd2.options: return False
        return Numeric.equal(pd1.amount, pd2.amount)

    @staticmethod
    def numeric_predicate(how: QueryCompare, options: NumericMatch, value: Numeric):
        if not isinstance(how, QueryCompare):
            raise ValueError("EXPECTED TYPE QueryCompare for parameter 1 got %s" % type(how))
        if not isinstance(options, NumericMatch):
            raise ValueError("EXPECTED TYPE NumericMatch for parameter 2 got %s" % type(options))
        if not isinstance(value, Numeric):
            raise ValueError("EXPECTED TYPE Numeric for parameter 3 got %s" % type(value))
        pdata = QueryNumeric()
        pdata.pd = QueryPredData(query_numeric_type, how)
        pdata.options = options
        pdata.amount = value
        return pdata

    @staticmethod
    def numeric_to_string(obj, getter):
        num = getter.getfcn(obj)
        # return Numeric.to(num)

    @staticmethod
    def debcred_to_string(obj, getter):
        num = getter.getfcn(obj)
        # return gnc_numeric_to_string(num)

    @staticmethod
    def guid_match_predicate(obj, getter, pdata):
        node = None
        guid = None
        if pdata.options == GuidMatch.ALL:
            for g in pdata.guids:
                node = g
                o = None
                for n in obj:
                    o = n
                    if n == g:
                        break
                    o = None
                if o is None:
                    break
                node = None

        elif pdata.options == GuidMatch.LIST_ANY:
            pass
        else:
            guid = getter.getfcn(obj)
            if guid in pdata.guids:
                node = guid

        if pdata.options == GuidMatch.ANY or pdata.options == GuidMatch.LIST_ANY:
            return node is not None
        elif pdata.options == GuidMatch.NONE or pdata.options == GuidMatch.ALL:
            return node is None
        elif pdata.options == GuidMatch.NULL:
            return guid is None
        else:
            return 0

    @staticmethod
    def guid_copy_predicate(pdata):
        return QueryCore.guid_predicate(pdata.options, pdata.guids)

    @staticmethod
    def guid_predicate_equal(pd1, pd2):
        l1 = sorted(pd1.guids)
        l2 = sorted(pd2.guids)
        if pd1.options != pd2.options: return False
        if len(l1) != len(l2): return False
        for i, f1 in enumerate(l1):
            f2 = l2[i]
            if f1 != f2:
                return False
        return True

    @staticmethod
    def guid_predicate(options: GuidMatch, guid_list: list):
        if not isinstance(options, GuidMatch):
            raise ValueError("EXPECTED TYPE GuidMatch for parameter 1 got %s" % type(options))
        if guid_list is None or len(guid_list) == 0:
            if options != GuidMatch.NONE:
                return
        pdata = QueryGuid()
        pdata.pd = QueryPredData(query_guid_type, QueryCompare.EQUAL)
        pdata.options = options
        pdata.guids = guid_list.copy()
        return pdata

    @staticmethod
    def int32_match_predicate(obj, getter, pdata):
        val = getter.getfcn(obj)
        if pdata.pd.how == QueryCompare.LT:
            return val < pdata.val
        elif pdata.pd.how == QueryCompare.LTE:
            return val <= pdata.val
        elif pdata.pd.how == QueryCompare.EQUAL:
            return val == pdata.val
        elif pdata.pd.how == QueryCompare.GT:
            return val > pdata.val
        elif pdata.pd.how == QueryCompare.GTE:
            return val >= pdata.val
        elif pdata.pd.how == QueryCompare.NEQ:
            return val != pdata.val
        else:
            return 0

    @staticmethod
    def int32_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None: return -1
        v1 = getter.getfcn(a)
        v2 = getter.getfcn(b)
        return v1 - v2

    @staticmethod
    def int32_copy_predicate(pdata):
        return QueryCore.int32_predicate(pdata.pd.how, pdata.val)

    @staticmethod
    def int32_predicate_equal(pd1, pd2):
        return pd1.val == pd2.val

    @staticmethod
    def int32_predicate(how: QueryCompare, val: int):
        if not isinstance(how, QueryCompare):
            raise ValueError("EXPECTED TYPE QueryCompare for parameter 1 got %s" % type(how))
        pdata = QueryInt32()
        pdata.pd = QueryPredData(query_int32_type, how)
        pdata.val = val
        return pdata

    @staticmethod
    def int32_to_string(obj, getter):
        num = getter.getfcn(obj)
        return "%d" % num

    @staticmethod
    def int64_match_predicate(obj, getter, pdata):
        val = getter.getfcn(obj)
        if pdata.pd.how == QueryCompare.LT:
            return val < pdata.val
        elif pdata.pd.how == QueryCompare.LTE:
            return val <= pdata.val
        elif pdata.pd.how == QueryCompare.EQUAL:
            return val == pdata.val
        elif pdata.pd.how == QueryCompare.GT:
            return val > pdata.val
        elif pdata.pd.how == QueryCompare.GTE:
            return val >= pdata.val
        elif pdata.pd.how == QueryCompare.NEQ:
            return val != pdata.val
        else:
            return 0

    @staticmethod
    def int64_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None: return -1
        v1 = getter.getfcn(a)
        v2 = getter.getfcn(b)
        if v1 < v2: return -1
        if v1 > v2: return 1
        return 0

    @staticmethod
    def int64_copy_predicate(pdata):
        return QueryCore.int64_predicate(pdata.pd.how, pdata.val)

    @staticmethod
    def int64_predicate_equal(pd1, pd2):
        return pd1.val == pd2.val

    @staticmethod
    def int64_predicate(how, val):
        if not isinstance(how, QueryCompare):
            raise ValueError("EXPECTED TYPE QueryCompare for parameter 1 got %s" % type(how))
        pdata = QueryInt64()
        pdata.pd = QueryPredData(query_int64_type, how)
        pdata.val = val
        return pdata

    @staticmethod
    def int64_to_string(obj, getter):
        num = getter.getfcn(obj)
        return "%li" % num

    @staticmethod
    def double_match_predicate(obj, getter, pdata):
        val = getter.getfcn(obj)
        if pdata.pd.how == QueryCompare.LT:
            return val < pdata.val
        elif pdata.pd.how == QueryCompare.LTE:
            return val <= pdata.val
        elif pdata.pd.how == QueryCompare.EQUAL:
            return val == pdata.val
        elif pdata.pd.how == QueryCompare.GT:
            return val > pdata.val
        elif pdata.pd.how == QueryCompare.GTE:
            return val >= pdata.val
        elif pdata.pd.how == QueryCompare.NEQ:
            return val != pdata.val
        else:
            return 0

    @staticmethod
    def double_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None: return
        v1 = getter.getfcn(a)
        v2 = getter.getfcn(b)
        if v1 < v2: return -1
        if v1 > v2: return 1
        return 0

    @staticmethod
    def double_copy_predicate(pdata):
        return QueryCore.double_predicate(pdata.pd.how, pdata.val)

    @staticmethod
    def double_predicate_equal(pd1, pd2):
        return pd1.val == pd2.val

    @staticmethod
    def double_predicate(how: QueryCompare, val: float):
        if not isinstance(how, QueryCompare):
            raise ValueError("EXPECTED TYPE QueryCompare for parameter 1 got %s" % type(how))
        pdata = QueryDouble()
        pdata.pd = QueryPredData(query_double_type, how)
        pdata.val = val
        return pdata

    @staticmethod
    def double_to_string(obj, getter):
        num = getter.getfcn(obj)
        return "%f" % num

    @staticmethod
    def boolean_match_predicate(obj, getter, pdata):
        val = getter.getfcn(obj)
        if pdata.pd.how == QueryCompare.EQUAL:
            return val == pdata.val
        elif pdata.pd.how == QueryCompare.NEQ:
            return val != pdata.val
        else:
            return 0

    @staticmethod
    def boolean_to_string(obj, getter):
        res = getter.getfcn(obj)
        return "%s" % res

    @staticmethod
    def boolean_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None: return -1
        va = getter.getfcn(a)
        vb = getter.getfcn(b)
        if not va and vb: return -1
        if va and not vb: return 1
        return 0

    @staticmethod
    def boolean_copy_predicate(pdata):
        return QueryCore.boolean_predicate(pdata.pd.how, pdata.val)

    @staticmethod
    def boolean_predicate_equal(pd1, pd2):
        return pd1.val == pd2.val

    @staticmethod
    def boolean_predicate(how, val):
        if how != QueryCompare.EQUAL or how != QueryCompare.NEQ:
            return
        pdata = QueryBoolean()
        pdata.pd = QueryPredData(query_boolean_type, how)
        pdata.val = val
        return pdata

    @staticmethod
    def char_match_predicate(obj, getter, pdata):
        c = getter.getfcn(obj)
        if pdata.options == CharMatch.ANY:
            if c in pdata.char_list: return 1
            return 0
        elif pdata.options == CharMatch.NONE:
            if c not in pdata.char_list: return 1
            return 0
        else:
            return 0

    @staticmethod
    def char_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None: return
        va = getter.getfcn(a)
        vb = getter.getfcn(b)
        return va - vb

    @staticmethod
    def char_copy_predicate(pdata):
        return QueryCore.char_predicate(pdata.options, pdata.char_list)

    @staticmethod
    def char_predicate_equal(pd1, pd2):
        if pd1.options != pd2.options: return False
        return pd1.char_list == pd2.char_list

    @staticmethod
    def char_predicate(options, chars):
        pdata = QueryChar()
        pdata.pd = QueryPredData(query_char_type, QueryCompare.EQUAL)
        pdata.options = options
        pdata.char_list = chars
        return pdata

    @staticmethod
    def char_to_string(obj, getter):
        num = getter.getfcn(obj)
        return "%c" % num

    @staticmethod
    def enum_match_predicate(obj, getter, pdata):
        c = getter.getfcn(obj)
        if pdata.options == EnumMatch.ANY:
            if c in pdata.enum_list: return 1
            return 0
        elif pdata.options == EnumMatch.NONE:
            if c not in pdata.enum_list: return 1
            return 0
        else:
            return 0

    @staticmethod
    def enum_compare_func(a, b, options, getter):
        if a is None or b is None or getter is None or getter.getfcn is None: return
        va = getter.getfcn(a)
        vb = getter.getfcn(b)
        return va - vb

    @staticmethod
    def enum_copy_predicate(pdata):
        return QueryCore.enum_predicate(pdata.options, pdata.enum_list)

    @staticmethod
    def enum_predicate_equal(pd1, pd2):
        if pd1.options != pd2.options: return False
        return pd1.enum_list == pd2.enum_list

    @staticmethod
    def enum_predicate(options: EnumMatch, enums: list):
        pdata = QueryEnum()
        pdata.pd = QueryPredData(query_enum_type, QueryCompare.EQUAL)
        pdata.options = options
        pdata.enum_list = enums
        return pdata

    @staticmethod
    def enum_to_string(obj, getter):
        num = getter.getfcn(obj)
        return "%c" % num.value

    @staticmethod
    def collect_match_predicate(obj, getter, pdata):
        pass

    @staticmethod
    def collect_compare_func(a, b, options, getter):
        c1 = getter.getfcn(a)
        c2 = getter.getfcn(b)
        result = Collection.compare(c1, c2)
        return result

    @staticmethod
    def collect_copy_predicate(pdata):
        QueryCore.collect_predicate(pdata.options, pdata.coll)

    @staticmethod
    def collect_predicate_equal(pd1, pd2):
        result = Collection.compare(pd1.coll, pd2.coll)
        if result == 0:
            return True
        return False

    @staticmethod
    def collect_cb(ent, pdata):
        guid = ent.get_guid(ent)
        pdata.guids.append(guid)

    @staticmethod
    def collect_predicate(options: QueryCompare, coll: Collection):
        pdata = QueryCollection()
        pdata.pd = QueryPredData(query_collect_type, options)
        coll.foreach(QueryCore.collect_cb, pdata)
        if None is pdata.guids:
            return None
        return pdata

    @staticmethod
    def choice_match_predicate(obj, getter, pdata):
        pass

    @staticmethod
    def choice_copy_predicate(pdata):
        return QueryCore.choice_predicate(pdata.options, pdata.guids)

    @staticmethod
    def choice_predicate_equal(pd1, pd2):
        pass

    @staticmethod
    def choice_predicate(options, guid_list):
        if guid_list is None or not isinstance(guid_list, list): return
        pdata = QueryChoice()
        pdata.pd = QueryPredData(query_choice_type, QueryCompare.EQUAL)
        pdata.options = options
        pdata.guids = guid_list.copy()
        return pdata

    @classmethod
    def register_core_object(cls, core_name, pred, comp, copy, toString, pred_equal):
        if core_name is None or len(core_name) == 0:
            return
        if pred:
            cls.predTable[core_name] = pred
        if comp:
            cls.cmpTable[core_name] = comp
        if copy:
            cls.copyTable[core_name] = copy
        if toString:
            cls.toStringTable[core_name] = toString
        if pred_equal:
            cls.predEqualTable[core_name] = pred_equal

    @classmethod
    def init_tables(cls):
        for a in [(TYPE_STRING, cls.string_match_predicate, cls.string_compare_func,
                   cls.string_copy_predicate, cls.string_to_string, cls.string_predicate_equal),
                  (TYPE_DATE, cls.date_match_predicate, cls.date_compare_func,
                   cls.date_copy_predicate, cls.date_to_string, cls.date_predicate_equal),
                  (
                          TYPE_DEBCRED, cls.numeric_match_predicate, cls.numeric_compare_func,
                          cls.numeric_copy_predicate, cls.debcred_to_string,
                          cls.numeric_predicate_equal
                  ),
                  (
                          TYPE_NUMERIC, cls.numeric_match_predicate, cls.numeric_compare_func,
                          cls.numeric_copy_predicate, cls.numeric_to_string,
                          cls.numeric_predicate_equal
                  ),
                  (
                          TYPE_GUID, cls.guid_match_predicate, None,
                          cls.guid_copy_predicate, None,
                          cls.guid_predicate_equal
                  ),
                  (
                          TYPE_INT32, cls.int32_match_predicate, cls.int32_compare_func,
                          cls.int32_copy_predicate, cls.int32_to_string,
                          cls.int32_predicate_equal
                  ),
                  (
                          TYPE_INT64, cls.int64_match_predicate, cls.int64_compare_func,
                          cls.int64_copy_predicate, cls.int64_to_string,
                          cls.int64_predicate_equal
                  ),
                  (
                          TYPE_DOUBLE, cls.double_match_predicate, cls.double_compare_func,
                          cls.double_copy_predicate, cls.double_to_string,
                          cls.double_predicate_equal
                  ),
                  (
                          TYPE_BOOLEAN, cls.boolean_match_predicate, cls.boolean_compare_func,
                          cls.boolean_copy_predicate, cls.boolean_to_string,
                          cls.boolean_predicate_equal
                  ),
                  (
                          TYPE_CHAR, cls.char_match_predicate, cls.char_compare_func,
                          cls.char_copy_predicate, cls.char_to_string,
                          cls.char_predicate_equal
                  ),
                  (
                          TYPE_ENUM, cls.enum_match_predicate, cls.enum_compare_func,
                          cls.enum_copy_predicate, cls.enum_to_string,
                          cls.enum_predicate_equal
                  ),
                  (
                          TYPE_COLLECT, cls.collect_match_predicate, cls.collect_compare_func,
                          cls.collect_copy_predicate, None,
                          cls.collect_predicate_equal
                  ),
                  (
                          TYPE_CHOICE, cls.choice_match_predicate, None,
                          cls.choice_copy_predicate, None, cls.choice_predicate_equal
                  )]:
            cls.register_core_object(*a)

    @classmethod
    def copy_predicate(cls, _type):
        if _type is None or cls.copyTable is None: return
        return cls.copyTable.get(_type)

    @classmethod
    def init(cls):
        if cls.initialized: return
        cls.initialized = True
        cls.predTable = {}
        cls.cmpTable = {}
        cls.copyTable = {}
        cls.freeTable = {}
        cls.toStringTable = {}
        cls.predEqualTable = {}
        cls.init_tables()

    @classmethod
    def shutdown(cls):
        if not cls.initialized: return
        cls.initialized = False
        cls.predTable = None
        cls.cmpTable = None
        cls.copyTable = None
        cls.freeTable = None
        cls.toStringTable = None
        cls.predEqualTable = None

    @classmethod
    def get_predicate(cls, _type):
        if _type is None or cls.predTable is None: return
        return cls.predTable.get(_type)

    @classmethod
    def get_compare(cls, _type):
        if _type is None or cls.cmpTable is None: return
        return cls.cmpTable.get(_type)

    @classmethod
    def predicate_copy(cls, pdata):
        if pdata is None: return
        if pdata.pd.type_name is None: return
        return cls.copy_predicate(pdata.pd.type_name)(pdata)

    @classmethod
    def predicate_equal(cls, pdata, pdata1):
        pass
