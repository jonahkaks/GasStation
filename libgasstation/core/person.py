from sqlalchemy import VARCHAR, ForeignKey, Enum
from sqlalchemy.orm import relationship

from ._declbase import *
from .account import Account
from .address import AddressType
from .contact import Contact
from .engine import *
from .event import *
from .helpers import gs_set_num_action
from .payment import Payment
from .query_private import *
from .split import Split
from .transaction import TransactionType, Transaction


class SplitFlags(IntEnum):
    is_equal = 8
    is_more = 4
    is_less = 2
    is_pay_split = 1


class Gender(IntEnum):
    MALE = 0
    FEMALE = 1


class Person(DeclarativeBaseGuid):
    __abstract__ = True
    __table_args__ = {'implicit_returning': False}
    _id = Column('id', VARCHAR(length=50), nullable=False, unique=True)
    active = Column('active', BOOLEAN(), nullable=False, default=True)
    notes = Column('notes', VARCHAR(length=2048))
    gender: Gender = Column('gender', Enum(Gender), nullable=False, default=Gender.MALE)

    @declared_attr
    def contact_guid(cls):
        return Column("contact_guid", UUIDType(binary=False), ForeignKey("contact.guid", ondelete='CASCADE'),
                      nullable=False)

    @declared_attr
    def contact(cls):
        return relationship("Contact", lazy="joined", primaryjoin=Contact.guid == cls.contact_guid,uselist=False)

    @declared_attr
    def opening_transaction_guid(cls):
        return Column("opening_transaction_guid", UUIDType(binary=False), ForeignKey("transaction.guid",
                                                                                     ondelete='CASCADE'))

    @declared_attr
    def opening_transaction(cls):
        return relationship("Transaction", lazy="joined",uselist=False)

    @declared_attr
    def currency_guid(cls):
        return Column('currency_guid', UUIDType(binary=False), ForeignKey('commodity.guid'), nullable=False)

    @declared_attr
    def currency(cls):
        return relationship('Commodity', uselist=False)

    def get_cached_balance(self):
        return self.balance

    def set_cached_balance(self, new_bal):
        self.balance = new_bal

    def get_active(self):
        return bool(self.active)

    def set_active(self, active: (int, bool)):
        if active != self.active:
            self.begin_edit()
            self.active = active
            self.mark()
            self.commit_edit()

    def get_currency(self):
        return self.currency

    def set_currency(self, currency):
        if currency != self.currency:
            self.begin_edit()
            self.currency = currency
            self.mark()
            self.commit_edit()

    def get_notes(self):
        return self.notes

    def set_notes(self, notes):
        if notes != self.notes and len(notes):
            self.begin_edit()
            self.notes = notes
            self.mark()
            self.commit_edit()

    def get_gender(self):
        return self.gender

    def set_gender(self, gender):
        if gender != self.gender:
            self.begin_edit()
            self.gender = gender
            self.mark()
            self.commit_edit()

    def get_name(self):
        if self.contact is None:
            return ""
        return self.contact.get_display_name()

    def get_address(self):
        if self.contact is None:
            return ""
        addresses = list(filter(lambda a: a.type == AddressType.WORK, self.contact.addresses))
        if len(addresses):
            a = addresses[0]
            return "{}\n{}\n{}\nP.O Box:{}\n{}".format(a.line1, a.city, a.state,
                                                       a.postal_code, a.country)

    def get_name_and_address(self):
        if self.contact is None:
            return ""
        name = self.contact.get_display_name()
        if len(name) > 0:
            addresses = list(filter(lambda a: a.type == AddressType.WORK, self.contact.addresses))
            if len(addresses):
                a = addresses[0]
                return "{}\n\n{}\n{}\n{}\nP.O Box:{}\n{}".format(name, a.line1, a.city, a.state,
                                                                 a.postal_code, a.country)

        return name

    def get_contact(self) -> Contact:
        return self.contact

    def set_contact(self, add: Contact):
        if add != self.contact and isinstance(add, Contact):
            self.begin_edit()
            self.contact = add
            self.mark()
            self.commit_edit()

    def get_term(self):
        return self.term

    def set_term(self, term):
        if term != self.term:
            self.begin_edit()
            if self.term is not None:
                self.term.decrement_ref()
            self.term = term
            if self.term is not None:
                self.term.increment_ref()
            self.mark()
            self.commit_edit()

    def get_id(self):
        return self._id

    def set_id(self, _id):
        if _id != self._id:
            self.begin_edit()
            self._id = _id
            self.mark()
            self.commit_edit()

    def mark(self):
        self.set_dirty()
        Event.gen(self, EVENT_MODIFY)

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        print("CLEAN UP AFTER DESTROY PERSON")
        Event.gen(self, EVENT_DESTROY)
        self.contact.dispose()
        self.dispose()

    def get_opening_balance_transaction(self):
        return self.opening_transaction

    def get_opening_balance(self, account_type):
        if self.opening_transaction is not None:
            return self.opening_transaction.get_account_type_amount(account_type)
        return Decimal(0)

    def get_balance_in_currency(self, report_currency):
        raise NotImplementedError

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def on_error(self, error):
        Engine.signal_commit_error(error)

    def __lt__(self, other):
        if other is None:
            return True
        try:
            return self.get_display_name() < other.get_display_name()
        except TypeError:
            return False

    def create_payment_transaction(self, payable_receivable_account, deposit_account, amount, exchange_rate, date, memo,
                                   num):
        if payable_receivable_account is None or deposit_account is None:
            return
        book = payable_receivable_account.get_book()
        name = self.contact.get_display_name()
        commodity = self.get_currency()
        txn = Transaction(book)
        txn.begin_edit()

        txn.set_description(name if name is not None else "")
        txn.set_currency(commodity)

        split = Split(book)
        split.set_memo(memo)
        gs_set_num_action(None, split, num, "Payment")
        deposit_account.begin_edit()
        split.set_account(deposit_account)
        deposit_account.commit_edit()
        txn.append_split(split)

        if deposit_account.get_commodity() != commodity:
            split.set_base_value(amount, commodity)
        else:
            xfer_amount = amount * exchange_rate
            split.set_amount(xfer_amount)
            split.set_value(amount)

        split = Split(book)
        split.set_memo(memo)
        gs_set_num_action(None, split, num, "Payment")
        payable_receivable_account.begin_edit()
        split.set_account(payable_receivable_account)
        payable_receivable_account.commit_edit()
        txn.append_split(split)
        split.set_base_value(-amount, commodity)

        gs_set_num_action(txn, None, num, "Payment")
        txn.set_type(TransactionType.PAYMENT)
        txn.set_readonly("Cannot edit a payment transaction")
        txn.set_enter_date(datetime.datetime.now())
        txn.set_post_date(date)
        txn.commit_edit()
        return txn

    #
    # @staticmethod
    # def find_offsetting_split(lot: Lot, target_value: Decimal):
    #     best_val = Decimal(0)
    #     best_split = None
    #     best_flags = 0
    #     if lot is None:
    #         return
    #
    #     for split in lot.get_split_list():
    #         new_flags = 0
    #         if split is None:
    #             continue
    #         txn = split.get_transaction()
    #         if txn is None:
    #             continue
    #         split_value = split.get_value()
    #         if target_value > 0 == split_value > 0:
    #             continue
    #         val_cmp = abs(split_value) - abs(target_value)
    #         if val_cmp == 0:
    #             new_flags += SplitFlags.is_equal
    #         elif val_cmp > 0:
    #             new_flags += SplitFlags.is_more
    #         else:
    #             new_flags += SplitFlags.is_less
    #
    #         if txn.get_type() != TransactionType.LINK:
    #             new_flags += SplitFlags.is_pay_split
    #         if ((new_flags >= best_flags) and
    #                 abs(split_value) > abs(best_val)):
    #             best_split = split
    #             best_flags = new_flags
    #             best_val = split_value
    #     return best_split
    #
    # @staticmethod
    # def reduce_split_to(split: Split, target_value: Decimal):
    #     split_val = split.get_value()
    #     if split_val > 0 != target_value > 0:
    #         return False
    #     if split_val == target_value:
    #         return False
    #     rem_val = split_val - target_value
    #     rem_split = Split(split.get_book())
    #     split.copy_onto(rem_split)
    #     rem_split.set_amount(rem_val)
    #     rem_split.set_value(rem_val)
    #     txn = split.get_transaction()
    #     txn.begin_edit()
    #
    #     split.set_amount(target_value)
    #     split.set_value(target_value)
    #     rem_split.set_transaction(txn)
    #     txn.commit_edit()
    #     lot = split.get_lot()
    #     lot.add_split(rem_split)
    #
    #     return True
    #
    # @staticmethod
    # def set_lot_link_memo(ll_txn: Transaction):
    #     titles = []
    #     splits = []
    #     memo_prefix = "Offset between documents: "
    #     if ll_txn is None:
    #         return
    #     if ll_txn.get_type() != TransactionType.LINK:
    #         return
    #     for split in ll_txn.get_splits():
    #         if split is None:
    #             continue
    #         lot = split.get_lot()
    #         if lot is None:
    #             continue
    #
    #         invoice = lot.get_invoice() is None or lot.get_bill() is None
    #         if invoice is None:
    #             continue
    #
    #         title = "%s %s" % (invoice.__class__.__name__, invoice.get_id())
    #         titles.insert(0, title)
    #         splits.insert(0, split)
    #
    #     if len(titles) == 0:
    #         return
    #     titles.sort()
    #
    #     new_memo = memo_prefix + " _ ".join(titles)
    #     for split in splits:
    #         if split.get_memo() != new_memo:
    #             split.set_memo(new_memo)
    #
    # @staticmethod
    # def get_ll_transaction_from_lot(lot: Lot):
    #     if lot.get_invoice() is None or lot.get_bill() is None:
    #         return
    #     for ls in lot.get_split_list():
    #         ll_txn: Transaction = ls.get_transaction()
    #         if ll_txn.get_type() != TransactionType.LINK:
    #             continue
    #
    #         for ts in ll_txn.splits:
    #             ts_lot = ts.get_lot()
    #             if ts_lot is None:
    #                 continue
    #
    #             if ts_lot == lot:
    #                 continue
    #             return ll_txn
    #     return
    #
    # def create_lot_link(self, from_lot: Lot, to_lot: Lot):
    #     action = "Lot Link"
    #     acct = from_lot.get_account()
    #     name = self.contact.get_display_name()
    #
    #     if (from_lot.get_invoice() is None or from_lot.get_bill() is None) or (
    #             to_lot.get_invoice() is None or to_lot.get_bill() is None):
    #         return
    #
    #     from_time = from_lot.get_latest_split().get_transaction().get_post_date()
    #     to_time = to_lot.get_latest_split().get_transaction().get_post_date()
    #     if from_time >= to_time:
    #         time_posted = from_time
    #     else:
    #         time_posted = to_time
    #
    #     from_lot_bal = from_lot.get_balance()
    #     to_lot_bal = to_lot.get_balance()
    #     if abs(from_lot_bal) > abs(to_lot_bal):
    #         from_lot_bal = -to_lot_bal
    #     else:
    #         to_lot_bal = -from_lot_bal
    #
    #     acct.begin_edit()
    #     ll_txn: Transaction = self.get_ll_transaction_from_lot(from_lot)
    #
    #     if ll_txn is None:
    #         ll_txn = self.get_ll_transaction_from_lot(to_lot)
    #
    #     if ll_txn is None:
    #         ll_txn = Transaction(from_lot.get_book())
    #         ll_txn.begin_edit()
    #         # TODO Add location here
    #         ll_txn.set_description(name if name is not None else "(Unknown)")
    #         ll_txn.set_currency(acct.get_commodity())
    #         ll_txn.set_enter_date(datetime.datetime.now())
    #         ll_txn.set_post_date(time_posted)
    #         ll_txn.set_type(TransactionType.LINK)
    #     else:
    #         date_posted = ll_txn.get_post_date()
    #         ll_txn.begin_edit()
    #         if time_posted > date_posted:
    #             ll_txn.set_post_date(time_posted)
    #     split = Split(from_lot.get_book())
    #     gs_set_num_action(None, split, None, action)
    #     split.set_account(acct)
    #     ll_txn.append_split(split)
    #     split.set_base_value(-from_lot_bal, acct.get_commodity())
    #     from_lot.add_split(split)
    #     split = Split(to_lot.get_book())
    #     gs_set_num_action(None, split, None, action)
    #     split.set_account(acct)
    #     ll_txn.append_split(split)
    #     split.set_base_value(-to_lot_bal, acct.get_commodity())
    #     to_lot.add_split(split)
    #
    #     ll_txn.commit_edit()
    #     # xaccScrubMergeLotSubSplits (to_lot, False)
    #     # xaccScrubMergeLotSubSplits (from_lot, False)
    #     self.set_lot_link_memo(ll_txn)
    #     acct.commit_edit()
    #
    # def offset_lots(self, from_lot: Lot, to_lot: Lot):
    #     if from_lot.get_invoice() is not None or from_lot.get_bill() is not None:
    #         return
    #     target_offset = to_lot.get_balance()
    #     if target_offset.is_zero():
    #         return
    #
    #     split = self.find_offsetting_split(from_lot, target_offset)
    #     if split is None:
    #         return
    #     if abs(split.get_value()) > abs(target_offset):
    #         self.reduce_split_to(split, -target_offset)
    #     to_lot.add_split(split)
    #
    # def auto_apply_payment_with_lots(self, lots: List[Lot]):
    #     if len(lots) == 0:
    #         return
    #
    #     for i, left_lot in enumerate(lots):
    #         left_modified = False
    #         if left_lot is None:
    #             continue
    #         if left_lot.count_splits() == 0:
    #             left_lot.destroy()
    #             continue
    #
    #         if left_lot.is_closed():
    #             continue
    #
    #         acct = left_lot.get_account()
    #         acct.begin_edit()
    #
    #         left_lot_bal = left_lot.get_balance()
    #         left_lot_has_doc = left_lot.get_invoice() is not None or left_lot.get_bill() is not None
    #
    #         for right_lot in lots[i:]:
    #
    #             if right_lot.count_splits() == 0:
    #                 right_lot.destroy()
    #                 continue
    #
    #             if right_lot.is_closed():
    #                 continue
    #
    #             if acct != right_lot.get_account():
    #                 continue
    #             right_lot_bal = right_lot.get_balance()
    #             if left_lot_bal > 0 == right_lot_bal > 0:
    #                 continue
    #
    #             right_lot_has_doc = right_lot.get_invoice() is not None or right_lot.get_bill() is not None
    #             if left_lot_has_doc and right_lot_has_doc:
    #                 self.create_lot_link(left_lot, right_lot)
    #             elif not left_lot_has_doc and not right_lot_has_doc:
    #                 if abs(left_lot_bal) >= abs(right_lot_bal):
    #                     self.offset_lots(left_lot, right_lot)
    #                 else:
    #                     self.offset_lots(right_lot, left_lot)
    #             else:
    #                 doc_lot = left_lot if left_lot_has_doc else right_lot
    #                 pay_lot = right_lot if left_lot_has_doc else left_lot
    #                 self.offset_lots(pay_lot, doc_lot)
    #
    #             this_invoice = right_lot.get_invoice() if right_lot.get_invoice() is not None else right_lot.get_bill()
    #             if this_invoice is not None:
    #                 Event.gen(this_invoice, EVENT_MODIFY)
    #             left_modified = True
    #         if left_modified:
    #             this_invoice = left_lot.get_invoice() if left_lot.get_invoice() is not None else left_lot.get_bill()
    #             if this_invoice is not None:
    #                 Event.gen(this_invoice, EVENT_MODIFY)
    #         acct.commit_edit()
    #

    def get_upapplied_payments(self):
        raise NotImplementedError

    def auto_apply_payment(self, documents, payable_receivable_account: Account, payment_method,
                           amount: Decimal, exch: Decimal, date: datetime.date, memo: str, num: str):
        preset_txn = None
        if payable_receivable_account is None or (payment_method is None and not amount.is_zero()):
            return
        book = payable_receivable_account.get_book()
        be = book.get_backend()
        with be.add_lock():
            if not amount.is_zero():
                preset_txn = self.create_payment_transaction(payable_receivable_account, payment_method.get_account(),
                                                             amount, exch, date, memo, num)

            amount = abs(amount)
            payment = Payment(self.book)
            payment.begin_edit()
            payment.set_date(datetime.datetime.combine(date, datetime.datetime.now().time()))
            payment.set_amount(amount)
            payment.set_method(payment_method)
            payment.set_posted_transaction(preset_txn)
            payment.commit_edit()

            for document in sorted(documents):
                document_amount = document.get_balance()
                if amount >= document_amount:
                    amount -= document_amount
                elif amount < document_amount:
                    # Amount to distribute to the documents is less so we assign the remaining amount to the last invoice
                    document_amount = amount
                    amount = Decimal(0)
                else:
                    # Amount to distribute to the documents is over so we quit
                    break
                document.begin_edit()
                document.add_payment(payment, document_amount)
                document.commit_edit()
            if not amount.is_zero():
                self.add_payment(payment)
        return preset_txn
