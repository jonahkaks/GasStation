from sqlalchemy import DECIMAL
from sqlalchemy.orm import reconstructor

from .account import AccountType
from .bill import Bill
from .payment import PaymentMethod
from .person import *
from .price_db import PriceDB
from .scrub import Scrub, EquityType
from .tax import Tax
from .term import Term

SUPPLIER_ID = "id"
SUPPLIER_NAME = "name"
SUPPLIER_NOTES = "supplier_notes"
SUPPLIER_TERMS = "supplier_terms"
SUPPLIER_TAX_INC = "supplier_tax_included"
SUPPLIER_ACTIVE = "supplier_is_active"
SUPPLIER_TAX_OVERRIDE = "override_tax"
SUPPLIER_TAX_TABLE = "supplier_tax"


class SupplierPayment(DeclarativeBase):
    __tablename__ = "supplier_payment"
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True,
                             nullable=False, default=lambda: uuid.uuid4())
    supplier_guid = Column('supplier_guid', UUIDType(binary=False), ForeignKey('supplier.guid'), nullable=False)
    bill_guid = Column('bill_guid', UUIDType(binary=False), ForeignKey('bill.guid'))
    payment_guid = Column('payment_guid', UUIDType(binary=False), ForeignKey('payment.guid'), primary_key=True,
                          nullable=False)
    amount: Decimal = Column("amount", DECIMAL, nullable=False, default=Decimal(0))
    supplier = relationship("Supplier", back_populates="payments")
    bill = relationship("Bill", back_populates="payments")
    payment = relationship("Payment")

    def __init__(self, payment):
        self.payment = payment


class Supplier(Person):
    __tablename__ = 'supplier'

    __table_args__ = {}
    payment_method_guid = Column('payment_method_guid', UUIDType(binary=False), ForeignKey('payment_method.guid'))
    tax_guid = Column('tax_guid', UUIDType(binary=False), ForeignKey('tax.guid'))
    term_guid = Column('term_guid', UUIDType(binary=False), ForeignKey('term.guid'))
    tin = Column("tin_no", VARCHAR(25))
    billing_rate = Column('billing_rate', DECIMAL(10, 2), nullable=False)
    tax_included = Column("tax_included", BOOLEAN, default=False)
    term = relationship('Term')
    tax = relationship('Tax')
    products = relationship("Product", back_populates="default_supplier")
    payment_method = relationship("PaymentMethod")
    payments: List[SupplierPayment] = relationship("SupplierPayment", back_populates="supplier")
    bills: List[Bill] = relationship('Bill', back_populates='supplier', cascade='all, delete-orphan')
    e_type = ID_SUPPLIER
    event_id = 0

    def __init__(self, book):
        if book is not None:
            super().__init__(ID_SUPPLIER, book)
            self.business_id = None
            self.balance = None
            self.billing_rate = Decimal(0)
            self._id = book.increment_and_format_counter("Supplier")
            Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        self.balance = None
        self.infant = False
        Instance.__init__(self, ID_SUPPLIER, book)
        Event.gen(self, EVENT_CREATE)

    def get_payable_account(self):
        book = self.book
        return Scrub.get_or_make_account_with_fullname_parent(book.get_root_account(),
                                                              book.default_currency,
                                                              "Accounts Payable",
                                                              AccountType.PAYABLE,
                                                              "Liabilities", "Current Liabilities")

    def refers_to(self, other):
        if isinstance(other, Account):
            return other == self.get_expense_account() or other == self.get_payable_account()
        elif isinstance(other, Term):
            return self.term == other
        elif isinstance(other, Tax):
            return self.tax == other
        elif isinstance(other, PaymentMethod):
            return self.payment_method == other
        return False

    def get_business_id(self):
        return self.business_id

    @classmethod
    def lookup_display_name(cls, book, display_name):
        col = book.get_collection(ID_SUPPLIER)
        for cat in col.values():
            if cat.get_contact().get_display_name().lower().replace(" ", "") == display_name.lower().replace(" ", ""):
                return cat

    @classmethod
    def lookup_name(cls, book, display_name):
        col = book.get_collection(ID_SUPPLIER)
        for cat in col.values():
            if cat.get_contact().get_full_name().lower().replace(" ", "") == display_name.lower().replace(" ", ""):
                return cat

    def get_tax_included(self):
        return self.tax_included

    def set_tax_included(self, tax_included):
        if tax_included != self.tax_included:
            self.begin_edit()
            self.tax_included = tax_included
            self.mark()
            self.commit_edit()

    def get_tax(self):
        return self.tax

    def set_tax(self, tax):
        if tax != self.tax:
            self.begin_edit()
            self.tax = tax
            self.mark()
            self.commit_edit()

    def get_opening_balance(self, *args):
        return super().get_opening_balance(AccountType.PAYABLE)

    def set_opening_balance(self, balance: Decimal, opening_date: datetime.date):
        txn: Transaction = self.opening_transaction
        self.begin_edit()
        if txn is not None:
            txn.clear_readonly()
            txn.destroy()
        if not balance.is_zero():
            self.opening_transaction = Scrub.create_opening_balance(self.get_payable_account(), balance, opening_date,
                                                                    self.book,
                                                                    "Supplier--{}".format(self.contact.get_full_name()),
                                                                    equity_type=EquityType.RETAINED_EARNINGS, ttype=TransactionType.INVOICE)
        self.set_dirty()
        self.commit_edit()

    def add_payment(self, payment: Payment, bill=None, bill_amount=Decimal(0)):
        if payment is not None:
            self.begin_edit()
            sup_payment = SupplierPayment(payment=payment)
            if bill is not None:
                sup_payment.bill = bill
                sup_payment.amount = bill_amount
            self.payments.append(sup_payment)
            self.set_dirty()
            self.commit_edit()

    def get_payment_method(self):
        return self.payment_method

    def set_payment_method(self, payment_method):
        if payment_method != self.payment_method:
            self.begin_edit()
            self.payment_method = payment_method
            self.mark()
            self.commit_edit()

    def get_upapplied_payments(self):
        for payment in self.payments:
            if payment.bill is None:
                yield payment.payment

    def get_balance_in_currency(self, report_currency):
        book = self.get_book()
        person_currency = self.get_currency()
        cached_balance = self.get_cached_balance()
        if cached_balance is not None:
            balance = cached_balance
        else:
            balance = self.get_opening_balance()
            bill_balance = sum([bill.get_total() for bill in self.bills])
            payments = sum(map(lambda a: a.amount, set(cpayment.payment for cpayment in self.payments)))
            balance += Decimal(bill_balance - payments)
            self.set_cached_balance(balance)
        pdb = PriceDB.get_db(book)
        if report_currency is not None:
            balance = pdb.convert_balance_latest_price(balance, person_currency, report_currency)
        return balance

    def referred(self, other):
        if not isinstance(other, (Term, Account, Bill)):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)

    def get_tin(self):
        return self.tin

    def set_tin(self, tin):
        if tin != self.tin and len(tin):
            self.begin_edit()
            self.tin = tin
            self.mark()
            self.commit_edit()

    def get_billing_rate(self):
        return self.billing_rate

    def set_billing_rate(self, billing_rate):
        if billing_rate != self.billing_rate:
            self.begin_edit()
            self.billing_rate = billing_rate
            self.mark()
            self.commit_edit()

    def get_expense_account(self):
        book = self.book
        return Scrub.get_or_make_account_with_fullname_parent(book.get_root_account(),
                                                              book.default_currency,
                                                              "Purchases",
                                                              AccountType.COST_OF_SALES,
                                                              "Expenses")

    @classmethod
    def lookup(cls, book, guid):
        coll = book.get_collection(ID_SUPPLIER)
        return coll.get(guid)

    @classmethod
    def register(cls):
        params = [
            Param(SUPPLIER_ID, TYPE_STRING, cls.get_id),
            Param(SUPPLIER_NOTES, TYPE_STRING, cls.get_notes, cls.set_notes),
            Param(SUPPLIER_TERMS, ID_TERM, cls.get_term, cls.set_term),
            Param(PARAM_ACTIVE, TYPE_BOOLEAN, cls.get_active, cls.set_active),
            Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
            Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None)]
        ObjectClass.register(cls, ID_SUPPLIER, cls.__eq__, params)

    def __repr__(self):
        return u"Supplier<{}>".format(self.contact.get_display_name() if self.contact is not None else "")
