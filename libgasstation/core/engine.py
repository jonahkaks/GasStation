INVOICE_ID = "gsInvoice"
INVOICE_GUID = "invoice-guid"
PERSON_ID = "gsPerson"
PERSON_TYPE = "person-type"
PERSON_GUID = "person-guid"
SX_ID = "sched-xaction"


class Engine:
    g_error_cb = None
    g_error_cb_data = None
    engine_init_hooks = []
    engine_is_initialized = 0

    @staticmethod
    def init_part1():
        from .helpers import qof_init
        qof_init()
        from .cash_objects import CashObjects
        CashObjects.register()

    @classmethod
    def init_part2(cls, *args, **kwargs):
        for hook in cls.engine_init_hooks:
            hook(*args, **kwargs)

    @classmethod
    def add_init_hook(cls, hook):
        if hook is not None:
            cls.engine_init_hooks.append(hook)

    @classmethod
    def init(cls, *args, **kwargs):
        if 1 == cls.engine_is_initialized:
            return
        cls.init_part1()
        cls.init_part2(*args, **kwargs)
        cls.engine_is_initialized = 1

    @classmethod
    def add_commit_error_callback(cls, cb, *data):
        cls.g_error_cb = cb
        cls.g_error_cb_data = data

    @classmethod
    def signal_commit_error(cls, errcode):
        if cls.g_error_cb:
            cls.g_error_cb(errcode)

    @classmethod
    def is_initialized(cls):
        return bool(cls.engine_is_initialized)

    @classmethod
    def shutdown(cls):
        from .helpers import qof_close
        cls.engine_is_initialized = 0
        qof_close()
