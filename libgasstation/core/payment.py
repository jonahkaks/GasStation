from sqlalchemy import VARCHAR, ForeignKey, DECIMAL, DateTime
from sqlalchemy.orm import reconstructor, relationship

from libgasstation.core.query_private import *
from ._declbase import *
from .account import Account
from .event import *

PAYMENT_METHOD_NAME = "payment-method"


class PaymentMethod(DeclarativeBaseGuid):

    __tablename__ = 'payment_method'
    __table_args__ = {}
    account_guid: str = Column("account_guid", UUIDType(binary=False), ForeignKey("account.guid"), nullable=False)
    name = Column('name', VARCHAR(length=50), nullable=False, unique=True)

    is_credit_card = Column('is_credit_card', BOOLEAN(), default=False)
    account = relationship("Account")

    def __init__(self, book):
        super().__init__(ID_PAYMENT_METHOD, book)
        self.is_credit_card = False
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_PAYMENT_METHOD, book)
        Event.gen(self, EVENT_CREATE)

    @classmethod
    def lookup_name(cls, book, name):
        col = book.get_collection(ID_PAYMENT_METHOD)
        for cat in col.values():
            if cat.get_name() == name:
                return cat

    def set_name(self, name):
        if name is not None and name != self.name:
            self.begin_edit()
            self.name = name
            self.set_dirty()
            self.commit_edit()

    def get_name(self):
        return self.name

    def set_account(self, account):
        if account is not None and account != self.account:
            self.begin_edit()
            self.account = account
            self.set_dirty()
            self.commit_edit()

    def get_account(self):
        return self.account

    def set_is_credit_card(self, is_credit_card):
        if is_credit_card is not None and is_credit_card != self.is_credit_card:
            self.begin_edit()
            self.is_credit_card = is_credit_card
            self.set_dirty()
            self.commit_edit()

    @classmethod
    def get_methods(cls, book):
        col = book.get_collection(ID_PAYMENT_METHOD)
        return list(col.values())

    def get_is_credit_card(self):
        return self.is_credit_card

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def get_description(self):
        return self.description

    def refers_to(self, other):
        if isinstance(other, Account):
            return self.account == other
        return False

    def referred(self, other):
        if not isinstance(other, Account):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)

    @classmethod
    def register(cls):
        params = [Param(PAYMENT_METHOD_NAME, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_PAYMENT_METHOD, None, params)

    def __repr__(self):
        return "<PaymentMethod %s>" % self.name


class Payment(DeclarativeBaseGuid):
    __tablename__ = "payment"
    payment_method_guid: str = Column("payment_method_guid", UUIDType(binary=False), ForeignKey("payment_method.guid"),
                                      nullable=False)
    payment_txn_guid: str = Column("payment_txn_guid", UUIDType(binary=False), ForeignKey("transaction.guid"),unique=True,
                                      nullable=False)
    date: datetime.datetime = Column("date", DateTime(timezone=False), nullable=False,
                                     default=datetime.datetime.utcnow)
    amount: Decimal = Column("amount", DECIMAL, nullable=False, default=Decimal(0))
    method: PaymentMethod = relationship("PaymentMethod")
    posted_transaction = relationship("Transaction", cascade="delete")

    def __init__(self, book):
        super().__init__("Payment", book)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__("Payment", book)
        Event.gen(self, EVENT_CREATE)

    def set_method(self, method: PaymentMethod):
        if method is not None and method != self.method:
            self.begin_edit()
            self.method = method
            self.set_dirty()
            self.commit_edit()

    def get_method(self):
        return self.method

    def set_posted_transaction(self, posted_transaction):
        if posted_transaction is not None and posted_transaction != self.posted_transaction:
            self.begin_edit()
            self.posted_transaction = posted_transaction
            self.set_dirty()
            self.commit_edit()

    def get_posted_transaction(self):
        return self.posted_transaction
    
    def set_date(self,date:datetime.datetime):
        if date is not None and date != self.date:
            self.begin_edit()
            self.date = date
            self.set_dirty()
            self.commit_edit()
            
    def get_date(self):
        return self.date

    def set_amount(self,amount:Decimal):
        if amount is not None and amount != self.amount:
            self.begin_edit()
            self.amount = amount
            self.set_dirty()
            self.commit_edit()

    def get_amount(self):
        return self.amount

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)
        
    
