import calendar
import datetime
import time
from enum import IntEnum
from typing import List

from dateutil.relativedelta import relativedelta
from sqlalchemy import INTEGER, Enum, Date

from ._declbase import *


def day_label(dow):
    tm = time.struct_time((0, 0, 0, 0, 0, 0, dow + 6, 0, -1))
    return time.strftime("%a", tm)[:2]


def _is_last_day_in_month(day: datetime.date):
    dow, d = calendar.monthrange(day.year, day.month)
    return d == day.day


class RecurrencePeriodType(IntEnum):
    ONCE = 0
    DAY = 1
    WEEK = 2
    MONTH = 3
    END_OF_MONTH = 4
    NTH_WEEKDAY = 5
    LAST_WEEKDAY = 6
    YEAR = 7
    INVALID = -1

    def __str__(self):
        return self.name.replace("_", " ").title()


class RecurrenceWeekendAdjust(IntEnum):
    NONE = 0
    BACK = 1
    FORWARD = 2
    INVALID = -1

    def __str__(self):
        return self.name.title()


@dataclass
class Recurrence(DeclarativeBase):
    """
    Recurrence information for scheduled transactions

    Attributes:
        obj_guid (str): link to the parent ScheduledTransaction record.
        multiplier (int): Multiplier for the period type. Describes how many times
            the period repeats for the next occurrence.
        period_type (RecurrencePeriodType): type or recurrence (monthly, daily).
        start (datetime.date): the date the recurrence starts.
        week_adjustment (RecurrenceWeekendAdjust): adjustment to be made if the next occurrence
            falls on weekend / non-working day.
    """

    __tablename__ = 'recurrence'

    __table_args__ = {'sqlite_autoincrement': True}

    # column definitions
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True, nullable=False)
    multiplier: int = Column('multiplier', INTEGER(), nullable=False)
    period_type: RecurrencePeriodType = Column('period_type', Enum(RecurrencePeriodType), nullable=False)
    start: datetime.date = Column('period_start', Date(), nullable=False)
    week_adjustment: RecurrenceWeekendAdjust = Column('weekend_adjust', Enum(RecurrenceWeekendAdjust), nullable=False)

    def get_period_type(self) -> RecurrencePeriodType:
        return self.period_type if self.period_type else RecurrencePeriodType.INVALID

    def set_period_type(self, _type: RecurrencePeriodType):
        self.period_type = _type

    def get_multiplier(self) -> int:
        return self.multiplier if self.multiplier else 0

    def set_multiplier(self, multiplier: int):
        self.multiplier = multiplier

    def get_date(self) -> datetime.date:
        return self.start

    def set_date(self, date: datetime.date):
        self.start = date

    def get_weekend_adjustment(self) -> RecurrenceWeekendAdjust:
        return self.week_adjustment if self.week_adjustment else RecurrenceWeekendAdjust.INVALID

    def set_week_adjustment(self, adj: RecurrenceWeekendAdjust):
        self.week_adjustment = adj

    def set(self, multiplier: int, pt: RecurrencePeriodType, start: datetime.date,
            week_adjustment: RecurrenceWeekendAdjust):
        self.period_type = pt if pt != RecurrencePeriodType.INVALID else RecurrencePeriodType.MONTH
        self.multiplier = 0 if pt == RecurrencePeriodType.ONCE else multiplier if multiplier > 0 else 1
        self.start = start

        if pt == RecurrencePeriodType.END_OF_MONTH:
            self.start = start.replace(day=1) + relativedelta(months=1) - relativedelta(days=1)

        elif pt == RecurrencePeriodType.LAST_WEEKDAY:
            dim = calendar.monthrange(start.year, start.month)[1]
            while (dim - start.day) >= 7:
                start += relativedelta(days=7)
            self.start = start

        elif pt == RecurrencePeriodType.NTH_WEEKDAY:
            if (start.day - 1) / 7 == 4:
                pt = RecurrencePeriodType.LAST_WEEKDAY

        if pt == RecurrencePeriodType.MONTH or pt == RecurrencePeriodType.END_OF_MONTH \
                or pt == RecurrencePeriodType.YEAR:
            self.week_adjustment = week_adjustment
        else:
            self.week_adjustment = RecurrenceWeekendAdjust.NONE

    @staticmethod
    def nth_weekday_compare(start: datetime.date, nex: datetime.date, pt):
        nd = nex.day
        sd = start.day
        week = 3 if sd / 7 > 3 else sd / 7
        if week > 0 and sd % 7 == 0 and sd != 28:
            week -= 1
        match_day = 7 * week + (nd - (nex.weekday() + 1) + (start.weekday() + 1) + 7) % 7
        dim = calendar.monthrange(nex.year, nex.month)[1]
        if (dim - match_day) >= 7 and pt == RecurrencePeriodType.LAST_WEEKDAY:
            match_day += 7
        if pt == RecurrencePeriodType.NTH_WEEKDAY and (match_day % 7 == 0):
            match_day += 7
        return match_day - nd

    @staticmethod
    def adjust_for_weekend(pt: RecurrencePeriodType, week_adjustment: RecurrenceWeekendAdjust,
                           date: datetime.date) -> datetime.date:
        if pt == RecurrencePeriodType.YEAR or pt == RecurrencePeriodType.MONTH \
                or pt == RecurrencePeriodType.END_OF_MONTH:
            if date.weekday() == 5 or date.weekday() == 6:
                if week_adjustment == RecurrenceWeekendAdjust.BACK:
                    date -= relativedelta(days=1 if date.weekday() == 5 else 2)

                elif week_adjustment == RecurrenceWeekendAdjust.FORWARD:
                    date += relativedelta(days=2 if date.weekday() == 5 else 1)
        return date

    def next_instance(self, ref: datetime.date) -> datetime.date:
        start = self.start
        multiplier = self.multiplier
        pt = self.period_type
        week_adjustment = self.week_adjustment
        adjusted_start = self.adjust_for_weekend(pt, week_adjustment, start)
        if ref < adjusted_start:
            return adjusted_start
        nex = ref

        if pt == RecurrencePeriodType.YEAR or pt == RecurrencePeriodType.MONTH or pt == RecurrencePeriodType.NTH_WEEKDAY or \
                pt == RecurrencePeriodType.LAST_WEEKDAY or pt == RecurrencePeriodType.END_OF_MONTH:
            if pt == RecurrencePeriodType.YEAR:
                multiplier *= 12

            if self.week_adjustment == RecurrenceWeekendAdjust.BACK and \
                    ((pt == RecurrencePeriodType.YEAR or pt == RecurrencePeriodType.MONTH
                      or pt == RecurrencePeriodType.END_OF_MONTH) and (nex.weekday() == 5 or nex.weekday() == 6)):
                nex -= relativedelta(days=1 if nex.weekday() == 5 else 2)
            if self.week_adjustment == RecurrenceWeekendAdjust.BACK and (
                    pt == RecurrencePeriodType.YEAR or pt == RecurrencePeriodType.MONTH
                    or pt == RecurrencePeriodType.END_OF_MONTH) and nex.weekday() == 4:
                tmp_sat = nex + relativedelta(days=1)
                tmp_sun = nex + relativedelta(days=2)

                if pt == RecurrencePeriodType.END_OF_MONTH:
                    if _is_last_day_in_month(nex) or _is_last_day_in_month(tmp_sat) or _is_last_day_in_month(tmp_sun):
                        nex += relativedelta(months=multiplier)
                    else:
                        nex += relativedelta(months=multiplier - 1)
                else:
                    if tmp_sat.day == start.day:
                        nex += relativedelta(days=1, months=multiplier)

                    elif tmp_sun.day == start.day:
                        nex += relativedelta(days=2, months=multiplier)

                    elif nex.day >= start.day:
                        nex += relativedelta(months=multiplier)
                    elif _is_last_day_in_month(nex):
                        nex += relativedelta(months=multiplier)
                    elif _is_last_day_in_month(tmp_sat):
                        nex += relativedelta(days=1, months=multiplier)
                    elif _is_last_day_in_month(tmp_sun):
                        nex += relativedelta(days=2, months=multiplier)

                    else:
                        nex += relativedelta(months=multiplier - 1)

            elif _is_last_day_in_month(nex) or \
                    ((pt == RecurrencePeriodType.MONTH or pt == RecurrencePeriodType.YEAR) and nex.day >= start.day) \
                    or (pt == RecurrencePeriodType.NTH_WEEKDAY or pt == RecurrencePeriodType.LAST_WEEKDAY) and \
                    self.nth_weekday_compare(start, nex, pt) <= 0:
                nex += relativedelta(months=multiplier)
            else:
                nex += relativedelta(months=multiplier - 1)
        elif pt == RecurrencePeriodType.WEEK or pt == RecurrencePeriodType.DAY:
            if pt == RecurrencePeriodType.WEEK:
                multiplier *= 7
            nex += relativedelta(days=multiplier)

        elif pt == RecurrencePeriodType.ONCE:
            return ref

        if pt == RecurrencePeriodType.YEAR or pt == RecurrencePeriodType.MONTH or \
                pt == RecurrencePeriodType.NTH_WEEKDAY or pt == RecurrencePeriodType.LAST_WEEKDAY \
                or pt == RecurrencePeriodType.END_OF_MONTH:
            n_months = 12 * (nex.year - start.year) + (nex.month - start.month)
            nex -= relativedelta(months=n_months % multiplier)
            dim = calendar.monthrange(nex.year, nex.month)[1]
            if pt == RecurrencePeriodType.LAST_WEEKDAY or pt == RecurrencePeriodType.NTH_WEEKDAY:
                wdresult = self.nth_weekday_compare(start, nex, pt)
                if wdresult < 0:
                    wdresult = -wdresult
                    nex -= relativedelta(days=wdresult)
                else:
                    nex += relativedelta(days=wdresult)

            elif pt == RecurrencePeriodType.END_OF_MONTH or start.day >= dim:
                nex = nex.replace(day=dim)
            else:
                nex = nex.replace(day=start.day)
            nex = self.adjust_for_weekend(pt, week_adjustment, nex)
        elif pt == RecurrencePeriodType.WEEK or pt == RecurrencePeriodType.DAY:
            nex -= relativedelta(days=(nex - start).days % multiplier)
        return nex

    def nth_instance(self, n):
        date = self.start
        ref = datetime.date.today()
        for i in range(n):
            date = self.next_instance(ref)
        return date

    @staticmethod
    def list_next_instance(rlist: List, ref: datetime.date):
        rlist: List[Recurrence]
        nex = None
        if rlist is None:
            return nex
        for r in rlist:
            next_single = r.next_instance(ref)
            if nex is not None:
                if nex > next_single:
                    nex, next_single = next_single, nex
            else:
                nex = next_single

        return nex

    @staticmethod
    def list_to_string(rlist):
        if rlist is None or not any(rlist):
            return "None"
        else:
            return " + ".join(map(str, rlist))

    @classmethod
    def list_to_compact_string(cls, rlist):
        if rlist is None or len(rlist) == 0:
            return "None"
        if len(rlist) > 1:
            if cls.list_is_weekly_multiple(rlist):
                return cls._weekly_list_to_compact_string(rlist)
            elif cls.list_is_semi_monthly(rlist):
                first = rlist[0]
                second = rlist[1]
                s = "Semi-monthly "
                multiplier = first.get_multiplier()
                if multiplier > 1:
                    s += " (x%u)" % multiplier
                s += ": "
                s += cls._monthly_append_when(first)
                s += ", "
                s += cls._monthly_append_when(second)
                return s

            else:
                return "Unknown, %d-size list." % len(rlist)
        else:
            r = rlist[0]
            multiplier = r.get_multiplier()
            t = r.get_period_type()
            if t == RecurrencePeriodType.ONCE:
                return "Once"
            elif t == RecurrencePeriodType.DAY:
                s = "Daily"
                if multiplier > 1:
                    s += " (x%u)" % multiplier
                return s
            elif t == RecurrencePeriodType.WEEK:
                return cls._weekly_list_to_compact_string(rlist)
            elif t == RecurrencePeriodType.MONTH or t == RecurrencePeriodType.END_OF_MONTH or \
                    t == RecurrencePeriodType.LAST_WEEKDAY:
                s = "Monthly"
                if multiplier > 1:
                    s += " (x%u)" % multiplier
                s += ": "
                return s + cls._monthly_append_when(r)

            elif t == RecurrencePeriodType.NTH_WEEKDAY:
                s = "Monthly"
                if multiplier > 1:
                    s += " (x%u)" % multiplier
                s += ": "
                return s + cls._monthly_append_when(r)

            elif t == RecurrencePeriodType.YEAR:
                s = "Yearly"
                if multiplier > 1:
                    s += " (x%u)" % multiplier
                s += ": "
                return s + cls._monthly_append_when(r)

    @staticmethod
    def list_is_semi_monthly(recurrences):
        if len(recurrences) != 2:
            return False
        first = recurrences[0]
        second = recurrences[1]
        first_period = first.get_period_type()
        second_period = second.get_period_type()
        if not ((first_period == RecurrencePeriodType.MONTH
                 or first_period == RecurrencePeriodType.END_OF_MONTH
                 or first_period == RecurrencePeriodType.LAST_WEEKDAY)
                and (second_period == RecurrencePeriodType.MONTH
                     or second_period == RecurrencePeriodType.END_OF_MONTH
                     or second_period == RecurrencePeriodType.LAST_WEEKDAY)):
            return False
        return True

    @staticmethod
    def list_is_weekly_multiple(recurrences):
        for r in recurrences:
            if r.get_period_type() != RecurrencePeriodType.WEEK:
                return False
        return True

    def __str__(self):
        tmp_date = self.start.strftime("%x")
        if self.period_type == RecurrencePeriodType.ONCE:
            return u"once on %s" % tmp_date
        tmp_period = self.period_type.value
        if self.multiplier > 1:
            return u"Recurrence<Every %d %ss beginning %s>" % (self.multiplier, tmp_period, tmp_date)
        else:
            return u"Recurrence<Every %s beginning %s>" % (tmp_period, tmp_date)

    def __repr__(self):
        return str(self)

    @classmethod
    def _weekly_list_to_compact_string(cls, rs):
        dow_present_bits = 0
        multiplier = -1
        for r in rs:
            date = r.get_date()
            dow = date.weekday() + 1
            dow_present_bits |= (1 << (dow % 7))
            multiplier = r.get_multiplier()
        s = "Weekly "
        if multiplier > 1:
            s += " (x%u) " % multiplier
        for dow_idx in range(7):
            if (dow_present_bits & (1 << dow_idx)) != 0:
                s += day_label(dow_idx)
            else:
                s += "-"
        return s

    @classmethod
    def _monthly_append_when(cls, r):
        date = r.get_date()
        if r.get_period_type() == RecurrencePeriodType.LAST_WEEKDAY:
            date_name_buf = day_label((date.weekday() + 1) % 7)
            return "last %s" % date_name_buf
        elif r.get_period_type() == RecurrencePeriodType.NTH_WEEKDAY:
            numerals = ["1st", "2nd", "3rd", "4th"]
            date_name_buf = day_label((date.weekday() + 1) % 7)
            day_of_month_index = date.day - 1
            week = 3 if day_of_month_index / 7 > 3 else day_of_month_index / 7
            return "%s %s" % (numerals[week], date_name_buf)
        else:
            return str(date.day)
