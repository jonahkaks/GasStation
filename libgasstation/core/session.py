import locale
import logging
import weakref

from .backend import *
from .event import Event


class Session:
    current_session = None
    providers = None
    m_uri: Uri
    m_book: Book
    m_backend: BackEnd

    def __init__(self, book):
        self.m_book = book
        self.m_uri = None
        self.m_backend = None
        self.m_saving: bool = False
        self.m_creating = False
        self.m_last_err = BackEndError.NO_ERR
        self.m_error_message = ""

    def get_url(self):
        return self.m_uri if self.m_uri is not None else self.m_book.get_uri() if self.m_book is not None else ""

    def get_error(self):
        if self.m_last_err != BackEndError.NO_ERR:
            return self.m_last_err
        if self.m_book is None or self.m_book.get_backend() is None:
            return BackEndError.NO_ERR
        self.m_last_err = self.m_book.get_backend().get_error()
        return self.m_last_err

    def get_backend(self) -> BackEnd:
        return self.m_backend

    def close(self):
        self.destroy()

    def push_error(self, err, msg):
        self.m_last_err = err
        self.m_error_message = msg

    def load(self, percentage_func):
        if self.m_uri is None:
            return
        self.clear_error()
        if self.m_book is not None:
            self.m_book.set_backend(self.m_backend)
        if self.m_backend is not None:
            self.m_backend.set_percentage(percentage_func)
            self.m_backend.do_load(self, BackendLoadType.INITIAL_LOAD)
            self.push_error(self.m_backend.get_error(), "")

        err = self.get_error()
        if ((err != BackEndError.NO_ERR) and
                (err != FileError.FILE_TOO_OLD) and
                (err != FileError.NO_ENCODING) and
                (err != FileError.FILE_UPGRADE) and
                (err != SqlError.DB_TOO_OLD) and
                (err != SqlError.DB_TOO_NEW)):
            old_book = self.m_book
            self.m_book = Book()
            if old_book is not None:
                old_book.destroy()

    def begin(self, new_uri: Uri, mode: SessionOpenMode):
        self.clear_error()
        if self.m_uri is not None:
            if self.get_error() != BackEndError.NO_ERR:
                self.push_error(BackEndError.LOCKED, "")
                return
        if new_uri is None:
            if self.get_error() != BackEndError.NO_ERR:
                self.push_error(BackEndError.BAD_URL, "")
                return
        filename = None
        if new_uri.is_file():
            filename = os.path.basename(new_uri.get_path())
        elif new_uri.scheme is None:
            filename = new_uri
        if filename is not None and os.path.isdir(new_uri.get_path()):

            if BackEndError.NO_ERR == self.get_error():
                self.push_error(BackEndError.BAD_URL, "Url is a directory")
            return
        self.destroy_backend()
        self.m_uri = new_uri
        self.m_creating = mode == SessionOpenMode.NEW_STORE or mode == SessionOpenMode.NEW_STORE
        if filename is not None:
            self.load_backend("file")
        else:
            self.load_backend(new_uri.get_scheme())
        if self.m_backend is None:
            logging.debug("Unkown file type no backend")
            self.m_uri = None
            if BackEndError.NO_ERR == self.get_error():
                self.push_error(BackEndError.BAD_URL, "")
            return
        self.m_backend.session_begin(self, self.m_uri, mode)
        err = self.m_backend.get_error()
        msg = self.m_backend.get_message()
        if err != BackEndError.NO_ERR:
            self.push_error(err, msg)
            return

    def clear_error(self):
        self.m_error_message = ""
        self.m_last_err = BackEndError.NO_ERR

    def get_error_message(self):
        return self.m_error_message

    def pop_error(self):
        err = self.get_error()
        self.clear_error()
        return err

    def get_book(self):
        if self.m_book is None:
            return None
        if 'y' == self.m_book.book_open:
            return self.m_book
        return self.m_book

    def get_file_path(self):
        backend = self.m_book.get_backend()
        if backend is None:
            return None
        return backend.get_uri()

    def get_uri(self):
        return self.m_uri

    def is_saving(self):
        return self.m_saving

    def load_backend(self, scheme):
        if scheme == "sqlite3" or scheme == "sqlite" or scheme == "file":
            self.m_backend = SqliteBackEnd()
        elif scheme.startswith("mysql"):
            self.m_backend = MysqlBackEnd()
        elif scheme.startswith("pos"):
            self.m_backend = PosgreSqlBackEnd()

    def destroy_backend(self):
        if self.m_backend is not None:
            self.clear_error()
            self.m_backend = None
            if self.m_book is not None:
                self.m_book.set_backend(None)

    def save(self, percentage_func):
        if self.m_book is None or not self.m_book.session_not_saved():
            return
        self.m_saving = True
        if self.m_backend is not None:
            if self.m_book.get_backend() != self.m_backend:
                self.m_book.set_backend(self.m_backend)
            self.m_backend.set_percentage(percentage_func)
            self.m_backend.sync(self.m_book)
            self.m_backend.set_percentage(None)
            err = self.m_backend.get_error()
            if err != BackEndError.NO_ERR:
                self.push_error(err, "")
                self.m_saving = False
                return
            self.clear_error()
        else:
            self.push_error(BackEndError.NO_HANDLER, "failed to load backend")
        self.m_saving = False

    def safe_save(self, percentage_func):
        backend = self.m_book.get_backend()
        if backend is None: return
        backend.set_percentage(percentage_func)
        backend.safe_sync(self.get_book())
        err = backend.get_error()
        msg = backend.get_message()
        if err != BackEndError.NO_ERR:
            self.m_uri = None
            self.push_error(err, msg)
        backend.set_percentage(None)

    def ensure_all_data_loaded(self):
        if not (self.m_book and self.m_backend):
            return
        if self.m_book.get_backend() != self.m_backend:
            self.m_book.set_backend(self.m_backend)
        self.m_backend.do_load(self.m_book, BackendLoadType.LOAD_ALL)
        self.push_error(self.m_backend.get_error(), "")

    def swap_books(self, other):
        if self.m_book is not None and other.m_book is not None:
            self.m_book.read_only, other.m_book.read_only = other.m_book.read_only, self.m_book.read_only
        self.m_book, other.m_book = other.m_book, self.m_book
        if self.m_book is not None:
            mybackend = self.m_book.get_backend()
            if other.m_book is not None:
                self.m_book.set_backend(other.m_book.get_backend())
                other.m_book.set_backend(mybackend)

    def swap_data(self, sess):
        if self == sess or sess is None:
            return
        self.swap_books(sess)

    @staticmethod
    def events_pending():
        return False

    @staticmethod
    def process_events():
        return False

    def export(self, real_session, percentage_func):
        real_book = real_session.get_book()
        backend2 = self.m_book.get_backend()
        if backend2 is None: return False
        backend2.set_percentage(percentage_func)
        backend2.export_coa(real_book)
        err = backend2.get_error()
        if err != BackEndError.NO_ERR:
            return False
        return True

    @classmethod
    def get_current_session(cls):
        if cls.current_session is None:
            Event.suspend()
            book = Book()
            cls.current_session = Session(book)
            Event.resume()
        return cls.current_session

    @classmethod
    def current_session_exist(cls):
        return cls.current_session is not None

    @classmethod
    def set_current_session(cls, session):
        if cls.current_session is None:
            cls.current_session = session
        elif cls.current_session != session:
            cls.current_session.destroy()
            cls.current_session = session

    @classmethod
    def clear_current_session(cls):
        if cls.current_session is not None:
            cls.current_session.destroy()
            cls.current_session = None

    @classmethod
    def get_current_book(cls):
        s = cls.get_current_session()
        return weakref.ref(s.m_book)() if s.m_book is not None and s.m_book.book_open == 'y' else None

    @staticmethod
    def get_current_root():
        book = Session.get_current_book()
        if book is not None:
            return weakref.ref(book.get_root_account())()

    @staticmethod
    def set_current_root(root):
        book = Session.get_current_book()
        if book is not None:
            book.set_root_account(root)

    @staticmethod
    def get_current_commodity_table():
        from .commodity import CommodityTable
        return CommodityTable.get_table(Session.get_current_book())

    @staticmethod
    def locale_default_currency():
        from .commodity import COMMODITY_NAMESPACE_NAME_CURRENCY, CommodityTable
        p = locale.localeconv()
        currency = None
        cm = p.get("int_curr_symbol", False)
        if cm:
            currency = CommodityTable.get_table(Session.get_current_book()).lookup(COMMODITY_NAMESPACE_NAME_CURRENCY, cm)
        return currency if currency is not None else Session.get_current_commodity_table().lookup(
            COMMODITY_NAMESPACE_NAME_CURRENCY, "UGX")

    def destroy(self):
        del self

    def end(self):
        if self.m_book is not None:
            be = self.m_book.get_backend()
            if be is not None:
                be.session_end()
        self.clear_error()
        self.m_uri = None

    def __del__(self):
        try:
            self.end()
            self.destroy_backend()
            if self.m_book is not None:
                self.m_book.set_backend(None)
                self.m_book.destroy()
                self.m_book = None
        except:
            pass

    def save_in_progress(self):
        return self.is_saving()

    @staticmethod
    def is_new_book():
        return True if not Session.current_session_exist() or (Session.current_session_exist() and (
                Session.get_current_root() is None or not Session.get_current_root().get_n_children())) else False
