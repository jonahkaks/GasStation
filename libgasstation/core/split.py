import datetime
import enum
import logging

from sqlalchemy import Index, DECIMAL, DateTime, Enum, ForeignKey, VARCHAR
from sqlalchemy.orm import reconstructor, relationship

from ._declbase import *
from .event import *
from .query_private import *

utc = pytz.UTC
GAINS_STATUS_UNKNOWN = 0xff
GAINS_STATUS_CLEAN = 0x0
GAINS_STATUS_GAINS = 0x3
GAINS_STATUS_DATE_DIRTY = 0x10
GAINS_STATUS_AMNT_DIRTY = 0x20
GAINS_STATUS_VALU_DIRTY = 0x40
GAINS_STATUS_LOT_DIRTY = 0x80
GAINS_STATUS_ADIRTY = (GAINS_STATUS_AMNT_DIRTY | GAINS_STATUS_LOT_DIRTY)
GAINS_STATUS_VDIRTY = GAINS_STATUS_VALU_DIRTY
GAINS_STATUS_A_VDIRTY = (GAINS_STATUS_AMNT_DIRTY | GAINS_STATUS_VALU_DIRTY | GAINS_STATUS_LOT_DIRTY)

SPLIT_DATE_RECONCILED = "date-reconciled"
SPLIT_BALANCE = "balance"
SPLIT_CLEARED_BALANCE = "cleared-balance"
SPLIT_RECONCILED_BALANCE = "reconciled-balance"
SPLIT_MEMO = "memo"
SPLIT_ACTION = "action"
SPLIT_RECONCILE = "reconcile-flag"
SPLIT_AMOUNT = "amount"
SPLIT_SHARE_PRICE = "share-price"
SPLIT_VALUE = "value"
SPLIT_TYPE = "type"
SPLIT_VOIDED_AMOUNT = "voided-amount"
SPLIT_VOIDED_VALUE = "voided-value"
SPLIT_LOT = "lot"
SPLIT_TRANS = "trans"
SPLIT_ACCOUNT = "account"
SPLIT_ACCOUNT_GUID = "account-guid"
SPLIT_ACCT_FULLNAME = "acct-fullname"
SPLIT_CORR_ACCT_NAME = "corr-acct-fullname"
SPLIT_CORR_ACCT_CODE = "corr-acct-code"


class SplitState(enum.Enum):
    UNRECONCILED = 'n'
    CLEARED = 'c'
    RECONCILED = 'y'
    FROZEN = 'f'
    VOIDED = 'v'


class SplitType(enum.Enum):
    NORMAL = 'n'
    STOCK = 's'


@dataclass
class Split(DeclarativeBaseGuid):
    """
    A Basic transaction Split.
    .. note::
        A split used in a scheduled transaction has its main attributes in form of slots.
    Attributes:
        transaction(:class:`models.accounting.transaction.Transaction`): transaction of the split
        account(:class:`models.accounting.account.Account`): account of the split
        lot(:class:`models.accounting.lot.Lot`): lot to which the split pertains
        memo(str): memo of the split
        value(:class:`decimal.Decimal`): amount express in the currency of the transaction of the split
        amount(:class:`decimal.Decimal`): amount express in the commodity of the account of the split
        reconcile_state(:class:'SplitState'):current state of the split
        reconcile_date(:class:`datetime.datetime`): time
        action(str): describe the type of action behind the split (free form string but with dropdown in the GUI
    """

    __tablename__ = "split"

    __table_args__ = (
        # indices
        Index("splits_tx_guid_index", "tx_guid"),
        Index("splits_account_guid_index", "account_guid"),
    )
    transaction_guid: str = Column("tx_guid", UUIDType(binary=False), ForeignKey("transaction.guid"), nullable=False)
    account_guid: str = Column("account_guid", UUIDType(binary=False), ForeignKey("account.guid"),
                               nullable=False)
    type: SplitType = Column('type', Enum(SplitType), nullable=False, default=SplitType.NORMAL)
    memo: str = Column("memo", VARCHAR(length=50))
    action: str = Column("action", VARCHAR(length=20))
    reconcile_state: SplitState = Column("reconcile_state", Enum(SplitState), nullable=False,
                                         default=SplitState.UNRECONCILED)
    reconcile_date: datetime.date = Column("reconcile_date", DateTime(timezone=False), default=None)
    value: Decimal = Column("value", DECIMAL(15, 2), nullable=False)
    amount: Decimal = Column("amount", DECIMAL(15, 2), nullable=False)
    lot_guid: str = Column("lot_guid", UUIDType(binary=False), ForeignKey("lot.guid"))

    void_former_amt: Decimal = Column('void_former_amount', DECIMAL(15, 2))
    void_former_val: Decimal = Column('void_former_value', DECIMAL(15, 2))

    scheduled_credit_formula: str = Column("credit_formula", VARCHAR(length=90))
    scheduled_debit_formula: str = Column("debit_formula", VARCHAR(length=90))
    scheduled_debit: Decimal = Column("debit", DECIMAL(15, 2), default=None)
    scheduled_credit: Decimal = Column("credit", DECIMAL(15, 2), default=None)
    scheduled_account_guid = Column('sx_account_guid', UUIDType(binary=False), ForeignKey('account.guid'))

    account = relationship('Account', foreign_keys=[account_guid])
    scheduled_account = relationship('Account', foreign_keys=[scheduled_account_guid])
    lot = relationship('Lot', back_populates='splits')
    transaction = relationship('Transaction', back_populates='splits')
    balance_dirty = False
    sort_dirty = False
    balance = Decimal(0)
    cleared_balance = Decimal(0)
    reconciled_balance = Decimal(0)
    noclosing_balance = Decimal(0)
    orig_acc = None
    orig_trans = None
    gains = GAINS_STATUS_UNKNOWN
    gains_split = None
    e_type = ID_SPLIT

    def __init__(self, book):
        self.value = Decimal(0)
        self.amount = Decimal(0)
        self.reconcile_state = SplitState.UNRECONCILED
        self.balance = Decimal(0)
        self.cleared_balance = Decimal(0)
        self.reconciled_balance = Decimal(0)
        self.noclosing_balance = Decimal(0)
        self.gains = GAINS_STATUS_UNKNOWN
        self.type = SplitType.NORMAL
        self.gains_split = None
        if book is not None:
            super().__init__(ID_SPLIT, book)
            Event.gen(self, EVENT_CREATE)

    def reinit(self):
        self.account = None
        self.transaction = None
        self.lot = None
        self.action = ""
        self.memo = ""
        self.reconcile_state = SplitState.UNRECONCILED
        self.set_amount(Decimal(0))
        self.set_value(Decimal(0))
        self.balance = Decimal(0)
        self.cleared_balance = Decimal(0)
        self.reconciled_balance = Decimal(0)
        self.noclosing_balance = Decimal(0)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        self.balance = Decimal(0)
        self.cleared_balance = Decimal(0)
        self.reconciled_balance = Decimal(0)
        self.noclosing_balance = Decimal(0)
        book = Session.get_current_book()
        super().__init__(ID_SPLIT, book)
        self.infant = False
        self.dirty = True

    @staticmethod
    def get_unique_transactions(slist):
        r = []
        if slist is None:
            return []
        for t in map(lambda sp: sp.transaction, slist):
            if t not in r:
                r.append(t)
        return r

    def get_currency_denom(self):
        from .commodity import COMMODITY_MAX_FRACTION
        if self.transaction is None or not self.transaction.currency:
            return COMMODITY_MAX_FRACTION
        else:
            return self.transaction.currency.get_fraction()

    def get_commodity_denom(self):
        from .commodity import COMMODITY_MAX_FRACTION
        if not self.account:
            return COMMODITY_MAX_FRACTION
        else:
            return self.account.get_commodity_scu()

    def get_void_former_amount(self):
        return self.void_former_amt

    def get_void_former_value(self):
        return self.void_former_val

    def void(self):
        zero = Decimal(0)
        self.void_former_amt = self.get_amount()
        self.void_former_val = self.get_value()
        self.set_amount(zero)
        self.set_value(zero)
        self.set_reconcile_state(SplitState.VOIDED)

    def unvoid(self):
        self.set_amount(self.get_void_former_amount())
        self.set_value(self.get_void_former_value())
        self.set_reconcile_state(SplitState.UNRECONCILED)
        self.void_former_amt = Decimal(0)
        self.void_former_val = Decimal(0)
        self.set_dirty()

    def __gt__(self, other):
        if self == other:
            return False
        if other is None:
            return False
        action_for_num = self.book.use_split_action_for_num_field()
        if action_for_num and self.transaction.order_num_action(self.action, other.transaction, other.action):
            return True
        elif self.transaction > other.transaction:
            return True
        if self.memo is not None and other.memo is not None and self.memo != other.memo:
            return True if self.memo > other.memo else False
        if self.action is not None and other.action is not None and self.action != other.action:
            return True if self.action > other.action else False

        if self.reconcile_state != other.reconcile_state:
            return True if self.reconcile_state > other.reconcile_state else False

        if self.amount != other.amount:
            return True if self.amount > other.amount else False
        if self.value != other.value:
            return True if self.value > other.value else False
        if self.reconcile_date is not None and other.reconcile_date is not None and self.reconcile_date != other.reconcile_date:
            return True if self.reconcile_date > other.reconcile_date else False
        return True if self.guid > other.guid else False

    def get_guid(self):
        return self.guid

    def mark(self):
        if self.account is not None:
            self.account.balance_dirty = True
            self.account.sort_dirty = True
        if self.lot is not None:
            self.lot.set_closed_unknown()

    def clone(self):
        split = self.dupe()
        return split

    def dupe(self):
        split = Split(None)
        split.infant = None
        split.transaction = self.transaction
        split.account = self.account
        split.lot = self.lot
        split.memo = self.memo
        split.action = self.action
        split.type = self.type
        split.custom_data = self.custom_data.copy() if self.custom_data is not None else None
        split.reconcile_state = self.reconcile_state
        split.reconcile_date = self.reconcile_date
        split.value = self.value
        split.amount = self.amount
        split.void_former_amt = self.void_former_amt
        split.void_former_val = self.void_former_val
        split.scheduled_account_guid = self.scheduled_account_guid
        split.scheduled_account = self.scheduled_account
        split.scheduled_debit_formula = self.scheduled_debit_formula
        split.scheduled_credit_formula = self.scheduled_credit_formula
        split.scheduled_credit = self.scheduled_credit
        split.scheduled_debit = self.scheduled_debit
        sess = object_session(split)
        if sess is not None:
            sess.expunge(split)
        return split

    def copy_onto(self, other):
        if other is None: return
        if other.transaction:
            other.transaction.begin_edit()
        other.set_memo(self.get_memo())
        other.set_action(self.get_action())
        other.set_amount(self.get_amount())
        other.set_value(self.get_value())
        other.set_account(self.get_account())
        other.set_dirty()
        if other.transaction:
            other.transaction.commit_edit()

    def clone_no_kvp(self):
        split = Split(None)
        split.memo = self.memo
        split.action = self.action
        split.reconcile_state = self.reconcile_state
        split.reconcile_date = self.reconcile_date
        split.value = self.value
        split.amount = self.amount
        split.balance = self.balance
        split.cleared_balance = self.cleared_balance
        split.reconciled_balance = self.reconciled_balance
        split.noclosing_balance = self.noclosing_balance
        split.account = self.account
        Instance.__init__(split, ID_SPLIT, self.book)
        if self.lot is not None:
            self.lot.add_split(split)
        return split

    def set_transaction(self, transaction):
        if transaction is None or transaction == self.transaction:
            return
        transaction.begin_edit()
        old_trans = self.transaction
        if old_trans is not None:
            old_trans.begin_edit()
        ed = EventData()
        ed.node = self
        if old_trans is not None:
            ed.idx = old_trans.get_split_index(self)
            Event.gen(old_trans, EVENT_ITEM_REMOVED, ed)
        self.transaction = transaction
        if old_trans is not None:
            old_trans.commit_edit()
        self.set_dirty()
        if transaction is not None:
            ed.idx = -1
            Event.gen(transaction, EVENT_ITEM_ADDED, ed)
        transaction.commit_edit()

    def set_share_price(self, price):
        if self.transaction is None:
            return
        self.transaction.begin_edit()
        self.value = self.get_amount() * price
        self.mark()
        self.set_dirty()
        self.transaction.commit_edit()

    def get_share_price(self):
        amt = self.amount
        val = self.value
        if amt.is_zero():
            if val.is_zero():
                return Decimal(1)
            return Decimal(0)
        return val / amt

    def get_type(self):
        return self.type

    def make_stock(self):
        self.transaction.begin_edit()
        self.amount = Decimal(0)
        self.type = SplitType.STOCK
        self.mark()
        self.set_dirty()
        self.transaction.commit_edit()

    def get_transaction(self):
        return self.transaction

    def get_other_split(self):
        if self.transaction is None:
            return None
        other = None
        trading_accts = self.transaction.use_trading_accounts()
        num_splits = len(self.transaction)
        count = num_splits
        if num_splits != 2 and not trading_accts:
            return None
        for s in self.transaction.get_splits():
            if s == self:
                count -= 1
                continue
            if trading_accts and s.get_account().get_type().is_trading():
                count -= 1
                continue
            other = s
        if count == 1:
            return other
        else:
            return None

    def commit_edit(self):
        if not self.get_dirty():
            return
        orig_acc = self.orig_acc
        acc = self.account
        if self.lot is not None and (self.lot.get_account() != acc or self.get_destroying()):
            self.lot.remove_split(self)
        if orig_acc is not None and (orig_acc != acc or self.get_destroying()):
            if not orig_acc.remove_split(self):
                logging.error("Account lost track of moved or deleted split.")
        if acc is not None and orig_acc != acc and not self.get_destroying():
            if acc.insert_split(self):
                if self.lot is not None and self.lot.get_account() is None:
                    acc.insert_lot(self.lot)
            else:
                raise ValueError("Account grabbed split prematurely.")
            self.set_amount(self.get_amount())
        if self.transaction != self.orig_trans and self.orig_trans is not None:
            Event.gen(self.orig_trans, EVENT_MODIFY)
        if self.lot is not None:
            Event.gen(self.lot, EVENT_MODIFY)

        self.orig_acc = self.account
        self.orig_trans = self.transaction
        if self.transaction is not None:
            if self.get_destroying() and not self.transaction.get_destroying():
                self.commit_edit_part2(self.on_error, None, self.free)
            elif self.get_dirty() and not self.transaction.get_dirty() and not self.get_destroying():
                self.commit_edit_part2(self.on_error, None, self.free)
        if acc is not None:
            acc.sort_dirty = True
            acc.balance_dirty = True
            acc.recompute_balance()

    def free(self):
        self.account = None
        self.orig_acc = None
        self.orig_trans = None
        if self.transaction is not None:
            self.transaction.splits.remove(self)
            self.transaction = None
        self.memo = None
        del self

    def rollback_edit(self):
        if self.account != self.orig_acc:
            self.account = self.orig_acc
        if self.get_destroying() and self.transaction is not None:
            ed = EventData()
            self.set_destroying(False)
            ed.node = self
            ed.idx = -1
            Event.gen(self.transaction, EVENT_ITEM_ADDED, ed)
        self.set_transaction(self.orig_trans)

    def set_type(self, value):
        if value == self.get_type():
            return
        if self.transaction is not None:
            self.transaction.begin_edit()
        self.type = value
        self.set_dirty()
        if self.transaction is not None:
            self.transaction.commit_edit()

    def set_account(self, account):
        if account is None or account == self.account:
            return
        if not self.books_equal(account):
            return
        if self.transaction is not None:
            self.transaction.begin_edit()
        self.account = account
        self.set_dirty()
        if self.transaction is not None:
            self.transaction.commit_edit()

    def get_account(self):
        return self.account

    def set_reconcile_state(self, state):
        if state == self.reconcile_state:
            return
        if self.transaction is not None:
            self.transaction.begin_edit()
        if isinstance(state, SplitState):
            self.reconcile_state = state
            self.set_dirty()
            self.mark()
            if self.account is not None:
                self.account.recompute_balance()

        if self.transaction is not None:
            self.transaction.commit_edit()

    def set_credit_formula(self, cf):
        self.scheduled_credit_formula = cf

    def get_credit_formula(self):
        return self.scheduled_credit_formula

    def set_debit_formula(self, cf):
        self.scheduled_debit_formula = cf

    def get_debit_formula(self):
        return self.scheduled_debit_formula

    def set_credit(self, cf):
        self.scheduled_credit = cf

    def get_credit(self):
        return self.scheduled_credit

    def set_debit(self, cf):
        self.scheduled_debit = cf

    def get_debit(self):
        return self.scheduled_debit

    def get_reconcile_state(self):
        return self.reconcile_state or SplitState.UNRECONCILED

    def set_reconcile_date(self, date):
        if date == self.reconcile_date:
            return
        if self.transaction is not None:
            self.transaction.begin_edit()
        self.reconcile_date = date
        self.set_dirty()
        if self.transaction is not None:
            self.transaction.commit_edit()

    def get_reconcile_date(self):
        return self.reconcile_date

    def set_value(self, value: Decimal):
        if value != self.value and not value.is_zero():
            if self.transaction is not None:
                self.transaction.begin_edit()
            self.value = value
            self.mark()
            self.set_dirty()
            if self.transaction is not None:
                self.transaction.commit_edit()

    def get_value(self) -> Decimal:
        return self.value

    def set_amount(self, amount: Decimal):
        if amount != self.amount and not amount.is_zero():
            if self.transaction is not None:
                self.transaction.begin_edit()
            self.amount = amount
            self.mark()
            self.set_dirty()
            if self.transaction is not None:
                self.transaction.commit_edit()

    def get_amount(self) -> Decimal:
        return self.amount

    def set_memo(self, memo):
        if memo == self.memo:
            return
        if self.transaction is not None:
            self.transaction.begin_edit()
        self.memo = memo
        self.set_dirty()
        if self.transaction is not None:
            self.transaction.commit_edit()

    def get_memo(self):
        return self.memo

    def set_action(self, action):
        if action is None or not len(action):
            return
        if self.transaction is not None:
            self.transaction.begin_edit()
        self.action = action
        self.set_dirty()
        if self.transaction is not None:
            self.transaction.commit_edit()

    def get_action(self):
        return self.action

    def set_lot(self, lot):
        if self.transaction is not None:
            self.transaction.begin_edit()
        self.lot = lot
        self.set_dirty()
        if self.transaction is not None:
            self.transaction.commit_edit()

    def get_lot(self):
        return self.lot

    def get_balance(self):
        return self.balance

    def get_no_closing_balance(self):
        return self.noclosing_balance

    def get_reconciled_balance(self):
        return self.reconciled_balance

    def get_cleared_balance(self):
        return self.cleared_balance

    def set_base_value(self, value: Decimal, base_currency):
        self.transaction.begin_edit()
        if self.account is None: return
        currency = self.transaction.get_currency()
        commodity = self.account.get_commodity()

        if currency == base_currency:
            if commodity == base_currency:
                self.amount = value
            self.value = value
        elif commodity == base_currency:
            self.amount = value
        else:
            return
        self.mark()
        self.set_dirty()
        self.transaction.commit_edit()

    def get_base_value(self, commodity, base_currency):
        if self.account is None or self.transaction is None: return Decimal(0)

        if self.transaction.get_currency() == base_currency:
            return self.value
        if self.account.get_commodity() == base_currency:
            return self.amount
        return Decimal(0)

    def __repr__(self) -> str:
        try:
            cur = self.transaction.currency.mnemonic
            acc = self.account
            com = acc.commodity.mnemonic
            if com == "template":
                # case of template split from scheduled transaction
                credit = self.scheduled_credit_formula
                debit = self.scheduled_debit_formula
                return u"SplitTemplate<{} {} {}>".format(self.scheduled_account_guid,
                                                         "credit={}".format(credit) if credit else "",
                                                         "debit={}".format(debit) if debit else "",

                                                         )
            elif cur == com:
                # case of same currency split
                return u"Split<{} {} {}>".format(acc,
                                                 self.value, cur)
            else:
                # case of non currency split
                return u"Split<{} {} {} [{} {}]>".format(acc,
                                                         self.value, cur,
                                                         self.get_amount(), com)
        except AttributeError:
            return u"Split<{}>".format(self.account)

    def convert_amount(self, account):
        amount = self.get_amount()
        split_acc = self.account
        if split_acc == account or split_acc is None or account is None:
            return amount
        acc_com = split_acc.get_commodity()
        to_commodity = account.get_commodity()
        if acc_com and acc_com.equal(to_commodity):
            return amount
        txn = self.transaction
        if txn and txn.is_balanced():
            osplit = self.get_other_split()
            if osplit:
                split_comm = osplit.get_account().get_commodity()
                if not to_commodity.equal(split_comm):
                    return Decimal(0)
            return -osplit.get_amount()
        value = self.get_value()
        if value.is_zero():
            return value
        convrate = txn.get_account_conv_rate(account)
        return value * convrate

    def destroy(self):
        acc = self.account
        trans = self.transaction
        if trans is None:
            return False
        if acc is not None and not acc.get_destroying() and not trans.get_destroying() and trans.is_readonly():
            return False
        trans.begin_edit()
        self.set_dirty()
        self.set_destroying(True)
        self.mark()
        Event.gen(trans, EVENT_ITEM_REMOVED, EventData(self, trans.get_split_index(self)))
        trans.commit_edit()
        return True

    def equal(self, other, check_guids, check_balances, check_txn_splits):
        if other is None: return False
        if self == other: return True
        same_book = self.get_book() == other.get_book()
        if check_guids:
            if self.guid == other.guid:
                return False
        if (same_book and self.memo != other.memo) or (not same_book and self.memo != other.memo):
            return False
        if (same_book and self.action != other.action) or (not same_book and self.action != other.action):
            return False
        if self.reconcile_state != other.reconcile_state:
            return False
        if self.reconcile_date != other.reconcile_date:
            return False

        if self.get_amount() != other.get_amount():
            return False
        if self.get_value() != other.get_value():
            return False

        if check_balances:
            if self.balance != other.balance: return False
            if self.cleared_balance != other.cleared_balance: return False
            if self.reconciled_balance != other.reconciled_balance: return False

        if not self.transaction.equal(other.transaction, check_guids, check_txn_splits,
                                      check_balances, False): return False
        return True

    def no_op(self, p):
        return self

    def compare_other_account_names(self, o):
        pass

    def compare_other_account_codes(self, o):
        pass

    @classmethod
    def register(cls):
        params = [Param(SPLIT_DATE_RECONCILED, TYPE_DATE, cls.get_reconcile_date, cls.set_reconcile_date),
                  Param(SPLIT_BALANCE, TYPE_NUMERIC, cls.get_balance),
                  Param(SPLIT_CLEARED_BALANCE, TYPE_NUMERIC, cls.get_cleared_balance),
                  Param(SPLIT_RECONCILED_BALANCE, TYPE_NUMERIC, cls.get_reconciled_balance),
                  Param(SPLIT_MEMO, TYPE_STRING, cls.get_memo, cls.set_memo),
                  Param(SPLIT_ACTION, TYPE_STRING, cls.get_action, cls.set_action),
                  Param(SPLIT_RECONCILE, TYPE_ENUM, cls.get_reconcile_state, cls.set_reconcile_state),
                  Param(SPLIT_AMOUNT, TYPE_NUMERIC, cls.get_amount, cls.set_amount),
                  Param(SPLIT_SHARE_PRICE, TYPE_NUMERIC, cls.get_share_price, cls.set_share_price),
                  Param(SPLIT_VALUE, TYPE_DEBCRED, cls.get_value, cls.set_value),
                  Param(SPLIT_TYPE, TYPE_STRING, cls.get_type, cls.set_type),
                  Param(SPLIT_VOIDED_AMOUNT, TYPE_NUMERIC, cls.get_void_former_amount),
                  Param(SPLIT_VOIDED_VALUE, TYPE_NUMERIC, cls.get_void_former_value),
                  Param(SPLIT_LOT, ID_LOT, cls.get_lot),
                  Param(SPLIT_TRANS, ID_TRANS, cls.get_transaction, cls.set_transaction),
                  Param(SPLIT_ACCOUNT, ID_ACCOUNT, cls.get_account, cls.set_account),
                  Param(SPLIT_ACCOUNT_GUID, TYPE_GUID, cls.get_guid),
                  Param(SPLIT_ACCT_FULLNAME, SPLIT_ACCT_FULLNAME, cls.no_op),
                  Param(SPLIT_CORR_ACCT_NAME, SPLIT_CORR_ACCT_NAME, cls.no_op),
                  Param(SPLIT_CORR_ACCT_CODE, SPLIT_CORR_ACCT_CODE, cls.no_op),
                  Param(PARAM_BOOK, ID_BOOK, cls.get_book),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_SPLIT, None, params)
        ObjectClass.register(cls, SPLIT_ACCT_FULLNAME, None, [])
        ObjectClass.register(cls, SPLIT_CORR_ACCT_NAME, Split.compare_other_account_names, [])
        ObjectClass.register(cls, SPLIT_CORR_ACCT_CODE, Split.compare_other_account_codes, [])

    @staticmethod
    def order_date_only(self, other):
        if self is None:
            return -1
        if other is None:
            return 1
        ta = self.transaction
        tb = other.transaction
        if ta is None and tb is None: return 0
        if tb is None: return -1
        if ta is None: return +1
        if ta.post_date < tb.post_date:
            return -1
        if ta.post_date > tb.post_date:
            return 1
        return int(ta.enter_date.replace(tzinfo=utc) > tb.enter_date.replace(tzinfo=utc)) - int(
            ta.enter_date.replace(tzinfo=utc) < tb.enter_date.replace(tzinfo=utc))
