from functools import cmp_to_key
from uuid import UUID

from .book import Book
from .query_core import *


class QueryOp(IntEnum):
    AND = 1
    OR = 2
    NAND = 3
    NOR = 4
    XOR = 5


QUERY_FIRST_TERM = QueryOp.AND
QUERY_DEFAULT_SORT = "QueryDefaultSort"
PARAM_BOOK = "book"
PARAM_GUID = "guid"
PARAM_KVP = "kvp"
PARAM_ACTIVE = "active"
PARAM_VERSION = "version"


def param_list_cmp(l1, l2):
    if l1 is None and l2 is None: return 0
    if l1 is None and l2: return -1
    if l1 and l2 is None: return 1
    ret = 0
    try:
        for i, x in enumerate(l1):
            if x != l2[i]:
                ret = 1
                break
    except IndexError:
        return -1

    return ret


class QueryTerm:
    def __init__(self):
        self.param_list = []
        self.pdata = None
        self.invert = False
        self.param_fcns = []
        self.pred_fcn = None

    def __repr__(self):
        return "QueryTerms({}, pred={}, pred_func={}) ".format(".".join(self.param_list), self.pdata, self.pred_fcn)

    def get_param_path(self):
        if self is None:
            return None
        return self.param_list

    def get_pred_data(self):
        if self is None:
            return None
        return self.pdata

    def is_inverted(self):
        if self is None:
            return False
        return self.invert

    def __eq__(self, other):
        if self is None or other is None: return False
        if self.invert != other.invert: return False
        if param_list_cmp(self.param_list, other.param_list): return False
        return QueryCore.predicate_equal(self.pdata, other.pdata)


class QuerySort:
    def __init__(self):
        self.param_list = None
        self.options = None
        self.increasing = False
        self.use_default = False
        self.param_fcns = []
        self.obj_cmp = None
        self.comp_fcn = None

    def get_param_path(self):
        if self is None:
            return None
        return self.param_list

    def get_sort_options(self):
        if self is None:
            return 0
        return self.options

    def get_increasing(self):
        if self is None:
            return False
        return self.increasing

    def __eq__(self, other):
        if self == other: return True
        if self is None or other is None: return False
        if self.param_list is None and other.param_list is None: return True

        if self.options != other.options: return False
        if self.increasing != other.increasing: return False
        return param_list_cmp(self.param_list, self.param_list) == 0


class QueryPrivate:
    def __init__(self, initial_term=None):
        self.search_for_ = None
        self.defaultSort = None
        self.books: List[Book] = []
        self.results = None
        _or_ = []
        _and_ = []
        if initial_term is not None:
            _and_.append(initial_term)
            _or_.append(_and_)
        self.be_compiled = None
        self.terms = _or_
        self.changed = 1
        self.max_results = -1
        self.primary_sort = QuerySort()
        self.secondary_sort = QuerySort()
        self.tertiary_sort = QuerySort()
        self.primary_sort.param_list = [QUERY_DEFAULT_SORT]
        self.primary_sort.increasing = True
        self.secondary_sort.increasing = True
        self.tertiary_sort.increasing = True

    def swap_terms(self, other):
        if self is None or other is None:
            return
        g = self.terms
        self.terms = other.terms
        other.terms = g
        g = self.books
        self.books = other.books
        other.books = g
        self.changed = 1
        other.changed = 1

    @staticmethod
    def copy_query_term(qt: QueryTerm):
        if qt is None: return
        new_qt = QueryTerm()
        new_qt.param_list = qt.param_list.copy()
        new_qt.param_fcns = qt.param_fcns.copy()
        new_qt.pdata = QueryCore.predicate_copy(qt.pdata)
        return new_qt

    @staticmethod
    def copy_and_terms(and_terms):
        _and_ = []
        for qt in and_terms:
            _and_.append(QueryPrivate.copy_query_term(qt))
        return _and_

    @staticmethod
    def copy_or_terms(or_terms):
        _or_ = []
        for qt in or_terms:
            _or_.append(QueryPrivate.copy_and_terms(qt))
        return _or_

    @staticmethod
    def copy_sort(dst: QuerySort, src: QuerySort):
        dst.param_list = src.param_list.copy() if src.param_list is not None else []
        dst.param_fcns = src.param_fcns.copy() if src.param_fcns is not None else []

    def free_members(self):
        self.terms = None
        self.books = None
        self.results = None

    @staticmethod
    def cmp_func(sort, default_sort, a, b) -> int:
        conva = a
        convb = b
        param = None
        if sort is None: return 0
        if sort.use_default:
            if default_sort: return default_sort(a, b)
            return 0
        if sort.param_fcns is None: return 0
        if sort.comp_fcn is None and sort.obj_cmp is None: return 0
        x = len(sort.param_fcns) - 1
        for i, node in enumerate(sort.param_fcns):
            param = node
            if i == x and not sort.obj_cmp:
                break
            conva = param.getfcn(conva)
            convb = param.getfcn(convb)
        if sort.comp_fcn:
            return sort.comp_fcn(conva, convb, sort.options, param)
        return sort.obj_cmp(conva, convb)

    def sort_func(self, a, b):
        if self is None: return 0
        retval = QueryPrivate.cmp_func(self.primary_sort, self.defaultSort, a, b)
        if retval == 0:
            retval = QueryPrivate.cmp_func(self.secondary_sort, self.defaultSort, a, b)
            if retval == 0:
                retval = QueryPrivate.cmp_func(self.tertiary_sort, self.defaultSort, a, b)
                return retval if self.tertiary_sort.increasing else -retval
            else:
                return retval if self.secondary_sort.increasing else -retval
        else:
            return retval if self.primary_sort.increasing else -retval

    def check_object(self, obj):
        for or_ptr in self.terms:
            and_terms_ok = 1
            for qt in or_ptr:
                if qt.pred_fcn is not None:
                    conv_obj = obj
                    for p in qt.param_fcns[:len(qt.param_fcns) - 1]:
                        conv_obj = p.getfcn(conv_obj)
                        if conv_obj is None:
                            break
                    param = qt.param_fcns[-1]
                    if conv_obj is None or qt.pred_fcn(conv_obj, param, qt.pdata) == qt.invert:
                        and_terms_ok = 0
                        break
            if and_terms_ok:
                return 1

        return 0

    @staticmethod
    def compile_params(param_list, start_obj):
        final = None
        fcns = []
        if param_list is None or len(param_list) == 0 or start_obj is None:
            return None, None
        for param_name in param_list:
            final = ObjectClass.get_parameter(start_obj, param_name)
            if final is None:
                break
            fcns.append(final)
            start_obj = final.type
        return final, fcns

    @staticmethod
    def compile_sort(sort, obj):
        sort.use_default = False
        sort.param_fcns = None
        sort.comp_fcn = None
        sort.obj_cmp = None
        if sort.param_list is None:
            return
        resObj, sort.param_fcns = QueryPrivate.compile_params(sort.param_list, obj)
        if sort.param_fcns and resObj:
            if resObj.compfcn is not None:
                sort.comp_fcn = resObj.compfcn
            else:
                sort.comp_fcn = QueryCore.get_compare(resObj.type)
            if sort.comp_fcn is None:
                sort.obj_cmp = ObjectClass.get_default_sort(resObj.type)

        elif len(sort.param_list) > 0 and sort.param_list[0] == QUERY_DEFAULT_SORT:
            sort.use_default = True

    def compile_terms(self):
        for or_ptr in self.terms:
            for qt in or_ptr:
                resObj, qt.param_fcns = self.compile_params(qt.param_list, self.search_for_)
                if qt.param_fcns and resObj:
                    qt.pred_fcn = QueryCore.get_predicate(resObj.type)
                else:
                    qt.pred_fcn = None
        self.compile_sort(self.primary_sort, self.search_for_)
        self.compile_sort(self.secondary_sort, self.search_for_)
        self.compile_sort(self.tertiary_sort, self.search_for_)
        self.defaultSort = ObjectClass.get_default_sort(self.search_for_)
        for book in self.books:
            be = book.get_backend()
            if be is not None:
                result = be.compile_query(self)
                if result:
                    self.be_compiled[book] = result

    @staticmethod
    def merge_books(l1: List[Book], l2: List[Book]):
        if len(l2) == 0:
            return l1
        elif len(l1) == 0:
            return l2
        l1.extend(l2)
        l2.clear()
        return list(set(l1))

    @staticmethod
    def free_compiled(book, value):
        be = book.backend
        if be is not None and be.free_query:
            be.free_query(be, value)
        return True

    def clear_compiles(self):
        if self.be_compiled is None: return
        for k, v in self.be_compiled.items():
            QueryPrivate.free_compiled(k, v)

    def add_term(self, param_list: list, pred_data: QueryPredData, op: QueryOp):
        if param_list is None or pred_data is None or len(param_list) == 0: return
        qt = QueryTerm()
        qt.param_list = param_list
        qt.pdata = pred_data
        qs = QueryPrivate(qt)
        if self.has_terms():
            qr = self.merge(qs, op)
        else:
            qr = self.merge(qs, QueryOp.OR)
        self.swap_terms(qr)
        qs.destroy()
        qr.destroy()

    def purge_terms(self, param_list):
        if not isinstance(param_list, (list, tuple, set)) or len(param_list) == 0: return
        for ot in self.terms:
            for qt in ot:
                if not param_list_cmp(qt.param_list, param_list):
                    if len(ot) == 1:
                        self.terms.remove(ot)
                        ot.clear()
                        _or_ = self.terms
                        break
                    else:
                        ot.remove(qt)
                    self.changed = 1

    def run_internal(self, yielder, run_cb):
        if self.search_for_ is None:
            return
        if self.books is None or len(self.books) == 0:
            raise ValueError("No Book:PLEASE SET A BOOK TO SEARCH FROM")
        if run_cb is None:
            return
        self.results = None
        if self.changed:
            self.compile_terms()
        if yielder:
            return run_cb()
        matching_objects = list(run_cb())
        object_count = len(matching_objects)
        if self.primary_sort.comp_fcn or self.primary_sort.obj_cmp or (
                self.primary_sort.use_default and self.defaultSort):
            matching_objects.sort(key=cmp_to_key(self.sort_func))
        if object_count > self.max_results > -1:
            if self.max_results > 0:
                matching_objects = matching_objects[object_count - self.max_results:]
            else:
                matching_objects = []
        self.changed = 0
        self.results = matching_objects
        return matching_objects

    def run_cb(self):
        results = []
        for f in map(lambda b: filter(self.check_object, b.get_collection(self.search_for_).values()), self.books):
            results.extend(f)
        return results

    def run(self, yielder=False):
        return self.run_internal(yielder, self.run_cb)

    def run_subq_cb(self):
        return filter(self.check_object, self.last_run())

    @staticmethod
    def run_subquery(subq, primaryq):
        if subq is None or primaryq is None: return
        if subq.search_for_ is None or primaryq.search_for_ is None: return
        if subq.search_for_ == primaryq.search_for_: return
        return subq.run_internal(False, primaryq.run_subq_cb)

    def last_run(self):
        return self.results

    def clear(self):
        q2 = QueryPrivate.create()
        self.swap_terms(q2)
        q2.destroy()
        self.books.clear()
        self.books = None
        self.results.clear()
        self.results = None
        self.changed = 1

    @classmethod
    def create(cls):
        self = cls()
        self.be_compiled = {}
        return self

    def search_for(self, obj_type):
        if obj_type is None: return
        if self.search_for_ != obj_type:
            self.search_for_ = obj_type
            self.changed = 1

    @classmethod
    def create_for(cls, obj_type):
        if obj_type is None: return
        q = cls.create()
        q.search_for(obj_type)
        return q

    def has_terms(self):
        return len(self.terms)

    def num_terms(self):
        return sum(map(len, self.terms))

    def has_term_type(self, term_param):
        if term_param is None:
            return False
        for ot in self.terms:
            for qt in ot:
                if not param_list_cmp(term_param, qt.param_list):
                    return True
        return False

    def get_term_type(self, term_param):
        r = []
        if term_param is None:
            return False
        for ot in self.terms:
            for qt in ot:
                if not param_list_cmp(term_param, qt.param_list):
                    r.append(qt.pdata)
        return r

    def destroy(self):
        if self is None: return
        self.free_members()
        self.clear_compiles()
        self.be_compiled = None

    def copy(self):
        cpy = super(QueryPrivate, self.__class__).__new__(self.__class__)
        cpy.__init__()
        ht = cpy.be_compiled
        cpy.search_for_ = self.search_for_
        cpy.max_results = self.max_results
        cpy.free_members()
        cpy.be_compiled = ht
        cpy.terms = self.copy_or_terms(self.terms)
        cpy.books = self.books.copy()
        if self.results is not None:
            cpy.results = self.results.copy()
        self.copy_sort(cpy.primary_sort, self.primary_sort)
        self.copy_sort(cpy.secondary_sort, self.secondary_sort)
        self.copy_sort(cpy.tertiary_sort, self.tertiary_sort)
        cpy.changed = 1
        return cpy

    def invert(self):
        num_or_terms = len(self.terms)
        if num_or_terms == 0:
            retval = QueryPrivate.create()
            retval.max_results = self.max_results
        elif num_or_terms == 1:
            retval = QueryPrivate.create()
            retval.max_results = self.max_results
            retval.books = self.books.copy()
            retval.search_for_ = self.search_for_
            retval.changed = 1
            for cur in self.terms[0]:
                qt = self.copy_query_term(cur)
                qt.invert = not qt.invert
                new_oterm = [qt]
                retval.terms.append(new_oterm)
        else:
            right = QueryPrivate.create()
            right.terms = [self.copy_or_terms(self.terms[1])]

            left = QueryPrivate.create()
            left.terms = [self.copy_and_terms(self.terms[0])]
            iright = right.invert()
            ileft = left.invert()

            retval = iright.merge(ileft, QueryOp.AND)
            retval.books = self.books.copy()
            retval.max_results = self.max_results
            retval.search_for_ = self.search_for_
            retval.changed = 1
            iright.destroy()
            ileft.destroy()
            right.destroy()
            left.destroy()

        return retval

    def merge(self, other, op):
        if self is None: return other
        if other is None: return self
        if self.search_for_ is not None and other.search_for_ is not None:
            if self.search_for_ != other.search_for_: return None
        search_for = self.search_for_ if self.search_for_ is not None else other.search_for_
        if QueryOp.AND == op and ((0 == self.has_terms()) or (0 == other.has_terms())):
            op = QueryOp.OR
        retval = QueryPrivate.create()
        if op == QueryOp.OR:
            retval.terms = self.copy_or_terms(self.terms) + self.copy_or_terms(other.terms)
            retval.books = self.merge_books(self.books, other.books)
            retval.max_results = self.max_results
            retval.changed = 1
        elif op == QueryOp.AND:
            retval.books = self.merge_books(self.books, other.books)
            retval.max_results = self.max_results
            retval.changed = 1
            for i in self.terms:
                for j in other.terms:
                    retval.terms.append(self.copy_and_terms(i) + self.copy_and_terms(j))
        elif op == QueryOp.NAND:
            i1 = self.invert()
            i2 = other.invert()
            retval = i1.merge(i2, QueryOp.OR)
            i1.destroy()
            i2.destroy()

        elif op == QueryOp.NOR:
            i1 = self.invert()
            i2 = other.invert()
            retval = i1.merge(i2, QueryOp.AND)
            i1.destroy()
            i2.destroy()
        elif op == QueryOp.XOR:
            i1 = self.invert()
            i2 = other.invert()
            t1 = self.merge(i2, QueryOp.AND)
            t2 = i1.merge(other, QueryOp.AND)
            retval = t1.merge(t2, QueryOp.OR)

            i1.destroy()
            i2.destroy()
            t1.destroy()
            t2.destroy()
        retval.search_for_ = search_for
        return retval

    def merge_in_place(self, other, op):
        if self is None or other is None: return
        tmp_q = self.merge(other, op)
        self.swap_terms(tmp_q)
        tmp_q.destroy()

    def set_sort_order(self, params1, params2, params3):
        self.primary_sort.param_list = params1
        self.primary_sort.options = 0
        self.secondary_sort.param_list = params2
        self.secondary_sort.options = 0
        self.tertiary_sort.param_list = params3
        self.tertiary_sort.options = 0
        self.changed = 1

    def set_sort_options(self, prim_op: int = 0, sec_op: int = 0, tert_op: int = 0):
        self.primary_sort.options = prim_op
        self.secondary_sort.options = sec_op
        self.tertiary_sort.options = tert_op

    def set_sort_increasing(self, prim_inc: bool = False, sec_inc: bool = False, tert_inc: bool = False):
        self.primary_sort.increasing = prim_inc
        self.secondary_sort.increasing = sec_inc
        self.tertiary_sort.increasing = tert_inc

    def set_max_results(self, n: int):
        self.max_results = int(n)

    def add_guid_list_match(self, param_list: List[str], guid_list: List[UUID], options: GuidMatch, op: QueryOp):
        assert param_list is not None or len(param_list) > 0, "Param List absent"
        if not any(guid_list):
            if options != GuidMatch.NONE:
                return
        self.add_term(param_list, QueryCore.guid_predicate(options, guid_list), op)

    def add_guid_match(self, param_list: List[str], guid: UUID, op: QueryOp):
        assert param_list is not None or len(param_list) > 0, "Param List absent"
        self.add_guid_list_match(param_list, [guid], GuidMatch.ANY, op)

    def set_book(self, book):
        assert book is not None, "book value cannot be None"
        if book not in self.books:
            self.books.append(book)
        QueryPrivate.add_guid_match(self, [PARAM_BOOK, PARAM_GUID], book.get_guid(), QueryOp.AND)

    def get_books(self):
        return self.books

    def add_boolean_match(self, param_list, value: bool, op: QueryOp):
        self.add_term(param_list, QueryCore.boolean_predicate(QueryCompare.EQUAL, value), op)

    @staticmethod
    def init():
        QueryCore.init()
        ObjectClass.init()

    @staticmethod
    def shutdown():
        ObjectClass.shutdown()
        QueryCore.shutdown()

    def get_max_results(self):
        return self.max_results

    def get_search_for(self):
        return self.search_for_

    def get_terms(self):
        return self.terms

    def get_sorts(self):
        return self.primary_sort, self.secondary_sort, self.tertiary_sort

    def __eq__(self, other):
        if self is None or other is None: return False
        if len(self.terms) != len(other.terms): return False
        if self.max_results != other.max_results: return False

        for i, or1 in enumerate(self.terms):
            or2 = other.terms[i]
            and1 = or1[0]
            and2 = or2[0]
            if len(and1) != len(and2): return False
            for it, qt1 in and1:
                qt2 = and2[it]
                if qt1 != qt2:
                    return False
        if self.primary_sort != other.primary_sort: return False
        if self.secondary_sort != other.secondary_sort: return False
        if self.tertiary_sort != other.tertiary_sort: return False
        return True

    def print_terms(self):
        output = []
        terms = self.get_terms()
        for lst in terms:
            if len(lst):
                output += [str(s) for s in lst]
        return "".join(output)

    def print_sorts(self):
        gs = "Sort Parameters: "
        for sor in self.get_sorts():
            increasing = sor.get_increasing()
            gsl = sor.get_param_path()
            if gsl:
                gs += " Param:("
                t = " ".join(gsl)
                gs += t
            if gsl:
                gs += " DESC " if increasing else "ASC"
                gs += " Options: {})".format(sor.options)
        return gs

    def __repr__(self):
        return u"<SELECT {} WHERE {} LIMIT {}]".format(self.get_search_for(), self.print_terms(),
                                                       self.print_sorts(),
                                                       self.get_max_results())
