import datetime
import uuid
from abc import abstractmethod
from dataclasses import dataclass

from sqlalchemy import Column, BOOLEAN, TIMESTAMP, JSON, MetaData
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import object_session, as_declarative
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy_utils import UUIDType

from .instance import Instance

ID_ACCOUNT = "Account"
ID_COMMODITY = "Commodity"
ID_COMMODITY_NAMESPACE = "CommodityNamespace"
ID_COMMODITY_TABLE = "CommodityTable"
ID_LOT = "Lot"
ID_PERIOD = "Period"
ID_PRICE = "Price"
ID_PRICEDB = "PriceDB"
ID_SPLIT = "Split"
ID_BUDGET = "Budget"
ID_SCHEDXACTION = "SchedXaction"
ID_SXES = "ScheduledTransactionList"
ID_SXTG = "SXTGroup"
ID_SXTT = "SXTTrans"
ID_TRANS = "Trans"
ID_FUEL = "Fuel"
ID_POS = "Pos"
ID_DIPS = "Dip"
ID_PRODUCT = "Product"
ID_PRODUCT_LOCATION = "ProductLocation"
ID_PUMP = "Pump"
ID_TANK = "Tank"
ID_NOZZLE = "Nozzle"
ID_ROOT_ACCOUNT = "RootAccount"
ID_TERM = "Term"
ID_CONTACT = "Contact"
ID_ADDRESS = "Address"
ID_EMAIL = "Email"
ID_PHONE = "Phone"
ID_TAXTABLE = "Tax"
ID_INVOICE = "Invoice"
ID_BILL = "Bill"
ID_ORDER = "Order"
ID_BILL_ENTRY = "BillEntry"
ID_BILL_ACCOUNT_ENTRY = "BillAccountEntry"
ID_INVOICE_ENTRY = "InvoiceEntry"
ID_SUPPLIER = "Supplier"
ID_CUSTOMER = "Customer"
ID_STAFF = "Staff"
ID_LOCATION = "Location"
ID_INVENTORY = "Inventory"
ID_PRODUCT_CATEGORY = "ProductCategory"
ID_PRODUCT_UNIT = "UnitOfMeasure"
ID_UNIT_OF_MEASURE_GROUP = "UnitOfMeasureGroup"
ID_PAYMENT_METHOD = "PaymentMethod"
ID_JOB = "Job"
ID_BOOK = "Book"
ID_SESSION = "Session"


class ConversionError(Exception):
    pass


class ImageMixin:
    @abstractmethod
    def get_image_url(self):
        pass

    @abstractmethod
    def set_image_url(self, url):
        pass

    @abstractmethod
    def get_full_name(self):
        pass


metadata = MetaData()


@as_declarative(metadata=metadata)
class DeclarativeBase:
    pass


@dataclass
class DeclarativeBaseGuid(DeclarativeBase, Instance):
    __abstract__ = True
    __table_args__ = {'implicit_returning': False}

    #: the unique identifier of the object
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True, nullable=False)

    @declared_attr
    def active(cls):
        return Column("active", BOOLEAN(), nullable=False, default=True)

    @declared_attr
    def deleted(cls):
        return Column("deleted", BOOLEAN(), nullable=False, default=False)

    @declared_attr
    def last_updated_time(cls):
        return Column('last_modified', TIMESTAMP, onupdate=datetime.datetime.utcnow)

    @declared_attr
    def create_time(cls):
        return Column('created_on', TIMESTAMP, nullable=False, default=datetime.datetime.utcnow)

    @declared_attr
    def custom_data(cls):
        return Column('custom_data', JSON)

    def __init__(self, *args, **kwargs):
        Instance.__init__(self, *args, **kwargs)

    def __contains__(self, key):
        if self.custom_data is None:
            return False
        keys = key.split("/")
        if not any(keys):
            return False
        loop_data = self.custom_data.copy()
        for k in keys:
            loop_data = loop_data.get(k)
            if loop_data is None:
                return False
        return True

    def __getitem__(self, key):
        if self.custom_data is None:
            return
        assert not isinstance(key, int), \
            "You are accessing custom_data with an integer (={}) while a string is expected".format(key)
        keys = key.split("/")
        if not any(keys):
            return
        loop_data = self.custom_data.copy()
        for k in keys:
            loop_data = loop_data.get(k)
            if loop_data is None:
                return
        return loop_data

    def __setitem__(self, key, value):
        keys = key.split("/")
        if self.custom_data is None:
            self.custom_data = {}
        loop_data = self.custom_data
        while len(keys) > 0:
            key_i = keys.pop(0)
            if len(keys) > 0:
                if loop_data.get(key_i) is None:
                    loop_data[key_i] = {}

                loop_data = loop_data[key_i]
            else:
                loop_data[key_i] = value
        flag_modified(self, "custom_data")

    def __delitem__(self, key):
        if isinstance(key, slice):
            # delete all
            del self.custom_data[key]
            return
        keys = key.split("/", 1)
        pass

    def __iter__(self):
        for key, val in self.custom_data.items():
            yield key, val

    def get(self, key, default=None):
        try:
            return self[key].value
        except (KeyError, AttributeError):
            return default

    def get_guid(self):
        return self.guid

    def get_resident(self):
        return True

    def dispose(self):
        sess = object_session(self)
        if sess is not None:
            sess.expunge(self)
        Instance.dispose(self)

    def guid_compare(self, other):
        if self.guid < other.guid:
            return -1
        if self.guid == other.guid: return 0
        return 1

    def destroy(self):
        self.edit_level = 1
        self.set_destroying(True)
        self.set_dirty()
        self.commit_edit()

    def is_open(self):
        return self.edit_level > 0

    @classmethod
    def lookup_type(cls, type_name):
        for a in cls.__subclasses__():
            if a.e_type == type_name:
                return a

    @classmethod
    def get_col(cls, type_name, book):
        if book is None or type_name is None: return
        if cls.lookup(type_name) is None:
            return
        return book.get_collection(type_name)

    def __hash__(self):
        return id(self)
