from .strategy import *


class ItemImportStrategy(ImportStrategy):
    __type_name__ = "Item"

    @staticmethod
    def get_column_names() -> List[str]:
        return ["NAME", "DESCRIPTION", "SKU", "CATEGORY", "TYPE"]

    @staticmethod
    def validate(dataframe, book) -> List[int]:
        pass

    @staticmethod
    def save_data(data_frame, book):
        pass
