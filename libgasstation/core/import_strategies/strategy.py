from abc import ABC, abstractmethod, abstractproperty
from typing import List, final


class ImportStrategy(ABC):
    __type_name__: abstractproperty = None

    @staticmethod
    @abstractmethod
    def get_column_names() -> List[str]:
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def validate(dataframe, book) -> List[int]:
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def save_data(data_frame, book):
        raise NotImplementedError

    @final
    @classmethod
    def get_all(cls) -> List:
        return cls.__subclasses__()

