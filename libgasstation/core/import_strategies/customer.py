from .strategy import *


class CustomerImportStrategy(ImportStrategy):
    __type_name__ = "Customer"

    @staticmethod
    def get_column_names() -> List[str]:
        return ["NAME", "DISPLAY NAME", "COMPANY", "EMAIL", "PHONE", "FAX"]

    @staticmethod
    def validate(dataframe, book) -> List[int]:
        pass

    @staticmethod
    def save_data(data_frame, book):
        pass
