import uuid
from collections import defaultdict
from datetime import datetime, timedelta

from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship, reconstructor
from sqlalchemy_utils import UUIDType

from ._declbase import DeclarativeBaseGuid, ID_BOOK, ID_ROOT_ACCOUNT
from .collection import Collection
from .event import *
from .object import Param, TYPE_GUID, ObjectClass
from .policy import Policy

KVP_OPTION_PATH = "options"
OPTION_SECTION_ACCOUNTS = "Accounts"
OPTION_NAME_TRADING_ACCOUNTS = "Use Trading Accounts"
OPTION_NAME_CURRENCY_ACCOUNTING = "Currency Accounting"
OPTION_NAME_BOOK_CURRENCY = "Book Currency"
OPTION_NAME_DEFAULT_GAINS_POLICY = "Default Gains Policy"
OPTION_NAME_DEFAULT_GAINS_LOSS_ACCT_GUID = "Default Gain or Loss Account"
OPTION_NAME_AUTO_READONLY_DAYS = "Day Threshold for Read-Only Transactions (red line)"
OPTION_NAME_NUM_FIELD_SOURCE = "Use Split Action Field for Number"

OPTION_SECTION_BUDGETING = "Budgeting"
OPTION_NAME_DEFAULT_BUDGET = "Default Budget"
PARAM_NAME_NUM_FIELD_SOURCE = "split-action-num-field"
PARAM_NAME_NUM_AUTOREAD_ONLY = "auto_readonly-days"


class Book(DeclarativeBaseGuid):
    __tablename__ = 'book'
    guid = Column('guid', UUIDType(binary=False), primary_key=True,
                  nullable=False, default=lambda: uuid.uuid4())
    # column definitions
    root_account_guid = Column('root_account_guid', UUIDType(binary=False),
                               ForeignKey('account.guid'), nullable=False)
    root_template_guid = Column('root_template_guid', UUIDType(binary=False),
                                ForeignKey('account.guid'), nullable=False)
    root_account = relationship('Account',
                                # back_populates='root_book',
                                foreign_keys=[root_account_guid],
                                )
    root_template = relationship('Account',
                                 foreign_keys=[root_template_guid])
    uri = ""
    # link options to KVP
    e_type = ID_BOOK

    def __init__(self):
        self.data_tables = {}
        self.hash_of_collections = {}
        super().__init__(ID_BOOK, self)
        self.uri = None
        self.backend = None
        self.read_only = False
        self.book_open = 'y'
        self.session_dirty = False
        self.dirty_time = None
        self.dirty_cb = None
        self.dirty_data = None
        self.cached_num_days_auto_readonly_isvalid = False
        self.cached_num_days_auto_readonly = 1
        self.cached_num_field_source_isvalid = False
        self.cached_num_field_source = False
        self.version = 0
        self.shutting_down = False
        self.data_finalizers = {}
        ObjectClass.book_begin(self)
        self.create_root()
        self.create_root_template()
        Event.gen(self, EVENT_CREATE)

    def create_root(self):

        from .account import Account, AccountType
        root = Account(self)
        root.begin_edit()
        root.set_name("Root Account")
        root.set_type(AccountType.ROOT)
        root.set_dirty()
        root.commit_edit()
        self.root_account = root

    def create_root_template(self):
        from .account import Account, AccountType
        root = Account(self)
        root.begin_edit()
        root.set_name("Template Root")
        root.set_type(AccountType.ROOT)
        root.set_dirty()
        root.commit_edit()
        self.root_template = root

    def get_data(self, key):
        if key is None or self.data_tables is None:
            return
        return self.data_tables.get(key)

    def set_data(self, key, data):
        if key is None or self.data_tables is None:
            return
        self.data_tables[key] = data

    def get_collection(self, entity_type: str) -> Collection:
        from .collection import Collection
        if entity_type is None or self.hash_of_collections is None:
            return
        col = self.hash_of_collections.get(entity_type)
        if col is None:
            col = Collection(entity_type)
            self.hash_of_collections[entity_type] = col
        return col

    def get_template_root(self):
        return self.root_template

    @reconstructor
    def init_on_load(self):
        self.cached_num_days_auto_readonly_isvalid = False
        self.cached_num_days_auto_readonly = 1
        self.cached_num_field_source_isvalid = False
        self.cached_num_field_source = False
        self.version = 0
        self.shutting_down = False
        self.data_tables = {}
        self.hash_of_collections = {}
        self.uri = None
        self.backend = None
        self.read_only = False
        self.book_open = 'y'
        self.session_dirty = False
        self.dirty_time = None
        self.dirty_cb = None
        self.dirty_data = None
        self.data_finalizers = defaultdict(list)
        self.infant = False
        ObjectClass.book_begin(self)
        super().__init__(ID_BOOK, self)
        Event.gen(self, EVENT_CREATE)

    def __repr__(self):
        return u"Book<{}>".format(self.backend.url if self.backend is not None else self.guid)

    def get_backend(self):
        return self.backend

    def set_backend(self, be):
        self.backend = be

    @property
    def query(self):
        return self.backend.query

    @property
    def session(self):
        return self.backend.sql_session

    def get_uri(self):
        return self.uri

    def set_uri(self, ur):
        self.uri = ur
        Event.gen(self, EVENT_MODIFY)

    def set_root_account(self, root):
        if root is None or root.get_book() != self:
            return
        col = self.get_collection(ID_ROOT_ACCOUNT)
        self.col_set_root_account(col, root)
        self.root_account = root
        Event.gen(self, EVENT_MODIFY)

    @staticmethod
    def col_set_root_account(col, root):
        if col is None: return
        old_root = col.get_data()
        if old_root == root: return
        if root.parent is not None:
            root.begin_edit()
            root.parent.remove_child(root)
            root.commit_edit()
        col.set_data(root)
        if old_root is not None:
            old_root.begin_edit()
            old_root.destroy()

    def set_template_account(self, rt):
        self.root_template = rt
        Event.gen(self, EVENT_MODIFY)

    def get_root_account(self):
        return self.root_account

    @property
    def default_currency(self):
        return self.root_account.commodity

    @default_currency.setter
    def default_currency(self, value):
        self.root_account.commodity = value
        Event.gen(self, EVENT_MODIFY)

    def set_data_fin(self, key, data, cb):
        if not key or key is None:
            return
        if not cb or cb is None:
            return
        self.data_tables[key] = data
        self.data_finalizers[key] = cb

    @property
    def is_saved(self):
        """Save the changes to the file/DB (=commit transaction)
        """
        return True

    def mark_read_only(self):
        self.read_only = True

    def mark_closed(self):
        self.book_open = "n"

    def mark_session_saved(self):
        self.dirty_time = 0
        if self.session_dirty:
            self.session_dirty = False
            if self.dirty_cb is not None:
                self.dirty_cb(self, False, self.dirty_data)

    def mark_session_dirty(self):
        if not self.session_dirty:
            self.session_dirty = True
            self.dirty_time = datetime.now()
            if self.dirty_cb is not None:
                self.dirty_cb(self, True, self.dirty_data)

    def get_session_dirty_time(self):
        return self.dirty_time or datetime.now()

    def is_empty(self):
        return self.root_account is None

    def session_not_saved(self):
        return not self.is_empty() and self.session_dirty

    def is_readonly(self):
        return self.read_only

    def set_dirty_cb(self, cb):
        self.dirty_cb = cb

    def is_shutting_down(self):
        return self.shutting_down

    def final(self, key, cb):
        user_data = self.data_tables.get(key)
        if user_data is not None:
            cb(self, key, user_data)

    def destroy(self):
        self.shutting_down = True
        Event.force(self, EVENT_DESTROY)
        ObjectClass.book_end(self)
        for k, v in self.data_finalizers.items():
            self.final(k, v)
        self.data_finalizers = None
        if self.data_tables is not None:
            self.data_tables.clear()
        self.data_tables = None
        if self.hash_of_collections is not None:
            self.hash_of_collections.clear()
        self.hash_of_collections = None

    def get_fiscal_year_end(self):
        return self.get_property("fy-end")

    def get_num_of_days_auto_readonly(self):
        if not self.cached_num_days_auto_readonly_isvalid:
            self.cached_num_days_auto_readonly = self.get_property("auto-readonly-days")
            self.cached_num_days_auto_readonly_isvalid = True
        return int(self.cached_num_days_auto_readonly)

    def uses_auto_readonly(self):
        return self.get_num_of_days_auto_readonly() != 0

    def get_auto_readonly_date(self):
        num_days = self.get_num_of_days_auto_readonly()
        if num_days > 0:
            td = datetime.now() - timedelta(days=num_days)
            return td
        return 0

    @staticmethod
    def list_to_option_path(pathlist):
        if pathlist[0].startswith("counter"):
            return "/".join(pathlist)
        pathlist.insert(0, KVP_OPTION_PATH)
        return "/".join(pathlist)

    def set_option(self, value, path):
        self.begin_edit()
        self[self.list_to_option_path(path)] = value
        self.set_dirty()
        self.commit_edit()
        self.cached_num_field_source_isvalid = False

    def get_option(self, path):
        return self[self.list_to_option_path(path)]

    def delete_option(self, *path):
        self.begin_edit()
        del self[self.list_to_option_path(list(path))]
        self.set_dirty()
        self.commit_edit()

    def delete_options(self, *paths):
        try:
            if any(paths):
                del self[self.list_to_option_path(list(paths))]
            else:
                del self[KVP_OPTION_PATH]
        except KeyError:
            pass

    def get_counter(self, counter_name):
        if counter_name is None or counter_name == "":
            return -1
        value = self.get_option(["counters", counter_name])
        if value is not None:
            return value
        return 0

    def get_counter_format(self, counter_name):
        if counter_name is None or counter_name == "":
            return "{:0>5}"
        value = self.get_option(["counter_formats", counter_name])
        if value is not None:
            return value
        return "{:0>5}"

    def format_counter(self, counter_name):
        if counter_name is None or counter_name == "":
            raise ValueError("Invalid counter name")
        counter = self.get_counter(counter_name)
        if counter < 0:
            return None
        counter += 1
        f = self.get_counter_format(counter_name)
        if f is None:
            return
        try:
            if f.find("{") == -1 or f.find("}") == -1:
                f = "{}"
            return f.format(int(counter))
        except ValueError:
            return str(counter)

    def increment_and_format_counter(self, counter_name):
        if counter_name is None or counter_name == "":
            raise ValueError("Invalid counter name")
        counter = self.get_counter(counter_name)
        if counter < 0:
            return None
        counter += 1
        self.set_option(int(counter), ["counters", counter_name])
        f = self.get_counter_format(counter_name)
        if f is None:
            return
        try:
            if f.find("{") == -1:
                f = f + "{}"
            return f.format(int(counter))
        except ValueError:
            return str(counter)

    def use_split_action_for_num_field(self):
        if not self.cached_num_field_source_isvalid:
            act = self.get_property("num-field-source")
            if not isinstance(act, bool):
                return False
            self.cached_num_field_source = act
            self.cached_num_field_source_isvalid = True
        return self.cached_num_field_source

    def use_trading_accounts(self):
        act = self.get_property("trading-accounts")
        if not isinstance(act, bool):
            return False
        return act

    def get_currency_name(self):
        if self.use_currency():
            return self.get_property("book-currency")

    def get_default_gains_policy(self):
        if self.use_currency():
            return self.get_property("default-gains-policy")

    def get_default_gain_loss_acct_guid(self):
        return self.get_property("default-gain-loss-account-guid")

    def get_default_gain_loss_account(self):
        from .account import Account, AccountType
        from .commodity import Commodity
        gains_account = None
        if self.use_currency():
            guid = self.get_default_gain_loss_acct_guid()
            gains_account = Account.lookup(guid, self)
        if gains_account is not None and not gains_account.get_placeholder() and not gains_account.get_hidden() and \
                Commodity.equiv(gains_account.get_commodity(), self.get_currency()) and \
                gains_account.get_type() in [AccountType.INCOME, AccountType.EXPENSE]:
            return gains_account
        return None

    def commit_edit(self):
        if not super().commit_edit():
            return
        self.commit_edit_part2(None, None, None)

    @classmethod
    def register(cls):

        from libgasstation.core.query_private import PARAM_GUID
        params = [Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_BOOK, None, params)
        return True

    def get_property(self, name):
        if name == "trading-accounts":
            return self.get("/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_TRADING_ACCOUNTS]), False)

        elif name == "book-currency":
            return self.get("/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_BOOK_CURRENCY]))

        elif name == "default-gains-policy":
            return self.get("/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_DEFAULT_GAINS_POLICY]))

        elif name == "default-gains-account-guid":
            return self.get(
                "/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_DEFAULT_GAINS_LOSS_ACCT_GUID]))

        elif name == "auto-readonly-days":
            return self.get("/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_AUTO_READONLY_DAYS]), 0)

        elif name == "num-field-source":
            return self.get("/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_NUM_FIELD_SOURCE]))

        elif name == "default-budget":
            return self.get("/".join([KVP_OPTION_PATH, OPTION_SECTION_BUDGETING, OPTION_NAME_DEFAULT_BUDGET]))

        elif name == "fy-end":
            return self.get("fy_end")

        elif name == "ab-templates":
            return self.get("AB_KEY/AB_TEMPLATES")

    def set_property(self, name, value):
        if name == "trading-accounts":
            self["/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_TRADING_ACCOUNTS])] = value

        elif name == "book-currency":
            self["/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_BOOK_CURRENCY])] = value

        elif name == "default-gains-policy":
            self["/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_DEFAULT_GAINS_POLICY])] = value

        elif name == "default-gains-account-guid":
            self[
                "/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_DEFAULT_GAINS_LOSS_ACCT_GUID])] = value

        elif name == "auto-readonly-days":
            self["/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_AUTO_READONLY_DAYS])] = value

        elif name == "num-field-source":
            self["/".join([KVP_OPTION_PATH, OPTION_SECTION_ACCOUNTS, OPTION_NAME_NUM_FIELD_SOURCE])] = value

        elif name == "default-budget":
            self["/".join([KVP_OPTION_PATH, OPTION_SECTION_BUDGETING, OPTION_NAME_DEFAULT_BUDGET])] = value

        elif name == "fy-end":
            self["fy_end"] = value

        elif name == "ab-templates":
            self["AB_KEY/AB_TEMPLATES"] = value

    def use_currency(self):
        from .commodity import CommodityTable, COMMODITY_NAMESPACE_NAME_CURRENCY
        policy = self.get_property("default-gains-policy")
        currency = self.get_property("book-currency")
        if policy is None or currency is None:
            return False
        if not Policy.valid_name(policy) or not CommodityTable.lookup(CommodityTable.get_table(self),
                                                                      COMMODITY_NAMESPACE_NAME_CURRENCY, currency):
            return False
        if self.use_trading_accounts():
            return False
        return True

    def get_currency(self):
        from .commodity import CommodityTable, COMMODITY_NAMESPACE_NAME_CURRENCY
        if self.use_currency():
            return CommodityTable.lookup(CommodityTable.get_table(self), COMMODITY_NAMESPACE_NAME_CURRENCY,
                                         self.get_property("book-currency"))

    def option_num_field_source_changed_cb(self, val):
        self.cached_num_field_source_isvalid = False

    def option_num_auto_readonly_changed_cb(self, val):
        self.cached_num_days_auto_readonly_isvalid = False

    def foreach_collection(self, cb, *args):
        if self.hash_of_collections is None or cb is None:
            return False
        for col in self.hash_of_collections.values():
            cb(col, *args)
