from collections import defaultdict

from sqlalchemy import DECIMAL, Date, ForeignKey
from sqlalchemy.orm import reconstructor, relationship

from ._declbase import *
from .event import *
from .query_private import *

DIPS_DATE = "dips-date"
DIPS_TANK = "dips-tank"
DIPS_LOCATION = "dips-location"


class Dip(DeclarativeBaseGuid):
    __tablename__ = 'dip'

    __table_args__ = {}
    date = Column('date', Date(), nullable=False)
    tank_guid = Column('tank_id', UUIDType(binary=False), ForeignKey('tank.guid'), nullable=False)
    location_guid = Column('location_id', UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    opening = Column('opening', DECIMAL(10, 2), nullable=False)
    closing = Column('closing', DECIMAL(10, 2), nullable=False)
    tank = relationship('Tank', lazy="joined", back_populates='dips')
    location = relationship('Location')
    meters = defaultdict(dict)

    def __init__(self, book):
        self.date = datetime.date.today()
        self.opening = Decimal(0)
        self.closing = Decimal(0)
        self.meter_sales = Decimal(0)
        super().__init__(ID_DIPS, book)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from ..core.session import Session
        book = Session.get_current_book()
        super().__init__(ID_DIPS, book)
        Event.gen(self, EVENT_CREATE)
        self.infant = False
        self.meter_sales = Decimal(0)

    def set_date(self, d):
        if d and d != self.date:
            self.begin_edit()
            self.date = d
            self.set_dirty()
            self.commit_edit()

    def get_date(self):
        return self.date

    def get_tank_name(self):
        return self.tank.get_name() if self.tank is not None else ""

    def set_opening(self, value):
        if value is not None and value != self.opening:
            self.begin_edit()
            self.opening = value
            self.set_dirty()
            self.commit_edit()

    def set_closing(self, value):
        if value is not None and value != self.closing:
            self.begin_edit()
            self.closing = value
            self.set_dirty()
            self.commit_edit()

    def get_opening(self):
        return self.opening

    def get_closing(self):
        return self.closing

    def get_tank_sales(self):
        return self.opening - self.closing

    @classmethod
    def set_meter_sales(cls, fuel, remove: bool = False):
        nozzle = fuel.get_nozzle()
        if nozzle is not None:
            tank = nozzle.get_tank()
            if tank is not None:
                if remove:
                    cls.meters[tank].pop(nozzle)
                else:
                    cls.meters[tank][nozzle] = fuel

    @classmethod
    def from_fuel(cls, fuel):
        nozzle = fuel.get_nozzle()
        if nozzle is not None:
            tank = nozzle.get_tank()
            if tank is not None:
                return list(filter(
                    lambda t: t.get_date() == fuel.get_date(),
                    tank.get_dips()))

    def get_meter_sales(self):
        if self.tank is not None:
            d = self.meters.get(self.tank)
            if d is not None:
                return sum(map(lambda a: a.get_litres(), d.values()))
        return Decimal(0)

    def get_stock_loss(self):
        return self.get_meter_sales() - self.get_tank_sales()

    def set_tank(self, ta):
        if ta is not None and ta != self.tank:
            self.begin_edit()
            self.tank = ta
            self.set_dirty()
            self.commit_edit()

    def get_tank(self):
        return self.tank

    def begin_edit(self):
        if not super().begin_edit():
            return False

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def get_location(self):
        return self.location

    def set_location(self, value):
        if value != self.location:
            self.begin_edit()
            self.location = value
            self.set_dirty()
            self.commit_edit()

    @classmethod
    def register(cls):
        params = [Param(DIPS_DATE, TYPE_DATE, cls.get_date, cls.set_date),
                  Param(DIPS_TANK, ID_TANK, cls.get_tank, cls.set_tank),
                  Param(DIPS_LOCATION, ID_LOCATION, cls.get_location, cls.set_location),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_DIPS, None, params)

    def __repr__(self):
        return u"<Dip (%s) %s %s>" % (
            self.tank.get_name() if self.tank is not None else "Tank#", self.opening, self.closing)
