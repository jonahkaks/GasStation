from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError, NoSuchModuleError, InterfaceError
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists
from sqlalchemy_utils.functions import create_database, drop_database

from libgasstation.core.uri import Uri
from .backend import *

PGSQL_DEFAULT_PORT = 5432


class PosgreSqlBackEnd(BackEnd):
    def __init__(self):
        super().__init__()

    def session_begin(self, session, m_book_id: Uri, mode):
        create = mode == SessionOpenMode.NEW_STORE or mode == SessionOpenMode.NEW_OVERITE
        force = mode == SessionOpenMode.NEW_OVERITE
        if m_book_id.port == 0:
            m_book_id.port = PGSQL_DEFAULT_PORT
        m_book_id.set_driver("psycopg2")
        m_book_id.set_scheme("postgresql")
        m_book_id = m_book_id.__to_string__(False)
        try:
            if not database_exists(m_book_id) and not create:
                self.push_error(FileError.FILE_NOT_FOUND, "Database doesn't exist")
                return
            elif create:
                if database_exists(m_book_id):
                    if force:
                        drop_database(m_book_id)
                    else:
                        self.push_error(BackEndError.STORE_EXISTS, "Database already exists")
                        return
                create_database(m_book_id)
            engine = create_engine(m_book_id, echo=False)
            self.sql_session = sessionmaker(bind=engine)(autoflush=False, autocommit=False,
                                                         expire_on_commit=False,
                                                         enable_baked_queries=True)
        except OperationalError as e:
            self.push_error(BackEndError.SERVER_ERR, e)
        except (NoSuchModuleError, ModuleNotFoundError) as e:
            self.push_error(BackEndError.NO_HANDLER, e)
        except InterfaceError as e:
            self.push_error(BackEndError.CANT_CONNECT, e)
        except ProgrammingError as e:
            self.push_error(BackEndError.CANT_CONNECT, e)
