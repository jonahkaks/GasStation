import os

from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils.functions import create_database

from libgasstation.core.uri import Uri
from .backend import *


class SqliteBackEnd(BackEnd):
    def __init__(self):
        super().__init__()

    def session_begin(self, session, new_uri: Uri, mode):
        create = mode == SessionOpenMode.NEW_STORE or mode == SessionOpenMode.NEW_OVERITE
        force = mode == SessionOpenMode.NEW_OVERITE
        filepath = new_uri.get_path()
        file_exists = os.path.isfile(filepath) and os.path.exists(filepath)

        if not create and not file_exists:
            self.push_error(FileError.FILE_NOT_FOUND, "Sqlite3 File %s does not exist" % new_uri)
            return
        if create and file_exists:
            if force:
                os.remove(filepath)
                file_exists = False
            else:
                self.push_error(BackEndError.STORE_EXISTS, "Might clobber, no force")
                return
        new_uri.set_scheme("sqlite")
        if not file_exists:
            try:
                create_database(str(new_uri))
            except OperationalError as e:
                self.push_error(BackEndError.CANT_CONNECT, e.args[0])
        # if not create:
        #     url = str(filepath)
        #     url_backup = url.replace(".gas", ".{:%Y%m%d%H%M%S}.gas".format(datetime.now()))
        #     shutil.copyfile(url, url_backup)
        engine = create_engine(str(new_uri), connect_args={'check_same_thread': False}, echo=False)
        self.sql_session = sessionmaker(bind=engine)(autoflush=False, autocommit=False, expire_on_commit=False)
