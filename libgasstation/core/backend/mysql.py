from sqlalchemy import create_engine, MetaData
from sqlalchemy.exc import InterfaceError, NoSuchModuleError, OperationalError, ProgrammingError
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists
from sqlalchemy_utils.functions import create_database, drop_database

from libgasstation.core.uri import Uri
from .backend import *


class MysqlBackEnd(BackEnd):

    def __init__(self):
        super().__init__()

    def session_begin(self, session, m_book_id: Uri, mode):
        create = mode == SessionOpenMode.NEW_STORE or mode == SessionOpenMode.NEW_OVERITE
        force = mode == SessionOpenMode.NEW_OVERITE
        if m_book_id.port == 0:
            m_book_id.port = 3306
        try:
            import pymysql
            m_book_id.set_driver("pymysql")
        except ModuleNotFoundError:
            import mysql.connector.authentication
            m_book_id.set_driver("mysqlconnector")
        m_book_id = m_book_id.__to_string__(False)
        try:
            if not create and not database_exists(m_book_id):
                self.push_error(FileError.FILE_NOT_FOUND, "Database doesn't exist")
                return
            elif create:
                if database_exists(m_book_id):
                    if force:
                        drop_database(m_book_id)
                    else:
                        self.push_error(BackEndError.STORE_EXISTS, "Database already exists")
                        return
                create_database(m_book_id)
            engine = create_engine(m_book_id, echo=False)
            self.sql_session = sessionmaker(bind=engine, autoflush=False)(autocommit=False,
                                                                          expire_on_commit=False,
                                                                          enable_baked_queries=True)
        except OperationalError as e:
            self.push_error(BackEndError.SERVER_ERR, e)
        except (NoSuchModuleError, ModuleNotFoundError) as e:
            self.push_error(BackEndError.NO_HANDLER, e)
        except InterfaceError as e:
            self.push_error(BackEndError.CANT_CONNECT, e)
        except ProgrammingError as e:
            self.push_error(BackEndError.CANT_CONNECT, e)

    def replicate(self, master_url="mysql+pymysql://root:newtonjonah@localhost:3306/gasstation_backup"):
        if not database_exists(master_url):
            create_database(master_url)
        slave_engine = self.sql_session.get_bind()
        slave_meta = DeclarativeBase.metadata
        master_engine = create_engine(master_url, echo=False)
        master_session = sessionmaker(bind=master_engine, autoflush=False)(autocommit=False,
                                                                           expire_on_commit=False,
                                                                           enable_baked_queries=True)
        master_engine._metadata = MetaData(bind=master_engine)
        for table in slave_meta.sorted_tables:
            # table.append_column(Column('branch_guid', UUIDType(binary=False),
            #                            nullable=False, default=lambda: "ae435898dc824856b2a32d33a35d724b"))

            table.create(bind=master_engine, checkfirst=True)

        for table in slave_meta.sorted_tables:
            for data in self.sql_session.query(table).all():
                master_session.merge(data)
        master_session.commit()
