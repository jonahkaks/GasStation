from contextlib import contextmanager
from enum import IntEnum

# from sqlalchemy import PrimaryKeyConstraint, event
from sqlalchemy.exc import DatabaseError, SQLAlchemyError, ProgrammingError
from sqlalchemy.exc import NoResultFound, MultipleResultsFound
from sqlalchemy.orm import joinedload, object_session

from .. import Bill
from .._declbase import DeclarativeBase
from ..account import Account
from ..book import Book
from ..commodity import Commodity
from ..customer import Customer
from ..invoice import Invoice
# from sqlalchemy.sql.ddl import DropConstraint, DropIndex
from ..location import Location
from ..payment import PaymentMethod
from ..product import ProductCategory, UnitOfMeasure, UnitOfMeasureGroup
from ..pump import Pump
from ..scheduled_transaction import ScheduledTransaction
from ..staff import Staff
from ..supplier import Supplier
from ..tank import Tank
from ..tax import Tax
from ..term import Term
from ..transaction import Transaction

fixed_load_order = [Commodity, Transaction, ScheduledTransaction, Location]
business_fixed_load_order = [UnitOfMeasure, UnitOfMeasureGroup, ProductCategory, Tank, Pump, Term, Tax, PaymentMethod,
                             Customer,Supplier, Staff, Invoice, Bill]


class SessionOpenMode(IntEnum):
    NORMAL_OPEN = 0
    NEW_STORE = 1
    NEW_OVERITE = 2
    READ_ONLY = 3
    BREAK_LOCK = 4


class BackendLoadType(IntEnum):
    INITIAL_LOAD = 0
    LOAD_ALL = 1


providers = {"mysql": "mysql+mysqlconnector", "sqlite": "sqlite", "postgres": "postgresql"}

FILE_URI_TYPE = "file"
FILE_URI_PREFIX = FILE_URI_TYPE + "://"
SQLITE3_URI_TYPE = "sqlite"
SQLITE3_URI_PREFIX = SQLITE3_URI_TYPE + "://"

class BackEndError(IntEnum):
    NO_ERR = 0
    NO_HANDLER = 1
    NO_BACKEND = 2
    BAD_URL = 3
    NO_SUCH_DB = 4
    CANT_CONNECT = 5
    CONN_LOST = 6
    LOCKED = 7
    STORE_EXISTS = 8
    READONLY = 9
    TOO_NEW = 10
    DATA_CORRUPT = 11
    SERVER_ERR = 12
    ALLOC = 13
    PERM = 14
    MODIFIED = 15
    MOD_DESTROY = 16
    MISC = 17


class FileError(IntEnum):
    FILE_BAD_READ = 1000,
    FILE_EMPTY = 1
    FILE_LOCKERR = 2
    FILE_NOT_FOUND = 3
    FILE_TOO_OLD = 4
    UNKNOWN_FILE_TYPE = 5
    PARSE_ERROR = 6
    BACKUP_ERROR = 7
    WRITE_ERROR = 8
    READ_ERROR = 9
    NO_ENCODING = 10
    FILE_EACCES = 11
    RESERVED_WRITE = 12
    FILE_UPGRADE = 13


class NetworkError(IntEnum):
    SHORT_READ = 2000,
    WRONG_CONTENT_TYPE = 1
    NOT_GSXML = 2


class SqlError(IntEnum):
    MISSING_DATA = 3000
    DB_TOO_OLD = 1
    DB_TOO_NEW = 2
    DB_BUSY = 3
    BAD_DBI = 4
    DBI_UNTESTABLE = 5


class BackEnd:
    def __init__(self):
        self.url = None
        self.pe = None
        self.m_last_err = BackEndError.NO_ERR
        self.m_error_message = ""
        self.sql_session = None
        self.m_book = None
        self.m_loading = False
        self.lock = False
        self.bulk_op = False
        self.no_save = False
        self.bulk_data = None
        self.m_is_pristine_db = False

    def compile_query(self, *args):
        pass

    def can_rollback(self):
        return True

    def begin(self, ins):
        # self.m_book.set_backend(None)
        # self.m_book.destroy()
        # self.m_book = None
        pass

    def rollback(self, ins):
        # self.sql_session.rollback()
        pass

    def session_begin(self, session, new_uri, mode):
        raise NotImplementedError

    def session_end(self):
        if self.sql_session is not None:
            self.sql_session.flush()
            self.sql_session.close()
            self.sql_session = None

    def create_tables(self):
        DeclarativeBase.metadata.create_all(self.sql_session.get_bind())

    @property
    def query(self):
        return self.sql_session.query

    @contextmanager
    def add_lock(self):
        self.lock = True
        # self.sql_session.begin_nested()
        yield self
        self.lock = False
        if self.sql_session is not None:
            self.sql_session.commit()

    def commit(self, inst):
        if self.sql_session is None:
            return
        if self.m_loading:
            inst.mark_clean()
            return
        if self.m_book.is_readonly():
            self.sql_session.rollback()
            self.set_error(BackEndError.READONLY)
            return

        if inst.e_type == "PriceDB":
            inst.mark_clean()
            self.m_book.mark_session_saved()

        is_dirty = inst.get_dirty_flag()
        is_destroying = inst.get_destroying()
        is_infant = inst.get_infant()
        if not is_dirty and not is_destroying:
            return
        if is_destroying and is_infant:
            return
        if is_destroying:
            self.sql_session.delete(inst)
        elif is_infant:
            self.sql_session.add(inst)
        if not self.lock:
            self.sql_session.commit()
        inst.mark_clean()
        self.m_book.mark_session_saved()
        return

    def get_url(self):
        return self.url

    def get_error(self):
        err = self.m_last_err
        self.m_last_err = BackEndError.NO_ERR
        return err

    def set_error(self, err):
        self.m_last_err = err

    def push_error(self, err, msg):
        self.m_last_err = err
        self.m_error_message = str(msg)

    def get_message(self):
        return self.m_error_message

    def do_load(self, session, load_type):
        if load_type == BackendLoadType.INITIAL_LOAD:
            self.create_tables()
        self.load(session, load_type)

    def load(self, session, load_type):
        book = None
        if session is None:
            return
        self.m_loading = True
        if load_type == BackendLoadType.INITIAL_LOAD:
            trans = []
            num_types = len(fixed_load_order + business_fixed_load_order)
            num_done = 0
            try:
                book = self.sql_session.query(Book).first()
                if book is None:
                    self.set_error(BackEndError.SERVER_ERR)
                    return
                book.set_backend(self)
                if session.m_book is not None:
                    session.m_book.destroy()
                    session.m_book = None
                session.m_book = book

            except (MultipleResultsFound, NoResultFound, DatabaseError):
                self.set_error(BackEndError.SERVER_ERR)
                return
            try:
                root = book.get_root_account()
                for _type in fixed_load_order:
                    num_done += 1
                    self.update_progress(num_done / num_types * 100)
                    if _type == Transaction and not self.lock:
                        trans = self.sql_session.query(Transaction).options(
                            joinedload(Transaction.splits, innerjoin=True)).all()
                    elif _type == Commodity:
                        self.sql_session.query(Commodity).options(joinedload(Commodity.prices)).all()
                    else:
                        self.sql_session.query(_type).all()
                for _type in business_fixed_load_order:
                    num_done += 1
                    self.update_progress(num_done / num_types * 100)
                    self.sql_session.query(_type).all()
            except ProgrammingError:
                self.sql_session.close()
                self.set_error(BackEndError.DATA_CORRUPT)
                return
            for t in trans:
                t.commit_edit()
            root.foreach_descendant(Account.commit_edit)
            root.commit_edit()
        else:
            self.sql_session.query(Transaction).options(joinedload(Transaction.splits, innerjoin=True)).all()
        self.m_book = book
        self.m_loading = False
        if self.m_book is not None:
            self.m_book.mark_session_saved()

    def set_percentage(self, pe):
        self.pe = pe

    def write_account_tree(self, root):
        is_ok = True
        if root is None:
            return False
        self.sql_session.add(root)
        for acc in root.get_descendants():
            self.sql_session.add(acc)
        self.update_progress(101.0)
        return is_ok

    def write_accounts(self):
        self.update_progress(101.0)
        is_ok = self.write_account_tree(self.m_book.get_root_account())
        if is_ok:
            self.update_progress(101.0)
            is_ok = self.write_account_tree(self.m_book.get_template_root())
        return is_ok

    def write_tx(self, transaction):
        self.sql_session.add(transaction)

    def write_transactions(self):
        self.m_book.get_root_account().foreach_transaction(self.write_tx)
        self.update_progress(101.0)
        return True

    def write_template_transactions(self):
        self.m_book.get_template_root().foreach_transaction(self.write_tx)
        self.update_progress(101.0)
        return True

    def write_schedXactions(self):
        from ..scheduled_transaction_list import ScheduledTransactionList
        for obj in ScheduledTransactionList.get_all(self.m_book):
            self.sql_session.add(obj)
        return True

    def sync(self, book):
        self.update_progress(101.0)
        self.create_tables()
        self.m_book = book
        ss = object_session(book)
        if ss is not None:
            ss.expunge_all()
        self.sql_session.add(book)
        self.write_accounts()
        self.write_transactions()
        self.write_template_transactions()
        self.write_schedXactions()

        try:
            self.sql_session.commit()
            book.mark_session_saved()
        except SQLAlchemyError as e:
            self.push_error(BackEndError.SERVER_ERR, e.args[1])
            self.sql_session.rollback()
        self.finish_progress()

    def safe_sync(self, book):
        self.sync(book)

    def replicate(self, master_url):
        raise NotImplementedError

    def export_coa(self, book):
        pass

    def update_progress(self, p=None, m=None):
        if self.pe is not None:
            self.pe(m, p)

    def close(self):
        del self

    def __del__(self):
        self.bulk_data = None
        self.pe = None
        self.m_book = None

    def finish_progress(self):
        self.update_progress(-1)

    @classmethod
    def release(cls):
        pass
