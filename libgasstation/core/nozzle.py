from sqlalchemy import Column, VARCHAR, ForeignKey, DECIMAL
from sqlalchemy.orm import relationship, reconstructor
from sqlalchemy_utils import UUIDType

from ._declbase import DeclarativeBaseGuid, ID_PUMP, ID_TANK, ID_NOZZLE
from .engine import Engine
from .event import *
from .pump import Pump
from .query_private import *
from .tank import Tank

NOZZLE_NAME = "nozzle-name"
NOZZLE_PUMP = "nozzle-pump"
NOZZLE_TANK = "nozzle-tank"


class Nozzle(DeclarativeBaseGuid):
    __tablename__ = 'nozzle'

    __table_args__ = {}
    location_guid = Column(UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    pump_guid = Column(UUIDType(binary=False), ForeignKey('pump.guid'), nullable=False)
    tank_guid = Column(UUIDType(binary=False), ForeignKey('tank.guid'), nullable=False)
    name = Column('name', VARCHAR(length=25), nullable=False)
    opening_meter = Column(DECIMAL(10, 2), nullable=False, default=Decimal(0))
    pump = relationship('Pump')
    tank = relationship('Tank')
    fuel = relationship('Fuel', lazy="joined", back_populates="nozzle")
    location = relationship('Location')

    def __init__(self, book):
        self.infant = True
        super().__init__(ID_NOZZLE, book)
        self.meter = Decimal(0)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        self.infant = False
        book = Session.get_current_book()
        super().__init__(ID_NOZZLE, book)
        Event.gen(self, EVENT_CREATE)

    @classmethod
    def lookup_name(cls, book, name):
        a = book.get_collection(ID_NOZZLE)
        for cat in a.values():
            if cat.get_name() == name.strip():
                return cat

    @classmethod
    def lookup_name_with_location(cls, book, name, location):
        a = book.get_collection(ID_NOZZLE)
        for cat in a.values():
            if cat.name == name.strip() and cat.location == location:
                return cat

    def get_product(self):
        return self.tank.product if self.tank is not None else None

    def get_product_str(self):
        return self.tank.get_product_str() if self.tank is not None else ""

    def set_product(self, inv):
        if inv is not None and inv != self.product:
            self.begin_edit()
            self.product = inv
            self.set_dirty()
            self.commit_edit()

    def get_location(self):
        return self.location

    def set_location(self, location):
        if location != self.location:
            self.begin_edit()
            self.location = location
            self.set_dirty()
            self.commit_edit()

    def set_opening_meter(self, om):
        if om != self.opening_meter and om is not None:
            self.begin_edit()
            self.opening_meter = om
            self.set_dirty()
            self.commit_edit()

    def get_opening_meter(self):
        return self.opening_meter

    def get_name(self):
        return self.name

    def set_name(self, name):
        if name is not None and name != self.name:
            self.begin_edit()
            self.name = name.strip()
            self.set_dirty()
            self.commit_edit()
            Event.gen(self, EVENT_MODIFY)

    def set_pump(self, p: Pump):
        if p is not None and isinstance(p, Pump) and p != self.pump:
            self.begin_edit()
            self.pump = p
            self.set_dirty()
            self.commit_edit()
            Event.gen(self, EVENT_MODIFY)

    def get_pump(self) -> Pump:
        return self.pump

    def set_tank(self, t: Tank):
        if t is not None and isinstance(t, Tank) and t != self.tank:
            self.begin_edit()
            self.tank = t
            self.set_dirty()
            self.commit_edit()
            Event.gen(self, EVENT_MODIFY)

    def get_tank(self) -> Tank:
        return self.tank

    def refers_to(self, other):
        if isinstance(other, (Tank, Pump)):
            if self.tank == other:
                return True
            elif self.pump == other:
                return True
        return False

    def get_fuel(self):
        return self.fuel

    def get_tank_str(self):
        return self.tank.get_name() if self.tank is not None else ""

    def get_pump_str(self):
        return self.pump.get_name() if self.pump is not None else ""

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.product = self.tank.product
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        self.product = None
        self.pump = None
        self.fuel = None
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def on_error(self, error):
        Engine.signal_commit_error(error)

    def __lt__(self, other):
        if other is None:
            return False
        if self.create_time is None:
            return True
        if other.create_time is None:
            return False
        return self.create_time < other.create_time

    @classmethod
    def register(cls):
        params = [Param(NOZZLE_NAME, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(NOZZLE_PUMP, ID_PUMP, cls.get_pump, cls.set_pump),
                  Param(NOZZLE_TANK, ID_TANK, cls.get_tank, cls.set_tank),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_NOZZLE, None, params)

    def __repr__(self):
        return "Nozzle<%s>" % self.get_name()
