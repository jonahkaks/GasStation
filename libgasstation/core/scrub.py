import enum
from decimal import Decimal

from .account import AccountType, Account
from .transaction import Split, Transaction, TransactionType


class EquityType(enum.Enum):
    OPENING_BALANCE = 0
    RETAINED_EARNINGS = 1


def equity_base_name(equity_type):
    if equity_type == EquityType.OPENING_BALANCE:
        return "Opening Balances"
    elif equity_type == EquityType.RETAINED_EARNINGS:
        return "Retained Earnings"
    else:
        return ""


class CommodityCount:
    def __init__(self):
        self.commodity = None
        self.count = 0


class Scrub:
    @staticmethod
    def account_tree_orphans(acc, percentage_func):
        if acc is None:
            return
        Scrub.account_orphans(acc, percentage_func)
        acc.foreach_descendant(Scrub.account_orphans, percentage_func)

    @staticmethod
    def trans_orphans_fast(trans, root):
        if trans is None or root is None:
            return
        for split in filter(lambda s: s.account is None, trans.splits):
            accname = "Orphan" + "-" + trans.get_currency().get_mnemonic()
            orph = Scrub.get_or_make_account(root, trans.get_currency(), accname, AccountType.BANK, False)
            if orph is None:
                continue
            split.set_account(orph)

    @staticmethod
    def account_orphans(acc, percentagefunc):
        message = "Looking for orphans in account %s: %u of %u"
        current_split = 0
        if acc is None:
            return
        st = acc.get_name() or "(null)"
        splits = acc.get_splits()
        total_splits = len(splits)

        for split in splits:
            if current_split % 100 == 0:
                progress_msg = message % (st, current_split, total_splits)
                percentagefunc(progress_msg, (100 * current_split) / total_splits)
            Scrub.trans_orphans_fast(split.get_transaction(), acc.get_root())
            current_split += 1
        percentagefunc(None, -1.0)

    @staticmethod
    def trans_orphans(trans):
        if trans is None: return
        for split in trans.splits:
            if split.account:
                Scrub.trans_orphans_fast(trans, split.account.get_root())
                return
        book = trans.get_book()
        root = book.get_root_account()
        Scrub.trans_orphans_fast(trans, root)

    @staticmethod
    def account_tree_splits(account):
        if account is None:
            return
        Scrub.account_splits(account)
        account.foreach_descendant(Scrub.account_splits)

    @staticmethod
    def account_splits(account):
        for sp in account.get_splits():
            Scrub.split(sp)

    @staticmethod
    def split(split):
        if split is None:
            return
        trans = split.get_transaction()
        if trans is None:
            return
        account = split.get_account()

        if account is None:
            Scrub.trans_orphans(trans)
            account = split.get_account()

        if account is None:
            return

        value = split.get_value()
        if value is None:
            value = Decimal(0)
            split.set_value(value)

        amount = split.get_amount()
        if amount is None:
            amount = Decimal(0)
            split.set_amount(amount)
        currency = trans.get_currency()
        acc_commodity = account.get_commodity()
        if acc_commodity is None:
            Scrub.account_commodity(account)

        if acc_commodity is None or not acc_commodity.equiv(currency):
            return
        if amount == value:
            return
        trans.begin_edit()
        split.set_amount(value)
        trans.commit_edit()

    @staticmethod
    def account_tree_imbalance(acc, percentagefunc):
        Scrub.account_imbalance(acc, percentagefunc)
        acc.foreach_descendant(Scrub.account_imbalance, percentagefunc)

    @staticmethod
    def account_imbalance(acc, percentagefunc):
        message = "Looking for imbalances in account %s: %u of %u"
        curr_split_no = 0
        if acc is None:
            return
        st = acc.get_name() or "(null)"
        splits = acc.get_splits()
        split_count = len(splits)
        for split in splits:
            trans = split.get_transaction()
            if curr_split_no % 100 == 0:
                progress_msg = message % (st, curr_split_no, split_count)
                percentagefunc(progress_msg, (100 * curr_split_no) / split_count)
            Scrub.trans_orphans_fast(split.get_transaction(), acc.get_root())
            percentagefunc(None, 0.0)
            Scrub.trans_currency(trans)
            Scrub.trans_imbalance(trans, acc.get_root(), None)
            curr_split_no += 1
        percentagefunc(None, -1.0)

    @staticmethod
    def get_balance_split(trans, root, account, commodity):
        if account is None or not commodity.equiv(account.get_commodity()):
            if root is None:
                root = trans.get_book().get_root_account()
                if root is None:
                    return
            accname = "Imbalance-" + commodity.get_mnemonic()
            account = Scrub.get_or_make_account(root, commodity, accname, AccountType.BANK, False)
            if account is None:
                return
        balance_split = trans.find_split_by_account(account)
        if balance_split is None:
            balance_split = Split(trans.get_book())
            trans.begin_edit()
            balance_split.set_transaction(trans)
            balance_split.set_account(account)
            trans.commit_edit()
        return balance_split

    @staticmethod
    def get_trading_split(trans, root, commodity):
        if root is None:
            root = trans.get_book().get_root_account()
            if root is None: return
        income_account = root.lookup_by_name("Income")
        default_currency = income_account.get_commodity() if income_account is not None else None
        if default_currency is None:
            default_currency = commodity
        trading_account = Scrub.get_or_make_account(root, default_currency, "Trading", AccountType.TRADING, True)
        if trading_account is None:
            return
        ns_account = Scrub.get_or_make_account(trading_account, default_currency, commodity.get_namespace(),
                                               AccountType.TRADING, True)
        if ns_account is None:
            return
        account = Scrub.get_or_make_account(ns_account, commodity, commodity.get_mnemonic(), AccountType.TRADING, False)
        if account is None:
            return
        balance_split = trans.find_split_by_account(account)
        if balance_split is None:
            balance_split = Split(trans.get_book())
            trans.begin_edit()
            balance_split.set_transaction(trans)
            balance_split.set_account(account)
            trans.commit_edit()
        return balance_split

    @staticmethod
    def find_trading_split(trans, root, commodity):
        if root is None:
            root = trans.get_book().get_root_account()
            if root is None:
                return
        trading_account = root.lookup_by_name("Trading")
        if trading_account is None:
            return
        ns_account = trading_account.lookup_by_name(commodity.get_namespace())
        if ns_account is None:
            return
        account = ns_account.lookup_by_name(commodity.get_mnemonic())
        if account is None: return
        return trans.find_split_by_account(account)

    @staticmethod
    def add_balance_split(trans, imbalance, root, account):
        currency = trans.get_currency()
        balance_split = Scrub.get_balance_split(trans, root, account, currency)
        if balance_split is None:
            return
        account = balance_split.get_account()
        trans.begin_edit()
        old_value = balance_split.get_value()
        new_value = old_value - imbalance
        balance_split.set_value(new_value)
        commodity = account.get_commodity()
        if currency.equiv(commodity):
            balance_split.set_amount(new_value)
        Scrub.split(balance_split)
        trans.commit_edit()

    @staticmethod
    def transaction_balance_no_trading(trans, root, account):
        imbalance = trans.get_imbalance_value()
        if not imbalance.is_zero():
            Scrub.add_balance_split(trans, imbalance, root, account)

    @staticmethod
    def transaction_adjust_trading_splits(trans, root):
        imbalance = Decimal(0)
        for split in trans.splits:
            txn_curr = trans.get_currency()
            if not trans.still_has_split(split):
                continue
            commodity = split.get_account().get_commodity()
            if commodity is None:
                continue
            balance_split = Scrub.find_trading_split(trans, root, commodity)
            if balance_split != split:
                imbalance += split.get_value()
            value = split.get_value()
            amount = split.get_amount()
            if amount.is_zero() or value.is_zero():
                continue

            if balance_split and balance_split != split:
                convrate = amount / value
                old_value = balance_split.get_value()
                new_value = balance_split.get_amount() / convrate
                if not old_value.equal(new_value):
                    trans.begin_edit()
                    balance_split.set_value(new_value)
                    Scrub.split(balance_split)
                    trans.commit_edit()
        return imbalance

    @staticmethod
    def transaction_get_commodity_imbalance(trans, commodity):
        val_imbalance = Decimal(0)
        for split in trans.splits():
            split_commodity = split.get_account().get_commodity()
            if trans.still_has_split(split) and commodity.equal(split_commodity):
                val_imbalance += split.get_value()
        return val_imbalance

    @staticmethod
    def transaction_balance_trading(trans, root):
        imbal_list = trans.get_imbalance()
        if not imbal_list:
            return

        for imbal_mon in imbal_list:
            txn_curr = trans.get_currency()
            commodity = imbal_mon.get_commodity()
            balance_split = Scrub.get_trading_split(trans, root, commodity)
            trans.begin_edit()
            old_amount = balance_split.get_amount()
            new_amount = old_amount - imbal_mon.get_value()
            balance_split.set_amount(new_amount)
            if txn_curr.equal(commodity):
                balance_split.set_value(new_amount)
            else:
                val_imbalance = Scrub.transaction_get_commodity_imbalance(trans, commodity)
                old_value = balance_split.get_value()
                new_value = old_value - val_imbalance
                balance_split.set_value(new_value)
            Scrub.split(balance_split)
            trans.commit_edit()

    @staticmethod
    def transaction_balance_trading_more_splits(trans, root):
        splits_dup = trans.splits.copy()
        txn_curr = trans.get_currency()
        for split in splits_dup:
            if not trans.still_has_split(split): continue
            if not split.get_value().is_zero() and split.get_amount().is_zero():
                commodity = split.get_account().get_commodity() if split.get_account() is not None else None
                if commodity is None:
                    continue
                balance_split = Scrub.get_trading_split(trans, root, commodity)
                if balance_split is None:
                    return
                trans.begin_edit()

                old_value = balance_split.get_value()
                new_value = old_value - split.get_value()
                balance_split.set_value(new_value)
                Scrub.split(balance_split)
                trans.commit_edit()

    @staticmethod
    def trans_imbalance(trans, root, account):
        if trans is None:
            return
        trans.scrub_splits()
        if trans.is_balanced():
            return

        if not trans.use_trading_accounts():
            Scrub.transaction_balance_no_trading(trans, root, account)
            return
        imbalance = Scrub.transaction_adjust_trading_splits(trans, root)
        if not imbalance.is_zero():
            Scrub.add_balance_split(trans, imbalance, root, account)
        Scrub.transaction_balance_trading(trans, root)
        if trans.get_imbalance_value().is_zero():
            return
        Scrub.transaction_balance_trading_more_splits(trans, root)

    # @staticmethod
    # def FindCommonExclSCurrency(splits,ra, rb,*excl_split) {
    #     if (!splits) return None
    #
    #     for (node = splits node node = node->next) {
    #         Split *s = node->data
    #     sa, *sb
    #
    #     if (s == excl_split) continue
    #
    #     g_return_val_if_fail (s->acc, None)
    #
    #     sa = DxaccAccountGetCurrency(s->acc)
    #     sb = xaccAccountGetCommodity(s->acc)
    #
    #     if (ra and rb) {
    #     int aa = !gnc_commodity_equiv(ra, sa)
    #     int ab = !gnc_commodity_equiv(ra, sb)
    #     int ba = !gnc_commodity_equiv(rb, sa)
    #     int bb = !gnc_commodity_equiv(rb, sb)
    #
    #     if ((!aa) and bb) rb = None
    #     else if ((!ab) and ba) rb = None
    #     else if ((!ba) and ab) ra = None
    #     else if ((!bb) and aa) ra = None
    #     else if (aa and bb and ab and ba) {
    #     ra = None
    #     rb = None
    #     }
    #
    #     if (!ra) {
    #     ra = rb
    #     rb = None
    #     }
    #     } else if (ra and !rb) {
    #     int aa = !gnc_commodity_equiv(ra, sa)
    #     int ab = !gnc_commodity_equiv(ra, sb)
    #     if (aa and ab) ra = None
    #     } else if (!ra and rb) {
    #     int aa = !gnc_commodity_equiv(rb, sa)
    #     int ab = !gnc_commodity_equiv(rb, sb)
    #     ra = (aa and ab) ? None : rb
    #     }
    #
    #     if ((!ra) and (!rb)) return None
    #     }
    #
    #     return (ra)
    #     }
    #
    # @staticmethod
    # def FindCommonCurrency(splits, ra, rb):
    #     return Scrub.FindCommonExclSCurrency(splits, ra, rb, None)
    #
    # def commodity_equal(gconstpointer a, gconstpointer b) {
    #     CommodityCount *cc = (CommodityCount *) a
    #     com = () b
    #     if (cc == None or cc->commodity == None or
    #     !GNC_IS_COMMODITY(cc->commodity))
    #     return -1
    #     if (com == None or !GNC_IS_COMMODITY(com)) return 1
    #     if (gnc_commodity_equal(cc->commodity, com))
    #     return 0
    #     return 1
    #     }
    #
    #     static gint
    #     commodity_compare(gconstpointer a, gconstpointer b) {
    #     CommodityCount *ca = (CommodityCount *) a, *cb = (CommodityCount *) b
    #     if (ca == None or ca->commodity == None or
    #     !GNC_IS_COMMODITY(ca->commodity)) {
    #     if (cb == None or cb->commodity == None or
    #     !GNC_IS_COMMODITY(cb->commodity))
    #     return 0
    #     return -1
    #     }
    #     if (cb == None or cb->commodity == None or
    #         !GNC_IS_COMMODITY(cb->commodity))
    #         return 1
    #     if (ca->count == cb->count)
    #         return 0
    #     return ca->count > cb->count ? 1 : -1
    #     }
    #
    #
    # def trans_find_common_currency(trans, book):
    #     if trans is None:
    #         return None
    #
    #     if len(trans)==0:
    #         return
    #     if book is None:
    #         return
    #
    #     for (node = trans->splits node node = node->next) {
    #         Split *s = node->data
    #         unsigned int curr_weight
    #
    #         if (s == None or s->acc == None) continue
    #         if (xaccAccountGetType(s->acc) == AccountType.TRADING) continue
    #         com_scratch = xaccAccountGetCommodity(s->acc)
    #         if (com_scratch and gnc_commodity_is_currency(com_scratch)) {
    #         curr_weight = 3
    #         } else {
    #         com_scratch = gnc_account_get_currency_or_parent(s->acc)
    #         if (com_scratch == None) continue
    #         curr_weight = 1
    #         }
    #         if (comlist) {
    #         found = g_slist_find_custom(comlist, com_scratch, commodity_equal)
    #         }
    #         if (comlist == None or found == None) {
    #         CommodityCount *count = g_slice_new0(CommodityCount)
    #         count->commodity = com_scratch
    #         count->count = curr_weight
    #         comlist = g_slist_append(comlist, count)
    #         } else {
    #         CommodityCount *count = (CommodityCount *) (found->data)
    #         count->count += curr_weight
    #         }
    #     }
    #     found = g_slist_sort(comlist, commodity_compare)
    #
    #     if (found and found->data and (((CommodityCount *) (found->data))->commodity != None)) {
    #     return ((CommodityCount *) (found->data))->commodity
    #     }
    #
    #     return xaccTransFindOldCommonCurrency(trans, book)
    #     }

    @staticmethod
    def trans_currency(trans):
        if trans is None:
            return
        Scrub.trans_orphans(trans)
        currency = trans.get_currency()
        if currency and currency.is_currency():
            return
        currency = Scrub.trans_find_common_currency(trans, trans.get_book())
        if currency:
            trans.begin_edit()
            trans.set_currency(currency)
            trans.commit_edit()
        else:
            for split in trans.splits:
                if split.get_account() is not None:
                    currency = split.get_account().get_commodity()
                    trans.begin_edit()
                    trans.set_currency(currency)
                    trans.commit_edit()
                    return
        for sp in trans.splits:
            if not sp.get_amount().equal(sp.get_value()):
                acc_currency = sp.account.get_commodity() if sp.account is not None else None
                if acc_currency == currency:
                    trans.begin_edit()
                    sp.set_amount(sp.get_value())
                    trans.commit_edit()

    @staticmethod
    def account_commodity(account):
        if account is None:
            return
        if account.get_type() == AccountType.ROOT:
            return
        commodity = account.get_commodity()
        if commodity is not None:
            return

    @staticmethod
    def trans_currency_helper(t):
        Scrub.trans_currency(t)
        return 0

    @staticmethod
    def account_commodity_helper(account):
        Scrub.account_commodity(account)

    @staticmethod
    def account_tree_commodity(acc):
        if acc is None:
            return
        # xaccAccountTreeForEachTransaction(acc, Scrub.trans_currency_helper, None)
        Scrub.account_commodity_helper(acc)
        acc.foreach_descendant(Scrub.account_commodity_helper)

    @staticmethod
    def check_quote_source(com, commodity_has_quote_src):
        if com and not com.is_iso():
            commodity_has_quote_src |= com.get_quote_flag()
        return commodity_has_quote_src

    @staticmethod
    def get_or_make_account_with_fullname_parent(root, currency, name, type, *parent_name):
        accname = name + "-" + currency.get_mnemonic()
        account = Scrub.get_or_make_account(root, currency, accname, type, False)
        separator = Account.get_separator()
        parent = root.lookup_by_full_name(separator.join(parent_name))
        if parent is not None and account.get_parent() != parent:
            account.set_parent(parent)
        return account

    @staticmethod
    def get_or_make_account(root, currency, accname, acctype, placeholder):
        if root is None:
            return
        if currency is None:
            return
        acc = root.lookup_by_name(accname)
        if acc is None:
            acc = Account(root.get_book())
            acc.begin_edit()
            acc.set_name(accname)
            acc.set_description(accname)
            acc.set_commodity(currency)
            acc.set_type(acctype)
            acc.set_placeholder(placeholder)
            root.append_child(acc)
            acc.commit_edit()
        return acc

    @staticmethod
    def trans_post_date(trans):
        orig = trans.get_post_date()
        pass

    @staticmethod
    def find_or_create_equity_account(root, equity_type, currency):
        if root is None or currency is None: return
        base_name = equity_base_name(equity_type)
        account = root.lookup_by_name(base_name)
        if account is not None and account.get_type() != AccountType.EQUITY:
            account = None

        if account is None:
            base_name = base_name or ""
            account = root.lookup_by_name(base_name)
            if account and account.get_type() != AccountType.EQUITY:
                account = None
        base_name_exists = account is not None
        if account is not None and currency == account.get_commodity():
            return account

        name = base_name + " - " + currency.get_mnemonic()
        account = root.lookup_by_name(name)
        if account is not None and account.get_type() != AccountType.EQUITY:
            account = None
        name_exists = account is not None

        if account is not None and currency == account.get_commodity():
            return account

        if name_exists and base_name_exists: return None

        if not base_name_exists:
            name = base_name
        parent = root.lookup_by_name("Equity")
        if not parent or parent.get_type() != AccountType.EQUITY:
            parent = root
        account = Account(root.get_book())
        account.begin_edit()
        account.set_name(name)
        account.set_type(AccountType.EQUITY)
        account.set_commodity(currency)
        parent.begin_edit()
        parent.append_child(account)
        parent.commit_edit()
        account.commit_edit()
        return account

    @staticmethod
    def create_opening_balance(account, balance, date, book, description=None, location=None,
                               equity_type=EquityType.OPENING_BALANCE, ttype=TransactionType.NONE):
        if balance.is_zero():
            return None
        if account is None:
            return None
        equity_account = Scrub.find_or_create_equity_account(account.get_root(), equity_type,
                                                             account.get_commodity())
        if equity_account is None:
            raise ValueError("Counld not find or create equity account")
        account.begin_edit()
        equity_account.begin_edit()
        trans = Transaction(book)
        trans.begin_edit()
        if location is not None:
            trans.set_location(location)
        trans.set_currency(account.get_commodity() or book.default_currency)
        trans.set_post_date(date)
        trans.set_type(ttype)
        if description is None:
            trans.set_description("Opening Balance")
        else:
            trans.set_description(description)
            trans.set_notes("Opening Balance")
        trans.set_readonly("Is opening balance transaction")
        split = Split(book)
        trans.append_split(split)
        split.set_account(account)
        split.set_value(balance)
        split.set_amount(balance)
        balance = balance * -1
        split = Split(book)
        split.set_account(equity_account)
        split.set_value(balance)
        split.set_amount(balance)
        trans.append_split(split)
        trans.commit_edit()
        equity_account.commit_edit()
        account.commit_edit()
        return trans
