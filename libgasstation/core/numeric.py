import math
from decimal import Decimal
from enum import IntEnum
from fractions import Fraction


class RoundParam:
    def __init__(self, n, d, r):
        self.num = n
        self.den = d
        self.rem = r


max_leg_digits = 17
pten = [1, 10, 100, 1000, 10000, 100000, 1000000,
        10000000, 100000000, 1000000000,
        10000000000, 100000000000,
        1000000000000, 10000000000000,
        100000000000000,
        10000000000000000,
        100000000000000000,
        1000000000000000000]

POWTEN_OVERFLOW = -5
DENOM_AUTO = 0
NUMERIC_RND_MASK = 0x0000000f
NUMERIC_DENOM_MASK = 0x000000f0
NUMERIC_SIGFIMASK = 0x0000ff00


def powten(exp):
    if exp > max_leg_digits:
        exp = max_leg_digits
    return pten[exp]


class NumericRound(IntEnum):
    FLOOR = 0x01
    CEIL = 0x02
    TRUNC = 0x03
    PROMOTE = 0x04
    HALF_DOWN = 0x05
    HALF_UP = 0x06
    ROUND = 0x07
    NEVER = 0x08


class NumericDenom(IntEnum):
    EXACT = 0x10
    REDUCE = 0x20
    LCD = 0x30
    FIXED = 0x40
    SIGFIG = 0x50


class RoundType(IntEnum):
    floor = NumericRound.FLOOR
    ceiling = NumericRound.CEIL
    truncate = NumericRound.TRUNC
    promote = NumericRound.PROMOTE
    half_down = NumericRound.HALF_DOWN
    half_up = NumericRound.HALF_UP
    bankers = NumericRound.ROUND
    never = NumericRound.NEVER


class DenomType(IntEnum):
    den_auto = DENOM_AUTO
    exact = NumericDenom.EXACT
    reduce = NumericDenom.REDUCE
    lcd = NumericDenom.LCD
    fixed = NumericDenom.FIXED
    sigfigs = NumericDenom.SIGFIG


def num_round(num, den, rem, t):
    if t == RoundType.never:
        if rem == 0:
            return num
    elif t == RoundType.floor:
        if rem == 0:
            return num
        if num < 0:
            return num + 1
        return num
    elif t == RoundType.ceiling:
        if rem == 0:
            return num
        if num > 0:
            return num + 1
        return num
    elif t == RoundType.truncate:
        return num
    elif t == RoundType.promote:
        if rem == 0:
            return num
        return num + (-1 if num < 0 else 1)

    elif t == RoundType.half_down:
        if rem == 0:
            return num
        if abs(rem * 2) > abs(den):
            return num + (-1 if num < 0 else 1)
        return num

    elif t == RoundType.half_up:
        if rem == 0:
            return num
        if abs(rem) * 2 >= abs(den):
            return num + (-1 if num < 0 else 1)
        return num

    elif t == RoundType.bankers:
        if rem == 0:
            return num
        if abs(rem * 2) > abs(den) or (abs(rem * 2) == abs(den) and num % 2):
            return num + (-1 if num < 0 else 1)
        return num


def HOW_DENOM_SIGFIGS(n): return ((n & 0xff) << 8) | NumericDenom.SIGFIG


def HOW_GET_SIGFIGS(a): return (a & 0xff00) >> 8


class NumericException(Exception):
    pass


class NumericErrorCode(IntEnum):
    OK = 0
    ARG = -1
    OVERFLOW = -2
    DENOM_DIFF = -3
    REMAINDER = -4


class Numeric(Fraction):
    @classmethod
    def zero(cls):
        return super(Numeric, cls).__new__(cls, 0, 1)

    def get_numerator(self):
        return self.numerator

    def neg(self):
        return Numeric(-self.numerator, self.denominator)

    def get_denom(self):
        return self.denominator

    def check(self):
        if self.denominator != 0:
            return NumericErrorCode.OK
        elif self.numerator < -4:
            return NumericErrorCode.OVERFLOW
        return NumericErrorCode.ARG

    def zero_p(self):
        if self.check():
            return False
        else:
            if (self.numerator == 0) and (self.denominator != 0):
                return True
            else:
                return False

    def negative_p(self):
        if self.check():
            return False
        else:
            if (self.numerator < 0) and (self.denominator != 0):
                return True
            else:
                return False

    def positive_p(self):
        if self.check():
            return True
        else:
            if (self.numerator > 0) and (self.denominator != 0):
                return True
            else:
                return False

    def compare(self, other):
        if other is None: return
        if self.check() or Numeric(other).check():
            return 0
        if self < other:
            return -1
        elif self > other:
            return 1
        return 0

    def abs(self):
        return Numeric(self.__abs__())

    def equal(self, other):
        if self is None or other is None:
            return False
        if self.check():
            if other.check():
                return True
            else:
                return False
        if other.check():
            return False
        return self.compare(other) == 0

    def denom_lcd(self, other, denom, how):
        if denom == DENOM_AUTO and (how & NUMERIC_RND_MASK) == NumericDenom.LCD:
            denom = self.lcm(other)
        return denom

    def lcm(self, other):
        return (self.denominator * other.denominator) / math.gcd(self.denominator, other.denominator)

    def to_decimal(self, max_decimal_places=0):
        if self.numerator == 0:
            return False, Decimal(0)
        elif self.denominator == 0:
            return False, Decimal(0)
        a = Decimal(self.numerator / self.denominator)
        return True, a

    def convert(self, new_denom, how):
        rtype = RoundType.never
        if how & NUMERIC_RND_MASK != 0:
            rtype = RoundType(how & NUMERIC_RND_MASK)
        figs = HOW_GET_SIGFIGS(how)
        dtype = DenomType(how & NUMERIC_DENOM_MASK)
        sigfigs = dtype == DenomType.sigfigs
        if dtype == DenomType.reduce:
            self.reduce()
        if sigfigs:
            return self.__convert_sigfigs(rtype, figs)
        else:
            return self.__convert(rtype, new_denom)

    def __convert(self, rtype, new_denom):
        params = self.prepare_conversion(new_denom)
        if new_denom == DENOM_AUTO:
            new_denom = self.denominator
        if params.rem == 0:
            return Numeric(params.num, new_denom, _normalize=False)
        return Numeric(num_round(params.num, params.den, params.rem, rtype), new_denom, _normalize=False)

    def reduce(self):
        return self.__reduce__()

    def sigfigs_denom(self, figs):
        digits = 0
        if self.numerator == 0:
            return 1
        num_abs = abs(self.numerator)
        not_frac = num_abs > self.denominator
        val = num_abs / self.denominator if not_frac else self.denominator / num_abs
        while val >= 10:
            digits += 1
            val /= 10
        return powten(figs - digits - 1 if digits < figs else 0) if not_frac else powten(figs + digits)

    def __convert_sigfigs(self, rtype, figs):
        new_denom = self.sigfigs_denom(figs)
        params = self.prepare_conversion(new_denom)
        if new_denom == 0:
            new_denom = 1
        if params.rem == 0:
            return Numeric(params.num, new_denom)
        return Numeric(num_round(params.num, params.den, params.rem, rtype), new_denom)

    def add(self, other, denom, how):
        if self.check() or other.check():
            return Numeric.zero()
        denom = self.denom_lcd(other, denom, how)
        try:
            sm = Numeric(self + other)
            if how & NUMERIC_DENOM_MASK != NumericDenom.EXACT:
                sm = sm.convert(denom, how)
                return sm
            if denom == DENOM_AUTO and how & NUMERIC_RND_MASK != NumericRound.NEVER:
                return sm.round_to_numeric()
            sm = sm.convert(denom, how)
            return sm
        except Exception as e:
            return Numeric.zero()

    def sub(self, other, denom, how):
        if self.check() or other.check():
            return Numeric.zero()
        denom = self.denom_lcd(other, denom, how)
        try:
            sm = Numeric(self - other)
            if how & NUMERIC_DENOM_MASK != NumericDenom.EXACT:
                sm = Numeric.convert(sm, denom, how)
                return sm
            if denom == DENOM_AUTO and how & NUMERIC_RND_MASK != NumericRound.NEVER:
                return sm.round_to_numeric()

            sm = Numeric.convert(sm, denom, how)
            return sm
        except:
            return Numeric.zero()

    def mul(self, other, denom, how):
        if self.check() or other.check():
            return Numeric.zero()
        denom = self.denom_lcd(other, denom, how)
        try:
            sm = Numeric(self * other)
            if how & NUMERIC_DENOM_MASK != NumericDenom.EXACT:
                sm = sm.convert(denom, how)
                return sm
            if denom == DENOM_AUTO and how & NUMERIC_RND_MASK != NumericRound.NEVER:
                return sm.round_to_numeric()
            sm = sm.convert(denom, how)
            return sm
        except:
            return Numeric.zero()

    def div(self, other, denom, how):
        if self.check() or other.check():
            return Numeric.zero()
        denom = self.denom_lcd(other, denom, how)
        try:
            if (how & NUMERIC_DENOM_MASK) != NumericDenom.EXACT:
                sm = Numeric(self / other)
                return sm.convert(denom, how)
            sm = Numeric(self / other)
            if denom == DENOM_AUTO and (how & NUMERIC_RND_MASK) != NumericRound.NEVER:
                return sm.round_to_numeric()
            return sm.convert(denom, how)
        except Exception as e:
            return Numeric.zero()

    def add_fixed(self, other):
        return Numeric(self.add(other, DENOM_AUTO, NumericDenom.FIXED | NumericRound.NEVER))

    def sub_fixed(self, other):
        return self.sub(other, DENOM_AUTO, NumericDenom.FIXED | NumericRound.NEVER)

    def prepare_conversion(self, new_denom):
        if new_denom == self.denominator or new_denom == DENOM_AUTO:
            return RoundParam(self.numerator, self.denominator, 0)
        conv = Numeric((new_denom * self.numerator) / self.denominator)
        n, r = divmod(conv.numerator, conv.denominator)
        return RoundParam(n, conv.denominator, r)

    def round_to_numeric(self):
        return self.convert(1, NumericRound.HALF_UP)

    def to_double(self):
        if self.denominator > 0:
            return float(self.numerator) / float(self.denominator)
        else:
            return float(self.numerator * -self.denominator)

    @staticmethod
    def from_double(d, denom, how):
        a = Numeric(d)
        return a.convert(denom, how)

    def to_int(self):
        if self.denominator > 0:
            return int(self.numerator / self.denominator)
        else:
            return 0

    def invert(self):
        if self.numerator == 0:
            return Numeric.zero()
        if self.numerator < 0:
            return Numeric(-self.denominator, -self.numerator)
        return Numeric(self.denominator, self.numerator)

    def same(self, other, denom, how):
        aconv = self.convert(denom, how)
        bconv = other.convert(denom, how)
        return Numeric.equal(aconv, bconv)

    @classmethod
    def error(cls, code):
        return super(Numeric, cls).__new__(cls, code, 12)
