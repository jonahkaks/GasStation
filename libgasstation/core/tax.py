import time
import uuid
from dataclasses import dataclass
from decimal import Decimal
from enum import IntEnum

from sqlalchemy import Column, VARCHAR, BIGINT, INTEGER, ForeignKey, BOOLEAN, DECIMAL, Enum
from sqlalchemy.orm import relationship, reconstructor
from sqlalchemy_utils import UUIDType

from ._declbase import DeclarativeBaseGuid, DeclarativeBase, ID_TAXTABLE, ID_BOOK
from .account import Account
from .event import *
from .object import *
from .query_private import PARAM_GUID, PARAM_BOOK

TT_NAME = "tax table name"
TT_REFCOUNT = "reference count"


class TaxAmountType(IntEnum):
    VALUE = 0
    PERCENT = 1

    def __str__(self):
        return self.name.title()


@dataclass
class TaxEntry(DeclarativeBase):
    __tablename__ = 'tax_entry'

    __table_args__ = {'sqlite_autoincrement': True}

    # column definitions
    id: int = Column('id', INTEGER(), primary_key=True, nullable=False)
    tax_guid: uuid.UUID = Column('tax_guid', UUIDType(binary=False), ForeignKey('tax.guid'), nullable=False)
    account_guid: uuid.UUID = Column('account_guid', UUIDType(binary=False), ForeignKey('account.guid'), nullable=False)
    rate: Decimal = Column('rate', DECIMAL(), nullable=False, default=Decimal(0))
    lower_scale: Decimal = Column('lower_scale', DECIMAL(), nullable=False, default=Decimal(0))
    upper_scale: Decimal = Column('upper_scale', DECIMAL(), nullable=False, default=Decimal(0))
    type: TaxAmountType = Column('type', Enum(TaxAmountType), nullable=False)

    # relation definitions
    table = relationship('Tax', back_populates='entries')
    account = relationship('Account')

    def __init__(self):
        self.type = TaxAmountType.PERCENT
        self.rate = Decimal(0)

    def set_account(self, account):
        if account is None:
            return
        if self.account == account:
            return
        self.account = account
        if self.table is not None:
            self.table.mark()
            self.table.modified()

    def set_type(self, _type: TaxAmountType):
        if self.type == _type:
            return
        self.type = _type
        if self.table is not None:
            self.table.mark()
            self.table.modified()

    def set_rate(self, rate: Decimal):
        if rate != self.rate:
            self.rate = rate
            if self.table is not None:
                self.table.mark()
                self.table.modified()

    def set_lower_scale(self, lower_scale: Decimal):
        if lower_scale != self.lower_scale:
            self.lower_scale = lower_scale
            if self.table is not None:
                self.table.mark()
                self.table.modified()

    def set_upper_scale(self, upper_scale: Decimal):
        if upper_scale != self.upper_scale:
            self.upper_scale = upper_scale
            if self.table is not None:
                self.table.mark()
                self.table.modified()

    def get_account(self):
        return self.account

    def get_type(self):
        return self.type

    def get_rate(self):
        return self.rate

    def get_table(self):
        return self.table

    def __eq__(self, other):
        if self is None and other is None:
            return True
        if self is None or other is None:
            return False
        if not self.account == other.account:
            return False
        if self.type != other.type:
            return False
        if self.rate != other.rate:
            return False
        return True

    def copy(self):
        e = TaxEntry()
        e.set_account(self.account)
        e.set_type(self.type)
        e.set_rate(self.rate)
        e.set_upper_scale(self.upper_scale)
        e.set_lower_scale(self.lower_scale)
        return e

    def destroy(self):
        self.account = None
        self.table = None

    def __repr__(self):
        return u"TaxEntry<{} {} in {}>".format(self.rate, self.type,
                                               self.account.name if self.account is not None else "")


@dataclass
class Tax(DeclarativeBaseGuid):
    __tablename__ = 'tax'

    __table_args__ = {}
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True,
                             nullable=False, default=lambda: uuid.uuid4())
    name: str = Column('name', VARCHAR(length=100), nullable=False)
    refcount: int = Column('refcount', BIGINT(), nullable=False)
    invisible: bool = Column('invisible', BOOLEAN(), nullable=False)
    parent_guid: uuid.UUID = Column('parent', UUIDType(binary=False), ForeignKey('tax.guid'))

    # relation definitions
    entries: List[TaxEntry] = relationship('TaxEntry',
                                           back_populates='table',
                                           collection_class=list,
                                           cascade='all, delete-orphan')
    children = relationship('Tax',
                            back_populates='parent',
                            cascade='all, delete-orphan')
    parent = relationship('Tax',
                          back_populates='children',
                          remote_side=guid,
                          )
    child = None
    modtime = 0

    def __init__(self, book):
        super().__init__(ID_TAXTABLE, book)
        self.child = None
        self.refcount = 0
        self.modtime = 0
        self.invisible = False
        self.add_obj(self)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_TAXTABLE, book)
        self.add_obj(self)
        Event.gen(self, EVENT_CREATE)

    def set_name(self, name):
        if name != self.name:
            self.begin_edit()
            self.name = name
            self.mark()
            self.resort()
            self.commit_edit()

    def get_name(self):
        return self.name

    def set_parent(self, parent):
        self.begin_edit()
        if self.parent is not None:
            self.parent.remove_child(self)
            self.parent = parent
            if parent is not None:
                parent.add_child(self)
            self.refcount = 0
            self.mark_invisible()
            self.mark()
            self.commit_edit()

    def set_child(self, child):
        self.begin_edit()
        self.child = child
        self.mark()
        self.commit_edit()

    def modified(self):
        self.modtime = time.mktime(time.localtime())

    def mark(self):
        self.set_dirty()
        Event.gen(self, EVENT_MODIFY)

    def resort(self):
        bi = self.book.get_data(ID_TAXTABLE)
        if bi is None:
            return
        bi.sort()

    @classmethod
    def add_obj(cls, obj):
        bi = obj.book.get_data(ID_TAXTABLE)
        if bi is None:
            return
        bi.append(obj)
        bi.sort()

    @classmethod
    def remove_obj(cls, obj):
        bi = obj.book.get_data(ID_TAXTABLE)
        if bi is None:
            return
        bi.remove(obj)

    def add_child(self, child):
        self.children.append(child)

    def remove_child(self, child):
        try:
            self.children.remove(child)
            return True
        except ValueError:
            return False

    def free(self):
        Event.gen(self, EVENT_DESTROY)
        self.remove_obj(self)
        for entry in self.entries:
            entry.destroy()
        if self.parent is not None:
            self.parent.remove_child(self)

        for child in self.children:
            child.set_parent(None)
        self.dispose()

    def increment_ref(self):
        if self.parent is not None or self.invisible:
            return
        self.begin_edit()
        self.refcount += 1
        self.mark()
        self.commit_edit()

    def decrement_ref(self):
        if self.parent is not None or self.invisible:
            return
        if self.refcount < 0:
            return
        self.begin_edit()
        self.refcount -= 1
        self.mark()
        self.commit_edit()

    def set_refcount(self, refcount):
        if refcount <= 0:
            return
        self.begin_edit()
        self.refcount = refcount
        self.mark()
        self.commit_edit()

    def mark_invisible(self):
        self.begin_edit()
        self.invisible = True
        bi = self.book.get_data(ID_TAXTABLE)
        bi.remove(self)
        self.commit_edit()

    def add_entry(self, entry):
        if entry is None:
            return
        if entry.table == self:
            return
        self.begin_edit()
        if entry.table is not None:
            entry.table.remove(entry)
        entry.table = self
        if entry not in self.entries:
            self.entries.append(entry)
        self.mark()
        self.modified()
        self.commit_edit()

    def remove_entry(self, entry):
        if entry is None:
            return
        self.begin_edit()
        self.entries.remove(entry)
        self.mark()
        self.modified()
        self.commit_edit()
        return True

    def changed(self):
        self.begin_edit()
        self.child = None
        self.commit_edit()

    def commit_edit(self):
        if not super().commit_edit():
            return
        self.commit_edit_part2(self.on_error, None, self.free)

    @classmethod
    def get_tables(cls, book):
        bi = book.get_data(ID_TAXTABLE)
        return bi

    @classmethod
    def get_list(cls, book):
        return list(filter(lambda a: a.parent is None, cls.get_tables(book)))

    @classmethod
    def lookup_by_name(cls, book, name):
        lis = cls.get_list(book)
        for t in lis:
            if t.name == name:
                return t

    def copy(self):
        t = Tax(self.book)
        t.set_name(self.name)
        for entry in self.entries:
            e = entry.copy()
            t.add_entry(e)
        return t

    def return_child(self, make_new=False):
        if self.child is not None:
            return self.child
        child = None
        if self.parent is not None or self.invisible:
            return self
        if make_new:
            child = self.copy()
            self.set_child(child)
            child.set_parent(self)
        return child

    def get_parent(self):
        return self.parent

    def get_entries(self):
        return self.entries

    def get_refcount(self):
        return self.refcount

    def last_modified_secs(self):
        return self.modtime

    def get_invisible(self):
        return self.invisible

    @classmethod
    def book_begin(cls, book):
        book.set_data(ID_TAXTABLE, [])

    def __lt__(self, other):
        if self is None and other is None:
            return False
        if self.name is None or other.name is None:
            return False
        return self.name < other.name

    @classmethod
    def register(cls):
        params = [
            Param(TT_NAME, TYPE_STRING, cls.get_name, cls.set_name),
            Param(TT_REFCOUNT, TYPE_INT64, cls.get_refcount, cls.set_refcount),
            Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
            Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None)]
        ObjectClass.register(cls, ID_TAXTABLE, None, params)

    def __str__(self):
        return u"Tax< %s >" % self.name

    def __repr__(self):
        if self.entries:
            return u"Tax<{}:{}>".format(self.name, self.entries)
        else:
            return u"Tax<{}>".format(self.name)

    def refers_to(self, other):
        if isinstance(other, Account):
            for entry in self.entries:
                if entry.account == other:
                    return True
        return False

    def referred(self, other):
        if not isinstance(other, Account):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)
