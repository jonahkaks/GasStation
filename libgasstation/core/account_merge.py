from enum import IntEnum


class AccountMergeDisposition(IntEnum):
    USE_EXISTING = 0
    CREATE_NEW = 1


class AccountMerge:
    existing_acct = None
    new_acct = None
    disposition = AccountMergeDisposition.USE_EXISTING

    @staticmethod
    def determine_account_merge_disposition(existing_acct):
        if existing_acct is None:
            return AccountMergeDisposition.CREATE_NEW
        return AccountMergeDisposition.USE_EXISTING

    @staticmethod
    def determine_merge_disposition(existing_root, new_acct):
        full_name = new_acct.get_full_name()
        existing_acct = existing_root.lookup_by_full_name(full_name)
        return AccountMerge.determine_account_merge_disposition(existing_acct)

    @staticmethod
    def account_trees_merge(existing_root, new_accts_root):
        if existing_root is None or new_accts_root is None:
            return
        children = new_accts_root.get_children()
        for new in children:
            existing_named = existing_root.lookup_by_name(new.get_name())
            f = AccountMerge.determine_account_merge_disposition(existing_named)
            if f == AccountMergeDisposition.USE_EXISTING:
                AccountMerge.account_trees_merge(existing_named, new)
            elif f == AccountMergeDisposition.CREATE_NEW:
                existing_root.append_child(new)
