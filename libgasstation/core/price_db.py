import datetime
import decimal
import itertools
import logging
import sys
from collections import defaultdict
from decimal import Decimal
from enum import IntEnum
from uuid import UUID

from sqlalchemy import DateTime, DECIMAL, Enum, ForeignKey
from sqlalchemy.orm import reconstructor, relationship

from ._declbase import *
from .event import *
from .object import *


class PriceLookupType(IntEnum):
    LATEST = 1
    ALL = 2
    AT_TIME = 3
    NEAREST_IN_TIME = 4
    LATEST_BEFORE = 5
    EARLIEST_AFTER = 6


class PriceSource(IntEnum):
    EDIT_DLG = 0
    FQ = 1
    USER_PRICE = 2
    XFER_DLG_VAL = 3
    SPLIT_REG = 4
    STOCK_SPLIT = 5
    INVOICE = 6
    TEMP = 7
    INVALID = -1

    def __str__(self):
        return ["user:price-editor",
                "Finance::Quote",
                "user:price",
                "user:xfer-dialog",
                "user:split-register",
                "user:stock-split",
                "user:invoice-post",
                "temporary",
                "invalid"][self.value]


class PriceType(IntEnum):
    LAST = 0
    UNK = 1
    TRN = 2

    def __str__(self):
        return ["last", "unknown", "transaction"][self.value]


CURRENCY_DENOM = 10000
COMMODITY_DENOM_MULT = 10000


class PriceRemoveSourceFlags(IntEnum):
    FQ = 1
    USER = 2
    APP = 4
    COMM = 8


class PriceRemoveKeepOptions(IntEnum):
    NONE = 0
    LAST_WEEKLY = 1
    LAST_MONTHLY = 2
    LAST_QUARTERLY = 3
    LAST_PERIOD = 4
    SCALED = 5


PRICE_COMMODITY = "price-commodity"
PRICE_CURRENCY = "price-currency"
PRICE_DATE = "price-date"
PRICE_SOURCE = "price-source"
PRICE_TYPE = "price-type"
PRICE_VALUE = "price-value"


class PriceDBForeachData:
    ok = False
    func = None
    user_data = None


class PriceListForeachData:
    ok = False
    func = None
    user_data = None


def price_list_from_hashtable(h, currency):
    if currency is not None:
        price_list = h.get(currency)
        if price_list is None:
            return PriceList()
        return price_list
    else:
        a = PriceList()
        for s in h.values():
            a.extend(s)
        return a


@dataclass
class Price(DeclarativeBaseGuid):
    """
    A single Price for a commodity.

    Attributes:
        commodity (:class:`Commodity`): commodity to which the Price relates
        currency (:class:`Commodity`): currency in which the Price is expressed
        date (:class:`datetime.date`): date object representing the day at which the price is relevant
        source (str): source of the price
        type (str): last, ask, bid, unknown, nav
        value (:class:`decimal.Decimal`): the price itself
    """
    __tablename__ = 'price'

    __table_args__ = {}

    # column definitions
    commodity_guid: UUID = Column('commodity_guid', UUIDType(binary=False), ForeignKey('commodity.guid'),
                                  nullable=False)
    currency_guid: UUID = Column('currency_guid', UUIDType(binary=False), ForeignKey('commodity.guid'), nullable=False)
    date: datetime.datetime = Column('date', DateTime(timezone=False), nullable=False)
    source: PriceSource = Column('source', Enum(PriceSource), default=PriceSource.USER_PRICE, nullable=False)
    type: PriceType = Column('type', Enum(PriceType), nullable=False, default=PriceType.TRN)
    value: Decimal = Column('value', DECIMAL(8, 2), nullable=False)
    refcount: int = 1
    # relation definitions
    commodity = relationship('Commodity',
                             back_populates="prices",
                             foreign_keys=[commodity_guid],
                             )
    currency = relationship('Commodity',
                            foreign_keys=[currency_guid],
                            )

    def __init__(self, book):
        super().__init__(ID_PRICE, book)
        self.refcount = 1
        self.value = Decimal(0)
        self.type = PriceType.TRN
        self.source = PriceSource.INVALID
        self.db = None
        self.date = datetime.datetime.now()
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        self.refcount = 1
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        super().__init__(ID_PRICE, book)
        pricedb = PriceDB.get_db(book)
        pricedb.add_price(self)
        self.unref()

    def destroy(self):
        super().destroy()
        Event.gen(self, EVENT_DESTROY)

    def ref(self):
        self.refcount += 1

    def unref(self):
        if self.refcount == 0:
            return
        self.refcount -= 1
        if self.refcount <= 0:
            self.destroy()

    def set_dirty(self):
        super().set_dirty()
        Event.gen(self, EVENT_MODIFY)

    def noop(self):
        pass

    def commit_edit(self):
        if not super().commit_edit():
            return
        self.commit_edit_part2(self.on_error, self.noop, self.noop)

    def set_commodity(self, c):
        if self.commodity is None or not self.commodity.equiv(c):
            self.ref()
            PriceDB._remove_price(self.db, self, True)
            self.begin_edit()
            self.commodity = c
            self.set_dirty()
            self.commit_edit()
            PriceDB._add_price(self.db, self)
            self.unref()

    def set_currency(self, c):
        if self.commodity is None or not self.commodity.equiv(c):
            self.ref()
            PriceDB._remove_price(self.db, self, True)
            self.begin_edit()
            self.currency = c
            self.set_dirty()
            self.commit_edit()
            PriceDB._add_price(self.db, self)
            self.unref()

    def set_time64(self, t: datetime.datetime):
        if t != self.date and t is not None:
            self.ref()
            PriceDB._remove_price(self.db, self, False)
            self.begin_edit()
            self.date = t
            self.set_dirty()
            self.commit_edit()
            PriceDB._add_price(self.db, self)
            self.unref()

    def set_source(self, s: PriceSource):
        if s is not None and self.source != s:
            self.begin_edit()
            self.source = PriceSource(s)
            self.set_dirty()
            self.commit_edit()

    def set_type(self, _type: PriceType):
        if self.type != _type:
            self.begin_edit()
            self.type = _type
            self.set_dirty()
            self.commit_edit()

    def set_value(self, value: Decimal):
        if value is None:
            return
        if self.value != value:
            self.begin_edit()
            self.value = value
            self.set_dirty()
            self.commit_edit()

    @staticmethod
    def lookup(guid, book):
        if not guid or not book: return None
        col = book.get_collection(book, ID_PRICE)
        for price in col:
            if price.guid == guid:
                return price

    def get_commodity(self):
        return self.commodity

    def get_time64(self) -> datetime.datetime:
        return self.date

    def get_source(self) -> PriceSource:
        return self.source

    def get_source_string(self):
        return str(self.source or PriceSource.INVALID)

    def get_type_string(self) -> str:
        return str(self.type)

    def get_type(self) -> PriceType:
        return self.type

    def get_value(self) -> Decimal:
        return self.value

    def get_currency(self):
        return self.currency

    def equal(self, other):
        if self == other: return True
        if self is None or other is None: return False
        if not self.get_commodity().equal(other.get_commodity()): return False
        if not self.get_currency().equal(other.get_currency()): return False
        time1 = self.get_time64()
        time2 = other.get_time64()
        if time1 != time2: return False
        if self.get_source() != other.get_source(): return False
        if self.get_type_string() != other.get_type_string(): return False
        if self.get_value() != other.get_value(): return False
        return True

    def __eq__(self, other):
        if self is None and other is None:
            return 0
        if self is None:
            return -1
        if other is None:
            time_b = 0
        else:
            time_b = other.get_time64()
        time_a = self.get_time64()
        return time_a == time_b

    def __lt__(self, other):
        if self is None and other is None:
            return False
        if self is None:
            return False
        time_a = self.get_time64()
        time_b = other.get_time64()
        return time_a < time_b

    def commodity_and_currency_equal(self, other):
        from .commodity import Commodity
        if other is None:
            return False
        ret_comm = False
        ret_curr = False
        if Commodity.equal(self.get_commodity(), other.get_commodity()):
            ret_comm = True
        if Commodity.equal(self.get_currency(), other.get_currency()):
            ret_curr = True
        return ret_comm and ret_curr

    def compare_by_commodity_date(self, other):
        if self is None and other is None: return 0
        if self is None: return -1
        if other is None: return 1
        comma = self.get_commodity()
        commb = other.get_commodity()
        if not comma.equal(commb):
            return comma.compare(commb)
        curra = self.get_currency()
        currb = other.get_currency()
        if not curra.equal(currb):
            return curra.compare(currb)
        time_a = self.get_time64()
        time_b = other.get_time64()
        result = time_b == time_a
        if result: return result
        return self.guid == other.guid

    def clone(self, book):
        if book is None: return
        new_p = Price(book)
        new_p.begin_edit()
        new_p.set_commodity(self.get_commodity())
        new_p.set_time64(self.get_time64())
        new_p.set_source(self.get_source())
        new_p.set_type(self.get_type())
        new_p.set_value(self.get_value())
        new_p.set_currency(self.get_currency())
        new_p.commit_edit()
        return new_p

    @classmethod
    def register(cls):
        params = [
            Param(PRICE_COMMODITY, ID_COMMODITY, cls.get_commodity, cls.set_commodity),
            Param(PRICE_CURRENCY, ID_COMMODITY, cls.get_currency, cls.set_currency),
            Param(PRICE_DATE, TYPE_DATE, cls.get_time64, cls.set_time64),
            Param(PRICE_SOURCE, TYPE_STRING, cls.get_source, cls.set_source),
            Param(PRICE_TYPE, TYPE_STRING, cls.get_type_string, cls.set_type),
            Param(PRICE_VALUE, TYPE_NUMERIC, cls.get_value, cls.set_value)]
        ObjectClass.register(cls, ID_PRICE, None, params)

    def refers_to(self, other):
        pass

    def __repr__(self):
        return u"Price<{:%Y-%m-%d} : {} {}/{}>".format(self.date,
                                                       self.value,
                                                       self.currency.mnemonic if self.currency is not None else "",
                                                       self.commodity.mnemonic if self.commodity is not None else "")


class PriceList(list):
    class UsesCommodity:
        def __init__(self):
            self.l: List[Price] = []
            self.com = None
            self.t = datetime.datetime.now()

    @staticmethod
    def is_duplicate(price_a: Price, price_b: Price):
        time_a, time_b = price_a.get_time64().replace(hour=12, minute=0, second=0), \
                         price_b.get_time64().replace(hour=12, minute=0, second=0)
        if price_a.get_value() != price_b.get_value():
            return False
        if price_a.get_commodity() != price_b.get_commodity():
            return False
        if price_a.get_currency() != price_b.get_currency():
            return False
        if time_a != time_b:
            return False
        return True

    def insert(self, p: Price, check_duplicate: bool):
        if p is None or not isinstance(p, Price): return False
        p.ref()
        if check_duplicate:
            for s in self:
                if self.is_duplicate(p, s):
                    return
        self.append(p)
        self.sort(reverse=True)
        return True

    def remove(self, p):
        if p is None:
            return False
        if p not in self:
            return False
        super().remove(p)
        p.unref()
        return True

    def destroy(self):
        for price in self:
            if price is not None:
                price.unref()
        self.clear()
        del self

    def __eq__(self, other):
        if other is None:
            return False
        if self == other:
            return True
        if len(self) != len(other):
            return False
        for s in sorted(self):
            for p in sorted(other):
                if not s.equal(p):
                    return False
        return True

    def scan_any_currency(self, helper: UsesCommodity):
        if helper is None:
            helper = self.UsesCommodity()
        if len(self) == 0:
            return True
        node = self[0]
        com = node.get_commodity()
        cur = node.get_currency()
        if com != helper.com and cur != helper.com:
            return True
        for i, price in enumerate(self):
            price_t = price.get_time64()
            if price_t < helper.t:
                if i > 0:
                    prev_price = self[i - 1]
                    prev_price.ref()
                    helper.l.insert(0, prev_price)
                price.ref()
                helper.l.insert(0, price)
                break
            elif i + 1 < len(self):
                price.ref()
                helper.l.insert(0, price)
        return True


class PriceDB(Instance):
    def refers_to(self, other):
        pass

    class RemoveInfo:
        def __init__(self):
            self.db = None
            self.cutoff: datetime.datetime = datetime.datetime.now()
            self.delete_fq = False
            self.delete_user = False
            self.delete_app = False
            self.l = []

    class PriceTuple:
        def __init__(self, to=None, fro=None):
            self.to = to
            self.fro = fro

    def __init__(self, book):
        super().__init__(ID_PRICEDB, book)
        self.commodity_hash = None
        self.bulk_update = False

    @classmethod
    def create(cls, book):
        col = book.get_collection(ID_PRICEDB)
        result = col.get_data()
        if result is not None:
            return result
        result = cls(book)
        col.mark_clean()
        col.set_data(result)
        result.commodity_hash = defaultdict(dict)
        return result

    @staticmethod
    def destroy_currency_hash_data(price_list):
        for price in price_list:
            price.db = None
        price_list.destroy()

    @staticmethod
    def destroy_commodity_hash_data(currency_hash):
        if not currency_hash: return
        for v in currency_hash.values():
            PriceDB.destroy_currency_hash_data(v)
        currency_hash.clear()

    def destroy(self):
        if self.commodity_hash is None: return
        for k in self.commodity_hash.values():
            self.destroy_commodity_hash_data(k)
        self.commodity_hash.clear()
        self.commodity_hash = None

    def set_bulk_update(self, bulk_update):
        self.bulk_update = bulk_update

    @staticmethod
    def get_db(book):
        if book is None:
            return None
        col = book.get_collection(ID_PRICEDB)
        return col.get_data() if col is not None  else None

    def _add_price(self, p):
        if p is None or self is None:
            return False
        if self.book != p.book:
            return False
        commodity = p.get_commodity()
        if commodity is None:
            return False
        currency = p.get_currency()
        if currency is None:
            return False
        if self.commodity_hash is None:
            return False
        old_price = self.lookup_day_t64(p.get_commodity(), p.get_currency(), p.get_time64())
        if not self.bulk_update and old_price is not None:
            if p.source > old_price.source:
                p.unref()
                return False
            self.remove_price(old_price)
        currency_hash = self.commodity_hash.get(commodity)
        if currency_hash is None:
            self.commodity_hash[commodity][currency] = PriceList()
            currency_hash = self.commodity_hash.get(commodity)
        price_list = currency_hash.get(currency)
        if not price_list.insert(p, not self.bulk_update):
            return False
        p.db = self
        Event.gen(p, EVENT_ADD)
        return True

    def add_price(self, p):
        if self is None or not p or p is None:
            return False
        if self._add_price(p):
            return False
        self.begin_edit()
        self.set_dirty()
        self.commit_edit()
        return True

    def _remove_price(self, p, cleanup):
        if self is None or not p or p is None:
            return False
        commodity = p.get_commodity()
        if commodity is None:
            return False
        currency = p.get_currency()
        if currency is None:
            return False
        if self.commodity_hash is None:
            return False
        currency_hash = self.commodity_hash.get(commodity)
        if currency_hash is None: return False
        Event.gen(p, EVENT_REMOVE)
        price_list = currency_hash.get(currency)
        p.ref()
        if price_list is None or not price_list.remove(p):
            p.unref()
            return False

        if price_list is not None:
            currency_hash[currency] = price_list
        else:
            currency_hash.pop(currency)

            if cleanup:
                num_currencies = len(currency_hash)
                if 0 == num_currencies:
                    self.commodity_hash.pop(commodity)
                    currency_hash.clear()
        p.unref()
        return True

    def remove_price(self, p):
        if self is None or p is None:
            return False
        p.ref()
        rc = self._remove_price(p, True)
        self.begin_edit()
        self.set_dirty()
        self.commit_edit()
        p.begin_edit()
        p.set_destroying(True)
        p.commit_edit()
        p.db = None
        p.unref()
        return rc

    @staticmethod
    def check_one_price_date(price: Price, data: RemoveInfo):
        source = price.get_source()
        if source == PriceSource.FQ and data.delete_fq:
            logging.info("Delete Quote Source")
        elif source == PriceSource.USER_PRICE and data.delete_user:
            logging.info("Delete User Source")
        elif source != PriceSource.FQ and source != PriceSource.USER_PRICE and data.delete_app:
            logging.info("Delete App Source")
        else:
            logging.info("Not a matching source")
            return True
        t = price.get_time64()
        if t < data.cutoff:
            data.l.insert(0, price)
        return True

    @staticmethod
    def remove_foreach_pricelist(val, data):
        for p in val:
            PriceDB.check_one_price_date(p, data)

    @staticmethod
    def roundUp(numToRound, multiple):
        if multiple == 0:
            return numToRound
        remainder = numToRound % multiple
        if remainder == 0:
            return numToRound
        return numToRound + multiple - remainder

    @staticmethod
    def get_fiscal_quarter(date, fiscal_start):
        return ((PriceDB.roundUp(22 - fiscal_start + date.month, 3) / 3) % 4) + 1

    @staticmethod
    def set_fiscal_year_end(date, fy_end):

        if date is None or fy_end is None: return
        temp = fy_end
        temp = temp.replace(year=fy_end.year)
        new_fy = date > temp
        date = temp
        if new_fy:
            return date.replace(year=date.year + 1)

    def process_removal_list(self, fiscal_end_date: datetime.datetime, data: RemoveInfo, keep: PriceRemoveKeepOptions):
        saved_test_value = 0
        next_test_value = 0
        cloned_price = None
        tmp_date = fiscal_end_date
        tmp_date = tmp_date.replace(year=tmp_date.year + 1)
        fiscal_month_start = tmp_date.month + 1
        data.l.sort(key=lambda p: p.date)
        for price in data.l:
            if keep == PriceRemoveKeepOptions.NONE:
                self.remove_price(price)
                continue
            save_first_price = not price.commodity_and_currency_equal(cloned_price)
            if save_first_price:
                cloned_price = price.clone(price.book)
                continue
            saved_price_date = datetime.date.fromtimestamp(cloned_price.get_time64())
            next_price_date = datetime.date.fromtimestamp(price.get_time64())

            if keep == PriceRemoveKeepOptions.LAST_PERIOD and not save_first_price:
                saved_fiscal_end = saved_price_date
                next_fiscal_end = next_price_date

                saved_fiscal_end = self.set_fiscal_year_end(saved_fiscal_end, fiscal_end_date)
                next_fiscal_end = self.set_fiscal_year_end(next_fiscal_end, fiscal_end_date)

                saved_test_value = saved_fiscal_end.year
                next_test_value = next_fiscal_end.year

            if keep == PriceRemoveKeepOptions.LAST_QUARTERLY and not save_first_price:
                saved_test_value = self.get_fiscal_quarter(saved_price_date, fiscal_month_start)
                next_test_value = self.get_fiscal_quarter(next_price_date, fiscal_month_start)

            if keep == PriceRemoveKeepOptions.LAST_MONTHLY and not save_first_price:
                saved_test_value = saved_price_date.month
                next_test_value = next_price_date.month

            if keep == PriceRemoveKeepOptions.LAST_WEEKLY and not save_first_price:
                saved_test_value = saved_price_date.isoweekday()
                next_test_value = next_price_date.isoweekday()

            if saved_test_value == next_test_value:
                self.remove_price(price)
            else:
                cloned_price = price.clone(price.book)

            if cloned_price:
                cloned_price.unref()

    def remove_old_prices(self, comm_list, fiscal_end_date: datetime.datetime,
                          cutoff: datetime.datetime, source: PriceSource, keep: PriceRemoveKeepOptions):
        data = self.RemoveInfo()
        data.db = self
        data.cutoff = cutoff
        data.delete_fq = False
        data.delete_user = False
        data.delete_app = False

        if source & PriceRemoveSourceFlags.APP:
            data.delete_app = True

        if source & PriceRemoveSourceFlags.FQ:
            data.delete_fq = True

        if source & PriceRemoveSourceFlags.USER:
            data.delete_user = True

        for node in comm_list:
            currencies_hash = self.commodity_hash.get(node, {})
            for k, v in currencies_hash.items():
                self.remove_foreach_pricelist(v, data)
        if data.l is None or len(data.l) == 0:
            return False
        if fiscal_end_date is None:
            fiscal_end_date = datetime.date.today().replace(day=31, month=12)
        self.process_removal_list(fiscal_end_date, data, keep)
        return True

    def get_prices_internal(self, commodity, currency, bidi):
        forward_list = None
        reverse_hash = None
        if commodity is None: return None
        forward_hash = self.commodity_hash.get(commodity)
        if currency is not None and bidi:
            reverse_hash = self.commodity_hash.get(currency)
        if forward_hash is None and reverse_hash is None:
            return PriceList()
        if forward_hash is not None:
            forward_list = price_list_from_hashtable(forward_hash, currency)
        if currency is not None and reverse_hash is not None:
            reverse_list = price_list_from_hashtable(reverse_hash, commodity)
            if reverse_list:
                if forward_list:
                    merged_list = forward_list + reverse_list
                    forward_list = merged_list
                else:
                    forward_list = reverse_list
        return forward_list

    def lookup_latest(self, commodity, currency):
        if commodity is None or currency is None:
            return None
        price_list = self.get_prices_internal(commodity, currency, True)
        if not any(price_list):
            return None
        result = price_list[0]
        result.unref()
        return result

    @staticmethod
    def latest_before(prices, target, t):
        found_coms = []
        retval = []
        for price in prices:
            com = price.get_commodity()
            cur = price.get_currency()
            price_t = price.get_time64()
            if t < price_t or com == target and cur in found_coms or cur == target and com in found_coms:
                continue
            else:
                price.ref()
                retval.insert(0, price)
                found_coms.insert(0, cur if com == target else com)
        retval.reverse()
        return PriceList(retval)

    @staticmethod
    def find_comtime(array, com):
        for price_p in array:
            if price_p.get_commodity() == com or price_p.get_currency() == com:
                return price_p
        return None

    @staticmethod
    def add_nearest_price(target_list, price_array, price, target, t):
        if price is None or not isinstance(price, Price): return
        com = price.get_commodity()
        cur = price.get_currency()
        price_t = price.get_time64()
        other = cur if com == target else com
        com_price = PriceDB.find_comtime(price_array, other)
        if com_price is None:
            com_price = price
            price_array.append(com_price)
            if price_t <= t:
                price.ref()
                target_list.insert(0, price)
            return target_list

        com_t = com_price.get_time64()
        if com_t <= t:
            return target_list
        if price_t > t:
            i = price_array.index(com_price)
            price_array[i] = price
        else:
            com_diff = com_t - t
            price_diff = t - price_t
            if com_diff < price_diff:
                com_price.ref()
                target_list.insert(0, com_price)
            else:
                price.ref()
                target_list.insert(0, com_price)
            i = price_array.index(com_price)
            price_array[i] = price
        return target_list

    @staticmethod
    def nearest_to(prices, target, t):
        price_array = []
        retval = []
        for price in prices:
            retval = PriceDB.add_nearest_price(retval, price_array, price, target, t)

        for com_price in price_array:
            price_t = com_price.get_time64()
            if price_t >= t:
                com_price.ref()
                retval.insert(0, com_price)
        return sorted(retval, key=lambda p: p.date)

    def lookup_latest_any_currency(self, commodity):
        return self.lookup_latest_before_any_currency_t64(commodity, datetime.datetime.now())

    def lookup_nearest_in_time_any_currency_t64(self, commodity, t):
        prices = PriceList()
        helper = prices.UsesCommodity()
        helper.list = prices
        helper.com = commodity
        helper.t = datetime.datetime.combine(t, datetime.time(0, 0, 0))
        if commodity is None: return PriceList()
        self.pricelist_traversal(PriceList.scan_any_currency, helper)
        prices = PriceList(sorted(prices, key=lambda p: p.date))
        result = self.nearest_to(prices, commodity, t)
        prices.destroy()
        return result

    def lookup_latest_before_any_currency_t64(self, commodity, t):
        prices = PriceList()
        helper = prices.UsesCommodity()
        helper.list = prices
        helper.com = commodity
        helper.t = datetime.datetime.combine(t, datetime.time(0, 0, 0))
        if commodity is None: return PriceList()
        self.pricelist_traversal(PriceList.scan_any_currency, helper)
        prices = PriceList(sorted(prices, key=lambda p: p.date))
        result = self.latest_before(prices, commodity, t)
        prices.destroy()
        return result

    def has_prices(self, commodity, currency):
        if commodity is None: return False
        currency_hash = self.commodity_hash.get(commodity)
        if currency_hash is None: return False
        if currency:
            price_list = currency_hash.get(currency)
            if price_list:
                return True
            return False
        return any(currency_hash)

    def get_prices(self, commodity, currency):
        if commodity is None:
            return PriceList()
        result = self.get_prices_internal(commodity, currency, False)
        if result is None: return PriceList()
        for p in result:
            p.ref()
        return result

    def get_num_prices(self, c):
        return len(self.commodity_hash.get(c, []))

    def nth_price(self, c, n):
        if n < 0: return None
        currency_hash = self.commodity_hash.get(c)
        if currency_hash is not None:
            itertools.chain.from_iterable(currency_hash.values())
            try:
                return list(itertools.chain.from_iterable(currency_hash.values()))[n]
            except IndexError:
                return None
        return None

    def lookup_day_t64(self, c, currency, t):
        return self.lookup_nearest_in_time(c, currency, t, True)

    def lookup_at_time64(self, c, currency, t):
        if c is None or currency is None: return
        price_list = self.get_prices_internal(c, currency, True)
        for p in price_list:
            price_time = p.get_time64()
            if price_time == t:
                p.ref()
                return p
        return None

    def lookup_nearest_in_time(self, c, currency, t, sameday):
        if c is None or currency is None: return
        price_list = self.get_prices_internal(c, currency, True)
        if not price_list: return None
        current_price = price_list[0]
        next_price = None
        result = None
        if isinstance(t, datetime.date):
            t = datetime.datetime.combine(t, datetime.time(hour=10, minute=59))

        for price in price_list:
            price_time = price.get_time64()
            if price_time <= t:
                next_price = price
                break
            current_price = price
        if current_price is not None:
            if next_price is None:
                result = current_price
                if sameday:
                    price_day = current_price.get_time64()
                    t_day = t
                    if price_day != t_day:
                        result = None
            else:
                current_t = current_price.get_time64()
                next_t = next_price.get_time64()
                diff_current = current_t - t
                diff_next = next_t - t
                abs_current = abs(diff_current)
                abs_next = abs(diff_next)
                if sameday:
                    t_day = t
                    current_day = current_t
                    next_day = next_t
                    if current_day == t_day:
                        if next_day == t_day:
                            if abs_current < abs_next:
                                result = current_price
                            else:
                                result = next_price
                        else:
                            result = current_price
                    elif next_day == t_day:
                        result = next_price
                else:
                    if abs_current < abs_next:
                        result = current_price
                    else:
                        result = next_price
        if result is not None:
            result.ref()
        return result

    def lookup_nearest_in_time64(self, c, currency, t):
        return self.lookup_nearest_in_time(c, currency, t, False)

    def lookup_latest_before_t64(self, c, currency, t):
        current_price = None
        if c is None or currency is None: return
        price_list = self.get_prices_internal(c, currency, True)
        if not price_list or len(price_list) == 0: return None
        price_time = price_list[0].get_time64()
        if price_time <= t:
            current_price = price_list[0]
        for price in price_list[1:]:
            price_time = price.get_time64()
            if price_time <= t:
                current_price = price
            if price_time <= t: break
        if current_price:
            current_price.ref()
        return current_price

    def direct_balance_conversion(self, bal, fro, to, t):
        retval = Decimal(0)
        if fro is None or to is None or bal.is_zero(): return retval

        if t != sys.maxsize:
            price = self.lookup_nearest_in_time64(fro, to, t)
        else:
            price = self.lookup_latest(fro, to)
        if price is None:
            return retval
        if price.get_commodity() == fro:
            retval = bal * price.get_value()
        else:
            retval = bal / price.get_value()
        price.unref()
        return retval

    @classmethod
    def extract_common_prices(cls, from_prices, to_prices, fro, to):
        from_price = None
        to_price = None
        retval = cls.PriceTuple()
        for f in from_prices:
            from_price = f
            for t in to_prices:
                to_price = t
                to_com = to_price.get_commodity()
                to_cur = to_price.get_currency()
                from_com = from_price.get_commodity()
                from_cur = from_price.get_currency()
                if (((to_com == from_com or to_com == from_cur) and
                     (to_com != fro and to_com != to)) or
                        ((to_cur == from_com or to_cur == from_cur) and
                         (to_cur != fro and to_cur != to))):
                    break
                to_price = None
                from_price = None
            if to_price is not None and from_price is not None:
                break
        if from_price is None or to_price is None:
            return retval
        from_price.ref()
        to_price.ref()
        retval = cls.PriceTuple(from_price, to_price)
        return retval

    @staticmethod
    def convert_balance(bal: Decimal, fro, to, tup: PriceTuple) -> Decimal:
        if tup is None: return Decimal(0)
        from_com = tup.fro.get_commodity()
        from_cur = tup.fro.get_currency()
        to_com = tup.to.get_commodity()
        to_cur = tup.to.get_currency()
        from_val = tup.fro.get_value()
        to_val = tup.to.get_value()

        if from_cur == fro and to_cur == to:
            return (bal * to_val) / from_val
        if from_com == fro and to_com == to:
            return (bal * from_val) / to_val
        if from_cur == fro:
            return bal / (from_val * to_val)
        return bal * from_val * to_val

    def indirect_balance_conversion(self, bal: Decimal, fro, to, t) -> Decimal:
        to_prices = None
        zero = Decimal(0)
        if fro is None or to is None: return zero
        if bal.is_zero():
            return zero
        if t == sys.maxsize:
            from_prices = self.lookup_latest_any_currency(fro)
            if from_prices:
                to_prices = self.lookup_latest_any_currency(to)
        else:
            from_prices = self.lookup_nearest_in_time_any_currency_t64(fro, t)
            if from_prices:
                to_prices = self.lookup_nearest_in_time_any_currency_t64(to, t)

        if from_prices is None or to_prices is None:
            return zero
        tup = self.extract_common_prices(from_prices, to_prices, fro, to)
        from_prices.destroy()
        to_prices.destroy()
        if tup.fro:
            return self.convert_balance(bal, fro, to, tup)
        return zero

    def convert_balance_latest_price(self, balance, balance_currency, new_currency):
        if balance is None or balance_currency is None or new_currency is None: return Decimal(0)
        if balance.is_zero() or balance_currency.equal(new_currency): return balance
        new_value = self.direct_balance_conversion(balance, balance_currency, new_currency, sys.maxsize)
        if not new_value.is_zero():
            return new_value
        return self.indirect_balance_conversion(balance, balance_currency, new_currency, sys.maxsize)

    def convert_balance_nearest_price_t64(self, balance, balance_currency, new_currency, t):
        if balance.is_zero() or balance_currency.equal(new_currency): return balance
        new_value = self.direct_balance_conversion(balance, balance_currency, new_currency, t)
        if not new_value.is_zero():
            return new_value
        return self.indirect_balance_conversion(balance, balance_currency, new_currency, t)

    @staticmethod
    def foreach_pricelist(price_list, foreach_data):
        for p in price_list:
            if foreach_data.ok:
                foreach_data.ok = foreach_data.func(p, foreach_data.user_data)

    @staticmethod
    def foreach_currencies_hash(currencies_hash, user_data):
        for v in currencies_hash.values():
            PriceDB.foreach_pricelist(v, user_data)

    def unstable_price_traversal(self, f, user_data):
        foreach_data = PriceDBForeachData()
        if f is None: return False
        foreach_data.ok = True
        foreach_data.func = f
        foreach_data.user_data = user_data
        if self.commodity_hash is None: return False
        for v in self.commodity_hash.values():
            self.foreach_currencies_hash(v, foreach_data)
        return foreach_data.ok

    @staticmethod
    def pricelist_foreach_pricelist(price_list, foreach_data):
        if foreach_data.ok:
            foreach_data.ok = foreach_data.func(price_list, foreach_data.user_data)

    @staticmethod
    def pricelist_foreach_currencies_hash(currencies_hash, user_data):
        for v in currencies_hash.values():
            PriceDB.pricelist_foreach_pricelist(v, user_data)

    def pricelist_traversal(self, f, user_data):
        foreach_data = PriceListForeachData()
        if f is None: return False
        foreach_data.ok = True
        foreach_data.func = f
        foreach_data.user_data = user_data
        if self.commodity_hash is None: return False
        for k in self.commodity_hash.values():
            self.pricelist_foreach_currencies_hash(k, foreach_data)
        return foreach_data.ok

    @classmethod
    def book_begin(cls, book):
        cls.create(book)

    @classmethod
    def book_end(cls, book):
        col = book.get_collection(ID_PRICEDB)
        db = col.get_data()
        col.set_data(None)
        if db is None:
            return
        db.destroy()

    def __repr__(self):
        return "<PriceDB>"

    @classmethod
    def register(cls):
        Price.register()
        ObjectClass.register(cls, ID_PRICEDB, None, [])
