import enum

from sqlalchemy import Date, DECIMAL, DATE
from sqlalchemy.orm import reconstructor

from .account import AccountType
from .location import Location
from .payment import PaymentMethod
from .person import *
from .price_db import PriceDB
from .scrub import Scrub, EquityType
from .tax import Tax


class StaffEarningType(enum.Enum):
    SALARY = "Basic Salary"
    BONUS = "Bonus"
    ALLOWANCE = "Allowance"


class StaffPayment(DeclarativeBase):
    __tablename__ = "staff_payment"
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True,
                             nullable=False, default=lambda: uuid.uuid4())
    staff_guid = Column('staff_guid', UUIDType(binary=False), ForeignKey('staff.guid'), nullable=False)
    payslip_guid = Column('payslip_guid', UUIDType(binary=False), ForeignKey('payslip.guid'))
    payment_guid = Column('payment_guid', UUIDType(binary=False), ForeignKey('payment.guid'), primary_key=True,
                          nullable=False)
    amount: Decimal = Column("amount", DECIMAL, nullable=False, default=Decimal(0))
    staff = relationship("Staff", back_populates="payments")
    payslip = relationship("PaySlip", back_populates="payments")
    payment: Payment = relationship("Payment", cascade="delete")

    def __init__(self, payment):
        self.payment = payment


class StaffEarning(DeclarativeBaseGuid):
    __tablename__ = 'staff_earning'
    payslip_guid: str = Column("payslip_guid", UUIDType(binary=False), ForeignKey('payslip.guid'), nullable=False)
    type = Column("type", Enum(StaffEarningType), default=StaffEarningType.SALARY, nullable=False)
    amount = Column("amount", DECIMAL, nullable=False)
    payslip = relationship("PaySlip", back_populates="earnings")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class StaffDeduction(DeclarativeBase):
    __tablename__ = 'staff_deduction'
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True,
                             nullable=False, default=lambda: uuid.uuid4())
    payslip_guid: str = Column("payslip_guid", UUIDType(binary=False), ForeignKey('payslip.guid'), nullable=False)
    name = Column("name", VARCHAR(50), nullable=False)
    amount = Column("amount", DECIMAL, nullable=False)
    payslip = relationship("PaySlip", back_populates="deductions")


class StaffContribution(DeclarativeBase):
    __tablename__ = 'staff_contribution'
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True,
                             nullable=False, default=lambda: uuid.uuid4())
    payslip_guid: str = Column("payslip_guid", UUIDType(binary=False), ForeignKey('payslip.guid'), nullable=False)
    amount = Column("amount", DECIMAL, nullable=False)
    payslip = relationship("PaySlip", back_populates="contributions")


class PaySlip(DeclarativeBaseGuid):
    __tablename__ = "payslip"
    staff_guid: str = Column("staff_guid", UUIDType(binary=False), ForeignKey('staff.guid'), nullable=False)
    date: datetime.date = Column("date", DATE(), nullable=False)
    reference: str = Column("reference", VARCHAR(40))
    notes: str = Column("notes", VARCHAR(200))
    staff = relationship("Staff", back_populates="payslips")
    earnings = relationship("StaffEarning", back_populates="payslip")
    deductions = relationship("StaffDeduction", back_populates="payslip")
    contributions = relationship("StaffContribution", back_populates="payslip")
    payments: List[StaffPayment] = relationship('StaffPayment', cascade="delete", back_populates="payslip")


@dataclass
class Staff(Person):
    __tablename__ = 'staff'

    __table_args__ = {}
    payment_method_guid: str = Column("payment_method_guid", UUIDType(binary=False), ForeignKey('payment_method.guid'),
                                      nullable=False)
    currency_guid: str = Column("currency_guid", UUIDType(binary=False), ForeignKey('commodity.guid'), nullable=False)
    tax_guid: str = Column("tax_guid", UUIDType(binary=False), ForeignKey('tax.guid'))
    location_guid: str = Column("location_guid", UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    next_of_kin_guid = Column("next_of_kin_guid", UUIDType(binary=False), ForeignKey("contact.guid"))
    date_of_hire: datetime.date = Column('date_of_hire', Date(), nullable=False)
    date_of_birth: datetime.date = Column('date_of_birth', Date(), nullable=False)
    tin_number = Column("tin_number", VARCHAR(25))
    id_number = Column("id_number", VARCHAR(60))
    ss_number = Column("ss_number", VARCHAR(50))
    salary_advance_limit = Column("salary_advance_limit", DECIMAL)
    location = relationship("Location", back_populates="staff")
    payments: List[StaffPayment] = relationship('StaffPayment', cascade="delete", back_populates="staff")
    next_of_kin = relationship("Contact", lazy="joined", foreign_keys=[next_of_kin_guid])
    tax = relationship("Tax")
    payment_method = relationship("PaymentMethod")
    payslips = relationship("PaySlip", back_populates="staff")
    balance = None

    def __init__(self, book):
        super().__init__(ID_STAFF, book)
        self._id = book.increment_and_format_counter("Staff")
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        self.balance = None
        self.infant = False
        Instance.__init__(self, ID_STAFF, book)
        Event.gen(self, EVENT_CREATE)

    def refers_to(self, other):
        if isinstance(other, Account):
            return other == self.get_clearing_account()
        elif isinstance(other, Location):
            return self.location == other
        elif isinstance(other, Tax):
            return self.tax == other
        elif isinstance(other, PaymentMethod):
            return self.payment_method == other
        return False

    def get_upapplied_payments(self):
        payslip_balance = sum([payslip.get_total() for payslip in self.payslips])
        payments = sum(map(lambda a: a.amount, set(cpayment.payment for cpayment in self.payments)))
        balance = payslip_balance - payments
        for payment in self.payments:
            if payment.payslip is None:
                yield payment.payment, balance

    def get_clearing_account(self):
        book = self.book
        return Scrub.get_or_make_account_with_fullname_parent(book.get_root_account(),
                                                              book.default_currency,
                                                              "Employee Clearing Account",
                                                              AccountType.CURRENT_LIABILITY,
                                                              "Liabilities", "Current Liabilities")

    def get_opening_balance(self, *args):
        return super().get_opening_balance(AccountType.CURRENT_LIABILITY)

    def set_opening_balance(self, balance: Decimal, opening_date: datetime.date):
        txn: Transaction = self.opening_transaction
        self.begin_edit()
        if txn is not None:
            txn.clear_readonly()
            txn.destroy()
        if not balance.is_zero():
            self.opening_transaction = Scrub.create_opening_balance(self.get_clearing_account(), balance, opening_date, self.book,
                                                                "Staff--{}".format(self.contact.get_full_name()),
                                                                self.location, equity_type=EquityType.RETAINED_EARNINGS)
        self.set_dirty()
        self.commit_edit()

    def get_balance_in_currency(self, report_currency):
        book = self.get_book()
        person_currency = self.get_currency()
        cached_balance = self.get_cached_balance()
        if cached_balance is not None:
            balance = cached_balance
        else:
            balance = self.get_opening_balance()
            payslip_balance = sum([payslip.get_total() for payslip in self.payslips])
            payments = sum(map(lambda a: a.amount, set(cpayment.payment for cpayment in self.payments)))
            balance += Decimal(payslip_balance - payments)
            self.set_cached_balance(balance)
        pdb = PriceDB.get_db(book)
        if report_currency is not None:
            balance = pdb.convert_balance_latest_price(balance, person_currency, report_currency)
        return balance

    def add_payment(self, payment: Payment, payslip=None, payslip_amount=Decimal(0)):
        if payment is not None:
            self.begin_edit()
            sup_payment = StaffPayment(payment=payment)
            if payslip is not None:
                sup_payment.payment = payslip
                sup_payment.amount = payslip_amount
            self.payments.append(sup_payment)
            self.set_dirty()
            self.commit_edit()

    @classmethod
    def lookup(cls, book, guid):
        coll = book.get_collection(ID_STAFF)
        return coll.get(guid)

    def get_location(self):
        return self.location

    def set_location(self, location):
        if location != self.location:
            self.begin_edit()
            self.location = location
            self.set_dirty()
            self.commit_edit()

    def get_payment_method(self):
        return self.payment_method

    def set_payment_method(self, payment_method):
        if payment_method != self.payment_method:
            self.begin_edit()
            self.payment_method = payment_method
            self.mark()
            self.commit_edit()

    def get_salary_advance_limit(self) -> Decimal:
        return self.salary_advance_limit

    def set_salary_advance_limit(self, limit: Decimal):
        if limit != self.salary_advance_limit:
            self.begin_edit()
            self.salary_advance_limit = limit
            self.mark()
            self.commit_edit()

    def get_tax(self):
        return self.tax

    def set_tax(self, tax):
        if tax != self.tax:
            self.begin_edit()
            self.tax = tax
            self.mark()
            self.commit_edit()

    def get_tin(self):
        return self.tin_number

    def set_tin(self, tin):
        if tin != self.tin_number:
            self.begin_edit()
            self.tin_number = tin
            self.mark()
            self.commit_edit()

    def get_nin(self):
        return self.id_number

    def set_nin(self, nin):
        if nin != self.id_number:
            self.begin_edit()
            self.id_number = nin
            self.mark()
            self.commit_edit()

    def get_ssn(self):
        return self.ss_number

    def set_ssn(self, ssn):
        if ssn != self.ss_number:
            self.begin_edit()
            self.ss_number = ssn
            self.mark()
            self.commit_edit()

    def get_date_of_hire(self):
        return self.date_of_hire

    def set_date_of_hire(self, acc):
        if acc != self.date_of_hire:
            self.begin_edit()
            self.date_of_hire = acc
            self.mark()
            self.commit_edit()

    def get_date_of_birth(self):
        return self.date_of_birth

    def set_date_of_birth(self, acc):
        if acc != self.date_of_birth:
            self.begin_edit()
            self.date_of_birth = acc
            self.mark()
            self.commit_edit()

    def get_next_of_kin(self):
        return self.next_of_kin

    def set_next_of_kin(self, acc):
        if acc != self.next_of_kin:
            self.begin_edit()
            self.next_of_kin = acc
            self.mark()
            self.commit_edit()

    @classmethod
    def lookup_display_name(cls, book, display_name):
        col = book.get_collection(ID_STAFF)
        for cat in col.values():
            if cat.get_contact().get_display_name().lower().replace(" ", "") == display_name.lower().replace(" ", ""):
                return cat

    @classmethod
    def lookup_name(cls, book, display_name):
        col = book.get_collection(ID_STAFF)
        for cat in col.values():
            if cat.get_contact().get_full_name().lower().replace(" ", "") == display_name.lower().replace(" ", ""):
                return cat

    @classmethod
    def register(cls):
        params = [
            Param(PARAM_ACTIVE, TYPE_BOOLEAN, cls.get_active, cls.set_active),
            Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
            Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None)]
        ObjectClass.register(cls, ID_STAFF, cls.__eq__, params)

    def __repr__(self):
        return u"Staff<{}>".format(self.contact.get_display_name() if self.contact is not None else "")
