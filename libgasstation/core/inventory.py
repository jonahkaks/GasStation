from decimal import Decimal

from sqlalchemy import DECIMAL, DateTime, ForeignKey
from sqlalchemy.orm import reconstructor, relationship

from ._declbase import *
from .event import *
from .object import *
from .transquery import PARAM_GUID


class Inventory(DeclarativeBaseGuid):
    __tablename__ = "inventory"
    product_location_guid = Column('product_location_guid', UUIDType(binary=False), ForeignKey('product_location.guid'),
                                   nullable=False)
    date = Column('date', DateTime(timezone=False), nullable=False, default=datetime.datetime.now)
    quantity = Column('quantity', DECIMAL(), default=Decimal(0))
    remaining_quantity = Column('remaining_quantity', DECIMAL(), default=Decimal(0))
    price = Column('price', DECIMAL(), default=Decimal(0))
    product_location = relationship('ProductLocation', back_populates="inventories")

    def __init__(self, book):
        super().__init__(ID_INVENTORY, book)
        self.quantity = Decimal(0)
        self.price = Decimal(0)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_INVENTORY, book)
        Event.gen(self, EVENT_CREATE)

    def set_price(self, price: Decimal):
        if price is not None:
            self.begin_edit()
            self.price = price
            self.set_dirty()
            self.commit_edit()

    def get_price(self) -> Decimal:
        return self.price

    def set_date(self, date: datetime.date):
        if date is not None:
            self.begin_edit()
            self.date = date
            self.set_dirty()
            self.commit_edit()

    def get_date(self) -> datetime.datetime:
        return self.date if isinstance(self.date, datetime.datetime) else datetime.datetime.combine(self.date,
                                                                                                    datetime.time())

    def set_quantity(self, quantity: Decimal):
        if quantity is not None:
            self.begin_edit()
            self.quantity = quantity
            self.set_dirty()
            self.commit_edit()

    def get_quantity(self) -> Decimal:
        return self.quantity

    def set_remaining_quantity(self, remaining_quantity: Decimal):
        if remaining_quantity is not None and remaining_quantity != self.remaining_quantity:
            self.begin_edit()
            self.remaining_quantity = remaining_quantity
            self.set_dirty()
            self.commit_edit()

    def get_remaining_quantity(self) -> Decimal:
        return self.remaining_quantity

    def set_product_location(self, product_location):
        if product_location is not None:
            self.begin_edit()
            self.product_location = product_location
            self.set_dirty()
            self.commit_edit()

    def get_product_location(self):
        return self.product_location

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    @classmethod
    def register(cls):
        params = [Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_INVENTORY, None, params)

    def __repr__(self):
        return "<Inventory>"
