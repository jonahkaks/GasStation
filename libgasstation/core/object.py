from typing import List

TYPE_STRING = "string"
TYPE_DATE = "date"
TYPE_NUMERIC = "numeric"
TYPE_DEBCRED = "debcred"
TYPE_GUID = "guid"
TYPE_INT32 = "gint32"
TYPE_INT64 = "gint64"
TYPE_DOUBLE = "double"
TYPE_BOOLEAN = "boolean"
TYPE_KVP = "kvp"
TYPE_CHAR = "character"
TYPE_COLLECT = "collection"
TYPE_ENUM = "enum"
TYPE_URL = "url"


class Param:
    name = None
    type = None
    getfcn = None
    setfcn = None
    compfcn = None
    userdata = None

    def __init__(self, name=None, _type=None, getfcn=None, setfcn=None, compfcn=None, *userdata):
        self.name = name
        self.type = _type
        self.getfcn = getfcn
        self.setfcn = setfcn
        self.compfcn = compfcn
        self.userdata = userdata

    def __repr__(self):
        return "<QueryParam name={}, type={}, get_fn={}, set_fn={} >".format(self.name,
                                                                             self.type, self.getfcn, self.setfcn)


class ObjectClass:
    classTable = None
    sortTable = None
    initialized = False
    object_modules = []
    book_list = None

    @classmethod
    def check_init(cls):
        return cls.initialized

    @classmethod
    def init(cls):
        if cls.initialized: return
        cls.initialized = True
        cls.classTable = {}
        cls.sortTable = {}
        cls.book_list = []
        cls.object_modules = []

    @classmethod
    def shutdown(cls):
        if not cls.initialized: return
        cls.initialized = False
        cls.classTable.clear()
        cls.sortTable.clear()
        cls.book_list.clear()
        cls.object_modules.clear()

    @classmethod
    def get_default_sort(cls, obj_name):
        if obj_name is None: return
        return cls.sortTable.get(obj_name)

    @classmethod
    def book_begin(cls, book):
        if cls.object_modules is not None and any(cls.object_modules):
            for obj in cls.object_modules:
                obj.book_begin(book)
            cls.book_list.insert(0, book)
        else:
            raise AssertionError("NO OBJECTS")


    @classmethod
    def book_end(cls, book):
        if cls.object_modules is not None and any(cls.object_modules):
            for obj in cls.object_modules:
                obj.book_end(book)
            cls.book_list.remove(book)
        else:
            raise AssertionError("NO OBJECTS")

    @classmethod
    def register(cls, obj, obj_name, default_sort_function, params:List[Param]):
        if obj_name is None or len(obj_name) == 0: return
        if not cls.check_init():
            cls.init()
        if obj is not None:
            if obj not in cls.object_modules:
                cls.object_modules.append(obj)

            if cls.book_list is not None:
                for book in cls.book_list:
                    obj.book_begin(book)
        if default_sort_function is not None:
            cls.sortTable[obj_name] = default_sort_function
        ht = cls.classTable.get(obj_name)
        if ht is None:
            ht = {}
            cls.classTable[obj_name] = ht
        if params:
            for p in params:
                ht[p.name] = p
        return True

    @classmethod
    def get_classes(cls):
        return cls.classTable

    @classmethod
    def is_registered(cls, obj_name):
        if obj_name is None or len(obj_name) == 0: return False
        if not cls.check_init(): return False
        return cls.classTable.get(obj_name) is not None

    @classmethod
    def get_parameter(cls, obj_name, parameter):
        if obj_name is None or parameter is None:
            return
        if not cls.check_init(): return
        ht = cls.classTable.get(obj_name)
        assert ht is not None, "%s::%s is not REGISTERED" % (obj_name, parameter)
        return ht.get(parameter)

    @classmethod
    def get_parameter_getter(cls, obj_name, parameter):
        if obj_name is None or parameter is None:
            return
        prm = cls.get_parameter(obj_name, parameter)
        if prm is not None:
            return prm.getfcn
        return None

    @classmethod
    def get_parameter_setter(cls, obj_name, parameter):
        if obj_name is None or parameter is None:
            return
        prm = cls.get_parameter(obj_name, parameter)
        if prm is not None:
            return prm.setfcn
        return None

    @classmethod
    def get_parameter_type(cls, obj_name, parameter):
        if obj_name is None or parameter is None:
            return
        prm = cls.get_parameter(obj_name, parameter)
        if prm is not None:
            return prm.type
        return None

    @classmethod
    def foreach(cls, type_name, book, cb, *user_data):
        if book is None or type_name is None: return
        obj = cls.lookup(type_name)
        if obj is None:
            return
        col = book.get_collection(type_name)
        obj.foreach(col, cb, *user_data)
        return

    @classmethod
    def lookup(cls, type_name):
        if not cls.initialized:
            return None
        if type_name is None:
            return None
        for obj in cls.object_modules:
            if obj.e_type == type_name or obj.__name__ == type_name:
                return obj

    @classmethod
    def param_foreach(cls, obj_name, cb, *user_data):
        if cb is None or obj_name is None: return
        if cls.classTable is None: return
        ht = cls.classTable.get(obj_name)
        if ht is None: return
        for k in ht.values():
            cb(k, *user_data)
