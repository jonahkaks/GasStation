import enum

from sqlalchemy import DECIMAL, ForeignKey, VARCHAR, Enum
from sqlalchemy.orm import reconstructor, relationship
from sqlalchemy_utils import URLType

from ._declbase import *
from .account import Account
from .event import *
from .inventory import Inventory
from .location import Location
from .query_private import *
from .tax import Tax

PRODUCT_TYPE = "product-type"
PRODUCT_NAME = "product-name"
PRODUCT_CATEGORY = "product-category"
PRODUCT_CATEGORY_NAME = "product-category-name"
UNIT_OF_MEASURE_NAME = "product-unit-name"
UNIT_OF_MEASURE_GROUP_NAME = "product-unit-group-name"
UNIT_OF_MEASURE_ABBREVIATION = "product-abbreviation-name"
PRODUCT_DESC = "product-desc"


class ProductType(enum.Enum):
    INVENTORY = "Inventory"
    NON_INVENTORY = "Non Inventory"
    SERVICE = "Service"


class UnitOfMeasure(DeclarativeBaseGuid):
    __tablename__ = 'unit_of_measure'
    __table_args__ = {}
    guid = Column('guid', UUIDType(binary=False), primary_key=True,
                  nullable=False, default=lambda: uuid.uuid4())
    group_guid = Column('group_guid', UUIDType(binary=False), ForeignKey('unit_of_measure_group.guid'))
    base_guid = Column('base_guid', UUIDType(binary=False), ForeignKey('unit_of_measure.guid'))
    name = Column('name', VARCHAR(length=30), nullable=False, unique=True)
    quantity = Column('quantity', DECIMAL(asdecimal=True), nullable=False, default=Decimal(1.0))
    group = relationship("UnitOfMeasureGroup", foreign_keys=[group_guid])
    children = relationship('UnitOfMeasure',
                            back_populates='base',
                            cascade='all, delete-orphan',
                            order_by=name
                            )
    base = relationship('UnitOfMeasure',
                        back_populates='children',
                        remote_side=guid,
                        )

    def __init__(self, book):
        super().__init__(ID_PRODUCT_UNIT, book)
        self.quantity = Decimal(1)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_PRODUCT_UNIT, book)
        Event.gen(self, EVENT_CREATE)

    @classmethod
    def lookup_name(cls, book, name):
        col = book.get_collection(ID_PRODUCT_UNIT)
        for cat in col.values():
            if cat.get_name() == name:
                return cat

    @classmethod
    def get_units(cls, book):
        col = book.get_collection(ID_PRODUCT_UNIT)
        return list(col.values())

    def set_name(self, name):
        if name is not None and name != self.name:
            self.begin_edit()
            self.name = name
            self.set_dirty()
            self.commit_edit()

    def get_name(self):
        return self.name

    def set_quantity(self, quantity):
        if quantity is not None and quantity != self.quantity:
            self.begin_edit()
            self.quantity = quantity
            self.set_dirty()
            self.commit_edit()

    def get_quantity(self):
        return self.quantity

    def set_group(self, group):
        if group is not None and group != self.group:
            self.begin_edit()
            self.group = group
            self.set_dirty()
            self.commit_edit()

    def get_group(self):
        return self.group

    def get_base(self):
        return self.base

    def set_base(self, base):
        if base is not None and base != self.base:
            self.begin_edit()
            self.base = base
            self.set_dirty()
            self.commit_edit()

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def referred(self, other):
        if not isinstance(other, ProductCategory):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)

    def refers_to(self, other):
        if isinstance(other, UnitOfMeasureGroup):
            return other == self.group
        elif isinstance(other, UnitOfMeasure):
            return other == self.base
        return False

    @classmethod
    def register(cls):
        params = [Param(UNIT_OF_MEASURE_NAME, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_PRODUCT_UNIT, None, params)

    def compute_quantity(self, qty, to_unit, raise_if_failure=False):
        """ Convert the given quantity from the current UoM `self` into a given one
            :param qty: the quantity to convert
            :param to_unit: the destination UoM record (uom.uom)
            :param raise_if_failure: only if the conversion is not possible
                - if true, raise an exception if the conversion is not possible (different UoM group),
                - otherwise, return the initial quantity
        """
        if not self or not qty:
            return qty

        if self != to_unit and self.group != to_unit.group:
            if raise_if_failure:
                raise TypeError('The unit of measure %s defined on the order line doesn\'t belong '
                                'to the same group as the unit of measure %s defined on the product. '
                                'Please correct the unit of measure defined on the order line or on the product,'
                                ' they should belong to the same group.' % (self.name, to_unit.name))
            else:
                return qty
        if to_unit is None:
            to_unit = self.base

        if self == to_unit:
            amount = qty
        else:
            # Convert to base unit
            amount = qty * self.quantity
            if to_unit:
                amount = amount / to_unit.quantity
            while to_unit.base is not None:
                if to_unit:
                    amount = amount / to_unit.quantity
                to_unit = to_unit.base
        return amount

    def compute_price(self, price, to_unit):
        if not self or not price or not to_unit or self == to_unit:
            return price
        if self.group != to_unit.group:
            return price
        if to_unit is None:
            to_unit = self.base
        # Get price per peice
        amount = price / self.quantity
        if to_unit:
            amount *= to_unit.quantity
        return amount

    def __repr__(self):
        return "<UnitOfMeasure %s>" % self.name


class UnitOfMeasureGroup(DeclarativeBaseGuid):
    __tablename__ = 'unit_of_measure_group'
    __table_args__ = {}
    name = Column('name', VARCHAR(length=30), nullable=False, unique=True)
    primary_unit = relationship("UnitOfMeasure", primaryjoin="and_(UnitOfMeasureGroup.guid==UnitOfMeasure.group_guid, "
                                                             "UnitOfMeasure.base_guid == None)",viewonly=True, uselist=False)
    units = relationship("UnitOfMeasure", back_populates="group")

    def __init__(self, book):
        super().__init__(ID_UNIT_OF_MEASURE_GROUP, book)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_UNIT_OF_MEASURE_GROUP, book)
        Event.gen(self, EVENT_CREATE)

    @classmethod
    def lookup_name(cls, book, name):
        col = book.get_collection(ID_UNIT_OF_MEASURE_GROUP)
        for cat in col.values():
            if cat.get_name() == name:
                return cat

    def refers_to(self, other):
        pass

    @classmethod
    def get_unit_groups(cls, book):
        col = book.get_collection(ID_UNIT_OF_MEASURE_GROUP)
        return list(col.values())

    def get_units(self):
        return self.units

    def set_name(self, name):
        if name is not None and name != self.name:
            self.begin_edit()
            self.name = name
            self.set_dirty()
            self.commit_edit()

    def get_name(self):
        return self.name

    def set_primary_unit(self, primary_unit):
        if primary_unit is not None and primary_unit != self.primary_unit:
            self.begin_edit()
            self.primary_unit = primary_unit
            self.set_dirty()
            self.commit_edit()

    def get_primary_unit(self):
        return self.primary_unit

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def referred(self, other):
        if not isinstance(other, UnitOfMeasure):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)

    @classmethod
    def register(cls):
        params = [Param(UNIT_OF_MEASURE_GROUP_NAME, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_UNIT_OF_MEASURE_GROUP, None, params)

    def __repr__(self):
        return "<UnitOfMeasureGroup %s>" % self.name


class ProductCategory(DeclarativeBaseGuid):
    __tablename__ = 'product_category'
    __table_args__ = {}
    name = Column('name', VARCHAR(length=60), nullable=False, unique=True)
    products = relationship("Product", lazy="joined", back_populates="category")

    def __init__(self, book):
        super().__init__(ID_PRODUCT_CATEGORY, book)
        self.type = ProductType.INVENTORY
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_PRODUCT_CATEGORY, book)
        Event.gen(self, EVENT_CREATE)

    @classmethod
    def lookup_name(cls, book, name):
        col = book.get_collection(ID_PRODUCT_CATEGORY)
        for cat in col.values():
            if cat.get_name() == name:
                return cat

    @classmethod
    def get_categories(cls, book):
        col = book.get_collection(ID_PRODUCT_CATEGORY)
        return list(col.values())

    def set_name(self, name):
        if name is not None and name != self.name:
            self.begin_edit()
            self.name = name
            self.set_dirty()
            self.commit_edit()

    def get_name(self):
        return self.name

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def get_description(self):
        return self.description

    @classmethod
    def register(cls):
        params = [Param(PRODUCT_CATEGORY_NAME, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_PRODUCT_CATEGORY, None, params)

    def __repr__(self):
        return "<ProductCategory %s>" % self.name


class ProductLocation(DeclarativeBaseGuid):
    __tablename__ = "product_location"
    product_guid = Column(UUIDType(binary=False), ForeignKey('product.guid'), nullable=False)
    location_guid = Column(UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    transaction_guid = Column(UUIDType(binary=False), ForeignKey('transaction.guid'))
    reorder_level: Decimal = Column('reorder_level', DECIMAL(10, 2), nullable=False, default=Decimal(0))
    selling_price: Decimal = Column("selling_price", DECIMAL(10, 2), nullable=False, default=Decimal(0))
    product = relationship('Product', back_populates="locations")
    location = relationship('Location')
    inventories: List[Inventory] = relationship("Inventory", back_populates="product_location",
                                                cascade='all, delete-orphan',
                                                order_by="Inventory.date")
    opening_transaction = relationship("Transaction", cascade='delete')

    def __init__(self, book):
        super().__init__(ID_PRODUCT_LOCATION, book)
        self.reorder_level = Decimal(0)
        self.current_stock = Decimal(0)
        self.selling_price = Decimal(0)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_PRODUCT_LOCATION, book)
        Event.gen(self, EVENT_CREATE)

    def set_selling_price(self, price: Decimal):
        if price is not None:
            self.begin_edit()
            self.selling_price = price
            self.set_dirty()
            self.commit_edit()

    def get_selling_price(self) -> Decimal:
        return self.selling_price

    def get_reorder_level(self) -> Decimal:
        return self.reorder_level

    def set_reorder_level(self, level: Decimal):
        if level != self.reorder_level and level is not None:
            self.begin_edit()
            self.reorder_level = level
            self.set_dirty()
            self.commit_edit()

    def get_location(self):
        return self.location

    def set_location(self, location):
        if location != self.location:
            self.begin_edit()
            self.location = location
            self.set_dirty()
            self.commit_edit()

    def get_opening_transaction(self):
        return self.opening_transaction

    def set_opening_transaction(self, opening_transaction):
        if opening_transaction != self.opening_transaction:
            self.begin_edit()
            self.opening_transaction = opening_transaction
            self.set_dirty()
            self.commit_edit()

    def get_product(self):
        return self.product

    def get_product_str(self):
        return self.product.get_name() if self.product is not None else ""

    def set_product(self, inv):
        if inv is not None and inv != self.product:
            self.begin_edit()
            self.product = inv
            self.set_dirty()
            self.commit_edit()

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def refers_to(self, other):
        if isinstance(other, Location):
            return other == self.location
        elif isinstance(other, Product):
            return self.product == other
        return False

    @classmethod
    def register(cls):
        params = [Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_PRODUCT_LOCATION, None, params)

    def __repr__(self):
        return "<ProductLocation>%s" % (self.location.get_name())


class ProductImage(DeclarativeBaseGuid):
    __tablename__ = "product_image"
    image_url = Column("image_url", URLType, nullable=False)
    product_guid = Column('product_guid', UUIDType(binary=False), ForeignKey('product.guid'), primary_key=True)
    product = relationship("Product", back_populates="profile_image")

    def __init__(self, book, image_url):
        super().__init__("ProductImage", book)
        self.image_url = image_url
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, "ProductImage", book)
        Event.gen(self, EVENT_CREATE)

    def refers_to(self, other):
        if isinstance(other, Product):
            return other == self.product
        return False


class ProductUnit(DeclarativeBaseGuid):
    __tablename__ = 'product_unit'
    product_guid = Column('product_guid', UUIDType(binary=False), ForeignKey('product.guid'), nullable=False,
                          primary_key=True)
    default_unit_guid = Column('default_unit_guid', UUIDType(binary=False), ForeignKey('unit_of_measure.guid'),
                               nullable=False)
    supplier_unit_guid = Column('supplier_unit_guid', UUIDType(binary=False), ForeignKey('unit_of_measure.guid'),
                                nullable=False)
    supplier_unit: UnitOfMeasure = relationship('UnitOfMeasure', primaryjoin=UnitOfMeasure.guid == supplier_unit_guid)
    default_unit: UnitOfMeasure = relationship('UnitOfMeasure', primaryjoin=UnitOfMeasure.guid == default_unit_guid)
    product = relationship("Product", back_populates="unit_of_measure")

    def __init__(self, book):
        super().__init__("ProductUnit", book)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, "ProductUnit", book)
        Event.gen(self, EVENT_CREATE)

    def refers_to(self, other):
        pass

    def set_default_unit(self, default_unit):
        assert default_unit is not None, "Default unit can't be None"
        if default_unit is not None and default_unit != self.default_unit:
            self.begin_edit()
            self.default_unit = default_unit
            self.set_dirty()
            self.commit_edit()

    def get_default_unit(self):
        return self.default_unit

    def set_supplier_unit(self, unit):
        if unit is not None and unit != self.supplier_unit:
            self.begin_edit()
            self.supplier_unit = unit
            self.set_dirty()
            self.commit_edit()

    def get_supplier_unit(self):
        return self.supplier_unit


class Product(DeclarativeBaseGuid, ImageMixin):
    __tablename__ = 'product'
    __table_args__ = {}
    category_guid = Column('category_guid', UUIDType(binary=False), ForeignKey('product_category.guid'), nullable=False)
    supplier_guid = Column('supplier_guid', UUIDType(binary=False), ForeignKey('supplier.guid'))
    tax_guid = Column('tax_guid', UUIDType(binary=False), ForeignKey('tax.guid'))
    type = Column('type', Enum(ProductType), nullable=False, default=ProductType.INVENTORY)
    income_acc_guid = Column('income_account', UUIDType(binary=False), ForeignKey('account.guid'), nullable=False)
    name = Column('name', VARCHAR(length=30), nullable=False, unique=True)
    _id = Column('id', VARCHAR(length=50), nullable=False, unique=True)
    description: str = Column('description', VARCHAR(length=30))
    sku: str = Column('sku', VARCHAR(length=36), unique=True)
    tax_included = Column('tax_included', BOOLEAN(), nullable=False)
    tax = relationship('Tax')
    category: ProductCategory = relationship('ProductCategory', back_populates="products")
    default_supplier = relationship('Supplier', back_populates="products")
    locations: List[ProductLocation] = relationship("ProductLocation", cascade='all, delete-orphan',
                                                    back_populates="product")
    profile_image = relationship("ProductImage", uselist=False, back_populates="product")
    unit_of_measure = relationship("ProductUnit", uselist=False, back_populates="product")
    income_acc = relationship("Account", foreign_keys=[income_acc_guid])

    def __init__(self, book):
        super().__init__(ID_PRODUCT, book)
        self._id = book.increment_and_format_counter("Products")
        self.type = ProductType.INVENTORY
        self.tax_included = False
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from .session import Session
        book = Session.get_current_book()
        super().__init__(ID_PRODUCT, book)
        Event.gen(self, EVENT_CREATE)

    @classmethod
    def lookup_name(cls, book, name):
        col = book.get_collection(ID_PRODUCT)
        for cat in col.values():
            if cat.get_name() == name:
                return cat

    @classmethod
    def lookup_sku(cls, book, sku):
        col = book.get_collection(ID_PRODUCT)
        for cat in col.values():
            if cat.get_sku() == sku:
                return cat

    def get_image_url(self):
        return self.profile_image.image_url if self.profile_image is not None else None

    def set_image_url(self, url):
        if url is not None:
            if self.profile_image is not None:
                if self.profile_image.image_url == url:
                    return
            else:
                self.profile_image = ProductImage(self.book, url)
            self.begin_edit()
            self.profile_image.image_url = url
            self.set_dirty()
            self.commit_edit()

    def get_full_name(self):
        return self.name

    def set_name(self, name):
        if name is not None and name != self.name:
            self.begin_edit()
            self.name = name
            self.set_dirty()
            self.commit_edit()

    def get_name(self):
        return self.name

    def set_type(self, type):
        if type is not None and type != self.type and isinstance(type, ProductType):
            self.begin_edit()
            self.type = type
            self.set_dirty()
            self.commit_edit()

    def get_type(self):
        return self.type

    def set_unit_of_measure(self, unit_of_measure):
        if unit_of_measure is not None and unit_of_measure != self.unit_of_measure:
            self.begin_edit()
            self.unit_of_measure = unit_of_measure
            self.set_dirty()
            self.commit_edit()

    def get_unit_of_measure(self):
        return self.unit_of_measure

    def set_default_supplier(self, default_supplier):
        if default_supplier is not None and default_supplier != self.default_supplier:
            self.begin_edit()
            self.default_supplier = default_supplier
            self.set_dirty()
            self.commit_edit()

    def get_default_supplier(self):
        return self.default_supplier

    def get_tax(self):
        return self.tax

    def set_tax(self, tax):
        if tax != self.tax:
            self.begin_edit()
            self.tax = tax
            self.commit_edit()

    def get_tax_included(self):
        return self.tax_included

    def set_tax_included(self, tax_included):
        if tax_included != self.tax_included:
            self.begin_edit()
            self.tax_included = tax_included
            self.commit_edit()

    def set_sku(self, sku):
        if sku is None or sku == "" or sku == self.sku:
            return
        self.begin_edit()
        self.sku = sku
        self.commit_edit()

    def get_sku(self):
        return self.sku

    def set_category(self, category: ProductCategory):
        if category is not None and category != self.category:
            self.begin_edit()
            self.category = category
            self.set_dirty()
            self.commit_edit()

    def get_category(self) -> ProductCategory:
        return self.category

    def set_description(self, desc: str):
        if desc is not None and desc != self.description:
            self.begin_edit()
            self.description = desc
            self.set_dirty()
            self.commit_edit()

    def get_description(self) -> str:
        return self.description

    def adjust_inventory(self, location, quantity: Decimal, price: Decimal = None) -> Decimal:
        adjusted_amount = Decimal(0)
        locations = list(filter(lambda inv: inv.get_location() == location, self.locations))
        if any(locations):
            product_location = locations[0]
            if quantity > 0:
                inventory = Inventory(self.book)
                inventory.begin_edit()
                inventory.set_quantity(quantity)
                inventory.set_remaining_quantity(quantity)
                inventory.set_price(price)
                product_location.inventories.append(inventory)
                inventory.commit_edit()
                return quantity * price
            else:
                quantity = abs(quantity)
                for inventory in filter(lambda inv: inv.get_remaining_quantity() > 0, product_location.inventories):
                    inventory_quantity = inventory.get_remaining_quantity()
                    inventory_price = inventory.get_price()
                    if inventory_quantity >= quantity:
                        inventory.begin_edit()
                        inventory.set_remaining_quantity(inventory_quantity - quantity)
                        inventory.commit_edit()
                        adjusted_amount += quantity * inventory_price
                        break
                    else:
                        quantity -= inventory_quantity
                        inventory.begin_edit()
                        inventory.set_remaining_quantity(Decimal(0))
                        inventory.commit_edit()
                        adjusted_amount += inventory_quantity * inventory_price
        return adjusted_amount

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def get_sales_account(self):
        return self.income_acc

    def set_sales_account(self, acc):
        if acc == self.income_acc:
            return
        self.begin_edit()
        self.income_acc = acc
        self.set_dirty()
        self.commit_edit()

    def get_quantity_at_hand_on_date(self, location=None, date=None):
        if date is None:
            date = datetime.datetime.now()
        if isinstance(date, datetime.date):
            date = datetime.datetime.combine(date, datetime.datetime.now().time())
        inventories = []
        for loc in self.locations:
            if location is not None:
                if loc.get_location() == location:
                    inventories.extend(loc.inventories)
            else:
                inventories.extend(loc.inventories)
        return sum(map(lambda i: i.get_remaining_quantity(), filter(lambda q: q.get_date() <= date, inventories)))

    def get_id(self):
        return self._id

    def refers_to(self, other):
        from .supplier import Supplier
        if isinstance(other, Tax):
            return self.tax == other
        elif isinstance(other, Account):
            return self.income_acc == other
        elif isinstance(other, ProductCategory):
            return self.category == other
        elif isinstance(other, Supplier):
            return self.default_supplier == other
        return False

    def referred(self, other):
        from .supplier import Supplier
        if not isinstance(other, (Tax, ProductCategory, Supplier)):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)

    def __lt__(self, other):
        if other is None:
            return
        if self.name is None and other.name is None:
            return True
        if self.name is None:
            return True
        if other.name is None:
            return False
        return self.name < other.name

    @classmethod
    def register(cls):
        params = [Param(PRODUCT_NAME, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(PRODUCT_CATEGORY, ID_PRODUCT_CATEGORY, cls.get_category, cls.set_category),
                  Param(PRODUCT_DESC, TYPE_STRING, cls.get_description, cls.set_description),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_PRODUCT, None, params)

    def __repr__(self):
        return "<Product %s %s>" % (self.name, self.category)
