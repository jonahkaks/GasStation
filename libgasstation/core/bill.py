from .account import Account
from .commodity import Commodity
from .entry import BillAccountEntry, BillEntry
from .helpers import gs_set_num_action
from .product import ProductLocation
from .term import Term
from .transaction import *

BILL_ID = "id"
BILL_SUPPLIER = "supplier"
BILL_OPENED = "date_opened"
BILL_POSTED = "date_posted"
BILL_DUE = "date_due"
BILL_IS_POSTED = "is_posted?"
BILL_IS_PAID = "is_paid?"
BILL_TERMS = "terms"
BILL_NOTES = "notes"
BILL_DOCLINK = "doclink"
BILL_ACC = "account"
BILL_POST_TXN = "posted_txn"
BILL_IS_CN = "debit_note"
BILL_TYPE = "type"
BILL_TYPE_STRING = "type_string"
BILL_ENTRIES = "list_of_entries"
BILL_FROM_TXN = "bill-from-txn"


class Bill(DeclarativeBaseGuid):
    __tablename__ = 'bill'

    __table_args__ = {}

    # column definitions
    id = Column('id', VARCHAR(length=48), nullable=False)
    date_posted = Column('date_posted', Date(), nullable=False)
    date_due = Column('date_due', Date(), nullable=False, default=datetime.date.today)
    notes = Column('notes', VARCHAR(length=200))
    active = Column('active', BOOLEAN(), nullable=False, default=True)
    is_debit_note = Column('is_debit', BOOLEAN(), nullable=False, default=False)
    currency_guid = Column('currency', UUIDType(binary=False), ForeignKey('commodity.guid'), nullable=False)
    supplier_guid = Column('supplier_guid', UUIDType(binary=False), ForeignKey("supplier.guid"), nullable=False)
    location_guid = Column('location_guid', UUIDType(binary=False), ForeignKey('location.guid'))
    term_guid = Column('term_guid', UUIDType(binary=False), ForeignKey('term.guid'))
    post_transaction_guid = Column('post_transaction_guid', UUIDType(binary=False), ForeignKey('transaction.guid'))
    post_account_guid = Column('post_account_guid', UUIDType(binary=False), ForeignKey('account.guid'))

    terms = relationship('Term')
    currency = relationship('Commodity')
    location = relationship('Location')
    supplier = relationship('Supplier', back_populates='bills')
    posted_account = relationship('Account')
    posted_txn = relationship('Transaction')
    entries = relationship("BillEntry", back_populates="bill")
    payments = relationship("SupplierPayment", back_populates="bill")
    account_entries = relationship("BillAccountEntry", back_populates="bill")

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, ID_BILL, book)
        Event.gen(self, EVENT_CREATE)

    def __init__(self, book):
        super().__init__(ID_BILL, book)
        self.is_debit_note = False
        self.prices = []
        self.active = True
        self.id = book.format_counter("bill")
        Event.gen(self, EVENT_CREATE)

    def get_active(self):
        return self.active

    def set_active(self, active):
        if active != self.active:
            self.begin_edit()
            self.active = active
            self.set_dirty()
            self.commit_edit()

    def get_location(self):
        return self.location

    def set_location(self, location):
        if location != self.location:
            self.begin_edit()
            self.location = location
            self.set_dirty()
            self.commit_edit()

    @staticmethod
    def lookup(guid, book):
        if guid is None or book is None: return
        col = book.get_collection(ID_BILL)
        return col.lookup_entity(guid)

    def get_supplier(self):
        return self.supplier

    def set_supplier(self, supplier):
        if supplier != self.supplier:
            self.begin_edit()
            self.supplier = supplier
            self.set_dirty()
            self.commit_edit()

    def get_currency(self):
        return self.currency if self.currency is not None else self.supplier.get_currency()

    def set_currency(self, currency):
        if currency != self.currency:
            self.begin_edit()
            self.currency = currency
            self.set_dirty()
            self.commit_edit()

    def get_date_opened(self):
        return self.date_opened

    def set_date_due(self, date_due):
        if date_due != self.date_due:
            self.begin_edit()
            self.date_due = date_due
            self.set_dirty()
            self.commit_edit()

    def get_date_due(self):
        return self.date_due

    def get_date_posted(self):
        return self.date_posted

    def set_date_posted(self, date_posted):
        if date_posted != self.date_posted:
            self.begin_edit()
            self.date_posted = date_posted
            self.set_dirty()
            self.commit_edit()

    def get_terms(self):
        return self.terms

    def set_terms(self, terms):
        if terms != self.terms:
            self.begin_edit()
            if self.terms is not None:
                self.terms.decrement_ref()
            self.terms = terms
            if self.terms is not None:
                self.terms.increment_ref()
            self.set_dirty()
            self.commit_edit()

    def get_notes(self):
        return self.notes

    def set_notes(self, notes):
        if notes != self.notes:
            self.begin_edit()
            self.notes = notes
            self.set_dirty()
            self.commit_edit()

    def get_posted_txn(self):
        return self.posted_txn

    def set_posted_txn(self, post_txn):
        if post_txn != self.posted_txn:
            self.begin_edit()
            self.posted_txn = post_txn
            self.set_dirty()
            self.commit_edit()

    def add_payment(self, payment, amount):
        self.begin_edit()
        self.supplier.add_payment(payment, self, amount)
        self.set_dirty()
        self.commit_edit()

    def get_balance(self):
        return self.get_total() - sum(map(lambda p: p.amount, self.payments))

    def get_posted_account(self):
        return self.posted_account

    def set_posted_account(self, posted_account):
        if posted_account != self.posted_account:
            self.begin_edit()
            self.posted_account = posted_account
            self.set_dirty()
            self.commit_edit()

    def add_entry(self, entry: BillEntry):
        if entry is None:
            return
        old = entry.get_bill()
        if old is not None and old == self:
            return
        if old is not None:
            old.remove_entry(entry)
        self.begin_edit()
        self.entries.append(entry)
        self.entries.sort()
        self.set_dirty()
        self.commit_edit()

    def remove_entry(self, entry: BillEntry):
        if entry is None:
            return
        self.begin_edit()
        entry.set_bill(None)
        self.entries.remove(entry)
        self.set_dirty()
        self.commit_edit()

    def add_account_entry(self, entry: BillAccountEntry):
        if entry is None:
            return
        old = entry.get_bill()
        if old == self:
            return
        if old is not None:
            old.remove_account_entry(entry)
        self.begin_edit()
        self.account_entries.append(entry)
        self.account_entries.sort()
        self.set_dirty()
        self.commit_edit()

    def remove_account_entry(self, entry: BillAccountEntry):
        if entry is None:
            return
        self.begin_edit()
        entry.set_bill(None)
        self.account_entries.remove(entry)
        self.set_dirty()
        self.commit_edit()

    def get_is_debit_note(self):
        return self.is_debit_note

    def set_is_debit_note(self, credit_note):
        self.begin_edit()
        self.is_debit_note = credit_note
        self.set_dirty()
        self.commit_edit()

    def add_price(self, price):
        if price is None:
            return
        commodity = price.get_commodity()
        node = None
        for curr in self.prices.copy():
            if curr.get_commodity() == commodity:
                node = curr
        self.begin_edit()
        if node is not None:
            self.prices.remove(node)
        self.prices.append(price)
        self.set_dirty()
        self.commit_edit()

    def get_price(self, commodity):
        for curr in self.prices:
            if curr.get_commodity() == commodity:
                return curr

    def get_id(self):
        return self.id

    def set_id(self, _id):
        if _id != self.id:
            self.begin_edit()
            self.id = _id
            self.set_dirty()
            self.commit_edit()

    def is_posted(self):
        return self.posted_txn is not None and isinstance(self.posted_txn, Transaction)

    def is_paid(self):
        return sum(map(lambda p:p.payment.amount, self.payments)) == self.get_total()

    def post_add_split(self, book, acc: Account, txn: Transaction, value: Decimal, memo: str, _type):
        split = Split(book)
        split.set_memo(memo)
        gs_set_num_action(None, split, self.get_id(), _type)
        acc.begin_edit()
        split.set_account(acc)
        acc.commit_edit()
        txn.append_split(split)
        if acc.get_commodity() == self.currency:
            split.set_base_value(value, self.currency)
        else:
            price = self.get_price(acc.get_commodity())
            if price is None:
                return False
            else:
                split.set_value(value)
                converted_amount = value / price.get_value()
                split.set_amount(converted_amount)
        return True

    def post_to_account(self, payable_account: Account, post_date: datetime.date, auto_pay=False):
        if payable_account is None:
            return
        if self.is_posted(): return
        memo = self.get_notes()
        self.begin_edit()
        book = self.get_book()

        if self.terms is not None:
            self.set_terms(self.terms.return_child(True))
        is_cn = self.is_debit_note
        if not auto_pay:
            _type = "Debit Note" if self.is_debit_note else "Bill"
        else:
            _type = "Expense"
        txn = Transaction(book)
        txn.begin_edit()
        txn.set_location(self.get_location())
        txn.set_post_date(post_date)
        txn.set_type(TransactionType.INVOICE)
        txn.set_enter_date(datetime.datetime.now())
        txn.set_notes(self.get_notes())
        name = self.supplier.get_contact().get_display_name()
        txn.set_description(name)
        gs_set_num_action(txn, None, self.get_id(), _type)
        txn.set_currency(self.currency)
        total = self.get_total()
        taxes = self.get_total_tax_list()
        if is_cn:
            total = -total
            for acc_val in taxes:
                taxes[acc_val] *= -1

        acc_dict = defaultdict(Decimal)

        for entry in self.entries:
            value = entry.get_value()
            product = entry.get_product()
            if self.location is not None:
                locations = list(filter(lambda pl: pl.get_location() == self.location, product.locations))
                if not any(locations):
                    product_location = ProductLocation(self.book)
                    product_location.begin_edit()
                    product_location.set_location(self.location)
                    product_location.set_reorder_level(Decimal(1))
                    product_location.set_selling_price(Decimal(1))
                    product.locations.append(product_location)
                    product_location.commit_edit()
                supplier_unit = product.get_supplier_unit()
                default_unit = product.get_default_unit()
                product_uom_qty = supplier_unit.compute_quantity(entry.get_quantity(), default_unit, False)
                product_uom_price = supplier_unit.compute_price(entry.get_price(), default_unit)
                product.adjust_inventory(self.location, product_uom_qty, product_uom_price)
            if is_cn:
                value *= -1
            this_acc = entry.get_account()
            acc_dict.setdefault(this_acc, Decimal(0))
            if this_acc is not None:
                acc_dict[this_acc] += value

        for entry in self.account_entries:
            acc = entry.get_account()
            if acc_dict.get(acc) is not None:
                acc_dict[acc] += entry.get_value()
            else:
                acc_dict[acc] = entry.get_value()

        for acc, val in taxes.items():
            if acc_dict.get(acc) is not None:
                acc_dict[acc] += val
            else:
                acc_dict[acc] = val

        for acc, val in acc_dict.items():
            if acc is not None and not val.is_zero() and not self.post_add_split(book, acc, txn, val, memo, _type):
                return None

        split = Split(book)
        split.set_memo(memo)
        gs_set_num_action(None, split, self.get_id(), _type)
        payable_account.begin_edit()
        split.set_account(payable_account)
        payable_account.commit_edit()
        txn.append_split(split)
        self.date_posted = post_date
        self.set_date_posted(post_date)
        split.set_base_value(-total, self.currency)
        gs_set_num_action(None, split, self.get_id(), _type)
        self.set_posted_txn(txn)
        self.set_posted_account(payable_account)
        txn.set_readonly("Generated from a bill. Try UnPosting the bill.")
        txn.commit_edit()
        self.set_dirty()
        self.commit_edit()
        return txn

    def get_entry_total(self):
        total = Decimal(0)
        for e in self.entries:
            total += e.get_value()
        return total

    def get_account_entry_total(self):
        total = Decimal(0)
        for e in self.account_entries:
            total += e.get_value()
        return total

    def get_total(self):
        return self.__get_total_internal(True, True, False, 0)

    def get_subtotal(self):
        return self.__get_total_internal(True, False, False, 0)

    def get_total_tax(self):
        return self.__get_total_internal(False, True, False, 0)

    @staticmethod
    def __sum_taxes_internal(taxes):
        tt = Decimal(0)
        if taxes is not None and any(taxes):
            for acc, val in taxes.items():
                tt += val
        return tt

    def __get_total_internal(self, use_value, use_tax, use_payment_type, _type=0):
        total, taxes = self.__get_net_and_taxes_internal(use_value, use_tax, use_payment_type, _type)
        if use_tax:
            total += self.__sum_taxes_internal(taxes)
        return total

    def get_total_tax_list(self):
        total, taxes = self.__get_net_and_taxes_internal(False, True, False, 0)
        return taxes

    def __get_net_and_taxes_internal(self, use_value, use_tax, use_payment_type, _type):
        net_total = Decimal(0)
        tv_list = defaultdict(Decimal)
        for entry in self.entries + self.account_entries:
            if use_value:
                net_total += entry.get_value()
            if use_tax and isinstance(entry, BillEntry):
                tv_list = entry.get_tax_value()
        return net_total, tv_list

    def get_entries(self):
        return self.entries

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def refers_to(self, other):
        if isinstance(other, Term):
            return self.terms == other
        elif isinstance(other, Commodity):
            return self.currency == other
        elif isinstance(other, Account):
            return self.posted_account == other
        elif isinstance(other, Transaction):
            return self.posted_txn == other
        return False

    def referred(self, other):
        if not isinstance(other, (Term, Commodity, Account, Transaction)):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)

    def __eq__(self, other):
        if other is None or not isinstance(other, Bill):
            return False
        if self.id != other.id:
            return False
        if self.date_posted != other.date_posted:
            return False
        return self.guid == other.guid

    def __repr__(self):
        return u"BILL<{}>".format(
            self.supplier.get_contact().get_display_name() if self.supplier is not None else "")

    @classmethod
    def register(cls):
        params = [Param(BILL_ID, TYPE_STRING, cls.get_id, cls.set_id),
                  Param(BILL_SUPPLIER, ID_SUPPLIER, cls.get_supplier),
                  Param(BILL_DUE, TYPE_DATE, cls.get_date_due, cls.set_date_due),
                  Param(BILL_POSTED, TYPE_DATE, cls.get_date_posted, cls.set_date_posted),
                  Param(BILL_IS_POSTED, TYPE_BOOLEAN, cls.is_posted, None),
                  Param(BILL_IS_PAID, TYPE_BOOLEAN, cls.is_paid, None),
                  Param(BILL_NOTES, TYPE_STRING, cls.get_notes, cls.set_notes),
                  Param(BILL_ACC, ID_ACCOUNT, cls.get_posted_account, cls.set_posted_account),
                  Param(BILL_POST_TXN, ID_TRANS, cls.get_posted_txn, cls.set_posted_txn),
                  Param(BILL_TERMS, ID_TERM, cls.get_terms, cls.set_terms),
                  Param(BILL_ENTRIES, TYPE_COLLECT, cls.get_entries, None),
                  Param(PARAM_ACTIVE, TYPE_BOOLEAN, cls.get_active, cls.set_active),
                  Param(BILL_IS_CN, TYPE_BOOLEAN, cls.get_is_debit_note, cls.set_is_debit_note),
                  Param(PARAM_BOOK, ID_BOOK, cls.get_book),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_BILL, None, params)
