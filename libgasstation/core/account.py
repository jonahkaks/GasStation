from decimal import Decimal
from enum import IntEnum
from functools import cmp_to_key

from sqlalchemy import INTEGER, VARCHAR, ForeignKey, Enum
from sqlalchemy.orm import reconstructor, relationship
from sqlalchemy.orm.exc import DetachedInstanceError

from ._declbase import *
from .event import *
from .object import *
from .policy import FifoPolicy

ACCOUNT_NAME_ = "name"
ACCOUNT_CODE_ = "code"
ACCOUNT_DESCRIPTION_ = "desc"
ACCOUNT_COLOR_ = "color"
ACCOUNT_FILTER_ = "filter"
ACCOUNT_SORT_ORDER_ = "sort-order"
ACCOUNT_SORT_REVERSED_ = "sort-reversed"
ACCOUNT_NOTES_ = "notes"
ACCOUNT_BALANCE_ = "balance"
ACCOUNT_CLEARED_ = "cleared"
ACCOUNT_RECONCILED_ = "reconciled"
ACCOUNT_PRESENT_ = "present"
ACCOUNT_FUTURE_MINIMUM_ = "future-minimum"
ACCOUNT_TAX_RELATED = "tax-related-p"
ACCOUNT_TYPE_ = "account-type"
ACCOUNT_SCU = "smallest-commodity-unit"
ACCOUNT_NSCU = "non-standard-scu"
ACCOUNT_PARENT = "parent-account"
ACCOUNT_MATCH_ALL_TYPE = "account-match-all"


class CurrencyBalance:
    def __init__(self, currency=None, balance=None, fn=None, asofdatefn=None, date=None):
        self.currency = currency
        self.balance = balance
        self.fn = fn
        self.asOfDateFn = asofdatefn
        self.date = date


account_type_name = ["Bank",
                     "Cash",
                     "Fixed Asset",
                     "Current Asset",
                     "Credit Card",
                     "Current Liability",
                     "Long Term Liability",
                     "Stock",
                     "Mutual Fund",
                     "Currency",
                     "Income",
                     "Expense",
                     "Cost of Sales",
                     "Equity",
                     "A/Receivable",
                     "A/Payable",
                     "Root",
                     "Trading",
                     "Checking",
                     "",
                     "Savings",
                     "Money Market",
                     "Credit Line",
                     "Invalid"]


class AccountType(IntEnum):
    INVALID = -1
    NONE = -1
    BANK = 0
    CASH = 1
    FIXED_ASSET = 2
    CURRENT_ASSET = 3
    CREDIT = 4
    LONG_TERM_LIABILITY = 5
    CURRENT_LIABILITY = 6
    STOCK = 7
    MUTUAL = 8
    CURRENCY = 9
    INCOME = 10
    EXPENSE = 11
    COST_OF_SALES = 12
    EQUITY = 13
    RECEIVABLE = 14
    PAYABLE = 15
    ROOT = 16
    TRADING = 17
    NUM_ACCOUNT_TYPES = 18
    CHECKING = 19
    SAVINGS = 20
    MONEYMRKT = 21
    CREDITLINE = 22
    LAST = 23

    def has_auto_interest_charge(self):
        return self == AccountType.CREDIT or self == AccountType.CURRENT_LIABILITY or AccountType.LONG_TERM_LIABILITY or self == AccountType.PAYABLE

    def has_auto_interest_payment(self):
        return self == AccountType.BANK or self == AccountType.FIXED_ASSET or AccountType.CURRENT_ASSET \
               or self == AccountType.MUTUAL or self == AccountType.RECEIVABLE

    def has_auto_interest_xfer(self):
        return self.has_auto_interest_charge() or self.has_auto_interest_payment()

    def to_string(self):
        return account_type_name[self.value]

    @classmethod
    def from_int(cls, val):
        return cls(val)

    def to_int(self):
        return self.value

    @classmethod
    def from_name(cls, name):
        try:
            return cls.__getitem__(name)
        except KeyError:
            return AccountType.INVALID

    @classmethod
    def from_string(cls, val):
        try:
            p = account_type_name.index(val)
            return cls(p)
        except ValueError:
            return cls.INVALID

    def types_compatible_with(self):
        if self in [AccountType.BANK, AccountType.CASH, AccountType.CURRENT_ASSET, AccountType.FIXED_ASSET,
                    AccountType.CREDIT, AccountType.CURRENT_LIABILITY, AccountType.LONG_TERM_LIABILITY,
                    AccountType.INCOME, AccountType.EXPENSE, AccountType.COST_OF_SALES,
                    AccountType.EQUITY]:
            return (1 << AccountType.BANK) | (1 << AccountType.CASH) | (1 << AccountType.CURRENT_ASSET) \
                   | (1 << AccountType.FIXED_ASSET) | (1 << AccountType.CREDIT) \
                   | (1 << AccountType.CURRENT_LIABILITY) | (1 << AccountType.LONG_TERM_LIABILITY) \
                   | (1 << AccountType.INCOME) | (1 << AccountType.EXPENSE) | \
                   (1 << AccountType.COST_OF_SALES) | (1 << AccountType.EQUITY)
        elif self in [AccountType.STOCK, AccountType.MUTUAL, AccountType.CURRENCY]:
            return (1 << AccountType.STOCK) | (1 << AccountType.MUTUAL) | (1 << AccountType.CURRENCY)
        elif self == AccountType.RECEIVABLE:
            return 1 << AccountType.RECEIVABLE
        elif self == AccountType.PAYABLE:
            return 1 << AccountType.PAYABLE
        elif self == AccountType.TRADING:
            return 1 << AccountType.TRADING
        else:
            return 0

    def parent_types_compatible_with(self):
        if self in [AccountType.BANK,
                    AccountType.CASH,
                    AccountType.CURRENT_ASSET,
                    AccountType.FIXED_ASSET,
                    AccountType.STOCK,
                    AccountType.MUTUAL,
                    AccountType.CURRENCY,
                    AccountType.CREDIT,
                    AccountType.CURRENT_LIABILITY,
                    AccountType.LONG_TERM_LIABILITY,
                    AccountType.RECEIVABLE,
                    AccountType.PAYABLE,
                    ]:
            return (1 << AccountType.BANK) | (1 << AccountType.CASH) | (1 << AccountType.CURRENT_ASSET) \
                   | (1 << AccountType.FIXED_ASSET) \
                   | (1 << AccountType.STOCK) | (1 << AccountType.MUTUAL) | (1 << AccountType.CURRENCY) \
                   | (1 << AccountType.CREDIT) | (1 << AccountType.CURRENT_LIABILITY) | (
                               1 << AccountType.LONG_TERM_LIABILITY) | (
                           1 << AccountType.RECEIVABLE) \
                   | (1 << AccountType.PAYABLE) | (1 << AccountType.ROOT)
        elif self in [AccountType.INCOME, AccountType.EXPENSE, AccountType.COST_OF_SALES]:
            return (1 << AccountType.INCOME) | (1 << AccountType.EXPENSE) | (
                    1 << AccountType.ROOT) | (1 << AccountType.COST_OF_SALES)

        elif self == AccountType.EQUITY:
            return (1 << AccountType.EQUITY) | (1 << AccountType.ROOT)
        elif self == AccountType.TRADING:
            return (1 << AccountType.TRADING) | (1 << AccountType.ROOT)
        elif self == AccountType.ROOT:
            return (1 << AccountType.BANK) | (1 << AccountType.CASH) \
                   | (1 << AccountType.CURRENT_ASSET) | (1 << AccountType.FIXED_ASSET) \
                   | (1 << AccountType.STOCK) | (1 << AccountType.MUTUAL) | (1 << AccountType.CURRENCY) \
                   | (1 << AccountType.CREDIT) | (1 << AccountType.CURRENT_LIABILITY) \
                   | (1 << AccountType.LONG_TERM_LIABILITY) | (1 << AccountType.RECEIVABLE) \
                   | (1 << AccountType.PAYABLE) | (1 << AccountType.INCOME) | (1 << AccountType.EXPENSE) | \
                   (1 << AccountType.COST_OF_SALES) \
                   | (1 << AccountType.EQUITY) | (1 << AccountType.TRADING) | (1 << AccountType.CREDIT) \
                   | (1 << AccountType.ROOT)
        else:
            return 0

    def is_priced(self):
        return self == AccountType.STOCK or self == AccountType.MUTUAL or self == AccountType.CURRENCY

    def is_asset_liability(self):
        if self.is_payable_receivable():
            return False
        else:
            return self.is_asset() or self.is_liability()

    def is_payable_receivable(self):
        return self == AccountType.RECEIVABLE or self == AccountType.PAYABLE

    def is_asset(self):
        return self.types_compatible(AccountType.CURRENT_ASSET, self)

    def is_liability(self):
        return self == AccountType.CURRENT_LIABILITY or self == AccountType.LONG_TERM_LIABILITY

    def is_equity(self):
        return self == AccountType.EQUITY

    def is_income(self):
        return self.types_compatible(AccountType.INCOME, self)

    def is_income_expense(self):
        return self.is_income() or self.is_expense()

    def is_expense(self):
        return self.types_compatible(AccountType.EXPENSE, self)

    def is_trading(self):
        return self.types_compatible(AccountType.TRADING, self)

    def is_root(self):
        return self.types_compatible(AccountType.ROOT, self)

    def is_positive(self):
        return True if self.is_asset() or self.is_expense() or self.is_trading() else False

    @staticmethod
    def valid():
        mask = (1 << AccountType.NUM_ACCOUNT_TYPES) - 1
        mask &= ~((1 << AccountType.CURRENCY) | (1 << AccountType.ROOT))
        return mask

    @staticmethod
    def types_compatible(type_parent, type_child):
        if type_child >= AccountType.NUM_ACCOUNT_TYPES or type_child == AccountType.INVALID:
            return False
        return (type_parent.parent_types_compatible_with() & (1 << type_child)) != 0

    def types_compatible_to_list(self):
        comp = []
        for t in self._member_map_:
            _otype = AccountType.__getitem__(t)
            if _otype == AccountType.NUM_ACCOUNT_TYPES:
                break
            if self.types_compatible(_otype, self):
                comp.append(_otype)
        return comp

    def get_mask(self):
        return self.parent_types_compatible_with() - (1 << AccountType.ROOT)


acct_debit_strs = {
    AccountType.NONE: "Funds In",
    AccountType.BANK: "Deposit",
    AccountType.CASH: "Receive",
    AccountType.CREDIT: "Payment",
    AccountType.CURRENT_ASSET: "Increase",
    AccountType.FIXED_ASSET: "Increase",
    AccountType.CURRENT_LIABILITY: "Decrease",
    AccountType.LONG_TERM_LIABILITY: "Decrease",
    AccountType.STOCK: "Buy",
    AccountType.MUTUAL: "Buy",
    AccountType.CURRENCY: "Buy",
    AccountType.INCOME: "Charge",
    AccountType.EXPENSE: "Expense",
    AccountType.COST_OF_SALES: "Purchase",
    AccountType.PAYABLE: "Payment",
    AccountType.RECEIVABLE: "Payment",
    AccountType.TRADING: "Decrease",
    AccountType.EQUITY: "Decrease"
}
acct_debit_str = "Debit"

acct_credit_strs = {AccountType.NONE: "Funds Out",
                    AccountType.BANK: "Withdrawal",
                    AccountType.CASH: "Spend",
                    AccountType.CREDIT: "Charge",
                    AccountType.CURRENT_ASSET: "Decrease",
                    AccountType.FIXED_ASSET: "Decrease",
                    AccountType.CURRENT_LIABILITY: "Increase",
                    AccountType.LONG_TERM_LIABILITY: "Increase",
                    AccountType.STOCK: "Sell",
                    AccountType.MUTUAL: "Sell",
                    AccountType.CURRENCY: "Sell",
                    AccountType.INCOME: "Income",
                    AccountType.EXPENSE: "Rebate",
                    AccountType.COST_OF_SALES: "Rebate",
                    AccountType.PAYABLE: "Bill",
                    AccountType.RECEIVABLE: "Invoice",
                    AccountType.TRADING: "Increase",
                    AccountType.EQUITY: "Increase"
                    }
acct_credit_str = "Credit"


class Placeholder:
    NONE = 0
    THIS = 1
    CHILD = 2


@dataclass
class Account(DeclarativeBaseGuid):
    __tablename__ = 'account'
    # column definitions
    guid: str = Column('guid', UUIDType(binary=False), primary_key=True,
                       nullable=False, default=lambda: uuid.uuid4())
    name: str = Column('name', VARCHAR(length=48), nullable=False)
    type: AccountType = Column('type', Enum(AccountType), nullable=False, default=AccountType.BANK)
    commodity_guid: uuid.UUID = Column('commodity_guid', UUIDType(binary=False), ForeignKey('commodity.guid'))
    _commodity_scu: int = Column('commodity_scu', INTEGER(), nullable=False, default=0)
    _non_std_scu: int = Column('non_std_scu', INTEGER(), nullable=False, default=0)
    parent_guid: uuid.UUID = Column('parent_guid', UUIDType(binary=False), ForeignKey('account.guid'))
    code: str = Column('code', VARCHAR(length=20))
    description: str = Column('description', VARCHAR(length=200))
    hidden: bool = Column('hidden', BOOLEAN(), default=0)
    placeholder: bool = Column('placeholder', BOOLEAN(), default=0)
    last_num: int = Column('last_num', INTEGER(), default=0)
    color: str = Column('color', VARCHAR(length=30), default=None)
    notes: str = Column('notes', VARCHAR(length=200), default="")
    commodity = relationship('Commodity', back_populates='accounts')
    children: List = relationship('Account',
                                  back_populates='parent',
                                  lazy="selectin",
                                  cascade='all, delete-orphan',
                                  order_by=name
                                  )
    parent = relationship('Account',
                          back_populates='children',
                          remote_side=guid,
                          )
    lots = relationship('Lot',
                        back_populates='account',
                        cascade='all, delete-orphan'
                        )
    budget_amounts = relationship('BudgetAmount',
                                  back_populates='account',
                                  cascade='all, delete-orphan'
                                  )
    scheduled_transaction = relationship('ScheduledTransaction',
                                         back_populates='template_account',
                                         cascade='all, delete-orphan',
                                         uselist=False,
                                         )
    tax_related = Column('tax_related', BOOLEAN(), default=False)
    # filter = kvp_attribute("Account/filter")
    # lot_next_id = kvp_attribute("lot-mgmt/next-id", default=0)
    # sort_order = kvp_attribute("Account/sort-order")
    # sort_reversed = kvp_attribute("Account/sort-reversed")
    separator = ":"
    policy = FifoPolicy()
    e_type = ID_ACCOUNT
    sort_dirty = False
    balance_dirty = False
    balance = Decimal(0)
    reconciled_balance = Decimal(0)
    cleared_balance = Decimal(0)
    starting_balance = Decimal(0)
    starting_cleared_balance = Decimal(0)
    starting_reconciled_balance = Decimal(0)
    noclosing_balance = Decimal(0)
    starting_noclosing_balance = Decimal(0)

    def __init__(self, book):
        self.balance_dirty = True
        self.splits = []
        self.sort_dirty = True
        self.infant = True
        self.balance = Decimal(0)
        self.reconciled_balance = Decimal(0)
        self.cleared_balance = Decimal(0)
        self.starting_balance = Decimal(0)
        self.starting_cleared_balance = Decimal(0)
        self.starting_reconciled_balance = Decimal(0)
        self.noclosing_balance = Decimal(0)
        self.starting_noclosing_balance = Decimal(0)
        super().__init__(ID_ACCOUNT, book)
        Event.gen(self, EVENT_CREATE)

    @staticmethod
    def get_debit_string(acc_type, use_aaccounting_labels):
        if use_aaccounting_labels:
            return acct_debit_str
        return acct_debit_strs.get(acc_type)

    @staticmethod
    def get_credit_string(acc_type, use_aaccounting_labels):
        if use_aaccounting_labels:
            return acct_credit_str
        return acct_credit_strs.get(acc_type)

    def get_lot_next_id(self):
        return self.lot_next_id

    def set_lot_next_id(self, lid):
        self.lot_next_id = lid

    @classmethod
    def set_separator(cls, sep):
        cls.separator = sep

    @classmethod
    def get_separator(cls):
        return cls.separator

    @staticmethod
    def lookup(guid, book):
        if guid is None or book is None: return
        col = book.get_collection(ID_ACCOUNT)
        return col.lookup_entity(guid)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, ID_ACCOUNT, book)
        self.balance_dirty = True
        self.splits = []
        self.sort_dirty = True
        self.balance = Decimal(0)
        self.reconciled_balance = Decimal(0)
        self.cleared_balance = Decimal(0)
        self.starting_balance = Decimal(0)
        self.starting_cleared_balance = Decimal(0)
        self.starting_reconciled_balance = Decimal(0)
        self.noclosing_balance = Decimal(0)
        self.starting_noclosing_balance = Decimal(0)
        self.infant = False
        self.begin_edit()
        Event.gen(self, EVENT_CREATE)

    def set_parent(self, p):
        if p is not None and isinstance(p, Account):
            p.append_child(self)

    def remove_child(self, child):
        if child is None:
            return
        if child.get_parent() != self or child not in self.children:
            return
        ed = EventData(self, self.children.index(child))
        Event.gen(child, EVENT_REMOVE, ed)
        self.children.remove(child)
        Event.gen(self, EVENT_MODIFY)

    def get_auto_interest(self):
        return False

    def clone(self, book):
        p = Account(book)
        p.name = self.name
        p.type = self.type
        p.code = self.code
        p.notes = self.notes
        p.color = self.color
        p.custom_data = self.custom_data.copy() if self.custom_data is not None else None
        if self.commodity is not None:
            p.commodity = self.commodity.obtain_twin(book)
            p.commodity.increment_usage_count()
        p.description = self.description
        p.placeholder = self.placeholder
        p.hidden = self.hidden
        p._commodity_scu = self._commodity_scu
        p._non_std_scu = self._non_std_scu
        p.set_dirty()
        return p

    def get_parent(self):
        try:
            return self.parent
        except DetachedInstanceError:
            return None

    def get_non_std_scu(self):
        return self._non_std_scu

    def set_non_std_scu(self, scu):
        self._non_std_scu = scu
        Event.gen(self, EVENT_MODIFY)

    def get_commodity_scu(self):
        return self._commodity_scu

    def get_name(self):
        return self.name

    def set_name(self, name):
        if name == self.name or name is None:
            return
        self.begin_edit()
        self.name = name
        self.mark()
        self.commit_edit()

    def insert_split(self, split):
        from .transaction import Split
        if split is None or not isinstance(split, Split):
            return False
        if split in self.splits:
            return False
        self.splits.insert(0, split)
        if self.get_edit_level() == 0:
            self.splits.sort()
        else:
            self.sort_dirty = True
        split.set_account(self)
        Event.gen(self, EVENT_MODIFY)
        Event.gen(self, EVENT_ITEM_ADDED, split)
        self.balance_dirty = True
        return True

    def remove_split(self, split):
        self.splits.remove(split)
        self.balance_dirty = True
        self.recompute_balance()
        Event.gen(self, EVENT_MODIFY)
        Event.gen(self, EVENT_ITEM_REMOVED, split)
        return True

    def append_child(self, acc):
        if acc is not None:
            old_parent = acc.parent
            if self == old_parent:
                return
            acc.begin_edit()
            if old_parent is not None:
                old_parent.remove_child(acc)
                if not self.books_equal(old_parent):
                    Event.gen(acc, EVENT_DESTROY)
                    col = self.get_book().get_collection(ID_ACCOUNT)
                    col.insert_entity(acc)
                    Event.gen(acc, EVENT_CREATE)
            self.children.append(acc)
            self.set_dirty()
            acc.set_dirty()
            Event.gen(acc, EVENT_ADD)
            acc.commit_edit()

    def get_placeholder(self):
        return self.placeholder

    def set_placeholder(self, value=True):
        if value == self.placeholder or value is None:
            return
        self.begin_edit()
        self.placeholder = bool(value)
        self.commit_edit()

    def get_descendant_placeholder(self):
        ret = Placeholder.NONE
        if self.placeholder:
            return Placeholder.THIS
        for acc in self.children:
            if acc.placeholder:
                ret = Placeholder.CHILD
                break
        return ret

    def get_descendants(self):
        a = []
        for child in self.children.copy():
            a.append(child)
            a.extend(child.get_descendants())
        return a

    def get_descendants_sorted(self):
        a = []
        for child in sorted(self.children.copy()):
            a.append(child)
            a += child.get_descendants_sorted()
        return a

    def has_ancestor(self, ancestor):
        parent = self
        while parent is not None and parent != ancestor:
            parent = parent.parent
        return parent == ancestor

    def get_root(self):
        acc = self
        try:
            while acc.parent is not None:
                acc = acc.parent
            return acc
        except DetachedInstanceError:
            return None

    def get_description(self):
        return self.description if self.description is not None else ""

    def set_description(self, desc):
        if desc == self.description or desc is None:
            return
        self.begin_edit()
        self.description = desc
        self.mark()
        self.commit_edit()

    def get_color(self):
        return self.color

    def set_color(self, c):
        self.begin_edit()
        self.color = c
        self.mark()
        self.commit_edit()

    def get_notes(self):
        return self.notes

    def set_notes(self, n):
        if n == self.notes or n is None:
            return
        self.begin_edit()
        self.notes = n
        self.mark()
        self.commit_edit()

    def mark(self):
        self.set_dirty()

    def get_code(self):
        return self.code if self.code is not None else ""

    def set_code(self, code):
        if code == self.code or code is None:
            return
        self.begin_edit()
        self.code = code
        self.mark()
        self.commit_edit()

    def get_hidden(self):
        return self.hidden

    def set_hidden(self, h=True):
        if h == self.hidden or h is None:
            return
        self.begin_edit()
        self.hidden = h
        self.mark()
        self.commit_edit()

    def get_tax_related(self):
        return self.tax_related

    def set_tax_related(self, value=True):
        if self.tax_related == value or value is None:
            return
        self.begin_edit()
        self.tax_related = value
        self.mark()
        self.commit_edit()

    def get_type(self) -> AccountType:
        return self.type

    def set_type(self, value: AccountType):
        self.begin_edit()
        self.type = value
        self.balance_dirty = True
        self.sort_dirty = True
        self.mark()
        self.commit_edit()

    def set_commodity_scu(self, value=0):
        if value == self._commodity_scu or value is None:
            return
        self.begin_edit()
        self._commodity_scu = value
        if self.commodity is not None and value != self.commodity.get_fraction():
            self._non_std_scu = 1
        self.mark()
        self.commit_edit()

    def lookup_by_full_name(self, fullname):
        if fullname is None or len(fullname) == 0:
            return
        root = self.get_root()
        names = fullname.strip().split(self.get_separator())
        found = self.lookup_by_full_name_helper(root, names)
        return found

    def lookup_by_code(self, code):
        if code is None:
            return None
        code = code.strip()
        for c in self.children:
            if c.code == code:
                return c
        for c in self.children:
            result = c.lookup_by_code(code)
            if result is not None:
                return result

    @staticmethod
    def lookup_by_full_name_helper(parent, names):
        if parent is None or names is None: return None
        for account in parent.children:
            if account.name.strip() == names[0]:
                if len(names) == 1:
                    return account
                if not account.children:
                    return None
                found = Account.lookup_by_full_name_helper(account, names[1:])
                if found is not None:
                    return found
        return None

    def lookup_by_name(self, name: str):
        if name is None:
            return None
        name = name.strip()
        for c in self.children:
            if str(c.name).strip() == name:
                return c
        for c in self.children:
            result = c.lookup_by_name(name)
            if result is not None:
                return result

    def get_last_num(self):
        return self.last_num

    def set_last_num(self, num):
        self.begin_edit()
        self.last_num = num
        self.mark()
        self.commit_edit()

    def tree_begin_staged_transaction_traversals(self):
        descendants = self.get_descendants()
        for acc in descendants:
            for split in acc.splits:
                if split.transaction is not None:
                    split.transaction.marker = 0

    def tree_staged_transaction_traversals(self, stage, fun, *args):
        for acc in self.children:
            retval = acc.tree_staged_transaction_traversals(stage, fun, *args)
            if retval:
                return retval
        for split in self.splits:
            trans = split.transaction
            if trans is not None and trans.marker < stage:
                trans.marker = stage
                if fun is not None:
                    retval = fun(trans, *args)
                    if retval:
                        return retval
        return 0

    def begin_staged_transaction_traversals(self):
        for split in self.splits:
            if split.transaction is not None:
                split.transaction.marker = 0

    def staged_transaction_traversals(self, stage, fun, *args):
        for split in self.splits:
            trans = split.transaction
            if trans is not None and trans.marker < stage:
                trans.marker = stage
                if fun is not None:
                    retval = fun(trans, *args)
                    if retval:
                        return retval
        return 0

    def tree_foreach_transaction(self, fn, *args):
        self.tree_begin_staged_transaction_traversals()
        self.tree_staged_transaction_traversals(42, fn, *args)

    def foreach_transaction(self, fn, *args):
        self.begin_staged_transaction_traversals()
        self.staged_transaction_traversals(42, fn, *args)

    def foreach_child(self, fn, *args):
        for child in self.children:
            fn(child, *args)

    def foreach_descendant(self, fn, *args):
        for child in self.children:
            fn(child, *args)
            child.foreach_descendant(fn, *args)

    def foreach_descendant_until(self, fn, *args):
        for child in self.children:
            result = fn(child, *args)
            if result and result is not None:
                return result
            result = child.foreach_descendant_until(fn, *args)
            if result and result is not None:
                return result
        return None

    def is_root(self):
        return True if self.parent is None else False

    def get_commodity(self):
        return self.commodity

    def set_commodity(self, comm):
        if comm == self.commodity:
            return
        self.begin_edit()
        if self.commodity is not None:
            self.commodity.decrement_usage_count()
        self.commodity = comm
        comm.increment_usage_count()
        self._commodity_scu = comm.get_fraction()
        self._non_std_scu = 0
        for s in self.splits:
            trans = s.get_transaction()
            if trans is not None:
                trans.begin_edit()
            s.set_amount(s.get_amount())
            if trans is not None:
                trans.commit_edit()
        self.sort_dirty = True
        self.balance_dirty = True
        self.mark()
        self.commit_edit()

    def get_splits(self):
        self.sort_splits(False)
        return self.splits

    def get_children(self):
        return self.children.copy()

    def get_children_sorted(self):
        return sorted(self.children.copy())

    def is_priced(self):
        return self.get_type() == AccountType.MUTUAL or self.get_type() == AccountType.STOCK or self.get_type() == AccountType.CURRENCY

    def get_child_index(self, account):
        if account is None:
            return -1
        try:
            return self.children.index(account)
        except ValueError:
            return -1

    def get_nth_child(self, i):
        try:
            return self.children[i]
        except IndexError:
            return None

    def get_n_children(self):
        return len(self.children)

    def count_splits(self, include_children):
        nr = len(self.splits)
        if include_children and self.get_n_children() != 0:
            for acc in self.children:
                nr += acc.count_splits(True)
        return nr

    def get_full_name(self):
        if self.parent is not None and self.parent != self:
            pfn = self.parent.get_full_name()
            if pfn:
                return u"{}{}{}".format(pfn, Account.separator, self.name)
            else:
                return self.name
        return ""

    def set_balance_dirty(self):
        self.balance_dirty = True

    def set_sort_dirty(self):
        self.sort_dirty = True

    def recompute_balance(self):
        from .transaction import SplitState
        if not self.balance_dirty:
            return
        if self.get_edit_level() > 0:
            return
        if self.get_destroying():
            return
        if self.get_book().is_shutting_down():
            return
        noclosing_balance = self.starting_noclosing_balance
        balance = self.starting_balance
        reconciled_balance = self.starting_reconciled_balance
        cleared_balance = self.starting_cleared_balance

        for sp in self.splits:
            amt = sp.get_amount()
            balance += amt
            rn = sp.get_reconcile_state()
            if rn != SplitState.UNRECONCILED:
                cleared_balance += amt
            if rn == SplitState.RECONCILED or rn == SplitState.FROZEN:
                reconciled_balance += amt
            if sp.transaction is not None and not sp.transaction.is_closing():
                noclosing_balance += amt
            sp.balance = balance
            sp.noclosing_balance = noclosing_balance
            sp.cleared_balance = cleared_balance
            sp.reconciled_balance = reconciled_balance
        self.balance = balance
        self.noclosing_balance = noclosing_balance
        self.reconciled_balance = reconciled_balance
        self.cleared_balance = cleared_balance
        self.balance_dirty = False

    def get_balance(self):
        return self.balance

    def get_reconciled_balance(self):
        return self.reconciled_balance

    def get_cleared_balance(self):
        return self.cleared_balance

    def _get_balance_as_of_date(self, date, ignclosing):
        latest = None
        self.sort_splits(True)
        self.recompute_balance()
        for sp in self.splits:
            if sp.transaction is not None and sp.transaction.get_post_date() >= date:
                break
            latest = sp
        if latest is not None:
            return latest.get_no_closing_balance() if ignclosing else latest.get_balance()
        return Decimal(0)

    def get_balance_as_of_date(self, date):
        return self._get_balance_as_of_date(date, False)

    def get_no_closing_balance_as_of_date(self, date):
        return self._get_balance_as_of_date(date, True)

    def get_reconciled_balance_as_of_date(self, date):
        from .transaction import SplitState
        balance = Decimal(0)
        for sp in self.splits:
            if sp.get_reconcile_state() == SplitState.RECONCILED and sp.get_reconcile_date() <= date:
                balance += sp.get_amount()

        return balance

    def get_present_balance(self):
        return self.get_balance_as_of_date(datetime.date.today())

    def get_projected_minimum_balance(self):
        lowest = Decimal(0)
        seen_a_transaction = 0
        today = datetime.date.today()
        for sp in reversed(self.splits):
            if not seen_a_transaction:
                lowest = sp.balance
                seen_a_transaction = 1
            elif sp.balance.compare(lowest) < 0:
                lowest = sp.balance
            if sp.transaction is not None and sp.transaction.get_post_date() <= today:
                break
        return lowest

    def convert_balance_to_currency(self, balance, balance_currency, new_currency):
        from .price_db import PriceDB
        if balance.is_zero() or balance_currency == new_currency:
            return balance
        pdb = PriceDB.get_db(self.book)
        if pdb is not None:
            balance = pdb.convert_balance_latest_price(balance, balance_currency, new_currency)
        return balance

    def get_Xxx_balance_in_currency(self, fn, report_currency):
        if fn is None or report_currency is None: return Decimal(0)
        return self.convert_balance_to_currency(fn(self), self.commodity, report_currency)

    def get_balance_in_currency(self, commodity, include_children):
        return self.get_Xxx_balance_in_currency_recursive(Account.get_balance, commodity, include_children)

    def get_cleared_balance_in_currency(self, commodity, include_children):
        return self.get_Xxx_balance_in_currency_recursive(Account.get_cleared_balance, commodity, include_children)

    def get_reconciled_balance_in_currency(self, commodity, include_children):
        return self.get_Xxx_balance_in_currency_recursive(Account.get_reconciled_balance, commodity, include_children)

    def get_present_balance_in_currency(self, commodity, include_children):
        return self.get_Xxx_balance_in_currency_recursive(Account.get_present_balance, commodity, include_children)

    def get_projected_minimum_balance_in_currency(self, commodity, include_children):
        return self.get_Xxx_balance_in_currency_recursive(Account.get_projected_minimum_balance, commodity,
                                                          include_children)

    def get_balance_as_of_date_in_currency(self, date, report_commodity, include_children):
        return self.get_Xxx_balance_as_of_date_in_currency_recursive(date, Account.get_balance_as_of_date,
                                                                     report_commodity, include_children)

    def get_balance_change_for_period(self, t1, t2, recurse):
        b1 = self.get_balance_as_of_date_in_currency(t1, None, recurse)
        b2 = self.get_balance_as_of_date_in_currency(t2, None, recurse)
        return b2 - b1

    def get_Xxx_balance_in_currency_recursive(self, fn, report_commodity, include_children):
        if report_commodity is None:
            report_commodity = self.get_commodity()
        if report_commodity is None:
            return Decimal(0)
        balance = self.get_Xxx_balance_in_currency(fn, report_commodity)
        if include_children:
            cb = CurrencyBalance(report_commodity, balance, fn, None, 0)
            self.foreach_descendant(self.balance_helper, cb)
            balance = cb.balance
        return balance

    def get_Xxx_balance_as_of_date_in_currency(self, date, fn, report_commodity):
        if fn is None or report_commodity is None:
            return Decimal(0)
        return self.convert_balance_to_currency(fn(self, date), self.commodity, report_commodity)

    def get_Xxx_balance_as_of_date_in_currency_recursive(self, date, fn, report_commodity, include_children):
        if report_commodity is None:
            report_commodity = self.get_commodity()
        if report_commodity is None:
            return Decimal(0)
        balance = self.get_Xxx_balance_as_of_date_in_currency(date, fn, report_commodity)
        if include_children:
            cb = CurrencyBalance(report_commodity, 0, None, fn, date)
            cb.balance = balance
            self.foreach_descendant(self.get_balance_as_of_date_helper, cb)
            balance = cb.balance
        return balance

    @staticmethod
    def balance_helper(acc, cb):
        if cb.fn is None or cb.currency is None: return
        cb.balance += acc.get_Xxx_balance_in_currency(cb.fn, cb.currency)

    @staticmethod
    def get_balance_as_of_date_helper(acc, cb):
        if cb is None or cb.asOfDateFn is None or cb.currency is None:
            return
        cb.balance += acc.get_Xxx_balance_as_of_date_in_currency(cb.date, cb.asOfDateFn, cb.currency)

    def sort_splits(self, force):
        if not self.sort_dirty or (not force and self.get_edit_level() > 0):
            return
        self.splits = list(sorted(self.splits))
        self.sort_dirty = False
        self.balance_dirty = True

    def get_currency_or_parent(self):
        commodity = self.get_commodity()
        if commodity.is_currency():
            return commodity
        else:
            parent_account = self.parent
            while parent_account is not None:
                commodity = parent_account.get_commodity()
                if commodity is not None and commodity.is_currency():
                    return commodity
                parent_account = parent_account.get_parent()
        return None

    def is_template(self):
        return self.commodity.is_template()

    def __repr__(self):
        if self.commodity:
            return u"Account<{acc.name}[{acc.commodity.mnemonic}]>".format(acc=self)
        else:
            return u"Account<{acc.name}>".format(acc=self)

    def __lt__(self, other):
        return self.name < other.name

    def __len__(self):
        return len(self.children)

    def bring_up_to_date(self):
        self.sort_splits(False)
        self.recompute_balance()

    def free_children(self):
        for acc in self.children.copy():
            if acc.get_edit_level() == 0:
                acc.begin_edit()
            acc.destroy()
        self.children.clear()

    def destroy_do(self):
        self.increase_edit_level()
        self.free_children()
        book = self.get_book()
        if not book.is_shutting_down():
            for sp in self.splits:
                sp.destroy()
        self.splits.clear()
        try:
            self.lots.clear()
        except DetachedInstanceError:
            pass
        self.set_dirty()
        self.decrease_edit_level()

    def commit_edit(self):
        if not super().commit_edit():
            return
        if self.get_destroying():
            be = self.book.get_backend()
            if be is not None:
                with be.add_lock():
                    self.destroy_do()
            else:
                self.destroy_do()
        else:
            self.bring_up_to_date()
        self.commit_edit_part2(self.on_error, self.on_done, self.on_free)

    def on_done(self):
        Event.gen(self, EVENT_MODIFY)

    def on_free(self):
        try:
            if self.parent is not None:
                self.parent.remove_child(self)
        except DetachedInstanceError:
            pass
        self.free()

    @staticmethod
    def free_one_child(acct):
        if acct is None: return
        if acct.get_edit_level() == 0:
            acct.begin_edit()
        acct.destroy()

    def free(self):
        Event.gen(self, EVENT_DESTROY)
        if self.children is not None and len(self.children):
            self.free_children()
        try:
            if len(self.lots):
                for lot in self.lots:
                    lot.destroy()
        except DetachedInstanceError:
            pass
        try:
            if len(self.splits):
                self.reset_edit_level()
                slist = self.splits.copy()
                for sp in slist:
                    if sp.get_account() == self:
                        sp.destroy()
            if self.custom_data is not None:
                self.custom_data.clear()
        except DetachedInstanceError:
            pass
        self.splits.clear()
        self.children.clear()
        self.dispose()

    @staticmethod
    def name_violations_errmsg(separator, invalid_account_names):
        if not invalid_account_names:
            return None
        account_list = ""
        for p in invalid_account_names:
            if not account_list:
                account_list = p
            else:
                account_list = account_list + "\n" + p
        message = "The separator character \"%s\" is used in one or more account names.\n\n" \
                  "This will result in unexpected behaviour. " \
                  "Either change the account names or choose another separator character.\n\n" \
                  " Below you will find the list of invalid account names:\n%s" % (separator, account_list)
        return message

    @staticmethod
    def list_name_violations(book, separator):
        invalid_list = []
        if book is None:
            return
        root_account = book.get_root_account()
        if separator is None: return None
        if root_account is None:
            return None
        accounts = root_account.get_descendants()
        for acct in accounts:
            acct_name = acct.get_name()
            if acct_name.find(separator) != -1:
                invalid_list.insert(0, acct_name)
        return invalid_list

    @staticmethod
    def pre_split_move(split):
        split.get_transaction().begin_edit()

    @staticmethod
    def post_split_move(split, accto):
        split.set_account(accto)
        split.set_amount(split.get_amount())
        split.get_transaction().commit_edit()

    def move_all_splits(self, other):
        be = self.book.get_backend()
        if be is not None:
            with be.add_lock():
                if other is None or not isinstance(other, Account): return
                if len(self.splits) == 0 or self == other: return
                if self.get_book() != other.get_book(): return
                self.begin_edit()
                other.begin_edit()
                for split in self.splits:
                    self.pre_split_move(split)
                for split in self.splits:
                    self.post_split_move(split, other)
                self.commit_edit()
                other.commit_edit()


    def order(self, other):
        if self is not None and other is None: return -1
        if self is None and other is not None: return 1
        if self is None and other is None: return 0
        da = self.code
        db = other.code
        if da is not None and db is not None:
            try:
                la = int(da)
                lb = int(db)
                if la < lb: return -1
                if la > lb: return +1
            except (ValueError, TypeError):
                if da < db: return -1
                if da > db: return +1

        ta = self.type
        tb = other.type
        if ta < tb: return -1
        if ta > tb: return +1
        da = self.name
        db = other.name
        if da < db: return -1
        if da > db: return +1
        return self.guid_compare(other)

    def set_filter(self, fl):
        if fl != self.filter:
            self.begin_edit()
            self.filter = fl
            self.set_dirty()
            self.commit_edit()

    def set_sort_order(self, so):
        if so is not None and so != self.sort_order:
            self.begin_edit()
            self.sort_order = so
            self.set_dirty()
            self.commit_edit()

    def get_filter(self):
        return self.filter

    def get_sort_order(self):
        return self.sort_order

    def get_sort_reversed(self):
        return self.sort_reversed

    def set_sort_reversed(self, sr):
        self.sort_reversed = sr

    def set_policy(self, pc):
        self.policy = pc if pc is not None else FifoPolicy()

    def get_policy(self):
        return self.policy

    def get_lots(self):
        return self.lots.copy()

    def remove_lot(self, lot):
        if lot is None: return
        if len(self.lots) == 0: return
        try:
            self.lots.remove(lot)
        except ValueError:
            return
        Event.gen(lot, EVENT_REMOVE)
        Event.gen(self, EVENT_MODIFY)

    def insert_lot(self, lot):
        lot_account = lot.get_account()
        if lot_account == self:
            return
        if lot_account is not None:
            try:
                lot_account.lots.remove(lot)
            except ValueError:
                return
        self.lots.append(lot)
        lot.set_account(self)
        Event.gen(lot, EVENT_ADD)
        Event.gen(self, EVENT_MODIFY)

    def find_open_lots(self, match_func, sort_func=None, *user_data):
        a = list(filter(lambda x: not x.is_closed(), self.lots))
        if match_func is not None:
            a = list(filter(lambda x: match_func(x, *user_data), a))
        if sort_func is not None:
            a.sort(key=cmp_to_key(sort_func))
        return a

    def get_tree_depth(self):
        account = self
        depth = 0
        while account.parent is not None and account.type != AccountType.ROOT:
            account = account.parent
            depth += 1
        return depth

    def refers_to(self, other):
        if other == self.commodity:
            return True

        elif other == self.scheduled_transaction:
            return True
        return False

    def __hash__(self):
        return id(self)

    def referred(self, other):
        from libgasstation.core.commodity import Commodity
        from libgasstation.core.scheduled_transaction import ScheduledTransaction
        if not isinstance(other, (Commodity, ScheduledTransaction)):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)

    @classmethod
    def book_end(cls, book):
        root = book.get_root_account()
        if root is not None:
            root.begin_edit()
            root.destroy()

    @classmethod
    def register(cls):
        params = [Param(ACCOUNT_NAME_, TYPE_STRING, cls.get_name, cls.set_name),
                  Param(ACCOUNT_CODE_, TYPE_STRING, cls.get_code, cls.set_code),
                  Param(ACCOUNT_DESCRIPTION_, TYPE_STRING, cls.get_description, cls.set_description),
                  Param(ACCOUNT_COLOR_, TYPE_STRING, cls.get_color, cls.set_color),
                  Param(ACCOUNT_FILTER_, TYPE_STRING, cls.get_filter, cls.set_filter),
                  Param(ACCOUNT_SORT_ORDER_, TYPE_STRING, cls.get_sort_order, cls.set_sort_order),
                  Param(ACCOUNT_SORT_REVERSED_, TYPE_BOOLEAN, cls.get_sort_reversed, cls.set_sort_reversed),
                  Param(ACCOUNT_NOTES_, TYPE_STRING, cls.get_notes, cls.set_notes),
                  Param(ACCOUNT_PRESENT_, TYPE_NUMERIC, cls.get_present_balance),
                  Param(ACCOUNT_BALANCE_, TYPE_NUMERIC, cls.get_balance),
                  Param(ACCOUNT_CLEARED_, TYPE_NUMERIC, cls.get_cleared_balance),
                  Param(ACCOUNT_RECONCILED_, TYPE_NUMERIC, cls.get_reconciled_balance),
                  Param(ACCOUNT_TYPE_, TYPE_ENUM, cls.get_type, cls.set_type),
                  Param(ACCOUNT_FUTURE_MINIMUM_, TYPE_NUMERIC, cls.get_projected_minimum_balance),
                  Param(ACCOUNT_TAX_RELATED, TYPE_BOOLEAN, cls.get_tax_related, cls.set_tax_related),
                  Param(ACCOUNT_SCU, TYPE_INT32, cls.get_commodity_scu, cls.set_commodity_scu),
                  Param(ACCOUNT_NSCU, TYPE_BOOLEAN, cls.get_non_std_scu, cls.set_non_std_scu),
                  Param(ACCOUNT_PARENT, ID_ACCOUNT, cls.get_parent, cls.set_parent),
                  Param("book", "Book", cls.get_book),
                  Param("guid", TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_ACCOUNT, cls.__eq__, params)
