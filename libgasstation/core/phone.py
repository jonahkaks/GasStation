import enum

from sqlalchemy import ForeignKey, Enum
from sqlalchemy.orm import reconstructor, relationship
from sqlalchemy_utils import PhoneNumberType

from ._declbase import *
from .engine import *
from .event import *
from .query_private import *


class PhoneType(enum.Enum):
    MOBILE = 0
    WORK = 1
    HOME = 2
    MAIN = 3
    WORK_FAX = 4
    HOME_FAX = 5
    PAGER = 6
    OTHER = 7


class Phone(DeclarativeBaseGuid):
    __tablename__ = 'phone'

    __table_args__ = {}
    contact_guid = Column("contact_guid", UUIDType(binary=False), ForeignKey('contact.guid'), nullable=False)
    type = Column("type", Enum(PhoneType), default=PhoneType.WORK)
    number = Column("number", PhoneNumberType(region="UG"), nullable=False)
    contact = relationship("Contact", back_populates="phones")

    def __init__(self, book):
        super().__init__(ID_PHONE, book)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, ID_PHONE, book)
        Event.gen(self, EVENT_CREATE)

    def get_type(self):
        return self.type

    def set_type(self, t: PhoneType):
        if t != self.type:
            self.begin_edit()
            self.type = t
            self.set_dirty()
            self.commit_edit()

    def get_contact(self):
        return self.contact

    def set_contact(self, t):
        if t != self.contact:
            self.begin_edit()
            self.contact = t
            self.set_dirty()
            self.commit_edit()

    def get_number(self):
        return self.number

    def set_number(self, t):
        if t != self.number:
            self.begin_edit()
            self.number = t
            self.set_dirty()
            self.commit_edit()

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def on_error(self, error):
        Engine.signal_commit_error(error)

    def __eq__(self, other):
        if other is None:
            return False
        return self.guid == other.guid

    @classmethod
    def register(cls):
        params = [
            Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
            Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None)]
        ObjectClass.register(cls, ID_PHONE, cls.__eq__, params)

    def __str__(self):
        return "Phone<{}>".format(self.number)
