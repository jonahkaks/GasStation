from sqlalchemy import VARCHAR, INTEGER
from sqlalchemy.orm import reconstructor

from ._declbase import *
from .event import *


class Bank(DeclarativeBaseGuid):
    __tablename__ = "bank"
    name = Column("name", VARCHAR(100))
    branch = Column("branch", VARCHAR(100))
    account_name = Column("account_name", VARCHAR(100))
    account_number = Column("account_number", INTEGER)

    def __init__(self, book, bank):
        super().__init__("ContactImage", book)
        self.bank = bank
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, "ContactImage", book)
        Event.gen(self, EVENT_CREATE)

    def get_name(self):
        return self.name

    def set_name(self, name):
        if name != self.name:
            self.begin_edit()
            self.name = name
            self.set_dirty()
            self.commit_edit()

    def get_branch(self):
        return self.branch

    def set_branch(self, branch):
        if branch != self.branch:
            self.begin_edit()
            self.branch = branch
            self.set_dirty()
            self.commit_edit()

    def get_account_name(self):
        return self.account_name

    def set_account_name(self, account_name):
        if account_name != self.account_name:
            self.begin_edit()
            self.account_name = account_name
            self.set_dirty()
            self.commit_edit()

    def get_account_number(self):
        return self.account_number

    def set_account_number(self, account_number):
        if account_number != self.account_number:
            self.begin_edit()
            self.account_number = account_number
            self.set_dirty()
            self.commit_edit()

