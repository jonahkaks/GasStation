class Collection(dict):
    def __init__(self, _type):
        super().__init__()
        self.e_type = _type
        self.dirty = False
        self.data = None

    def destroy(self):
        self.e_type = None
        self.dirty = False
        self.data = None
        self.clear()
        del self

    def get_type(self):
        return self.e_type

    @staticmethod
    def remove_entity(inst):
        from .instance import Instance
        if inst is None: return
        col = Instance.get_collection(inst)
        if col is None: return
        guid = inst.guid
        if col.get(guid) is not None:
            col.pop(guid)
        inst.set_collection(None)

    def insert_entity(self, ent):
        if ent is None: return
        guid = ent.guid
        if self.e_type != ent.e_type: return
        self.remove_entity(ent)
        self[guid] = ent
        ent.set_collection(self)

    def add_entity(self, ent):
        if ent is None: return
        guid = ent.guid
        if guid is None or guid == "":
            return False
        if self.e_type != ent.e_type: return
        e = self.lookup_entity(guid)
        if e is not None:
            return False
        self[guid] = ent
        return True

    def compare(self, other):
        if self is None or other is None: return 0
        if self == other: return 0
        if self is None and other is not None:
            return -1
        if self is not None and other is None:
            return 1

        if self.e_type != other.e_type:
            return -1

    def lookup_entity(self, guid):
        if guid is None or guid == "": return
        return self.get(guid)

    @classmethod
    def from_list(cls, _type, glist):
        coll = cls(_type)
        for ent in glist:
            if not coll.add_entity(ent):
                coll.destroy()
                return None
        return coll

    def count(self):
        return len(self)

    def is_dirty(self):
        return self.dirty

    def mark_clean(self):
        self.dirty = False

    def mark_dirty(self):
        self.dirty = True

    def set_data(self, user_data):
        self.data = user_data

    def get_data(self):
        return self.data

    def foreach(self, func, *user_data):
        for obj in self.values():
            func(obj, *user_data)

    def get_all(self):
        return list(self.values())

    def __repr__(self):
        return u"Collection<%s>" % self.e_type

    def __del__(self):
        self.destroy()
