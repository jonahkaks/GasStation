from sqlalchemy import VARCHAR, ForeignKey
from sqlalchemy.orm import reconstructor, relationship

from ._declbase import *
from .contact import Contact
from .event import *
from .query_private import *

LOCATION_NAME = "location_name"


class Location(DeclarativeBaseGuid):
    __tablename__ = "location"
    name: str = Column("name", VARCHAR(200), unique=True, nullable=False)
    is_main: bool = Column("is_main", BOOLEAN(), nullable=False, default=False)
    contact_guid: str = Column("contact_guid", UUIDType(binary=False), ForeignKey('contact.guid'), nullable=False)
    contact: Contact = relationship("Contact")
    # manager = relationship("Staff", foreign_keys=[manager_guid])
    staff = relationship("Staff", back_populates="location", primaryjoin=" Staff.location_guid==Location.guid")
    devices = relationship("Device", back_populates="location")

    def __init__(self, book):
        super().__init__(ID_LOCATION, book)
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, ID_LOCATION, book)
        Event.gen(self, EVENT_CREATE)

    def get_name(self):
        return self.name

    @classmethod
    def lookup_name(cls, book, name):
        a = book.get_collection(ID_LOCATION)
        for cat in a.values():
            if cat.name == name.strip():
                return cat

    def set_name(self, name: str):
        if name != self.name and isinstance(name, str):
            self.begin_edit()
            self.name = name
            self.set_dirty()
            self.commit_edit()

    def get_contact(self):
        return self.contact

    def set_contact(self, cont):
        if cont != self.contact:
            self.begin_edit()
            self.contact = cont
            self.set_dirty()
            self.commit_edit()

    def refers_to(self, other):
        pass

    @classmethod
    def register(cls):
        params = [
            Param(LOCATION_NAME, TYPE_STRING, cls.get_name, cls.set_name),
            Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
            Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None)]
        ObjectClass.register(cls, ID_LOCATION, cls.__eq__, params)

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def __lt__(self, other):
        if other is None:
            return False
        if other.name is None or self.name is None:
            return False
        return self.name < other.name
