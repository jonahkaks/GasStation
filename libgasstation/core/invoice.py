from sqlalchemy import INTEGER

from .account import Account, AccountType
from .commodity import Commodity
from .entry import InvoiceEntry
from .helpers import gs_set_num_action
from .product import Product
from .scrub import Scrub
from .term import Term
from .transaction import *

INVOICE_ID = "id"
INVOICE_CUSTOMER = "customer"
INVOICE_OPENED = "date_opened"
INVOICE_POSTED = "date_posted"
INVOICE_DUE = "date_due"
INVOICE_IS_POSTED = "is_posted?"
INVOICE_IS_PAID = "is_paid?"
INVOICE_TERMS = "terms"
INVOICE_NOTES = "notes"
INVOICE_DOCLINK = "doclink"
INVOICE_ACC = "account"
INVOICE_POST_TXN = "posted_txn"
INVOICE_IS_CN = "credit_note"
INVOICE_TYPE = "type"
INVOICE_TYPE_STRING = "type_string"
INVOICE_ENTRIES = "list_of_entries"
INVOICE_FROM_LOT = "invoice-from-lot"
INVOICE_FROM_TXN = "invoice-from-txn"


class Invoice(DeclarativeBaseGuid):
    __tablename__ = 'invoice'

    __table_args__ = {}

    # column definitions
    id = Column('id', VARCHAR(length=200), nullable=False)
    date_posted = Column('date_posted', Date(), nullable=False)
    date_due = Column('date_due', Date(), nullable=False)
    notes = Column('notes', VARCHAR(length=200))
    active = Column('active', BOOLEAN(), nullable=False, default=True)
    is_credit_note = Column('is_credit', BOOLEAN(), nullable=False, default=False)
    currency_guid = Column('currency_guid', UUIDType(binary=False), ForeignKey('commodity.guid'), nullable=False)
    location_guid = Column('location_guid', UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    customer_guid = Column('customer_guid', UUIDType(binary=False), ForeignKey("customer.guid"), nullable=False)
    term_guid = Column('term_guid', UUIDType(binary=False), ForeignKey('term.guid'))
    post_transaction_guid = Column('post_transaction_guid', UUIDType(binary=False), ForeignKey('transaction.guid'),
                                   nullable=False)
    post_account_guid = Column('post_account_guid', UUIDType(binary=False), ForeignKey('account.guid'), nullable=False)

    terms = relationship('Term')
    currency = relationship('Commodity')
    location = relationship("Location")
    customer = relationship('Customer', back_populates='invoices', uselist=False)
    posted_account = relationship('Account')
    payments = relationship("CustomerPayment", back_populates="invoice")
    posted_txn = relationship('Transaction')
    entries: List[InvoiceEntry] = relationship('InvoiceEntry', lazy="subquery", back_populates="invoice",
                                               cascade="all, delete-orphan")

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        Instance.__init__(self, ID_INVOICE, book)
        Event.gen(self, EVENT_CREATE)

    def __init__(self, book):
        super().__init__(ID_INVOICE, book)
        self.is_credit_note = False
        self.prices = []
        self.active = True
        self.infant = True
        self.id = book.format_counter("Invoice")
        Event.gen(self, EVENT_CREATE)

    def get_active(self):
        return self.active

    def set_active(self, active):
        if active != self.active:
            self.begin_edit()
            self.active = active
            self.set_dirty()
            self.commit_edit()

    def get_location(self):
        return self.location

    def set_location(self, location):
        if location != self.location:
            self.begin_edit()
            self.location = location
            self.set_dirty()
            self.commit_edit()

    @staticmethod
    def lookup(guid, book):
        if guid is None or book is None: return
        col = book.get_collection(ID_INVOICE)
        return col.lookup_entity(guid)

    @classmethod
    def from_lot(cls, lot):
        if lot is None:
            return
        return lot.get_invoice()

    def get_customer(self):
        return self.customer

    def set_customer(self, customer):
        if customer != self.customer:
            self.begin_edit()
            self.customer = customer
            self.set_dirty()
            self.commit_edit()

    def get_currency(self):
        return self.currency

    def set_currency(self, currency):
        if currency != self.currency:
            self.begin_edit()
            self.currency = currency
            self.set_dirty()
            self.commit_edit()

    def get_date_opened(self):
        return self.date_opened

    def set_date_opened(self, date_opened):
        if date_opened != self.date_opened:
            self.begin_edit()
            self.date_opened = date_opened
            self.set_dirty()
            self.commit_edit()

    def get_date_due(self):
        return self.date_due

    def set_date_due(self, date_due):
        if date_due != self.date_due:
            self.begin_edit()
            self.date_due = date_due
            self.set_dirty()
            self.commit_edit()

    def get_date_posted(self):
        return self.date_posted

    def set_date_posted(self, date_posted):
        if date_posted != self.date_posted:
            self.begin_edit()
            self.date_posted = date_posted
            self.set_dirty()
            self.commit_edit()

    def get_terms(self):
        return self.terms

    def set_terms(self, terms):
        if terms != self.terms:
            self.begin_edit()
            if self.terms is not None:
                self.terms.decrement_ref()
            self.terms = terms
            if self.terms is not None:
                self.terms.increment_ref()
            self.set_dirty()
            self.commit_edit()

    def get_notes(self):
        return self.notes

    def set_notes(self, notes):
        if notes != self.notes:
            self.begin_edit()
            self.notes = notes
            self.set_dirty()
            self.commit_edit()

    def get_posted_txn(self):
        return self.posted_txn

    def set_posted_txn(self, post_txn):
        if post_txn != self.posted_txn:
            self.begin_edit()
            self.posted_txn = post_txn
            self.set_dirty()
            self.commit_edit()

    def get_name(self):
        return "Invoice<{}>".format(self.get_id())

    def get_posted_account(self):
        return self.posted_account

    def set_posted_account(self, posted_account):
        if posted_account != self.posted_account:
            self.begin_edit()
            self.posted_account = posted_account
            self.set_dirty()
            self.commit_edit()

    def add_entry(self, entry):
        if entry is None:
            return
        old = entry.get_invoice()
        if old is not None and old == self:
            return
        if old is not None:
            old.remove_entry(entry)
        self.begin_edit()
        self.entries.append(entry)
        self.entries.sort()
        self.set_dirty()
        self.commit_edit()

    def remove_entry(self, entry):
        if entry is None:
            return
        self.begin_edit()
        entry.set_invoice(None)
        self.entries.remove(entry)
        self.set_dirty()
        self.commit_edit()

    def get_is_credit_note(self):
        return self.is_credit_note

    def set_is_credit_note(self, credit_note):
        self.begin_edit()
        self.is_credit_note = credit_note
        self.set_dirty()
        self.commit_edit()

    def add_price(self, price):
        if price is None:
            return
        commodity = price.get_commodity()
        node = None
        for curr in self.prices.copy():
            if curr.get_commodity() == commodity:
                node = curr
        self.begin_edit()
        if node is not None:
            self.prices.remove(node)
        self.prices.append(price)
        self.set_dirty()
        self.commit_edit()

    def get_price(self, commodity):
        for curr in self.prices:
            if curr.get_commodity() == commodity:
                return curr

    def get_id(self):
        return self.id if self.id is not None else self.book.format_counter("Invoice")

    def set_id(self, _id):
        if _id != self.id:
            self.begin_edit()
            self.id = _id
            self.set_dirty()
            self.commit_edit()

    def is_posted(self):
        return self.posted_txn is not None and isinstance(self.posted_txn, Transaction)

    def is_paid(self):
        return sum(map(lambda p: p.amount, self.payments)) == self.get_total()

    def post_add_split(self, book, acc: Account, txn: Transaction, value: Decimal, memo: str, _type):
        split = Split(book)
        split.set_memo(memo)
        acc.begin_edit()
        split.set_account(acc)
        acc.commit_edit()
        gs_set_num_action(None, split, self.get_id(), _type)
        txn.append_split(split)
        if acc.get_commodity() == self.currency:
            split.set_base_value(value, self.currency)
        else:
            price = self.get_price(acc.get_commodity())
            if price is None:
                return False
            else:
                split.set_value(value)
                converted_amount = value / price.get_value()
                split.set_amount(converted_amount)
        return True

    @classmethod
    def from_transaction(cls, txn):
        if txn is None:
            return
        return txn.get_invoice()

    def post_to_account(self, receivable_account: Account, post_date: datetime.date):
        if receivable_account is None:
            return
        if self.is_posted():
            return
        self.begin_edit()
        book = self.get_book()
        if self.terms is not None:
            self.set_terms(self.terms.return_child(True))
        is_cn = self.is_credit_note
        memo = self.notes
        _type = "Credit Note" if self.is_credit_note else "Invoice"
        txn = Transaction(book)
        txn.begin_edit()
        txn.set_location(self.get_location())
        txn.set_post_date(post_date)
        txn.set_type(TransactionType.INVOICE)
        txn.set_notes(self.get_notes())
        txn.set_enter_date(datetime.datetime.now())
        name = self.customer.get_contact().get_display_name()
        txn.set_description("{}-{}".format(self.location.get_name(), name))
        gs_set_num_action(txn, None, self.get_id(), _type)
        txn.set_currency(self.currency)
        total = self.get_total()
        taxes = self.get_total_tax_list()
        if not is_cn:
            total *= -1
            for acc_val in taxes:
                taxes[acc_val] *= -1
        acc_dict = defaultdict(Decimal)
        root_account = book.get_root_account()

        inventory_account = Scrub.get_or_make_account_with_fullname_parent(root_account, self.currency, "Inventory Asset",AccountType.CURRENT_ASSET,
                                                                           "Asset", "Current Assets")
        cost_of_goods_account = Scrub.get_or_make_account_with_fullname_parent(root_account, self.currency, "Cost of Sales",AccountType.COST_OF_SALES,
                                                                               "Expenses")
        acc_dict.setdefault(inventory_account, Decimal(0))
        acc_dict.setdefault(cost_of_goods_account, Decimal(0))
        for entry in self.entries:
            value = entry.get_value()
            if not is_cn:
                value *= -1
            sales_account = entry.get_account()
            product: Product = entry.get_product()
            cogs_value = product.adjust_inventory(self.location, -entry.get_quantity())

            acc_dict.setdefault(sales_account, Decimal(0))
            if sales_account is not None:
                acc_dict[sales_account] += value
            if cogs_value > 0:
                if inventory_account is not None:
                    acc_dict[inventory_account] -= cogs_value
                if cost_of_goods_account is not None:
                    acc_dict[cost_of_goods_account] += cogs_value
        for acc, val in acc_dict.items():
            if acc is not None and not val.is_zero() and not self.post_add_split(book, acc, txn, val, memo, _type):
                return None
        split = Split(book)
        split.set_memo(memo)
        receivable_account.begin_edit()
        split.set_account(receivable_account)
        receivable_account.commit_edit()
        gs_set_num_action(None, split, self.get_id(), _type)
        txn.append_split(split)
        split.set_base_value(-total, self.currency)

        self.set_posted_account(receivable_account)
        txn.set_readonly("Generated from an invoice. Try UnPosting the invoice.")
        txn.commit_edit()
        self.posted_txn = txn
        self.set_dirty()
        self.commit_edit()
        return txn

    def get_total(self):
        return self.__get_total_internal(True, True, False)

    def add_payment(self, payment, amount):
        self.begin_edit()
        self.customer.add_payment(payment, self, amount)
        self.set_dirty()
        self.commit_edit()

    def get_balance(self):
        return self.get_total() - sum(map(lambda p: p.amount, self.payments))

    def get_subtotal(self):
        return self.__get_total_internal(True, False, False)

    def get_total_tax(self):
        return self.__get_total_internal(False, True, False)

    @staticmethod
    def __sum_taxes_internal(taxes):
        tt = Decimal(0)
        if taxes is not None and any(taxes):
            for acc, val in taxes.items():
                tt += val
        return tt

    def __get_total_internal(self, use_value, use_tax, use_payment_type, _type=0):
        total, taxes = self.__get_net_and_taxes_internal(use_value, use_tax, use_payment_type, _type)
        if use_tax:
            total += self.__sum_taxes_internal(taxes)
        return total

    def get_total_tax_list(self):
        total, taxes = self.__get_net_and_taxes_internal(False, True, False, 0)
        return taxes

    def __get_net_and_taxes_internal(self, use_value, use_tax, use_payment_type, _type):
        net_total = Decimal(0)
        tv_list = defaultdict(Decimal)
        for entry in self.entries:
            if use_value:
                net_total += entry.get_value()
            if use_tax:
                tv_list = entry.get_tax_value()
        return net_total, tv_list

    def get_entries(self):
        return self.entries

    def commit_edit(self):
        if not super().commit_edit():
            return False
        self.commit_edit_part2(self.on_error, self.cleanup_commit, self.do_destroy)

    def do_destroy(self):
        Event.gen(self, EVENT_DESTROY)
        self.dispose()

    def cleanup_commit(self):
        Event.gen(self, EVENT_MODIFY)

    def refers_to(self, other):
        from .customer import Customer
        if isinstance(other, Term):
            return self.terms == other
        elif isinstance(other, Commodity):
            return self.currency == other
        elif isinstance(other, Account):
            return self.posted_account == other
        elif isinstance(other, Transaction):
            return self.posted_txn == other

        elif isinstance(other, Customer):
            return self.customer == other

        return False

    def referred(self, other):
        if not isinstance(other, (Term, Commodity, Account, Transaction, InvoiceEntry)):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)

    def __lt__(self, other):
        if other is None or not isinstance(other, Invoice):
            return False
        if self.id < other.id:
            return True
        if self.date_posted < other.date_posted:
            return True
        return self.guid < other.guid

    def __repr__(self):
        return u"Invoice<{}>".format(
            self.customer.get_contact().get_display_name() if self.customer is not None else "")

    @classmethod
    def register(cls):
        params = [Param(INVOICE_ID, TYPE_STRING, cls.get_id, cls.set_id),
                  Param(INVOICE_CUSTOMER, ID_CUSTOMER, cls.get_customer),
                  Param(INVOICE_OPENED, TYPE_DATE, cls.get_date_opened, cls.set_date_opened),
                  Param(INVOICE_DUE, TYPE_DATE, cls.get_date_due, cls.set_date_due),
                  Param(INVOICE_POSTED, TYPE_DATE, cls.get_date_posted, cls.set_date_posted),
                  Param(INVOICE_IS_POSTED, TYPE_BOOLEAN, cls.is_posted, None),
                  Param(INVOICE_IS_PAID, TYPE_BOOLEAN, cls.is_paid, None),
                  Param(INVOICE_NOTES, TYPE_STRING, cls.get_notes, cls.set_notes),
                  Param(INVOICE_ACC, ID_ACCOUNT, cls.get_posted_account, cls.set_posted_account),
                  Param(INVOICE_POST_TXN, ID_TRANS, cls.get_posted_txn, cls.set_posted_txn),
                  Param(INVOICE_TERMS, ID_TERM, cls.get_terms, cls.set_terms),
                  Param(INVOICE_ENTRIES, TYPE_COLLECT, cls.get_entries, None),
                  Param(PARAM_ACTIVE, TYPE_BOOLEAN, cls.get_active, cls.set_active),
                  Param(INVOICE_IS_CN, TYPE_BOOLEAN, cls.get_is_credit_note, cls.set_is_credit_note),
                  Param(PARAM_BOOK, ID_BOOK, cls.get_book),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid)]
        ObjectClass.register(cls, ID_INVOICE, None, params)
        ObjectClass.register(cls, ID_LOT, None, [Param(INVOICE_FROM_LOT, ID_INVOICE, cls.from_lot)])
        ObjectClass.register(cls, ID_TRANS, None, [Param(INVOICE_FROM_TXN, ID_INVOICE, cls.from_transaction)])


# This class exists in code but not in the GUI (to confirm?)

class Order(DeclarativeBaseGuid):
    __tablename__ = 'order'

    __table_args__ = {}

    # column definitions
    id = Column('id', VARCHAR(length=2048), nullable=False)
    notes = Column('notes', VARCHAR(length=2048), nullable=False)
    reference = Column('reference', VARCHAR(length=2048), nullable=False)
    active = Column('active', INTEGER(), nullable=False)

    date_opened = Column('date_opened', Date(), nullable=False)
    date_closed = Column('date_closed', Date(), nullable=False)
    customer_guid = Column('customer_guid', UUIDType(binary=False), ForeignKey("customer.guid"), nullable=False)
    customer = relationship('Customer', back_populates='orders')
