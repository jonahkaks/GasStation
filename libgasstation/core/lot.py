from sqlalchemy import INTEGER, ForeignKey, VARCHAR
from sqlalchemy.orm import reconstructor, relationship

from libgasstation.core.query_private import *
from ._declbase import *
from .event import *
from .object import *

LOT_IS_CLOSED = "is-closed?"
LOT_BALANCE = "balance"
LOT_TITLE = "lot-title"
LOT_NOTES = "notes"
LOT_CLOSED_UNKNOWN = -1


class Lot(DeclarativeBaseGuid):
    __tablename__ = 'lot'

    # column definitions
    account_guid = Column('account_guid', UUIDType(binary=False), ForeignKey('account.guid'))
    closed = Column('is_closed', INTEGER(), nullable=False, default=LOT_CLOSED_UNKNOWN)
    title = Column('title', VARCHAR(length=150))
    notes = Column('notes', VARCHAR(length=150))

    # relation definitions
    account = relationship('Account', back_populates='lots')
    splits = relationship('Split', back_populates='lot')
    e_type = ID_LOT

    def __init__(self, book):
        super().__init__(ID_LOT, book)
        self.closed = LOT_CLOSED_UNKNOWN
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init(self):
        from .session import Session
        super().__init__(ID_LOT, Session.get_current_book())
        self.cached_invoice = None
        Event.gen(self, EVENT_CREATE)

    def free(self):
        Event.gen(self, EVENT_DESTROY)
        for sp in self.splits:
            sp.lot = None
        self.splits.clear()

        if self.account is not None and not self.account.get_destroying():
            self.account.remove_lot(self)
        self.account = None
        self.closed = 1
        del self

    def commit_edit(self):
        if not super().commit_edit():
            return
        self.commit_edit_part2(self.on_error, None, self.free)

    def __repr__(self):
        return u"Lot<'{}' on {}>".format(self.title, self.account.name if self.account is not None else "")

    @staticmethod
    def lookup(guid, book):
        if guid is None or book is None: return
        col = book.get_collection(ID_LOT)
        return col.lookup_entity(guid)

    def is_closed(self):
        if self.closed < 0:
            return False
        return self.closed

    def get_account(self):
        return self.account

    def set_account(self, account):
        self.account = account

    def set_closed_unknown(self):
        self.closed = LOT_CLOSED_UNKNOWN

    def get_split_list(self):
        return self.splits

    def count_splits(self) -> int:
        return len(self.splits)

    def get_title(self):
        return self.title

    def set_title(self, title: str):
        self.begin_edit()
        self.title = title
        self.set_dirty()
        self.commit_edit()

    def get_notes(self):
        return self.notes

    def set_notes(self, notes):
        self.begin_edit()
        self.notes = notes
        self.set_dirty()
        self.commit_edit()

    def get_balance(self):
        zero = Decimal(0)
        baln = zero
        if len(self.splits) == 0:
            self.closed = 0
            return zero
        for sp in self.splits:
            baln += sp.get_amount()
        if baln.is_zero():
            self.closed = 1
        else:
            self.closed = 0
        return baln

    def get_balance_before(self, split):
        zero = Decimal(0)
        amt = zero
        val = zero
        if any(self.splits):
            target = split.get_gains_source_split()
            if target is None:
                target = split
            tb = target.get_transaction()
            for s in self.splits:
                source = s.get_gains_source_split()
                if source is None:
                    source = s
                ta = source.get_transaction()
                if (ta == tb and source != target) or ta.order(tb) < 0:
                    amt += s.get_amount()
                    val += s.get_value()
        return amt, val

    def add_split(self, split):
        if split is None: return
        self.begin_edit()
        acc = split.get_account()
        self.set_dirty()
        if self.account is None:
            acc.insert_lot(self)
        elif self.account != acc:
            self.commit_edit()
            return
        if self == split.get_lot():
            self.commit_edit()
            return
        if split.get_lot():
            split.get_lot().remove_split(split)
        split.set_lot(self)
        self.closed = LOT_CLOSED_UNKNOWN
        self.commit_edit()
        Event.gen(self, EVENT_MODIFY)

    def remove_split(self, split):
        if split is None: return
        self.begin_edit()
        self.set_dirty()
        try:
            self.splits.remove(split)
        except ValueError:
            return
        split.set_lot(None)
        self.closed = LOT_CLOSED_UNKNOWN
        if len(self.splits) == 0:
            self.account.remove_lot(self)
            self.account = None
        self.commit_edit()
        Event.gen(self, EVENT_MODIFY)

    def get_earliest_split(self):
        from .transaction import Split
        if len(self.splits) == 0: return None
        self.splits.sort(key=cmp_to_key(Split.order_date_only))
        return self.splits[0]

    def get_latest_split(self):
        from .transaction import Split
        if len(self.splits) == 0: return None
        self.splits.sort(key=cmp_to_key(Split.order_date_only), reverse=True)
        return self.splits[0]

    @classmethod
    def register(cls):
        params = [Param(LOT_TITLE, TYPE_STRING, cls.get_title, cls.set_title),
                  Param(LOT_NOTES, TYPE_STRING, cls.get_notes, cls.set_notes),
                  Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None),
                  Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
                  Param(LOT_IS_CLOSED, TYPE_BOOLEAN, cls.is_closed, None),
                  Param(LOT_BALANCE, TYPE_NUMERIC, cls.get_balance, None)]
        ObjectClass.register(cls, ID_LOT, None, params)

    @classmethod
    def make_default(cls, acc):
        if acc is None:
            return
        lot = cls(acc.get_book())
        acc.begin_edit()
        lid = acc.get_lot_next_id()
        buff = "%s %li" % ("Lot", lid)
        lot.set_title(buff)
        lid += 1
        acc.set_lot_next_id(lid)
        acc.commit_edit()
        return lot

    def refers_to(self, other):
        pass
