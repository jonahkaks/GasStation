import numpy as np
import pandas as pd
from nameparser import HumanName
from nameparser.config import CONSTANTS
from phonenumbers import geocoder, PhoneNumber, format_number, PhoneNumberFormat

# 0759683980
if __name__ == '__main__':
    # ctx = quick_login("admin", "admin")
    df = pd.read_excel(io="/home/newton/Documents/church/roofing.xlsx", sheet_name="goals", usecols=["NAME", "MTN",
                                                                                                     "GOAL", "PAID",
                                                                                                     "BALANCE",
                                                                                                     "L", "GROUP",
                                                                                                     "AIRTEL"])

    CONSTANTS.titles.add("Elder", "Pastor", "Pr")


    def names(a):
        if pd.isnull(a):
            return pd.Series(['', '', '', '', ''])
        n = HumanName(a.title())
        return pd.Series([n.title, n.first, n.middle, n.last, n.suffix])


    def phonenumber(a) -> PhoneNumber:
        num = str(int(a)) if not pd.isnull(a) else ""
        if len(num) == 9:
            num = '0' + num
        elif len(num) > 10:
            num = '+' + num
        else:
            return a
        return geocoder.parse(num, "UG")


    df.dropna(axis=0, inplace=True, how="all", subset=['NAME', 'BALANCE'])
    df["LANGUAGE"] = np.where(df["L"].isnull(), "LUGANDA", "ENGLISH")
    # df = df[df.AIRTEL.isnull()]
    df["AIRTEL"] = df["AIRTEL"].apply(phonenumber)
    df["MTN"] = df["MTN"].apply(phonenumber)
    df[['TITLE', 'FIRSTNAME', 'MIDDLENAME', 'LASTNAME', 'SUFFIX']] = df['NAME'].apply(names)
    df.drop(columns=["L"], inplace=True)
    # print(df.to_csv("contacts.csv"))
    i = 0
    with open("contacts.vcf", "w") as file:
        for person in df.itertuples():
            d = []
            if pd.isnull(person.AIRTEL) and pd.isnull(person.MTN):
                continue

            d.append("BEGIN:VCARD")
            d.append("VERSION:4.0")
            d.append("N:{};{};{};{};".format(person.LASTNAME, person.FIRSTNAME, person.MIDDLENAME, person.TITLE))
            d.append("FN:{}".format(person.NAME.title()))
            cats = [person.GROUP.title()]
            if not pd.isnull(person.AIRTEL):
                cats.append("Airtel")
                d.append("TEL;TYPE=cell:%s" % format_number(person.AIRTEL, PhoneNumberFormat.E164))
            if not pd.isnull(person.MTN):
                cats.append("Mtn")
                d.append("TEL;TYPE=cell:%s" % format_number(person.MTN, PhoneNumberFormat.E164))
            d.append("CATEGORIES:%s" % ",".join(cats))
            d.append("END:VCARD\n")
            file.writelines("\n".join(d))
        i += 1
        # if balance > 0: # if pd.isnull(goal): #     continue if language == "ENGLISH": msg = "SDA-Namuwongo,
        # {} your goal is {:,} and your balance {:,}".format(name, int(goal), int(balance)) else: msg =
        # "SDA-Namuwongo,{} tukusaba omaleyo goal yo {:,} nga owayo {:,}".format(name, int(goal), int(balance))
        # else: if language == "ENGLISH": msg = "SDA-Namuwongo,{} thank you for contribution {:,} towards roofing
        # of our church".format(name, int( paid)) else: msg = "SDA-Namuwongo,{} tukwebaza okuwayo {:,} okuzimba
        # ekkanisa".format(name, int(paid))
        # if person.LANGUAGE == "ENGLISH":
        #     msg = "SDA-Namuwongo,Dear {} {} {}, i remind you to come tommorrow and thank the lord as we conclude this " \
        #           "year from 1PM to 5PM".format(person.TITLE, person.FIRSTNAME, person.LASTNAME)
        # else:
        #     msg = "SDA-Namuwongo,Owoluganda {} {} {} nkulamusiza mu Kristo Yesu.Nkujukiza nti enkya Saawa" \
        #           " musanvu(1PM-5PM) okuja mukwebaza,nga tumalako omwaka.".format(person.TITLE, person.FIRSTNAME,
        #                                                                           person.LASTNAME).replace("  ",
        #                                                                                                    " ")
        # msg = "SDA-Namuwongo,Owoluganda {} {} {} nkulamusiza mu Kristo Yesu.Nkujukiza nti tuja kuba nokuyiga ku saawa 5pm paka 6PM.".format(person.TITLE, person.FIRSTNAME,
        #                                                                       person.LASTNAME).replace("  ",
        #                                                                                                " ")

        # if person.LANGUAGE == "ENGLISH":
        #     msg = "SDA-Namuwongo,Dear {} {} {}, this Sunday 3rd January 2021 we shall have prayer and fasting at " \
        #           "church led by Pr Sserunkuuma".format(person.TITLE, person.FIRSTNAME, person.LASTNAME)
        # else:
        #     msg = "SDA-Namuwongo,Owoluganda {} {} {}, Sunday eno nga 3rd January 2021 wajja kubaayo okusaba nokusiiba " \
        #           "ku kkanisa ne Pr Sserunkuuma.".format(person.TITLE, person.FIRSTNAME, person.LASTNAME)
        # msg = msg.replace("  ", " ").strip()
        # print(i, msg, len(msg), person.NUMBER)
    # # subprocess.check_output(['ls', '-l'])  # All that is technically needed... # print(subprocess.check_output([
    # 'kdeconnect-cli --send-sms "is it raining that side" --destination 0757564576 --device fea8981c19bb9155 #
    # ]))
    #     try:
    #         print(send_sms(ctx, "0" + str(person.NUMBER.national_number), msg))
    #     except Exception as e:
    #         print(e)
    # continue
    #
    # # print(numbers)
