from collections import namedtuple
from decimal import Decimal
EuroRate = namedtuple("EuroRate", "currency, rate")

euro_rates = [
    EuroRate("ATS", 13.7603),
    EuroRate("BEF", 40.3399),
    EuroRate("BFR", 40.3399),
    EuroRate("CYP", .585274),
    EuroRate("DEM", 1.95583),
    EuroRate("DM", 1.95583),
    EuroRate("EEK", 15.6466),
    EuroRate("ESC", 200.482),
    EuroRate("ESP", 166.386),
    EuroRate("EUR", 1.00000),
    EuroRate("EURO", 1.00000),
    EuroRate("FF", 6.55957),
    EuroRate("FIM", 5.94573),
    EuroRate("FMK", 5.94573),
    EuroRate("FRF", 6.55957),
    EuroRate("GRD", 340.750),
    EuroRate("HFL", 2.20371),
    EuroRate("IEP", .787564),
    EuroRate("IRP", .787564),
    EuroRate("ITL", 1936.27),
    EuroRate("LFR", 40.3399),
    EuroRate("LIT", 1936.27),
    EuroRate("LUF", 40.3399),
    EuroRate("LVL", .702804),
    EuroRate("MTL", .429300),
    EuroRate("NLG", 2.20371),
    EuroRate("PTA", 166.386),
    EuroRate("PTE", 200.482),
    EuroRate("S", 13.7603),
    EuroRate("SCH", 13.7603),
    EuroRate("SIT", 239.640),
    EuroRate("SKK", 30.1260)]


def bsearch(p, array, cmp):
    l = 0
    r = len(array) - 1
    while l <= r:
        mid = int(l + (r - l) / 2)
        if cmp(p, array[mid]) == 0:
            return array[mid]
        elif cmp(p, array[mid]) < 0:
            l = mid + 1
        else:
            r = mid - 1
    return None


def strcasecmp(a, b):
    if a == b:
        return 0
    return sum(map(ord, str(a).casefold().lower())) - sum(map(ord, str(b).casefold().lower()))


def euro_rate_compare(curr, euro):
    if curr is None or euro is None: return -1
    return strcasecmp(curr.get_mnemonic(), euro.currency)


def euro_rate_compare_code(code, euro):
    if code is None or euro is None: return -1
    return strcasecmp(code, euro.currency)


def gs_is_euro_currency(currency):
    if currency is None:
        return False
    if not currency.is_iso():
        return False
    return bsearch(currency, euro_rates, euro_rate_compare)


def gs_convert_to_euro(currency, value):
    if currency is None:
        return Decimal(0)
    if not currency.is_iso():
        return Decimal(0)

    result = bsearch(currency, euro_rates, euro_rate_compare)
    if result is None:
        return Decimal(0)
    return value / result.rate


def gs_convert_from_euro(currency, value):
    if currency is None:
        return Decimal(0)
    if not currency.is_iso():
        return Decimal(0)
    result = bsearch(currency, euro_rates, euro_rate_compare)
    if result is None:
        return Decimal(0)
    return value * result.rate


def gs_euro_currency_get_rate(currency):
    if currency is None:
        return Decimal(0)
    if not currency.is_iso():
        return Decimal(0)
    result = bsearch(currency, euro_rates, euro_rate_compare)
    if result is None:
        return Decimal(0)
    return result.rate


def gs_get_euro():
    from .commodity import CommodityTable, COMMODITY_NAMESPACE_NAME_CURRENCY
    from .session import Session
    table = CommodityTable.get_table(Session.get_current_book())
    return CommodityTable.lookup(table, COMMODITY_NAMESPACE_NAME_CURRENCY, "EUR")
