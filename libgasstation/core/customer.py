import enum

from sqlalchemy import DECIMAL
from sqlalchemy.orm import reconstructor

from .account import AccountType
from .invoice import Invoice
from .location import Location
from .payment import PaymentMethod
from .person import *
from .price_db import PriceDB
from .scrub import Scrub, EquityType
from .tax import Tax
from .term import Term

CUSTOMER_ID = "id"
CUSTOMER_NAME = "name"
CUSTOMER_NOTES = "notes"
CUSTOMER_DELIVERY_METHOD = "customer_delivery_method"
CUSTOMER_CREDIT = "amount of credit"
CUSTOMER_TT_OVER = "tax table override"
CUSTOMER_TAX_INC = "customer_tax_included"
CUSTOMER_TERMS = "customer_terms"
CUSTOMER_ACTIVE = "customer_is_active"
CUSTOMER_SLOTS = "customer_values"
CUSTOMER_FROM_LOT = "customer_lot"
CUSTOMER_PARENT_GUID = "customer_lot"


class DeliveryMethod(enum.Enum):
    SEND = "Send"
    PRINT = "Print"
    PRINT_SEND = "Print Send"

    @classmethod
    def from_int(cls, i):
        try:
            return [cls.SEND, cls.PRINT, cls.PRINT_SEND][i]
        except IndexError:
            return cls.SEND

    def __int__(self):
        return [DeliveryMethod.SEND, DeliveryMethod.PRINT, DeliveryMethod.PRINT_SEND].index(self)


class CustomerPayment(DeclarativeBase):
    __tablename__ = "customer_payment"
    guid: uuid.UUID = Column('guid', UUIDType(binary=False), primary_key=True,
                             nullable=False, default=lambda: uuid.uuid4())
    customer_guid = Column('customer_guid', UUIDType(binary=False), ForeignKey('customer.guid'), nullable=False)
    invoice_guid = Column('invoice_guid', UUIDType(binary=False), ForeignKey('invoice.guid'))
    payment_guid = Column('payment_guid', UUIDType(binary=False), ForeignKey('payment.guid'), primary_key=True,
                          nullable=False)
    amount: Decimal = Column("amount", DECIMAL, nullable=False, default=Decimal(0))
    customer = relationship("Customer", back_populates="payments")
    invoice = relationship("Invoice", back_populates="payments")
    payment:Payment = relationship("Payment", cascade="delete")

    def __init__(self, payment):
        self.payment = payment


class Customer(Person):
    __tablename__ = 'customer'

    __table_args__ = {}
    tin = Column("tin_no", VARCHAR(25))
    payment_method_guid = Column('payment_method_guid', UUIDType(binary=False), ForeignKey('payment_method.guid'),
                                 nullable=False)
    location_guid = Column('location_guid', UUIDType(binary=False), ForeignKey('location.guid'), nullable=False)
    parent_guid = Column('parent_guid', UUIDType(binary=False), ForeignKey('customer.guid'))
    term_guid = Column('term_guid', UUIDType(binary=False), ForeignKey('term.guid'))
    tax_guid = Column('tax_guid', UUIDType(binary=False), ForeignKey('tax.guid'))
    tax_included = Column("tax_included", BOOLEAN, default=False)
    credit_limit = Column('credit_limit', DECIMAL(), nullable=False, default=Decimal(0))
    bill_customer = Column('bill_customer', BOOLEAN(), default=False)
    delivery_method = Column("delivery_method", Enum(DeliveryMethod), nullable=False,
                             default=DeliveryMethod.PRINT)
    term = relationship('Term')
    location = relationship('Location')
    tax = relationship('Tax')
    payments:List[CustomerPayment] = relationship('CustomerPayment', cascade="delete", back_populates="customer")
    children = relationship('Customer',
                            back_populates='parent',
                            lazy="joined",
                            post_update=True
                            )
    invoices = relationship('Invoice', back_populates='customer', cascade='delete')
    orders = relationship('Order', back_populates='customer', cascade='delete')

    payment_method = relationship("PaymentMethod")
    e_type = ID_CUSTOMER
    event_id = 0

    @declared_attr
    def parent(cls):
        return relationship('Customer',
                            back_populates='children',
                            lazy="immediate",
                            remote_side=cls.guid,
                            post_update=True
                            )

    def __init__(self, book):
        super().__init__(ID_CUSTOMER, book)
        self.credit_limit = Decimal(0)
        self._id = book.increment_and_format_counter("Customer")
        self.discount = Decimal(0)
        self.delivery_method = DeliveryMethod.PRINT
        self.active = 1
        self.balance = None
        self.bill_customer = False
        Event.gen(self, EVENT_CREATE)

    @reconstructor
    def init_on_load(self):
        from libgasstation.core.session import Session
        book = Session.get_current_book()
        self.balance = None
        self.infant = False
        Instance.__init__(self, ID_CUSTOMER, book)
        Event.gen(self, EVENT_CREATE)

    def refers_to(self, other):
        if isinstance(other, Term):
            return self.term == other
        elif isinstance(other, Account):
            return self.get_receivable_account() == other
        elif isinstance(other, Location):
            return self.location == other
        elif isinstance(other, Tax):
            return self.tax == other
        elif isinstance(other, PaymentMethod):
            return self.payment_method == other
        return False

    def get_upapplied_payments(self):
        invoice_balance = sum([invoice.get_total() for invoice in self.invoices])
        payments = sum(map(lambda a:a.amount, set(cpayment.payment for cpayment in self.payments)))
        balance = invoice_balance - payments
        for payment in self.payments:
            if payment.invoice is None:
                yield payment.payment, balance

    def get_receivable_account(self):
        book = self.book
        return Scrub.get_or_make_account_with_fullname_parent(book.get_root_account(),
                                                              book.default_currency,
                                                              "Accounts Receivable",
                                                              AccountType.RECEIVABLE,
                                                              "Assets", "Current Assets")

    def get_opening_balance(self, *args):
        return super().get_opening_balance(AccountType.RECEIVABLE)

    def set_opening_balance(self, balance: Decimal, opening_date: datetime.date):
        receivable_account = self.get_receivable_account()
        txn: Transaction = self.opening_transaction
        self.begin_edit()
        if txn is not None:
            txn.clear_readonly()
            txn.destroy()

        if not balance.is_zero():
            self.opening_transaction = Scrub.create_opening_balance(receivable_account, balance, opening_date, self.book,
                                                                "Customer--{}".format(self.contact.get_full_name()),
                                                                self.location, equity_type=EquityType.RETAINED_EARNINGS, ttype=TransactionType.INVOICE)
        self.set_dirty()
        self.commit_edit()

    def get_balance_in_currency(self, report_currency):
        book = self.get_book()
        person_currency = self.get_currency()
        cached_balance = self.get_cached_balance()
        if cached_balance is not None:
            balance = cached_balance
        else:
            balance = self.get_opening_balance()
            invoice_balance = sum([invoice.get_total() for invoice in self.invoices])
            payments = sum(map(lambda a:a.amount, set(cpayment.payment for cpayment in self.payments)))
            balance += Decimal(invoice_balance - payments)
            self.set_cached_balance(balance)
        pdb = PriceDB.get_db(book)
        if report_currency is not None:
            balance = pdb.convert_balance_latest_price(balance, person_currency, report_currency)
        return balance

    def add_payment(self, payment: Payment, invoice=None, invoice_amount=Decimal(0)):
        if payment is not None:
            self.begin_edit()
            sup_payment = CustomerPayment(payment)
            if invoice is not None:
                sup_payment.invoice = invoice
                sup_payment.amount = invoice_amount
            self.payments.append(sup_payment)
            self.set_dirty()
            self.commit_edit()

    @classmethod
    def lookup(cls, book, guid):
        coll = book.get_collection(ID_CUSTOMER)
        return coll.get(guid)

    def get_location(self):
        return self.location

    def set_location(self, location):
        if location != self.location:
            self.begin_edit()
            self.location = location
            self.set_dirty()
            self.commit_edit()

    def get_payment_method(self):
        return self.payment_method

    def set_payment_method(self, payment_method):
        if payment_method != self.payment_method:
            self.begin_edit()
            self.payment_method = payment_method
            self.mark()
            self.commit_edit()

    def get_billable(self):
        return self.bill_customer

    def set_billable(self, bill_customer):
        if bill_customer != self.bill_customer:
            self.begin_edit()
            self.bill_customer = bill_customer
            self.mark()
            self.commit_edit()

    def get_delivery_method(self):
        return self.delivery_method

    def set_delivery_method(self, delivery_method: DeliveryMethod):
        if delivery_method != self.delivery_method and isinstance(delivery_method, DeliveryMethod):
            self.begin_edit()
            self.delivery_method = delivery_method
            self.mark()
            self.commit_edit()

    def get_credit_limit(self) -> Decimal:
        return self.credit_limit

    def set_credit_limit(self, limit: Decimal):
        if limit != self.credit_limit:
            self.begin_edit()
            self.credit_limit = limit
            self.mark()
            self.commit_edit()

    def get_tax_included(self):
        return self.tax_included

    def set_tax_included(self, tax_included):
        if tax_included != self.tax_included:
            self.begin_edit()
            self.tax_included = tax_included
            self.mark()
            self.commit_edit()

    def get_tax(self):
        return self.tax

    def set_tax(self, tax):
        if tax != self.tax:
            self.begin_edit()
            self.tax = tax
            self.mark()
            self.commit_edit()

    def get_tin(self):
        return self.tin

    def set_tin(self, tin):
        if tin != self.tin:
            self.begin_edit()
            self.tin = tin
            self.mark()
            self.commit_edit()

    def get_jobs(self):
        return self.jobs

    def add_job(self, job):
        self.jobs.append(job)
        Event.gen(self, EVENT_MODIFY, job)

    def referred(self, other):
        if not isinstance(other, (Term, Account, Invoice)):
            return []
        return self.get_referrers_from_collection(self.get_collection(), other)

    def get_parent(self):
        return self.parent

    def set_parent(self, p):
        if p is not None and isinstance(p, Customer) and p != self:
            p.append_child(self)

    def remove_child(self, child):
        if child is None:
            return
        if child.get_parent() != self or child not in self.children:
            return
        ed = EventData(self, self.children.index(child))
        Event.gen(child, EVENT_REMOVE, ed)
        self.children.remove(child)
        child.parent = None
        Event.gen(self, EVENT_MODIFY)

    @classmethod
    def lookup_display_name(cls, book, display_name):
        col = book.get_collection(ID_CUSTOMER)
        for cat in col.values():
            if cat.get_contact().get_display_name().lower().replace(" ", "") == display_name.lower().replace(" ", ""):
                return cat

    @classmethod
    def lookup_name(cls, book, display_name):
        col = book.get_collection(ID_CUSTOMER)
        for cat in col.values():
            if cat.get_contact().get_full_name().lower().replace(" ", "") == display_name.lower().replace(" ", ""):
                return cat

    def append_child(self, acc):
        if acc is not None:
            old_parent = acc.get_parent()
            if self == old_parent:
                return
            acc.begin_edit()
            if old_parent is not None:
                old_parent.remove_child(acc)
                if not self.books_equal(old_parent):
                    Event.gen(acc, EVENT_DESTROY)
                    col = self.get_book().get_collection(ID_CUSTOMER)
                    col.insert_entity(acc)
                    Event.gen(acc, EVENT_CREATE)
            self.children.append(acc)
            self.set_dirty()
            acc.set_dirty()
            Event.gen(acc, EVENT_ADD)
            acc.commit_edit()

    @classmethod
    def register(cls):
        params = [
            Param(CUSTOMER_ID, TYPE_STRING, cls.get_id),
            Param(CUSTOMER_NOTES, TYPE_STRING, cls.get_notes, cls.set_notes),
            Param(
                CUSTOMER_DELIVERY_METHOD, TYPE_ENUM, cls.get_delivery_method,
                cls.set_delivery_method
            ),
            Param(
                CUSTOMER_CREDIT, TYPE_NUMERIC, cls.get_credit_limit,
                cls.set_credit_limit
            ),

            Param(CUSTOMER_TERMS, ID_TERM, cls.get_term, cls.set_term),
            Param(PARAM_ACTIVE, TYPE_BOOLEAN, cls.get_active, cls.set_active),
            Param(PARAM_BOOK, ID_BOOK, cls.get_book, None),
            Param(PARAM_GUID, TYPE_GUID, cls.get_guid, None)]
        ObjectClass.register(cls, ID_CUSTOMER, cls.__eq__, params)

    def __repr__(self):
        return u"Customer<{}>".format(self.contact.get_display_name() if self.contact is not None else "")
