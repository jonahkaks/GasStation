class TransactionTemplate:
    def __init__(self):
        self.description = None
        self.num = None
        self.notes = None
        self.common_currency = None
        self.location = None
        self.splits = []

    def get_description(self):
        return self.description

    def set_description(self, desc):
        self.description = desc

    def get_location(self):
        return self.location

    def set_location(self, location):
        self.location = location

    def get_num(self):
        return self.num

    def set_num(self, num):
        self.num = num

    def get_notes(self):
        return self.notes

    def set_notes(self, notes):
        self.notes = notes

    def set_currency(self, curr):
        self.common_currency = curr

    def get_currency(self):
        return self.common_currency

    def set_template_splits(self, splits):
        self.splits = splits

    def append_template_split(self, split_i):
        self.splits.append(split_i)

    def get_template_splits(self):
        return self.splits


class SplitTemplate:
    def __init__(self):
        self.action = None
        self.memo = None
        self.credit_formula = None
        self.debit_formula = None
        self.acc = None

    def set_action(self, action):
        self.action = action

    def get_action(self):
        return self.action

    def set_memo(self, memo):
        self.memo = memo

    def get_memo(self):
        return self.memo

    def set_credit_formula_numeric(self, credit):
        self.credit_formula = str(credit)
        if self.debit_formula:
            self.debit_formula = None

    def set_credit_formula(self, credit_formula):
        self.credit_formula = credit_formula
        if self.debit_formula:
            self.debit_formula = None

    def get_credit_formula(self):
        return self.credit_formula

    def get_debit_formula(self):
        return self.debit_formula

    def set_debit_formula_numeric(self, debit):
        self.debit_formula = str(debit)

        if self.credit_formula:
            self.credit_formula = None

    def set_debit_formula(self, debit_formula):
        self.debit_formula = debit_formula

        if self.credit_formula:
            self.credit_formula = None

    def set_account(self, acc):
        self.acc = acc

    def get_account(self):
        return self.acc
