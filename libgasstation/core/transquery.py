from .transaction import *


class QueryTransMatch(IntEnum):
    ALL = 1
    ANY = 2


class ClearedMatch(IntEnum):
    NONE = 0x0000
    NO = 0x0001
    CLEARED = 0x0002
    RECONCILED = 0x0004
    FROZEN = 0x0008
    VOIDED = 0x0010
    ALL = 0x001F


class Query(QueryPrivate):
    def __init__(self):
        super().__init__()

    def get_splits_unique_trans(self):
        trans_hash = {}
        splits = self.run(yielder=True)
        result = []
        for sp in splits:
            trans = sp.transaction
            if trans_hash.get(trans) is None:
                trans_hash[trans] = trans
                result.append(sp)
        return result

    @staticmethod
    def match_all_filter_func(t, num_matches, matches):
        if num_matches == len(t):
            matches.insert(0, t)

    def get_transactions(self, runtype):
        retval = []
        trans_hash = {}
        count = 0
        for sp in self.run():
            trans = sp.get_transaction()
            if runtype == QueryTransMatch.ALL:
                count = trans_hash.get(trans, 0)
            trans_hash[trans] = count + 1
        if runtype == QueryTransMatch.ALL:
            for trans, count in trans_hash.items():
                if len(trans) == count:
                    retval.append(trans)
        else:
            retval = list(trans_hash.keys())
        return retval

    @staticmethod
    def match_all_lot_filter_func(t, num_matches, matches):
        if num_matches == t.count_splits():
            matches.insert(0, t)

    @staticmethod
    def match_any_lot_filter_func(t, num_matches, matches):
        matches.insert(0, t)

    def get_lots(self, runtype):
        retval = []
        trans_hash = {}
        count = 0
        for sp in self.run():
            trans = sp.get_lot()
            if runtype == QueryTransMatch.ALL:
                count = trans_hash.get(trans, 0)
            trans_hash[trans] = count + 1

        if runtype == QueryTransMatch.ALL:
            for k in trans_hash.items():
                self.match_all_lot_filter_func(*k, retval)
        else:
            for k in trans_hash.items():
                self.match_any_lot_filter_func(*k, retval)
        return retval

    def add_account_match(self, accounts, how, op):
        return self.add_account_guid_match([acc.get_guid() for acc in accounts], how, op)

    def add_account_guid_match(self, guid_list, how, op):
        if guid_list is None or len(guid_list) == 0 and how != GuidMatch.NULL:
            return
        param_list = []
        pred_data = QueryCore.guid_predicate(how, guid_list)
        if pred_data is None:
            return
        if how == GuidMatch.ANY or how == GuidMatch.NONE:
            param_list = [SPLIT_ACCOUNT, PARAM_GUID]

        elif how == GuidMatch.ALL:
            param_list = [SPLIT_TRANS, TRANS_SPLITLIST, SPLIT_ACCOUNT_GUID]
        self.add_term(param_list, pred_data, op)

    def add_single_account_match(self, acc, op):
        if acc is None: return
        guid = acc.get_guid()
        if guid is None: return
        self.add_account_guid_match([guid], GuidMatch.ANY, op)

    def add_string_match(self, matchstring, case_sens, use_regexp, how, op, *args):
        pred_data = QueryCore.string_predicate(how, matchstring,
                                               StringMatch.NORMAL if case_sens else StringMatch.CASEINSENSITIVE,
                                               use_regexp)
        if pred_data is None:
            return
        self.add_term(list(args), pred_data, op)

    def add_numeric_match(self, amount, sign, how, op, *args):
        pred_data = QueryCore.numeric_predicate(how, sign, amount)
        if pred_data is None:
            return
        self.add_term(list(args), pred_data, op)

    def add_date_match_tt(self, use_start, stt, use_end, ett, op):
        if not use_start and not use_end:
            return
        tmp_q = self.create()
        if use_start and stt is not None:
            pred_data = QueryCore.date_predicate(QueryCompare.GTE, DateMatch.NORMAL, stt)
            if pred_data is None:
                tmp_q.destroy()
                return
            param_list = [SPLIT_TRANS, TRANS_DATE_POSTED]
            tmp_q.add_term(param_list, pred_data, QueryOp.AND)
        if use_end and ett is not None:
            pred_data = QueryCore.date_predicate(QueryCompare.LTE, DateMatch.NORMAL, ett)
            if pred_data is None:
                tmp_q.destroy()
                return
            param_list = [SPLIT_TRANS, TRANS_DATE_POSTED]
            tmp_q.add_term(param_list, pred_data, QueryOp.AND)
        self.merge_in_place(tmp_q, op)
        tmp_q.destroy()

    def get_date_match_tt(self):
        stt = 0
        ett = 0
        param_list = [SPLIT_TRANS, TRANS_DATE_POSTED]
        terms = self.get_term_type(param_list)
        for term_data in terms:
            if term_data.pd.how == QueryCompare.GTE:
                stt = term_data.date
            if term_data.pd.how == QueryCompare.LTE:
                ett = term_data.date
        return stt, ett

    def add_cleared_match(self, how, op):
        enums = []
        if how & ClearedMatch.CLEARED:
            enums.append(SplitState.CLEARED)
        if how & ClearedMatch.RECONCILED:
            enums.append(SplitState.RECONCILED)
        if how & ClearedMatch.FROZEN:
            enums.append(SplitState.FROZEN)
        if how & ClearedMatch.NO:
            enums.append(SplitState.UNRECONCILED)
        if how & ClearedMatch.VOIDED:
            enums.append(SplitState.VOIDED)
        pred_data = QueryCore.enum_predicate(EnumMatch.ANY, enums)
        if pred_data is None:
            return
        param_list = [SPLIT_RECONCILE]
        self.add_term(param_list, pred_data, op)

    def add_guid_match(self, guid, id_type, op):
        if guid is None or id_type is None:
            return
        if id_type == ID_SPLIT:
            param_list = [PARAM_GUID]
        elif id_type == ID_TRANS:
            param_list = [SPLIT_TRANS, PARAM_GUID]
        elif id_type == ID_ACCOUNT:
            param_list = [SPLIT_ACCOUNT, PARAM_GUID]
        else:
            logging.error("Invalid match type: %s" % id_type)
            raise TypeError("Invalid match type: %s" % id_type)
        super().add_guid_match(param_list, guid, op)

    def add_closing_trans_match(self, value, op):
        param_list = [SPLIT_TRANS, TRANS_IS_CLOSING]
        self.add_boolean_match(param_list, value, op)

    def get_earliest_date_found(self):
        slist = self.last_run()
        if slist is None or len(slist) == 0:
            return 0
        return min(map(
            lambda sp: sp.transaction.get_post_date() if sp.transaction is not None else datetime.date.today(),
            slist))

    def get_latest_date_found(self):
        slist = self.last_run()
        if slist is None or len(slist) == 0:
            return 0
        return max(map(
            lambda sp: sp.transaction.get_post_date() if sp.transaction is not None else datetime.date.today(),
            slist))

    def add_description_match(self, m, c, r, h, o):
        self.add_string_match(m, c, r, h, o, SPLIT_TRANS, TRANS_DESCRIPTION, None)

    def add_notes_match(self, m, c, r, h, o):
        self.add_string_match(m, c, r, h, o, SPLIT_TRANS, TRANS_NOTES, None)

    def add_number_match(self, m, c, r, h, o):
        self.add_string_match(m, c, r, h, o, SPLIT_TRANS, TRANS_NUM, None)

    def add_action_match(self, m, c, r, h, o):
        self.add_string_match(m, c, r, h, o, SPLIT_ACTION, None)

    def add_memo_match(self, m, c, r, h, o):
        self.add_string_match(m, c, r, h, o, SPLIT_MEMO, None)

    def add_value_match(self, amt, sgn, how, op):
        self.add_numeric_match(amt, sgn, how, op, SPLIT_VALUE, None)

    def add_share_price_match(self, amt, how, op):
        self.add_numeric_match(amt, NumericMatch.ANY, how, op, SPLIT_SHARE_PRICE, None)

    def add_shares_match(self, amt, how, op):
        self.add_numeric_match(amt, NumericMatch.ANY, how, op, SPLIT_AMOUNT, None)

    def add_balance_match(self, bal, op):
        self.add_numeric_match(Numeric.zero(), NumericMatch.ANY,
                               QueryCompare.EQUAL if bal else QueryCompare.NEQ, op,
                               SPLIT_TRANS, TRANS_IMBALANCE, None)
