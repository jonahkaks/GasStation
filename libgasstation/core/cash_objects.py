class CashObjects:
    @classmethod
    def register(cls):
        from .term import Term
        from .transaction import Transaction, Split
        from .scheduled_transaction import ScheduledTransaction
        from .price_db import PriceDB
        from .lot import Lot
        from .account import Account
        from .scheduled_transaction_list import ScheduledTransactionList
        from .commodity import CommodityTable
        from .tax import Tax
        CommodityTable.register()
        Account.register()
        Transaction.register()
        Split.register()
        ScheduledTransaction.register()
        ScheduledTransactionList.register()
        PriceDB.register()
        Lot.register()
        Tax.register()

        from .fuel import Fuel
        from .dip import Dip
        from .nozzle import Nozzle
        from .tank import Tank
        from .pump import Pump
        from .address import Address
        from .location import Location
        from .product import ProductCategory, Product, UnitOfMeasure, ProductLocation
        from .invoice import Invoice
        from .bill import Bill
        from .inventory import Inventory
        from .entry import InvoiceEntry, BillEntry, BillAccountEntry
        from .customer import Customer
        from .supplier import Supplier
        from .staff import Staff
        Fuel.register()
        Dip.register()
        Tank.register()
        Pump.register()
        Nozzle.register()
        ProductCategory.register()
        Product.register()
        UnitOfMeasure.register()
        ProductLocation.register()
        Inventory.register()
        Term.register()
        Address.register()
        Customer.register()
        Staff.register()
        Supplier.register()
        Location.register()
        Invoice.register()
        InvoiceEntry.register()
        Bill.register()
        BillEntry.register()
        BillAccountEntry.register()


