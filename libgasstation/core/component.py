import weakref
from collections import defaultdict

from ._declbase import ID_TRANS
from .event import *


class EntityTypeEventInfo:
    def __init__(self):
        self.entity_type = None
        self.event_mask = None


class ComponentEventInfo:
    def __init__(self):
        self.event_masks = {}
        self.entity_events = {}
        self.match = False


class ComponentInfo:
    def __init__(self, _id=None, klass=None):
        self.refresh_handler = None
        self.close_handler = None
        self.user_data = None
        self.watch_info = ComponentEventInfo()
        self.component_class = klass
        self.component_id = _id
        self.session = None

    def __del__(self):
        self.refresh_handler = None
        self.close_handler = None
        self.watch_info = None
        self.session = None


NO_COMPONENT = -1


class EventInfo:
    def __init__(self):
        self.event_mask = None


def match_type_helper(id_type, et, cei):
    et_2 = cei.event_masks.get(id_type)
    if et_2 is None:
        return False
    if et & et_2:
        cei.match = True


def match_helper(guid, ei_1, cei):
    ei_2 = cei.entity_events.get(guid)
    if ei_2 is None:
        return
    if ei_1.event_mask & ei_2.event_mask:
        cei.match = True


class ComponentManager:
    suspend_counter = 0
    changes = None
    changes_backup = None
    handler_id = 0
    next_component_id = 0
    components = None

    @staticmethod
    def clear_event_info(cei):
        if cei is None: return
        cei.event_masks.clear()
        cei.entity_events.clear()

    @staticmethod
    def add_event(cei, entity, event_mask, or_in):
        if not cei or cei.entity_events is None or not entity:
            return
        if event_mask == 0:
            if or_in:
                return
            if cei.entity_events.get(entity):
                cei.entity_events.pop(entity)
        else:
            ei = cei.entity_events.get(entity)
            if ei is None:
                ei = EventInfo()
                ei.event_mask = 0
                cei.entity_events[entity] = ei
            if or_in:
                ei.event_mask |= event_mask
            else:
                ei.event_mask = event_mask

    @staticmethod
    def add_event_type(cei, entity_type, event_mask, or_in):
        mask = cei.event_masks.get(entity_type)

        if mask is None:
            mask = EVENT_NONE
        if or_in:
            mask |= event_mask
        else:
            mask = event_mask
        cei.event_masks[entity_type] = mask

    @classmethod
    def event_handler(cls, entity, event_type, *args):
        from .transaction import Split
        try:
            guid = entity.guid
        except AttributeError:
            return
        cls.add_event(cls.changes, guid, event_type, True)
        if isinstance(entity, Split):
            cls.add_event_type(cls.changes, ID_TRANS, EVENT_MODIFY, True)
        else:
            cls.add_event_type(cls.changes, entity.__class__.__name__, event_type, True)
        cls.got_events = True
        if cls.suspend_counter == 0:
            cls.refresh_internal(False)

    @classmethod
    def init(cls):
        cls.suspend_counter = 0
        cls.next_component_id = 1
        cls.components = set()
        cls.handler_id = 0
        cls.changes = ComponentEventInfo()
        cls.changes_backup = ComponentEventInfo()
        cls.got_events = False
        cls.handler_id = Event.register_handler(cls.event_handler, None)

    @classmethod
    def shutdown(cls):
        if not cls.changes.entity_events:
            return
        cls.changes.event_masks = None
        cls.changes.entity_events = None
        cls.changes_backup.event_masks = None
        cls.changes_backup.entity_events = None
        Event.unregister_handler(cls.handler_id)

    @classmethod
    def find(cls, component_id):
        for ci in cls.components:
            if ci.component_id == component_id:
                return ci
        return None

    @classmethod
    def find_components_by_data(cls, *user_data):
        return list(filter(lambda ci: ci.user_data == user_data, cls.components))

    @classmethod
    def find_by_session(cls, session):
        return list(filter(lambda ci: ci.session is not None and ci.session() == session, cls.components))

    @classmethod
    def register_internal(cls, component_class):
        if component_class is None:
            return
        component_id = cls.next_component_id
        while cls.find(component_id):
            if component_id + 1 == NO_COMPONENT:
                component_id += 1
        ci = ComponentInfo(_id=component_id, klass=component_class)
        cls.components.add(ci)
        cls.next_component_id = component_id + 1
        return ci

    @classmethod
    def register(cls, component_class, refresh_handler, close_handler, *user_data):
        if not component_class:
            return NO_COMPONENT
        ci = cls.register_internal(component_class)
        if refresh_handler is not None:
            ci.refresh_handler = weakref.WeakMethod(refresh_handler)
        if close_handler is not None:
            ci.close_handler = weakref.WeakMethod(close_handler)
        ci.user_data = user_data
        return ci.component_id

    @classmethod
    def watch_entity(cls, component_id, entity, event_mask):
        if entity is None:
            return
        ci = cls.find(component_id)
        if ci is None:
            return
        cls.add_event(ci.watch_info, entity, event_mask, False)

    @classmethod
    def watch_entity_type(cls, component_id, entity_type, event_mask):
        ci = cls.find(component_id)
        if ci is None:
            return
        cls.add_event_type(ci.watch_info, entity_type, event_mask, False)

    @staticmethod
    def get_entity_events(chan, entity):
        if chan is None or entity is None:
            return EVENT_NONE
        return chan.get(entity, None)

    @classmethod
    def clear_watches(cls, component_id):
        ci = cls.find(component_id)
        if ci is None:
            return
        cls.clear_event_info(ci.watch_info)

    @classmethod
    def unregister(cls, component_id):
        ci = cls.find(component_id)
        if ci is None:
            return
        cls.clear_watches(component_id)
        cls.components.remove(ci)

    @classmethod
    def unregister_by_data(cls, component_class, user_data):
        for c in filter(lambda ci: ci.component_class == component_class, cls.find_components_by_data(user_data)):
            cls.unregister(c.component_id)

    @classmethod
    def suspend(cls):
        cls.suspend_counter += 1

    @classmethod
    def resume(cls):
        if cls.suspend_counter == 0:
            return
        cls.suspend_counter -= 1
        if cls.suspend_counter == 0:
            cls.refresh_internal(False)

    @staticmethod
    def changes_match(cei, changes):
        if cei is None:
            return False
        cei.match = False
        for k, v in changes.event_masks.items():
            match_type_helper(k, v, cei)
        if cei.match:
            return True
        if len(cei.entity_events) <= len(changes.entity_events):
            small_table = cei.entity_events
            big_cei = changes
        else:
            small_table = changes.entity_events
            big_cei = cei
        big_cei.match = False
        for k, v in small_table.items():
            match_helper(k, v, big_cei)
        return big_cei.match

    @classmethod
    def refresh_internal(cls, force):
        if not cls.got_events and not force:
            return
        cls.suspend()
        table = cls.changes_backup.event_masks
        cls.changes_backup.event_masks = cls.changes.event_masks
        cls.changes.event_masks = table
        table = cls.changes_backup.entity_events
        cls.changes_backup.entity_events = cls.changes.entity_events
        cls.changes.entity_events = table

        for ci in cls.components.copy():
            if ci is None or ci.refresh_handler is None:
                continue
            func = ci.refresh_handler()
            if func is not None:
                if force:
                    func(None, *ci.user_data)
                elif cls.changes_match(ci.watch_info, cls.changes_backup):
                    func(cls.changes_backup.entity_events, *ci.user_data)
        cls.clear_event_info(cls.changes_backup)
        cls.got_events = False
        cls.resume()

    @classmethod
    def refresh_all(cls):
        if cls.suspend_counter != 0:
            return
        cls.refresh_internal(True)

    @classmethod
    def refresh_suspended(cls):
        return cls.suspend_counter != 0

    @classmethod
    def close(cls, component_id):
        ci = cls.find(component_id)
        if ci is not None and ci.close_handler is not None:
            try:
                ci.close_handler()(*ci.user_data)
            except TypeError:
                pass

    @classmethod
    def close_by_data(cls, component_class, user_data):
        for c in filter(lambda ci: ci.component_class == component_class, cls.find_components_by_data(user_data)):
            cls.close(c.component_id)

    @classmethod
    def set_session(cls, component_id, session):
        ci = cls.find(component_id)
        if ci is None:
            return
        if session is not None:
            ci.session = weakref.ref(session)

    @classmethod
    def close_by_session(cls, session):
        for ci in cls.find_by_session(session):
            cls.close(ci.component_id)

    @classmethod
    def find_gui_components(cls, component_class, find_handler, *find_data):
        lis = []
        if component_class is None: return None

        for ci in filter(lambda c: c.component_class == component_class, cls.components):
            if find_handler and not find_handler(find_data, ci.user_data):
                continue
            lis.insert(0, ci.user_data)
        return lis

    @classmethod
    def find_first(cls, component_class, find_handler, *find_data):
        if component_class is None:
            return None
        lis = cls.find_gui_components(component_class, find_handler, *find_data)
        if lis is None or len(lis) == 0:
            return None
        return lis[0]

    @classmethod
    def find_ids_by_class(cls, component_class):
        ids = []
        for ci in cls.components:
            if component_class is not None and component_class != ci.component_class:
                continue
            ids.insert(0, ci.component_id)
        return ids

    @classmethod
    def forall(cls, component_class, handler, *iter_data):
        count = 0
        if handler is None:
            return 0
        for ci in map(cls.find, cls.find_ids_by_class(component_class)):
            if ci is None:
                continue
            if handler(ci.component_class, ci.component_id, *(ci.user_data + iter_data)):
                count += 1
        return count


class Tracking:
    singleton = None

    @classmethod
    def table(cls):
        if cls.singleton is None:
            cls.singleton = defaultdict(weakref.WeakSet)
        return cls.singleton

    @classmethod
    def dump(cls):
        cls.table().clear()

    @classmethod
    def remember(cls, obj):
        if obj is None:
            return
        klass = obj.__class__.__name__
        table = cls.table()
        table[klass].add(obj)

    @classmethod
    def forget_internal(cls, obj):
        if obj is None:
            return
        name = obj.__class__.__name__

        table = cls.table()
        if obj in table[name]:
            table[name].remove(obj)
        return True

    @classmethod
    def forget(cls, obj):
        cls.forget_internal(obj)

    @classmethod
    def get_list(cls, name):
        return list(cls.table().get(name, set()))


ComponentManager.init()
