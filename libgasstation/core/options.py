import locale
import os
from abc import ABC
from collections import defaultdict

from .book import OPTION_NAME_NUM_FIELD_SOURCE, OPTION_NAME_DEFAULT_GAINS_LOSS_ACCT_GUID, \
    OPTION_NAME_DEFAULT_GAINS_POLICY, OPTION_NAME_AUTO_READONLY_DAYS, OPTION_NAME_BOOK_CURRENCY, \
    OPTION_NAME_CURRENCY_ACCOUNTING, OPTION_NAME_TRADING_ACCOUNTS, OPTION_SECTION_ACCOUNTS
from .commodity import CommodityTable, COMMODITY_NAMESPACE_NAME_CURRENCY, Commodity
from .person import *
from .session import Session

option_section_counters = "Counters"
business_label = "Business"
company_name = "Company Name"
company_addy = "Company Address"
company_id = "Company ID"
company_phone = "Company Phone Number"
company_fax = "Company Fax Number"
company_url = "Company Website URL"
company_email = "Company Email Address"
company_contact = "Company Contact Person"
fancy_date_label = "Fancy Date Format"
fancy_date_format = "custom"
tax_label = "Tax"
tax_nr_label = "Tax Number"

option_section_accounts = OPTION_SECTION_ACCOUNTS
option_name_trading_accounts = OPTION_NAME_TRADING_ACCOUNTS
option_name_currency_accounting = OPTION_NAME_CURRENCY_ACCOUNTING
option_name_book_currency = OPTION_NAME_BOOK_CURRENCY
option_name_default_gains_policy = OPTION_NAME_DEFAULT_GAINS_POLICY
option_name_default_gain_loss_account = OPTION_NAME_DEFAULT_GAINS_LOSS_ACCT_GUID
option_name_auto_readonly_days = OPTION_NAME_AUTO_READONLY_DAYS
option_name_num_field_source = OPTION_NAME_NUM_FIELD_SOURCE


def pair(value):
    return isinstance(value, (list, tuple)) and len(value) == 2


class NewOptions:
    def __init__(self, default_section=None):
        self.option_hash = defaultdict(dict)
        self.options_changed = False
        self.changed_hash = {}
        self.callback_hash = {}
        self.last_callback_id = 0
        self.new_names_alist = [{"Accounts to include", False, "Accounts"},
                                {"Exclude transactions between selected accounts?", False,
                                 "Exclude transactions between selected accounts"},
                                {"Filter Accounts", False, "Filter By..."},
                                {"Flatten list to depth limit?", False, "Flatten list to depth limit"},
                                {"From", False, "Start Date"},
                                {"Report Accounts", False, "Accounts"},
                                {"Report Currency", False, "Report's currency"},
                                {"Show Account Code?", False, "Show Account Code"},
                                {"Show Full Account Name?", False, "Show Full Account Name"},
                                {"Show Multi-currency Totals?", False, "Show Multi-currency Totals"},
                                {"Show zero balance items?", False, "Show zero balance items"},
                                {"Sign Reverses?", False, "Sign Reverses"},
                                {"To", False, "End Date"},
                                {"Charge Type", False, "Action"},
                                {"Individual income columns", False, "Individual sales columns"},
                                {"Individual expense columns", False, "Individual purchases columns"},
                                {"Remittance amount", False, "Gross Balance"},
                                {"Net Income", False, "Net Balance"},
                                {"Use Full Account Name?", False, "Use Full Account Name"},
                                {"Use Full Other Account Name?", False, "Use Full Other Account Name"},
                                {"Void Transactions?" "Filter" "Void Transactions"},
                                {"Void Transactions" "Filter" "Void Transactions"},
                                {"Account Substring" "Filter" "Account Name Filter"},
                                {"Individual Taxes", False, "Use Detailed Tax Summary"}]
        self.default_section = default_section

    def get_default_section(self):
        return self.default_section

    def set_default_section(self, section):
        self.default_section = section

    def touch(self):
        self.options_changed = True
        self.run_callbacks()

    def lookup_option(self, section, name):
        section_hash = self.option_hash.get(section)
        if section_hash is not None:
            option = section_hash.get(name)
            if option is not None:
                return option
            # name_match = self.new_names_alist

    def serialize(self):
        a = defaultdict(dict)
        for section, options in self.option_hash.items():
            for name, option in options.items():
                value = option.get_value()
                if value != option.get_default():
                    a[section][name] = option.serialize()
        return a

    def deserialize(self, h):
        for section, options in h.items():
            for name, value in options.items():
                self.option_hash[section][name].deserialize(value)

    def option_changed(self, section, name):
        self.options_changed = True
        section_changed_hash = self.changed_hash.get(section)
        if section_changed_hash is None:
            section_changed_hash = {}
            self.changed_hash[section] = section_changed_hash
        section_changed_hash[name] = True

    def clear_changes(self):
        self.options_changed = False
        self.changed_hash.clear()

    def register_option(self, option):
        name = option.get_name()
        section = option.get_section()
        self.option_hash[section][name] = option
        option.set_changed_cb(lambda v: self.option_changed(section, name))

    def unregister_option(self, option):
        from gasstation.utilities.custom_dialogs import show_error
        section = option.get_section()
        name = option.get_name()
        section_hash = self.option_hash.get(section)
        if section_hash is not None and section_hash.get(name) is not None:
            self.option_hash[section].pop(name)
        else:
            show_error(None, "options:unregister-option: no such option\n")

    def options_foreach(self, callback, *args):
        for h in self.option_hash.values():
            for opt in sorted(h.values()):
                callback(opt, *args)

    def options_foreach_general(self, section_cb, option_cb):
        for k, v in self.option_hash.items():
            if section_cb is not None:
                section_cb(k)
            for opt in v.values():
                option_cb(opt)

    def to_kvp(self, book):
        def save_cb(option):
            value = option.get_value()
            default_value = option.get_default()
            section = option.get_section()
            name = option.get_name()
            if value != default_value and value is not None:
                option.to_kvp(book, [section, name])

        self.options_foreach(save_cb)

    def from_kvp(self, book):
        def load_cb(option):
            section = option.get_section()
            name = option.get_name()
            option.from_kvp(book, [section, name])

        self.options_foreach(load_cb)

    def register_cb(self, section, name, callback, *args):
        if callback is None:
            return
        data = [section, name, callback, args]
        self.callback_hash[self.last_callback_id] = data
        self.last_callback_id += 1
        return self.last_callback_id - 1

    def unregister_cb_id(self, _id):
        if self.callback_hash.get(_id) is not None:
            self.callback_hash.pop(_id)

    def run_callbacks(self):
        if self.options_changed:
            call_copy = self.callback_hash.copy()
            for section, name, callback, args in call_copy.values():
                if section is None:
                    callback()
                else:
                    section_changed_hash = self.changed_hash.get(section)
                    if section_changed_hash is not None:
                        if name is None:
                            callback(*args)
                        elif section_changed_hash.get(name):
                            callback(*args)
        self.clear_changes()

    def send(self, db_handle):
        self.options_foreach(lambda option: db_handle.register_option(option))


class OptionDB:
    option_dbs = []
    kvp_registery = defaultdict(list)

    def __init__(self, options: NewOptions):
        self.options = options
        self.options_dirty = False
        self.handle = 0
        self.get_ui_value = None
        self.set_ui_value = None
        self.set_selectable = None
        options.send(self)
        OptionDB.option_dbs.append(self)
        self.handle = OptionDB.option_dbs.index(self)

    def destroy(self):
        self.options = None
        self.get_ui_value = None
        self.set_ui_value = None
        self.set_selectable = None
        del self

    @classmethod
    def register_kvp_option_generator(cls, id_type, gen):
        cls.kvp_registery[id_type].append(gen)

    def register_option(self, option):
        self.options_dirty = True
        option.set_db(self)

    @classmethod
    def make_kvp_options(cls, id_type, default_section=None):
        lis = cls.kvp_registery.get(id_type, [])
        options = NewOptions(default_section)
        for generator in lis:
            generator(options)
        return options

    @classmethod
    def new_for_type(cls, id_type, default_section=None):
        if not id_type:
            return None
        options = cls.make_kvp_options(id_type, default_section)
        return cls(options)

    def load(self, book):
        if book is None:
            return
        self.options.from_kvp(book)

    def save(self, book, clear_all):
        if book is None:
            return
        if clear_all:
            book.delete_options()
        self.options.to_kvp(book)

    def set_ui_callbacks(self, get_ui_value, set_ui_value, set_selectable):
        self.set_ui_value = set_ui_value
        self.get_ui_value = get_ui_value
        self.set_selectable = set_selectable

    def register_change_callback(self, cb, section, name, *args):
        if self.options is not None:
            return self.options.register_cb(section, name, cb, *args)

    def unregister_change_callback_id(self, _id):
        if self.options is not None:
            self.options.unregister_cb_id(_id)

    def sections(self):
        return self.options.option_hash.keys()

    def section_options(self, section):
        return sorted(self.options.option_hash.get(section).values())

    def get_option_by_name(self, section_name, name):
        return self.options.option_hash[section_name].get(name)

    def get_changed(self):
        for section in self.options.option_hash.values():
            for option in section:
                if option.changed:
                    return True
        return False

    def commit(self):
        changed_something = False
        commit_errors = []
        for section in self.options.option_hash.values():
            for option in section.values():
                if option.get_changed():
                    result = option.commit()
                    if result is not None:
                        commit_errors.append(result)
                    changed_something = True
                    option.set_changed(False)
        if changed_something:
            self.options.run_callbacks()
        return commit_errors

    def reset_widgets(self):
        for section, options in self.options.option_hash.items():
            if section is None or section[:2] == "__":
                continue
            for option in options.values():
                option.set_ui_value(True)
                option.set_changed(True)

    def reset_section_widgets(self, section):
        for option in self.options.option_hash.get(section).values():
            option.set_ui_value(True)
            option.set_changed(True)

    def get_default_section(self):
        return self.options.get_default_section()

    def set_option(self, section, name, value):
        option = self.get_option_by_name(section, name)
        if option is None:
            return False
        try:
            value = option.validator(value)
            option.set_value(value)
        except ValueError:
            return False

    def lookup_option(self, section, name, default_value):
        option = self.get_option_by_name(section, name)
        if option is None:
            return default_value
        return option.get_value()

    def lookup_boolean_option(self, section, name, default_value):
        value = self.lookup_option(section, name, default_value)
        if isinstance(value, bool):
            return value
        else:
            return default_value

    def lookup_string_option(self, section, name, default_value):
        value = self.lookup_option(section, name, default_value)
        if isinstance(value, str):
            return value
        else:
            return default_value

    def lookup_font_option(self, section, name, default_value):
        return self.lookup_string_option(section, name, default_value)

    def clean(self):
        self.options_dirty = False


class CollectionInterface(ABC):
    def __init__(self):
        self._data = None

    def index_get_name(self, index):
        raise NotImplementedError

    def index_get_description(self, index):
        raise NotImplementedError

    def index_get_value(self, index):
        raise NotImplementedError

    def value_get_index(self, value):
        try:
            return self._data.index(value)
        except ValueError:
            return -1

    def num_values(self):
        raise NotImplementedError


class KvpInterface:
    def to_kvp(self, book, path):
        raise NotImplementedError

    def from_kvp(self, book, path):
        raise NotImplementedError


class Option:
    def __init__(self, section, name, sort_tag, _type, doc, default,
                 data=None, strings_cb=None, changed_cb=None):
        self._type = _type
        self._changed = False
        self._widget = None
        self._odb = None
        self._section = section
        self._name = name
        self._sort_tag = sort_tag
        self._doc = doc
        self._value = default
        self._data = data
        self._strings_cb = strings_cb
        self._changed_cb = changed_cb
        self._default = default

    def widget_changed(self):
        if self._changed_cb is not None:
            value = self.get_ui_value()
            self._changed_cb(value)

    def validator(self, value):
        raise NotImplementedError("Option subclasses of %s must first implent validator method " % self._type)

    def serialize(self):
        return self.get_value()

    def deserialize(self, value):
        self.set_value(value)

    def get_type(self):
        return self._type

    def get_sort_tag(self):
        return self._sort_tag

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_section(self):
        return self._section

    def set_section(self, sec):
        self._section = sec

    def get_documentation(self):
        return self._doc

    def set_documentation(self, doc):
        self._doc = doc

    def set_value(self, value):
        self._value = value
        if self._changed_cb is not None:
            self._changed_cb(value)

    def get_value(self):
        return self._value

    def set_default(self, value):
        self._default = value
        self.set_value(value)

    def get_default(self):
        return self._default

    def set_changed(self, changed):
        self._changed = changed

    def get_changed(self):
        return self._changed

    def get_widget(self):
        return self._widget

    def set_widget(self, w):
        self._widget = w

    def set_db(self, db):
        self._odb = db

    def get_ui_value(self):
        if self._odb is None:
            return
        if self._odb.get_ui_value is not None:
            return self._odb.get_ui_value(self)

    def set_ui_value(self, use_default):
        if self._odb is None:
            return
        if self._odb.set_ui_value is not None:
            return self._odb.set_ui_value(self, use_default)

    def set_selectable(self, selectable):
        if self._odb is None:
            return
        if self._odb.set_selectable is not None:
            self._odb.set_selectable(self, selectable)

    def set_changed_cb(self, cb):
        self._changed_cb = cb

    def commit(self):
        value = self.get_ui_value()
        try:
            self.set_value(value)
            self.set_ui_value(False)
        except ValueError as e:
            return e.args[0]

    def __str__(self):
        return self._value

    def __repr__(self):
        return u"Option<{}>".format(self._name)

    def __lt__(self, other):
        return self._sort_tag < other._sort_tag


class StringOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, "string", doc, default)

    def to_kvp(self, book, path):
        book.set_option(self._value, path)

    def from_kvp(self, book, path):
        v = book.get_option(path)
        if v is not None and isinstance(v, str):
            self.set_default(v)

    def validator(self, value):
        if not isinstance(value, str):
            raise ValueError("Expected string value got %s for option %s" % (type(value), self.get_name()))
        return value


class TextOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, "text", doc, default)

    def validator(self, value):
        if not isinstance(value, str):
            raise ValueError("Expected string value got %s for option %s" % (type(value), self.get_name()))
        return value

    def to_kvp(self, book, path):
        book.set_option(self._value, path)

    def from_kvp(self, book, path):
        v = book.get_option(path)
        if v is not None and isinstance(v, str):
            self.set_default(v)


class FontOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, "font", doc, default)

    def validator(self, value):
        if not isinstance(value, str):
            raise ValueError("Expected string value got %s for option %s" % (type(value), self.get_name()))
        return value

    def to_kvp(self, book, path):
        book.set_option(self._value, path)

    def from_kvp(self, book, path):
        v = book.get_option(path)
        if v is not None and isinstance(v, str):
            self.set_default(v)


class CurrencyOption(Option):
    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, "currency", doc, self.currency_to(default))

    def serialize(self):
        return self._value

    @staticmethod
    def currency_to(currency):
        if isinstance(currency, str):
            return currency
        else:
            return currency.get_mnemonic()

    def validator(self, value):
        if not isinstance(value, Commodity):
            raise ValueError("Expected a commodity value for option %s" % self._name)
        return value

    def set_value(self, value):
        self._value = self.currency_to(value)
        if self._changed_cb is not None:
            self._changed_cb(value)

    def get_value(self):
        from libgasstation.core.commodity import CommodityTable, COMMODITY_NAMESPACE_NAME_CURRENCY
        from libgasstation.core.session import Session
        value = self._value
        if isinstance(value, str):
            return CommodityTable.get_table(Session.get_current_book()).lookup(COMMODITY_NAMESPACE_NAME_CURRENCY, value)
        return value

    def get_default(self):
        from libgasstation.core.commodity import CommodityTable, COMMODITY_NAMESPACE_NAME_CURRENCY
        from libgasstation.core.session import Session
        value = self._default
        if isinstance(value, str):
            return CommodityTable.get_table(Session.get_current_book()).lookup(COMMODITY_NAMESPACE_NAME_CURRENCY, value)
        return value


class BudgetOption(Option):
    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, doc, "budget", default)

    def validator(self, value):
        if not isinstance(value, str):
            raise ValueError("Expected string value got %s for option %s" % (type(value), self.get_name()))
        return value


class CommodityOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, "commodity", doc, self.commodity_to(default))

    def serialize(self):
        return self._value

    def deserialize(self, value):
        self._value = value

    @staticmethod
    def commodity_to(commodity):
        if isinstance(commodity, str):
            return [COMMODITY_NAMESPACE_NAME_CURRENCY, commodity]
        else:
            return [commodity.get_namespace(), commodity.get_mnemonic()]

    def set_value(self, commodity):
        self._value = self.commodity_to(commodity)
        if self._changed_cb is not None:
            self._changed_cb(commodity)

    def validator(self, value):
        if not isinstance(value, Commodity):
            raise ValueError("Expected a commodity value for option %s" % self._name)
        return value

    def get_value(self):
        name_space, comm = self._value
        return CommodityTable.get_table(Session.get_current_book()).lookup(name_space, comm)

    def get_default(self):
        name_space, comm = self._default
        return CommodityTable.get_table(Session.get_current_book()).lookup(name_space, comm)

    def to_kvp(self, book, path):
        book.set_option(self._value[0], path + ['("ns")'])
        book.set_option(self._value[1], path + ['("monic")'])

    def from_kvp(self, book, path):
        ns = book.get_option(path + ['("ns")'])
        monic = book.get_option(path + ['("monic")'])
        if ns and isinstance(ns, str) and monic and isinstance(monic, str):
            self.set_default([ns, monic])


class ComplexBooleanOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, default, setter_function_called_cb=None):
        super().__init__(section, name, sort_tag, "boolean", doc, default)
        self.setter_function_called_cb = setter_function_called_cb

    def validator(self, value):
        if not isinstance(value, bool):
            raise ValueError("Expected boolean value got %s for option %s" % (type(value), self.get_name()))
        return value

    def set_value(self, value):
        self._value = value
        if self.setter_function_called_cb is not None:
            self.setter_function_called_cb(value)
        if self._changed_cb is not None:
            self._changed_cb(value)

    def to_kvp(self, book, path):
        book.set_option(self._value, path)

    def from_kvp(self, book, path):
        v = book.get_option(path)
        if v is not None and isinstance(v, (bool, int)):
            self.set_default(bool(v))


class SimpleBooleanOption(ComplexBooleanOption):
    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, doc, default)


class PixMapOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, "pixmap", doc, default)

    def validator(self, value):
        if value is not None and not isinstance(value, str):
            raise ValueError("Expected string value got %s for option %s" % (type(value), self.get_name()))
        return value

    def to_kvp(self, book, path):
        book.set_option(self._value, path)

    def from_kvp(self, book, path):
        v = book.get_option(path)
        if v is not None and isinstance(v, str):
            self.set_default(v)


class DateOption(Option, CollectionInterface, KvpInterface):

    def __init__(self, section, name, sort_tag, doc, default,
                 show_time, subtype, relative_date_list):
        super().__init__(section, name, sort_tag, "date", doc, self.maybe_convert_to_time64(default),
                         data=relative_date_list)
        self.subtype = subtype
        self.show_time = show_time

    def validator(self, value):
        if not isinstance(value, (list, tuple)):
            raise ValueError("Expected a list value for option %s" % self._name)
        return value

    def num_values(self):
        return len(self._data)

    def index_get_value(self, index):
        return self._data[index]

    def index_get_name(self, index):
        from gasstation.utilities.date import RelativeDate
        return RelativeDate.get_date_string(self._data[index])

    def index_get_description(self, index):
        from gasstation.utilities.date import RelativeDate
        return RelativeDate.get_date_desc(self._data[index])

    def get_subtype(self):
        return self.subtype

    @staticmethod
    def value_get_type(value):
        return value[0]

    def get_show_time(self):
        return self.show_time

    def get_relative_date_list(self):
        return self._data

    @staticmethod
    def absolute_time(value):
        from gasstation.utilities.date import RelativeDate
        if value is None:
            return datetime.date.today()
        if value[0] == "absolute":
            return value[1]
        return RelativeDate.get_absolute(value[1])

    @staticmethod
    def relative_time(value):
        if value[0] == "absolute":
            return
        return value[1]

    def to_kvp(self, book, path):
        v = self._value
        book.set_option(v[0], path + ['("type")'])
        book.set_option(v[1], path + ['("value")'])

    def from_kvp(self, book, path):
        t = book.get_option(path + ['("type")'])
        v = book.get_option(path + ['("value")'])
        if isinstance(t, str) and isinstance(v, str):
            self.set_default([t, v])

    @staticmethod
    def date_legal(date):
        return pair(date) and ((date[0] == "relative" and isinstance(date[1], str))
                               or (date[0] == "absolute" and isinstance(date[1], (int, float))))

    @staticmethod
    def maybe_convert_to_time64(date):
        if pair(date[1]):
            return date[0], date[1][0]
        else:
            return date

    def set_value(self, value):
        from gasstation.utilities.custom_dialogs import show_error
        if self.date_legal(value):
            self._value = self.maybe_convert_to_time64(value)
        else:
            show_error(None, "Illegal date option")


class AccountListLimitedOption(Option, ABC):

    def __init__(self, section, name, sort_tag, doc, default, multiple_selection, account_type_list):
        super().__init__(section, name, sort_tag, "account-list", doc, default)
        self.multiple = multiple_selection
        self.acc_type_list = account_type_list

    def multiple_selection(self):
        return self.multiple

    def get_account_type_list(self):
        return self.acc_type_list


class AccountListOption(AccountListLimitedOption):
    def validator(self, value):
        pass

    def __init__(self, section, name, sort_tag, doc, default_getter, validator, multiple_section):
        super().__init__(section, name, sort_tag, doc, default_getter(), multiple_section, [])
        self.get_default = default_getter
        self.validator = validator

    def serialize(self):
        return list(map(lambda a: a.get_full_name(), self._value))

    def deserialize(self, value):
        cb = Session.get_current_root().lookup_by_full_name
        self._value = list(map(lambda a: cb(a), value))


class AccountSelLimitedOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, acct_type_list):
        super().__init__(section, name, sort_tag, "account-sel", doc, None)
        self.acct_type_list = acct_type_list
        self.set = False

    @staticmethod
    def convert_to_guid(item):
        if isinstance(item, UUID):
            return item
        return item.get_guid()

    @staticmethod
    def convert_to_account(item):
        if isinstance(item, UUID):
            return Account.lookup(item, Session.get_current_book())
        return item

    def get_default(self):
        current_root = Session.get_current_root()
        account_list = current_root.get_descendants_sorted()
        if self.acct_type_list is None:
            return
        for acc in account_list:
            if acc.get_type() in self.acct_type_list:
                return acc

    def validator(self, value):
        return value

    def get_value(self):
        return self.convert_to_account(self._value if self.set else self.get_default())

    def set_value(self, value):
        if value is None:
            value = self.get_default()
        value = self.convert_to_account(value)
        self._value = self.convert_to_guid(value)
        self.set = True

    def to_kvp(self, book, path):
        book.set_option(self._value, path)

    def from_kvp(self, book, path):
        v = book.get_option(path)
        if v is not None and isinstance(v, str):
            self.set_default(v)

    def get_account_type_list(self):
        return self.acct_type_list

    def deserialize(self, value):
        self.set_value(uuid.UUID(value))

    def serialize(self):
        return str(self._value)

class AccountSelOption(AccountSelLimitedOption):
    def __init__(self, section, name, sort_tag, doc):
        super().__init__(section, name, sort_tag, doc, None)


class MultiChoiceCallBackOption(Option, CollectionInterface):
    def __init__(self, section, name, sort_tag, doc, default, ok_values, setter_function_called, option_widget_changed):
        super().__init__(section, name, sort_tag, "multichoice", doc, default, data=ok_values,
                         changed_cb=option_widget_changed)
        self.setter_function_cb = setter_function_called

    def index_get_value(self, index):
        return self._data[index][0]

    def index_get_name(self, index):
        return self._data[index][1] if len(self._data[index]) > 2 else self._data[index][0]

    def index_get_description(self, index):
        return self._data[index][2] if len(self._data[index]) > 2 else self._data[index][1]

    def num_values(self):
        return len(self._data)

    def value_get_index(self, value):
        for x, v in enumerate(self._data):
            if v[0] == value:
                return x
        return -1

    def validator(self, value):
        return value

    def set_value(self, value):
        super().set_value(value)
        if self.setter_function_cb is not None:
            self.setter_function_cb(value)


class MultiChoiceOption(MultiChoiceCallBackOption):
    def __init__(self, section, name, sort_tag, doc, default, ok_values):
        super().__init__(section, name, sort_tag, doc, default, ok_values, None, None)


class RadioOption(Option):
    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, "radiobutton", doc, default)

    def validator(self, value):
        return value


class NumberRangeOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, default, lower_bound,
                 upper_bound, num_decimals, step_size):
        super().__init__(section, name, sort_tag, "number-range", doc, default, data=[lower_bound, upper_bound,
                                                                                      num_decimals, step_size])

    def get_range_info(self):
        return self._data

    def validator(self, value):
        if not isinstance(value, (int, float)):
            raise ValueError("EXpected a number type for this option")
        return value

    def to_kvp(self, book, path):
        book.set_option(self._value, path)

    def from_kvp(self, book, path):
        v = book.get_option(path)
        if v is not None and isinstance(v, (int, float)):
            self.set_default(v)
        else:
            self.set_default(0)


class NumberPlotSizeOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, default, lower_bound,
                 upper_bound, num_decimals, step_size):
        super().__init__(section, name, sort_tag, "plot-size", doc, default, data=[lower_bound, upper_bound,
                                                                                   num_decimals, step_size])

    def get_range_info(self):
        return self._data

    def set_value(self, value):
        if isinstance(value, int):
            self._value = ["pixels", value]
        else:
            self._value = value

    def get_value_type(self):
        return self._value[0]

    def get_value(self):
        return self._value

    def to_kvp(self, book, path):
        book.set_option(self._value[0], path + ['("type")'])
        book.set_option(self._value[1], path + ['("value")'])

    def from_kvp(self, book, path):
        _type = book.get_option(path + ['("type")'])
        value = book.get_option(path + ['("value")'])
        if isinstance(_type, str) and isinstance(value, int):
            self.set_default([_type, value])

    def validator(self, value):
        return value


class InternalOption(Option):
    def __init__(self, section, name, default):
        super().__init__(section, name, "", "internal", None, default)

    def validator(self, value):
        return value


class QueryOption(Option):
    def __init__(self, section, name, default):
        super().__init__(section, name, "", "query", None, default)

    def validator(self, value):
        return value


class ColorOption(Option):
    def __init__(self, section, name, sort_tag, doc, default, range, alpha):
        default = self.canonicalize(default)
        super().__init__(section, name, sort_tag, "color", doc, default)
        self._range = range
        self._alpha = alpha

    def range(self):
        return self._range

    def use_alpha(self):
        return self._alpha

    def set_value(self, value):
        self._value = value

    @staticmethod
    def canonicalize(values):
        return list(map(lambda x: int(x, 0) if isinstance(x, str) else x, values))

    @staticmethod
    def color_hex(color, range):
        def html_value(value):
            return min(255, math.trunc(value * (255 / range)))

        def number_hex(number):
            ret = "{0:X}".format(number)
            if len(ret) < 2:
                ret += "0"
            return ret

        return "".join([number_hex(html_value(color[0])), number_hex(html_value(color[1])),
                        number_hex(html_value(color[2]))])

    def hex_string(self):
        return self.color_hex(self._value, self._range)

    def html(self):
        return "#" + self.hex_string()

    def validator(self, value):
        return value


class DateFormatOption(Option, KvpInterface):
    def __init__(self, section, name, sort_tag, doc, default):
        def default_value():
            if isinstance(default, list):
                return default
            else:
                return ["unset", "number", True, ""]

        super().__init__(section, name, sort_tag, "dateformat", doc, default_value)

    def to_kvp(self, book, path):
        v = self._value
        if v[0] == "unset":
            book.delete_option(path)
        else:
            book.set_option(v[0], path + ["fmt"])
            book.set_option(v[1], path + ["month"])
            book.set_option(v[2], path + ["years"])
            book.set_option(v[3], path + ["custom"])

    def from_kvp(self, book, path):
        fmt = book.get_option(path + ["fmt"])
        month = book.get_option(path + ["month"])
        years = book.get_option(path + ["years"])
        custom = book.get_option(path + ["custom"])
        if isinstance(fmt, str) and isinstance(month, str) and isinstance(years, str) and isinstance(custom, str):
            self.set_default([fmt, month, years, custom])

    def validator(self, value):
        if not isinstance(value, (list, tuple)) or len(value) < 4:
            raise ValueError("Wrong type supplied for DateFormat ")
        return value


class CurrencyAccountingOption(Option):
    def validator(self, value):
        pass

    def __init__(self, section, name, sort_tag, doc, default):
        super().__init__(section, name, sort_tag, "currency-accounting", doc, default)

class ReferenceOption(Option, KvpInterface):
    def validator(self, value):
        pass

    def __init__(self, section, name, sort_tag, doc, default, entity_type, new_cb=None, depends_on=None,
                 depends_cb=None):
        super().__init__(section, name, sort_tag, "reference-sel", doc, default)
        self.entity_type = entity_type
        self.new_cb=new_cb
        self.depends_on = depends_on
        self.depends_cb = depends_cb
        self.option_set = False
        self._value = self.convert_to_guid(self.get_default())
        self.set = False

    def get_entity_type(self):
        return self.entity_type

    def get_new_cb(self):
        return self.new_cb

    def get_depends_on(self):
        return self.depends_on

    def get_depends_cb(self):
        return self.depends_cb

    @staticmethod
    def convert_to_guid(item):
        return item if isinstance(item, UUID) else item.get_guid() if item is not None else None

    def convert_to_reference(self, item):
        col = Session.get_current_book().get_collection(self.entity_type.__name__)
        return col.lookup_entity(item) if isinstance(item, UUID) else item

    def get_value(self):
        return self.convert_to_reference(self._value if self.set else self.get_default())

    def set_value(self, value):
        if value is None:
            value = self.get_default()
        value = self.convert_to_reference(value)
        self._value = self.convert_to_guid(value)
        self.set = True

    def serialize(self):
        return str(self._value)

    def deserialize(self, value):
        self.set_value(self.convert_to_reference(UUID(value)))

    def to_kvp(self, book, path):
        book.set_option(self._value, path)

    def from_kvp(self, book, path):
        v = book.get_option(path)
        if v is not None and isinstance(v, (str)):
            self.set_default(v)


class InvoiceOption(Option, KvpInterface):
    def validator(self, value):
        pass

    def __init__(self, section, name, sort_tag, doc, default):
        super(InvoiceOption, self).__init__(section, name, sort_tag, "reference-sel", doc, default)
        self.option_set = False
        self._value = self.convert_to_guid(self.get_default())
        self.set = False

    def get_entity_type(self):
        return Invoice

    @staticmethod
    def convert_to_guid(item):
        return item if isinstance(item, UUID) else item.get_guid() if item is not None else None

    @staticmethod
    def convert_to_invoice(item):
        return Invoice.lookup(item, Session.get_current_book()) if isinstance(item, UUID) else item

    def get_value(self):
        return self.convert_to_invoice(self._value if self.set else self.get_default())

    def set_value(self, value):
        if value is None:
            value = self.get_default()
        value = self.convert_to_invoice(value)
        self._value = self.convert_to_guid(value)
        self.set = True

    def serialize(self):
        return self._value

    def deserialize(self, value):
        self.set_value(self.convert_to_invoice(value))

    def to_kvp(self, book, path):
        book.set_option(self._value, path)

    def from_kvp(self, book, path):
        v = book.get_option(path)
        if v is not None and isinstance(v, (str)):
            self.set_default(v)


class CounterOption(NumberRangeOption):
    def __init__(self, section, name, key, sort_tag, documentation_string, default_value):
        super().__init__(section, name, sort_tag, documentation_string, default_value, 0, 999999999, 0, 1)
        self.key = key

    def to_kvp(self, book, *_):
        book.set_option(self._value, ["counters", self.key])

    def from_kvp(self, book, *_):
        self.set_value(book.get_option(["counters", self.key]))


class CounterFormatOption(StringOption):
    def __init__(self, section, name, key, sort_tag, documentation_string, default_value):
        super(CounterFormatOption, self).__init__(section, name, sort_tag, documentation_string, default_value)
        self.key = key

    def to_kvp(self, book, *_):
        book.set_option(self._value, ["counter_formats", self.key])

    def from_kvp(self, book, *_):
        self.set_value(book.get_option(["counter_formats", self.key]))


counter_types = [("Products", "Products number format", "Product number",
                  "The format string to use for generating product numbers. This is a printf_style format string.",
                  "The previous product number generated. This number will be incremented to generate the next "
                  "customer number."),
                 ("Customer", "Customer number format", "Customer number",
                  "The format string to use for generating customer numbers. This is a printf_style format string.",
                  "The previous customer number generated. This number will be incremented to generate the next "
                  "customer number."),
                 ("Staff", "Staff number format", "Staff number",
                  "The format string to use for generating employee numbers. "
                  "This is a printf_style format string.",
                  "The previous employee number generated. This number will be incremented to generate the next "
                  "employee number."),
                 ("Invoice", "Invoice number format", "Invoice number",
                  "The format string to use for generating invoice numbers."
                  " This is a printf_style format string.",
                  "The previous invoice number generated. This number will be incremented to generate the next "
                  "invoice number."),
                 ("Bill", "Bill number format", "Bill number", "The format string to use for generating bill numbers. "
                                                               "This is a printf_style format string.",
                  "The previous bill number generated. This number will be incremented to generate the next bill "
                  "number."),
                 ("ExpVoucher", "Expense voucher number format", "Expense voucher number",
                  "The format string to use for generating "
                  "expense voucher numbers. "
                  "This is a printf_style format string.",
                  "The previous expense voucher number generated. This number will be incremented to generate the "
                  "next voucher number."),
                 ("Job", "Job number format", "Job number", "The format string to use for generating job numbers."
                                                            " This is a printf_style format string.",
                  "The previous job number generated. This number will be incremented to generate the next job number."),
                 ("Order", "Order number format", "Order number",
                  "The format string to use for generating order numbers. "
                  "This is a printf_style format string.",
                  "The previous order number generated. This number will be incremented to generate the next order "
                  "number."),
                 ("Supplier", "Supplier number format", "Supplier number",
                  "The format string to use for generating vendor numbers."
                  " This is a printf_style format string.",
                  "The previous vendor number generated. This number will be incremented to generate the next vendor "
                  "number.")]


def book_options_generator(options):
    reg_option = lambda new_option: options.register_option(new_option)
    reg_option(StringOption(business_label, company_name,
                            "a", "The name of your business.", ""))

    reg_option(TextOption(
        business_label, company_addy,
        "b1", "The address of your business.", ""))

    reg_option(StringOption(
        business_label, company_contact,
        "b2", "The contact person to print on invoices.", ""))

    reg_option(StringOption(
        business_label, company_phone,
        "c1", "The phone number of your business.", ""))

    reg_option(StringOption(
        business_label, company_fax,
        "c2", "The fax number of your business.", ""))

    reg_option(StringOption(
        business_label, company_email,
        "c3", "The email address of your business.", ""))

    reg_option(StringOption(
        business_label, company_url,
        "c4", "The URL address of your website.", ""))

    reg_option(StringOption(
        business_label, company_id,
        "c5", "The ID for your company (eg 'Tax_ID: 00_000000).", ""))

    # reg_option(make_tax_option
    #  business_label,"Default Customer Tax")
    # "e","The default tax table to apply to customers.")
    # (lambda () '()) None))
    #
    # reg_option(make_tax_option
    #  business_label,"Default Supplier Tax")
    # "f","The default tax table to apply to vendors.")
    # (lambda () '()) None))

    reg_option(DateFormatOption(
        business_label, fancy_date_label,
        "g", "The default date format used for fancy printed dates.", None))

    reg_option(NumberRangeOption(option_section_accounts, option_name_auto_readonly_days, "a",
                                 "Choose the number of days after which transactions will be read_only and cannot "
                                 "be edited anymore. "
                                 "This threshold is marked by a red line in the account register windows. If zero, "
                                 "all transactions can be "
                                 "edited and none are read_only.",
                                 0, 0, 3650, 0, 1))

    reg_option(SimpleBooleanOption(
        option_section_accounts, option_name_num_field_source,
        "b", "Check to have split action field used in registers for 'Num' field in place of transaction number;"
             " transaction number shown as 'T_Num' on second line of register. Has corresponding effect on business"
             " features, reporting and imports/exports.", None))

    reg_option(SimpleBooleanOption(
        option_section_accounts, option_name_trading_accounts,
        "a", "Check to have trading accounts used for transactions involving more "
             "than one currency or commodity.", None))

    # reg_option(make_budget_option
    # option_section_budgeting option_name_default_budget
    #                                     "a","Budget to be used when none has been otherwise specified.")))

    reg_option(StringOption(tax_label, tax_nr_label, "a", "The electronic tax number of your business", ""))
    for key, format_label, number_label, format_description, number_description in counter_types:
        reg_option(CounterOption(option_section_counters, number_label, key, key + "a", number_description, {}))
        reg_option(CounterFormatOption(option_section_counters, format_label, key, key + "b", format_description,
                                       "%s{}" % key[:3]))


OptionDB.register_kvp_option_generator(ID_BOOK, book_options_generator)


def make_end_date(options, pagename, optname, sort_tag, info):
    options.register_option(
        DateOption(pagename, optname, sort_tag, info, ["relative", "end-accounting-period"],
                   False, "both", ["today", "end-this-month", "end-prev-month",
                                   "end-current-quarter", "end-prev-quarter", "end-cal-year",
                                   "end-prev-year", "end-accounting-period"]))


def make_date_interval(options, pagename, name_from, info_from, name_to, info_to, sort_tag):
    options.register_option(
        DateOption(pagename, name_from, sort_tag + "a", info_from, ["relative", "start-accounting-period"],
                   False, "both", ["today", "start-this-month", "start-prev-month",
                                   "start-current-quarter", "start-prev-quarter", "start-cal-year",
                                   "start-prev-year", "start-accounting-period"]))
    make_end_date(options, pagename, name_to, sort_tag + "b", info_to)


if os.name == "posix":
    D_FMT = locale.nl_langinfo(locale.D_FMT)
    D_T_FMT = locale.nl_langinfo(locale.D_T_FMT)
    T_FMT = locale.nl_langinfo(locale.T_FMT)
else:
    D_FMT = '%d/%m/%y'
    D_T_FMT = '%a %b %e %H:%M:%S %Y'
    T_FMT = '%H:%M:%S'


def company_info(book, key):
    return book.get_option([business_label, key])


def fancy_date_info(book, key):
    return book.get_option([business_label, fancy_date_label, key])


def fancy_date(book):
    fmt = book.get_option([business_label, fancy_date_format])
    if fmt is not None:
        return fmt
    return D_FMT
