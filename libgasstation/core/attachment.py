from dataclasses import dataclass

from sqlalchemy import Column, VARCHAR
from sqlalchemy_utils import URLType

from ._declbase import DeclarativeBaseGuid


@dataclass
class Attachment(DeclarativeBaseGuid):
    __tablename__ = "attachment"
    name: str = Column("name", VARCHAR(100))
    content_url: str = Column("content_url", URLType)
    thumbnail_url: str = Column("thumbnail_url", URLType)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
