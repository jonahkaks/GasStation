if __name__ == '__main__':
    custom_data = None
    def set_item(key, value):
        global custom_data
        if custom_data is None:
            custom_data = {}
        keys = key.split("/")
        loop_data = custom_data
        while len(keys) > 0:
            key_i = keys.pop(0)
            # loop_data = loop_data.get(key_i, {})
            if len(keys) > 0:
                if loop_data.get(key_i) is None:
                    loop_data[key_i] = {}

                loop_data = loop_data[key_i]
            else:
                loop_data[key_i] = value

    def get_item(key):
        global custom_data
        if custom_data is None:
            return
        assert not isinstance(key, int), \
            "You are accessing custom_data with an integer (={}) while a string is expected".format(key)
        keys = key.split("/")
        if not any(keys):
            return
        loop_data = custom_data.copy()
        for k in keys:
            loop_data = loop_data.get(k)
            if loop_data is None:
                return
        return loop_data


    set_item("options/counter/bill/number", 35)
    set_item("options/counter/invoice/number", 65)
    set_item("options/counter/bill/format", "%s")
    set_item("options/counter/invoice/format", "%s")
    set_item("options/counter/bill/number", 95)
    # set_item("options/accounts/counter_format", "%f")
    print(get_item("options/counter/bill/number"))
