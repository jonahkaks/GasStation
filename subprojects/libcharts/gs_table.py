from gi import require_version

require_version('Gtk', '3.0')
require_version('PangoCairo', '1.0')

from subprojects.libcharts import *
import math

MINOR_BORDER_SIZE = 1
COL_BORDER_SIZE = 3
MONTH_NAME_BUFSIZE = 10

M_PI = math.pi
DBGDRAW_RECT = 0
DBGDRAW_TEXT = 0
DBGDRAW_ITEM = 0
dashed3 = 3.0

DYNAMICS = 1
MYDEBUG = 1
PHI = 1.61803399
GTK_TABLE_BARW = 41

GTK_TABLE_MINBARW = 8
GTK_TABLE_MAXBARW = 41
GTK_TABLE_SPANBARW = GTK_TABLE_MAXBARW + 1

GTK_TABLE_MINRADIUS = 64

TABLE_BUFFER_LENGTH = 128
TABLE_PARAM_PIE_LINE = True
TABLE_PARAM_PIE_MARK = False
TABLE_PARAM_PIE_HOLEVALUE = 0.5
TABLE_MARGIN = 12
TABLE_SPACING = 6

TABLE_LINE_SPACING = 1.25


class TableFontSize(Enum):
    TITLE = 0
    SUBTITLE = 1
    NORMAL = 2
    SMALL = 3


def _isarray(a):
    if isinstance(a, (set, dict, list, tuple, map)):
        return True
    return False


def _prep_args(args):
    args = list(args)
    if args[0] and len(args) == 1 and _isarray(args[0]):
        if not args[0]['data']:
            for key in args[0]:
                val = args[0][key]
                if _isarray(val) and val['data']:
                    args[key] = str(val)
                else:
                    args[key] = {'data': str(val)}
    else:
        for k, v in enumerate(args):
            if not _isarray(v):
                args[k] = {'data': str(v)}
    return args


def calculate_step_size(step_range, target_steps):
    if not step_range:
        return 0
    step_range = float(step_range)
    target_steps = float(target_steps) or 1
    temp_step = step_range / target_steps
    mag = float(math.floor(math.log10(temp_step)))
    mag_pow = float(math.pow(10, mag))
    mag_msd = int(temp_step / mag_pow + 0.5)
    if mag_msd > 5.0:
        mag_msd = 10.0
    elif mag_msd > 2.0:
        mag_msd = 5.0
    elif mag_msd >= 1.0:
        mag_msd = 2.0
    return mag_msd * mag_pow


class GsTable(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self)
        self.rows = []
        self.heading = []
        self.surface = None
        self.auto_heading = True
        self.caption = None
        self.template = None
        self.newline = '\n'
        self.empty_cells = ''
        self.function = None
        self.caption_length = 1
        self.title = None
        self.subtitle = None
        self.rayon = 0
        self.rawmin = 0
        self.rawmax = 100
        self.pfd = None
        self.abs = False
        self.dual = False
        self.mark = 1
        self.usrbarw = 0.0
        self.buffer1 = ""
        self.barw = GTK_TABLE_BARW
        self.color_scheme = ColorScheme(ColMap.OFFICE2013)
        self.title_zh = 0.0
        self.subtitle_zh = 0.0
        self.subtitle_y = 0.0
        self.graph = Pango.Rectangle()
        self.show_mono = False
        self.minor = False
        self.minor_symbol = True
        self.active = -1
        self.label_w = 0.0

        self.lastactive = -1
        self.pfd_size = 13
        self.blkw = 0.0
        self.visible = 0
        self.ox = 0.0
        self.oy = 0.0
        self.left = 0
        self.top = 0
        self.right = 0
        self.border = 0
        self.width = 0
        self.height = 0

        self.show_over = True
        self.unit = 0.0
        self.minimum = 0.0
        self.average = 0.0
        self.min = 0.0
        self.max = 0.0
        self.div = 0.0
        self.font_h = 1.0
        self.minor_rate = 1.0
        self.timer_tag = 0
        self.total = 0
        self.range = 1.0
        self.every_xval = 7
        self.scale_w = 0.0

        self.set_homogeneous(False)
        self.pfd = None
        self.pfd_size = 0
        frame = Gtk.Frame()
        frame.set_shadow_type(Gtk.ShadowType.ETCHED_IN)
        self.drawarea = Gtk.DrawingArea()
        self.drawarea.connect("draw", self.draw)
        self.drawarea.connect("realize", self.realize)
        self.drawarea.connect("configure_event", self.configure)
        frame.add(self.drawarea)
        self.pack_start(frame, True, True, 0)
        self.show_all()

    def configure(self, widget, event):
        if self.surface:
            self.surface = None
        allocation = widget.get_allocation()

        self.surface = widget.get_window().create_similar_surface(cairo.Content.COLOR,
                                                                  allocation.width,
                                                                  allocation.height)
        context = widget.get_style_context()

        colfound, color = context.lookup_color("theme_base_color")

        if not colfound:
            colfound, color = context.lookup_color("base_color")

        if colfound:
            tcol = global_colors[ColorTypes.THBASE]
            tcol.r = color.red * 255
            tcol.g = color.green * 255
            tcol.b = color.blue * 255

        colfound, color = context.lookup_color("theme_text_color")
        if not colfound:
            context.lookup_color("text_color", color)
        if colfound:
            tcol = global_colors[ColorTypes.THTEXT]
            tcol.r = color.red * 255
            tcol.g = color.green * 255
            tcol.b = color.blue * 255

        desc = context.get_property("font", Gtk.StateFlags.NORMAL)
        if self.pfd:
            self.pfd = None
        self.pfd = desc.copy()
        self.pfd_size = desc.get_size() / PANGO_SCALE
        if widget.get_realized():
            self.recompute()
        self.full_redraw(widget)
        return True

    def queue_redraw(self):
        if self.drawarea.get_realized():
            self.recompute()
            self.full_redraw(self.drawarea)
            self.drawarea.queue_draw()

    def realize(self, widget):
        self.reconfig()

    def reconfig(self):
        alloc = self.drawarea.get_allocation()
        self.surface = cairo.ImageSurface(cairo.Format.ARGB32,
                                          alloc.width,
                                          alloc.height)
        self.recompute()

    def recompute(self):
        if not self.drawarea.get_realized() or self.surface is None:
            return
        self.calculation()

    def draw(self, widget, cr):
        if not widget.get_realized() or not self.surface:
            return False
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()

    def set_heading(self, *args):
        self.heading = _prep_args(args)
        return self

    def make_columns(self, col_limit=0, array=None):
        self.auto_heading = False
        if col_limit == 0:
            return array
        new = []
        while len(array) > 0:
            temp = array[0:col_limit]
            if len(temp) < col_limit:
                for i in range(len(temp), col_limit, 1):
                    temp.append('&nbsp')
                    new.append(temp)
        return new

    def add_row(self, *args):
        self.rows.append(_prep_args(args))
        return self

    def set_caption(self, caption, length=1):
        self.caption_length = length
        self.caption = caption
        return self

    def set_auto_heading(self, autoheading=True):
        if isinstance(autoheading, bool):
            self.auto_heading = autoheading
            return self

    def set_title(self, title):
        self.title = title

    def set_subtitle(self, subtitle):
        self.subtitle = subtitle

    def clear(self):
        self.caption = ""
        self.rows = []
        self.heading = []
        self.auto_heading = True
        return self

    def set_from_array(self, data):
        if self.auto_heading and not any(self.heading):
            self.heading = _prep_args(data.pop(0))
        self.rows = list(map(_prep_args, data))

    def set_font_size(self, layout, font_size):
        size = 10
        if font_size == TableFontSize.TITLE:
            size = self.pfd_size + 5
        elif font_size == TableFontSize.SUBTITLE:
            size = self.pfd_size + 1

        elif font_size == TableFontSize.NORMAL:
            size = self.pfd_size - 1
        elif font_size == TableFontSize.SMALL:
            size = self.pfd_size - 2
        self.pfd.set_size(size * PANGO_SCALE)
        layout.set_font_description(self.pfd)

    def calculation(self):
        drawarea = self.drawarea
        allocation = drawarea.get_allocation()
        self.left = TABLE_MARGIN
        self.top = TABLE_MARGIN
        self.right = allocation.width - TABLE_MARGIN
        self.border = allocation.height - TABLE_MARGIN
        self.width = allocation.width - (TABLE_MARGIN * 2)
        self.height = allocation.height - (TABLE_MARGIN * 2)
        gdkwindow = drawarea.get_window()
        if not gdkwindow:
            surf = cairo.ImageSurface(cairo.Format.ARGB32, allocation.width,
                                      allocation.height)
            cr = cairo.Context(surf)
        else:
            cr = gdkwindow.cairo_create()
        layout = PangoCairo.create_layout(cr)
        self.title_zh = 50
        if self.title is not None:
            self.set_font_size(layout, TableFontSize.TITLE)
            layout.set_text(self.title, -1)
            tw, th = layout.get_size()
            self.title_zh = th / PANGO_SCALE
        self.subtitle_zh = 50
        if self.subtitle is not None:
            self.set_font_size(layout, TableFontSize.SUBTITLE)
            layout.set_text(self.subtitle, -1)
            tw, th = layout.get_size()
            self.subtitle_zh = th / PANGO_SCALE
        self.subtitle_y = self.top + self.title_zh
        self.graph.y = self.top + self.title_zh + self.subtitle_zh
        self.graph.height = self.height - self.title_zh - self.subtitle_zh

        if self.title_zh > 0 or self.subtitle_zh > 0:
            self.graph.y += TABLE_MARGIN
            self.graph.height -= TABLE_MARGIN
        self.set_font_size(layout, TableFontSize.NORMAL)
        self.scale_w = 0
        self.graph.x = self.left + self.scale_w + 2
        self.graph.width = self.width - self.scale_w - 2
        self.graph.height -= (self.font_h + TABLE_SPACING)

    def full_redraw(self, widget):
        cr = cairo.Context(self.surface)
        cairo_user_set_rgbcol(cr, global_colors[ColorTypes.THBASE])
        cr.paint()
        layout = PangoCairo.create_layout(cr)

        if self.title:
            self.set_font_size(layout, TableFontSize.TITLE)
            layout.set_text(self.title.upper(), -1)
            tw, th = layout.get_size()
            cairo_user_set_rgbcol(cr, global_colors[ColorTypes.THTEXT])
            cr.move_to(self.left + (self.width - (tw / PANGO_SCALE)) / 2, self.top)
            PangoCairo.show_layout(cr, layout)

        if self.subtitle:
            self.set_font_size(layout, TableFontSize.SUBTITLE)
            layout.set_text(self.subtitle, -1)
            tw, th = layout.get_size()
            cairo_user_set_rgbcol(cr, global_colors[ColorTypes.THTEXT])
            x = self.left + (self.width - (tw / PANGO_SCALE)) / 2
            cr.move_to(x, self.top + self.subtitle_zh)
            PangoCairo.show_layout(cr, layout)

        if not self.heading and not self.rows:
            raise ValueError('Undefined table rows')
        cr.set_line_width(1)
        cairo_user_set_rgbacol(cr, global_colors[ColorTypes.THTEXT], 0.8)
        t = 0
        unit = 1
        if any(self.heading):
            ncols = len(self.heading)-1
            for i, heading in enumerate(self.heading):
                cspan = heading.get('colspan', 0)
                ncols += cspan
            unit = self.graph.width / ncols
            print(ncols, unit)
        x = self.graph.x
        y = self.graph.y
        c = 50
        for i, heading in enumerate(self.heading):
            data = heading.get('data', '')
            cspan = heading.get('colspan', 0)
            x += (unit * i) + (unit * cspan)
            layout.set_text(data, -1)
            tw, th = layout.get_size()
            c = max(c, th / PANGO_SCALE)
            cr.move_to(x, y)
            PangoCairo.show_layout(cr, layout)

            cr.rectangle(x + 0.5, y+ 0.5, unit, (th / PANGO_SCALE))
            cr.stroke()
        y += c

        if any(self.rows):
            for row in self.rows:
                for i, cell in enumerate(row):
                    data = cell.get('data', '')
                    cspan = cell.get('colspan', 1)
                    x += math.floor((unit * i) + (unit * cspan))
                    layout.set_text(data, -1)
                    tw, th = layout.get_size()
                    c = max(c, th / PANGO_SCALE)
                    cr.move_to(x, y)
                    PangoCairo.show_layout(cr, layout)
                    cr.rectangle(x + 0.5, y + 0.5, unit, (th / PANGO_SCALE))
                    cr.stroke()
                y += c

        # self.clear()


GObject.type_register(GsTable)

if __name__ == '__main__':
    window = Gtk.Window()
    w = GsTable()
    w.set_title("JULAW ENERGY LTD")
    w.set_subtitle("DEBTORS NAME FOR PERIOD")
    window.add(w)
    w.set_heading("Debtor's Name", {'data': "Balance", 'colspan': 2}, "Kafeero", "Charles", "Mutumba", "Timothy",
                  "Tonny")
    w.add_row("Kafeero", {'data': "435000", 'style': 'color:blue'})
    w.add_row("Kapuch", {'data': "435000", 'style': 'color:blue'})
    w.add_row("Kibalama", {'data': "435000", 'style': 'color:blue'})
    w.add_row("Kiggundu", {'data': "435000", 'style': 'color:blue'})
    w.add_row("Kamulegeya", {'data': "435000", 'style': 'color:blue'})
    w.add_row("Kato", {'data': "435000", 'style': 'color:blue'})
    w.add_row("Kityo", {'data': "435000", 'style': 'color:blue'})
    w.add_row("Jemmy", {'data': "435000", 'style': 'color:blue'})
    w.show_all()

    window.connect("destroy", lambda w: Gtk.main_quit())
    window.maximize()
    window.show_all()
    Gtk.main()
