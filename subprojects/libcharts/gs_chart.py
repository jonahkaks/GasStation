import gi
gi.require_version('Gtk', '3.0')
gi.require_version('PangoCairo', '1.0')
from subprojects.libcharts.gs_colors import *
from gi.repository import Gtk, Gdk, GObject, Pango, PangoCairo
import cairo
import math
from enum import Enum

M_PI = math.pi
DBGDRAW_RECT = 0
DBGDRAW_TEXT = 0
DBGDRAW_ITEM = 0
dashed3 = 3.0

DYNAMICS = 1
MYDEBUG = 1
PHI = 1.61803399
GTK_CHART_BARW = 41

GTK_CHART_MINBARW = 8
GTK_CHART_MAXBARW = 41
GTK_CHART_SPANBARW = GTK_CHART_MAXBARW + 1

GTK_CHART_MINRADIUS = 64

CHART_BUFFER_LENGTH = 128
CHART_PARAM_PIE_LINE = True
CHART_PARAM_PIE_MARK = False
CHART_PARAM_PIE_HOLEVALUE = 0.95
CHART_MARGIN = 12
CHART_SPACING = 6

CHART_LINE_SPACING = 1.25


class ChartFontSize(Enum):
    TITLE = 0
    SUBTITLE = 1
    NORMAL = 2
    SMALL = 3


class ChartType(Enum):
    NONE = 0
    COL = 1
    PIE = 2
    LINE = 3
    MAX = 4
    DOUGHNUT = 5


class LstLegend(Enum):
    FAKE = 1
    COLOR = 2
    TITLE = 3
    AMOUNT = 4
    RATE = 5
    LEGEND = 6


class ChartItem:
    def __init__(self):
        self.label = None
        self.series = None
        self.rates = None
        self.legends = None
        self.angle = None
        self.height = None


def calculate_step_size(step_range, target_steps):
    if not step_range:
        return 0
    step_range = float(step_range)
    target_steps = float(target_steps) or 1
    temp_step = step_range / target_steps
    mag = float(math.floor(math.log10(temp_step)))
    mag_pow = float(math.pow(10, mag))
    mag_msd = int(temp_step / mag_pow + 0.5)
    if mag_msd > 5.0:
        mag_msd = 10.0
    elif mag_msd > 2.0:
        mag_msd = 5.0
    elif mag_msd >= 1.0:
        mag_msd = 2.0
    return mag_msd * mag_pow


class GsChart(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self)
        self.surface = None
        self.nb_items = 0
        self.items = []
        self.title = None
        self.subtitle = None
        self.rayon = 0
        self.rawmin = 0
        self.rawmax = 100
        self.pfd = None
        self.abs = False
        self.dual = False
        self.mark = 1
        self.usrbarw = 0.0
        self.buffer1 = ""
        self.barw = GTK_CHART_BARW
        self.color_scheme = ColorScheme(ColMap.OFFICE2013)
        self.title_zh = 0.0
        self.subtitle_zh = 0.0
        self.subtitle_y = 0.0
        self.model = None
        self.graph = Pango.Rectangle()
        self.legend = Pango.Rectangle()
        self.legend_label_w = ""
        self._show_legend = True
        self._show_legend_wide = True
        self.show_mono = False
        self.minor = False
        self.minor_symbol = True
        self.kcur = None
        self.type = 0
        self.active = -1
        self.label_w = 0.0
        self.legend_font_h = 0.0
        self.legend_label_w = 0.0
        self.legend_value_w = 0.0
        self.legend_rate_w = 0.0
        self.lastactive = -1
        self.pfd_size = 13
        self.blkw = 0.0
        self.visible = 0
        self.ox = 0.0
        self.oy = 0.0
        self.l = 0
        self.t = 0
        self.r = 0
        self.b = 0
        self.w = 0
        self.h = 0
        self.param_pie_donut = False
        self._show_xval = True
        self.average = True
        self._show_average = True
        self.show_over = True
        self.unit = 0.0
        self.minimum = 0.0
        self.average = 0.0
        self.min = 0.0
        self.max = 0.0
        self.div = 0.0
        self.font_h = 1.0
        self.minor_rate = 1.0
        self.timer_tag = 0
        self.total = 0
        self.range = 1.0
        self.every_xval = 7
        self.scale_w = 0.0
        self.set_homogeneous(False)
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        self.pack_start(vbox, True, True, 0)
        frame = Gtk.Frame()
        frame.set_shadow_type(Gtk.ShadowType.ETCHED_IN)

        self.drawarea = Gtk.DrawingArea()
        frame.add(self.drawarea)
        vbox.pack_start(frame, True, True, 0)
        self.drawarea.set_size_request(100, 100)
        if DYNAMICS == 1:
            self.drawarea.set_has_tooltip(True)
        self.drawarea.show()

        self.adjustment = Gtk.Adjustment(value=0.0, lower=0.0, upper=1.0,
                                         step_increment=1.0,
                                         page_increment=1.0,
                                         page_size=1.0)
        self.scrollbar = Gtk.Scrollbar.new(orientation=Gtk.Orientation.HORIZONTAL,
                                           adjustment=self.adjustment)
        vbox.pack_start(self.scrollbar, False, True, 0)
        self.scrollbar.show()
        self.drawarea.connect("configure-event", self.configure_event_callback)

        # self.drawarea.connect("realize", self.realize_callback)
        self.drawarea.connect("draw", self.draw_callback)

        if DYNAMICS == 1:
            self.drawarea.add_events(Gdk.EventMask.EXPOSURE_MASK)
            self.drawarea.add_events(Gdk.EventMask.POINTER_MOTION_HINT_MASK)
            self.drawarea.connect("query-tooltip", self.querytooltip_callback)
            self.drawarea.connect("motion-notify-event", self.motion_notify_event_cb)
        self.adjustment.connect("value-changed", self.col_first_changed)

    def print_int(self, value):
        self.buffer1 = str(value)
        return self.buffer1

    def print_rate(self, value):
        self.buffer1 = "%.2f%%" % value
        return self.buffer1

    def print_double(self, buffer, value):
        buffer = str(value)
        return buffer

    def clear(self):
        if self.title:
            self.title = None
        if self.subtitle:
            self.subtitle = None
        if self.items:
            self.items = []

        self.nb_items = 0

        self.total = 0
        self.range = 0
        self.rawmin = 0
        self.rawmax = 0
        self.every_xval = 7

        self.active = -1
        self.lastactive = -1

    def set_font_size(self, layout, font_size):
        if font_size == ChartFontSize.TITLE:
            size = self.pfd_size + 5
        elif font_size == ChartFontSize.SUBTITLE:
            size = self.pfd_size + 1
        elif font_size == ChartFontSize.NORMAL:
            size = self.pfd_size - 1
        elif font_size == ChartFontSize.SMALL:
            size = self.pfd_size - 2
        else:
            size = 10
        self.pfd.set_size(size * PANGO_SCALE)
        layout.set_font_description(self.pfd)

    def recompute(self):
        if not self.drawarea.get_realized() or self.surface is None:
            return
        self.calculation()
        if self.type in (ChartType.LINE, ChartType.COL):
            self.col_calculation()
            self.adjustment.set_value(0)
            self.col_scrollbar_setvalues()
            self.scrollbar.show()

        if self.type == ChartType.PIE or self.type == ChartType.DOUGHNUT:
            self.pie_calculation()
            self.scrollbar.hide()

    def col_compute_range(self):
        lobound = self.rawmin
        hibound = self.rawmax
        self.range = self.rawmax - self.rawmin
        maxticks = min(10, math.floor(self.graph.height / (self.font_h * 2)))
        self.unit = calculate_step_size((hibound - lobound), abs(maxticks))
        self.min = -self.unit * math.ceil(-lobound / self.unit)
        self.max = self.unit * math.ceil(hibound / self.unit)
        self.range = self.max - self.min
        self.div = self.range / self.unit

    def calculation(self):
        drawarea = self.drawarea
        allocation = drawarea.get_allocation()
        self.l = CHART_MARGIN
        self.t = CHART_MARGIN
        self.r = allocation.width - CHART_MARGIN
        self.b = allocation.height - CHART_MARGIN
        self.w = allocation.width - (CHART_MARGIN * 2)
        self.h = allocation.height - (CHART_MARGIN * 2)
        gdkwindow = drawarea.get_window()
        if not gdkwindow:
            surf = cairo.ImageSurface(cairo.Format.ARGB32, allocation.width,
                                      allocation.height)
            cr = cairo.Context(surf)
        else:
            cr = gdkwindow.cairo_create()
        layout = PangoCairo.create_layout(cr)
        self.title_zh = 50
        if self.title is not None:
            self.set_font_size(layout, ChartFontSize.TITLE)
            layout.set_text(self.title, -1)
            tw, th = layout.get_size()
            self.title_zh = th / PANGO_SCALE
        self.subtitle_zh = 50
        if self.subtitle is not None:
            self.set_font_size(layout, ChartFontSize.SUBTITLE)
            layout.set_text(self.subtitle, -1)
            tw, th = layout.get_size()
            self.subtitle_zh = th / PANGO_SCALE
        self.subtitle_y = self.t + self.title_zh
        self.graph.y = self.t + self.title_zh + self.subtitle_zh
        self.graph.height = self.h - self.title_zh - self.subtitle_zh

        if self.title_zh > 0 or self.subtitle_zh > 0:
            self.graph.y += CHART_MARGIN
            self.graph.height -= CHART_MARGIN
        self.set_font_size(layout, ChartFontSize.NORMAL)
        self.scale_w = 0
        self.col_compute_range()
        valstr = self.print_int(self.min)
        layout.set_text(valstr, -1)
        tw, th = layout.get_size()
        self.scale_w = (tw / PANGO_SCALE)
        valstr = self.print_int(self.max)
        layout.set_text(valstr, -1)
        tw, th = layout.get_size()

        self.scale_w = max(self.scale_w, (tw / PANGO_SCALE))
        label_w = 0
        for item in self.items:
            layout.set_text(item.label, -1)
            tw, th = layout.get_size()
            label_w = max(label_w, (tw / PANGO_SCALE))
        self.label_w = label_w
        self.font_h = th / PANGO_SCALE
        if self.type == ChartType.LINE or ChartType.COL:
            self.graph.x = self.l + self.scale_w + 2
            self.graph.width = self.w - self.scale_w - 2
        elif self.type == ChartType.PIE or self.type == ChartType.DOUGHNUT:
            self.graph.x = self.l
            self.graph.width = self.w

        if self.type == ChartType.LINE or self.type == ChartType.COL and self._show_xval:
            self.graph.height -= (self.font_h + CHART_SPACING)

        if self._show_legend:
            self.set_font_size(layout, ChartFontSize.SMALL)
            layout.set_text("00.00 %", -1)
            tw, th = layout.get_size()
            self.legend_font_h = (th / PANGO_SCALE)
            lw = math.floor(self.graph.width / 4)
            self.legend_label_w = min(self.label_w, lw)

            self.legend.width = self.legend_font_h + CHART_SPACING + self.legend_label_w
            self.legend.height = min(math.floor(self.nb_items * self.legend_font_h * CHART_LINE_SPACING),
                                     self.graph.height)

            if self._show_legend_wide:
                self.legend_value_w = self.scale_w
                self.legend_rate_w = (tw / PANGO_SCALE)
                self.legend.width += CHART_SPACING + self.legend_value_w + CHART_SPACING + self.legend_rate_w
            self.graph.width -= (self.legend.width + CHART_MARGIN)

            self.legend.x = self.graph.x + self.graph.width + CHART_MARGIN
            self.legend.y = self.graph.y

    def col_calculation(self):
        if not self.nb_items:
            return
        if self.usrbarw > 0.0:
            blkw = self.usrbarw
            self.barw = blkw - 3
        else:
            maxvisi = math.floor(self.graph.width / (GTK_CHART_MAXBARW + 3))
            if self.nb_items <= maxvisi:
                self.barw = GTK_CHART_MAXBARW
                blkw = GTK_CHART_MAXBARW + (self.graph.width - (self.nb_items * GTK_CHART_MAXBARW)) / self.nb_items
            else:
                blkw = max(GTK_CHART_MINBARW, math.floor(self.graph.width / self.nb_items))
                self.barw = blkw - 3
        if self.dual:
            self.barw /= 2
        self.blkw = blkw
        self.visible = self.graph.width / blkw
        self.visible = min(self.visible, self.nb_items)

        self.ox = self.l
        self.oy = self.b
        if self.range > 0:
            self.oy = math.floor(self.graph.y + (self.max / self.range) * self.graph.height)
        if self.label_w > 0 and self.visible > 0:
            if self.label_w <= self.blkw:
                self.every_xval = 1
            else:
                self.every_xval = math.floor(0.5 + (self.label_w + CHART_SPACING) / self.blkw)

    def col_draw_scale(self, widget):
        cr = cairo.Context(self.surface)
        layout = PangoCairo.create_layout(cr)
        self.set_font_size(layout, ChartFontSize.NORMAL)
        cr.set_line_width(1)
        curxval = self.max
        cr.set_dash([10.0, 0.0], 0)
        for i in range(0, int(self.div), 1):
            y = 0.5 + math.floor(self.graph.y + ((i * self.unit) / self.range) * self.graph.height)
            cairo_user_set_rgbacol(cr, global_colors[ColorTypes.THTEXT], 0.8 if curxval == 0.0 else 0.1)
            cr.move_to(self.graph.x, y)
            cr.line_to(self.graph.x + self.graph.width, y)
            cr.stroke()
            cairo_user_set_rgbacol(cr, global_colors[ColorTypes.THTEXT], 0.78)
            valstr = self.print_int(curxval)
            layout.set_text(valstr, -1)
            tw, th = layout.get_size()
            cr.move_to(self.graph.x - (tw / PANGO_SCALE) - 2, y - ((th / PANGO_SCALE) * 0.8))
            PangoCairo.show_layout(cr, layout)
            curxval -= self.unit

        if self._show_xval and self.every_xval > 0:
            x = self.graph.x + (self.blkw / 2)
            y = self.b - self.font_h
            first = self.adjustment.get_value()
            cr.set_dash([dashed3, 1.0], 0)
            for i in range(int(first), int(first + self.visible), 1):
                if not i % self.every_xval:
                    item = self.items[i]
                    cairo_user_set_rgbacol(cr, global_colors[ColorTypes.THTEXT], 0.1)
                    cr.move_to(x, self.graph.y)
                    cr.line_to(x, self.b - self.font_h)
                    cr.stroke()

                    valstr = item.label
                    layout.set_text(valstr, -1)
                    tw, th = layout.get_size()
                    cairo_user_set_rgbacol(cr, global_colors[ColorTypes.THTEXT], 0.78)
                    cr.move_to(x - ((tw / PANGO_SCALE) / 2), y)
                    PangoCairo.show_layout(cr, layout)
                x += self.blkw

        if self._show_average:
            if self.average < 0:
                y = 0.5 + self.oy + (abs(self.average) / self.range) * self.graph.height
            else:
                y = 0.5 + self.oy - (abs(self.average) / self.range) * self.graph.height
            cairo_user_set_rgbacol(cr, global_colors[ColorTypes.THTEXT], 1.0)
            cr.set_line_width(1.0)
            cr.set_dash([dashed3, 1.0], 0)
            cr.move_to(self.graph.x, y)
            cr.line_to(self.graph.x + self.graph.width, y)
            cr.stroke()

    def col_draw_bars(self, widget):

        if self.nb_items <= 0:
            return
        x = self.graph.x
        first = self.adjustment.get_value()

        cr = widget.get_window().cairo_create()
        if DBGDRAW_ITEM == 1:
            x2 = x + 0.5
            cr.set_line_width(1.0)
            dashlength = 4.0
            cr.set_dash([dashlength, 1.0], 0)
            cr.set_source_rgb(1.0, 0.0, 1.0)
            for i in range(int(first), int(first + self.visible), 1):
                cr.move_to(x2, self.graph.y)
                cr.line_to(x2, self.graph.x + self.graph.height)
                x2 += self.blkw
            cr.stroke()
            cr.set_dash([dashlength, 0.0], 0)

        for i, item in enumerate(self.items):
            barw = self.barw
            if not self.show_mono:
                color = i % self.color_scheme.nb_cols
            else:
                color = self.color_scheme.cs_green
            cairo_user_set_rgbcol_over(cr, self.color_scheme.colors[color], i == self.active)
            x2 = x + (self.blkw / 2) - 1
            if not self.dual:
                x2 -= (barw / 2)
            else:
                x2 -= barw - 1
            for serie in item.series:
                h = math.floor((serie / self.range) * self.graph.height)
                y2 = self.oy - h
                if serie < 0.0:
                    y2 += 1
                    if self.show_mono:
                        color = self.color_scheme.cs_red
                        cairo_user_set_rgbcol_over(cr, self.color_scheme.colors[color], i == self.active)

                cr.rectangle(x2 + 2, y2, barw, h)
                cr.fill()
                x2 = x2 + barw + 1
            x += self.blkw

    def col_get_active(self, widget, x, y):
        retval = -1
        if self.r >= x >= self.graph.x and self.graph.y <= y <= self.b:
            px = x - self.graph.x
            first = self.adjustment.get_value()
            index = first + (px / self.blkw)
            if index < self.nb_items:
                retval = index
        return retval

    def col_first_changed(self, adj):
        self.full_redraw(self.drawarea)
        self.drawarea.queue_draw()

    def col_scrollbar_setvalues(self):
        first = self.adjustment.get_value()
        self.adjustment.set_upper(self.nb_items)
        self.adjustment.set_page_size(self.visible)
        self.adjustment.set_page_increment(self.visible)
        if first + self.visible > self.nb_items:
            self.adjustment.set_value(self.nb_items - self.visible)
        # self.adjustment.value_changed()
        if self.visible < self.nb_items:
            self.scrollbar.hide()
        else:
            self.scrollbar.show()

    def line_draw_plot(self, cr, x, y, r):
        cr.set_line_width(r / 2)
        cairo_user_set_rgbcol(cr, global_colors[ColorTypes.THBASE])
        cr.arc(x, y, r, 0, 2 * math.pi)
        cr.stroke_preserve()
        cairo_user_set_rgbcol(cr, self.color_scheme.colors[self.color_scheme.cs_blue])
        cr.fill()

    def line_draw_lines(self, widget):
        if self.nb_items <= 0:
            return
        x = self.graph.x
        y = self.oy
        first = self.adjustment.get_value()

        cr = widget.get_window().cairo_create()
        if DBGDRAW_ITEM == 1:
            x2 = x + 0.5
            cr.set_line_width(1.0)
            dashlength = 4.0
            cr.set_dash([dashlength, 1.0], 0)
            cr.set_source_rgb(1.0, 0.0, 1.0)
            for i in range(int(first), int(first + self.visible), 1):
                cr.move_to(x2, self.graph.y)
                cr.line_to(x2, self.graph.x + self.graph.height)
                x2 += self.blkw
            cr.stroke()
            cr.set_dash([dashlength, 0.0], 0)

        lastx = x
        firstx = x
        linew = 4.0
        if self.barw < 24:
            linew = 1 + (self.barw / 8.0)
        cr.set_line_join(cairo.LineJoin.BEVEL)
        cr.set_line_width(linew)
        for i, item in enumerate(self.items):
            x2 = x + self.blkw / 2
            y2 = self.oy - (item.series[0] / self.range) * self.graph.height
            if i == first:
                firstx = x2
                cr.move_to(x2, y2)
            else:
                if i < self.nb_items:
                    cr.line_to(x2, y2)
                    lastx = x2
                else:
                    lastx = x2 - self.barw
            x += self.blkw

        cairo_user_set_rgbcol(cr, self.color_scheme.colors[self.color_scheme.cs_blue])
        cr.stroke_preserve()
        cr.line_to(lastx, y)
        cr.line_to(firstx, y)
        cr.close_path()
        cairo_user_set_rgbacol(cr, self.color_scheme.colors[self.color_scheme.cs_blue], AREA_ALPHA)
        cr.fill()
        x = self.graph.x
        y = self.oy
        first = self.adjustment.get_value()
        for i in range(int(first), int(first + self.visible), 1):
            item = self.items[i]
            x2 = x + self.blkw / 2
            for serie in item.series:
                y2 = self.oy - (item.series[0] / self.range) * self.graph.height
                if i == self.active:
                    cairo_user_set_rgbacol(cr, global_colors[ColorTypes.THTEXT], 0.1)
                    cr.set_line_width(1.0)
                    cr.move_to(x2, self.graph.y)
                    cr.line_to(x2, self.b - self.font_h)
                    cr.stroke()
                    linew += 1
                self.line_draw_plot(cr, x2, y2, linew)
            x += self.blkw
        if self._show_average:
            if self.average < 0:
                y = 0.5 + self.oy + (abs(self.average) / self.range) * self.graph.height
            else:
                y = 0.5 + self.oy - (abs(self.average) / self.range) * self.graph.height
        y2 = (abs(self.min) / self.range) * self.graph.height - (y - self.oy) + 1
        cairo_user_set_rgbacol(cr, global_colors[ColorTypes.THTEXT], 1.0)
        cr.set_line_width(1.0)
        cr.set_dash([dashed3, 1.0], 0)
        cr.move_to(self.graph.x, y)
        cr.line_to(self.graph.x + self.graph.width, y)
        cr.stroke()

        if self.show_over:
            if self.minimum >= self.min:
                if self.minimum < 0:
                    y = 0.5 + self.oy + (abs(self.minimum) / self.range) * self.graph.height
                else:
                    y = 0.5 + self.oy - (abs(self.minimum) / self.range) * self.graph.height

                y2 = (abs(self.min) / self.range) * self.graph.height - (y - self.oy) + 1

                cr.set_source_rgba(COLTOCAIRO(255), COLTOCAIRO(0), COLTOCAIRO(0), AREA_ALPHA / 2)

                cr.rectangle(self.graph.x, y, self.graph.width, y2)
                cr.fill()

                cr.set_line_width(1.0)
                cr.set_source_rgb(COLTOCAIRO(255), COLTOCAIRO(0), COLTOCAIRO(0))

                cr.set_dash([dashed3, 1.0], 0)
                cr.move_to(self.graph.x, y)
                cr.line_to(self.graph.x + self.graph.width, y)
                cr.stroke()

    def line_get_active(self, widget, x, y):
        retval = -1
        if self.r >= x >= self.graph.x and self.graph.y <= y <= self.b:
            px = (x - self.graph.x)
            first = self.adjustment.get_value()
            index = first + (px / self.blkw)
            if index < self.nb_items:
                retval = index

        return retval

    def pie_calculation(self):
        w = self.graph.width
        h = self.graph.height

        self.rayon = min(w, h)
        self.mark = 0

        if CHART_PARAM_PIE_MARK:
            m = math.floor(self.rayon / 100)
            m = max(2, m)
            self.rayon -= (m * 2)
            self.mark = m

        self.ox = self.graph.x + (w / 2)
        self.oy = self.graph.y + (self.rayon / 2)

    def pie_draw_slices(self, widget):
        if self.nb_items <= 0 or self.total == 0:
            return
        cx = self.ox
        cy = self.oy
        radius = self.rayon / 2
        sum = 0.0
        cr = cairo.Context(self.surface)
        for i, item in enumerate(self.items):
            a1 = ((360 * (sum / self.total)) - 90) * (M_PI / 180)
            sum += abs(item.series[0])
            a2 = ((360 * (sum / self.total)) - 90) * (M_PI / 180)
            dx = cx
            dy = cy
            cr.move_to(dx, dy)
            cr.arc(dx, dy, radius, a1, a2)
            if CHART_PARAM_PIE_LINE:
                cr.set_line_width(2.0)
                cr.set_source_rgb(1.0, 1.0, 1.0)
                cr.line_to(cx, cy)
                cr.stroke_preserve()
                color = i % self.color_scheme.nb_cols
                cairo_user_set_rgbcol_over(cr, self.color_scheme.colors[color], i == self.active)
                cr.fill()

        if self.param_pie_donut:
            a1 = 0
            a2 = 2 * M_PI
            radius = int((self.rayon / 2) * CHART_PARAM_PIE_HOLEVALUE)
            cr.arc(cx, cy, radius, a1, a2)
            cairo_user_set_rgbcol(cr, global_colors[ColorTypes.THBASE])
            cr.fill()

    def pie_get_active(self, widget, x, y):
        retval = -1
        px = x - self.ox
        py = y - self.oy
        h = math.sqrt(math.pow(px, 2) + math.pow(py, 2))
        radius = self.rayon / 2
        if radius >= h >= (radius * CHART_PARAM_PIE_HOLEVALUE):
            b = (math.acos(px / h) * 180) / M_PI
            if py > 0:
                angle = 0
            else:
                angle = 360 - b
            angle += 90
            if angle > 360:
                angle -= 360

            cumul = 0
            for index, item in enumerate(self.items):
                cumul += abs(item.series[0] / self.total) * 360
                if cumul > angle:
                    retval = index
                    break

        return retval

    def full_redraw(self, widget):
        cr = cairo.Context(self.surface)
        cairo_user_set_rgbcol(cr, global_colors[ColorTypes.THBASE])
        cr.paint()

        if not self.nb_items:
            return False
        if DBGDRAW_RECT == 1:
            cr.set_line_width(1.0)
            cr.set_source_rgb(0.0, 1.0, 0.0)
            cr.rectangle(self.l + 0.5, self.t + 0.5, self.w, self.h)
            cr.stroke()

            cr.set_source_rgb(1.0, 0.5, 0.0)
            cr.rectangle(self.graph.x + 0.5, self.graph.y + 0.5, self.graph.width, self.graph.height)
            cr.stroke()

        if self.title:
            layout = PangoCairo.create_layout(cr)
            self.set_font_size(layout, ChartFontSize.TITLE)
            layout.set_text(self.title, -1)

            tw, th = layout.get_size()
            cairo_user_set_rgbcol(cr, global_colors[ColorTypes.THTEXT])
            x = self.l + (self.w - (tw / PANGO_SCALE)) / 2
            y = self.t
            cr.move_to(x, y)
            PangoCairo.show_layout(cr, layout)
            if DBGDRAW_TEXT == 1:
                cr.set_source_rgb(0.0, 0.0, 1.0)
                dashlength = 3.0
                cr.set_dash([dashlength, 1.0], 0)
                cr.rectangle(x + 0.5, y + 0.5, (tw / PANGO_SCALE), (th / PANGO_SCALE))
                cr.stroke()

        if self.subtitle:
            layout = PangoCairo.create_layout(cr)
            self.set_font_size(layout, ChartFontSize.SUBTITLE)
            layout.set_text(self.subtitle, -1)
            tw, th = layout.get_size()
            cairo_user_set_rgbcol(cr, global_colors[ColorTypes.THTEXT])
            x = self.l + (self.w - (tw / PANGO_SCALE)) / 2
            y = self.t + self.title_zh
            cr.move_to(x, y)
            PangoCairo.show_layout(cr, layout)
            if DBGDRAW_TEXT == 1:
                cr.set_source_rgb(0.0, 0.0, 1.0)
                dashlength = 3.0
                cr.set_dash([dashlength, 1.0], 0)
                cr.rectangle(x + 0.5, y + 0.5, (tw / PANGO_SCALE), (th / PANGO_SCALE))
                cr.stroke()

        if self.type == ChartType.COL:
            self.col_draw_scale(widget)
        elif self.type == ChartType.LINE:
            self.col_draw_scale(widget)
        elif self.type == ChartType.PIE or self.type == ChartType.DOUGHNUT:
            self.pie_draw_slices(widget)

        if self._show_legend:
            layout = PangoCairo.create_layout(cr)
            self.set_font_size(layout, ChartFontSize.SMALL)
            layout.set_ellipsize(Pango.EllipsizeMode.END)
            x = self.legend.x
            y = self.legend.y
            radius = self.legend_font_h

            if DBGDRAW_RECT == 1:
                cr.set_source_rgb(1.0, 0.5, 0.0)
                dashlength = 3.0
                cr.set_dash([dashlength, 1.0], 0)
                cr.rectangle(self.legend.x + 0.5, self.legend.y + 0.5, self.legend.width, self.legend.height)
                cr.stroke()

            for i, item in enumerate(self.items):
                if item:
                    if DBGDRAW_TEXT == 1:
                        cr.set_source_rgb(0.0, 0.0, 1.0)
                        dashlength = 3.0
                        cr.set_dash([dashlength, 1.0], 0)
                        cr.rectangle(x + 0.5, y + 0.5, self.legend_font_h + CHART_SPACING + self.legend_label_w,
                                     self.legend_font_h)
                        cr.stroke()
                if self.nb_items - i > 1:
                    if (y + math.floor(2 * radius * CHART_LINE_SPACING)) > self.b:
                        layout.set_text("...", -1)
                        cr.move_to(x + radius + CHART_SPACING, y)
                        PangoCairo.show_layout(cr, layout)
                        break
                cr.arc(x + (radius / 2), y + (radius / 2), (radius / 2), 0, 2 * M_PI)
                color = i % self.color_scheme.nb_cols
                cairo_user_set_rgbcol(cr, self.color_scheme.colors[color])
                cr.fill()
                cairo_user_set_rgbacol(cr, global_colors[ColorTypes.THTEXT], 0.78)

                valstr = item.label
                layout.set_text(valstr, -1)
                layout.set_width(self.legend_label_w * PANGO_SCALE)
                cr.move_to(x + self.legend_font_h + CHART_SPACING, y)
                PangoCairo.show_layout(cr, layout)

                if self._show_legend_wide:
                    layout.set_width(-1)
                    valstr = self.print_double(self.buffer1, item.series[0])
                    layout.set_text(valstr, -1)
                    tw, th = layout.get_size()
                    cr.move_to(x + self.legend_font_h + self.legend_label_w + (
                            CHART_SPACING * 3) + self.legend_value_w - (tw / PANGO_SCALE), y)
                    PangoCairo.show_layout(cr, layout)
                    valstr = self.print_rate(item.rate)
                    layout.set_text(valstr, -1)
                    tw, th = layout.get_size()
                    cr.move_to(
                        x + self.legend_font_h + self.legend_label_w + self.legend_value_w + self.legend_rate_w + (
                                CHART_SPACING * 3) - (tw / PANGO_SCALE), y)
                    PangoCairo.show_layout(cr, layout)

                y += math.floor(radius * CHART_LINE_SPACING)

        return True

    def configure_event_callback(self, widget, event):
        if self.surface is not None:
            self.surface.finish()
            self.surface = None
        allocation = widget.get_allocation()
        self.surface = widget.get_window().create_similar_surface(cairo.Content.COLOR, allocation.width,
                                                                  allocation.height)
        context = widget.get_style_context()
        color_global_default()
        colfound, color = context.lookup_color("theme_base_color")
        if not colfound:
            colfound, color = context.lookup_color("base_color")

        if colfound:
            tcol = global_colors[ColorTypes.THBASE]
            tcol.r = color.red * 255
            tcol.g = color.green * 255
            tcol.b = color.blue * 255

        colfound, color = context.lookup_color("theme_text_color")
        if not colfound:
            context.lookup_color("text_color", color)
        if colfound:
            tcol = global_colors[ColorTypes.THTEXT]
            tcol.r = color.red * 255
            tcol.g = color.green * 255
            tcol.b = color.blue * 255

        desc = context.get_property("font", Gtk.StateFlags.NORMAL)
        if self.pfd:
            self.pfd = None
        self.pfd = desc.copy()
        self.pfd_size = desc.get_size() / PANGO_SCALE
        if widget.get_realized():
            self.recompute()
        self.full_redraw(widget)
        return True

    def draw_callback(self, widget, cr):
        if not widget.get_realized() or not self.surface:
            return False
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()
        if self.type == ChartType.COL:
            self.col_draw_bars(widget)
        elif self.type == ChartType.LINE:
            self.line_draw_lines(widget)
        elif self.type == ChartType.PIE or self.type == ChartType.DOUGHNUT:
            self.pie_draw_slices(widget)
        return False

    def querytooltip_callback(self, widget, x, y, keyboard_mode, tooltip):
        retval = False
        if not self.surface:
            return False
        if self.lastactive != self.active:
            self.lastactive = self.active
            return retval
        if self.active >= 0:
            item = self.items[int(self.active)]
            strval = self.print_double(self.buffer1, item.series[0])
            if not self.dual:
                if self.type == ChartType.PIE or self.type == ChartType.DOUGHNUT:
                    buffer = "%s\n%s\n%.2f%%" % (item.label, strval, item.rate)
                else:
                    buffer = "%s\n%s" % (item.label, strval)
            else:
                strval2 = self.print_double(self.buffer2, item.serie2)
                buffer = "%s\n+%s\n%s" % (item.label, strval2, strval)
            tooltip.set_text(buffer)
            retval = True
        self.lastactive = self.active
        return retval

    def motion_notify_event_cb(self, widget, event):
        retval = True
        if not self.surface or self.nb_items == 0:
            return False
        x = event.x
        y = event.y
        if event.is_hint:
            t, x, y, mask = event.window.get_device_position(event.device)
        if self.type == ChartType.COL:
            self.active = self.col_get_active(widget, x, y)
        if self.type == ChartType.LINE:
            self.active = self.line_get_active(widget, x, y)
        if self.type == ChartType.PIE or self.type == ChartType.DOUGHNUT:
            self.active = self.pie_get_active(widget, x, y)
        if self._show_legend and self.active == - 1:
            if x >= self.legend.x and (x <= self.legend.x + self.legend.width) \
                    and y >= self.legend.y and (y <= self.legend.y + self.legend.height):
                self.active = (y - self.legend.y) / math.floor(self.legend_font_h * CHART_LINE_SPACING)

            if self.active > self.nb_items - 1:
                self.active = -1

        if self.lastactive != self.active:
            first = int(self.adjustment.get_value())
            update_rect = Gdk.Rectangle()
            if self.type == ChartType.PIE or self.type == ChartType.DOUGHNUT:
                update_rect.x = self.graph.x
                update_rect.y = self.graph.y
                update_rect.width = self.graph.width
                update_rect.height = self.graph.height
                widget.get_window().invalidate_rect(update_rect, False)
            if self.lastactive != -1:
                if self.type == ChartType.COL or self.type == ChartType.LINE:
                    update_rect.x = self.graph.x + (self.lastactive - first) * self.blkw
                    update_rect.y = self.graph.y - 6
                    update_rect.width = self.blkw
                    update_rect.height = self.graph.height + 12
                    widget.get_window().invalidate_rect(update_rect, False)
            if self.type == ChartType.COL or self.type == ChartType.LINE:
                update_rect.x = self.graph.x + (self.active - first) * self.blkw
                update_rect.y = self.graph.y - 6
                update_rect.width = self.blkw
                update_rect.height = self.graph.height + 12
                widget.get_window().invalidate_rect(update_rect, False)
        return retval

    def queue_redraw(self):
        if self.drawarea.get_realized():
            self.recompute()
            self.full_redraw(self.drawarea)
            self.drawarea.queue_draw()

    def plot(self, model, x_axis, y_axis):
        self.clear()
        self.nb_items = model.iter_n_children(None)
        self.items = []
        self.dual = len(y_axis) > 1
        if self.type == ChartType.PIE or self.type == ChartType.DOUGHNUT or self.type == ChartType.LINE:
            self.dual = False
        valid = model.get_iter_first()
        while valid is not None:
            label = model[valid][x_axis]
            values = [model[valid][i] for i in y_axis]

            if self.dual or self.abs:
                values = list(map(abs, values))
            self.rawmin = min(self.rawmin, min(values))
            self.rawmax = max(self.rawmax, max(values))
            item = ChartItem()
            item.label = label
            item.series = values
            self.items.append(item)
            if self.rawmin == self.rawmax:
                self.rawmin = 0
                self.rawmax = 100
            self.total += abs(values[0])
            valid = model.iter_next(valid)
        for item in self.items:
            strval = self.print_double(self.buffer1, item.series[0])
            item.rate = abs(item.series[0] * 100 / self.total)
            item.legend = "%s\n%s (%.f%%)" % (item.label, strval, item.rate)
        self.queue_draw()

    def set_type(self, t):
        if t == ChartType.DOUGHNUT:
            self.param_pie_donut = True
        else:
            self.param_pie_donut = False
        self.type = t
        self.dual = False
        self.queue_redraw()

    def set_color_scheme(self, index):
        self.color_scheme = ColorScheme(index)

    def set_minor_prefs(self, rate, symbol):
        self.minor_rate = rate
        self.minor_symbol = symbol

    def set_absolute(self, abs):
        self.abs = abs

    def set_currency(self, kcur):
        self.kcur = kcur

    def set_overdrawn(self, minimum):
        self.minimum = minimum

    def set_barw(self, barw):
        if GTK_CHART_MINBARW <= barw <= GTK_CHART_MAXBARW:
            self.usrbarw = barw
        else:
            self.usrbarw = 0

        if self.type != ChartType.PIE:
            self.queue_draw()

    def set_showmono(self, mono):
        self.show_mono = mono
        if self.type != ChartType.PIE:
            self.queue_redraw()

    def show_legend(self, visible, showextracol):
        self._show_legend = visible
        self._show_legend_wide = showextracol
        self.queue_redraw()

    def show_xval(self, visible):
        self._show_xval = visible

    def show_average(self, value, visible):
        self.average = value
        self._show_average = visible

    def show_overdrawn(self, visible):
        self.show_over = visible

    def show_minor(self, minor):
        self.minor = minor
        if self.type != ChartType.PIE:
            self.queue_redraw()

    def set_title(self, title):
        self.title = title
        self.queue_redraw()

    def set_subtitle(self, subtitle):
        self.subtitle = subtitle
        self.queue_redraw()


GObject.type_register(GsChart)

if __name__ == '__main__':
    model = Gtk.TreeStore(str, float, float, float)
    window = Gtk.Window()
    w = GsChart()
    w.set_type(ChartType.LINE)
    w.show_legend(False, False)
    w.set_color_scheme(ColMap.OFFICE2013)
    window.add(w)
    for b in [("Sauba", 22.67, 13, 54), ("Kafeero", 23.89, 27, 51), ("Kibalama", 21, 54, 42), ("Devota", 21, 67, 12),
              ("Esther", 19, 89, 62),
              ("Mariam", 18, 20, 75)]:
        model.append(None, b)
    w.plot(model=model, x_axis=0, y_axis=[1])
    w.set_title("Julaw Energy 2019 2020 DAY BOOKS TRIAL BALAMCE")
    w.set_subtitle("For The period")
    window.connect("destroy", lambda w: Gtk.main_quit())
    window.maximize()
    window.show_all()
    Gtk.main()
