from enum import IntEnum

MASKCOL = 255.0

AREA_ALPHA = 0.33
OVER_ALPHA = 0.15
PANGO_SCALE = 1024


def COLTO16(col8):
    return col8 | col8 << 8


def COLTOOVER(col8):
    return (col8 + MASKCOL) / 2


def OVER_COLOR():
    return MASKCOL * OVER_ALPHA


def COLTOCAIRO(col8):
    return col8 / 255.0


def COLTOCAIROOVER(col8):
    return ((col8 * (1 - OVER_ALPHA)) + OVER_COLOR()) / 255.0


class ColMap(IntEnum):
    HOMEBANK = 1
    MSMONEY = 2
    SAP = 3
    QUICKEN = 4
    OFFICE2010 = 5
    OFFICE2013 = 6
    ANALYTICS = 7
    YNAB = 8

    def __str__(self):
        return ["HomeBank", "Money", "SAP", "Quicken", "Office 2010",
                "Office 2013", "Analytics", "YNAB"][self.value - 1]


class ColorTypes(IntEnum):
    BLACK = 0
    WHITE = 1
    GREY1 = 2
    TEXT = 3
    XYLINES = 4
    THBASE = 5
    THTEXT = 6


class RgbCol:
    def __init__(self, r, g, b):
        self.r = r
        self.g = g
        self.b = b


ynab_colors = (RgbCol(239, 234, 172),
               RgbCol(143, 186, 209),
               RgbCol(211, 229, 134),
               RgbCol(163, 180, 120),
               RgbCol(167, 209, 195),
               RgbCol(51, 177, 191),
               RgbCol(214, 227, 99),
               RgbCol(246, 166, 209),
               RgbCol(131, 131, 131))
ynab_nbcolors = len(ynab_colors)

money_colors = (RgbCol(255, 193, 96),
                RgbCol(92, 131, 180),
                RgbCol(165, 88, 124),
                RgbCol(108, 124, 101),
                RgbCol(230, 121, 99),
                RgbCol(91, 160, 154),
                RgbCol(207, 93, 96),
                RgbCol(70, 136, 106),

                RgbCol(245, 163, 97),
                RgbCol(158, 153, 88),
                RgbCol(255, 140, 90),
                RgbCol(122, 151, 173),
                RgbCol(84, 142, 128),
                RgbCol(185, 201, 149),
                RgbCol(165, 99, 103),
                RgbCol(77, 140, 172),

                RgbCol(251, 228, 128),
                RgbCol(73, 99, 149),
                RgbCol(192, 80, 77),
                RgbCol(139, 180, 103),
                RgbCol(132, 165, 214),
                RgbCol(221, 216, 115),
                RgbCol(77, 103, 137),
                RgbCol(165, 181, 156))
money_nbcolors = len(money_colors)

quicken_colors = (RgbCol(226, 73, 13),
                  RgbCol(223, 180, 6),
                  RgbCol(124, 179, 0),
                  RgbCol(44, 108, 182),
                  RgbCol(184, 81, 186),
                  RgbCol(165, 165, 165),
                  RgbCol(122, 122, 122),
                  RgbCol(137, 42, 40),

                  RgbCol(70, 161, 100),
                  RgbCol(220, 106, 0),
                  RgbCol(113, 113, 113))
quicken_nbcolors = len(quicken_colors)

analytics_colors = (
    RgbCol(5, 141, 199),
    RgbCol(80, 180, 50),
    RgbCol(237, 86, 27),
    RgbCol(237, 239, 0),
    RgbCol(36, 203, 229),
    RgbCol(100, 229, 114),
    RgbCol(255, 150, 85),
    RgbCol(255, 242, 99),

    RgbCol(106, 249, 196),
    RgbCol(178, 222, 255),
    RgbCol(204, 204, 204))
analytics_nbcolors = len(analytics_colors)

office2010_colors = (
    RgbCol(60, 100, 149),
    RgbCol(150, 60, 59),
    RgbCol(120, 147, 68),
    RgbCol(99, 75, 123),
    RgbCol(61, 133, 157),
    RgbCol(196, 115, 49),
    RgbCol(73, 120, 176),
    RgbCol(179, 74, 71),
    RgbCol(144, 178, 84),
    RgbCol(117, 93, 153),
    RgbCol(73, 161, 185),
    RgbCol(232, 140, 65),
    RgbCol(126, 155, 199),
    RgbCol(202, 126, 126),
    RgbCol(174, 197, 129),
    RgbCol(156, 137, 182),
    RgbCol(123, 185, 206),
    RgbCol(248, 170, 121))

office2010_nbcolors = len(office2010_colors)

office2013_colors = (
    RgbCol(91, 155, 213),
    RgbCol(237, 125, 49),
    RgbCol(165, 165, 165),
    RgbCol(255, 192, 0),
    RgbCol(68, 114, 196),
    RgbCol(112, 173, 71),

    RgbCol(37, 94, 145),
    RgbCol(158, 72, 14),
    RgbCol(99, 99, 99),
    RgbCol(153, 115, 0),
    RgbCol(38, 68, 120),
    RgbCol(67, 104, 43),

    RgbCol(124, 175, 221),
    RgbCol(241, 151, 90),
    RgbCol(183, 183, 183),
    RgbCol(255, 205, 51),
    RgbCol(105, 142, 208),
    RgbCol(140, 193, 104))

office2013_nbcolors = len(office2013_colors)
sap_colors = (
    RgbCol(107, 148, 181),
    RgbCol(239, 205, 120),
    RgbCol(160, 117, 146),
    RgbCol(107, 181, 144),
    RgbCol(237, 164, 112),
    RgbCol(107, 106, 161),
    RgbCol(183, 213, 104),
    RgbCol(214, 128, 118),

    RgbCol(135, 115, 161),
    RgbCol(218, 217, 86),
    RgbCol(207, 111, 122),
    RgbCol(85, 168, 161),
    RgbCol(253, 213, 65),
    RgbCol(146, 98, 148),
    RgbCol(115, 192, 59),
    RgbCol(205, 81, 96),

    RgbCol(53, 180, 201),
    RgbCol(248, 175, 103),
    RgbCol(186, 97, 125),
    RgbCol(117, 202, 249),
    RgbCol(244, 131, 35),
    RgbCol(178, 45, 110),
    RgbCol(87, 229, 151),
    RgbCol(204, 171, 68),

    RgbCol(172, 110, 145),
    RgbCol(61, 132, 137),
    RgbCol(224, 117, 79),
    RgbCol(117, 84, 148),
    RgbCol(155, 206, 158),
    RgbCol(255, 133, 100),
    RgbCol(60, 98, 153),
    RgbCol(128, 197, 122))

sap_nbcolors = len(sap_colors)

homebank_colors = (
    RgbCol(72, 118, 176),
    RgbCol(180, 198, 230),
    RgbCol(227, 126, 35),
    RgbCol(238, 186, 123),
    RgbCol(97, 158, 58),
    RgbCol(175, 222, 142),
    RgbCol(184, 43, 44),
    RgbCol(231, 151, 149),
    RgbCol(136, 103, 185),
    RgbCol(190, 174, 210),
    RgbCol(127, 87, 77),
    RgbCol(184, 155, 147),
    RgbCol(202, 118, 190),
    RgbCol(230, 181, 208),
    RgbCol(126, 126, 126),
    RgbCol(198, 198, 198),
    RgbCol(187, 188, 56),
    RgbCol(218, 218, 144),
    RgbCol(109, 189, 205),
    RgbCol(176, 217, 228),

    RgbCol(237, 212, 0),
    RgbCol(255, 239, 101),
    RgbCol(207, 93, 96),
    RgbCol(234, 186, 187),
    RgbCol(193, 124, 17),
    RgbCol(240, 181, 90),
    RgbCol(186, 189, 182),
    RgbCol(225, 227, 223),
    RgbCol(115, 210, 22),
    RgbCol(175, 240, 112),
    RgbCol(255, 140, 90),
    RgbCol(255, 191, 165))
homebank_nbcolors = len(homebank_colors)

global_colors = (
    RgbCol(0, 0, 0),
    RgbCol(239, 239, 239),
    RgbCol(255, 255, 255),
    RgbCol(68, 68, 68),
    RgbCol(51, 51, 51),
    RgbCol(255, 255, 255),
    RgbCol(46, 52, 54),
    RgbCol(255, 255, 0),
    RgbCol(255, 0, 255),
    RgbCol(0, 255, 0),
    RgbCol(0, 0, 255),
    RgbCol(255, 255, 255),
    RgbCol(46, 52, 54)
)


def color_global_default():
    tcol = global_colors[ColorTypes.THBASE]
    tcol.r = 255
    tcol.g = 255
    tcol.b = 255
    tcol = global_colors[ColorTypes.THTEXT]
    tcol.r = 46
    tcol.g = 52
    tcol.b = 54


def cairo_user_set_rgbcol(cr, col):
    if isinstance(col, RgbCol):
        cr.set_source_rgb(COLTOCAIRO(col.r), COLTOCAIRO(col.g), COLTOCAIRO(col.b))


def cairo_user_set_rgbacol(cr, col, alpha):
    if isinstance(col, RgbCol):
        cr.set_source_rgba(COLTOCAIRO(col.r), COLTOCAIRO(col.g), COLTOCAIRO(col.b), alpha)


def cairo_user_set_rgbcol_over(cr, col, over):
    if over:
        cr.set_source_rgb(COLTOCAIROOVER(col.r), COLTOCAIROOVER(col.g), COLTOCAIROOVER(col.b))
    else:
        cr.set_source_rgb(COLTOCAIRO(col.r), COLTOCAIRO(col.g), COLTOCAIRO(col.b))


class ColorScheme:
    colors = RgbCol(0, 0, 0)
    nb_cols = 0
    cs_red = 0
    cs_green = 0
    cs_blue = 0
    cs_yellow = 0
    cs_orange = 0

    def __init__(self, index):
        self.cs_blue = 0
        if index == ColMap.HOMEBANK:
            self.colors = homebank_colors
            self.nb_cols = homebank_nbcolors
            self.cs_green = 4
            self.cs_red = 6
            self.cs_yellow = 2
            self.cs_orange = 2
        elif index == ColMap.MSMONEY:
            self.colors = money_colors
            self.nb_cols = money_nbcolors
            self.cs_blue = 17
            self.cs_green = 19
            self.cs_red = 18
            self.cs_yellow = 16
            self.cs_orange = 8
        elif index == ColMap.QUICKEN:
            self.colors = quicken_colors
            self.nb_cols = quicken_nbcolors
            self.cs_blue = 3
            self.cs_green = 2
            self.cs_red = 0
            self.cs_yellow = 1
            self.cs_orange = 9
        elif index == ColMap.ANALYTICS:
            self.colors = analytics_colors
            self.nb_cols = analytics_nbcolors
            self.cs_green = 1
            self.cs_red = 2
            self.cs_yellow = 3
            self.cs_orange = 6
        elif index == ColMap.OFFICE2010:
            self.colors = office2010_colors
            self.nb_cols = office2010_nbcolors
            self.cs_green = 2
            self.cs_red = 1
            self.cs_yellow = 5
            self.cs_orange = 5
        elif index == ColMap.OFFICE2013:
            self.colors = office2013_colors
            self.nb_cols = office2013_nbcolors
            self.cs_green = 5
            self.cs_red = 1
            self.cs_yellow = 3
            self.cs_orange = 1
        elif index == ColMap.SAP:
            self.colors = sap_colors
            self.nb_cols = sap_nbcolors
            self.cs_green = 14
            self.cs_red = 15
            self.cs_yellow = 12
            self.cs_orange = 20

        elif index == ColMap.YNAB:
            self.colors = ynab_colors
            self.nb_cols = ynab_nbcolors
            self.cs_blue = 3
            self.cs_green = 5
            self.cs_red = 0
            self.cs_orange = 1
