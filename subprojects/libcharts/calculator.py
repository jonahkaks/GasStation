from gi import require_version

require_version('Gtk', '3.0')
from gi.repository import Gtk


class GsCalculator(Gtk.Box):
    def __init__(self):
        super().__init__()
        self.buffer = ""
        self.number_grid = Gtk.Grid()
        self.number_grid.set_row_spacing(8)
        self.number_grid.set_column_spacing(8)
        self.number_grid.set_column_homogeneous(True)
        self.number_grid.set_row_homogeneous(True)
        self.entry = Gtk.TextView()
        sw = Gtk.ScrolledWindow()
        sw.add(self.entry)
        # self.entry.set_has_frame(False)
        self.buffer = ""
        self.stack = []
        self.set_orientation(Gtk.Orientation.VERTICAL)
        self.pack_start(sw, False, False, 5)
        col = 0
        row = 0
        for i, v in enumerate(['7', '8', '9', '/', 'U', 'C', '4', '5', '6', '*',
                               '(', ')', '1', '2', '3', '-', 'Sq', 'Sqrt', '0', '.', '%', '+', '=']):
            b = Gtk.Button(label=v)
            b.connect("clicked", self.button_clicked_cb, v)
            self.number_grid.attach(b, col, row, 1, 1)
            if col == 5:
                row += 1
                col = 0
                continue
            col += 1
        self.pack_start(self.number_grid, True, True, 0)
        self.set_homogeneous(False)

    def button_clicked_cb(self, button, num):
        if num.isdigit() or num == '+' or num == '-' or num == '.':
            self.buffer += num
            return


if __name__ == '__main__':
    window = Gtk.Window()
    window.set_border_width(10)
    window.set_size_request(600, 200)
    box = GsCalculator()
    window.add(box)
    window.connect("destroy", lambda w: Gtk.main_quit())
    window.show_all()
    Gtk.main()
