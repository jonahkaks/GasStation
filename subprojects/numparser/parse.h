#include <string.h>
#include <ctype.h>
#include <stdlib.h>
struct _Ast;
union UnionValue
{
    char x;
    double y;
};

typedef struct _token
{
    char *type;
    union UnionValue value;
} Token;


typedef struct
{
    char *text;
} Lexer;

typedef struct
{
    Lexer *lexer;
    Token *ctoken;
} Parser;

typedef struct
{
    Parser *parser;
} Interpreter;


typedef struct
{
    struct _Ast *left;
    Token *token, *op;
    struct _Ast *right;
} BinOp;

typedef struct
{
    Token *token;
    double value;
} Num;

union Node
{
    BinOp *bo;
    Num *num;
};
typedef struct _Ast
{
    int type;
    union Node node;
} Ast;
Ast *new_BinOp (Ast * left, Token * op, Ast * right);
Ast *new_Num (Token * token);
double lexer_numeric (Lexer * lexer);
Token *lexer_get_next_token (Lexer * lexer);
void parser_init (Parser * p, Lexer * l);
void parser_eat (Parser * p, const char *token_type);
Ast *parser_factor (Parser * p);
Ast *parser_term (Parser * p);
Ast *parser_expr (Parser * p);
double parse_text (char *c);
double interp_interprete (Interpreter * inp);
