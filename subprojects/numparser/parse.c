#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define NUMERIC "NUMERIC"
#define PLUS "PLUS"
#define MINUS "MINUS"
#define MUL "MUL"
#define DIV "DIV"
#define LPAREN "("
#define RPAREN ")"
#define END "EOF"
struct _Ast;
union UnionValue
{
    char x;
    double y;
};

typedef struct _token
{
    char *type;
    union UnionValue value;
} Token;


typedef struct
{
    char *text;
} Lexer;

typedef struct
{
    Lexer *lexer;
    Token *ctoken;
} Parser;

typedef struct
{
    Parser *parser;
} Interpreter;


typedef struct
{
    struct _Ast *left;
    Token *token, *op;
    struct _Ast *right;
} BinOp;

typedef struct
{
    Token *token;
    double value;
} Num;

union Node
{
    BinOp *bo;
    Num *num;
};
typedef struct _Ast
{
    int type;
    union Node node;
} Ast;
Ast *new_BinOp (Ast * left, Token * op, Ast * right);
Ast *new_Num (Token * token);
double lexer_numeric (Lexer * lexer);
Token *lexer_get_next_token (Lexer * lexer);
void parser_init (Parser * p, Lexer * l);
void parser_eat (Parser * p, const char *token_type);
Ast *parser_factor (Parser * p);
Ast *parser_term (Parser * p);
Ast *parser_expr (Parser * p);
double parse_text (const char *c);
double interp_interprete (Interpreter * inp);

Ast *
new_BinOp (Ast * left, Token * op, Ast * right)
{
    Ast *x = malloc (sizeof (Ast));
    BinOp *bo=malloc(sizeof(BinOp));
    x->type = 1;
    bo->left = left;
    bo->right = right;
    bo->token = op;
    bo->op = op;
    x->node.bo = bo;
    return x;

}

Ast *
new_Num (Token * token)
{
    Ast *x = malloc (sizeof (Ast));
    Num *n = malloc(sizeof(Num));
    x->type = 0;
    n->token = token;
    n->value = token->value.y;
    x->node.num = n;
    return x;

}

double
lexer_numeric (Lexer * lexer)
{
    char *ptr = lexer->text;
    char s[15]={'\0'};
    while (*ptr != '\0'){
        if (isdigit (*ptr) || *ptr == '.'){
            strncat (s, ptr, 1);
            ptr++;
            lexer->text++;
        } else if ((*ptr >= 'a' && *ptr <= 'z') || (*ptr >= 'A' && *ptr <= 'Z') || *ptr == ','){
            ptr++;
            lexer->text++;
        }
        else{
            break;
        }

    }
    printf("STRING IS %s\n", s);
    return strtod (s, NULL);
}

Token *
lexer_get_next_token (Lexer * lexer)
{
    Token *tok = malloc (sizeof (Token));
    char *ptr = lexer->text;
    while (*ptr != '\0')
    {

        if isdigit
        (*ptr)
        {
            tok->type = NUMERIC;
            tok->value.y = lexer_numeric (lexer);
            return tok;
        }
        else if (*ptr == '+')
        {
            lexer->text++;
            tok->type = PLUS;
            tok->value.x = '+';
            return tok;
        }
        else if (*ptr == '-')
        {
            lexer->text++;
            tok->type = MINUS;
            tok->value.x = '-';
            return tok;
        }
        else if (*ptr == '/')
        {
            lexer->text++;
            tok->type = DIV;
            tok->value.x = '/';
            return tok;
        }
        else if (*ptr == '*')
        {
            lexer->text++;
            tok->type = MUL;
            tok->value.x = '*';
            return tok;
        }
        else if (*ptr == ')')
        {
            lexer->text++;
            tok->type = RPAREN;
            tok->value.x = ')';
            return tok;
        }
        else if (*ptr == '(')
        {
            lexer->text++;
            tok->type = LPAREN;
            tok->value.x = '(';
            return tok;
        }
        lexer->text++;
        *ptr++;
    }
    tok->value.x='E';
    tok->type = END;
    return tok;

}

void
parser_init (Parser * p, Lexer * l)
{
    p->lexer = l;
    p->ctoken = lexer_get_next_token (l);
}

void
parser_eat (Parser * p, const char *token_type)
{
    if (strcmp (p->ctoken->type, token_type) == 0)
    {

        p->ctoken = lexer_get_next_token (p->lexer);
    }
}

Ast *
parser_factor (Parser * p)
{
    Ast *x=NULL;
    Token *token = p->ctoken;
    if (strcmp(token->type,NUMERIC)==0)
    {
        parser_eat (p, NUMERIC);
        x = new_Num (token);
    }
    else if (strcmp(token->type,LPAREN)==0)
    {
        parser_eat (p, LPAREN);
        x = parser_expr (p);
        parser_eat (p, RPAREN);
    }
    return x;
}

Ast *
parser_term (Parser * p)
{
    Token *token = NULL;
    Ast *ast=NULL;
    ast = parser_factor (p);

    while (strcmp(p->ctoken->type, MUL)==0 || strcmp(p->ctoken->type, DIV)==0)
    {
        token = p->ctoken;
        if (strcmp(token->type, MUL)==0)
        {
            parser_eat (p, MUL);
        }
        else if (strcmp(token->type, DIV)==0)
        {
            parser_eat (p, DIV);
        }
        ast = new_BinOp (ast, token, parser_factor (p));
    }
    return ast;
}

Ast *
parser_expr (Parser * p)
{
    Token *token;
    Ast *ast;
    ast= parser_term (p);
    while (strcmp(p->ctoken->type, PLUS)==0 || strcmp(p->ctoken->type,MINUS)==0)
    {
        token = p->ctoken;
        if (strcmp(token->type,PLUS)==0)
        {
            parser_eat (p, PLUS);
        }
        else if (strcmp(token->type, MINUS)==0)
        {
            parser_eat (p, MINUS);
        }
        ast = new_BinOp (ast, token, parser_term (p));
    }
    return ast;

}

double
interp_visit (Ast * ast)
{
    double ans=0.0;
    if (ast->type == 0)
    {
        ans =  ast->node.num->value;
    }
    else
    {
        if (strcmp(ast->node.bo->op->type, PLUS)==0)
        {
            ans= interp_visit (ast->node.bo->left) + interp_visit (ast->node.bo->right);

        }
        else if (strcmp(ast->node.bo->op->type, MINUS)==0)
        {
            ans= interp_visit (ast->node.bo->left) - interp_visit (ast->node.bo->right);
        }
        else if (strcmp(ast->node.bo->op->type, MUL)==0)
        {
            ans= interp_visit (ast->node.bo->left) * interp_visit (ast->node.bo->right);
        }
        else if (strcmp(ast->node.bo->op->type, DIV)==0)
        {
            ans= interp_visit (ast->node.bo->left) / interp_visit (ast->node.bo->right);
        }
        free(ast->node.bo);
        free(ast);

    }
    return ans;
}

double
interp_interprete (Interpreter * inp)
{
    return interp_visit (parser_expr (inp->parser));
}

double
parse_text (const char *c)
{
    if (c == NULL)
        return 0.0;
    if (*c == 0)
        return 0.0;
    Lexer l = { c };
    Parser p;
    parser_init (&p, &l);
    Interpreter inp;
    inp.parser = &p;
    double ans= interp_interprete (&inp);
    return ans;

}

//cc -fPIC -shared -o libparse.so parser.c
