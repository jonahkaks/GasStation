"""types

Revision ID: fcbc93b5d26e
Revises: 
Create Date: 2022-08-12 15:36:05.756651

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'fcbc93b5d26e'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('account', 'type',
               existing_type=sa.VARCHAR(length=25),
               type_=sa.Enum('INVALID', 'NONE', 'BANK', 'CASH', 'FIXED_ASSET', 'CURRENT_ASSET', 'CREDIT', 'LONG_TERM_LIABILITY', 'CURRENT_LIABILITY', 'STOCK', 'MUTUAL', 'CURRENCY', 'INCOME', 'EXPENSE', 'COST_OF_SALES', 'EQUITY', 'RECEIVABLE', 'PAYABLE', 'ROOT', 'TRADING', 'NUM_ACCOUNT_TYPES', 'CHECKING', 'SAVINGS', 'MONEYMRKT', 'CREDITLINE', 'LAST', name='accounttype'),
               existing_nullable=False, postgresql_using='type::accounttype')
    op.alter_column('address', 'type',
               existing_type=sa.INTEGER(),
               type_=sa.Enum('HOME', 'WORK', 'OTHER', name='addresstype'),
               existing_nullable=True, postgresql_using="case when type = 0 THEN 'HOME'::addresstype when type = 1 "
                                                        "THEN 'WORK'::addresstype ELSE 'OTHER'::addresstype END")
    op.alter_column('email', 'type',
               existing_type=sa.INTEGER(),
               type_=sa.Enum('WORK', 'HOME', 'OTHER', name='emailfortype'),
               existing_nullable=True, postgresql_using="case when type = 0 THEN 'WORK'::emailfortype when type = 1 "
                                                                         "THEN 'HOME'::emailfortype ELSE 'OTHER'::emailfortype END")
    op.alter_column('phone', 'type',
               existing_type=sa.INTEGER(),
               type_=sa.Enum('MOBILE', 'WORK', 'HOME', 'MAIN', 'WORK_FAX', 'HOME_FAX', 'PAGER', 'OTHER', name='phonetype'),
               existing_nullable=True, postgresql_using="case when type = 0 THEN 'MOBILE'::phonetype "
                                                        "when type = 1 THEN 'WORK'::phonetype "
                                                        "when type = 2 THEN 'HOME'::phonetype "
                                                        "when type = 3 THEN 'MAIN'::phonetype "
                                                        "when type = 4 THEN 'WORK_FAX'::phonetype "
                                                        "when type = 5 THEN 'HOME_FAX'::phonetype "
                                                        "when type = 6 THEN 'PAGER'::phonetype "
                                                        "ELSE 'OTHER'::phonetype END")
    # op.alter_column('product', 'income_account',
    #            existing_type=postgresql.UUID(),
    #            nullable=False)
    op.alter_column('product_category', 'name',
               existing_type=sa.VARCHAR(length=30),
               type_=sa.VARCHAR(length=60),
               existing_nullable=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('product_category', 'name',
               existing_type=sa.VARCHAR(length=60),
               type_=sa.VARCHAR(length=30),
               existing_nullable=False)
    op.alter_column('product', 'income_account',
               existing_type=postgresql.UUID(),
               nullable=True)
    op.alter_column('phone', 'type',
               existing_type=sa.Enum('MOBILE', 'WORK', 'HOME', 'MAIN', 'WORK_FAX', 'HOME_FAX', 'PAGER', 'OTHER', name='phonetype'),
               type_=sa.INTEGER(),
               existing_nullable=True)
    op.alter_column('email', 'type',
               existing_type=sa.Enum('WORK', 'HOME', 'OTHER', name='emailfortype'),
               type_=sa.INTEGER(),
               existing_nullable=True)
    op.alter_column('address', 'type',
               existing_type=sa.Enum('HOME', 'WORK', 'OTHER', name='addresstype'),
               type_=sa.INTEGER(),
               existing_nullable=True)
    op.alter_column('account', 'type',
               existing_type=sa.Enum('INVALID', 'NONE', 'BANK', 'CASH', 'FIXED_ASSET', 'CURRENT_ASSET', 'CREDIT', 'LONG_TERM_LIABILITY', 'CURRENT_LIABILITY', 'STOCK', 'MUTUAL', 'CURRENCY', 'INCOME', 'EXPENSE', 'COST_OF_SALES', 'EQUITY', 'RECEIVABLE', 'PAYABLE', 'ROOT', 'TRADING', 'NUM_ACCOUNT_TYPES', 'CHECKING', 'SAVINGS', 'MONEYMRKT', 'CREDITLINE', 'LAST', name='accounttype'),
               type_=sa.VARCHAR(length=25),
               existing_nullable=False)
    # ### end Alembic commands ###
