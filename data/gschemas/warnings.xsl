<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" encoding="UTF-8" indent="no"/>
<xsl:strip-space elements="*"/>
<xsl:template match="*">
<xsl:for-each select="//schema[@id='org.jonah.warnings.permanent']/key">
PREF_WARN_<xsl:value-of select="translate(@name,$smallcase,$uppercase)"/> = "<xsl:value-of select="@name"/>"
</xsl:for-each>
class WarningSpec:
    def __init__(self, name, desc, long_desc):
        self.name = name
        self.desc = desc
        self.long_desc = long_desc

warnings = [<xsl:for-each select="//schema[@id='org.jonah.warnings.permanent']/key">
WarningSpec(PREF_WARN_<xsl:value-of select="translate(@name,$smallcase,$uppercase)"/>,
               "<xsl:value-of select="normalize-space(summary)"/>",
               "<xsl:value-of select="normalize-space(description)"/>"),</xsl:for-each>]
</xsl:template>
<xsl:variable name="smallcase" select="'-abcdefghijklmnopqrstuvwxyz'" />
<xsl:variable name="uppercase" select="'_ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
</xsl:stylesheet> 